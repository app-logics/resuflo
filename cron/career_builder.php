<?php

define('PROJECT_DIR_JJ', '../');
include_once(PROJECT_DIR_JJ.'config.php');
include_once(PROJECT_DIR_JJ.'lib/DatabaseConnection2.php');

//$base = '../upload/DirectResume';
$base = DIRECTRESUME_UPLOAD_PATH;

$fi = new FilesystemIterator($base, FilesystemIterator::SKIP_DOTS);
if(iterator_count($fi) != 0){
    die('Wait for read operation');
}

$db = DatabaseConnection::getInstance();

error_reporting(E_ALL);
ini_set('display_errors', true);
ini_set('display_startup_errors', true);
error_reporting(E_ALL);

//5 minutes
ini_set('max_execution_time', 300);

function GetToken($Username, $Password){
    $request = 'http://ws.careerbuilder.com/resumes/resumes.asmx/BeginSessionV2?Packet=<Packet><Email>'.$Username.'</Email><Password>'.$Password.'</Password></Packet>';
    $token = '';
    $keyword = '';
    $request = $request.$token.$keyword;
    $session = curl_init($request);
    curl_setopt($session, CURLOPT_HEADER, false);
    curl_setopt($session, CURLOPT_RETURNTRANSFER, true);
    $response = curl_exec($session);
    curl_close($session);
    $status_code = array();
    preg_match('/\d\d\d/', $response, $status_code);
    if($status_code[0])
    {
      //echo 'Your call to Web Services returned an unexpected HTTP status of:' . $status_code[0];
    }
    $xml = simplexml_load_string(htmlspecialchars_decode($response));
    $token =  $xml->Packet->SessionToken;
    return $token;
}

function SearchResume(){
    //cbResumeFromLastDay, cbIndustry, cbEducationLevel, cbIsAuthInUS left
    $sql = "select
	user_id, user_name, cbUsername, cbPassword,  search_criteria, cbSearchLimit-(select count(*) from cb_resume where userId = User_id and  DATE_FORMAT(cb_resume.date_created,'%Y-%m-%d')  = DATE_FORMAT(NOW(),'%Y-%m-%d')) cbSearchLimit,
        cbFromDay,cbResumeFromLastDay,cbState,cbIsWillingToRelocate,cbCity,cbZipCode,cbRadius,cbJobTitle,cbCompany,cbIndustry,cbEducationLevel,cbExperienceLevel,
        cbMinSalary,cbMaxSalary,cbEmployment,cbHours,cbIsAuthInUS,cbIsUSCitizen,cbHasClearance
        from user left join 
        timezone on user.timezone = timezone.name 
        where cbActive = 1 and access_level <> 0 and search_criteria <> '' and cbSearchLimit > 0
        and ( cbIsSunday = DAYOFWEEK(CONVERT_TZ(DATE_ADD(NOW(), INTERVAL 1 HOUR),timezone.value,'-06:00')) or cbIsMonday = DAYOFWEEK(CONVERT_TZ(DATE_ADD(NOW(), INTERVAL 1 HOUR),timezone.value,'-06:00')) or 
              cbIsTuesday = DAYOFWEEK(CONVERT_TZ(DATE_ADD(NOW(), INTERVAL 1 HOUR),timezone.value,'-06:00')) or cbIsWednesday = DAYOFWEEK(CONVERT_TZ(DATE_ADD(NOW(), INTERVAL 1 HOUR),timezone.value,'-06:00')) or 
              cbIsThursday = DAYOFWEEK(CONVERT_TZ(DATE_ADD(NOW(), INTERVAL 1 HOUR),timezone.value,'-06:00')) or cbIsFriday = DAYOFWEEK(CONVERT_TZ(DATE_ADD(NOW(), INTERVAL 1 HOUR),timezone.value,'-06:00')) or 
              cbIsSaturday = DAYOFWEEK(CONVERT_TZ(DATE_ADD(NOW(), INTERVAL 1 HOUR),timezone.value,'-06:00'))
            )
        and cbScheduleTime <  DATE_FORMAT(CONVERT_TZ(DATE_ADD(NOW(), INTERVAL 1 HOUR),'-06:00','-06:00'), '%H:%i:%s')";
    //echo $sql;
    $getUsers = mysql_query($sql);
    $rows = 10;
    while($user = mysql_fetch_array($getUsers))
    {
        $SessionToken = GetToken($user["cbUsername"], $user["cbPassword"]);
        $page = 1;
        $count = 0;
        //career builder resumes limit for 24 hours
        $cbSearchLimit = $user["cbSearchLimit"];
        //in one iteration on fetch maximum 20 resumes
        if($cbSearchLimit > 20){
            $cbSearchLimit = 20;
        }
        while($count < $cbSearchLimit){
            $request = 'http://ws.careerbuilder.com/resumes/resumes.asmx/V2_AdvancedResumeSearch?Packet='.
                    urlencode('<Packet><SessionToken>'.$SessionToken.'</SessionToken>'
                            . '<Keywords>'.$user["search_criteria"].'</Keywords>');
            if($user["cbSearchPattern"] != ''){
                $request .= urlencode('<SearchPattern>'.$user["cbSearchPattern"].'</SearchPattern>');
            }
            if($user["cbFromDay"] >= 0){
                $request .= urlencode('<FreshnessInDays>'.$user["cbFromDay"].'</FreshnessInDays>');
            }
            if($user["cbState"] != ''){
                $request .= urlencode('<State>'.$user["cbState"].'</State>');
            }
            if($user["cbIsWillingToRelocate"] == 1){
                $request .= urlencode('<RelocationFilter>RS</RelocationFilter>');
            }
            if($user["cbCity"] != ''){
                $request .= urlencode('<City>'.$user["cbCity"].'</City>');
            }
            if($user["cbZipCode"] >= 0){
                $request .= urlencode('<ZipCode>'.$user["cbZipCode"].'</ZipCode>');
            }
            if($user["cbRadius"] >= 0){
                $request .= urlencode('<SearchRadiusInMiles>'.$user["cbRadius"].'</SearchRadiusInMiles>');
            }
            if($user["cbJobTitle"] != ''){
                $request .= urlencode('<JobTitle>'.$user["cbJobTitle"].'</JobTitle>');
            }
            if($user["cbCompany"] != ''){
                $request .= urlencode('<Company>'.$user["cbCompany"].'</Company>');
            }
            if(strpos($user["cbExperienceLevel"], '-') !== FALSE){
                $minMaxExperience = explode('-', $user["cbExperienceLevel"]);
                $request .= urlencode('<CBMinimumExperience>'.$minMaxExperience[0].'</CBMinimumExperience>');
                $request .= urlencode('<CBMaximumExperience>'.$minMaxExperience[1].'</CBMaximumExperience>');
            }
            if($user["cbMinSalary"] > 0){
                $request .= urlencode('<MinimumSalary>'.$user["cbMinSalary"].'</MinimumSalary>');
            }
            if($user["cbMaxSalary"] > 0){
                $request .= urlencode('<MaximumSalary>'.$user["cbMaxSalary"].'</MaximumSalary>');
            }
            if($user["cbEmployment"] != '' || $user["cbHours"] != ''){
                $request .= urlencode('<EmploymentType>');
                $request .= urlencode($user["cbEmployment"]);
                //if both are not empty then add piple |
                if($user["cbEmployment"] != '' && $user["cbHours"] != ''){
                    $request .= urlencode('|');
                }
                $request .= urlencode($user["cbHours"]);
                $request .= urlencode('</EmploymentType>');
            }
            if($user["cbIsUSCitizen"] == 1){
                $request .= urlencode('<WorkStatus>CTCT</WorkStatus>');
            }
            if($user["cbHasClearance"] == 1){
                $request .= urlencode('<SecurityClearance>Yes</SecurityClearance>');
            }
                            
            $request .= urlencode('<PageNumber>'.$page++.'</PageNumber>'
            . '<RowsPerPage>'.$rows.'</RowsPerPage></Packet>');
            
            //die(urldecode($request));
            $session = curl_init($request);
            curl_setopt($session, CURLOPT_HEADER, false);
            curl_setopt($session, CURLOPT_RETURNTRANSFER, true);
            $response = curl_exec($session);
            curl_close($session);
            //die($response);
            $status_code = array();
            preg_match('/\d\d\d/', $response, $status_code);
            if($status_code[0])
            {
              echo 'Status Code:' . $status_code[0].'<br>';
            }
            $xml = simplexml_load_string(htmlspecialchars_decode($response));
            //die($xml->Packet->Hits);
            //die($xml->Packet);
            echo '<br>============ Page '.($count+1).' '.$user["user_name"].' ============<br>';
            if($xml->Packet->Hits > 0)
            {
                foreach ($xml->Packet->Results->ResumeResultItem_V3 as $element) {
                    echo $element->ResumeID.'<br>';
                    //Display all sub tags
                    //foreach ($element as $key => $val){
                        //echo "{$key}: {$val}".'<br>';
                    //}
                    $emailExistsSql = 'select * from cb_resume where emailmd5 = \''.$element->ContactEmailMD5.'\' and userid = '.$user["user_id"];
                    //Email not exists already
                    $Result = mysql_query($emailExistsSql);
                    if(mysql_num_rows($Result) ==0){
                        PullResume($SessionToken, $element->ResumeID, $user["user_name"]);
                        $insertResumeSql = 'INSERT INTO cb_resume (userId,resumeId,emailMd5, isParsed)VALUES ('.$user["user_id"].',\''.$element->ResumeID.'\',\''.$element->ContactEmailMD5.'\', 1)';
                        //echo $insertResumeSql.'<br>';
                        mysql_query($insertResumeSql);
                        if(++$count >= $cbSearchLimit)
                        break;
                    }
                }
                //last page is fetched from search packet
                if($page > $xml->Packet->MaxPage){
                    break;
                }
            }
            else {
                break;
            }
        }
    }
    
}

function PullResume($SessionToken, $ResumeId, $Username){
    $request = 'http://ws.careerbuilder.com/resumes/resumes.asmx/V2_GetResume?Packet='.
        urlencode('<Packet><SessionToken>'.$SessionToken.'</SessionToken>'
                . '<ResumeID>'.$ResumeId.'</ResumeID>'
                . '</Packet>');
    $session = curl_init($request);
    curl_setopt($session, CURLOPT_HEADER, false);
    curl_setopt($session, CURLOPT_RETURNTRANSFER, true);
    $response = curl_exec($session);
    curl_close($session);
    $status_code = array();
    preg_match('/\d\d\d/', $response, $status_code);
    if($status_code[0])
    {
      echo 'Status Code:' . $status_code[0].'<br>';
    }
    $xml = simplexml_load_string(htmlspecialchars_decode($response));
    //die($response);
    foreach ($xml->Packet as $element) {
        $date = new DateTime();
        $str = $element->ResumeText;
        //die($str);
        
        $resumeText = 'Resume Date-Time: '.$date->format('d/m/Y h:m:s').PHP_EOL;
        $resumeText .= 'First Name: '.explode(' ', $element->ContactName)[0].PHP_EOL;
        $resumeText .= 'Last Name: '.explode(' ', $element->ContactName)[1].PHP_EOL;
        $resumeText .= 'Email Address: '.$element->ContactEmail.PHP_EOL;
        if(empty($element->ContactPhone)){
            preg_match("/(\d{3}-\d{3}-\d{4})/", $str, $phonematches);
            $resumeText .= 'Phone: '.$phonematches[1].PHP_EOL;
        }
        else{
            $resumeText .= 'Phone: '.$element->ContactPhone.PHP_EOL;
        }
        //print_r($xml->Packet->HomeLocation);
        if(empty($xml->Packet->HomeLocation)){
            preg_match("/[\n]*([a-zA-Z ]*),\s*(\w{2})[.]*\s*(\d{5})/", $str, $matches);
            print_r($matches);
            $resumeText .= 'City: '.$matches[1].PHP_EOL;
            $resumeText .= 'State: '.$matches[2].PHP_EOL;
            $resumeText .= 'Zipcode: '.$matches[3].PHP_EOL;
        }
        else{
            $resumeText .= 'City: '.$xml->Packet->HomeLocation->City.PHP_EOL;
            $resumeText .= 'State: '.$xml->Packet->HomeLocation->State.PHP_EOL;
            $resumeText .= 'Zipcode: '.$xml->Packet->HomeLocation->ZipCode.PHP_EOL;
        }
        $resumeText .= 'Export Date-Time: '.$date->format('d/m/Y h:m:s').PHP_EOL;
        $resumeText .= 'Resume Source: CareerBuilder - Resumes (WSI recent)'.PHP_EOL;
        $resumeText .= '<<Resume Content Follows>>'.PHP_EOL.PHP_EOL.PHP_EOL;

        //echo $element->ContactEmail.'<br>';
        //echo $element->ResumeText.'<br>';
        //Display all sub tags
        //foreach ($element as $key => $val){
            //echo "{$key}: {$val}".'<br>';
        //}
        
        //$Directory = '..\Upload\DirectResume\\'.$Username;
        $Directory = DIRECTRESUME_UPLOAD_PATH.$Username;
        //echo $Directory;
        $ResumeLogPath = $Directory.'\\'.$element->ContactEmail.'.txt';
        if(!is_dir($Directory)){
            mkdir($Directory, 0777, true);
        }
	file_put_contents($ResumeLogPath, $resumeText.$element->ResumeText, FILE_APPEND);        
    }
}

//goto resume;

//$token = GetToken('harvester4@mcclainrecruit.com', 'qwert@123');
//die($token);
//$token = '6624c36ab8854ab68f66069cf649f85c-519836086-X0-6';
SearchResume();
//PullResume($token, 'RHQ1NC6B6ZS9LTLGJ8N', 'REM');
//PullResume('6624c36ab8854ab68f66069cf649f85c-519836086-X0-6','RHV84P758JHW1VLXZB1', 'REM');
//PullResume('5319d254f354422b8adcb995d223dc85-519070292-X0-6', 'RHT4QY75RV8D6NSWC8Y');


