<?php
// only allow this to be run from the command line
//if (!empty($_SERVER['HTTP_HOST'])) die();

$recipients[] = 'malikabiid@gmail.com';
$recipients[] = 'jeff@recruiters-edge.com';

include_once('../config.php');
include_once('../lib/DatabaseConnection2.php');
include_once('../lib/phpmailer6/vendor/autoload.php');
include_once('../lib/UserMailer.php');
include_once('../Setting.php');

$dripReportDate = date('M jS, Y');
$db = DatabaseConnection::getInstance();

$failedQuery = 'SELECT 
            COUNT(*) error_count, u.user_name, dq.smtp_error
            FROM 
                drip_queue dq LEFT JOIN 
                user u ON dq.entered_by = u.user_id
            WHERE sent = 3 AND date_modified >  current_date() 
            GROUP BY dq.entered_by';

$successQuery = 'SELECT 
        COUNT(*) success_count, u.user_name
        FROM 
            drip_queue dq LEFT JOIN 
            user u ON dq.entered_by = u.user_id
        WHERE sent = 1 AND date_modified >  current_date() 
        GROUP BY dq.entered_by';
$FailReport = '<br><strong>Failed Drip Emails </strong><table style="border-collapse: collapse;"><thead><th style="text-align: left;border: 1px solid lightgrey;padding: 0px 15px;">Date</th><th style="text-align: left;border: 1px solid lightgrey;padding: 0px 15px;">User</th><th  style="text-align: left;border: 1px solid lightgrey;padding: 0px 15px;">Fail Count</th><th  style="text-align: left;border: 1px solid lightgrey;padding: 0px 15px;">Error</th></thead>';
$SuccessReport .= '<br><strong>Success Drip Emails </strong><table style="border-collapse: collapse;"><thead><th style="text-align: left;border: 1px solid lightgrey;padding: 0px 15px;">Date</th><th style="text-align: left;border: 1px solid lightgrey;padding: 0px 15px;">User</th><th  style="text-align: left;border: 1px solid lightgrey;padding: 0px 15px;">Success Count</th></thead>';

$totalFailed = 0;
$totalSent = 0;

foreach(\Configuration\Setting::Databases as $dbNames){
    $db = mysql_select_db($dbNames);
    $result = mysql_query($failedQuery);
    while($driprow = mysql_fetch_assoc($result)) {
        $userName = $driprow['user_name'];
        $errorCount = $driprow['error_count'];
        $smtpError = $driprow['smtp_error'];

        $FailReport .= '<tr><td style="border: 1px solid lightgrey;padding: 0px 15px;">'.$dripReportDate.'</td><td style="border: 1px solid lightgrey;padding: 0px 15px;">'.$userName.'</td><td style="border: 1px solid lightgrey;padding: 0px 15px;">'.$errorCount.'</td><td style="border: 1px solid lightgrey;padding: 0px 15px;">'.$smtpError.'</td></tr>';
        $totalFailed = $totalFailed + $errorCount;
    }

    $result = mysql_query($successQuery);
    while($driprow = mysql_fetch_assoc($result)) {
        $userName = $driprow['user_name'];
        $successCount = $driprow['success_count'];

        $SuccessReport .= '<tr><td style="border: 1px solid lightgrey;padding: 0px 15px;">'.$dripReportDate.'</td><td style="border: 1px solid lightgrey;padding: 0px 15px;">'.$userName.'</td><td style="border: 1px solid lightgrey;padding: 0px 15px;">'.$successCount.'</td></tr>';
        $totalSent = $totalSent + $successCount;
    }
}
$FailReport .= '</table>';
$SuccessReport .= '</table>';

$Body = '<table><tr><td><strong>Total Attempted</strong> </td><td>'.($totalFailed+$totalSent).'</td></tr>';
$Body .= '<tr><strong>Total Success</strong> </td><td>'.$totalSent.'</td></tr>';
$Body .= '<tr><strong>Total Failed</strong> </td><td>'.$totalFailed.'</td></tr></table></br></br>';
$Body .= $FailReport;
$Body .= $SuccessReport;

try{
    file_put_contents('C:/Windows/Temp/ResufloLog/DripReport.txt', PHP_EOL.(new DateTime())->format('d m Y H:i:s').'===== START Drip Report', FILE_APPEND);
    $email = new UserMailer(1);
    $result = $email->SendEmail($recipients, null, 'Resuflo Drip Report '.$dripReportDate, $Body);
    file_put_contents('C:/Windows/Temp/ResufloLog/DripReport.txt', '=> END ====='.$result, FILE_APPEND);
    if($result == true){
        echo '<strong>Report Sent</strong><br>'.$Body;
    }
    else{
        echo '<strong>Report Failed</strong><br>'.$Body;
    }
}
catch (Exception $e) {
    file_put_contents('C:/Windows/Temp/ResufloLog/DripReport.txt', (new DateTime())->format('d m Y H:i:s').'END Drip Report ====='.$e->getMessage().PHP_EOL, FILE_APPEND);
}
    