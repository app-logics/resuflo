<?php
require 'vendor/autoload.php';

session_start();

$client = new Google_Client();
$client->setAuthConfigFile('client_secret_989107603101-b9etopp2afp4mvoba9lgdsagevbnj1sa.apps.googleusercontent.com.json');
//$client->setRedirectUri('http://' . $_SERVER['HTTP_HOST'] . '/gcal/oauth2callback.php');
//$client->setRedirectUri('http://' . $_SERVER['HTTP_HOST'] . '?m=settings&amp;a=calender');
$client->setRedirectUri('http://' . $_SERVER['HTTP_HOST'] . '/Resuflo/GoogleCalAPI/oauth2callback.php');
//$client->setRedirectUri('https://' . $_SERVER['HTTP_HOST'] . '/GoogleCalAPI/oauth2callback.php');
$client->addScope(Google_Service_Drive::DRIVE_METADATA_READONLY);

if (! isset($_GET['code'])) {
    $auth_url = $client->createAuthUrl();
    header('Location: ' . filter_var($auth_url, FILTER_SANITIZE_URL));
} else{
    $client->authenticate($_GET['code']);
    $accessToken = $client->getAccessToken();
    echo $accessToken.PHP_EOL;
    $client->setAccessToken($accessToken);

    if ($client->isAccessTokenExpired()) {
        $client->refreshToken($client->getRefreshToken());
        $accessToken = $client->getAccessToken();
        echo $accessToken;
    }
//    define('PROJECT_DIR_JJ', '../');
//    include_once(PROJECT_DIR_JJ.'config.php');
//    include_once(PROJECT_DIR_JJ.'lib/DatabaseConnection2.php');
//    $db = DatabaseConnection::getInstance();
//
//    $sql = "update user set google_username='', google_access_token='$accessToken' WHERE user_id = $_GET[id]";
//    echo $sql;
//    $db->query($sql);

    $redirect_uri = 'http://' . $_SERVER['HTTP_HOST'] . '/Resuflo/index.php?m=settings&a=calender&code='.$accessToken;
    //$redirect_uri = 'https://' . $_SERVER['HTTP_HOST'] . '/index.php?m=settings&a=calender&code='.$accessToken;
    echo $redirect_uri;
    header('Location: ' . filter_var($redirect_uri, FILTER_SANITIZE_URL));
}