<?php
include_once('./GoogleCalAPI/vendor/autoload.php');

class GoogleAPIClient {
    private $_userID;
    private $_siteID;
    private $_db;
    private $client;
    public function __construct($userId)
    {
        $this->_userID = $userId;
        $this->_db = DatabaseConnection::getInstance();
        $this->client = new Google_Client();
    }
    public function GetGmailClient($accessToken)
    {
        $this->client->setApplicationName('Gmail API');
        $this->client->setScopes(Google_Service_Gmail::GMAIL_SEND);
        try{
            $this->client->setAuthConfigFile('./GoogleCalAPI/gmail_client_credentials.json');    
        } catch (Exception $ex) {
            $this->client->setAuthConfigFile(__DIR__.'/../GoogleCalAPI/gmail_client_credentials.json');
        }
        $this->client->setAccessType('offline');
        $this->client->setPrompt('select_account consent');
        
        $this->client->setAccessToken($accessToken);

        if ($this->client->isAccessTokenExpired()) {
            $this->client->refreshToken($this->client->getRefreshToken());
            $newAccessToken = $this->client->getAccessToken();
            $this->client->setAccessToken($newAccessToken);
            $query="select * from dripmarket_configuration where WHERE google_access_token='".$accessToken."'";
            $resourcedetail=mysql_query($query);
            $row = mysql_fetch_array($resourcedetail);
            if($row['google_access_token'] == $accessToken){
                $configQuery = "UPDATE `dripmarket_configuration` SET google_access_token='".$newAccessToken."' WHERE google_access_token='".$accessToken."'";
                $result = mysql_query($configQuery);
            }
            else if($row['google_access_token2'] == $accessToken)
            {
                $configQuery = "UPDATE `dripmarket_configuration` SET google_access_token2='".$newAccessToken."' WHERE google_access_token2='".$accessToken."'";
                $result = mysql_query($configQuery);
            }
        }
    }
    function getLabels()
    {
        try 
        {
            $service = new Google_Service_Gmail($this->client);
            $user = 'me';
            $labels = $service->users_labels->listUsersLabels($user);
            return $labels;
        } catch (Exception $e) {
            print 'An error occurred: ' . $e->getMessage();
        }
    }
    function sendMessage($base64String) 
    {
        try 
        {
            $message = new Google_Service_Gmail_Message();
            $message->setRaw($base64String);
            $service = new Google_Service_Gmail($this->client);
            $message = $service->users_messages->send('me', $message);
            return $message->labelIds[0] == 'SENT';
        } catch (Exception $e) {
            print 'An error occurred: ' . $e->getMessage();
        }
    }
}
