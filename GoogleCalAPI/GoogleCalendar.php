<?php
include_once('./GoogleCalAPI/vendor/autoload.php');

define('APPLICATION_NAME', 'Google Calendar API PHP Quickstart');
define('CREDENTIALS_PATH', './GoogleCalAPI/.credentials/calendar-php-quickstart.json');
define('CLIENT_SECRET_PATH', './GoogleCalAPI/client_secret.json');
define('USER_ID', $_GET['id']);
// If modifying these scopes, delete your previously saved credentials
// at ~/.credentials/calendar-php-quickstart.json
define('SCOPES', implode(' ', array(
  Google_Service_Calendar::CALENDAR)
));

//if (php_sapi_name() != 'cli') {
//  throw new Exception('This application must be run on the command line.');
//}

/**
 * Returns an authorized API client.
 * @return Google_Client the authorized client object
 */
function getNewAccessToken($accessToken){
    
    $client = new Google_Client();
    $client->setApplicationName(APPLICATION_NAME);
    $client->setScopes(SCOPES);
    $client->setAuthConfigFile(CLIENT_SECRET_PATH);
    $client->setAccessType('offline');
    
    $client->setAccessToken($accessToken);
    
    if ($client->isAccessTokenExpired()) {
        $client->refreshToken($client->getRefreshToken());
        $accessToken = $client->getAccessToken();
        $client->setAccessToken($accessToken);
        //$token = json_decode($accessToken);
        $result = $accessToken; //$token->access_token;
    }else{
        $result = $accessToken;
    }
    return $result;
}

function getClient($accessToken) {
    $client = new Google_Client();
    $client->setApplicationName(APPLICATION_NAME);
    $client->setScopes(SCOPES);
    $client->setAuthConfigFile(CLIENT_SECRET_PATH);
    $client->setAccessType('offline');
    
    $client->setAccessToken($accessToken);
    
    if ($client->isAccessTokenExpired()) {
        $client->refreshToken($client->getRefreshToken());
        $accessToken = $client->getAccessToken();
        $client->setAccessToken($accessToken);
        //echo $accessToken;
    }
    return $client;
}

/**
 * Expands the home directory alias '~' to the full path.
 * @param string $path the path to expand.
 * @return string the expanded path.
 */
function expandHomeDirectory($path) {
  $homeDirectory = getenv('HOME');
  if (empty($homeDirectory)) {
    $homeDirectory = getenv("HOMEDRIVE") . getenv("HOMEPATH");
  }
  return str_replace('~', realpath($homeDirectory), $path);
}
function AddEventToGoogle($accessToken, $Subject, $Location, $StartTime, $EndTime, $TimeZone, $allDay, $Description, $Body){

//   $StartingTime = date("Ymd\THis", strtotime($StartTime));
//   $EndingTime = date("Ymd\THis", strtotime($EndTime));
//   echo $StartingTime;
//   exit;
    // Get the API client and construct the service object.
//    'start' => array(
//        'dateTime' => '2016-02-28T09:00:00-07:00',
//        'timeZone' => 'America/Los_Angeles',
//      ),
    $client = getClient($accessToken);
    $service = new Google_Service_Calendar($client);

    $event = new Google_Service_Calendar_Event(array(
      'summary' => $Subject,
      'location' => $Location,
      'description' => $Description,
      'start' => array(
        'dateTime' => $StartTime,
        'timeZone' => $TimeZone,
      ),
      'end' => array(
        'dateTime' => $EndTime,
        'timeZone' => $TimeZone,
      ),
//      'recurrence' => array(
//        'RRULE:FREQ=DAILY;COUNT=2'
//      ),
//      'attendees' => array(
//        array('email' => 'malikabiid@gmail.com'),
//        array('email' => 'malikabiid@gmail.com'),
//      ),
      'reminders' => array(
        'useDefault' => True,
      ),
    ));

    $calendarId = 'primary';
    try {
            $event = $service->events->insert($calendarId, $event);
            return "<div>Google Sync: Event added to google calendar</div>".PHP_EOL;
    } catch (Exception $e) {
            return "<div>Google Sync: ".$e->getMessage()."</div>".PHP_EOL;
    }
//    printf('Event created: %s\n', $event->htmlLink);
}
?>