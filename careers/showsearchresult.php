<?php session_start();
/*
 * RESUFLO
 * Careers Page Display Module
 *
 * RESUFLO Version: 0.8.0 (Jhelum)
 *
 * Copyright (C) 2005 - 2007 Econn Technologies, Inc.
 *
 *
 * The contents of this file are subject to the RESUFLO Public License
 * Version 1.1a (the "License"); you may not use this file except in
 * compliance with the License. You may obtain a copy of the License at
 * http://www.econnoisseur.com/. Software distributed under the License is
 * distributed on an "AS IS" basis, WITHOUT WARRANTY OF ANY KIND, either
 * express or implied. See the License for the specific language governing
 * rights and limitations under the License.
 *
 * Software distributed under the License is distributed on an "AS IS"
 * basis, WITHOUT WARRANTY OF ANY KIND, either express or implied. See the
 * License for the specific language governing rights and limitations
 * under the License.
 *
 * The Original Code is "RESUFLO Standard Edition".
 *
 * The Initial Developer of the Original Code is Econn Technologies, Inc.
 * Portions created by the Initial Developer are Copyright (C) 2005 - 2007
 * (or from the year in which this file was created to the year 2007) by
 * Econn Technologies, Inc. All Rights Reserved.
 *
 * $Id: index.php 1623 2007-02-06 18:38:58Z will $
 */
 include("../config.php");
 $conn = mysql_connect(DATABASE_HOST,DATABASE_USER,DATABASE_PASS);
 mysql_select_db(DATABASE_NAME,$conn);

$sql = mysql_query("select * from joborder where joborder_id IN (select joborder_id from candidate_joborder where candidate_id='".$_GET['cid']."')");
$str = '<!DOCTYPE html PUBLIC "-//W3C//DTD XHTML 1.0 Transitional//EN"
"http://www.w3.org/TR/xhtml1/DTD/xhtml1-transitional.dtd">
<html>
    <head>
        <meta http-equiv="Content-Type" content="text/html; charset=UTF-8" />
        <title>Bank My Resume - Careers</title>
            <script type="text/javascript" src="../js/careerPortalApply.js"></script>
                    <script type="text/javascript" src="../js/lib.js"></script>
            <script type="text/javascript" src="../js/sorttable.js"></script>

            <script type="text/javascript" src="../js/calendarDateInput.js"></script>
                <style type="text/css" media="all">
            table.sortable
{
text-align:left;
empty-cells: show;
width: 940px;
}
td
{
padding:5px;
}
tr.rowHeading
{
 background: #e0e0e0; border: 1px solid #cccccc; border-left: none; border-right: none;
}
tr.oddTableRow
{
background: #ebebeb; 
}
tr.evenTableRow
{
 background: #ffffff; 
}
a.sortheader:hover,
a.sortheader:link,
a.sortheader:visited
{
color:#000;
}

body, html { margin: 0; padding: 0; background: #ffffff; font: normal 12px/14px Arial, Helvetica, sans-serif; color: #000000; }
#container { margin: 0 auto; padding: 0; width: 940px; height: auto; }
#logo { float: left; margin: 0; }
	#logo img { /*width: 424px; height: 103px; */}
#actions { float: right; margin: 0; width: 310px; height: 100px; background: #efefef; border: 1px solid #cccccc; }
	#actions img { float: left; margin: 2px 6px 2px 15px; width: 130px; height: 25px; }
#footer { clear: both; margin: 20px auto 0 auto; width: 150px; }
	#footer img { width: 137px; height: 38px; }

a:link, a:active { color: #1763b9; }
a:hover { color: #c75a01; }
a:visited { color: #333333; }
img { border: none; }

h1 { margin: 0 0 10px 0; font: bold 18px Arial, Helvetica, sans-serif; }
h2 { margin: 8px 0 8px 15px; font: bold 14px Arial, Helvetica, sans-serif; }
h3 { margin: 0; font: bold 14px Arial, Helvetica, sans-serif; }
p { font: normal 12px Arial, Helvetica, sans-serif; }
p.instructions { margin: 0 0 0 10px; font: italic 12px Arial, Helvetica, sans-serif; color: #666666; }


/* CONTENTS ON PAGE SPECS */
#careerContent { clear: both; padding: 15px 0 0 0; }

	
/* DISPLAY JOB DETAILS */
#detailsTable { width: 400px; }
	#detailsTable td.detailsHeader { width: 30%; }
div#descriptive { float: left; width: 585px; }
div#detailsTools { float: right; padding: 0 0 8px 0; width: 280px; background: #ffffff; border: 1px solid #cccccc; }
	div#detailsTools img { margin: 2px 6px 5px 15px;  }

/* DISPLAY APPLICATION FORM */
div.applyBoxLeft, div.applyBoxRight { width: 450px; height: 470px; background: #f9f9f9; border: 1px solid #cccccc; border-top: none; }
div.applyBoxLeft { float: left; margin: 0 10px 0 0; }
div.applyBoxRight { float: right; margin: 0 0 0 10px; }
	div.applyBoxLeft div, div.applyBoxRight div { margin: 0 0 5px 0; padding: 3px 10px; background: #efefef; border-top: 1px solid #cccccc; border-bottom: 1px solid #cccccc; }
	div.applyBoxLeft table, div.applyBoxRight table { margin: 0 auto; width: 420px; }
	div.applyBoxLeft table td, div.applyBoxRight table td { padding: 3px; vertical-align: top; }
		td.label { text-align: right; width: 110px; }
        form#applyToJobForm {  }
	form#applyToJobForm label { font-weight: bold; }
	form#applyToJobForm input.inputBoxName, form#applyToJobForm input.inputBoxNormal { width: 285px; height: 15px; }
        form#applyToJobForm input.submitButton { width: 197px; height: 27px; background: url(\'../images/careers_submit.gif\') no-repeat; }

        form#applyToJobForm input.submitButtonDown { width: 197px; height: 27px; background: url(\'../images/careers_submit-o.gif\') no-repeat; }
	form#applyToJobForm textarea { margin: 8px 0 0 0; width: 410px; height: 170px; }
	form#applyToJobForm textarea.inputBoxArea{ width: 285px; height: 70px; }

			#poweredRESUFLO { clear: both; margin: 30px auto; clear: both; width: 140px; height: 40px; border: none;}
			#poweredRESUFLO img { border: none; }
        </style>
    </head>
    <body>

    <!-- TOP -->
    <div id="container">
	<div id="logo"><img src="../images/careers_cats.gif" alt="IMAGE: CATS Applicant Tracking System Careers Page" /></div>
    <div id="actions">

    	<h2>Shortcuts:</h2>
        <a href="index.php" onmouseover="buttonMouseOver(\'returnToMain\',true);" onmouseout="buttonMouseOver(\'returnToMain\',false);"><img src="../images/careers_return.gif" id="returnToMain" alt="IMAGE: Return to Main" /></a>
<a href="../rss/" onmouseover="buttonMouseOver(\'rssFeed\',true);" onmouseout="buttonMouseOver(\'rssFeed\',false);"><img src="../images/careers_rss.gif" id="rssFeed" alt="IMAGE: RSS Feed" /></a>
        <a href="index.php?m=careers&p=showAll" onmouseover="buttonMouseOver(\'showAllJobs\',true);" onmouseout="buttonMouseOver(\'showAllJobs\',false);"><img src="../images/careers_show.gif" id="showAllJobs" alt="IMAGE: Show All Jobs" /></a>
    </div>
    <!-- CONTENT -->
    <div id="careerContent">
        <form style="padding:0;margin:0;border:0;" name="logout" id="logout" method="post" action="index.php?m=careers&p=showAll"><input type="hidden" id="pa" name="pa" value="" /><div style="margin: 20px 0 20px 0; line-height: 18px;"> <h3 style="font-weight: normal;"><b>Welcome back Gurpreet.</b>&nbsp;&nbsp;Not Gurpreet? <a href="javascript:void(0);" onclick="document.getElementById(\'pa\').value=\'logout\'; document.logout.submit();">Log Out</a>.&nbsp;&nbsp;Need to update your information? <a href="javascript:void(0);" onclick="document.getElementById(\'pa\').value=\'updateProfile\'; document.logout.submit();">Update Profile</a>.</h3><br><br></div>

        <h1>Search Result:'.$_POST['keyword'].'</h1>
<table class="sortable" style="width:100%;">
<tr class="rowHeading" align="left">
<th nowrap="nowrap">Title</th><th nowrap="nowrap" align="left">Duration</th><th nowrap="nowrap" align="left">Salary</th><th nowrap="nowrap" align="left">Location</th><th nowrap="nowrap" align="left">Action</th></tr>
';
/*$i=1;
while($row = mysql_fetch_array($sql)){
if($i%2==0){
$class= "oddTableRow";
}else{
$class = "evenTableRow";
}
$str.="<tr class='".$class."'><td>".$row['title']."</td><td>".$row['duration']."</td><td>".$row['salary']."</td><td>".$row['city']." ".$row['state']."</td><td><a href='ignore.php?jobid=".$row['joborder_id']."&canid=".$_GET['cid']."' onclick='return confirm(\"Are you sure to ignore this job?This action will remove your application to selected job.\");'>Ignore</a>  <a href='viewstatus.php?jobid=".$row['joborder_id']."&cid=".$_GET['cid']."'>View Status</a></td></tr>";
$i++;
}*/

$str.='</table>
    </div>
    <!-- FOOTER -->
    </div>    <div style="font-size:9px;">

        <br /><br /><br /><br />
    </div>
    <div style="text-align:center;">

                <div id="poweredRESUFLO">
		<img src="../images/RESUFLO_powered.gif" alt="Powered by: RESUFLO - Applicant Tracking System" title="Powered by: RESUFLO - Applicant Tracking System" />
		</div>
    </div>
    <script type="text/javascript">st_init();</script>

    </body>
</html>
';

echo $str;



?>
