<?php session_start();
/*
 * RESUFLO
 * Careers Page Display Module
 *
 * RESUFLO Version: 0.8.0 (Jhelum)
 *
 * Copyright (C) 2005 - 2007 Econn Technologies, Inc.
 *
 *
 * The contents of this file are subject to the RESUFLO Public License
 * Version 1.1a (the "License"); you may not use this file except in
 * compliance with the License. You may obtain a copy of the License at
 * http://www.econnoisseur.com/. Software distributed under the License is
 * distributed on an "AS IS" basis, WITHOUT WARRANTY OF ANY KIND, either
 * express or implied. See the License for the specific language governing
 * rights and limitations under the License.
 *
 * Software distributed under the License is distributed on an "AS IS"
 * basis, WITHOUT WARRANTY OF ANY KIND, either express or implied. See the
 * License for the specific language governing rights and limitations
 * under the License.
 *
 * The Original Code is "RESUFLO Standard Edition".
 *
 * The Initial Developer of the Original Code is Econn Technologies, Inc.
 * Portions created by the Initial Developer are Copyright (C) 2005 - 2007
 * (or from the year in which this file was created to the year 2007) by
 * Econn Technologies, Inc. All Rights Reserved.
 *
 * $Id: index.php 1623 2007-02-06 18:38:58Z will $
 */
 include("../config.php");

 $conn = mysql_connect(DATABASE_HOST,DATABASE_USER,DATABASE_PASS);
 mysql_select_db(DATABASE_NAME,$conn);

include("header.php");

$str = '
<script type="text/javascript">

function checkpassword(){
	if(document.getElementById("old_pwd").value==""){
	alert("Please enter old password");
	return false;	
	}
	if(document.getElementById("new_pwd").value==""){
	alert("Please enter New password");
	return false;	
	}
	if(document.getElementById("retype_pwd").value==""){
	alert("Please retype new password");
	return false;	
	}
	if(document.getElementById("new_pwd").value!=document.getElementById("retype_pwd").value){
	alert("New password and retype password field value do not match.");
	return false;	
	}

}

</script>
        <h1> Reset your password</h1>
		<form action="savenewpassword.php" method="post" onsubmit="return checkpassword();">
		<input type="hidden" name="id" value="'.$_GET['cid'].'">
		<table>
		<tr><td>Old pasword: </td><td><input type="text" name="old_pwd" id="old_pwd"></td></tr>
		<tr><td>New password: </td><td><input type="text" name="new_pwd" id="new_pwd"></td></tr>
		<tr><td>Retype password: </td><td><input type="text" name="retype_pwd" id="retype_pwd"></td></tr>
		<tr><td colspan="2"><input type="submit" name="sub" value="Submit"></td></tr>
		</table>
		</form>
   </div>
    <!-- FOOTER -->
    </div>    <div style="font-size:9px;">

        <br /><br /><br /><br />
    </div>
    <div style="text-align:center;">

                <div id="poweredRESUFLO">
		<img src="../images/RESUFLO_powered.gif" alt="Powered by: RESUFLO - Applicant Tracking System" title="Powered by: RESUFLO - Applicant Tracking System" />
		</div>
    </div>
    <script type="text/javascript">st_init();</script>

    </body>
</html>
';

echo $str;



?>
