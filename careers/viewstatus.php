<?php session_start();
/*
 * RESUFLO
 * Careers Page Display Module
 *
 * RESUFLO Version: 0.8.0 (Jhelum)
 *
 * Copyright (C) 2005 - 2007 Econn Technologies, Inc.
 *
 *
 * The contents of this file are subject to the RESUFLO Public License
 * Version 1.1a (the "License"); you may not use this file except in
 * compliance with the License. You may obtain a copy of the License at
 * http://www.econnoisseur.com/. Software distributed under the License is
 * distributed on an "AS IS" basis, WITHOUT WARRANTY OF ANY KIND, either
 * express or implied. See the License for the specific language governing
 * rights and limitations under the License.
 *
 * Software distributed under the License is distributed on an "AS IS"
 * basis, WITHOUT WARRANTY OF ANY KIND, either express or implied. See the
 * License for the specific language governing rights and limitations
 * under the License.
 *
 * The Original Code is "RESUFLO Standard Edition".
 *
 * The Initial Developer of the Original Code is Econn Technologies, Inc.
 * Portions created by the Initial Developer are Copyright (C) 2005 - 2007
 * (or from the year in which this file was created to the year 2007) by
 * Econn Technologies, Inc. All Rights Reserved.
 *
 * $Id: index.php 1623 2007-02-06 18:38:58Z will $
 */
 include("../config.php");
 $conn = mysql_connect(DATABASE_HOST,DATABASE_USER,DATABASE_PASS);
 mysql_select_db(DATABASE_NAME,$conn);

$sql = mysql_query("select * from joborder where joborder_id =".$_GET['jobid']);
$row = mysql_fetch_array($sql);

include("header.php");

$str = '
        <h1> Status for job: '.$row['title'].'</h1>
<table class="sortable" style="width:100%;">
<tr class="rowHeading" align="left">
<th nowrap="nowrap">Status Changed on</th><th nowrap="nowrap" align="left">Changed from</th><th nowrap="nowrap" align="left">Changed to</th></tr>
';
$sqlStatus = mysql_query("select candidate_joborder_status_id,short_description from candidate_joborder_status where is_enabled=1");

while($rowStatus=mysql_fetch_array($sqlStatus)){
$statusArr[$rowStatus['candidate_joborder_status_id']]=$rowStatus['short_description'];
}

$sqlStatusHistory = mysql_query("select * from candidate_joborder_status_history where candidate_id='".$_GET['cid']."' and joborder_id='".$_GET['jobid']."'");
$i=1;
while($rowHis = mysql_fetch_array($sqlStatusHistory)){
if($i%2==0){
$class= "oddTableRow";
}else{
$class = "evenTableRow";
}
$str.="<tr class='".$class."'><td>".$rowHis['date']."</td><td>".$statusArr[$rowHis['status_from']]."</td><td>".$statusArr[$rowHis['status_to']]."</td></tr>";
$i++;
}

$str.='</table>';




$sqlEvent = mysql_query("select * from calendar_event where data_item_id='".$_GET['cid']."' and joborder_id='".$_GET['jobid']."' and public=1");
if(mysql_num_rows($sqlEvent)){

$str.='<br><br><br><h1> Scheduled Events</h1>
<table class="sortable" style="width:100%;">
<tr class="rowHeading" align="left">
<th nowrap="nowrap">Scheduled on</th><th nowrap="nowrap" align="left">Description</th></tr>
';

$i=1;
while($rowEvent = mysql_fetch_array($sqlEvent)){
if($i%2==0){
$class= "oddTableRow";
}else{
$class = "evenTableRow";
}
$str.="<tr class='".$class."'><td valign='top'>".$rowEvent['date']."</td><td>
<table>
<tr><td valign='top'><strong>Title:</strong></td><td>".$rowEvent['title']."</td></tr>
<tr><td><strong>Venue:</strong></td><td>".$rowEvent['venue']."</td></tr>
<tr><td><strong>Person to meet:</strong></td><td>".$rowEvent['person_to_meet']."</td></tr>
<tr><td><strong>Contact Number:</strong></td><td>".$rowEvent['contact_numbers']."</td></tr>
<tr><td><strong>Email id:</strong></td><td>".$rowEvent['email_id']."</td></tr>
<tr><td><strong>Description:</strong></td><td>".$rowEvent['description']."</td></tr>
</table>
</td></tr>";
$i++;
}

$str.='</table>';
}
$str.=' </div>
    <!-- FOOTER -->
    </div>    <div style="font-size:9px;">

        <br /><br /><br /><br />
    </div>
    <div style="text-align:center;">

                <div id="poweredRESUFLO">
		<img src="../images/RESUFLO_powered.gif" alt="Powered by: RESUFLO - Applicant Tracking System" title="Powered by: RESUFLO - Applicant Tracking System" />
		</div>
    </div>
    <script type="text/javascript">st_init();</script>

    </body>
</html>
';

echo $str;



?>
