<?php 
/*
 * RESUFLO
 * Careers Page Display Module
 *
 * RESUFLO Version: 0.8.0 (Jhelum)
 *
 * Copyright (C) 2005 - 2007 Econn Technologies, Inc.
 *
 *
 * The contents of this file are subject to the RESUFLO Public License
 * Version 1.1a (the "License"); you may not use this file except in
 * compliance with the License. You may obtain a copy of the License at
 * http://www.econnoisseur.com/. Software distributed under the License is
 * distributed on an "AS IS" basis, WITHOUT WARRANTY OF ANY KIND, either
 * express or implied. See the License for the specific language governing
 * rights and limitations under the License.
 *
 * Software distributed under the License is distributed on an "AS IS"
 * basis, WITHOUT WARRANTY OF ANY KIND, either express or implied. See the
 * License for the specific language governing rights and limitations
 * under the License.
 *
 * The Original Code is "RESUFLO Standard Edition".
 *
 * The Initial Developer of the Original Code is Econn Technologies, Inc.
 * Portions created by the Initial Developer are Copyright (C) 2005 - 2007
 * (or from the year in which this file was created to the year 2007) by
 * Econn Technologies, Inc. All Rights Reserved.
 *
 * $Id: index.php 1623 2007-02-06 18:38:58Z will $
 */

$careerPage = true;

chdir('..');
include_once('./lib/RESUFLOUtility.php');
include_once(RESUFLOUtility::getIndexName());
//echo "<pre>";
//print_r($_SESSION);


?>


