LOCKFILE=/tmp/jjparser-file-mover-running.txt
if [ -e ${LOCKFILE} ] && kill -0 `cat ${LOCKFILE}`; then
    echo 'file mover running and found itself';
    exit
fi

# make sure the lockfile is removed when we exit and then claim it
trap "rm -f ${LOCKFILE}; exit" INT TERM EXIT
echo $$ > ${LOCKFILE}

# some evil php files have been coming in to the parsing folder
# in an attempt to (a) stop them and (b) see if they are coming
# from the infogist vps, we're going to obliterate any non TXT!
ssh jeffvps 'find ./test1/ -type f ! -name *.TXT -exec rm -f {} \;'

# create the spatial demarcation for which files may move
# aka only move folders that haven't been modified within the last 3 mins
ssh jeffvps 'touch -d "10 minutes ago" ./jjparser/tenminsago'

# only scp over the files that fall older than the spatial demarcation
for dir in $(ssh jeffvps 'find ./jjparser/ -type d -not -newer ./jjparser/tenminsago'); do
  #echo $dir
  scp -rCq jeffvps:$dir /var/www/vhosts/staging.resuflocrm.com/httpdocs/upload/resume/
  ssh jeffvps "rm -rf $dir"
done

# remove the spatial demarcation
ssh jeffvps 'rm ./jjparser/tenminsago'

# remove the lockfile
rm -f ${LOCKFILE}
exit
