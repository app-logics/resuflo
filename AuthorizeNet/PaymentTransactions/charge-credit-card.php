<?php
  include_once 'vendor/autoload.php';
  include_once './Setting.php';

  use net\authorize\api\contract\v1 as AnetAPI;
  use net\authorize\api\controller as AnetController;

  define("AUTHORIZENET_LOG_FILE", "phplog");

function chargeCreditCard($firstName, $lastName, $cardNumber, $expiryDate, $CC, $amount, $invoiceNumber, $description)
{
    /* Create a merchantAuthenticationType object with authentication details
       retrieved from the constants file */
    $merchantAuthentication = new AnetAPI\MerchantAuthenticationType();
    $merchantAuthentication->setName(\Configuration\Setting::AuthorizeNet_MERCHANT_LOGIN_ID);
    $merchantAuthentication->setTransactionKey(\Configuration\Setting::AuthorizeNet_MERCHANT_TRANSACTION_KEY);
    
    // Set the transaction's refId
    $refId = 'ref' . time();

    // Create the payment data for a credit card
    $creditCard = new AnetAPI\CreditCardType();
    $creditCard->setCardNumber($cardNumber);
    $creditCard->setExpirationDate($expiryDate);
    $creditCard->setCardCode($CC);

    // Add the payment data to a paymentType object
    $paymentOne = new AnetAPI\PaymentType();
    $paymentOne->setCreditCard($creditCard);

    // Create order information
    $order = new AnetAPI\OrderType();
    $order->setInvoiceNumber($invoiceNumber);
    $order->setDescription($description);

    // Set the customer's Bill To address
    $customerAddress = new AnetAPI\CustomerAddressType();
    $customerAddress->setFirstName($firstName);
    $customerAddress->setLastName($lastName);
    //$customerAddress->setCompany("Soft Logics");
    //$customerAddress->setAddress("14 Main Street");
    //$customerAddress->setCity("Pecan Springs");
    //$customerAddress->setState("TX");
    //$customerAddress->setZip("44628");
    //$customerAddress->setCountry("USA");

    // Set the customer's identifying information
    //$customerData = new AnetAPI\CustomerDataType();
    //$customerData->setType("individual");
    //$customerData->setId("99999456654");
    //$customerData->setEmail("EllenJohnson@example.com");

    // Add values for transaction settings
    //$duplicateWindowSetting = new AnetAPI\SettingType();
    //$duplicateWindowSetting->setSettingName("duplicateWindow");
    //$duplicateWindowSetting->setSettingValue("60");

    // Add some merchant defined fields. These fields won't be stored with the transaction,
    // but will be echoed back in the response.
    //$merchantDefinedField1 = new AnetAPI\UserFieldType();
    //$merchantDefinedField1->setName("customerLoyaltyNum");
    //$merchantDefinedField1->setValue("1128836273");

    //$merchantDefinedField2 = new AnetAPI\UserFieldType();
    //$merchantDefinedField2->setName("favoriteColor");
    //$merchantDefinedField2->setValue("blue");

    // Create a TransactionRequestType object and add the previous objects to it
    $transactionRequestType = new AnetAPI\TransactionRequestType();
    $transactionRequestType->setTransactionType("authCaptureTransaction");
    $transactionRequestType->setAmount($amount);
    $transactionRequestType->setOrder($order);
    $transactionRequestType->setPayment($paymentOne);
    $transactionRequestType->setBillTo($customerAddress);
    //$transactionRequestType->setCustomer($customerData);
    //$transactionRequestType->addToTransactionSettings($duplicateWindowSetting);
    //$transactionRequestType->addToUserFields($merchantDefinedField1);
    //$transactionRequestType->addToUserFields($merchantDefinedField2);

    // Assemble the complete transaction request
    $request = new AnetAPI\CreateTransactionRequest();
    $request->setMerchantAuthentication($merchantAuthentication);
    $request->setRefId($refId);
    $request->setTransactionRequest($transactionRequestType);

    // Create the controller and get the response
    $controller = new AnetController\CreateTransactionController($request);
    $response = $controller->executeWithApiResponse(\Configuration\Setting::AUTHORIZE_PRODUCTION);
    
    return $response;
}
