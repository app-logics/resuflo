<?php

// only allow this to be run from the command line
if (!empty($_SERVER['HTTP_HOST'])) die();

$dtime = date('Y-m-d H:i:s');
ob_start();
$dir = str_replace('que/'.basename(__FILE__), '', __FILE__).'/';
include "$dir/asset/config.php";
include_once("$dir/lib/Importoptout.php");

include_once('../lib/phpmailer6/vendor/autoload.php');
include_once('../lib/UserMailer.php');

$emails_per_five_minutes = '250';
//$email_debug = 'jay@toptal.com,jeff@recruiters-edge.com';
$email_debug = 'jeff@recruiters-edge.com';

$sql = '
select
  q.id drip_queue_id,
  cj.candidate_joborder_id,
  u.user_name, u.first_name as clientname,
  c.candidate_id, c.email1, c.first_name, c.last_name, c.middle_name, c.phone_home, c.campaign_date,
  dc.cname, dc.id, dc.userid, dc.default,
  dce.id dce_id, dce.subject, dce.creationdate, dce.hour, dce.min,
  dce.Message, dce.Timezone, dce.sendemailon,
  dcfg.fromemail, dcfg.bccemail, dcfg.smtpuser, dcfg.smtphost, dcfg.email_type,
  dcfg.smtppassword, dcfg.smtpport, dcfg.fromname,
  dq.id as quesid
from
  drip_queue q left join
  candidate c on q.candidate_id = c.candidate_id left join
  candidate_joborder cj on c.candidate_id = cj.candidate_id left join
  user u on u.user_id = q.entered_by left join
  dripmarket_compaign dc on dc.id = q.drip_campaign_id left join
  dripmarket_compaignemail dce on dce.id = q.drip_campemail_id left join
  dripmarket_questionaire dq on dq.userid = dc.userid left join
  dripmarket_configuration dcfg on dc.userid=dcfg.userid 
where
  q.sent = 0
order by
  smtp_error, q.id
limit '.$emails_per_five_minutes;
//echo $sql.'<br><br>';

$resourcedetail= mysql_query($sql);
$output = '';

while($row = mysql_fetch_array($resourcedetail)) {

  $scheduledDate = $row['scheduledDate'];
  $user_name = $row['user_name'];
  $candidateName = $row['first_name']." ".$row['middle_name']." ".$row['last_name'];
  $candidateEmailExploding = explode(",",$row['email1']);
  $candidateEmail = $candidateEmailExploding[0];
  $questionaireurl='<a href="'.questionaireurl.'?quesid='.$row['quesid'].'">Please click here</a>';
  $quesidencode=base64_encode($row['quesid']);
  $cidencode=base64_encode($row['candidate_id']);
  $unsubscribeurl='To remove your name from our candidate list <a href="'.unsubscribeurl.'?quesid='.$quesidencode.'&cid='.$cidencode.'">Please click here</a>';

  if (empty($candidateEmail)) continue;

  if (empty($output)) '***Drip (default campaigns) Report for '.date('Y-m-d H:i')."***\n\n";

  $bodytext=stripslashes($row['Message']);
  $candidate_id = $row['candidate_id'];
  $userid = $row['userid'];
  $subject = $row['subject'];
  //Insert Activity
  $sqlInsertActivity = "INSERT INTO activity (data_item_id, data_item_type, joborder_id, entered_by, type, notes, site_id, date_created, date_modified )VALUES ( '$candidate_id', '100', '0', '$userid', '200', '$subject (drip1)', '1', NOW(), NOW() )";
//  mysql_query($sqlInsertActivity);

  $output .= 'candidate ('.$row['candidate_id'].') email: '.$candidateEmail.', from: '.$row['fromemail'].' ('.$row['fromname'].') ';

  try{
    $datefunc=date("d-M-y");
    $bodytext=str_replace('$$NAME$$',$row['first_name'],$bodytext);
    $bodytext=str_replace('$$FIRSTNAME$$',$row['first_name'],$bodytext);
    $bodytext=str_replace('$$LASTNAME$$',$row['last_name'],$bodytext);

    $bodytext=str_replace('$$EMAIL$$',$row['email1'],$bodytext);
    $bodytext=str_replace('$$PHONE$$',$row['phone_home'],$bodytext);
    $bodytext=str_replace('$$STARTDATE$$',$row['date_created'],$bodytext);
    $bodytext=str_replace('$$CURDATE$$',$datefunc,$bodytext);
    $bodytext=str_replace('$$QUESTIONNAIRE$$',$questionaireurl,$bodytext);
    $bodytext=str_replace('$$UNSUBSCRIBE$$',$unsubscribeurl,$bodytext);

    // import opt-out trumps all other things
    if (Importoptout::is_optout_email($candidateEmail)) {
      $output .=' skipped ';
      continue;
    }

    try{
        file_put_contents('C:/Windows/Temp/ResufloLog/UserMailerCaller.txt', PHP_EOL.(new DateTime())->format('d m Y H:i:s').'===== START sfdripqueue_to_smtp', FILE_APPEND);
        $email = new UserMailer($userid);
        $r = $email->SendEmail(array($candidateEmail), null, $subject, $bodytext);
        file_put_contents('C:/Windows/Temp/ResufloLog/UserMailerCaller.txt', '=> END ====='.$r, FILE_APPEND);
    }
    catch (Exception $e) {
        file_put_contents('C:/Windows/Temp/ResufloLog/UserMailerCaller.txt', (new DateTime())->format('d m Y H:i:s').'sfdripqueue_to_smtp ====='.$e->getMessage().PHP_EOL, FILE_APPEND);
    }

    if ($r === true) {
      $UpdateNotesQuery = "UPDATE candidate SET activity_notes = '".$subject."' WHERE candidate_id = '".$candidate_id."'";
      mysql_query($UpdateNotesQuery);
      $output .= ' sent:'.$r."\n";

      $query = "UPDATE drip_queue SET sent = 1, smtp_id = '', smtp_error = '', date_modified = now() WHERE id = '".$row['drip_queue_id']."'";
      mysql_query($query);
      
    } else {
      $error = $r;
      $output .= ' ERROR:'.$error."\n\n";

      $error = mysql_real_escape_string($error);
      $query = "UPDATE drip_queue SET sent = 0, smtp_id = '', smtp_error = '$error', date_modified = now() WHERE id = '".$row['drip_queue_id']."'";
      mysql_query($query);
    }
  }
  catch (phpmailerException $e) {
    $output .= ' PHPMAILER ERROR:'.$e->errorMessage()."\n";
  }catch (Exception $e) {
    $output .= ' GENERAL ERROR:'.$e->getMessage()."\n";
  }
}

$error = ob_get_contents();
ob_end_clean();
$error = trim($error);
if (!empty($error)) {
  $output .= "\n\nERRORS AND OTHER OUTPUT: $error";
}

if (!empty($output)) {
  if (!mail($email_debug, 'jj-dripqueue-to-smtp: other '.$dtime, $output)) {
     echo 'ERROR SENDING drip OUTPUT: '.$output;
  }
}
