<?php

// only allow this to be run from the command line
if (!empty($_SERVER['HTTP_HOST'])) die();
/* https://resuflocrm.com/index.php?m=candidates&list=new_candidates&a=show&candidateID=3167484&view=1
 * Clicking on "Event Schedule" will add event to remind and thank you message
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
$dir = str_replace('\que\\'.basename(__FILE__), '', __FILE__);

include "$dir\asset\config.php";
include_once("$dir\lib\Importoptout.php");

include_once('../lib/phpmailer6/vendor/autoload.php');
include_once('../lib/UserMailer.php');

$emails_per_five_minutes = '250';
//$email_debug = 'jay@toptal.com,jeff@recruiters-edge.com';
$email_debug = 'malikabiid@gmail.com';

$sql = '
select 
	ce.calendar_event_id, ce.title, ce.venue, ce.person_to_meet, ce.description, DATE_FORMAT(ce.date, "%m-%d-%Y %h:%i:%s %p") date, ce.date_created, ce.date_modified, ce.data_item_id, ce.entered_by,
	ce.candidate_thanks_sent, ce.candidate_thanks_days, ce.candidate_thanks_message,
        cet.short_description,
	c.candidate_id, c.email1, c.first_name c_fname, c.last_name c_lname, c.middle_name c_mname, c.phone_home,
	u.user_id, u.site_id, u.user_name, u.first_name,
	dcfg.fromemail, dcfg.bccemail, dcfg.smtpuser, dcfg.smtphost, dcfg.email_type,
	dcfg.smtppassword, dcfg.smtpport, dcfg.fromname,
	dcfg.fromemail2, dcfg.bccemail2, dcfg.smtpuser2, dcfg.smtphost2, dcfg.email_type2,
	dcfg.smtppassword2, dcfg.smtpport2, dcfg.fromname2,dcfg.toggle_smtp
from
calendar_event ce left join
calendar_event_type cet on ce.type = cet.calendar_event_type_id left join
candidate c on ce.data_item_id = c.candidate_id left join
user u on ce.entered_by = u.user_id left join
timezone on u.timezone = timezone.name left join
dripmarket_configuration dcfg on u.user_id=dcfg.userid
where 
ce.candidate_thanks_sent  = 0 and candidate_thanks_days <> 0 and c.unsubscribe = 0 and 
date_add(convert_tz(ce.date, timezone.value, "-06:00"), interval ce.candidate_thanks_days day) < now()';
//echo $sql.'<br><br>';

$resourcedetail= mysql_query($sql);
$output = '';
while($row = mysql_fetch_array($resourcedetail)) {
    $calendarEventId = $row['calendar_event_id'];
    $scheduledDate = $row['date'];
    $user_name = $row['user_name'];
    $candidateName = $row['c_fname']." ".$row['c_mname']." ".$row['c_lname'];
    $candidateEmailExploding = explode(",",$row['email1']);
    $candidateEmail = $candidateEmailExploding[0];

    if (empty($candidateEmail)) continue;

    if (empty($output)) '***Drip (default campaigns) Report for '.date('Y-m-d H:i')."***\n\n";

    $subject = 'Thanks';
    $bodytext=stripslashes($row['candidate_thanks_message']);

    $candidate_id = $row['candidate_id'];
    $userid = $row['user_id'];
    $siteID = $row['site_id'];
    
    //Insert Activity
    $sqlInsertActivity = "INSERT INTO activity (data_item_id, data_item_type, joborder_id, entered_by, type, notes, site_id, date_created, date_modified )VALUES ( '$candidate_id', '100', '0', '$userid', '200', '$subject', '$siteID', NOW(), NOW() )";
    //mysql_query($sqlInsertActivity);

    try{
        $datefunc=date("d-M-y");
        
        $bodytext=str_replace('%DATETIME%',$datefunc,$bodytext);
        $bodytext=str_replace('%CANDFIRSTNAME%',$row['c_fname'],$bodytext);
        $bodytext=str_replace('%CANDFULLNAME%',$candidateName,$bodytext);
        $bodytext=str_replace('%USERFULLNAME%',$user_name,$bodytext);
        
        $bodytext=str_replace('%TITLE%',$row['title'],$bodytext);
        $bodytext=str_replace('%DESC%',$row['description'],$bodytext);
        $bodytext=str_replace('%EVENTTYPE%',$row['short_description'],$bodytext);
        $bodytext=str_replace('%VENUE%',$row['venue'],$bodytext);
        $bodytext=str_replace('%TOMEET%',$row['person_to_meet'],$bodytext);
        $bodytext=str_replace('%DATETIMEEVENT%',$row['date'],$bodytext);
        
        // import opt-out trumps all other things
        if (Importoptout::is_optout_email($candidateEmail)) {
            //$query = "UPDATE drip_queue SET sent = 2, smtp_id = '', smtp_error = '', date_modified = now() WHERE id = '".$row['drip_queue_id']."'";
            //mysql_query($query);
            $output .= 'candidate ('.$row['candidate_id'].') email: '.$candidateEmail.', from: '.$row['fromemail'].' ('.$row['fromname'].') ';
            $output .=' optout '.PHP_EOL;
            continue;
        }

        try{
            file_put_contents('C:/Windows/Temp/ResufloLog/UserMailerCaller.txt', PHP_EOL.(new DateTime())->format('d m Y H:i:s').'===== START candidate_event_thanks', FILE_APPEND);
            $email = new UserMailer($userid);
            $r = $email->SendEmail(array($candidateEmail), null, $subject, $bodytext);
            file_put_contents('C:/Windows/Temp/ResufloLog/UserMailerCaller.txt', '=> END ====='.$r, FILE_APPEND);
        }
        catch (Exception $e) {
            file_put_contents('C:/Windows/Temp/ResufloLog/UserMailerCaller.txt', (new DateTime())->format('d m Y H:i:s').'candidate_event_thanks ====='.$e->getMessage().PHP_EOL, FILE_APPEND);
        }

        if ($r === true) 
        {
            mysql_query($sqlInsertActivity);
            $UpdateCalendarEventQuery = "UPDATE calendar_event SET candidate_thanks_sent = 1, date_modified = Now() WHERE calendar_event_id = '".$calendarEventId."'";
            mysql_query($UpdateCalendarEventQuery);
            $output .= 'candidate ('.$row['candidate_id'].') email: '.$candidateEmail.', from: '.$row['fromemail'].' ('.$row['fromname'].') ';
            $output .= ' sent:'.$r.PHP_EOL;
        } else 
        {
            $output .= 'candidate ('.$row['candidate_id'].') email: '.$candidateEmail.', from: '.$row['fromemail'].' ('.$row['fromname'].') ';
            $error = $r;
            $output .= ' ERROR:'.$error.PHP_EOL;
            $error = mysql_real_escape_string($error);
        }
    }
    catch (phpmailerException $e) {
        $output .= ' PHPMAILER ERROR:'.$e->errorMessage()."\n";
    }catch (Exception $e) {
        $output .= ' GENERAL ERROR:'.$e->getMessage()."\n";
    }
}

$error = ob_get_contents();
ob_end_clean();
$error = trim($error);
if (!empty($error)) {
  //$output .= "\n\nERRORS AND OTHER OUTPUT: $error";
}

if (!empty($output)) {
	$date = new DateTime();
	$candidateEventThanksLogPath = str_replace('\que\\'.basename(__FILE__), '\tmp\\'.$date->format('d m Y').'CandidateEventThanksLog.txt', __FILE__);
	$candidateEventThanksLogPath = '../../log/CandidateEventThanksLog '.$date->format('d m Y').'.txt';
	file_put_contents($candidateEventThanksLogPath, PHP_EOL.'===================================================       START '.$date->format('d m Y H:i:s').'     =================================================='.PHP_EOL, FILE_APPEND);
	file_put_contents($candidateEventThanksLogPath, $output, FILE_APPEND);
	file_put_contents($candidateEventThanksLogPath, PHP_EOL.'===================================================                   END               =================================================='.PHP_EOL, FILE_APPEND);
	//if (!mail($email_debug, 'jj-dripqueue-to-smtp: other '.$dtime, $output)) {
		//echo 'ERROR SENDING drip OUTPUT: '.$output;
	//}
}

