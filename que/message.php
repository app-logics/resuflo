<!DOCTYPE html>

<html lang="en" xmlns="http://www.w3.org/1999/xhtml">
    <head>
        <meta charset="utf-8" />
        <title></title>
        <link href="./google calender/main.css" rel="stylesheet" type="text/css" />
        <style>
            body{
                background-color: #E9EBEC;
                background: #E9EBEC;
            }
            .txtbox_small{
                width: 150px;
                margin-right: 2px;
            }
            .hide{
                display: none;
            }
            #main{
                border: none;
                background: none;
                padding-top: 0px;
            }
            #contents{
                padding: 0px;
            }
        </style>
    </head>
     
    <div id="main" style="min-height: auto; margin-top: 5px;">
        <div id="contents" style="padding-top: 10px; min-height: auto;">
            <table class="fifthPage" width="950" border="0" align="center" cellpadding="0" cellspacing="0">
                <tr>
                    <td colspan="2" style="padding-bottom: 10px;">
                        <div id="headerBlock">
                            <table cellspacing="0" cellpadding="0" style="margin: 0px; padding: 0px; float: left;">
                                <tbody>
                                    <tr>
                                        <td rowspan="2"><img src="../<?php echo $_GET['logo']; ?>" border="0" alt="RESUFLO" style="max-width: 200px;"></td>
                                    </tr>
                                </tbody>
                            </table>
                        </div>
                    </td>
                </tr>
                                
                <tr>
                    <td style="vertical-align: top; width: 200px;">
                        <div style="padding: 8px; background-color: #C4DCE6; width: 200px;">
                            <span style="font-weight: bolder;">INSTRUCTIONS</span><br>
                                Please follow these simple steps.<br><br>
                                        <table>
                                            <tr>
                                                <td>
                                                    <img src="../images/career_page/tick.png" alt=""/>
                                                </td>
                                                <td>
                                                    <strong>Contact Information</strong>
                                                </td>
                                            </tr>
                                            <?php if($_GET['status'] != 6){?>
                                            <tr>
                                                <td style="vertical-align: top;">
                                                    <img src="../images/career_page/tick.png" alt=""/>
                                                </td>
                                                <td>
                                                    <strong>One Quick Question</strong><br>
                                                        <span style="">Answer this quick question to tell us about yourself.</span>
                                                </td>
                                            </tr>
                                            <?php } ?>
                                            <?php if($_GET['viewDate'] == 'yes'){?>
                                            <tr>
                                                <td style="vertical-align: top;">
                                                    <img src="../images/career_page/tick.png" alt=""/>
                                                </td>
                                                <td>
                                                    <strong>Pick a Date</strong><br>
                                                        <span style="">Choose a date to meet with our representatives.</span>
                                                </td>
                                            </tr>
                                            <?php } ?>
                                        </table>
                        </div>
                    </td>
                    <td style="padding-left: 10px; vertical-align: top;">
                        <table style="border-collapse: collapse;">
                            <?php if($_GET['status'] == 0){?>
                                <tr>
                                    <td colspan="2">
                                        <strong>
                                            Success!
                                        </strong>
                                    </td>
                                </tr>
                                <tr>
                                    <td colspan="2">
                                        <span>
                                            Your form has been submitted.  You will receive a confirmation email.  Please check inbox and spam folder as well.
                                        </span>
                                    </td>
                                </tr>
                            <?php } elseif ($_GET['status'] == 1) {?>
                                <tr>
                                    <td colspan="2">
                                        <strong>
                                            <!--Fail!-->
                                        </strong>
                                    </td>
                                </tr>
                                <tr>
                                    <td colspan="2">
                                        <span>
                                            Your email is not valid or you have left a required field blank. Try again.
                                        </span>
                                    </td>
                                </tr>
                            <?php } elseif ($_GET['status'] == 2) {?>
                                <tr>
                                    <td colspan="2">
                                        <strong>
                                            <!--Fail!-->
                                        </strong>
                                    </td>
                                </tr>
                                <tr>
                                    <td colspan="2">
                                        <span>
                                            You have already scheduled an interview.
                                        </span>
                                    </td>
                                </tr>
                            <?php } elseif ($_GET['status'] == 3) {?>
                                <tr>
                                    <td colspan="2">
                                        <strong>
                                            <!--Fail!-->
                                        </strong>
                                    </td>
                                </tr>
                                <tr>
                                    <td colspan="2">
                                        <span>
                                            You must be a USA citizen to qualify for this position.
                                        </span>
                                    </td>
                                </tr>
                            <?php } elseif ($_GET['status'] == 4) {?>
                                <tr>
                                    <td colspan="2">
                                        <strong>
                                            <!--Fail!-->
                                        </strong>
                                    </td>
                                </tr>
                                <tr>
                                    <td colspan="2">
                                        <span>
                                            Your email is in Opt-Out list.
                                        </span>
                                    </td>
                                </tr>
                            <?php } elseif ($_GET['status'] == 5) {?>
                                <tr>
                                    <td colspan="2">
                                        <strong>
                                            <!--Fail!-->
                                        </strong>
                                    </td>
                                </tr>
                                <tr>
                                    <td colspan="2">
                                        <span>
                                            Error! Candidate could not added.
                                        </span>
                                    </td>
                                </tr>
                            <?php } elseif ($_GET['status'] == 6) {?>
                                <tr>
                                    <td colspan="2">
                                        <strong>
                                            <!--Fail!-->
                                        </strong>
                                    </td>
                                </tr>
                                <tr>
                                    <td colspan="2">
                                        <span>
                                            Your interview cancelled successfully
                                        </span>
                                    </td>
                                </tr>
                            <?php } ?>
                        </table>
                    </td>
                </tr>
            </table>
        </div>
    </div>
    <br>
    <?php if ($_GET['uid']== 1585){ ?>
        <div style=" background: #bcbebf url('../images/career_page/pattern-03.png') repeat scroll 0 0; position: absolute; left: 0; width: 100%; height: 183px; ">
            <div class="container">
                <center>
                    <div class="footer-columns" style="background: url('../images/career_page/25p.png');width: 950px; text-align: left;">
                        <!--<img src="../../images/career_page/unnamed.png" alt=""/>-->
                            <div class="row-fluid footer-div equal" style="background: url('../images/career_page/footer-divider.png') repeat-y 50% 0; height: 183px;">

                            <div style="height: 183px;padding-left: 70px;padding-bottom: 14px;width: 405px;display: inline-block;float: left;" class="span6 footer-contact">
                                <div class="footer-logo" style="padding: 28px 0 25px;">
                                    <img src="../images/career_page/Logo-Reversed-Small.png" id="ctl00_FooterUC_ctl00_SiteFooterLogo_1" class="SiteFooterLogo" alt="logo" style="width: 228px;">
                                </div>
                                <address class="footer-address">
                                    <p>
                                        <span class="address1">134 Rumford Ave</span> <span class="address2">Suite 201</span><span class="address-comma">,</span> <span class="city">Newton,</span> <span class="state">MA</span> <span class="zip">02466</span><br>
                                        <span class="footer-phone">Phone <a href="tel:+1617 9165155" style="color: #808080;">(617) 916-5155</a> |</span>

                                        <span class="footer-email"><a href="mailto:jbreedlove@moodystreet.com" style="color: #808080;">jbreedlove@moodystreet.com</a> </span>

                                    </p>
                                </address>
                            </div>

                            <div style="height: 183px;width: 455px;display: inline-block;float: right;padding-left: 20px;padding-bottom: 14px;" class="span6 footer-bd-info">
                                <div class="row-fluid bd-logo" style="padding: 28px 30px 25px 0; float: right;">
                                    <a href="https://www.oneamerica.com/" title="OneAmerica" target="_blank"><img src="../images/career_page/logo-oneamerica.png" alt="OneAmerica" class="oa-logo"></a>
                                </div>
                                <div class="row-fluid" style="display: inline-block;">
                                    <div class="general-disclaimer">
                                        <p>The Moody Street Group, LLC is a general agency appointed with the insurance companies of OneAmerica<sup></sup>.</p>
                                    </div>
                                </div>
                            </div>

                        </div>
                </center>
            </div>
            <div style="box-shadow: inset 0px 6px 6px rgba(0,0,0,.43);">
                <br>
                <br>
            </div>
        </div>
<?php } ?>
<br>
</html>
                                                        
