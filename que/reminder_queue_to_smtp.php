<?php

// only allow this to be run from the command line
if (!empty($_SERVER['HTTP_HOST'])) die();
//$o = shell_exec('/var/www/vhosts/staging.resuflocrm.com/httpdocs/is_dripsmtp_on.sh');
//if ($o>1) exit();

$dtime = date('Y-m-d H:i:s');
ob_start();

$dir = str_replace('\que\\'.basename(__FILE__), '', __FILE__);

include "..\asset\config.php";
include_once("..\lib\Importoptout.php");

include_once('../lib/phpmailer6/vendor/autoload.php');
include_once('../lib/UserMailer.php');

$emails_per_five_minutes = '250';
//$email_debug = 'jay@toptal.com,jeff@recruiters-edge.com';
$email_debug = 'malikabiid@gmail.com';

$sql = '
select
  q.id reminder_queue_id,
  u.user_name, u.site_id, u.first_name as clientname,
  c.candidate_id, c.email1, c.first_name, c.last_name, c.middle_name, c.phone_home, c.campaign_date, c.schedule_date,
  ir.id dce_id, ir.subject, ir.creationdate, ir.hour, ir.min,
  ir.Message, ir.Timezone, ir.sendemailon,
  dcfg.fromemail, dcfg.bccemail, dcfg.smtpuser, dcfg.smtphost, dcfg.email_type,
  dcfg.smtppassword, dcfg.smtpport, dcfg.fromname
from
  reminder_queue q left join
  candidate c on q.candidate_id = c.candidate_id left join
  user u on u.user_id = q.entered_by left join
  interview_reminder ir on ir.id = q.interview_reminder_id left join
  dripmarket_configuration dcfg on u.user_id=dcfg.userid 
where
  q.sent = 0 and c.unsubscribe = 1
order by
  smtp_error, q.id
limit 0, '.$emails_per_five_minutes;
//echo $sql.'<br><br>';

$resourcedetail= mysql_query($sql);
$output = '';

while($row = mysql_fetch_array($resourcedetail)) {

  $user_name = $row['user_name'];
  $candidateName = $row['first_name']." ".$row['middle_name']." ".$row['last_name'];
  $candidateEmailExploding = explode(",",$row['email1']);
  $candidateEmail = $candidateEmailExploding[0];
  $cidencode=base64_encode($row['candidate_id']);

  if (empty($candidateEmail)) continue;

  if (empty($output)) '***Drip (default campaigns) Report for '.date('Y-m-d H:i')."***\n\n";

  $bodytext=stripslashes($row['Message']);

  $candidate_id = $row['candidate_id'];
  $userid = $row['userid'];
  $subject = $row['subject'];
  $siteID = $row['site_id'];
  //Insert Activity
  $sqlInsertActivity = "INSERT INTO activity (data_item_id, data_item_type, joborder_id, entered_by, type, notes, site_id, date_created, date_modified )VALUES ( '$candidate_id', '100', '0', '$userid', '200', '$subject', '$siteID', NOW(), NOW() )";
  //mysql_query($sqlInsertActivity);

  try{
    $datefunc=date("d-M-y");
    $bodytext=str_replace('$$NAME$$',$row['first_name'],$bodytext);
    $bodytext=str_replace('$$FIRSTNAME$$',$row['first_name'],$bodytext);
    $bodytext=str_replace('$$LASTNAME$$',$row['last_name'],$bodytext);

    $bodytext=str_replace('$$EMAIL$$',$row['email1'],$bodytext);
    $bodytext=str_replace('$$PHONE$$',$row['phone_home'],$bodytext);
	
    $scheduledate = new DateTime($row['date_created']);
    $scheduledate = $scheduledate->format('l, F j, Y g:i A');
    $bodytext=str_replace('$$STARTDATE$$',$scheduledate,$bodytext);
    
    $scheduledate = new DateTime($datefunc);
    $scheduledate = $scheduledate->format('l, F j, Y g:i A');
    $bodytext=str_replace('$$CURDATE$$',$scheduledate,$bodytext);
	
    $scheduledate = new DateTime($row['schedule_date']);
    $scheduledate = $scheduledate->format('l, F j, Y g:i A');
    $bodytext=str_replace('$$SchedDate$$',$scheduledate,$bodytext);

    // import opt-out trumps all other things
    if (Importoptout::is_optout_email($candidateEmail)) {
		$query = "UPDATE reminder_queue SET sent = 2, smtp_id = '', smtp_error = '', date_modified = now() WHERE id = '".$row['reminder_queue_id']."'";
		mysql_query($query);
		//$output .=' optout '.PHP_EOL;
		continue;
    }

    try{
        file_put_contents('C:/Windows/Temp/ResufloLog/UserMailerCaller.txt', PHP_EOL.(new DateTime())->format('d m Y H:i:s').'===== START reminder_queue_to_smtp', FILE_APPEND);
        $email = new UserMailer($userid);
        $r = $email->SendEmail(array($candidateEmail), null, $subject, $bodytext);
        file_put_contents('C:/Windows/Temp/ResufloLog/UserMailerCaller.txt', '=> END ====='.$r, FILE_APPEND);
    }
    catch (Exception $e) {
        file_put_contents('C:/Windows/Temp/ResufloLog/UserMailerCaller.txt', (new DateTime())->format('d m Y H:i:s').'reminder_queue_to_smtp ====='.$e->getMessage().PHP_EOL, FILE_APPEND);
    }

    if ($r === true) {
	  mysql_query($sqlInsertActivity);
      $UpdateNotesQuery = "UPDATE candidate SET activity_notes = '".$subject."' WHERE candidate_id = '".$candidate_id."'";
      mysql_query($UpdateNotesQuery);
      //$output .= ' sent:'.$r.PHP_EOL;

      $query = "UPDATE reminder_queue SET sent = 1, smtp_id = '', smtp_error = '', date_modified = now() WHERE id = '".$row['reminder_queue_id']."'";
      mysql_query($query);
      
    } else {
		$output .= 'candidate ('.$row['candidate_id'].') email: '.$candidateEmail.', from: '.$row['fromemail'].' ('.$row['fromname'].') ';
		$error = $r;
		$output .= ' ERROR:'.$error.PHP_EOL;

      $error = mysql_real_escape_string($error);
      $query = "UPDATE reminder_queue SET sent = 3, smtp_id = '', smtp_error = '$error', date_modified = now() WHERE id = '".$row['reminder_queue_id']."'";
      mysql_query($query);
    }
  }
  catch (phpmailerException $e) {
    //$output .= ' PHPMAILER ERROR:'.$e->errorMessage()."\n";
  }catch (Exception $e) {
    //$output .= ' GENERAL ERROR:'.$e->getMessage()."\n";
  }
}

$error = ob_get_contents();
ob_end_clean();
$error = trim($error);
if (!empty($error)) {
  //$output .= "\n\nERRORS AND OTHER OUTPUT: $error";
}

if (!empty($output)) {
	$date = new DateTime();
	$dripLogPath = str_replace('\que\\'.basename(__FILE__), '\tmp\\'.$date->format('d m Y').'ReminderLog.txt', __FILE__);
	$dripLogPath = 'C:\log\\ReminderLog '.$date->format('d m Y').'.txt';
	file_put_contents($dripLogPath, PHP_EOL.'===================================================       START '.$date->format('d m Y H:i:s').'     =================================================='.PHP_EOL, FILE_APPEND);
	file_put_contents($dripLogPath, $output, FILE_APPEND);
	file_put_contents($dripLogPath, PHP_EOL.'===================================================                   END               =================================================='.PHP_EOL, FILE_APPEND);
	//if (!mail($email_debug, 'jj-dripqueue-to-smtp: other '.$dtime, $output)) {
		//echo 'ERROR SENDING drip OUTPUT: '.$output;
	//}
}
