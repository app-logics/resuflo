<?php
die("no longer used!  now using drip_to_queue_other and dripqueue_to_smtp");
$dtime = date('Y-m-d H:i:s');
ob_start();
$dir = str_replace('que/'.basename(__FILE__), '', __FILE__).'/';
include "$dir/asset/config.php";
include_once("$dir/lib/Importoptout.php");
require_once("$dir/PHPMailer/class.phpmailer.php");

$email_debug = 'jay@toptal.com,jeff@recruiters-edge.com';

$sql = '
select
  date_format(date_add(convert_tz(concat(date_format(c.campaign_date, "%Y-%m-%d"), " ", lpad(dce.hour,"2","0"),":",lpad(dce.min,"2","0"), ":00"), replace(replace(dce.timezone, "GMT", ""), ".", ":"), "-6:0"), interval dce.sendemailon day), "%Y-%m-%d %H:%i:%m") as scheduledDate,
  cj.candidate_joborder_id,
  u.user_name, u.first_name as clientname,
  c.candidate_id, c.email1, c.first_name, c.last_name, c.middle_name, c.phone_home, c.campaign_date,
  dc.cname, dc.id, dc.userid,
  dce.subject, dce.creationdate, dce.hour, dce.min,
  dce.Message, dce.Timezone, dce.sendemailon,
  dcfg.fromemail, dcfg.bccemail, dcfg.smtpuser, dcfg.smtphost, dcfg.email_type,
  dcfg.smtppassword, dcfg.smtpport, dcfg.fromname,
  dq.id as quesid
from
  candidate c left join
  candidate_joborder cj on c.candidate_id = cj.candidate_id left join
  user u on u.user_id = c.entered_by left join
  dripmarket_compaign dc on u.user_id = dc.userid left join
  dripmarket_compaignemail dce on dc.id = dce.compaignid left join
  dripmarket_questionaire dq on dq.userid = dc.userid left join
  dripmarket_configuration dcfg on dc.userid=dcfg.userid
where
  cj.candidate_joborder_id is null and
  dce.sendemailon <> 0 and
  cast(date_format(date_add(convert_tz(concat(date_format(c.campaign_date, "%Y-%m-%d"), " ", lpad(dce.hour,"2","0"),":",lpad(dce.min,"2","0"), ":00"), replace(replace(dce.timezone, "GMT", ""), ".", ":"), "-6:0"), interval dce.sendemailon day), "%Y-%m-%d %H:%i") as datetime) between cast("'.date('Y-m-d H').':00:00" as datetime) and cast("'.date('Y-m-d H').':59:59" as datetime) and
  c.ishot = 0 and
  c.unsubscribe <> 1 and
  c.follow_up <> 1 and
  dc.default = 0 and
  c.campaign_id = dc.id and
  c.campaign_id <> "none"
order by
  c.candidate_id desc
';

$resourcedetail= mysql_query($sql);
$output = '';

while($row = mysql_fetch_array($resourcedetail)) {

  $scheduledDate = $row['scheduledDate'];
  $user_name = $row['user_name'];
  $candidateName = $row['first_name']." ".$row['middle_name']." ".$row['last_name'];
  $candidateEmailExploding = explode(",",$row['email1']);
  $candidateEmail = $candidateEmailExploding[0];
  $questionaireurl='<a href="'.questionaireurl.'?quesid='.$row['quesid'].'">Please click here</a>';
  $quesidencode=base64_encode($row['quesid']);
  $cidencode=base64_encode($row['candidate_id']);
  $unsubscribeurl='To remove your name from our candidate list <a href="'.unsubscribeurl.'?quesid='.$quesidencode.'&cid='.$cidencode.'">Please click here</a>';

  if (empty($candidateEmail)) continue;

  if (empty($output)) '***Drip (other campaigns) Report for '.date('Y-m-d H:i')."***\n\n";

  $mail= new PHPMailer();
  $mail->IsSMTP(); // telling the class to use SMTP
  $mail->SMTPAuth   = true;                  // enable SMTP authentication

  $mail->Host       = $row['smtphost'];
  $mail->Port       =  $row['smtpport'];
  $mail->Username   =  $row['smtpuser'];   // SMTP account username
  $mail->Password   = $row['smtppassword'];
  if($row['email_type']!='none') {
     $mail->SMTPSecure =$row['email_type'];
  }

  $mail->SetFrom( $row['fromemail'], $row['fromname']);
  if (!empty($row['bccemail'])) $mail->AddBcc( $row['bccemail'] );
  $mail->Subject    = $row['subject'];

  $bodytext=stripslashes($row['Message']);

  $candidate_id = $row['candidate_id'];
  $userid = $row['userid'];
  $subject = $row['subject'];
  //Insert Activity
  $sqlInsertActivity = "INSERT INTO activity (data_item_id, data_item_type, joborder_id, entered_by, type, notes, site_id, date_created, date_modified )VALUES ( '$candidate_id', '100', '0', '$userid', '200', '$subject (drip2)', '1', NOW(), NOW() )";
  mysql_query($sqlInsertActivity);

  $output .= 'candidate ('.$row['candidate_id'].') email: '.$candidateEmail.', from: '.$row['fromemail'].' ('.$row['fromname'].') ';

  try{
    $datefunc=date("d-M-y");
    $bodytext=str_replace('$$NAME$$',$row['first_name'],$bodytext);
    $bodytext=str_replace('$$EMAIL$$',$row['email1'],$bodytext);
    $bodytext=str_replace('$$PHONE$$',$row['phone_home'],$bodytext);
    $bodytext=str_replace('$$STARTDATE$$',$row['date_created'],$bodytext);
    $bodytext=str_replace('$$CURDATE$$',$datefunc,$bodytext);
    $bodytext=str_replace('$$QUESTIONNAIRE$$',$questionaireurl,$bodytext);
    $bodytext=str_replace('$$UNSUBSCRIBE$$',$unsubscribeurl,$bodytext);
    $mail->MsgHTML($bodytext);

    $mail->ClearAddresses();

    // import opt-out trumps all other things
    if (Importoptout::is_optout_email($candidateEmail)) {
      break;
    }

    $mail->AddAddress($candidateEmail, $candidateName);
    $r = $mail->Send();

    $output .= ' sent:'.$r."\n";

    $UpdateNotesQuery = "UPDATE candidate SET activity_notes = '".$subject."' WHERE candidate_id = '".$candidate_id."'";
    mysql_query($UpdateNotesQuery);
  }
  catch (phpmailerException $e) {
    $output .= ' PHPMAILER ERROR:'.$e->errorMessage()."\n";
  }catch (Exception $e) {
    $output .= ' GENERAL ERROR:'.$e->getMessage()."\n";
  }
}

$error = ob_get_contents();
ob_end_clean();
$error = trim($error);
if (!empty($error)) {
  $output .= "\n\nERRORS AND OTHER OUTPUT: $error";
}

if (!empty($output)) {
  if (!mail($email_debug, 'jjdrip: other campaigns '.$dtime, $output)) {
     echo 'ERROR SENDING drip OUTPUT: '.$output;
  }
}
