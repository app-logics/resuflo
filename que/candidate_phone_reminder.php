<?php

// only allow this to be run from the command line
if (!empty($_SERVER['HTTP_HOST'])) die();
//ini_set("display_errors", 0);

$dir = str_replace('\que\\'.basename(__FILE__), '', __FILE__);
// Require the bundled autoload file - the path may need to change
// based on where you downloaded and unzipped the SDK
require __DIR__ . '/twilio-php-master/Twilio/autoload.php';

// Use the REST API Client to make requests to the Twilio REST API
use Twilio\Rest\Client;

include "$dir\asset\config.php";

$emails_per_five_minutes = '250';
//$email_debug = 'jay@toptal.com,jeff@recruiters-edge.com';
$email_debug = 'malikabiid@gmail.com';

$sql = '
select 
	ce.calendar_event_id, ce.title, ce.venue, ce.person_to_meet, ce.description, DATE_FORMAT(ce.date, "%m-%d-%Y %h:%i:%s %p") date, ce.date_created, ce.date_modified, ce.data_item_id, ce.entered_by,
	ce.candidate_reminder_sent, ce.candidate_reminder_days, ce.candidate_reminder_message,
        cet.short_description,
	c.candidate_id, c.email1, c.first_name c_fname, c.last_name c_lname, c.middle_name c_mname, c.phone_home, c.phone_cell, c.phone_work,
	u.user_id, u.user_name, u.first_name, u.twillioSID, u.twillioToken, u.twillioFromNumber,
	dcfg.fromemail, dcfg.bccemail, dcfg.smtpuser, dcfg.smtphost, dcfg.email_type,
	dcfg.smtppassword, dcfg.smtpport, dcfg.fromname,
	dcfg.fromemail2, dcfg.bccemail2, dcfg.smtpuser2, dcfg.smtphost2, dcfg.email_type2,
	dcfg.smtppassword2, dcfg.smtpport2, dcfg.fromname2,dcfg.toggle_smtp
from
calendar_event ce left join
calendar_event_type cet on ce.type = cet.calendar_event_type_id left join
candidate c on ce.data_item_id = c.candidate_id left join
user u on ce.entered_by = u.user_id left join
timezone on u.timezone = timezone.name left join
dripmarket_configuration dcfg on u.user_id=dcfg.userid
where 
ce.candidate_phone_reminder_send  = 1 and candidate_phone_reminder_sent = 0 and
date_add(convert_tz(now(), timezone.value, "-06:00"), interval ce.candidate_reminder_days day) > ce.date';
//echo $sql.'<br><br>';

$resourcedetail= mysql_query($sql);
$output = '';
$manualToggle = 0;
while($row = mysql_fetch_array($resourcedetail)) {
    $calendarEventId = $row['calendar_event_id'];
    $scheduledDate = $row['date'];
    $user_name = $row['user_name'];
    $candidateName = $row['c_fname']." ".$row['c_mname']." ".$row['c_lname'];
    $candidatePhone = $row['phone_cell'];
    
    if (empty($candidatePhone)){
        $candidatePhone = $row['phone_work'];
    };
    if (empty($candidatePhone)) continue;

    if (empty($output)) '***Drip (default campaigns) Report for '.date('Y-m-d H:i')."***\n\n";

    $subject = 'Reminder';
    $bodytext=stripslashes($row['candidate_reminder_message']);

    $candidate_id = $row['candidate_id'];
    $userid = $row['user_id'];
    
    try{
        $datefunc=date("M-d-y");
        
        $bodytext=str_replace('%DATETIME%',$datefunc,$bodytext);
        $bodytext=str_replace('%CANDFIRSTNAME%',$row['c_fname'],$bodytext);
        $bodytext=str_replace('%CANDFULLNAME%',$candidateName,$bodytext);
        $bodytext=str_replace('%USERFULLNAME%',$user_name,$bodytext);
        
        $bodytext=str_replace('%TITLE%',$row['title'],$bodytext);
        $bodytext=str_replace('%DESC%',$row['description'],$bodytext);
        $bodytext=str_replace('%EVENTTYPE%',$row['short_description'],$bodytext);
        $bodytext=str_replace('%VENUE%',$row['venue'],$bodytext);
        $bodytext=str_replace('%TOMEET%',$row['person_to_meet'],$bodytext);
        $bodytext=str_replace('%DATETIMEEVENT%',$row['date'],$bodytext);
        
        // Your Account SID and Auth Token from twilio.com/console
        $sid = $row['twillioSID'];
        $token = $row['twillioToken'];
        $client = new Client($sid, $token);

        // Use the client to do fun stuff like send text messages!
        $result = $client->messages->create(
            // the number you'd like to send the message to
            $candidatePhone,
            array(
                // A Twilio phone number you purchased at twilio.com/console
                'from' => $row['twillioFromNumber'],
                // the body of the text message you'd like to send
                'body' => $bodytext
            )
        );
        if(empty($result->errorCode)){
            $sqlMessageQuery = "UPDATE calendar_event set candidate_phone_reminder_sent = 1 where calendar_event_id=$calendarEventId";
            mysql_query($sqlMessageQuery);
        }
        else{
            
        }
        
    }catch (Exception $e) {
        $output .= ' Text Message ERROR:'.$e->getMessage()."\n";
        echo $output;
    }
}

if (!empty($output)) {
	$date = new DateTime();
	$candidatePhoneReminderLogPath = str_replace('\que\\'.basename(__FILE__), '\tmp\\'.$date->format('d m Y').'CandidatePhoneReminderLog.txt', __FILE__);
	$candidatePhoneReminderLogPath = '../../log/CandidatePhoneReminderLog '.$date->format('d m Y').'.txt';
	file_put_contents($candidatePhoneReminderLogPath, PHP_EOL.'===================================================       START '.$date->format('d m Y H:i:s').'     =================================================='.PHP_EOL, FILE_APPEND);
	file_put_contents($candidatePhoneReminderLogPath, $output, FILE_APPEND);
	file_put_contents($candidatePhoneReminderLogPath, PHP_EOL.'===================================================                   END               =================================================='.PHP_EOL, FILE_APPEND);
}