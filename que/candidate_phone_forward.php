<?php

// only allow this to be run from the command line
if (!empty($_SERVER['HTTP_HOST'])) die();
//ini_set("display_errors", 0);

$dir = str_replace('\que\\'.basename(__FILE__), '', __FILE__);
// Require the bundled autoload file - the path may need to change
// based on where you downloaded and unzipped the SDK
require __DIR__ . '/twilio-php-master/Twilio/autoload.php';

// Use the REST API Client to make requests to the Twilio REST API
use Twilio\Rest\Client;


include "$dir\asset\config.php";
include_once("$dir\lib\Importoptout.php");
include_once('../lib/phpmailer6/vendor/autoload.php');
include_once('../lib/UserMailer.php');
//goto forward;

$emails_per_five_minutes = '250';
//$email_debug = 'jay@toptal.com,jeff@recruiters-edge.com';
$email_debug = 'malikabiid@gmail.com';

$sql = '
select 
	u.user_name, u.first_name, u.twillioSID, u.twillioToken, u.twillioFromNumber,
	dcfg.fromemail, dcfg.bccemail, dcfg.smtpuser, dcfg.smtphost, dcfg.email_type,
	dcfg.smtppassword, dcfg.smtpport, dcfg.fromname,
	dcfg.fromemail2, dcfg.bccemail2, dcfg.smtpuser2, dcfg.smtphost2, dcfg.email_type2,
	dcfg.smtppassword2, dcfg.smtpport2, dcfg.fromname2,dcfg.toggle_smtp
from
user u left join
dripmarket_configuration dcfg on u.user_id=dcfg.userid
where 
u.twillioSID is not null and u.twillioToken is not null and u.twillioFromNumber is not null';
//echo $sql.'<br><br>';

$resourcedetail= mysql_query($sql);

$output = '';
$manualToggle = 0;
while($row = mysql_fetch_array($resourcedetail)) {
    $sid = $row['twillioSID'];
    $token = $row['twillioToken'];
    $phone = $row['twillioFromNumber'];
    
    $client = new Client($sid, $token);
    $messages = $client->messages->read(array('DateSentAfter' => date("Y-m-d").'T00:00:00-06:00',));
    // Loop over the list of messages and echo a property for each one
    foreach ($messages as $message) {
        echo "Done";
        if($message->direction == "inbound"){
            $result = mysql_query("select count(*) as rowCount from  twilio_message where sid='$message->sid'");
            $row = mysql_fetch_assoc($result);
            if($row['rowCount'] == 0){
                $twilioInsert = "INSERT INTO `twilio_message` (`sid`, `type`, `from`, `to`, `body`, `date_created`) VALUES ('$message->sid', '$message->direction', '$message->from', '$message->to', '$message->body', NOW())";
                echo $twilioInsert.'<br>';
                mysql_query($twilioInsert);
            }
        }
    }
}
//forward:
    $sql = '
    select 
        t.message_id, t.body reply, t.from fromNumber, t.to toNumber,
        u.user_id, u.user_name, u.first_name, u.email, u.twillioSID, u.twillioToken, u.twillioFromNumber,
        dcfg.fromemail, dcfg.bccemail, dcfg.smtpuser, dcfg.smtphost, dcfg.email_type,
        dcfg.smtppassword, dcfg.smtpport, dcfg.fromname,
        dcfg.fromemail2, dcfg.bccemail2, dcfg.smtpuser2, dcfg.smtphost2, dcfg.email_type2,
        dcfg.smtppassword2, dcfg.smtpport2, dcfg.fromname2,dcfg.toggle_smtp
    from
        twilio_message t left join 
        user u on t.to = u.twillioFromNumber left join
        dripmarket_configuration dcfg on u.user_id=dcfg.userid
    where 
        t.result is null';
    //echo $sql.'<br><br>';

    $resourcedetail= mysql_query($sql);

    $output = '';
    $manualToggle = 0;
    while($row = mysql_fetch_array($resourcedetail)){
        $user_name = $row['user_name'];
    
        $subject = 'Message Reply';
        $bodytext=stripslashes($row['reply']);
        $userid = $row['user_id'];
        $messageId = $row['message_id'];
        $fromNumber = $row['fromNumber'];
        $toNumber = $row['toNumber'];
        $forwardEmail = $row['email'];
        
        $bodytext = "From: ".$fromNumber."<br>To: ".$toNumber."<br>Message: ".$bodytext;

    try{
        try{
            file_put_contents('C:/Windows/Temp/ResufloLog/UserMailerCaller.txt', PHP_EOL.(new DateTime())->format('d m Y H:i:s').'===== START candidate_phone_forward', FILE_APPEND);
            $email = new UserMailer($userid);
            $r = $email->SendEmail(array($forwardEmail), null, $subject, $bodytext);
            file_put_contents('C:/Windows/Temp/ResufloLog/UserMailerCaller.txt', '=> END ====='.$r, FILE_APPEND);
        }
        catch (Exception $e) {
            file_put_contents('C:/Windows/Temp/ResufloLog/UserMailerCaller.txt', (new DateTime())->format('d m Y H:i:s').'candidate_phone_forward ====='.$e->getMessage().PHP_EOL, FILE_APPEND);
        }

        if ($r === true) 
        {
            $query = 'update `twilio_message` set result="success", date_updated=Now() where message_id = '.$messageId;
            mysql_query($query);
        } else 
        {
            $error = $r;
            $output .= ' ERROR:'.$error.PHP_EOL;
            $error = mysql_real_escape_string($error);
        }
    }
    catch (phpmailerException $e) {
        $output .= ' PHPMAILER ERROR:'.$e->errorMessage()."\n";
    }catch (Exception $e) {
        $output .= ' GENERAL ERROR:'.$e->getMessage()."\n";
    }
}

if (!empty($output)) {
    $date = new DateTime();
    $candidateEventReminderLogPath = str_replace('\que\\'.basename(__FILE__), '\tmp\\'.$date->format('d m Y').'CandidatePhoneForwardLog.txt', __FILE__);
    $candidateEventReminderLogPath = '../../log/CandidatePhoneForwardLog '.$date->format('d m Y').'.txt';
    file_put_contents($candidateEventReminderLogPath, PHP_EOL.'===================================================       START '.$date->format('d m Y H:i:s').'     =================================================='.PHP_EOL, FILE_APPEND);
    file_put_contents($candidateEventReminderLogPath, $output, FILE_APPEND);
    file_put_contents($candidateEventReminderLogPath, PHP_EOL.'===================================================                   END               =================================================='.PHP_EOL, FILE_APPEND);
    //if (!mail($email_debug, 'jj-dripqueue-to-smtp: other '.$dtime, $output)) {
            //echo 'ERROR SENDING drip OUTPUT: '.$output;
    //}
}