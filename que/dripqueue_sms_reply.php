<?php
//Allo to run from command line only
//if (!empty($_SERVER['HTTP_HOST'])) die();
//$o = shell_exec('/var/www/vhosts/staging.resuflocrm.com/httpdocs/is_dripsmtp_on.sh');
//if ($o>1) exit();
	
$dtime = date('Y-m-d H:i:s');
ob_start();

$dir = str_replace('\que\\'.basename(__FILE__), '', __FILE__);
include_once("$dir\lib\Encryption.php");
include_once("$dir\asset\config.php");
include_once("$dir\asset\constants.php");
include_once("$dir\lib\Importoptout.php");
include_once("$dir\lib\UserMailer.php");


use PHPMailer\PHPMailer\PHPMailer;
include_once('../lib/phpmailer6/vendor/autoload.php');

// based on where you downloaded and unzipped the SDK
require __DIR__ . '/twilio-php-master/Twilio/autoload.php';

// Use the REST API Client to make requests to the Twilio REST API
use Twilio\Rest\Client;

error_reporting(E_ALL);
ini_set('display_errors', 1);

$emails_per_five_minutes = '250';
//$email_debug = 'jay@toptal.com,jeff@recruiters-edge.com';
$email_debug = 'malikabiid@gmail.com';
$SiteId = $_GET['SiteId'];
$db = DatabaseConnection::getInstance($SiteId);
	
$MessageSid = trim($_GET['MessageSid']);
if(empty($MessageSid)){
    goto log;
}
$SmsSid = trim($_GET['SmsSid']);
$AccountSid = trim($_GET['AccountSid']);
$MessagingServiceSid = trim($_GET['MessagingServiceSid']);
//Candidate number
$From = $_GET['From'];
//Client twilio number
$To = trim($_GET['To']);
$Body = trim($_GET['Body']);
$NumMedia = trim($_GET['NumMedia']);
$userQuery = 'select user_id, email from user where twillioFromNumber like \'%'.substr($To, -8).'%\'';
$userResult = $db->getAllAssoc($userQuery);
$UserId = $userResult[0]['user_id'];
$Email = $userResult[0]['email'];
$candidateQuery = "SELECT candidate_id, first_name, last_name FROM candidate where entered_by =".$UserId." AND phone_cell like '%".substr($From, -10)."'";
$candidateResult = $db->getAllAssoc($candidateQuery);
$queryResult = false;
if(sizeof($candidateResult) != 0){
    $candidateId = $candidateResult[0]['candidate_id'];
    $sql = sprintf(
                "INSERT INTO drip_sms_reply (
                    CandidateId,
                    UserId,
                    MessageSid,
                    SmsSid,
                    AccountSid,
                    MessagingServiceSid,
                    `From`,
                    `To`,
                    Body,
                    NumMedia,
                    Direction
                )
                VALUES (
                    %s,
                    %s,
                    %s,
                    %s,
                    %s,
                    %s,
                    %s,
                    %s,
                    %s,
                    %s,
                    2
                )",
                $db->makeQueryInteger(trim($candidateId)),
                $db->makeQueryInteger(trim($UserId)),
                $db->makeQueryString(trim($MessageSid)),
                $db->makeQueryString(trim($SmsSid)),
                $db->makeQueryString(trim($AccountSid)),
                $db->makeQueryString(trim($MessagingServiceSid)),
                $db->makeQueryString(trim($From)),
                $db->makeQueryString(trim($To)),
                $db->makeQueryString(trim($Body)),
                $db->makeQueryInteger(trim($NumMedia))
            );
//    echo $sql;
    $queryResult = $db->query($sql);
    if($queryResult == true){
        if (strcasecmp($Body, 'STOP') == 0){
            $unsubQuery ="UPDATE `candidate` SET unsubscribe=1,unsubscribe_date = NOW() WHERE candidate_id=".$candidateId;
            $db->query($unsubQuery);
        }
        else{
            $candidateSmsPage =BASEURL."index.php?m=candidates&amp;a=sms&amp;candidateID=".$candidateId;
            $encry = new Encryption();
            $Body = 'Reply From: '.$candidateResult[0]['first_name'].' '.$candidateResult[0]['last_name'].'<br/><br/>'.$Body.'<br/><br/>'.'<a href='.$candidateSmsPage.'>Click here to view message on resuflo</a><br/><br/> To remove your name from our candidate list <a href="'.$unsubscribeurl.'?cid='.urlencode($encry->safe_encrypt($candidateId)).'&siteId='.urlencode($encry->safe_encrypt($SiteId)).'">Please click here</a>'; 

            $dripConfig = $db->getAllAssoc("select * from dripmarket_configuration where userid = ".$UserId);
            try{
                file_put_contents('C:\windows\\Temp\\UserMailerCaller.txt', PHP_EOL.(new DateTime())->format('d m Y H:i:s').'===== START sms_reply', FILE_APPEND);
                $mailer = new UserMailer($UserId, $SiteId);
                $emailResult = $mailer->SendEmail(array($Email), null, 'Notification - Text Message Reply', $Body);
                file_put_contents('C:\windows\\Temp\\UserMailerCaller.txt', '=> END ====='.$emailResult, FILE_APPEND);
            }
            catch (Exception $e) {
                file_put_contents('C:\windows\\Temp\\UserMailerCaller.txt', (new DateTime())->format('d m Y H:i:s').'sms_reply ====='.$e->getMessage().PHP_EOL, FILE_APPEND);
            }
            echo "Record added";
        }
    }
}
//If candidate not found or record couldn't saved to db
if($queryResult == false){
    log:
    file_put_contents('C:\windows\\Temp\\DripQueue_Sms_Reply.txt', date('Y-m-d H:i:s').PHP_EOL, FILE_APPEND);
    file_put_contents('C:\windows\\Temp\\DripQueue_Sms_Reply.txt', 'MessageSid:'.$_GET['MessageSid'].PHP_EOL, FILE_APPEND);
    file_put_contents('C:\windows\\Temp\\DripQueue_Sms_Reply.txt', 'SmsSid:'.$_GET['SmsSid'].PHP_EOL, FILE_APPEND);
    file_put_contents('C:\windows\\Temp\\DripQueue_Sms_Reply.txt', 'AccountSid:'.$_GET['AccountSid'].PHP_EOL, FILE_APPEND);
    file_put_contents('C:\windows\\Temp\\DripQueue_Sms_Reply.txt', 'MessagingServiceSid:'.$_GET['MessagingServiceSid'].PHP_EOL, FILE_APPEND);
    file_put_contents('C:\windows\\Temp\\DripQueue_Sms_Reply.txt', 'From:'.$_GET['From'].PHP_EOL, FILE_APPEND);
    file_put_contents('C:\windows\\Temp\\DripQueue_Sms_Reply.txt', 'To:'.$_GET['To'].PHP_EOL, FILE_APPEND);
    file_put_contents('C:\windows\\Temp\\DripQueue_Sms_Reply.txt', 'Body:'.$_GET['Body'].PHP_EOL, FILE_APPEND);
    file_put_contents('C:\windows\\Temp\\DripQueue_Sms_Reply.txt', 'NumMedia:'.$_GET['NumMedia'].PHP_EOL, FILE_APPEND);
    file_put_contents('C:\windows\\Temp\\DripQueue_Sms_Reply.txt', '================='.PHP_EOL, FILE_APPEND);
    echo "Record failed";
}