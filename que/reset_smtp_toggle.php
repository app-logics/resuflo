<?php
//Author: App Logics
//Detail: Need to run this daily so that smtp setting can be reset

// only allow this to be run from the command line
if (!empty($_SERVER['HTTP_HOST'])) die();
include "../asset/config.php";

//reset smtp to primary
$query = 'update `dripmarket_configuration` set toggle_smtp=0 where toggle_smtp=1';
mysql_query($query);

$query = 'update drip_queue set sent = 0 where sent = 3 and date_created > curdate()';
mysql_query($query);

?>

