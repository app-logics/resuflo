<?php

require_once('../asset/constants.php');
include "../asset/config.php";
$emails_per_five_minutes = '250';
//$email_debug = 'jay@toptal.com,jeff@recruiters-edge.com';

//$sql = "select
//  u.user_id, u.user_name, u.email, u.first_name,
//  c.candidate_id, c.email1, c.first_name, c.last_name, c.phone_cell, c.schedule_date,
//  dcfg.fromemail, dcfg.bccemail, dcfg.smtpuser, dcfg.smtphost, dcfg.email_type, dcfg.smtppassword, dcfg.smtpport, dcfg.fromname
//from
//  candidate c left join
//  user u on u.user_id = c.entered_by left join
//  dripmarket_configuration dcfg on u.user_id=dcfg.userid 
//where
//  c.source = 'Career Page' AND
//  (c.sent is null OR c.sent = 0) AND
//  c.schedule_date < DATE_ADD(now(), INTERVAL 1 DAY)
//limit ".$emails_per_five_minutes;

$sql = "select
  u.user_id,
  c.schedule_date as scheduledDate,
  c.candidate_id,
  ir.id,
  ir.hour, ir.min, ir.timezone, ir.sendemailon
from
  candidate c left join
  user u on u.user_id = c.entered_by left join
  job j on u.user_id = j.entered_by left join
  interview_reminder ir on j.job_id = ir.jobid
where
  u.access_level > 0 and
  ir.sendemailon <> 0 and
  cast(date_format(date_sub(convert_tz(c.schedule_date, replace(replace(ir.timezone, 'GMT', ''), '.', ':'), '-6:0'), interval ir.sendemailon day), '%Y-%m-%d %H:%i') as datetime) between cast(concat(date_format(now(), '%Y-%m-%d %H'), ':00:00') as datetime) and cast(concat(date_format(now(), '%Y-%m-%d %H'), ':59:59') as datetime) and
  c.ishot = 1 and
  c.unsubscribe = 1 and
  c.follow_up <> 1 and
  c.schedule_date is not null and
  c.schedule_date <> ''
order by
  c.candidate_id desc
  limit ".$emails_per_five_minutes;
//echo $sql.'<br><br>';
$resourcedetail= mysql_query($sql);
while($row = mysql_fetch_array($resourcedetail)) {
    //echo $row['candidate_id'];
    $scheduledDate = $row['scheduledDate'];
    $cand_id = $row['candidate_id'];
    $ir_id = $row['id'];
    $user_id = $row['user_id'];

    //Insert Activity
    $sql = 'insert into reminder_queue set scheduledDate = "'.$scheduledDate.'", candidate_id = "'.$cand_id.'", interview_reminder_id = "'.$ir_id.'", entered_by = "'.$user_id.'", date_created = now(), date_modified = now()';
    mysql_query($sql);
}
