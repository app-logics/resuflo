<?php
//Allo to run from command line only
//if (!empty($_SERVER['HTTP_HOST'])) die();
//$o = shell_exec('/var/www/vhosts/staging.resuflocrm.com/httpdocs/is_dripsmtp_on.sh');
//if ($o>1) exit();
$dtime = date('Y-m-d H:i:s');
ob_start();

$dir = str_replace('\que\\'.basename(__FILE__), '', __FILE__);

include "$dir\asset\config.php";
include_once("$dir\lib\Importoptout.php");

use PHPMailer\PHPMailer\PHPMailer;
include_once('../lib/phpmailer6/vendor/autoload.php');
include_once('../lib/UserMailer.php');
include_once('../lib/Users.php');
include_once('../lib/SmsLog.php');

// based on where you downloaded and unzipped the SDK
require __DIR__ . '/twilio-php-master/Twilio/autoload.php';

// Use the REST API Client to make requests to the Twilio REST API
use Twilio\Rest\Client;

error_reporting(E_ALL);
ini_set('display_errors', 1);

$emails_per_five_minutes = '250';
//$email_debug = 'jay@toptal.com,jeff@recruiters-edge.com';
$email_debug = 'malikabiid@gmail.com';

$sql = '
select
  q.id drip_queue_id,
  q.scheduledDate,
  cj.candidate_joborder_id,
  u.user_name, u.site_id, u.first_name as clientname, u.signature, u.twillioSID, u.twillioToken, u.twillioFromNumber,
  c.candidate_id, c.site_id, c.email1, c.first_name, c.last_name, c.middle_name, c.phone_cell, c.source, c.campaign_date,
  dc.cname, dc.id, dc.userid, dc.default,
  dce.id dce_id, dce.nameofemail, dce.subject, dce.creationdate, dce.hour, dce.min, dce.isTextMessage,
  dce.Message, dce.Timezone, dce.sendemailon,
  dcfg.fromemail, dcfg.bccemail, dcfg.smtpuser, dcfg.smtphost, dcfg.email_type,
  dcfg.smtppassword, dcfg.smtpport, dcfg.fromname,
  dcfg.fromemail2, dcfg.bccemail2, dcfg.smtpuser2, dcfg.smtphost2, dcfg.email_type2,
  dcfg.smtppassword2, dcfg.smtpport2, dcfg.fromname2,dcfg.toggle_smtp,
  dq.id as quesid
from
  drip_queue q left join
  candidate c on q.candidate_id = c.candidate_id left join
  candidate_joborder cj on c.candidate_id = cj.candidate_id left join
  user u on u.user_id = q.entered_by left join
  dripmarket_compaign dc on dc.id = q.drip_campaign_id left join
  dripmarket_compaignemail dce on dce.id = q.drip_campemail_id left join
  dripmarket_questionaire dq on dq.userid = dc.userid left join
  dripmarket_configuration dcfg on dc.userid=dcfg.userid 
where
  u.isTextMessageEnable = 1 and q.sent = 0 and c.unsubscribe = 0 and dce.isTextMessage = 1 and q.scheduledDate < Now()
order by
  smtp_error, q.id
limit 0, '.$emails_per_five_minutes;
$resourcedetail= mysql_query($sql);
$output = '';
$manualToggle = 0;
$diffValues = "";
while($row = mysql_fetch_array($resourcedetail)) {
    // Send "SMS" if it is "TEXT MESSAGE" else "SEND EMAIL"
    if($row['isTextMessage'] == '1'){
        $date2=date_create($row['scheduledDate']);
        $diff=date_diff($date2, date_create($dtime));
        $dayDiff = $diff->format("%R%a");
        $diffValues = $diffValues.$row['scheduledDate'].' - '.$dtime.' = '.$dayDiff;
        //Ignore if email is older then 7 days
        if($dayDiff >7){
            $query = "UPDATE drip_queue SET sent = 4, smtp_id = '', smtp_error = 'IGNORE_OLD', date_modified = now() WHERE id = '".$row['drip_queue_id']."'";
            mysql_query($query);
            $diffValues = $diffValues.'Skipped<br/>';
            continue;
        }
        else{
            $diffValues = $diffValues.'<br/>';
        }

        // Your Account SID and Auth Token from twilio.com/console
        $sid = $row['twillioSID'];
        $token = $row['twillioToken'];
        $candidatePhone = $row['phone_cell'];
        $candidate_id = $row['candidate_id'];
        $userid = $row['userid'];
        $nameofemail = $row['nameofemail'];
        $subject = $row['subject'];
        $siteID = $row['site_id'];
        $client = new Client($sid, $token);
        $bodytext=stripslashes($row['Message']);
        $datefunc=date("d-M-y");
        $bodytext=str_replace('$$NAME$$',$row['first_name'],$bodytext);
        $bodytext=str_replace('$$FIRSTNAME$$',$row['first_name'],$bodytext);
        $bodytext=str_replace('$$LASTNAME$$',$row['last_name'],$bodytext);

        $bodytext=str_replace('$$EMAIL$$',$row['email1'],$bodytext);
        $bodytext=str_replace('$$PHONE$$',$row['phone_cell'],$bodytext);
        $bodytext=str_replace('$$CSOURCE$$',$row['source'],$bodytext);
//            $bodytext=str_replace('$$STARTDATE$$',$row['date_created'],$bodytext);
        $bodytext=str_replace('$$CURDATE$$',$datefunc,$bodytext);
//            $bodytext=str_replace('$$QUESTIONNAIRE$$',$quesUrl,$bodytext);
        $bodytext=str_replace('$$UNSUBSCRIBE$$','Reply with STOP to un-subscribe from service',$bodytext);
        $userMailer = new UserMailer($userid, $siteID);

        if(!empty($candidatePhone)){
            try{
                $result = $userMailer->SendSms($candidate_id, $candidatePhone, $bodytext);
                var_dump($result);
                if($result === true)
                {
                    $query = "UPDATE drip_queue SET sent = 1, smtp_id = '', smtp_error = '', date_modified = now() WHERE id = '".$row['drip_queue_id']."'";
                    mysql_query($query);

                }else{    
                    $query = "UPDATE drip_queue SET sent = 3, smtp_id = '', smtp_error = '".str_replace("'", "\'",$result)."', date_modified = now() WHERE id = '".$row['drip_queue_id']."'";
                    mysql_query($query);
                }
            }
            catch (Exception $e) {
                $query = "UPDATE drip_queue SET sent = 3, smtp_id = '', smtp_error = '".str_replace("'", "\'", $e->getMessage())."', date_modified = now() WHERE id = '".$row['drip_queue_id']."'";
                mysql_query($query);
            }
        }
        else{
            $query = "UPDATE drip_queue SET sent = 3, smtp_id = '', smtp_error = 'Cell phone not found', date_modified = now() WHERE id = '".$row['drip_queue_id']."'";
            mysql_query($query);
        }
    } 
}
//echo $diffValues;
$error = ob_get_contents();
ob_end_clean();
echo $diffValues;
$error = trim($error);
if (!empty($error)) {
  //$output .= "\n\nERRORS AND OTHER OUTPUT: $error";
}

if (!empty($output)) {
	$date = new DateTime();
	$dripLogPath = str_replace('\que\\'.basename(__FILE__), '\tmp\\'.$date->format('d m Y').'DripLog.txt', __FILE__);
	$dripLogPath = 'C:/Windows/Temp/ResufloLog/DripLog '.$date->format('d m Y').'.txt';
	file_put_contents($dripLogPath, PHP_EOL.'===================================================       START '.$date->format('d m Y H:i:s').'     =================================================='.PHP_EOL, FILE_APPEND);
	file_put_contents($dripLogPath, $output, FILE_APPEND);
	file_put_contents($dripLogPath, PHP_EOL.'===================================================                   END               =================================================='.PHP_EOL, FILE_APPEND);
	//if (!mail($email_debug, 'jj-dripqueue-to-smtp: other '.$dtime, $output)) {
		//echo 'ERROR SENDING drip OUTPUT: '.$output;
	//}
}
