<?php

// only allow this to be run from the command line
//if (!empty($_SERVER['HTTP_HOST'])) die();
/* https://resuflocrm.com/index.php?m=candidates&list=new_candidates&a=show&candidateID=3167484&view=1
 * Clicking on "Event Schedule" will add event to remind and thank you message
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
$dir = str_replace('\que\\'.basename(__FILE__), '', __FILE__);

include "$dir\asset\config.php";
include_once("$dir\lib\Importoptout.php");
include_once("$dir\lib\Encryption.php");

include_once('../lib/phpmailer6/vendor/autoload.php');
include_once('../lib/UserMailer.php');

$emails_per_five_minutes = '250';
//$email_debug = 'jay@toptal.com,jeff@recruiters-edge.com';
$email_debug = 'malikabiid@gmail.com';

$sql = "
SELECT  t.email_template_id, t.name, t.text, t.reminder_days, u.user_id, u.site_id, u.user_name, concat(u.first_name, ' ', u.last_name) UserName, 
        c.candidate_id,c.app_reminder_1,c.app_reminder_2,c.app_reminder_3,c.app_reminder_4,c.first_name, c.last_name, c.phone_cell, c.email1, c.ishot, c.date_created, c.schedule_date, c.sent,
        dc.fromemail, dc.bccemail, dc.smtpuser, dc.smtphost, dc.email_type,
        dc.smtppassword, dc.smtpport, dc.fromname,
        dc.fromemail2, dc.bccemail2, dc.smtpuser2, dc.smtphost2, dc.email_type2,
        dc.smtppassword2, dc.smtpport2, dc.fromname2,dc.toggle_smtp
FROM email_template t RIGHT JOIN
user u ON t.entered_by = u.user_id RIGHT JOIN
dripmarket_configuration dc on u.user_id=dc.userid RIGHT JOIN
candidate c ON c.entered_by = u.user_id
WHERE u.user_name = 'jbreedlove' AND tag = 'SCHEDULER_EMAIL_TEMPLATE_REMINDER' AND c.ishot = 1 AND c.schedule_date IS NOT NULL AND 
( (c.app_reminder_1 IS NULL OR c.app_reminder_1 != t.email_template_id) AND (c.app_reminder_2 IS NULL OR c.app_reminder_2 != t.email_template_id) AND (c.app_reminder_3 IS NULL OR c.app_reminder_3 != t.email_template_id) AND (c.app_reminder_4 IS NULL OR c.app_reminder_4 != t.email_template_id)) AND
c.schedule_date > now() AND (TIMESTAMPDIFF(MINUTE, c.schedule_date, date_add(now(), interval t.reminder_days day)) between 0 and 600) AND date_add(c.date_created, interval 10 day) > now()
ORDER BY c.candidate_id DESC;";
//echo $sql.'<br><br>';

$resourcedetail= mysql_query($sql);
$output = '';
while($row = mysql_fetch_array($resourcedetail)) {
    $CandidateId = $row['candidate_id'];
    $UserName = $row['UserName'];
    $FirstName = $row['first_name'];
    $LastName = $row['last_name'];
    $PhoneCell = $row['phone_cell'];
    $Email1 = $row['email1'];
    $ScheduleDate = $row['schedule_date'];
    
    if (empty($Email1)) continue;

    if (empty($output)) '***Drip (default campaigns) Report for '.date('Y-m-d H:i')."***\n\n";

    $subject = $row['name'];
    $bodytext=stripslashes($row['text']);

    $candidate_id = $row['candidate_id'];
    $userid = $row['user_id'];
    $siteID = $row['site_id'];
    //Insert Activity
    $sqlInsertActivity = "INSERT INTO activity (data_item_id, data_item_type, joborder_id, entered_by, type, notes, site_id, date_created, date_modified )VALUES ( '$candidate_id', '100', '0', '$userid', '200', '$subject', '$siteID', NOW(), NOW() )";
    //mysql_query($sqlInsertActivity);

    try{
        $datefunc=date("l F jS");
        $ScheduleDate = strtotime($ScheduleDate);
        $ScheduleDate = date('l F jS \a\t h:i A', $ScheduleDate);
        
        $encry = new Encryption();
        $cid_enc = $encry->safe_encrypt($candidate_id);
        $reSchedulerLink = "<a href='https://resuflocrm.com/index.php?a=rescheduler&appid=".$cid_enc."&cl=".$userid."&siteID=".$siteID."'>Click here to reschedule</a>";
        $cancelSchedulerLink = "<a href='https://resuflocrm.com/index.php?a=cancelinterview&appid=".$cid_enc."&cl=".$userid."&siteID=".$siteID."'>Click here to cancel interview</a>";
        
        $bodytext=str_replace('%DATETIME%',$datefunc,$bodytext);
        $bodytext=str_replace('%SCHEDULEDATETIME%',$ScheduleDate,$bodytext);
        $bodytext=str_replace('%CANDFIRSTNAME%',$FirstName,$bodytext);
        $bodytext=str_replace('%CANDFULLNAME%',$FirstName.' '.$LastName,$bodytext);
        $bodytext=str_replace('%CANDPHONE%',$PhoneCell.' '.$LastName,$bodytext);
        $bodytext=str_replace('%CANDEMAIL%',$Email1.' '.$LastName,$bodytext);
        $bodytext=str_replace('%USERFULLNAME%',$UserName,$bodytext);
        $bodytext=str_replace('%RESCHEDULE%',$reSchedulerLink,$bodytext);
        $bodytext=str_replace('%CANCEL%',$cancelSchedulerLink,$bodytext);
        
        // import opt-out trumps all other things
        if (Importoptout::is_optout_email($Email1)) {
            //$query = "UPDATE drip_queue SET sent = 2, smtp_id = '', smtp_error = '', date_modified = now() WHERE id = '".$row['drip_queue_id']."'";
            //mysql_query($query);
            $output .= 'candidate ('.$row['candidate_id'].') email: '.$Email1.', from: '.$row['fromemail'].' ('.$row['fromname'].') ';
            $output .=' optout '.PHP_EOL;
            continue;
        }

        try{
            file_put_contents('C:/Windows/Temp/ResufloLog/UserMailerCaller.txt', PHP_EOL.(new DateTime())->format('d m Y H:i:s').'===== START applicant_reminder', FILE_APPEND);
            $email = new UserMailer($userid);
            $r = $email->SendEmail(array($Email1), null, $subject, $bodytext);
            file_put_contents('C:/Windows/Temp/ResufloLog/UserMailerCaller.txt', '=> END ====='.$r, FILE_APPEND);
        }
        catch (Exception $e) {
            file_put_contents('C:/Windows/Temp/ResufloLog/UserMailerCaller.txt', (new DateTime())->format('d m Y H:i:s').'applicant_reminder ====='.$e->getMessage().PHP_EOL, FILE_APPEND);
        }

        if ($r === true) 
        {
            mysql_query($sqlInsertActivity);
            $UpdateCandidateQuery = "UPDATE candidate SET ";
            if(empty($row['app_reminder_1'])){
                $UpdateCandidateQuery = $UpdateCandidateQuery."app_reminder_1 = ".$row['email_template_id'].", app_reminder_1_sent=NOW() ";
            }
            else if(empty($row['app_reminder_2'])){
                $UpdateCandidateQuery = $UpdateCandidateQuery."app_reminder_2 = ".$row['email_template_id'].", app_reminder_2_sent=NOW() ";
            }
            else if(empty($row['app_reminder_3'])){
                $UpdateCandidateQuery = $UpdateCandidateQuery."app_reminder_3 = ".$row['email_template_id'].", app_reminder_3_sent=NOW() ";
            }
            else if(empty($row['app_reminder_4'])){
                $UpdateCandidateQuery = $UpdateCandidateQuery."app_reminder_4 = ".$row['email_template_id'].", app_reminder_4_sent=NOW() ";
            }
            $UpdateCandidateQuery = $UpdateCandidateQuery."WHERE candidate_id = $CandidateId";    
            mysql_query($UpdateCandidateQuery);
            
            $output .= 'candidate ('.$row['candidate_id'].') email: '.$Email1.', from: '.$row['fromemail'].' ('.$row['fromname'].') ';
            $output .= ' sent:'.$r.PHP_EOL;
        } 
        else 
        {
            $output .= 'candidate ('.$row['candidate_id'].') email: '.$Email1.', from: '.$row['fromemail'].' ('.$row['fromname'].') ';
            $error = $r;
            $output .= ' ERROR:'.$error.PHP_EOL;
            $error = mysql_real_escape_string($error);
        }
    }
    catch (phpmailerException $e) {
        $output .= ' PHPMAILER ERROR:'.$e->errorMessage()."\n";
    }catch (Exception $e) {
        $output .= ' GENERAL ERROR:'.$e->getMessage()."\n";
    }
}

$error = ob_get_contents();
ob_end_clean();
$error = trim($error);
if (!empty($error)) {
  //$output .= "\n\nERRORS AND OTHER OUTPUT: $error";
}

if (!empty($output)) {
	$date = new DateTime();
	$candidateEventReminderLogPath = str_replace('\que\\'.basename(__FILE__), '\tmp\\'.$date->format('d m Y').'ApplicantReminderLog.txt', __FILE__);
	$candidateEventReminderLogPath = '../../log/ApplicantReminderLog '.$date->format('d m Y').'.txt';
	file_put_contents($candidateEventReminderLogPath, PHP_EOL.'===================================================       START '.$date->format('d m Y H:i:s').'     =================================================='.PHP_EOL, FILE_APPEND);
	file_put_contents($candidateEventReminderLogPath, $output, FILE_APPEND);
	file_put_contents($candidateEventReminderLogPath, PHP_EOL.'===================================================                   END               =================================================='.PHP_EOL, FILE_APPEND);
	//if (!mail($email_debug, 'jj-dripqueue-to-smtp: other '.$dtime, $output)) {
		//echo 'ERROR SENDING drip OUTPUT: '.$output;
	//}
}

