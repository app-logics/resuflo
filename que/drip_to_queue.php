<?php

// only allow this to be run from the command line
if (!empty($_SERVER['HTTP_HOST'])) die();
ini_set('display_errors',1);
error_reporting(E_ALL);

$dtime = date('Y-m-d H:i:s');
$dir = str_replace('que\\'.basename(__FILE__), '', __FILE__);
include "$dir\asset\config.php";
include_once("$dir\lib\Importoptout.php");

//$email_debug = 'jay@toptal.com,jeff@recruiters-edge.com';

$hour = 'H';

$sql = '
select
  date_format(date_add(convert_tz(concat(date_format(c.campaign_date, "%Y-%m-%d"), " ", lpad(dce.hour,"2","0"),":",lpad(dce.min,"2","0"), ":00"), replace(replace(dce.timezone, "GMT", ""), ".", ":"), "-6:0"), interval dce.sendemailon day), "%Y-%m-%d %H:%i:%m") as scheduledDate,
  cj.candidate_joborder_id,
  u.user_name, u.first_name as clientname,
  c.candidate_id, c.email1, c.first_name, c.last_name, c.middle_name, c.phone_home, c.campaign_date,
  dc.cname, dc.id, dc.userid, dc.default,
  dce.id dce_id, dce.subject, dce.creationdate, dce.hour, dce.min,
  dce.Message, dce.Timezone, dce.sendemailon,
  dcfg.fromemail, dcfg.bccemail, dcfg.smtpuser, dcfg.smtphost, dcfg.email_type,
  dcfg.smtppassword, dcfg.smtpport, dcfg.fromname,
  dq.id as quesid
from
  candidate c left join
  candidate_joborder cj on c.candidate_id = cj.candidate_id left join
  user u on u.user_id = c.entered_by left join
  dripmarket_compaign dc on u.user_id = dc.userid left join
  dripmarket_compaignemail dce on dc.id = dce.compaignid left join
  dripmarket_questionaire dq on dq.userid = dc.userid left join
  dripmarket_configuration dcfg on dc.userid=dcfg.userid
where
  u.isDripEnable = 1 and
  u.access_level > 0 and
  cj.candidate_joborder_id is null and
  dce.sendemailon <> 0 and
  cast(date_format(date_add(convert_tz(concat(date_format(c.campaign_date, "%Y-%m-%d"), " ", lpad(dce.hour,"2","0"),":",lpad(dce.min,"2","0"), ":00"), replace(replace(dce.timezone, "GMT", ""), ".", ":"), "-6:0"), interval dce.sendemailon day), "%Y-%m-%d %H:%i") as datetime) between cast("'.date('Y-m-d '.$hour).':00:00" as datetime) and cast("'.date('Y-m-d '.$hour).':59:59" as datetime) and
  c.ishot = 0 and
  c.unsubscribe <> 1 and
  dc.default = 1 and
  (c.campaign_id = dc.id or c.campaign_id = 0) and
  c.campaign_id <> "none"
order by
  dc.default desc, c.candidate_id desc
';
//removed below line after resume distribution becuase campaign_id was not handled
//c.campaign_id = 0 and
//echo $sql.'<br><br>';
//die();

$resourcedetail= mysql_query($sql);
$output = '';

$date = new DateTime();
//$dripLogPath = str_replace('\que\\'.basename(__FILE__), '\tmp\\'.$date->format('d m Y').'DripToQueueLog.txt', __FILE__);
//Plesk use only reference path
$dripLogPath = '../../log/DripToQueueLog '.$date->format('d m Y').'.txt';
file_put_contents($dripLogPath, PHP_EOL.'===================================================       START '.$date->format('d m Y H:i:s').'     =================================================='.PHP_EOL, FILE_APPEND);
//die($dripLogPath);
while($row = mysql_fetch_array($resourcedetail)) {

  echo $row['candidate_id'];

  $scheduledDate = $row['scheduledDate'];
  $cand_id = $row['candidate_id'];
  $camp_id = $row['id'];
  $dcee_id = $row['dce_id'];
  $user_id = $row['userid'];
  $subject = $row['subject'];

  //Insert Activity
  $sql = 'insert into drip_queue set smtp_error="DripToQueue", scheduledDate = "'.$scheduledDate.'", candidate_id = "'.$cand_id.'", drip_campaign_id = "'.$camp_id.'", drip_campemail_id = "'.$dcee_id.'", entered_by = "'.$user_id.'", date_created = now(), date_modified = now()';
  
  file_put_contents($dripLogPath, $sql.PHP_EOL, FILE_APPEND);
  $result = mysql_query($sql);
    if ($result) {
        file_put_contents($dripLogPath, '#True'.PHP_EOL, FILE_APPEND);
    }
    else{
        file_put_contents($dripLogPath, '#'.mysql_error().PHP_EOL, FILE_APPEND);
    }
}
file_put_contents($dripLogPath, PHP_EOL.'===================================================                   END               =================================================='.PHP_EOL, FILE_APPEND);
