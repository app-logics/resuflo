<?php
//Allo to run from command line only
if (!empty($_SERVER['HTTP_HOST'])) die();
//$o = shell_exec('/var/www/vhosts/staging.resuflocrm.com/httpdocs/is_dripsmtp_on.sh');
//if ($o>1) exit();

$dtime = date('Y-m-d H:i:s');
ob_start();

$dir = str_replace('\que\\'.basename(__FILE__), '', __FILE__);

include_once("$dir\lib\Encryption.php");
include_once("$dir\asset\config.php");
include_once("$dir\lib\Importoptout.php");

include_once('../lib/phpmailer6/vendor/autoload.php');
include_once('../lib/UserMailer.php');

$emails_per_five_minutes = '250';
//$email_debug = 'jay@toptal.com,jeff@recruiters-edge.com';
$email_debug = 'malikabiid@gmail.com';

$sql = '
select
  q.id drip_queue_id,
  cj.candidate_joborder_id,
  u.user_name, u.site_id, u.first_name as clientname, u.signature,
  c.candidate_id, c.site_id, c.email1, c.first_name, c.last_name, c.middle_name, c.phone_home, c.source, c.campaign_date,
  dc.cname, dc.id, dc.userid, dc.default,
  dce.id dce_id, dce.subject, dce.creationdate, dce.hour, dce.min, dce.isTextMessage,
  dce.Message, dce.Timezone, dce.sendemailon,
  dcfg.fromemail, dcfg.bccemail, dcfg.smtpuser, dcfg.smtphost, dcfg.email_type,
  dcfg.smtppassword, dcfg.smtpport, dcfg.fromname,
  dcfg.fromemail2, dcfg.bccemail2, dcfg.smtpuser2, dcfg.smtphost2, dcfg.email_type2,
  dcfg.smtppassword2, dcfg.smtpport2, dcfg.fromname2,dcfg.toggle_smtp,
  dq.id as quesid
from
  drip_queue q left join
  candidate c on q.candidate_id = c.candidate_id left join
  candidate_joborder cj on c.candidate_id = cj.candidate_id left join
  user u on u.user_id = q.entered_by left join
  dripmarket_compaign dc on dc.id = q.drip_campaign_id left join
  dripmarket_compaignemail dce on dce.id = q.drip_campemail_id left join
  dripmarket_questionaire dq on dq.userid = dc.userid left join
  dripmarket_configuration dcfg on dc.userid=dcfg.userid 
where
  q.sent = 0 and c.unsubscribe = 0 and q.smtp_error = "Candidates" and (dce.isTextMessage is null || dce.isTextMessage = 0)
order by
  q.id
limit 0, '.$emails_per_five_minutes;
//echo $sql.'<br><br>';

$resourcedetail= mysql_query($sql);
$output = '';
$manualToggle = -1;
    while($row = mysql_fetch_array($resourcedetail)) {

        $user_name = $row['user_name'];
        $signature = $row['signature'];
        $candidateName = $row['first_name']." ".$row['middle_name']." ".$row['last_name'];
        $candidateEmailExploding = explode(",",$row['email1']);
        $candidateEmail = $candidateEmailExploding[0];
        $quesUrl='<a href="'.$questionaireurl.'?quesid='.$row['quesid'].'">Please click here</a>';
        $quesidencode=base64_encode($row['quesid']);
        $cidencode=base64_encode($row['candidate_id']);
        //$unsubscribeurl='To remove your name from our candidate list '.unsubscribeurl.'?cid='.$row['candidate_id'];
        //Remove this later if found no problem
        $encry = new Encryption();
        $unsubUrl='To remove your name from our candidate list <a href="'.$unsubscribeurl.'?cid='.urlencode($encry->safe_encrypt($row['candidate_id'])).'&siteId='.urlencode($encry->safe_encrypt($row['site_id'])).'">Please click here</a>';

        if (empty($candidateEmail)) continue;

        if (empty($output)) '***Drip (default campaigns) Report for '.date('Y-m-d H:i')."***\n\n";

        /*  End of SMTP settings  */
        $bodytext=stripslashes($row['Message']);

        $candidate_id = $row['candidate_id'];
        $userid = $row['userid'];
        $subject = $row['subject'];

        try{
            $datefunc=date("d-M-y");
            $bodytext=str_replace('$$NAME$$',$row['first_name'],$bodytext);
            $bodytext=str_replace('$$FIRSTNAME$$',$row['first_name'],$bodytext);
            $bodytext=str_replace('$$LASTNAME$$',$row['last_name'],$bodytext);
            $bodytext=str_replace('$$EMAIL$$',$row['email1'],$bodytext);
            $bodytext=str_replace('$$PHONE$$',$row['phone_home'],$bodytext);
            $bodytext=str_replace('$$CSOURCE$$',$row['source'],$bodytext);
//            $bodytext=str_replace('$$STARTDATE$$',$row['date_created'],$bodytext);
            $bodytext=str_replace('$$CURDATE$$',$datefunc,$bodytext);
            $bodytext=str_replace('$$QUESTIONNAIRE$$',$quesUrl,$bodytext);
            $bodytext=str_replace('$$UNSUBSCRIBE$$',$unsubUrl,$bodytext);
            $bodytext=$bodytext.'<br><br>'.$signature;


            $subject=str_replace('$$NAME$$',$row['first_name'],$subject);
            $subject=str_replace('$$FIRSTNAME$$',$row['first_name'],$subject);
            $subject=str_replace('$$LASTNAME$$',$row['last_name'],$subject);

            $subject=str_replace('$$EMAIL$$',$row['email1'],$subject);
            $subject=str_replace('$$PHONE$$',$row['phone_home'],$subject);
            $subject=str_replace('$$CSOURCE$$',$row['source'],$subject);
//            $subject=str_replace('$$STARTDATE$$',$row['date_created'],$subject);
            $subject=str_replace('$$CURDATE$$',$datefunc,$subject);
            $subject=str_replace('$$QUESTIONNAIRE$$',$quesUrl,$subject);
            $subject=str_replace('$$UNSUBSCRIBE$$',$unsubUrl,$subject);

            // import opt-out trumps all other things
            if (Importoptout::is_optout_email($candidateEmail)) {
                $query = "UPDATE drip_queue SET sent = 2, smtp_id = '', smtp_error = '', date_modified = now() WHERE id = '".$row['drip_queue_id']."'";
                mysql_query($query);
                //$output .=' optout '.PHP_EOL;
                continue;
            }
            try{
                file_put_contents('C:/Windows/Temp/ResufloLog/UserMailerCaller.txt', PHP_EOL.(new DateTime())->format('d m Y H:i:s').'===== START first_dripque_to_smtp', FILE_APPEND);
                $email = new UserMailer($userid);
                $r = $email->SendEmail(array($candidateEmail), null, $subject, $bodytext);
                file_put_contents('C:/Windows/Temp/ResufloLog/UserMailerCaller.txt', '=> END ====='.$r, FILE_APPEND);
            }
            catch (Exception $e) {
                file_put_contents('C:/Windows/Temp/ResufloLog/UserMailerCaller.txt', (new DateTime())->format('d m Y H:i:s').'first_dripque_to_smtp ====='.$e->getMessage().PHP_EOL, FILE_APPEND);
            }

            if ($r === true) 
            {
                $siteID = $row['site_id'];
                //Insert Activity
                $sqlInsertActivity = "INSERT INTO activity (data_item_id, data_item_type, joborder_id, entered_by, type, notes, site_id, date_created, date_modified )VALUES ( '$candidate_id', '100', '0', '$userid', '200', '$subject', '$siteID', NOW(), NOW() )";
                mysql_query($sqlInsertActivity);

                $UpdateNotesQuery = "UPDATE candidate SET activity_notes = '".$subject."' WHERE candidate_id = '".$candidate_id."'";
                mysql_query($UpdateNotesQuery);
                //$output .= ' sent:'.$r.PHP_EOL;

                $query="select * from dripmarket_configuration where userid=".$userid;
                $SmtpRotationResult=mysql_query($query);
                $SmtpRotationRecord = mysql_fetch_array($SmtpRotationResult);
                $query = "UPDATE drip_queue SET sent = 1, smtp_id = '', smtp_error = '', smtp_rotation = '".$SmtpRotationRecord['toggle_smtp']."', date_modified = now() WHERE id = '".$row['drip_queue_id']."'";
                mysql_query($query);

            } else 
            {
                $output .= 'candidate ('.$row['candidate_id'].') email: '.$candidateEmail.', from: '.$row['fromemail'].' ('.$row['fromname'].') ';
                $error = $r;
                $output .= ' ERROR:'.$error.PHP_EOL;
                $error = mysql_real_escape_string($error);
                $query="select * from dripmarket_configuration where userid=".$userid;
                $SmtpRotationResult=mysql_query($query);
                $SmtpRotationRecord = mysql_fetch_array($SmtpRotationResult);
                $query = "UPDATE drip_queue SET sent = 3, smtp_id = '', smtp_error = '$error', smtp_rotation = '".$SmtpRotationRecord['toggle_smtp']."', date_modified = now() WHERE id = '".$row['drip_queue_id']."'";
                mysql_query($query);
            }
        }
        catch (phpmailerException $e) {
            $output .= ' PHPMAILER ERROR:'.$e->errorMessage()."\n";
        }catch (Exception $e) {
            $output .= ' GENERAL ERROR:'.$e->getMessage()."\n";
        }
        }   
$error = ob_get_contents();
ob_end_clean();
$error = trim($error);
if (!empty($error)) {
  //$output .= "\n\nERRORS AND OTHER OUTPUT: $error";
}

if (!empty($output)) {
	$date = new DateTime();
	$dripLogPath = str_replace('\que\\'.basename(__FILE__), '\tmp\\'.$date->format('d m Y').'DripLog.txt', __FILE__);
	$dripLogPath = 'C:/Windows/Temp/ResufloLog/DripLog '.$date->format('d m Y').'.txt';
	file_put_contents($dripLogPath, PHP_EOL.'===================================================       START '.$date->format('d m Y H:i:s').'     =================================================='.PHP_EOL, FILE_APPEND);
	file_put_contents($dripLogPath, $output, FILE_APPEND);
	file_put_contents($dripLogPath, PHP_EOL.'===================================================                   END               =================================================='.PHP_EOL, FILE_APPEND);
	//if (!mail($email_debug, 'jj-dripqueue-to-smtp: other '.$dtime, $output)) {
		//echo 'ERROR SENDING drip OUTPUT: '.$output;
	//}
}
