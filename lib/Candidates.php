<?php
/**
 * RESUFLO
 * Candidates Library
 *
 * Copyright (C) 2005 - 2007 Econn Technologies, Inc.
 *
 *
 * The contents of this file are subject to the RESUFLO Public License
 * Version 1.1a (the "License"); you may not use this file except in
 * compliance with the License. You may obtain a copy of the License at
 *
 * Software distributed under the License is distributed on an "AS IS"
 * basis, WITHOUT WARRANTY OF ANY KIND, either express or implied. See the
 * License for the specific language governing rights and limitations
 * under the License.
 *
 * The Original Code is "RESUFLO Standard Edition".
 *
 * The Initial Developer of the Original Code is Econn Technologies, Inc.
 * Portions created by the Initial Developer are Copyright (C) 2005 - 2007
 * (or from the year in which this file was created to the year 2007) by
 * Econn Technologies, Inc. All Rights Reserved.
 *
 *
 * @package    RESUFLO
 * @subpackage Library
 * @copyright Copyright (C) 2005 - 2007 Econn Technologies, Inc.
 * @version    $Id: Candidates.php 3813 2007-12-05 23:16:22Z brian $
 */

include_once('./lib/Attachments.php');
include_once('./lib/Pipelines.php');
include_once('./lib/History.php');
include_once('./lib/SavedLists.php');
include_once('./lib/ExtraFields.php');
include_once('./lib/Importoptout.php');
include_once('lib/DataGrid.php');

class Sheesh
{
    static function get_sheesh() {
        // TODO: find a way to make updating all NINE of these configurations more easily
        $default_campaign = 'Default Campaign';
        $userid = $_SESSION['RESUFLO']->getuserid();
        if (!empty($userid)) {
          $_db = DatabaseConnection::getInstance();
          $rs = $_db->getAssoc('select cname from dripmarket_compaign where `default`=1 and `userid`='.$userid);
          if (!empty($rs['cname'])) $default_campaign = $rs['cname'];
        }
        $GLOBALS['sheesh'] = array(
            'select' => 'case when candidate.campaign_id = \'none\' then \'None selected\' when candidate.campaign_id = \'0\' then \''.$default_campaign.'\' else dripmarket_compaign.cname end AS Campaign, dripmarket_compaign.id campaign_id',
            //'pagerRender' => 'return \'<a href=/index.php?m=dripMarket&a=compaigndetail&id=\'.$rsData[\'campaign_id\'].\'>\'.$rsData[\'Campaign\'].\'</a>\';',
            'pagerRender' => 'return $rsData[\'Campaign\'];',
            'join' => 'LEFT JOIN dripmarket_compaign ON candidate.campaign_id = dripmarket_compaign.id',
            'sortableColumn'     => 'Campaign',
            'alphaNavigation' => true,
            //'filter'         => 'Campaign'
           'filter'         => 'dripmarket_compaign.cname'
        );  
    }
}

/**
 *  Candidates Library
 *  @package    RESUFLO
 *  @subpackage Library
 */
class Candidates
{
    private $_db;
    private $_siteID;

    public $extraFields;


    public function __construct($siteID)
    {
        $this->_siteID = $siteID;
        $this->_db = DatabaseConnection::getInstance();
        $this->extraFields = new ExtraFields($siteID, DATA_ITEM_CANDIDATE);
    }

    public function addFilter($userId, $name, $params)
    {
        $sql = sprintf(
            "INSERT INTO filter (
                userId,
                name,
                params,
                site_id,
                date_created,
                date_modified
            )
            VALUES (
                %s,
                %s,
                %s,
                %s,
                NOW(),
                NOW()
            )",
            $this->_db->makeQueryInteger(trim($userId)),
            $this->_db->makeQueryString(trim($name)),
            $this->_db->makeQueryString(trim($params)),
            $this->_siteID
        );

        $queryResult = $this->_db->query($sql);
        if (!$queryResult)
        {
            return -1;
        }    
        return 1;
    }
    
    public function deleteFilter($filterid)
    {
        /* Delete the candidate from candidate. */
        $sql = sprintf(
            "DELETE FROM
                filter
            WHERE
                filterid = %s
            AND
                site_id = %s",
            $this->_db->makeQueryInteger($filterid),
            $this->_siteID
        );
        $queryResult = $this->_db->query($sql);
        if (!$queryResult)
        {
            return -1;
        }    
        return 1;
    }

    private function _is_duplicate_email($userid, $email, $phoneCell = NULL, $phoneWork = NULL) {
        $email = trim($email);
        if (strpos($email, '@')===false) return 0;
        //die("email: $email\n");
        $userid = (integer) $userid;

        // this is the part that got left behind in the legacy insertion...allowing the tlewis/cflores halloween
        $sql = 'select entered_by from candidate where "'.mysql_escape_string($email).'" in (email1, email2)';
        if(!empty($phoneCell)){
            $sql .= ' or phone_cell = "'.trim($phoneCell).'"';
        }
            
        $resourcedetail = $this->_db->query($sql);
        $entered_by_list = array();
        while ($ebl = mysql_fetch_array($resourcedetail)) {
            $entered_by_list[] = $ebl['entered_by'];
        }
        // there can be no dupes since this email isn't in the db under any user_id
        if (empty($entered_by_list)) return 0;

        // the user table entered_by is indicative of a master or sub account
        // the $entered_by that we are feeding to this is the target recipient of the resumes we're trying to parse
        // and the reason we do this is that j. edwards does not want the same company (master or subs) to get non-unique email/reusmes
        $sql = 'select entered_by from user where user_id = '.$userid;
        $users = $this->_db->getAllAssoc($sql);
        $user = $users[0];

        // find the master account (we may already have it in the target $entered_by)
        $master_id = ($user['entered_by']=='1') ? $userid : $user['entered_by'];

        // get all sub accounts of that master account
        $sql = 'select user_id from user where '.$master_id.' in (entered_by, user_id) and 1 <> "'.$master_id.'" union select s.user_id from user s join user m on s.entered_by = m.user_id where m.entered_by = '.$master_id;
        $users = $this->_db->getAllAssoc($sql);
        foreach ($users as &$user) {
            // see if any of our email candidate.entered_by interesect with all company user.entered_by => user_id's
            $entered_by_user[] = $user['user_id'];
        }
        
        //If master is under another master
        if($master_id != $userid){
            $sql = 'select entered_by from user where user_id = '.$master_id;
            //$sql = 'select entered_by from user where entered_by = '.$master_id;
            
            $users = $this->_db->getAllAssoc($sql);
            $user = $users[0];

            // find the master account (we may already have it in the target $entered_by)
            $master_id = ($user['entered_by']=='1') ? $master_id : $user['entered_by'];

            // get all sub accounts of that master account
            $sql = 'select user_id from user where '.$master_id.' in (entered_by, user_id) and 1 <> "'.$master_id.'"';
            $users = $this->_db->getAllAssoc($sql);
            foreach ($users as &$user) {
                // see if any of our email candidate.entered_by interesect with all company user.entered_by => user_id's
                $entered_by_user[] = $user['user_id'];
            }
        }
//        else{
//            //flush existing ones
//            $entered_by_user = array();
//            //check all master and accounts under master and sub masters
//            $sql = 'select user_id from user where entered_by = '.$master_id.' union select s.user_id from user s join user m on s.entered_by = m.user_id where m.entered_by = '.$master_id;
//            echo $sql.'<br>';
//            $users = $this->_db->getAllAssoc($sql);
//            foreach ($users as &$user) {
//                // see if any of our email candidate.entered_by interesect with all company user.entered_by => user_id's
//                $entered_by_user[] = $user['user_id'];
//            }
//        }
//        echo $master_id;
//        echo '<br>';
//        var_dump($entered_by_list);
//        echo '<br>';
//        var_dump($entered_by_user);
        $duped = array_intersect($entered_by_user, $entered_by_list);
//return 1;
        return (empty($duped)) ? 0 : 1;
    }


    /**
     * Adds a candidate to the database and returns its candidate ID.
     *
     * @param string First name.
     * @param string Middle name / initial.
     * @param string Last name.
     * @param string Primary e-mail address.
     * @param string Secondary e-mail address.
     * @param string Home phone number.
     * @param string Mobile phone number.
     * @param string Work phone number.
     * @param string Address (can be multiple lines).
     * @param string City.
     * @param string State / province.
     * @param string Postal code.
     * @param string Source where this candidate was found.
     * @param string Key skills.
     * @param string Date available.
     * @param string Current employer.
     * @param boolean Is this candidate willing to relocate?
     * @param string Current pay rate / salary.
     * @param string Desired pay rate / salary.
     * @param string Misc. candidate notes.
     * @param string Candidate's personal web site.
     * @param integer Entered-by user ID. THIS IS THE MASTER ACCOUNT - MASTER ACCOUNTS SEE ALL SUB ACCOUNTS!
     * @param integer Owner user ID. THIS IS THE SUB ACCOUNT!! IT SHOWS UP AS RECRUITER IN THE CANDIDATE LISTINGS!!!
     * @param string EEO gender, or '' to not specify.
     * @param string EEO gender, or '' to not specify.
     * @param string EEO veteran status, or '' to not specify.
     * @param string EEO disability status, or '' to not specify.
     * @param boolean Skip creating a history entry?
     * @return integer Candidate ID of new candidate, or -1 on failure.
     */
    public function add($firstName, $middleName, $lastName, $email1, $email2,
        $phoneHome, $phoneCell, $phoneWork, $address, $city, $state, $zip,
        $source, $keySkills, $dateAvailable, $currentEmployer, $canRelocate,
        $currentPay, $desiredPay, $notes, $webSite, $bestTimeToCall, $enteredBy, $owner,
        $gender = '', $race = '', $veteran = '', $disability = '',$lifeLic,$propLic,$ser6,$ser63,$ser7,$camp_id,
        $skipHistory = false)
    {

       // import opt-out trumps all other things
       if (Importoptout::is_optout_email($email1) || Importoptout::is_optout_email($email2)) {
           $this->is_optedout_email = 1;
           return -3;
       }
 
        //die("$firstName\n, $middleName\n, $lastName\n, $email1\n");
       //die("$firstName, $middleName, $lastName, $email1, $email2, $phoneHome, $phoneCell, $phoneWork, $address, $city, $state, $zip, $source, $keySkills, $dateAvailable, $currentEmployer, $canRelocate, $currentPay, $desiredPay, $notes, $webSite, $bestTimeToCall, $enteredBy, $owner, $gender, $race, $veteran, $disability,$lifeLic,$propLic,$ser6,$ser63,$ser7,$camp_id, $skipHistory");

       if (is_object($_SESSION['RESUFLO'])) $userid= $_SESSION['RESUFLO']->getuserid();   
       else $userid = $GLOBALS['user_id'];
       if (empty($userid)) $userid = $enteredBy;

       if ($this->_is_duplicate_email($enteredBy, $email1,$phoneCell, $phoneWork)) {
           $this->is_duplicate_email = 1;
           return -2;
       }
       if ($this->_is_duplicate_email($enteredBy, $email2,$phoneCell, $phoneWork)) {
           $this->is_duplicate_email = 1;
           return -2;
       }


         $sql = sprintf(
            "INSERT INTO candidate (
                first_name,
                middle_name,
                last_name,
                email1,
                email2,
                phone_home,
                phone_cell,
                phone_work,
                address,
                city,
                state,
                zip,
                source,
                key_skills,
                date_available,
                current_employer,
                can_relocate,
                current_pay,
                desired_pay,
                web_site,
                best_time_to_call,
                entered_by,
                is_hot,
                owner,
                site_id,
                date_created,
                date_modified,
                eeo_ethnic_type_id,
                eeo_veteran_type_id,
                eeo_disability_status,
                eeo_gender,
                Life_and_Health_License,
                Property_Casualty_License,
                Series_6,
                Series_63,
                Series_7,
                campaign_id,
                campaign_date
            )
            VALUES (
                %s,
                %s,
                %s,
                %s,
                %s,
                ExtractNumber(%s),
                ExtractNumber(%s),
                ExtractNumber(%s),
                %s,
                %s,
                %s,
                %s,
                %s,
                %s,
                %s,
                %s,
                %s,
                %s,
                %s,
                %s,
                %s,
                %s,
                0,
                %s,
                %s,
                NOW(),
                NOW(),
                %s,
                %s,
                %s,
                %s,
                %s,
                %s,
                %s,
                %s,
                %s,
                %s,
                NOW()
            )",
            $this->_db->makeQueryString(trim($firstName)),
            $this->_db->makeQueryString(trim($middleName)),
            $this->_db->makeQueryString(trim($lastName)),
            $this->_db->makeQueryString(trim($email1)),
            $this->_db->makeQueryString(trim($email2)),
            $this->_db->makeQueryString(trim($phoneHome)),
            $this->_db->makeQueryString(trim($phoneCell)),
            $this->_db->makeQueryString(trim($phoneWork)),
            $this->_db->makeQueryString(trim($address)),
            $this->_db->makeQueryString(trim($city)),
            $this->_db->makeQueryString(trim($state)),
            $this->_db->makeQueryString(trim($zip)),
            $this->_db->makeQueryString(trim($source)),
            $this->_db->makeQueryString(trim($keySkills)),
            $this->_db->makeQueryStringOrNULL($dateAvailable),
            $this->_db->makeQueryString($currentEmployer),
            ($canRelocate ? '1' : '0'),
            $this->_db->makeQueryString(trim($currentPay)),
            $this->_db->makeQueryString(trim($desiredPay)),
            $this->_db->makeQueryString(trim($webSite)),
            $this->_db->makeQueryString(trim($bestTimeToCall)),
            $this->_db->makeQueryInteger(trim($enteredBy)),
            $this->_db->makeQueryInteger(trim($owner)),
            $this->_siteID,
            $this->_db->makeQueryInteger(trim($race)),
            $this->_db->makeQueryInteger(trim($veteran)),
            $this->_db->makeQueryString(trim($disability)),
            $this->_db->makeQueryString(trim($gender)),

            $this->_db->makeQueryString(trim($lifeLic)),
            $this->_db->makeQueryString(trim($propLic)),
            $this->_db->makeQueryString(trim($ser6)),
            $this->_db->makeQueryString(trim($ser63)),
            $this->_db->makeQueryString(trim($ser7)),
            $this->_db->makeQueryString(trim($camp_id))

        );

    $queryResult = $this->_db->query($sql);
    //empty sql query string
    $sql = '';
    if (!$queryResult)
    {
        return -1;
    }
    $candidateID = $this->_db->getLastInsertID();
    //echo $candidateID;   
    if($camp_id=='0')
    { 
        $sql =  "SELECT 
                    candidate.candidate_id,dripmarket_compaign.id campaignId, dripmarket_compaignemail.id campaignEmailId, dripmarket_compaignemail.hour, dripmarket_compaignemail.min, dripmarket_compaign.userid 
                FROM 
                    `candidate` INNER JOIN 
                    user ON user.user_id =candidate.entered_by INNER JOIN 
                    dripmarket_compaign ON user.user_id = dripmarket_compaign.userid INNER JOIN 
                    dripmarket_compaignemail ON dripmarket_compaign.id = dripmarket_compaignemail.compaignid
                WHERE 
                    candidate.ishot=0 AND
                    candidate.unsubscribe=0 AND
                    candidate.candidate_id=$candidateID AND
                    dripmarket_compaign.`default`=1 AND
                    dripmarket_compaignemail.sendemailon=0";
    }   
    if(($camp_id!=0) && ($camp_id!='none'))
    {
        $sql =  "SELECT 
                    candidate.candidate_id,dripmarket_compaign.id campaignId, dripmarket_compaignemail.id campaignEmailId, dripmarket_compaignemail.hour, dripmarket_compaignemail.min, dripmarket_compaign.userid 
                FROM 
                    `candidate` INNER JOIN 
                    user ON user.user_id =candidate.entered_by INNER JOIN 
                    dripmarket_compaign ON user.user_id = dripmarket_compaign.userid INNER JOIN 
                    dripmarket_compaignemail ON dripmarket_compaign.id = dripmarket_compaignemail.compaignid LEFT JOIN 
                    dripmarket_questionaire ON dripmarket_questionaire.userid = dripmarket_compaign.userid INNER JOIN 
                    dripmarket_configuration ON dripmarket_compaign.userid=dripmarket_configuration.userid 
                WHERE 
                    candidate.ishot=0 AND
                    candidate.unsubscribe=0 AND 
                    candidate.candidate_id=$candidateID AND
                    dripmarket_compaign.`default`=0 AND
                    dripmarket_compaign.id= candidate.campaign_id AND
                    dripmarket_compaignemail.sendemailon=0";
    }
    //If campaign query is built
    if(!empty($sql)){
        $resourcedetail = $this->_db->query($sql);
        
        $date=date("y-m-d H:i:s");
        $scheduleDate = new DateTime();
        while($row = mysql_fetch_array($resourcedetail)){
            $dripLogPath = 'C:/Windows/Temp/ResufloLog/log/DripToQueueLog '.$date.'.txt';
            file_put_contents($dripLogPath, PHP_EOL.'===================================================       START '.$date.'     =================================================='.PHP_EOL, FILE_APPEND);
            $cand_id = $row['candidate_id'];
            $camp_id = $row['campaignId'];
            $dcee_id = $row['campaignEmailId'];
            $user_id = $row['userid'];
            
            $scheduleDate->setTime($row['hour'], $row['min']);
            //Insert drip_queue
            $sql = 'insert into drip_queue set smtp_error="Candidates", scheduledDate = "'.$scheduleDate->format('Y-m-d H:i:s').'", candidate_id = "'.$cand_id.'", drip_campaign_id = "'.$camp_id.'", drip_campemail_id = "'.$dcee_id.'", entered_by = "'.$user_id.'", date_created = now(), date_modified = now()';
            file_put_contents($dripLogPath, $sql.PHP_EOL, FILE_APPEND);
            $result = mysql_query($sql);
            if ($result) {
                file_put_contents($dripLogPath, '#True'.PHP_EOL, FILE_APPEND);
            }
            else{
                file_put_contents($dripLogPath, '#'.mysql_error().PHP_EOL, FILE_APPEND);
            }
            file_put_contents($dripLogPath, PHP_EOL.'===================================================                   END               =================================================='.PHP_EOL, FILE_APPEND);
        }
    }
        //Abid, don't know purpose so i disabled that
        if (!$skipHistory)
        {
            $history = new History($this->_siteID);
            $history->storeHistoryNew(DATA_ITEM_CANDIDATE, $candidateID);
        }
        return $candidateID;
    }
    
    /*
    *function to insert extra field data while uploading a zip
    */
    public function InsertExtraFields($xmlArr,$candidateId){
        $siteid = $this->_siteID;
        if($xmlArr['Category']!=''){
        $sql = "INSERT INTO extra_field (data_item_id,field_name,value,import_id,site_id,data_item_type) 
                values('".$candidateId."','Category','".$xmlArr['Category']."','0','".$siteid."','100')";
        $queryResult = $this->_db->query($sql);
        }
        if($xmlArr['DateOfBirth']!=''){
        $sql = "INSERT INTO extra_field (data_item_id,field_name,value,import_id,site_id,data_item_type) 
                values('".$candidateId."','Date Of Birth','".$xmlArr['DateOfBirth']."','0','".$siteid."','100')";
        $queryResult = $this->_db->query($sql);
        }
        if($xmlArr['Gender']!=''){
        $sql = "INSERT INTO extra_field (data_item_id,field_name,value,import_id,site_id,data_item_type) 
                values('".$candidateId."','Gender','".$xmlArr['Gender']."','0','".$siteid."','100')";
        $queryResult = $this->_db->query($sql);
        }
        if($xmlArr['FatherName']!=''){
        $sql = "INSERT INTO extra_field (data_item_id,field_name,value,import_id,site_id,data_item_type) 
                values('".$candidateId."','Father Name','".$xmlArr['FatherName']."','0','".$siteid."','100')";
        $queryResult = $this->_db->query($sql);
        }
        if($xmlArr['MotherName']!=''){
        $sql = "INSERT INTO extra_field (data_item_id,field_name,value,import_id,site_id,data_item_type) 
                values('".$candidateId."','Mother Name','".$xmlArr['MotherName']."','0','".$siteid."','100')";
        $queryResult = $this->_db->query($sql);
        }
        if($xmlArr['MaritalStatus']!=''){
        $sql = "INSERT INTO extra_field (data_item_id,field_name,value,import_id,site_id,data_item_type) 
                values('".$candidateId."','Marital Status','".$xmlArr['MaritalStatus']."','0','".$siteid."','100')";
        $queryResult = $this->_db->query($sql);
        }
        if($xmlArr['PassportNo']!=''){
        $sql = "INSERT INTO extra_field (data_item_id,field_name,value,import_id,site_id,data_item_type) 
                values('".$candidateId."','Passport No','".$xmlArr['PassportNo']."','0','".$siteid."','100')";
        $queryResult = $this->_db->query($sql);
        }
        if($xmlArr['Nationality']!=''){
        $sql = "INSERT INTO extra_field (data_item_id,field_name,value,import_id,site_id,data_item_type) 
                values('".$candidateId."','Nationality','".$xmlArr['Nationality']."','0','".$siteid."','100')";
        $queryResult = $this->_db->query($sql);
        }
        if($xmlArr['WorkedPeriod']!=''){
        $sql = "INSERT INTO extra_field (data_item_id,field_name,value,import_id,site_id,data_item_type) 
                values('".$candidateId."','Worked Period','".$xmlArr['WorkedPeriod']."','0','".$siteid."','100')";
        $queryResult = $this->_db->query($sql);
        }
        if($xmlArr['GapPeriod']!=''){
        $sql = "INSERT INTO extra_field (data_item_id,field_name,value,import_id,site_id,data_item_type) 
                values('".$candidateId."','Gap Period','".$xmlArr['GapPeriod']."','0','".$siteid."','100')";
        $queryResult = $this->_db->query($sql);
        }
        if($xmlArr['FaxNo']!=''){
        $sql = "INSERT INTO extra_field (data_item_id,field_name,value,import_id,site_id,data_item_type) 
                values('".$candidateId."','Fax No','".$xmlArr['FaxNo']."','0','".$siteid."','100')";
        $queryResult = $this->_db->query($sql);
        }
        if($xmlArr['LicenseNo']!=''){
        $sql = "INSERT INTO extra_field (data_item_id,field_name,value,import_id,site_id,data_item_type) 
                values('".$candidateId."','License No','".$xmlArr['LicenseNo']."','0','".$siteid."','100')";
        $queryResult = $this->_db->query($sql);
        }
        if($xmlArr['Hobbies']!=''){
        $sql = "INSERT INTO extra_field (data_item_id,field_name,value,import_id,site_id,data_item_type) 
                values('".$candidateId."','Hobbies','".$xmlArr['Hobbies']."','0','".$siteid."','100')";
        $queryResult = $this->_db->query($sql);
        }
        if($xmlArr['Qualification']!=''){
        $sql = "INSERT INTO extra_field (data_item_id,field_name,value,import_id,site_id,data_item_type) 
                values('".$candidateId."','Qualification','".$xmlArr['Qualification']."','0','".$siteid."','100')";
        $queryResult = $this->_db->query($sql);
        }
        if($xmlArr['Achievments']!=''){
        $sql = "INSERT INTO extra_field (data_item_id,field_name,value,import_id,site_id,data_item_type) 
                values('".$candidateId."','Achievements','".$xmlArr['Achievments']."','0','".$siteid."','100')";
        $queryResult = $this->_db->query($sql);
        }
        if($xmlArr['Objectives']!=''){
        $sql = "INSERT INTO extra_field (data_item_id,field_name,value,import_id,site_id,data_item_type) 
                values('".$candidateId."','Objectives','".$xmlArr['Objectives']."','0','".$siteid."','100')";
        $queryResult = $this->_db->query($sql);
        }
        if($xmlArr['Experience']!=''){
        $sql = "INSERT INTO extra_field (data_item_id,field_name,value,import_id,site_id,data_item_type) 
                values('".$candidateId."','Experience','".$xmlArr['Experience']."','0','".$siteid."','100')";
        $queryResult = $this->_db->query($sql);
        }
        if($xmlArr['References']!=''){
        $sql = "INSERT INTO extra_field (data_item_id,field_name,value,import_id,site_id,data_item_type) 
                values('".$candidateId."','References','".$xmlArr['References']."','0','".$siteid."','100')";
        $queryResult = $this->_db->query($sql);
        }
        if($xmlArr['JobProfile']!=''){
        $sql = "INSERT INTO extra_field (data_item_id,field_name,value,import_id,site_id,data_item_type) 
                values('".$candidateId."','Job Profile','".$xmlArr['JobProfile']."','0','".$siteid."','100')";
        $queryResult = $this->_db->query($sql);
        }
    }



        
    public function SaveZipRecordForProcessing($filename,$key,$clientid=''){
        $to = $_SESSION['RESUFLO']->getEmail();
        $siteid = $this->_siteID;
        $camp_id = $_REQUEST['camp_id'];
        if($clientid==0 || $clientid==''){
        $userid = $_SESSION['resumeinsertuserid'];
        }else{
        
        $userid = $clientid;
        }
       
        $path_info = pathinfo($filename);

        $filename_new = $path_info['basename'];
                
        $sql = "INSERT INTO processzip(filename,inqueue,emailalert,siteid,countrykey,userid,campaign_id) values('".$filename_new."','N','".$to."','".$siteid."','".$key."','".$userid."','".$camp_id."')";
        $queryResult = $this->_db->query($sql);
    }
    
    /**
     * Updates a candidate.
     *
     * @param integer Candidate ID to update.
     * @param string First name.
     * @param string Middle name / initial.
     * @param string Last name.
     * @param string Primary e-mail address.
     * @param string Secondary e-mail address.
     * @param string Home phone number.
     * @param string Mobile phone number.
     * @param string Work phone number.
     * @param string Address (can be multiple lines).
     * @param string City.
     * @param string State / province.
     * @param string Postal code.
     * @param string Source where this candidate was found.
     * @param string Key skills.
     * @param string Date available.
     * @param string Current employer.
     * @param boolean Is this candidate willing to relocate?
     * @param string Current pay rate / salary.
     * @param string Desired pay rate / salary.
     * @param string Misc. candidate notes.
     * @param string Candidate's personal web site.
     * @param integer Owner user ID.
     * @param string EEO gender, or '' to not specify.
     * @param string EEO gender, or '' to not specify.
     * @param string EEO veteran status, or '' to not specify.
     * @param string EEO disability status, or '' to not specify.
     * @return boolean True if successful; false otherwise.
     */
    public function update($candidateID, $isActive, $firstName, $middleName, $lastName,
        $email1, $email2, $phoneHome, $phoneCell, $phoneWork, $address,
        $city, $state, $zip, $source, $keySkills, $dateAvailable, $scheduledDate,
        $currentEmployer, $canRelocate, $currentPay, $desiredPay,
        $notes, $webSite, $bestTimeToCall, $enteredBy,$owner, $isHot, $email, $emailAddress,
        $gender = '', $race = '', $veteran = '', $disability = '',$lifeLic,$propLic,$ser6,$ser63,$ser7,$camp_id)
    {
        $sql = sprintf(
            "UPDATE
                candidate
            SET
                is_active             = %s,
                first_name            = %s,
                middle_name           = %s,
                last_name             = %s,
                email1                = %s,
                email2                = %s,
                phone_home            = ExtractNumber(%s),
                phone_work            = ExtractNumber(%s),
                phone_cell            = ExtractNumber(%s),
                address               = %s,
                city                  = %s,
                state                 = %s,
                zip                   = %s,
                source                = %s,
                key_skills            = %s,
                date_available        = %s,
                schedule_date        = %s,   
                current_employer      = %s,
                current_pay           = %s,
                desired_pay           = %s,
                can_relocate          = %s,
                is_hot                = %s,
                notes                 = %s,
                web_site              = %s,
                best_time_to_call     = %s,
                entered_by            = %s,
                owner                 = %s,
                date_modified         = NOW(),
                eeo_ethnic_type_id    = %s,
                eeo_veteran_type_id   = %s,
                eeo_disability_status = %s,
                eeo_gender            = %s,
                Life_and_Health_License = %s,
                Property_Casualty_License = %s,
                Series_6 = %s,
                Series_63 = %s,
                Series_7 = %s,
                campaign_id = %s
                
            WHERE
                candidate_id = %s
            AND
                entered_by = %s
            AND
                site_id = %s",
            ($isActive ? '1' : '0'),
            $this->_db->makeQueryString($firstName),
            $this->_db->makeQueryString($middleName),
            $this->_db->makeQueryString($lastName),
            $this->_db->makeQueryString($email1),
            $this->_db->makeQueryString($email2),
            $this->_db->makeQueryString($phoneHome),
            $this->_db->makeQueryString($phoneWork),
            $this->_db->makeQueryString($phoneCell),
            $this->_db->makeQueryString($address),
            $this->_db->makeQueryString($city),
            $this->_db->makeQueryString($state),
            $this->_db->makeQueryString($zip),
            $this->_db->makeQueryString($source),
            $this->_db->makeQueryString($keySkills),
            $this->_db->makeQueryStringOrNULL($dateAvailable),
            $this->_db->makeQueryStringOrNULL($scheduledDate),
            $this->_db->makeQueryString($currentEmployer),
            $this->_db->makeQueryString($currentPay),
            $this->_db->makeQueryString($desiredPay),
            ($canRelocate ? '1' : '0'),
            ($isHot ? '1' : '0'),
            $this->_db->makeQueryString($notes),
            $this->_db->makeQueryString($webSite),
            $this->_db->makeQueryString($bestTimeToCall),
            $this->_db->makeQueryString($enteredBy),
            $this->_db->makeQueryInteger($owner),
            $this->_db->makeQueryInteger($race),
            $this->_db->makeQueryInteger($veteran),
            $this->_db->makeQueryString($disability),
            $this->_db->makeQueryString($gender),
            $this->_db->makeQueryString(trim($lifeLic)),
            $this->_db->makeQueryString(trim($propLic)),
            $this->_db->makeQueryString(trim($ser6)),
            $this->_db->makeQueryString(trim($ser63)),
            $this->_db->makeQueryString(trim($ser7)),
            $this->_db->makeQueryString(trim($camp_id)),
            $this->_db->makeQueryInteger($candidateID),
            $this->_db->makeQueryString($enteredBy),
            $this->_siteID
            
        );

        $preHistory = $this->get($candidateID);
        $queryResult = $this->_db->query($sql);
        $postHistory = $this->get($candidateID);

        $history = new History($this->_siteID);
        $history->storeHistoryChanges(
            DATA_ITEM_CANDIDATE, $candidateID, $preHistory, $postHistory
        );

        if (!$queryResult)
        {
            return false;
        }

        if (!empty($emailAddress))
        {
            /* Send e-mail notification. */
            //FIXME: Make subject configurable.
            $mailer = new Mailer($this->_siteID);
            $mailerStatus = $mailer->sendToOne(
                array($emailAddress, ''),
                'RESUFLO Notification: Candidate Ownership Change',
                $email,
                true
            );
        }

        return true;
    }

    /**
     * Removes a candidate and all associated records from the system.
     *
     * @param integer Candidate ID to delete.
     * @return void
     */
    public function delete($candidateID)
    {
        /* Delete for candidate */
        $sql = sprintf(
            "DELETE FROM
                candidate
            WHERE
                candidate_id = %s
            AND
                site_id = %s",
            $this->_db->makeQueryInteger($candidateID),
            $this->_siteID
        );
        //$this->_db->query($sql);

        //Commented by Abid to save time
        //$history = new History($this->_siteID);
        //$history->storeHistoryDeleted(DATA_ITEM_CANDIDATE, $candidateID);
        
        /* Delete for candidate_joborder */
        $sql = $sql.';'.sprintf(
            "DELETE FROM
                candidate_joborder
            WHERE
                candidate_id = %s
            AND
                site_id = %s",
            $this->_db->makeQueryInteger($candidateID),
            $this->_siteID
        );
        //$this->_db->query($sql);

        /* Delete for candidate_joborder_status_history */
        $sql = $sql.';'.sprintf(
            "DELETE FROM
                candidate_joborder_status_history
            WHERE
                candidate_id = %s
            AND
                site_id = %s",
            $this->_db->makeQueryInteger($candidateID),
            $this->_siteID
        );
        //$this->_db->query($sql);

        /* Delete from saved_list_entry */
        $sql = $sql.';'.sprintf(
            "DELETE FROM
                saved_list_entry
            WHERE
                data_item_id = %s
            AND
                site_id = %s
            AND
                data_item_type = %s",
            $this->_db->makeQueryInteger($candidateID),
            $this->_siteID,
            DATA_ITEM_CANDIDATE
        );
        //$this->_db->query($sql);

        // ---------------------------------------------------------
        // Addtional Delete Queries: Added by Waleed
        // ---------------------------------------------------------
        
        /* Delete for extra_field */
        $sql = $sql.';'.sprintf(
            "DELETE FROM
                extra_field
           WHERE
                data_item_id = %s
            AND
                site_id = %s",
            $this->_db->makeQueryInteger($candidateID),
            $this->_siteID
        );
        //$this->_db->query($sql);

        /* Delete for calendar_event */
        $sql = $sql.';'.sprintf(
            "DELETE FROM
                calendar_event
           WHERE
                data_item_id = %s
            AND
                site_id = %s",
            $this->_db->makeQueryInteger($candidateID),
            $this->_siteID
        );
        //$this->_db->query($sql);

        /* Delete for drip_queue */
        $sql = $sql.';'.sprintf(
            "DELETE FROM
                drip_queue
            WHERE
                candidate_id = %s",
            $this->_db->makeQueryInteger($candidateID)
        );
        //$this->_db->query($sql);

        /* Delete for candidatenew */
        $sql = $sql.';'.sprintf(
            "DELETE FROM
                candidatenew
            WHERE
                candidate_id = %s
            AND
                site_id = %s",
            $this->_db->makeQueryInteger($candidateID),
            $this->_siteID
        );
        //$this->_db->query($sql);

        /* Delete for activity */
        $sql = $sql.';'.sprintf(
            "DELETE FROM
                activity
            WHERE
                data_item_id = %s
            AND
                site_id = %s",
            $this->_db->makeQueryInteger($candidateID),
            $this->_siteID
        );
        //$this->_db->query($sql);

        /* Delete for zzz_temp_backup_saved_list_entry */
        $sql = $sql.';'.sprintf(
            "DELETE FROM
                zzz_temp_backup_saved_list_entry
            WHERE
                data_item_id = %s
            AND
                site_id = %s",
            $this->_db->makeQueryInteger($candidateID),
            $this->_siteID
        );
        //$this->_db->query($sql);

        /* Delete for dripmarket_quesans */
        $sql = $sql.';'.sprintf(
            "DELETE FROM
                dripmarket_quesans
            WHERE
                cid = %s",
            $this->_db->makeQueryInteger($candidateID)
        );
        //$this->_db->query($sql);

        /* Delete for reminder_queue */
        $sql = $sql.';'.sprintf(
            "DELETE FROM
                reminder_queue
            WHERE
                candidate_id = %s",
            $this->_db->makeQueryInteger($candidateID)
        );
        //$this->_db->query($sql);

        /* Delete for candidate_removed . */
        $sql = $sql.';'.sprintf(
            "DELETE FROM
                candidate_removed
            WHERE
                candidate_id = %s
            AND
                site_id = %s",
            $this->_db->makeQueryInteger($candidateID),
            $this->_siteID
        );
        //$this->_db->query($sql);
        
        /* Delete for history */
        $sql = $sql.';'.sprintf(
            "DELETE FROM
                history
            WHERE
                data_item_id = %s
            AND
                site_id = %s",
            $this->_db->makeQueryInteger($candidateID),
            $this->_siteID
        );
        //$this->_db->query($sql);
        
        /* Delete for career_portal_questionnaire_history */
        $sql = $sql.';'.sprintf(
            "DELETE FROM
                career_portal_questionnaire_history
            WHERE
                candidate_id = %s
            AND
                site_id = %s",
            $this->_db->makeQueryInteger($candidateID),
            $this->_siteID
        );
        //$this->_db->query($sql);

        // ---------------------------------------------------------
        // Closing Addtional Delete Queries: Added by Waleed
        // ---------------------------------------------------------
        
        
        //Added by Added so execute queries at once
        //die($sql);
        $this->_db->queryMultiple($sql);
        
        /* Delete attachments. */
        $attachments = new Attachments($this->_siteID);
        $attachmentsRS = $attachments->getAll(
            DATA_ITEM_CANDIDATE, $candidateID
        );

        foreach ($attachmentsRS as $rowNumber => $row)
        {
            $attachments->delete($row['attachmentID']);
        }

        /* Delete extra fields. */
        $this->extraFields->deleteValueByDataItemID($candidateID);
    }

    /**
     * Returns all relevent candidate information for a given candidate ID.
     *
     * @param integer Candidate ID.
     * @return array Associative result set array of candidate data, or array()
     *               if no records were returned.
     */
    public function get($candidateID)
    {
        if(isset($_SESSION['RESUFLO'])){
            $userId = $_SESSION['RESUFLO']->getuserid();
            $userIdSearch = !empty($userId) ? " AND candidate.entered_by in (select user.user_id from user where user_id = $userId or entered_by = $userId)" : "";
        }
        
        $sql = sprintf(
            "SELECT
                candidate.candidate_id AS candidateID,
                candidate.is_active AS isActive,
                candidate.first_name AS firstName,
                candidate.middle_name AS middleName,
                candidate.last_name AS lastName,
                candidate.email1 AS email1,
                candidate.email2 AS email2,
                candidate.phone_home AS phoneHome,
                candidate.phone_work AS phoneWork,
                candidate.phone_cell AS phoneCell,
                candidate.address AS address,
                candidate.city AS city,
                candidate.state AS state,
                candidate.zip AS zip,
                candidate.source AS source,
                candidate.key_skills AS keySkills,
                candidate.current_employer AS currentEmployer,
                candidate.current_pay AS currentPay,
                candidate.desired_pay AS desiredPay,
                candidate.notes AS notes,
                candidate.activity_notes AS activity_notes,
                candidate.owner AS owner,
                candidate.can_relocate AS canRelocate,
                candidate.web_site AS webSite,
                candidate.best_time_to_call AS bestTimeToCall,
                candidate.is_hot AS isHot,
                candidate.is_admin_hidden AS isAdminHidden,
                candidate.Life_and_Health_License AS lifeLic,
                candidate.Property_Casualty_License AS propLic,
                candidate.Series_6 AS ser6,
                candidate.Series_63 AS ser63,
                candidate.Series_7 AS ser7,
                candidate_joborder.joborder_id,
                candidate.campaign_id AS camp_id,
                candidate.unsubscribe,
                owner_user.isSourceVisible AS isSourceVisible,
                DATE_FORMAT(
                    candidate.date_created, '%%m-%%d-%%y (%%h:%%i %%p)'
                ) AS dateCreated,
                DATE_FORMAT(
                    candidate.date_modified, '%%m-%%d-%%y (%%h:%%i %%p)'
                ) AS dateModified,
                DATE_FORMAT(
                    candidate.schedule_date, '%%m-%%d-%%y (%%h:%%i %%p)'
                ) AS dateScheduled,
                COUNT(
                    candidate_joborder.joborder_id
                ) AS pipeline,
                (
                    SELECT
                        COUNT(*)
                    FROM
                        candidate_joborder_status_history
                    WHERE
                        candidate_id = %s
                    AND
                        status_to = %s
                    AND
                        site_id = %s
                ) AS submitted,
                CONCAT(
                    candidate.first_name, ' ', candidate.last_name
                ) AS candidateFullName,
                CONCAT(
                    entered_by_user.first_name, ' ', entered_by_user.last_name
                ) AS enteredByFullName,
                CONCAT(
                    owner_user.first_name, ' ', owner_user.last_name
                ) AS ownerFullName,
                owner_user.email AS owner_email,
                DATE_FORMAT(
                    candidate.date_available, '%%m-%%d-%%y'
                ) AS dateAvailable,
                eeo_ethnic_type.type AS eeoEthnicType,
                eeo_veteran_type.type AS eeoVeteranType,
                candidate.eeo_disability_status AS eeoDisabilityStatus,
                candidate.eeo_gender AS eeoGender,
                IF (candidate.eeo_gender = 'm',
                    'Male',
                    IF (candidate.eeo_gender = 'f',
                        'Female',
                        ''))
                     AS eeoGenderText
            FROM
                candidate
            LEFT JOIN user AS entered_by_user
                ON candidate.entered_by = entered_by_user.user_id
            LEFT JOIN user AS owner_user
                ON candidate.owner = owner_user.user_id
            LEFT JOIN candidate_joborder
                ON candidate.candidate_id = candidate_joborder.candidate_id
            LEFT JOIN eeo_ethnic_type
                ON eeo_ethnic_type.eeo_ethnic_type_id = candidate.eeo_ethnic_type_id
            LEFT JOIN eeo_veteran_type
                ON eeo_veteran_type.eeo_veteran_type_id = candidate.eeo_veteran_type_id
            WHERE
                candidate.candidate_id = %s
            AND
                candidate.site_id = %s
                %s
            GROUP BY
                candidate.candidate_id",
            $this->_db->makeQueryInteger($candidateID),
            PIPELINE_STATUS_SUBMITTED,
            $this->_siteID,
            $this->_db->makeQueryInteger($candidateID),
            $this->_siteID,
            $userIdSearch
        );
        //echo $sql;
        return $this->_db->getAssoc($sql);
    }

    /**
     * Returns all candidate information relevent for the Edit Candidate page
     * for a given candidate ID.
     *
     * @param integer Candidate ID.
     * @return array Associative result set array of candidate data, or array()
     *               if no records were returned.
     */
    public function getForEditing($candidateID)
    {
        $sql = sprintf(
            "SELECT
                candidate.candidate_id AS candidateID,
                candidate.is_active AS isActive,
                candidate.first_name AS firstName,
                candidate.middle_name AS middleName,
                candidate.last_name AS lastName,
                candidate.email1 AS email1,
                candidate.email2 AS email2,
                candidate.phone_home AS phoneHome,
                candidate.phone_work AS phoneWork,
                candidate.phone_cell AS phoneCell,
                candidate.address AS address,
                candidate.city AS city,
                candidate.state AS state,
                candidate.zip AS zip,
                candidate.source AS source,
                candidate.key_skills AS keySkills,
                candidate.current_employer AS currentEmployer,
                candidate.current_pay AS currentPay,
                candidate.desired_pay AS desiredPay,
                candidate.notes AS notes,
                candidate.activity_notes AS activity_notes,
                candidate.owner AS owner,
                candidate.can_relocate AS canRelocate,
                candidate.web_site AS webSite,
                candidate.best_time_to_call AS bestTimeToCall,
                candidate.is_hot AS isHot,
                candidate.eeo_ethnic_type_id AS eeoEthnicTypeID,
                candidate.eeo_veteran_type_id AS eeoVeteranTypeID,
                candidate.eeo_disability_status AS eeoDisabilityStatus,
                candidate.eeo_gender AS eeoGender,
                candidate.is_admin_hidden AS isAdminHidden,
                candidate.is_admin_hidden AS isAdminHidden,
                candidate.Life_and_Health_License AS lifeLic,
                candidate.Property_Casualty_License AS propLic,
                candidate.Series_6 AS ser6,
                candidate.Series_63 AS ser63,
                candidate.Series_7 AS ser7,
                candidate.campaign_id AS camp_id,
                DATE_FORMAT(
                    candidate.date_available, '%%m-%%d-%%y'
                ) AS dateAvailable,
                DATE_FORMAT(
                    candidate.schedule_date, '%%Y-%%m-%%d %%T'
                ) AS scheduledDate
            FROM
                candidate
            WHERE
                candidate.candidate_id = %s
            AND
                candidate.site_id = %s",
            $this->_db->makeQueryInteger($candidateID),
            $this->_siteID
        );

        return $this->_db->getAssoc($sql);
    }
    
    public function getSms($candidateID, $userId)
    {
        $updateSql = sprintf(
            "UPDATE drip_sms_reply SET IsRead = 1
            WHERE
                CandidateId = %s AND UserId = %s",
            $this->_db->makeQueryInteger($candidateID),
            $this->_db->makeQueryInteger($userId)
        );
        $this->_db->query($updateSql);
        
        $sql = sprintf(
            "SELECT s.CandidateId, c.first_name, c.last_name, c.phone_cell, s.DripSmsReplyId, s.MessageSid, s.Body, s.date_created, s.Direction FROM drip_sms_reply s left join candidate c on s.candidateid = c.candidate_id
            WHERE
                s.CandidateId = %s
            AND
                s.UserId = %s",
            $this->_db->makeQueryInteger($candidateID),
            $this->_db->makeQueryInteger($userId)
        );

        return $this->_db->getAllAssoc($sql);
    }

    // FIXME: Document me.
    public function getExport($IDs)
    {
        if (count($IDs) != 0)
        {
            $IDsValidated = array();
            
            foreach ($IDs as $id)
            {
                $IDsValidated[] = $this->_db->makeQueryInteger($id);
            }
            
            $criterion = 'AND candidate.candidate_id IN ('.implode(',', $IDsValidated).')';
        }
        else
        {
            $criterion = '';
        }

        $sql = sprintf(
            "SELECT
                candidate.candidate_id AS candidateID,
                candidate.last_name AS lastName,
                candidate.first_name AS firstName,
                candidate.phone_home AS phoneHome,
                candidate.phone_cell AS phoneCell,
                candidate.email1 AS email1,
                candidate.key_skills as keySkills
            FROM
                candidate
            WHERE
                candidate.site_id = %s
                %s
            ORDER BY
                candidate.last_name ASC,
                candidate.first_name ASC",
            $this->_siteID,
            $criterion
        );

        return $this->_db->getAllAssoc($sql);
    }

    /**
     * Returns a candidate ID that matches the specified e-mail address.
     *
     * @param string Candidate e-mail address,
     * @return integer Candidate ID, or -1 if no matching candidates were
     *                 found.
     */
    public function getIDByEmail($email)
    {
        $sql = sprintf(
            "SELECT
                candidate.candidate_id AS candidateID
            FROM
                candidate
            WHERE
            (
                candidate.email1 = %s
                OR candidate.email2 = %s
            )
            AND
                candidate.site_id = %s",
            $this->_db->makeQueryString($email),
            $this->_db->makeQueryString($email),
            $this->_siteID
        );
        $rs = $this->_db->getAssoc($sql);

        if (empty($rs))
        {
            return -1;
        }

        return $rs['candidateID'];
    }

    /**
     * Returns the number of candidates in the system.  Useful
     * for determining if the friendly "no candidates in system"
     * should be displayed rather than the datagrid.
     *
     * @param boolean Include administratively hidden candidates?
     * @return integer Number of Candidates in site.
     */
    public function getCount($allowAdministrativeHidden = false)
    {
        if (!$allowAdministrativeHidden)
        {
            $adminHiddenCriterion = 'AND candidate.is_admin_hidden = 0';
        }
        else
        {
            $adminHiddenCriterion = '';
        }

        $sql = sprintf(
            "SELECT
                COUNT(*) AS totalCandidates
            FROM
                candidate
            WHERE
                candidate.site_id = %s
            %s",
            $this->_siteID,
            $adminHiddenCriterion
        );

        return $this->_db->getColumn($sql, 0, 0);
    }

    /**
     * Returns the entire candidates list.
     *
     * @param boolean Include administratively hidden candidates?
     * @return array Multi-dimensional associative result set array of
     *               candidates data, or array() if no records were returned.
     */
    public function getAll($allowAdministrativeHidden = false)
    {
        if (!$allowAdministrativeHidden)
        {
            $adminHiddenCriterion = 'AND candidate.is_admin_hidden = 0';
        }
        else
        {
            $adminHiddenCriterion = '';
        }

        $sql = sprintf(
            "SELECT
                candidate.candidate_id AS candidateID,
                candidate.last_name AS lastName,
                candidate.first_name AS firstName,
                candidate.phone_home AS phoneHome,
                candidate.phone_cell AS phoneCell,
                candidate.email1 AS email1,
                candidate.key_skills AS keySkills,
                candidate.is_hot AS isHot,
                DATE_FORMAT(
                    candidate.date_created, '%%m-%%d-%%y'
                ) AS dateCreated,
                DATE_FORMAT(
                    candidate.date_modified, '%%m-%%d-%%y'
                ) AS dateModified,
                candidate.date_created AS dateCreatedSort,
                owner_user.first_name AS ownerFirstName,
                owner_user.last_name AS ownerLastName
            FROM
                candidate
            LEFT JOIN user AS owner_user
                ON candidate.entered_by = user.user_id
            WHERE
                candidate.site_id = %s
            %s
            ORDER BY
                candidate.last_name ASC,
                candidate.first_name ASC",
            $this->_siteID,
            $adminHiddenCriterion
        );

        return $this->_db->getAllAssoc($sql);
    }

    /**
     * Returns all resumes for a candidate.
     *
     * @param integer Candidate ID.
     * @return array Multi-dimensional associative result set array of
     *               candidate attachments data, or array() if no records were
     *               returned.
     */
    public function getResumes($candidateID)
    {
        $sql = sprintf(
            "SELECT
                attachment.attachment_id AS attachmentID,
                attachment.data_item_id AS candidateID,
                attachment.title AS title,
                attachment.text AS text
            FROM
                attachment
            WHERE
                resume = 1
            AND
                attachment.data_item_type = %s
            AND
                attachment.data_item_id = %s
            AND
                attachment.site_id = %s",
            DATA_ITEM_CANDIDATE,
            $this->_db->makeQueryInteger($candidateID),
            $this->_siteID
        );

        return $this->_db->getAllAssoc($sql);
    }

    /**
     * Returns a candidate resume attachment by attachment.
     *
     * @param integer Attachment ID.
     * @return array Associative result set array of candidate / attachment
     *               data, or array() if no records were returned.
     */
    public function getResume($attachmentID)
    {
        $sql = sprintf(
            "SELECT
                attachment.attachment_id AS attachmentID,
                attachment.data_item_id AS candidateID,
                attachment.title AS title,
                attachment.text AS text,
                candidate.first_name AS firstName,
                candidate.last_name AS lastName
            FROM
                attachment
            LEFT JOIN candidate
                ON attachment.data_item_id = candidate.candidate_id
                AND attachment.site_id = candidate.site_id
            WHERE
                attachment.resume = 1
            AND
                attachment.attachment_id = %s
            AND
                attachment.site_id = %s",
            $this->_db->makeQueryInteger($attachmentID),
            $this->_siteID
        );

        return $this->_db->getAssoc($sql);
    }

    /**
     * Returns an array of job orders data (jobOrderID, title, companyName)
     * for the specified candidate ID.
     *
     * @param integer Candidate ID,
     * @return array Multi-dimensional associative result set array of
     *               job orders data, or array() if no records were returned.
     */
    public function getJobOrdersArray($candidateID)
    {
        $sql = sprintf(
            "SELECT
                joborder.joborder_id AS jobOrderID,
                joborder.title AS title,
                company.name AS companyName
            FROM
                joborder
            LEFT JOIN company
                ON joborder.company_id = company.company_id
            LEFT JOIN candidate_joborder
                ON joborder.joborder_id = candidate_joborder.joborder_id
            WHERE
                candidate_joborder.candidate_id = %s
            AND
                joborder.site_id = %s
            ORDER BY
                title ASC",
            $this->_db->makeQueryInteger($candidateID),
            $this->_siteID
        );

        return $this->_db->getAllAssoc($sql);
     }

    /**
     * Updates a candidate's modified timestamp.
     *
     * @param integer Candidate ID.
     * @return boolean Boolean was the query executed successfully?
     */
    public function updateModified($candidateID)
    {
        $sql = sprintf(
            "UPDATE
                candidate
            SET
                date_modified = NOW()
            WHERE
                candidate_id = %s
            AND
                site_id = %s",
            $this->_db->makeQueryInteger($candidateID),
            $this->_siteID
        );

        return (boolean) $this->_db->query($sql);
    }

    /**
     * Returns all upcoming events for the candidate.
     *
     * @param integer Candidate ID.
     * @return array Multi-dimensional associative result set array of
     *               candidate events data, or array() if no records were
     *               returned.
     */
    public function getUpcomingEvents($candidateID)
    {
        $calendar = new Calendar($this->_siteID);
        return $calendar->getUpcomingEventsByDataItem(
            DATA_ITEM_CANDIDATE, $candidateID
        );
    }
    
    /**
     * Returns reminder, thanks and phone events for the candidate.
     *
     * @param integer Candidate ID.
     * @return array Multi-dimensional associative result set array of
     *               candidate events data, or array() if no records were
     *               returned.
     */
    public function getReminderThanksPhoneEvents($candidateID)
    {
        $calendar = new Calendar($this->_siteID);
        return $calendar->getReminderThanksPhoneEvents(
            DATA_ITEM_CANDIDATE, $candidateID
        );
    }

    /**
     * Gets all possible source suggestions for a site.
     *
     * @return array Multi-dimensional associative result set array of
     *               candidate sources data.
     */
    public function getPossibleSources()
    {
        $sql = sprintf(
            "SELECT
                candidate_source.source_id AS sourceID,
                candidate_source.name AS name
            FROM
                candidate_source
            WHERE
                candidate_source.site_id = %s
            ORDER BY
                candidate_source.name ASC",
            $this->_siteID
        );

        return $this->_db->getAllAssoc($sql);
    }

    /**
     * Updates a sites possible sources with an array generated
     * by getDifferencesFromList (ListEditor.php).
     *
     * @param array Result of ListEditor::getDifferencesFromList().
     * @return void
     */
    public function updatePossibleSources($updates)
    {
        $history = new History($this->_siteID);

        foreach ($updates as $update)
        {
            switch ($update[2])
            {
                case LIST_EDITOR_ADD:
                    $sql = sprintf(
                        "INSERT INTO candidate_source (
                            name,
                            site_id,
                            date_created
                         )
                         VALUES (
                            %s,
                            %s,
                            NOW()
                         )",
                         $this->_db->makeQueryString($update[0]),
                         $this->_siteID
                    );
                    $this->_db->query($sql);

                    break;

                case LIST_EDITOR_REMOVE:
                    $sql = sprintf(
                        "DELETE FROM
                            candidate_source
                         WHERE
                            source_id = %s
                         AND
                            site_id = %s",
                         $update[1],
                         $this->_siteID
                    );
                    $this->_db->query($sql);

                    break;

                case LIST_EDITOR_MODIFY:
                    $sql = sprintf(
                        "SELECT
                            name
                         FROM
                            candidate_source
                         WHERE
                            source_id = %s
                         AND
                            site_id = %s",
                         $this->_db->makeQueryInteger($update[1]),
                         $this->_siteID
                    );
                    $firstSource = $this->_db->getAssoc($sql);

                    $sql = sprintf(
                        "UPDATE
                            candidate
                         SET
                            source = %s
                         WHERE
                            source = %s
                         AND
                            site_id = %s",
                         $update[1],
                         $this->_db->makeQueryString($firstSource['name']),
                         $this->_siteID
                    );
                    $this->_db->query($sql);

                    $sql = sprintf(
                        "UPDATE
                            candidate_source
                         SET
                            name = %s
                         WHERE
                            source_id = %s
                         AND
                            site_id = %s",
                         $this->_db->makeQueryString($update[0]),
                         $this->_db->makeQueryInteger($update[1]),
                         $this->_siteID
                    );
                    $this->_db->query($sql);

                    break;

                default:
                    break;
            }
        }
    }

    /**
     * Changes the administrative hide / show flag.
     * Only can be accessed by a MSA or higher user.
     *
     * @param integer Candidate ID.
     * @param boolean Administratively hide this candidate?
     * @return boolean Was the query executed successfully?
     */    
    public function administrativeHideShow($candidateID, $state)
    {
        $sql = sprintf(
            "UPDATE
                candidate
            SET
                is_admin_hidden = %s
            WHERE
                candidate_id = %s
            AND
                site_id = %s",
            ($state ? 1 : 0),
            $this->_db->makeQueryInteger($candidateID),
            $this->_siteID
        );

        return (boolean) $this->_db->query($sql);
    }
}


class CandidatesDataGrid extends DataGrid
{
    protected $_siteID;
    
    // FIXME: Fix ugly indenting - ~400 character lines = bad.
    public function __construct($instanceName, $siteID, $parameters, $misc = 0) {
        Sheesh::get_sheesh();
        $userid= $_SESSION['RESUFLO']->getuserid(); 
        $this->_db = DatabaseConnection::getInstance();
        $this->_siteID = $siteID;
        if ($_SESSION['RESUFLO']->getClientLogin()==1 && $_SESSION['RESUFLO']->getAccessLevel()==400) {
            $this->_assignedCriterion = " AND (candidate.entered_by IN (SELECT user_id FROM user where user_id=".$_SESSION['RESUFLO']->getUserID()." or entered_by=".$_SESSION['RESUFLO']->getUserID()."))";
        } else if($_SESSION['RESUFLO']->getClientLogin()==1 && $_SESSION['RESUFLO']->getAccessLevel()!=400){
            $this->_assignedCriterion = "and candidate.entered_by=".$_SESSION['RESUFLO']->getUserID();
        } else if($_SESSION['RESUFLO']->getAccessLevel()==500){
            $this->_assignedCriterion = "";
        } else {
            $this->_assignedCriterion = "and candidate.entered_by=".$_SESSION['RESUFLO']->getUserID();
        }
        if(isset($_REQUEST['list']) && intval($_REQUEST['list']) > 0){
            $this->_assignedCriterion .= ' and candidate.list_id <> 0';
        }

        $this->_assignedCriterion .= " and (candidate.job_id is null or candidate.job_id = 0)  ";
        if($_GET['m'] == "lists" && $_GET['a'] == "showList")
        {
            $this->_assignedCriterion .= " AND candidate.unsubscribe <> 1 ";
        }
        $this->get = $_GET;
        
        if($_REQUEST['a']=='follow' || $_REQUEST['delrec']=='follow') {
            $this->_assignedCriterion .= "  
            and follow_up = '1' 
            and  candidate.candidate_id NOT IN (SELECT candidate_id FROM candidate_joborder where added_by=".$userid.") 
            and candidate.candidate_id NOT IN (SELECT candidate_id FROM candidate_joborder where added_by IN (SELECT user_id FROM user where entered_by=".$_SESSION['RESUFLO']->getUserID().")) 
            and candidate.list_id = '0'";
                            $this->_dataItemIDColumn = 'candidate.candidate_id';
                            
                    
                                
                                
                
                        $this->_classColumns = array(
                            'Attachments' => array('select' => 'IF(candidate_joborder_submitted.candidate_joborder_id, 1, 0) AS submitted,
                                                                IF(attachment_id, 1, 0) AS attachmentPresent',
                
                                                     'pagerRender' => 'if ($rsData[\'submitted\'] == 1)
                                                                    {
                                                                        $return = \'<img src="images/job_orders.gif" alt="" width="16" height="16" title="Submitted for a Job Order" />\';
                                                                    }
                                                                    else
                                                                    {
                                                                        $return = \'<img src="images/mru/blank.gif" alt="" width="16" height="16" />\';
                                                                    }
                
                                                                    if ($rsData[\'attachmentPresent\'] == 1)
                                                                    {
                                                                        $return .= \'<img src="images/paperclip.gif" alt="" width="16" height="16" title="Attachment Present" />\';
                                                                    }
                                                                    else
                                                                    {
                                                                        $return .= \'<img src="images/mru/blank.gif" alt="" width="16" height="16" />\';
                                                                    }
                
                                                                    return $return;
                                                                   ',
                
                                                     'join'     => 'LEFT JOIN attachment
                                                                        ON candidate.candidate_id = attachment.data_item_id
                                                                                                                                AND attachment.data_item_type = '.DATA_ITEM_CANDIDATE.'
                                                                    LEFT JOIN candidate_joborder AS candidate_joborder_submitted
                                                                        ON candidate_joborder_submitted.candidate_id = candidate.candidate_id
                                                                        AND candidate_joborder_submitted.status >= '.PIPELINE_STATUS_SUBMITTED.'
                                                                        AND candidate_joborder_submitted.site_id = '.$this->_siteID.'
                                                                        AND candidate_joborder_submitted.status != '.PIPELINE_STATUS_NOTINCONSIDERATION,
                                                     'pagerWidth'    => 34,
                                                     'pagerOptional' => true,
                                                     'pagerNoTitle' => true,
                                                     'sizable'  => false,
                                                     'exportable' => false,
                                                     'filterable' => false),
                
                            'First Name' =>     array('select'         => 'candidate.first_name AS firstName',
                                                      'pagerRender'    => 'if ($rsData[\'isHot\'] == 1) $className =  \'jobLinkHot\'; else $className = \'jobLinkCold\'; return \'<a href="'.RESUFLOUtility::getIndexName().'?m=candidates&amp;list='.$this->get['a'].'&amp;a=show&amp;candidateID=\'.$rsData[\'candidateID\'].\'&amp;view=1" class="\'.$className.\'">\'.htmlspecialchars($rsData[\'firstName\']).\'</a>\';',
                                                      'sortableColumn' => 'firstName',
                                                      'pagerWidth'     => 75,
                                                      'pagerOptional'  => false,
                                                      'alphaNavigation'=> true,
                                                      'filter'         => 'candidate.first_name'),
                
                            'Last Name' =>      array('select'         => 'candidate.last_name AS lastName',
                                                     'sortableColumn'  => 'lastName',
                                                     'pagerRender'     => 'if ($rsData[\'isHot\'] == 1) $className =  \'jobLinkHot\'; else $className = \'jobLinkCold\'; return \'<a href="'.RESUFLOUtility::getIndexName().'?m=candidates&amp;list='.$this->get['a'].'&amp;a=show&amp;candidateID=\'.$rsData[\'candidateID\'].\'&amp;view=1" class="\'.$className.\'">\'.htmlspecialchars($rsData[\'lastName\']).\'</a>\';',
                                                     'pagerWidth'      => 85,
                                                     'pagerOptional'   => false,
                                                     'alphaNavigation' => true,
                                                     'filter'         => 'candidate.last_name'),
                
                            'E-Mail' =>         array('select'   => 'candidate.email1 AS email1',
                                                     'sortableColumn'     => 'email1',
                                                     'pagerWidth'    => 80,
                                                     'filter'         => 'candidate.email1'),
                
                            '2nd E-Mail' =>     array('select'   => 'candidate.email2 AS email2',
                                                     'sortableColumn'     => 'email2',
                                                     'pagerWidth'    => 80,
                                                     'filter'         => 'candidate.email2'),
                
                            'Home Phone' =>     array('select'   => 'candidate.phone_home AS phoneHome',
                                                     'sortableColumn'     => 'phoneHome',
                                                     'pagerWidth'    => 80,
                                                     'filter'         => 'candidate.phone_home'),
                
                            'Cell Phone' =>     array('select'   => 'candidate.phone_cell AS phoneCell',
                                                     'sortableColumn'     => 'phoneCell',
                                                     'pagerWidth'    => 80,
                                                     'filter'         => 'candidate.phone_cell'),
                
                            'Work Phone' =>     array('select'   => 'candidate.phone_work AS phoneWork',
                                                     'sortableColumn'     => 'phoneWork',
                                                     'pagerWidth'    => 80),
                
                            'Address' =>        array('select'   => 'candidate.address AS address',
                                                     'sortableColumn'     => 'address',
                                                     'pagerWidth'    => 250,
                                                     'alphaNavigation' => true,
                                                     'filter'         => 'candidate.address'),
                
                            'City' =>           array('select'   => 'candidate.city AS city',
                                                     'sortableColumn'     => 'city',
                                                     'pagerWidth'    => 80,
                                                     'alphaNavigation' => true,
                                                     'filter'         => 'candidate.city'),
                                          'Company' =>           array('select'   => 'Case When candidate.entered_by = '.$_SESSION['RESUFLO']->getUserId().' then "'.$_SESSION['RESUFLO']->getFirstName().'" else (select first_name from user where user_id =(select entered_by from user where user_id=candidate.entered_by) ) End as company',
                                                     'sortableColumn'     => 'company',
                                                     'pagerWidth'    => 80,
                                                     'alphaNavigation' => true,
                                                     'filter'         => 'candidate.entered_by'),
                
                
                            'State' =>          array('select'   => 'candidate.state AS state',
                                                     'sortableColumn'     => 'state',
                                                     'filterType' => 'dropDown',
                                                     'pagerWidth'    => 50,
                                                     'alphaNavigation' => true,
                                                     'filter'         => 'candidate.state'),
                
                            'Zip' =>            array('select'  => 'candidate.zip AS zip',
                                                     'sortableColumn'    => 'zip',
                                                     'pagerWidth'   => 50,
                                                     'filter'         => 'candidate.zip'),
                
                            'Misc Notes' =>     array('select'  => 'candidate.notes AS notes',
                                                     'sortableColumn'    => 'notes',
                                                     'pagerWidth'   => 300,
                                                     'filter'         => 'candidate.notes'),
                
                            'Web Site' =>      array('select'  => 'candidate.web_site AS webSite',
                                                     'pagerRender'     => 'return \'<a href="\'.htmlspecialchars($rsData[\'webSite\']).\'">\'.htmlspecialchars($rsData[\'webSite\']).\'</a>\';',
                                                     'sortableColumn'    => 'webSite',
                                                     'pagerWidth'   => 80,
                                                     'filter'         => 'candidate.web_site'),
                
                            'Key Skills' =>    array('select'  => 'candidate.key_skills AS keySkills',
                                                     'pagerRender' => 'return substr(trim($rsData[\'keySkills\']), 0, 30) . (strlen(trim($rsData[\'keySkills\'])) > 30 ? \'...\' : \'\');',
                                                     'sortableColumn'    => 'keySkills',
                                                     'pagerWidth'   => 210,
                                                     'filter'         => 'candidate.key_skills'),
                
                            'Recent Status' => array('select'  => '(
                                                                    SELECT
                                                                        CONCAT(
                                                                            \'<a href="'.RESUFLOUtility::getIndexName().'?m=joborders&amp;list='.$this->get['a'].'&amp;a=show&amp;jobOrderID=\',
                                                                            joborder.joborder_id,
                                                                            \'" title="\',
                                                                            joborder.title,
                                                                            \' (\',
                                                                            company.name,
                                                                            \')">\',
                                                                            candidate_joborder_status.short_description,
                                                                            \'</a>\'
                                                                        )
                                                                    FROM
                                                                        candidate_joborder
                                                                    LEFT JOIN candidate_joborder_status
                                                                        ON candidate_joborder_status.candidate_joborder_status_id = candidate_joborder.status


                                                                    LEFT JOIN joborder
                                                                        ON joborder.joborder_id = candidate_joborder.joborder_id
                                                                    LEFT JOIN company
                                                                        ON joborder.company_id = company.company_id
                                                                    WHERE
                                                                        candidate_joborder.candidate_id = candidate.candidate_id
                                                                    ORDER BY
                                                                        candidate_joborder.date_modified DESC
                                                                    LIMIT 1
                                                                ) AS lastStatus
                                                                ',
                                                     'sort'    => 'lastStatus',
                                                     'pagerRender'     => 'return $rsData[\'lastStatus\'];',
                                                     'exportRender'     => 'return $rsData[\'lastStatus\'];',
                                                     'pagerWidth'   => 140,
                                                     'exportable' => false,
                                                     'filterHaving'  => 'lastStatus',
                                                     'filterTypes'   => '=~'),
                                                     
                            'Activity Notes' =>     array('select'  => 'candidate.activity_notes AS activity_notes',
                                                     'sortableColumn'    => 'activity_notes',
                                                     'pagerWidth'   => 300,
                                                     'filter'         => 'candidate.activity_notes'),
                            'Recent Status (Extended)' => array('select'  => '(
                                                                    SELECT
                                                                        CONCAT(
                                                                            candidate_joborder_status.short_description,
                                                                            \'<br />\',
                                                                            \'<a href="'.RESUFLOUtility::getIndexName().'?m=companies&amp;list='.$this->get['a'].'&amp;a=show&amp;companyID=\',
                                                                            company.company_id,
                                                                            \'">\',
                                                                            company.name,
                                                                            \'</a> - \',
                                                                            \'<a href="'.RESUFLOUtility::getIndexName().'?m=joborders&amp;list='.$this->get['a'].'&amp;a=show&amp;jobOrderID=\',
                                                                            joborder.joborder_id,
                                                                            \'">\',
                                                                            joborder.title,
                                                                            \'</a>\'
                                                                        )
                                                                    FROM
                                                                        candidate_joborder
                                                                    LEFT JOIN candidate_joborder_status
                                                                        ON candidate_joborder_status.candidate_joborder_status_id = candidate_joborder.status
                                                                    LEFT JOIN joborder
                                                                        ON joborder.joborder_id = candidate_joborder.joborder_id
                                                                    LEFT JOIN company
                                                                        ON joborder.company_id = company.company_id
                                                                    WHERE
                                                                        candidate_joborder.candidate_id = candidate.candidate_id
                                                                    ORDER BY
                                                                        candidate_joborder.date_modified DESC
                                                                    LIMIT 1
                                                                ) AS lastStatusLong
                                                                ',
                                                     'sortableColumn'    => 'lastStatusLong',
                                                     'pagerRender'     => 'return $rsData[\'lastStatusLong\'];',
                                                     'pagerWidth'   => 310,
                                                     'exportable' => false,
                                                     'filterable' => false),
                
                            'Source' =>        array('select'  => 'candidate.source AS source',
                                                     'sortableColumn'    => 'source',
                                                     'pagerWidth'   => 140,
                                                     'alphaNavigation' => true,
                                                     'filter'         => 'candidate.source'),
                
                            'Available' =>     array('select'   => 'DATE_FORMAT(candidate.date_available, \'%m-%d-%y\') AS dateAvailable',
                                                     'sortableColumn'     => 'dateAvailable',
                                                     'pagerWidth'    => 60),
                
                            'Current Employer' => array('select'  => 'candidate.current_employer AS currentEmployer',
                                                     'sortableColumn'    => 'currentEmployer',
                                                     'pagerWidth'   => 125,
                                                     'alphaNavigation' => true,
                                                     'filter'         => 'candidate.current_employer'),
                
                            'Current Pay' => array('select'  => 'candidate.current_pay AS currentPay',
                                                     'sortableColumn'    => 'currentPay',
                                                     'pagerWidth'   => 125,
                                                     'filter'         => 'candidate.current_pay',
                                                     'filterTypes'   => '===>=<'),
                
                            'Desired Pay' => array('select'  => 'candidate.desired_pay AS desiredPay',
                                                     'sortableColumn'    => 'desiredPay',
                                                     'pagerWidth'   => 125,
                                                     'filter'         => 'candidate.desired_pay',
                                                     'filterTypes'   => '===>=<'),
                
                            'Can Relocate'  => array('select'  => 'candidate.can_relocate AS canRelocate',
                                                     'pagerRender'     => 'return ($rsData[\'canRelocate\'] == 0 ? \'No\' : \'Yes\');',
                                                     'exportRender'     => 'return ($rsData[\'canRelocate\'] == 0 ? \'No\' : \'Yes\');',
                                                     'sortableColumn'    => 'canRelocate',
                                                     'pagerWidth'   => 80,
                                                     'filter'         => 'candidate.can_relocate'),
                
                            'Recruiter' =>         array('select'   => 'owner_user.first_name AS ownerFirstName,' .
                                                                   'owner_user.last_name AS ownerLastName,' .
                                                                   'CONCAT(owner_user.last_name, owner_user.first_name) AS ownerSort',
                                                     'join'     => 'LEFT JOIN user AS owner_user ON candidate.owner = owner_user.user_id',
                                                     'pagerRender'      => 'return StringUtility::makeInitialName($rsData[\'ownerFirstName\'], $rsData[\'ownerLastName\'], false, LAST_NAME_MAXLEN);',
                                                     'exportRender'     => 'return $rsData[\'ownerFirstName\'] . " " .$rsData[\'ownerLastName\'];',
                                                     'sortableColumn'     => 'ownerSort',
                                                     'pagerWidth'    => 75,
                                                     'alphaNavigation' => true,
                                                     'filter'         => 'CONCAT(owner_user.first_name, owner_user.last_name)'),
                
                            'Created' =>       array('select'   => 'DATE_FORMAT(candidate.date_created, \'%m-%d-%y\') AS dateCreated',
                                                     'pagerRender'      => 'return $rsData[\'dateCreated\'];',
                                                     'sortableColumn'     => 'dateCreatedSort',
                                                     'pagerWidth'    => 60,
                                                     'filterHaving' => 'DATE_FORMAT(candidate.date_created, \'%m-%d-%y\')'),
                            
                            'Date From' =>       array('select'   => 'DATE_FORMAT(candidate.date_created, \'%m-%d-%y\') AS dateCreated',
                                                    'pagerRender'      => 'return $rsData[\'dateCreated\'];',
                                                    'sortableColumn'     => 'dateCreatedSort',
                                                    'pagerWidth'    => 60,
                                                    'filterHaving' => 'DATE_FORMAT(candidate.date_created, \'%y-%m-%d\')',
                                                    'filterTypes'   => '=>'),
                
                            'Date To' =>       array('select'   => 'DATE_FORMAT(candidate.date_created, \'%m-%d-%y\') AS dateTo',
                                                    'pagerRender'      => 'return $rsData[\'dateTo\'];',
                                                    'sortableColumn'     => 'dateToSort',
                                                    'pagerWidth'    => 60,
                                                    'filterHaving' => 'DATE_FORMAT(candidate.date_created, \'%y-%m-%d\')',
                                                    'filterTypes'   => '=<'),
                
                            'Modified' =>      array('select'   => 'DATE_FORMAT(candidate.date_modified, \'%m-%d-%y\') AS dateModified',
                                                     'pagerRender'      => 'return $rsData[\'dateModified\'];',
                                                     'sortableColumn'     => 'dateModifiedSort',
                                                     'pagerWidth'    => 60,
                                                     'pagerOptional' => false,
                                                     'filterHaving' => 'DATE_FORMAT(candidate.date_modified, \'%m-%d-%y\')'),
                            
                            'Scheduled' =>      array('select'   => 'DATE_FORMAT(candidate.schedule_date, \'%m-%d-%y %r\') AS dateScheduled',
                                         'pagerRender'      => 'return $rsData[\'dateScheduled\'];',
                                         'sortableColumn'     => 'dateScheduledSort',
                                         'pagerWidth'    => 150,
                                         'pagerOptional' => true,
                                         'filterHaving' => 'DATE_FORMAT(candidate.schedule_date, \'%m-%d-%y %r\')'),
                
                            /* This one only works when called from the saved list view.  Thats why it is not optional, filterable, or exportable.
                             * FIXME:  Somehow make this defined in the associated savedListDataGrid class child.
                             */
                            'Added To List' =>  array('select'   => 'DATE_FORMAT(saved_list_entry.date_created, \'%m-%d-%y\') AS dateAddedToList,
                                                                     saved_list_entry.date_created AS dateAddedToListSort',
                                                     'pagerRender'      => 'return $rsData[\'dateAddedToList\'];',
                                                     'sortableColumn'     => 'dateAddedToListSort',
                                                     'pagerWidth'    => 60,
                                                     'pagerOptional' => false,
                                                     'filterable' => false,
                                                     'exportable' => false),
                
                            'OwnerID' =>       array('select'    => '',
                                                     'filter'    => 'candidate.owner',
                                                     'pagerOptional' => false,

                                                     'filterable' => false,
                                                     'filterDescription' => 'Only My Candidates'),
                
                            'IsHot' =>         array('select'    => '',
                                                     'filter'    => 'candidate.is_hot',
                                                     'pagerOptional' => false,
                                                     'filterable' => false,
                                                     'filterDescription' => 'Only Hot Candidates'),
                            'Campaign' => $GLOBALS['sheesh']
                        );
                
                        if (US_ZIPS_ENABLED)
                        {
                            $this->_classColumns['Near Zipcode'] =
                                               array('select'  => 'candidate.zip AS zip',
                                                     'filter' => 'candidate.zip',
                                                     'pagerOptional' => false,
                                                     'filterTypes'   => '=@');
                        }
                
                        /* Extra fields get added as columns here. */
                        $candidates = new Candidates($this->_siteID);
                        $extraFieldsRS = $candidates->extraFields->getSettings();
                        foreach ($extraFieldsRS as $index => $data)
                        {
                            $fieldName = $data['fieldName'];
                
                            if (!isset($this->_classColumns[$fieldName]))
                            {
                                $columnDefinition = $candidates->extraFields->getDataGridDefinition($index, $data, $this->_db);
                
                                /* Return false for extra fields that should not be columns. */
                                if ($columnDefinition !== false)
                                {
                                    $this->_classColumns[$fieldName] = $columnDefinition;
                                }
                            }
                        }
        
            
        }
        else if($_REQUEST['a']=='CareerPage' || $_REQUEST['delrec']=='CareerPage') {
            
            $this->_assignedCriterion .= " and (candidate.source= 'Career Page' or candidate.source= 'Online Careers Website') ";
            $this->_dataItemIDColumn = 'candidate.candidate_id';
                        
                         
            
                                $unsubscribe_date1 = mysql_query("Select candidate_id, unsubscribe_date from candidate where unsubscribe_date != '0000-00-00 00:00:00'");
                while ($row = mysql_fetch_array($unsubscribe_date1))
                {
                    $unsubscribe_date = $row['unsubscribe_date'];
                    $candidate_id = $row['candidate_id'];
                    $current_date = date('Y-m-d H:i:s');
                    $diff = strtotime($current_date) - strtotime($unsubscribe_date);
                    $months = floor(floatval($diff) / (60 * 60 * 24 * 365 / 12));
                    if($months == '6')
                    {
                        $update_unsubscribe = "delete from candidate where candidate_id = '$candidate_id' ";
                        $result = mysql_query($update_unsubscribe);
                    }    
                }    
        
        
        

                                $this->_classColumns = array(
                'Attachments' => array('select' => 'IF(candidate_joborder_submitted.candidate_joborder_id, 1, 0) AS submitted,
                                                    IF(attachment_id, 1, 0) AS attachmentPresent',
    
                                         'pagerRender' => 'if ($rsData[\'submitted\'] == 1)
                                                        {
                                                            $return = \'<img src="images/job_orders.gif" alt="" width="16" height="16" title="Submitted for a Job Order" />\';
                                                        }
                                                        else
                                                        {
                                                            $return = \'<img src="images/mru/blank.gif" alt="" width="16" height="16" />\';
                                                        }
    
                                                        if ($rsData[\'attachmentPresent\'] == 1)
                                                        {
                                                            $return .= \'<img src="images/paperclip.gif" alt="" width="16" height="16" title="Attachment Present" />\';
                                                        }
                                                        else
                                                        {
                                                            $return .= \'<img src="images/mru/blank.gif" alt="" width="16" height="16" />\';
                                                        }
    
                                                        return $return;
                                                       ',
    
                                         'join'     => 'LEFT JOIN attachment
                                                            ON candidate.candidate_id = attachment.data_item_id
                                                                                                                    AND attachment.data_item_type = '.DATA_ITEM_CANDIDATE.'
                                                        LEFT JOIN candidate_joborder AS candidate_joborder_submitted
                                                            ON candidate_joborder_submitted.candidate_id = candidate.candidate_id
                                                            AND candidate_joborder_submitted.status >= '.PIPELINE_STATUS_SUBMITTED.'
                                                            AND candidate_joborder_submitted.site_id = '.$this->_siteID.'
                                                            AND candidate_joborder_submitted.status != '.PIPELINE_STATUS_NOTINCONSIDERATION,
                                         'pagerWidth'    => 34,
                                         'pagerOptional' => true,
                                         'pagerNoTitle' => true,
                                         'sizable'  => false,
                                         'exportable' => false,
                                         'filterable' => false),
    
                'First Name' =>     array('select'         => 'candidate.first_name AS firstName',
                                          'pagerRender'    => 'if ($rsData[\'isHot\'] == 1) $className =  \'jobLinkHot\'; else $className = \'jobLinkCold\'; return \'<a href="'.RESUFLOUtility::getIndexName().'?m=candidates&amp;list='.$this->get['a'].'&amp;a=show&amp;candidateID=\'.$rsData[\'candidateID\'].\'&amp;view=1" class="\'.$className.\'">\'.htmlspecialchars($rsData[\'firstName\']).\'</a>\';',
                                          'sortableColumn' => 'firstName',
                                          'pagerWidth'     => 75,
                                          'pagerOptional'  => false,
                                          'alphaNavigation'=> true,
                                          'filter'         => 'candidate.first_name'),
    
                'Last Name' =>      array('select'         => 'candidate.last_name AS lastName',
                                         'sortableColumn'  => 'lastName',
                                         'pagerRender'     => 'if ($rsData[\'isHot\'] == 1) $className =  \'jobLinkHot\'; else $className = \'jobLinkCold\'; return \'<a href="'.RESUFLOUtility::getIndexName().'?m=candidates&amp;list='.$this->get['a'].'&amp;a=show&amp;candidateID=\'.$rsData[\'candidateID\'].\'&amp;view=1" class="\'.$className.\'">\'.htmlspecialchars($rsData[\'lastName\']).\'</a>\';',
                                         'pagerWidth'      => 85,
                                         'pagerOptional'   => false,
                                         'alphaNavigation' => true,
                                         'filter'         => 'candidate.last_name'),
    
                'E-Mail' =>         array('select'   => 'candidate.email1 AS email1',
                                         'sortableColumn'     => 'email1',
                                         'pagerWidth'    => 80,
                                         'filter'         => 'candidate.email1'),
    
                '2nd E-Mail' =>     array('select'   => 'candidate.email2 AS email2',
                                         'sortableColumn'     => 'email2',
                                         'pagerWidth'    => 80,
                                         'filter'         => 'candidate.email2'),
    
                'Home Phone' =>     array('select'   => 'candidate.phone_home AS phoneHome',
                                         'sortableColumn'     => 'phoneHome',
                                         'pagerWidth'    => 80,
                                         'filter'         => 'candidate.phone_home'),
    
                'Cell Phone' =>     array('select'   => 'candidate.phone_cell AS phoneCell',
                                         'sortableColumn'     => 'phoneCell',
                                         'pagerWidth'    => 80,
                                         'filter'         => 'candidate.phone_cell'),
    
                'Work Phone' =>     array('select'   => 'candidate.phone_work AS phoneWork',
                                         'sortableColumn'     => 'phoneWork',
                                         'pagerWidth'    => 80),
    
                'Address' =>        array('select'   => 'candidate.address AS address',
                                         'sortableColumn'     => 'address',
                                         'pagerWidth'    => 250,
                                         'alphaNavigation' => true,
                                         'filter'         => 'candidate.address'),
    
                'City' =>           array('select'   => 'candidate.city AS city',
                                         'sortableColumn'     => 'city',
                                         'pagerWidth'    => 80,
                                         'alphaNavigation' => true,
                                         'filter'         => 'candidate.city'),
                              'Company' =>           array('select'   => 'Case When candidate.entered_by = '.$_SESSION['RESUFLO']->getUserId().' then "'.$_SESSION['RESUFLO']->getFirstName().'" else (select first_name from user where user_id =(select entered_by from user where user_id=candidate.entered_by) ) End as company',
                                         'sortableColumn'     => 'company',
                                         'pagerWidth'    => 80,
                                         'alphaNavigation' => true,
                                         'filter'         => 'candidate.entered_by'),
    
    
                'State' =>          array('select'   => 'candidate.state AS state',
                                         'sortableColumn'     => 'state',
                                         'filterType' => 'dropDown',
                                         'pagerWidth'    => 50,
                                         'alphaNavigation' => true,
                                         'filter'         => 'candidate.state'),
    
                'Zip' =>            array('select'  => 'candidate.zip AS zip',
                                         'sortableColumn'    => 'zip',
                                         'pagerWidth'   => 50,
                                         'filter'         => 'candidate.zip'),
    
                'Misc Notes' =>     array('select'  => 'candidate.notes AS notes',
                                         'sortableColumn'    => 'notes',
                                         'pagerWidth'   => 300,
                                         'filter'         => 'candidate.notes'),
    
                'Web Site' =>      array('select'  => 'candidate.web_site AS webSite',
                                         'pagerRender'     => 'return \'<a href="\'.htmlspecialchars($rsData[\'webSite\']).\'">\'.htmlspecialchars($rsData[\'webSite\']).\'</a>\';',
                                         'sortableColumn'    => 'webSite',
                                         'pagerWidth'   => 80,
                                         'filter'         => 'candidate.web_site'),
    
                'Key Skills' =>    array('select'  => 'candidate.key_skills AS keySkills',
                                         'pagerRender' => 'return substr(trim($rsData[\'keySkills\']), 0, 30) . (strlen(trim($rsData[\'keySkills\'])) > 30 ? \'...\' : \'\');',
                                         'sortableColumn'    => 'keySkills',
                                         'pagerWidth'   => 210,
                                         'filter'         => 'candidate.key_skills'),
    
                'Recent Status' => array('select'  => '(
                                                        SELECT
                                                            CONCAT(
                                                                \'<a href="'.RESUFLOUtility::getIndexName().'?m=joborders&amp;list='.$this->get['a'].'&amp;a=show&amp;jobOrderID=\',
                                                                joborder.joborder_id,
                                                                \'" title="\',
                                                                joborder.title,
                                                                \' (\',
                                                                company.name,
                                                                \')">\',
                                                                candidate_joborder_status.short_description,
                                                                \'</a>\'
                                                            )
                                                        FROM
                                                            candidate_joborder
                                                        LEFT JOIN candidate_joborder_status
                                                            ON candidate_joborder_status.candidate_joborder_status_id = candidate_joborder.status
                                                        LEFT JOIN joborder
                                                            ON joborder.joborder_id = candidate_joborder.joborder_id
                                                        LEFT JOIN company
                                                            ON joborder.company_id = company.company_id
                                                        WHERE
                                                            candidate_joborder.candidate_id = candidate.candidate_id
                                                        ORDER BY
                                                            candidate_joborder.date_modified DESC
                                                        LIMIT 1
                                                    ) AS lastStatus
                                                    ',
                                         'sort'    => 'lastStatus',
                                         'pagerRender'     => 'return $rsData[\'lastStatus\'];',
                                         'exportRender'     => 'return $rsData[\'lastStatus\'];',
                                         'pagerWidth'   => 140,
                                         'exportable' => false,
                                         'filterHaving'  => 'lastStatus',
                                         'filterTypes'   => '=~'),
                                         
                'Activity Notes' =>     array('select'  => 'candidate.activity_notes AS activity_notes',
                                                     'sortableColumn'    => 'activity_notes',
                                                     'pagerWidth'   => 300,
                                                     'filter'         => 'candidate.activity_notes'),
                                     
                'Recent Status (Extended)' => array('select'  => '(
                                                        SELECT
                                                            CONCAT(
                                                                candidate_joborder_status.short_description,
                                                                \'<br />\',
                                                                \'<a href="'.RESUFLOUtility::getIndexName().'?m=companies&amp;list='.$this->get['a'].'&amp;a=show&amp;companyID=\',
                                                                company.company_id,
                                                                \'">\',
                                                                company.name,
                                                                \'</a> - \',
                                                                \'<a href="'.RESUFLOUtility::getIndexName().'?m=joborders&amp;list='.$this->get['a'].'&amp;a=show&amp;jobOrderID=\',
                                                                joborder.joborder_id,
                                                                \'">\',
                                                                joborder.title,
                                                                \'</a>\'
                                                            )
                                                        FROM
                                                            candidate_joborder
                                                        LEFT JOIN candidate_joborder_status
                                                            ON candidate_joborder_status.candidate_joborder_status_id = candidate_joborder.status
                                                        LEFT JOIN joborder
                                                            ON joborder.joborder_id = candidate_joborder.joborder_id
                                                        LEFT JOIN company
                                                            ON joborder.company_id = company.company_id
                                                        WHERE
                                                            candidate_joborder.candidate_id = candidate.candidate_id
                                                        ORDER BY
                                                            candidate_joborder.date_modified DESC
                                                        LIMIT 1
                                                    ) AS lastStatusLong
                                                    ',
                                         'sortableColumn'    => 'lastStatusLong',
                                         'pagerRender'     => 'return $rsData[\'lastStatusLong\'];',
                                         'pagerWidth'   => 310,
                                         'exportable' => false,
                                         'filterable' => false),
    
                'Source' =>        array('select'  => 'candidate.source AS source',
                                         'sortableColumn'    => 'source',
                                         'pagerWidth'   => 140,
                                         'alphaNavigation' => true,
                                         'filter'         => 'candidate.source'),
    
                'Available' =>     array('select'   => 'DATE_FORMAT(candidate.date_available, \'%m-%d-%y\') AS dateAvailable',
                                         'sortableColumn'     => 'dateAvailable',
                                         'pagerWidth'    => 60),
    
                'Current Employer' => array('select'  => 'candidate.current_employer AS currentEmployer',
                                         'sortableColumn'    => 'currentEmployer',
                                         'pagerWidth'   => 125,
                                         'alphaNavigation' => true,
                                         'filter'         => 'candidate.current_employer'),
    
                'Current Pay' => array('select'  => 'candidate.current_pay AS currentPay',
                                         'sortableColumn'    => 'currentPay',
                                         'pagerWidth'   => 125,
                                         'filter'         => 'candidate.current_pay',
                                         'filterTypes'   => '===>=<'),
    
                'Desired Pay' => array('select'  => 'candidate.desired_pay AS desiredPay',
                                         'sortableColumn'    => 'desiredPay',
                                         'pagerWidth'   => 125,
                                         'filter'         => 'candidate.desired_pay',
                                         'filterTypes'   => '===>=<'),
    
                'Can Relocate'  => array('select'  => 'candidate.can_relocate AS canRelocate',
                                         'pagerRender'     => 'return ($rsData[\'canRelocate\'] == 0 ? \'No\' : \'Yes\');',
                                         'exportRender'     => 'return ($rsData[\'canRelocate\'] == 0 ? \'No\' : \'Yes\');',
                                         'sortableColumn'    => 'canRelocate',
                                         'pagerWidth'   => 80,
                                         'filter'         => 'candidate.can_relocate'),
    
                'Recruiter' =>         array('select'   => 'owner_user.first_name AS ownerFirstName,' .
                                                       'owner_user.last_name AS ownerLastName,' .
                                                       'CONCAT(owner_user.last_name, owner_user.first_name) AS ownerSort',
                                         'join'     => 'LEFT JOIN user AS owner_user ON candidate.owner = owner_user.user_id',
                                         'pagerRender'      => 'return StringUtility::makeInitialName($rsData[\'ownerFirstName\'], $rsData[\'ownerLastName\'], false, LAST_NAME_MAXLEN);',
                                         'exportRender'     => 'return $rsData[\'ownerFirstName\'] . " " .$rsData[\'ownerLastName\'];',
                                         'sortableColumn'     => 'ownerSort',
                                         'pagerWidth'    => 75,
                                         'alphaNavigation' => true,
                                         'filter'         => 'CONCAT(owner_user.first_name, owner_user.last_name)'),
    
                'Created' =>       array('select'   => 'DATE_FORMAT(candidate.date_created, \'%m-%d-%y\') AS dateCreated',
                                         'pagerRender'      => 'return $rsData[\'dateCreated\'];',
                                         'sortableColumn'     => 'dateCreatedSort',
                                         'pagerWidth'    => 60,
                                         'filterHaving' => 'DATE_FORMAT(candidate.date_created, \'%m-%d-%y\')'),
                                    
                'Date From' =>       array('select'   => 'DATE_FORMAT(candidate.date_created, \'%m-%d-%y\') AS dateCreated',
                                        'pagerRender'      => 'return $rsData[\'dateCreated\'];',
                                        'sortableColumn'     => 'dateCreatedSort',
                                        'pagerWidth'    => 60,
                                        'filterHaving' => 'DATE_FORMAT(candidate.date_created, \'%y-%m-%d\')',
                                        'filterTypes'   => '=>'),
                
                'Date To' =>       array('select'   => 'DATE_FORMAT(candidate.date_created, \'%m-%d-%y\') AS dateTo',
                                         'pagerRender'      => 'return $rsData[\'dateTo\'];',
                                         'sortableColumn'     => 'dateToSort',
                                         'pagerWidth'    => 60,
                                         'filterHaving' => 'DATE_FORMAT(candidate.date_created, \'%y-%m-%d\')',
                                         'filterTypes'   => '=<'),
    
                'Modified' =>      array('select'   => 'DATE_FORMAT(candidate.date_modified, \'%m-%d-%y\') AS dateModified',
                                         'pagerRender'      => 'return $rsData[\'dateModified\'];',
                                         'sortableColumn'     => 'dateModifiedSort',
                                         'pagerWidth'    => 60,
                                         'pagerOptional' => false,
                                         'filterHaving' => 'DATE_FORMAT(candidate.date_modified, \'%m-%d-%y\')'),
                                    
                'Scheduled' =>      array('select'   => 'DATE_FORMAT(candidate.schedule_date, \'%m-%d-%y %r\') AS dateScheduled',
                                         'pagerRender'      => 'return $rsData[\'dateScheduled\'];',
                                         'sortableColumn'     => 'dateScheduledSort',
                                         'pagerWidth'    => 150,
                                         'pagerOptional' => true,
                                         'filterHaving' => 'DATE_FORMAT(candidate.schedule_date, \'%m-%d-%y %r\')'),
    
                /* This one only works when called from the saved list view.  Thats why it is not optional, filterable, or exportable.
                 * FIXME:  Somehow make this defined in the associated savedListDataGrid class child.
                 */
                'Added To List' =>  array('select'   => 'DATE_FORMAT(saved_list_entry.date_created, \'%m-%d-%y\') AS dateAddedToList,
                                                         saved_list_entry.date_created AS dateAddedToListSort',
                                         'pagerRender'      => 'return $rsData[\'dateAddedToList\'];',
                                         'sortableColumn'     => 'dateAddedToListSort',
                                         'pagerWidth'    => 60,
                                         'pagerOptional' => false,
                                         'filterable' => false,
                                         'exportable' => false),
    
                'OwnerID' =>       array('select'    => '',
                                         'filter'    => 'candidate.owner',
                                         'pagerOptional' => false,
                                         'filterable' => false,
                                         'filterDescription' => 'Only My Candidates'),
    
                'IsHot' =>         array('select'    => '',
                                         'filter'    => 'candidate.is_hot',
                                         'pagerOptional' => false,
                                         'filterable' => false,
                                         'filterDescription' => 'Only Hot Candidates'),
                            'Campaign' => $GLOBALS['sheesh']
            );
    
            if (US_ZIPS_ENABLED)
            {
                $this->_classColumns['Near Zipcode'] =
                                   array('select'  => 'candidate.zip AS zip',
                                         'filter' => 'candidate.zip',
                                         'pagerOptional' => false,
                                         'filterTypes'   => '=@');
            }
    
            /* Extra fields get added as columns here. */
            $candidates = new Candidates($this->_siteID);
            $extraFieldsRS = $candidates->extraFields->getSettings();
            foreach ($extraFieldsRS as $index => $data)
            {
                $fieldName = $data['fieldName'];
    
                if (!isset($this->_classColumns[$fieldName]))
                {
                    $columnDefinition = $candidates->extraFields->getDataGridDefinition($index, $data, $this->_db);
    
                    /* Return false for extra fields that should not be columns. */
                    if ($columnDefinition !== false)
                    {
                        $this->_classColumns[$fieldName] = $columnDefinition;
                    }
                }
            }
        
        }
        else if($_REQUEST['a']=='Inactive' || $_REQUEST['delrec']=='Inactive') {
            
            $this->_assignedCriterion .= " and candidate.unsubscribe = '1' and unsubscribe_date!='0000-00-00 00:00:00' and follow_up = '0' and candidate.list_id = '0' ";
            $this->_dataItemIDColumn = 'candidate.candidate_id';
                        
                         
            
                                $unsubscribe_date1 = mysql_query("Select candidate_id, unsubscribe_date from candidate where unsubscribe_date != '0000-00-00 00:00:00'");
                while ($row = mysql_fetch_array($unsubscribe_date1))
                {
                    $unsubscribe_date = $row['unsubscribe_date'];
                    $candidate_id = $row['candidate_id'];
                    $current_date = date('Y-m-d H:i:s');
                    $diff = strtotime($current_date) - strtotime($unsubscribe_date);
                    $months = floor(floatval($diff) / (60 * 60 * 24 * 365 / 12));
                    if($months == '6')
                    {
                        $update_unsubscribe = "delete from candidate where candidate_id = '$candidate_id' ";
                        $result = mysql_query($update_unsubscribe);
                    }    
                }    
        
        
        

                                $this->_classColumns = array(
                'Attachments' => array('select' => 'IF(candidate_joborder_submitted.candidate_joborder_id, 1, 0) AS submitted,
                                                    IF(attachment_id, 1, 0) AS attachmentPresent',
    
                                         'pagerRender' => 'if ($rsData[\'submitted\'] == 1)
                                                        {
                                                            $return = \'<img src="images/job_orders.gif" alt="" width="16" height="16" title="Submitted for a Job Order" />\';
                                                        }
                                                        else
                                                        {
                                                            $return = \'<img src="images/mru/blank.gif" alt="" width="16" height="16" />\';
                                                        }
    
                                                        if ($rsData[\'attachmentPresent\'] == 1)
                                                        {
                                                            $return .= \'<img src="images/paperclip.gif" alt="" width="16" height="16" title="Attachment Present" />\';
                                                        }
                                                        else
                                                        {
                                                            $return .= \'<img src="images/mru/blank.gif" alt="" width="16" height="16" />\';
                                                        }
    
                                                        return $return;
                                                       ',
    
                                         'join'     => 'LEFT JOIN attachment
                                                            ON candidate.candidate_id = attachment.data_item_id
                                                                                                                    AND attachment.data_item_type = '.DATA_ITEM_CANDIDATE.'
                                                        LEFT JOIN candidate_joborder AS candidate_joborder_submitted
                                                            ON candidate_joborder_submitted.candidate_id = candidate.candidate_id
                                                            AND candidate_joborder_submitted.status >= '.PIPELINE_STATUS_SUBMITTED.'
                                                            AND candidate_joborder_submitted.site_id = '.$this->_siteID.'
                                                            AND candidate_joborder_submitted.status != '.PIPELINE_STATUS_NOTINCONSIDERATION,
                                         'pagerWidth'    => 34,
                                         'pagerOptional' => true,
                                         'pagerNoTitle' => true,
                                         'sizable'  => false,
                                         'exportable' => false,
                                         'filterable' => false),
    
                'First Name' =>     array('select'         => 'candidate.first_name AS firstName',
                                          'pagerRender'    => 'if ($rsData[\'isHot\'] == 1) $className =  \'jobLinkHot\'; else $className = \'jobLinkCold\'; return \'<a href="'.RESUFLOUtility::getIndexName().'?m=candidates&amp;list='.$this->get['a'].'&amp;a=show&amp;candidateID=\'.$rsData[\'candidateID\'].\'&amp;view=1" class="\'.$className.\'">\'.htmlspecialchars($rsData[\'firstName\']).\'</a>\';',
                                          'sortableColumn' => 'firstName',
                                          'pagerWidth'     => 75,
                                          'pagerOptional'  => false,
                                          'alphaNavigation'=> true,
                                          'filter'         => 'candidate.first_name'),
    
                'Last Name' =>      array('select'         => 'candidate.last_name AS lastName',
                                         'sortableColumn'  => 'lastName',
                                         'pagerRender'     => 'if ($rsData[\'isHot\'] == 1) $className =  \'jobLinkHot\'; else $className = \'jobLinkCold\'; return \'<a href="'.RESUFLOUtility::getIndexName().'?m=candidates&amp;list='.$this->get['a'].'&amp;a=show&amp;candidateID=\'.$rsData[\'candidateID\'].\'&amp;view=1" class="\'.$className.\'">\'.htmlspecialchars($rsData[\'lastName\']).\'</a>\';',
                                         'pagerWidth'      => 85,
                                         'pagerOptional'   => false,
                                         'alphaNavigation' => true,
                                         'filter'         => 'candidate.last_name'),
    
                'E-Mail' =>         array('select'   => 'candidate.email1 AS email1',
                                         'sortableColumn'     => 'email1',
                                         'pagerWidth'    => 80,
                                         'filter'         => 'candidate.email1'),
    
                '2nd E-Mail' =>     array('select'   => 'candidate.email2 AS email2',
                                         'sortableColumn'     => 'email2',
                                         'pagerWidth'    => 80,
                                         'filter'         => 'candidate.email2'),
    
                'Home Phone' =>     array('select'   => 'candidate.phone_home AS phoneHome',
                                         'sortableColumn'     => 'phoneHome',
                                         'pagerWidth'    => 80,
                                         'filter'         => 'candidate.phone_home'),
    
                'Cell Phone' =>     array('select'   => 'candidate.phone_cell AS phoneCell',
                                         'sortableColumn'     => 'phoneCell',
                                         'pagerWidth'    => 80,
                                         'filter'         => 'candidate.phone_cell'),
    
                'Work Phone' =>     array('select'   => 'candidate.phone_work AS phoneWork',
                                         'sortableColumn'     => 'phoneWork',
                                         'pagerWidth'    => 80),
    
                'Address' =>        array('select'   => 'candidate.address AS address',
                                         'sortableColumn'     => 'address',
                                         'pagerWidth'    => 250,
                                         'alphaNavigation' => true,
                                         'filter'         => 'candidate.address'),
    
                'City' =>           array('select'   => 'candidate.city AS city',
                                         'sortableColumn'     => 'city',
                                         'pagerWidth'    => 80,
                                         'alphaNavigation' => true,
                                         'filter'         => 'candidate.city'),
                              'Company' =>           array('select'   => 'Case When candidate.entered_by = '.$_SESSION['RESUFLO']->getUserId().' then "'.$_SESSION['RESUFLO']->getFirstName().'" else (select first_name from user where user_id =(select entered_by from user where user_id=candidate.entered_by) ) End as company',
                                         'sortableColumn'     => 'company',
                                         'pagerWidth'    => 80,
                                         'alphaNavigation' => true,
                                         'filter'         => 'candidate.entered_by'),
    
    
                'State' =>          array('select'   => 'candidate.state AS state',
                                         'sortableColumn'     => 'state',
                                         'filterType' => 'dropDown',
                                         'pagerWidth'    => 50,
                                         'alphaNavigation' => true,
                                         'filter'         => 'candidate.state'),
    
                'Zip' =>            array('select'  => 'candidate.zip AS zip',
                                         'sortableColumn'    => 'zip',
                                         'pagerWidth'   => 50,
                                         'filter'         => 'candidate.zip'),
    
                'Misc Notes' =>     array('select'  => 'candidate.notes AS notes',
                                         'sortableColumn'    => 'notes',
                                         'pagerWidth'   => 300,
                                         'filter'         => 'candidate.notes'),
    
                'Web Site' =>      array('select'  => 'candidate.web_site AS webSite',
                                         'pagerRender'     => 'return \'<a href="\'.htmlspecialchars($rsData[\'webSite\']).\'">\'.htmlspecialchars($rsData[\'webSite\']).\'</a>\';',
                                         'sortableColumn'    => 'webSite',
                                         'pagerWidth'   => 80,
                                         'filter'         => 'candidate.web_site'),
    
                'Key Skills' =>    array('select'  => 'candidate.key_skills AS keySkills',
                                         'pagerRender' => 'return substr(trim($rsData[\'keySkills\']), 0, 30) . (strlen(trim($rsData[\'keySkills\'])) > 30 ? \'...\' : \'\');',
                                         'sortableColumn'    => 'keySkills',
                                         'pagerWidth'   => 210,
                                         'filter'         => 'candidate.key_skills'),
    
                'Recent Status' => array('select'  => '(
                                                        SELECT
                                                            CONCAT(
                                                                \'<a href="'.RESUFLOUtility::getIndexName().'?m=joborders&amp;list='.$this->get['a'].'&amp;a=show&amp;jobOrderID=\',
                                                                joborder.joborder_id,
                                                                \'" title="\',
                                                                joborder.title,
                                                                \' (\',
                                                                company.name,
                                                                \')">\',
                                                                candidate_joborder_status.short_description,
                                                                \'</a>\'
                                                            )
                                                        FROM
                                                            candidate_joborder
                                                        LEFT JOIN candidate_joborder_status
                                                            ON candidate_joborder_status.candidate_joborder_status_id = candidate_joborder.status
                                                        LEFT JOIN joborder
                                                            ON joborder.joborder_id = candidate_joborder.joborder_id
                                                        LEFT JOIN company
                                                            ON joborder.company_id = company.company_id
                                                        WHERE
                                                            candidate_joborder.candidate_id = candidate.candidate_id
                                                        ORDER BY
                                                            candidate_joborder.date_modified DESC
                                                        LIMIT 1
                                                    ) AS lastStatus
                                                    ',
                                         'sort'    => 'lastStatus',
                                         'pagerRender'     => 'return $rsData[\'lastStatus\'];',
                                         'exportRender'     => 'return $rsData[\'lastStatus\'];',
                                         'pagerWidth'   => 140,
                                         'exportable' => false,
                                         'filterHaving'  => 'lastStatus',
                                         'filterTypes'   => '=~'),
                                         
                'Activity Notes' =>     array('select'  => 'candidate.activity_notes AS activity_notes',
                                                     'sortableColumn'    => 'activity_notes',
                                                     'pagerWidth'   => 300,
                                                     'filter'         => 'candidate.activity_notes'),
                                     
                'Recent Status (Extended)' => array('select'  => '(
                                                        SELECT
                                                            CONCAT(
                                                                candidate_joborder_status.short_description,
                                                                \'<br />\',
                                                                \'<a href="'.RESUFLOUtility::getIndexName().'?m=companies&amp;list='.$this->get['a'].'&amp;a=show&amp;companyID=\',
                                                                company.company_id,
                                                                \'">\',
                                                                company.name,
                                                                \'</a> - \',
                                                                \'<a href="'.RESUFLOUtility::getIndexName().'?m=joborders&amp;list='.$this->get['a'].'&amp;a=show&amp;jobOrderID=\',
                                                                joborder.joborder_id,
                                                                \'">\',
                                                                joborder.title,
                                                                \'</a>\'
                                                            )
                                                        FROM
                                                            candidate_joborder
                                                        LEFT JOIN candidate_joborder_status
                                                            ON candidate_joborder_status.candidate_joborder_status_id = candidate_joborder.status
                                                        LEFT JOIN joborder
                                                            ON joborder.joborder_id = candidate_joborder.joborder_id
                                                        LEFT JOIN company
                                                            ON joborder.company_id = company.company_id
                                                        WHERE
                                                            candidate_joborder.candidate_id = candidate.candidate_id
                                                        ORDER BY
                                                            candidate_joborder.date_modified DESC
                                                        LIMIT 1
                                                    ) AS lastStatusLong
                                                    ',
                                         'sortableColumn'    => 'lastStatusLong',
                                         'pagerRender'     => 'return $rsData[\'lastStatusLong\'];',
                                         'pagerWidth'   => 310,
                                         'exportable' => false,
                                         'filterable' => false),
    
                'Source' =>        array('select'  => 'candidate.source AS source',
                                         'sortableColumn'    => 'source',
                                         'pagerWidth'   => 140,
                                         'alphaNavigation' => true,
                                         'filter'         => 'candidate.source'),
    
                'Available' =>     array('select'   => 'DATE_FORMAT(candidate.date_available, \'%m-%d-%y\') AS dateAvailable',
                                         'sortableColumn'     => 'dateAvailable',
                                         'pagerWidth'    => 60),
    
                'Current Employer' => array('select'  => 'candidate.current_employer AS currentEmployer',
                                         'sortableColumn'    => 'currentEmployer',
                                         'pagerWidth'   => 125,
                                         'alphaNavigation' => true,
                                         'filter'         => 'candidate.current_employer'),
    
                'Current Pay' => array('select'  => 'candidate.current_pay AS currentPay',
                                         'sortableColumn'    => 'currentPay',
                                         'pagerWidth'   => 125,
                                         'filter'         => 'candidate.current_pay',
                                         'filterTypes'   => '===>=<'),
    
                'Desired Pay' => array('select'  => 'candidate.desired_pay AS desiredPay',
                                         'sortableColumn'    => 'desiredPay',
                                         'pagerWidth'   => 125,
                                         'filter'         => 'candidate.desired_pay',
                                         'filterTypes'   => '===>=<'),
    
                'Can Relocate'  => array('select'  => 'candidate.can_relocate AS canRelocate',
                                         'pagerRender'     => 'return ($rsData[\'canRelocate\'] == 0 ? \'No\' : \'Yes\');',
                                         'exportRender'     => 'return ($rsData[\'canRelocate\'] == 0 ? \'No\' : \'Yes\');',
                                         'sortableColumn'    => 'canRelocate',
                                         'pagerWidth'   => 80,
                                         'filter'         => 'candidate.can_relocate'),
    
                'Recruiter' =>         array('select'   => 'owner_user.first_name AS ownerFirstName,' .
                                                       'owner_user.last_name AS ownerLastName,' .
                                                       'CONCAT(owner_user.last_name, owner_user.first_name) AS ownerSort',
                                         'join'     => 'LEFT JOIN user AS owner_user ON candidate.owner = owner_user.user_id',
                                         'pagerRender'      => 'return StringUtility::makeInitialName($rsData[\'ownerFirstName\'], $rsData[\'ownerLastName\'], false, LAST_NAME_MAXLEN);',
                                         'exportRender'     => 'return $rsData[\'ownerFirstName\'] . " " .$rsData[\'ownerLastName\'];',
                                         'sortableColumn'     => 'ownerSort',
                                         'pagerWidth'    => 75,
                                         'alphaNavigation' => true,
                                         'filter'         => 'CONCAT(owner_user.first_name, owner_user.last_name)'),
    
                'Created' =>       array('select'   => 'DATE_FORMAT(candidate.date_created, \'%m-%d-%y\') AS dateCreated',
                                         'pagerRender'      => 'return $rsData[\'dateCreated\'];',
                                         'sortableColumn'     => 'dateCreatedSort',
                                         'pagerWidth'    => 60,
                                         'filterHaving' => 'DATE_FORMAT(candidate.date_created, \'%m-%d-%y\')'),
                
                'Date From' =>       array('select'   => 'DATE_FORMAT(candidate.date_created, \'%m-%d-%y\') AS dateCreated',
                                        'pagerRender'      => 'return $rsData[\'dateCreated\'];',
                                        'sortableColumn'     => 'dateCreatedSort',
                                        'pagerWidth'    => 60,
                                        'filterHaving' => 'DATE_FORMAT(candidate.date_created, \'%y-%m-%d\')',
                                        'filterTypes'   => '=>'),
                
                'Date To' =>       array('select'   => 'DATE_FORMAT(candidate.date_created, \'%m-%d-%y\') AS dateTo',
                                         'pagerRender'      => 'return $rsData[\'dateTo\'];',
                                         'sortableColumn'     => 'dateToSort',
                                         'pagerWidth'    => 60,
                                         'filterHaving' => 'DATE_FORMAT(candidate.date_created, \'%y-%m-%d\')',
                                         'filterTypes'   => '=<'),
    
                'Modified' =>      array('select'   => 'DATE_FORMAT(candidate.date_modified, \'%m-%d-%y\') AS dateModified',
                                         'pagerRender'      => 'return $rsData[\'dateModified\'];',
                                         'sortableColumn'     => 'dateModifiedSort',
                                         'pagerWidth'    => 60,
                                         'pagerOptional' => false,
                                         'filterHaving' => 'DATE_FORMAT(candidate.date_modified, \'%m-%d-%y\')'),
                                    
                'Scheduled' =>      array('select'   => 'DATE_FORMAT(candidate.schedule_date, \'%m-%d-%y %r\') AS dateScheduled',
                     'pagerRender'      => 'return $rsData[\'dateScheduled\'];',
                     'sortableColumn'     => 'dateScheduledSort',
                     'pagerWidth'    => 150,
                     'pagerOptional' => true,
                     'filterHaving' => 'DATE_FORMAT(candidate.schedule_date, \'%m-%d-%y %r\')'),
    
                /* This one only works when called from the saved list view.  Thats why it is not optional, filterable, or exportable.
                 * FIXME:  Somehow make this defined in the associated savedListDataGrid class child.
                 */
                'Added To List' =>  array('select'   => 'DATE_FORMAT(saved_list_entry.date_created, \'%m-%d-%y\') AS dateAddedToList,
                                                         saved_list_entry.date_created AS dateAddedToListSort',
                                         'pagerRender'      => 'return $rsData[\'dateAddedToList\'];',
                                         'sortableColumn'     => 'dateAddedToListSort',
                                         'pagerWidth'    => 60,
                                         'pagerOptional' => false,
                                         'filterable' => false,
                                         'exportable' => false),
    
                'OwnerID' =>       array('select'    => '',
                                         'filter'    => 'candidate.owner',
                                         'pagerOptional' => false,
                                         'filterable' => false,
                                         'filterDescription' => 'Only My Candidates'),
    
                'IsHot' =>         array('select'    => '',
                                         'filter'    => 'candidate.is_hot',
                                         'pagerOptional' => false,
                                         'filterable' => false,
                                         'filterDescription' => 'Only Hot Candidates'),
                            'Campaign' => $GLOBALS['sheesh']
            );
    
            if (US_ZIPS_ENABLED)
            {
                $this->_classColumns['Near Zipcode'] =
                                   array('select'  => 'candidate.zip AS zip',
                                         'filter' => 'candidate.zip',
                                         'pagerOptional' => false,
                                         'filterTypes'   => '=@');
            }
    
            /* Extra fields get added as columns here. */
            $candidates = new Candidates($this->_siteID);
            $extraFieldsRS = $candidates->extraFields->getSettings();
            foreach ($extraFieldsRS as $index => $data)
            {
                $fieldName = $data['fieldName'];
    
                if (!isset($this->_classColumns[$fieldName]))
                {
                    $columnDefinition = $candidates->extraFields->getDataGridDefinition($index, $data, $this->_db);
    
                    /* Return false for extra fields that should not be columns. */
                    if ($columnDefinition !== false)
                    {
                        $this->_classColumns[$fieldName] = $columnDefinition;
                    }
                }
            }
        
        }
        else if($_REQUEST['a']=='LinkedIn') {
            $this->_assignedCriterion .= " and (candidate.source= 'LinkedIn') ";
            $this->_dataItemIDColumn = 'candidate.candidate_id';
                        
            $unsubscribe_date1 = mysql_query("Select candidate_id, unsubscribe_date from candidate where unsubscribe_date != '0000-00-00 00:00:00'");
            while ($row = mysql_fetch_array($unsubscribe_date1))
            {
                $unsubscribe_date = $row['unsubscribe_date'];
                $candidate_id = $row['candidate_id'];
                $current_date = date('Y-m-d H:i:s');
                $diff = strtotime($current_date) - strtotime($unsubscribe_date);
                $months = floor(floatval($diff) / (60 * 60 * 24 * 365 / 12));
                if($months == '6')
                {
                    $update_unsubscribe = "delete from candidate where candidate_id = '$candidate_id' ";
                    $result = mysql_query($update_unsubscribe);
                }    
            }    

            $this->_classColumns = array(
                'Attachments' => array('select' => 'IF(candidate_joborder_submitted.candidate_joborder_id, 1, 0) AS submitted,
                                                    IF(attachment_id, 1, 0) AS attachmentPresent',
    
                                         'pagerRender' => 'if ($rsData[\'submitted\'] == 1)
                                                        {
                                                            $return = \'<img src="images/job_orders.gif" alt="" width="16" height="16" title="Submitted for a Job Order" />\';
                                                        }
                                                        else
                                                        {
                                                            $return = \'<img src="images/mru/blank.gif" alt="" width="16" height="16" />\';
                                                        }
    
                                                        if ($rsData[\'attachmentPresent\'] == 1)
                                                        {
                                                            $return .= \'<img src="images/paperclip.gif" alt="" width="16" height="16" title="Attachment Present" />\';
                                                        }
                                                        else
                                                        {
                                                            $return .= \'<img src="images/mru/blank.gif" alt="" width="16" height="16" />\';
                                                        }
    
                                                        return $return;
                                                       ',
    
                                         'join'     => 'LEFT JOIN attachment
                                                            ON candidate.candidate_id = attachment.data_item_id
                                                                                                                    AND attachment.data_item_type = '.DATA_ITEM_CANDIDATE.'
                                                        LEFT JOIN candidate_joborder AS candidate_joborder_submitted
                                                            ON candidate_joborder_submitted.candidate_id = candidate.candidate_id
                                                            AND candidate_joborder_submitted.status >= '.PIPELINE_STATUS_SUBMITTED.'
                                                            AND candidate_joborder_submitted.site_id = '.$this->_siteID.'
                                                            AND candidate_joborder_submitted.status != '.PIPELINE_STATUS_NOTINCONSIDERATION,
                                         'pagerWidth'    => 34,
                                         'pagerOptional' => true,
                                         'pagerNoTitle' => true,
                                         'sizable'  => false,
                                         'exportable' => false,
                                         'filterable' => false),
    
                'First Name' =>     array('select'         => 'candidate.first_name AS firstName',
                                          'pagerRender'    => 'if ($rsData[\'isHot\'] == 1) $className =  \'jobLinkHot\'; else $className = \'jobLinkCold\'; return \'<a href="'.RESUFLOUtility::getIndexName().'?m=candidates&amp;list='.$this->get['a'].'&amp;a=show&amp;candidateID=\'.$rsData[\'candidateID\'].\'&amp;view=1" class="\'.$className.\'">\'.htmlspecialchars($rsData[\'firstName\']).\'</a>\';',
                                          'sortableColumn' => 'firstName',
                                          'pagerWidth'     => 75,
                                          'pagerOptional'  => false,
                                          'alphaNavigation'=> true,
                                          'filter'         => 'candidate.first_name'),
    
                'Last Name' =>      array('select'         => 'candidate.last_name AS lastName',
                                         'sortableColumn'  => 'lastName',
                                         'pagerRender'     => 'if ($rsData[\'isHot\'] == 1) $className =  \'jobLinkHot\'; else $className = \'jobLinkCold\'; return \'<a href="'.RESUFLOUtility::getIndexName().'?m=candidates&amp;list='.$this->get['a'].'&amp;a=show&amp;candidateID=\'.$rsData[\'candidateID\'].\'&amp;view=1" class="\'.$className.\'">\'.htmlspecialchars($rsData[\'lastName\']).\'</a>\';',
                                         'pagerWidth'      => 85,
                                         'pagerOptional'   => false,
                                         'alphaNavigation' => true,
                                         'filter'         => 'candidate.last_name'),
    
                'E-Mail' =>         array('select'   => 'candidate.email1 AS email1',
                                         'sortableColumn'     => 'email1',
                                         'pagerWidth'    => 80,
                                         'filter'         => 'candidate.email1'),
    
                '2nd E-Mail' =>     array('select'   => 'candidate.email2 AS email2',
                                         'sortableColumn'     => 'email2',
                                         'pagerWidth'    => 80,
                                         'filter'         => 'candidate.email2'),
    
                'Home Phone' =>     array('select'   => 'candidate.phone_home AS phoneHome',
                                         'sortableColumn'     => 'phoneHome',
                                         'pagerWidth'    => 80,
                                         'filter'         => 'candidate.phone_home'),
    
                'Cell Phone' =>     array('select'   => 'candidate.phone_cell AS phoneCell',
                                         'sortableColumn'     => 'phoneCell',
                                         'pagerWidth'    => 80,
                                         'filter'         => 'candidate.phone_cell'),
    
                'Work Phone' =>     array('select'   => 'candidate.phone_work AS phoneWork',
                                         'sortableColumn'     => 'phoneWork',
                                         'pagerWidth'    => 80),
    
                'Address' =>        array('select'   => 'candidate.address AS address',
                                         'sortableColumn'     => 'address',
                                         'pagerWidth'    => 250,
                                         'alphaNavigation' => true,
                                         'filter'         => 'candidate.address'),
    
                'City' =>           array('select'   => 'candidate.city AS city',
                                         'sortableColumn'     => 'city',
                                         'pagerWidth'    => 80,
                                         'alphaNavigation' => true,
                                         'filter'         => 'candidate.city'),
                              'Company' =>           array('select'   => 'Case When candidate.entered_by = '.$_SESSION['RESUFLO']->getUserId().' then "'.$_SESSION['RESUFLO']->getFirstName().'" else (select first_name from user where user_id =(select entered_by from user where user_id=candidate.entered_by) ) End as company',
                                         'sortableColumn'     => 'company',
                                         'pagerWidth'    => 80,
                                         'alphaNavigation' => true,
                                         'filter'         => 'candidate.entered_by'),
    
    
                'State' =>          array('select'   => 'candidate.state AS state',
                                         'sortableColumn'     => 'state',
                                         'filterType' => 'dropDown',
                                         'pagerWidth'    => 50,
                                         'alphaNavigation' => true,
                                         'filter'         => 'candidate.state'),
    
                'Zip' =>            array('select'  => 'candidate.zip AS zip',
                                         'sortableColumn'    => 'zip',
                                         'pagerWidth'   => 50,
                                         'filter'         => 'candidate.zip'),
    
                'Misc Notes' =>     array('select'  => 'candidate.notes AS notes',
                                         'sortableColumn'    => 'notes',
                                         'pagerWidth'   => 300,
                                         'filter'         => 'candidate.notes'),
    
                'Web Site' =>      array('select'  => 'candidate.web_site AS webSite',
                                         'pagerRender'     => 'return \'<a href="\'.htmlspecialchars($rsData[\'webSite\']).\'">\'.htmlspecialchars($rsData[\'webSite\']).\'</a>\';',
                                         'sortableColumn'    => 'webSite',
                                         'pagerWidth'   => 80,
                                         'filter'         => 'candidate.web_site'),
    
                'Key Skills' =>    array('select'  => 'candidate.key_skills AS keySkills',
                                         'pagerRender' => 'return substr(trim($rsData[\'keySkills\']), 0, 30) . (strlen(trim($rsData[\'keySkills\'])) > 30 ? \'...\' : \'\');',
                                         'sortableColumn'    => 'keySkills',
                                         'pagerWidth'   => 210,
                                         'filter'         => 'candidate.key_skills'),
    
                'Recent Status' => array('select'  => '(
                                                        SELECT
                                                            CONCAT(
                                                                \'<a href="'.RESUFLOUtility::getIndexName().'?m=joborders&amp;list='.$this->get['a'].'&amp;a=show&amp;jobOrderID=\',
                                                                joborder.joborder_id,
                                                                \'" title="\',
                                                                joborder.title,
                                                                \' (\',
                                                                company.name,
                                                                \')">\',
                                                                candidate_joborder_status.short_description,
                                                                \'</a>\'
                                                            )
                                                        FROM
                                                            candidate_joborder
                                                        LEFT JOIN candidate_joborder_status
                                                            ON candidate_joborder_status.candidate_joborder_status_id = candidate_joborder.status
                                                        LEFT JOIN joborder
                                                            ON joborder.joborder_id = candidate_joborder.joborder_id
                                                        LEFT JOIN company
                                                            ON joborder.company_id = company.company_id
                                                        WHERE
                                                            candidate_joborder.candidate_id = candidate.candidate_id
                                                        ORDER BY
                                                            candidate_joborder.date_modified DESC
                                                        LIMIT 1
                                                    ) AS lastStatus
                                                    ',
                                         'sort'    => 'lastStatus',
                                         'pagerRender'     => 'return $rsData[\'lastStatus\'];',
                                         'exportRender'     => 'return $rsData[\'lastStatus\'];',
                                         'pagerWidth'   => 140,
                                         'exportable' => false,
                                         'filterHaving'  => 'lastStatus',
                                         'filterTypes'   => '=~'),
                                         
                'Activity Notes' =>     array('select'  => 'candidate.activity_notes AS activity_notes',
                                                     'sortableColumn'    => 'activity_notes',
                                                     'pagerWidth'   => 300,
                                                     'filter'         => 'candidate.activity_notes'),
                                     
                'Recent Status (Extended)' => array('select'  => '(
                                                        SELECT
                                                            CONCAT(
                                                                candidate_joborder_status.short_description,
                                                                \'<br />\',
                                                                \'<a href="'.RESUFLOUtility::getIndexName().'?m=companies&amp;list='.$this->get['a'].'&amp;a=show&amp;companyID=\',
                                                                company.company_id,
                                                                \'">\',
                                                                company.name,
                                                                \'</a> - \',
                                                                \'<a href="'.RESUFLOUtility::getIndexName().'?m=joborders&amp;list='.$this->get['a'].'&amp;a=show&amp;jobOrderID=\',
                                                                joborder.joborder_id,
                                                                \'">\',
                                                                joborder.title,
                                                                \'</a>\'
                                                            )
                                                        FROM
                                                            candidate_joborder
                                                        LEFT JOIN candidate_joborder_status
                                                            ON candidate_joborder_status.candidate_joborder_status_id = candidate_joborder.status
                                                        LEFT JOIN joborder
                                                            ON joborder.joborder_id = candidate_joborder.joborder_id
                                                        LEFT JOIN company
                                                            ON joborder.company_id = company.company_id
                                                        WHERE
                                                            candidate_joborder.candidate_id = candidate.candidate_id
                                                        ORDER BY
                                                            candidate_joborder.date_modified DESC
                                                        LIMIT 1
                                                    ) AS lastStatusLong
                                                    ',
                                         'sortableColumn'    => 'lastStatusLong',
                                         'pagerRender'     => 'return $rsData[\'lastStatusLong\'];',
                                         'pagerWidth'   => 310,
                                         'exportable' => false,
                                         'filterable' => false),
    
                'Source' =>        array('select'  => 'candidate.source AS source',
                                         'sortableColumn'    => 'source',
                                         'pagerWidth'   => 140,
                                         'alphaNavigation' => true,
                                         'filter'         => 'candidate.source'),
    
                'Available' =>     array('select'   => 'DATE_FORMAT(candidate.date_available, \'%m-%d-%y\') AS dateAvailable',
                                         'sortableColumn'     => 'dateAvailable',
                                         'pagerWidth'    => 60),
    
                'Current Employer' => array('select'  => 'candidate.current_employer AS currentEmployer',
                                         'sortableColumn'    => 'currentEmployer',
                                         'pagerWidth'   => 125,
                                         'alphaNavigation' => true,
                                         'filter'         => 'candidate.current_employer'),
    
                'Current Pay' => array('select'  => 'candidate.current_pay AS currentPay',
                                         'sortableColumn'    => 'currentPay',
                                         'pagerWidth'   => 125,
                                         'filter'         => 'candidate.current_pay',
                                         'filterTypes'   => '===>=<'),
    
                'Desired Pay' => array('select'  => 'candidate.desired_pay AS desiredPay',
                                         'sortableColumn'    => 'desiredPay',
                                         'pagerWidth'   => 125,
                                         'filter'         => 'candidate.desired_pay',
                                         'filterTypes'   => '===>=<'),
    
                'Can Relocate'  => array('select'  => 'candidate.can_relocate AS canRelocate',
                                         'pagerRender'     => 'return ($rsData[\'canRelocate\'] == 0 ? \'No\' : \'Yes\');',
                                         'exportRender'     => 'return ($rsData[\'canRelocate\'] == 0 ? \'No\' : \'Yes\');',
                                         'sortableColumn'    => 'canRelocate',
                                         'pagerWidth'   => 80,
                                         'filter'         => 'candidate.can_relocate'),
    
                'Recruiter' =>         array('select'   => 'owner_user.first_name AS ownerFirstName,' .
                                                       'owner_user.last_name AS ownerLastName,' .
                                                       'CONCAT(owner_user.last_name, owner_user.first_name) AS ownerSort',
                                         'join'     => 'LEFT JOIN user AS owner_user ON candidate.owner = owner_user.user_id',
                                         'pagerRender'      => 'return StringUtility::makeInitialName($rsData[\'ownerFirstName\'], $rsData[\'ownerLastName\'], false, LAST_NAME_MAXLEN);',
                                         'exportRender'     => 'return $rsData[\'ownerFirstName\'] . " " .$rsData[\'ownerLastName\'];',
                                         'sortableColumn'     => 'ownerSort',
                                         'pagerWidth'    => 75,
                                         'alphaNavigation' => true,
                                         'filter'         => 'CONCAT(owner_user.first_name, owner_user.last_name)'),
    
                'Created' =>       array('select'   => 'DATE_FORMAT(candidate.date_created, \'%m-%d-%y\') AS dateCreated',
                                         'pagerRender'      => 'return $rsData[\'dateCreated\'];',
                                         'sortableColumn'     => 'dateCreatedSort',
                                         'pagerWidth'    => 60,
                                         'filterHaving' => 'DATE_FORMAT(candidate.date_created, \'%m-%d-%y\')'),
                                    
                'Date From' =>       array('select'   => 'DATE_FORMAT(candidate.date_created, \'%m-%d-%y\') AS dateCreated',
                                        'pagerRender'      => 'return $rsData[\'dateCreated\'];',
                                        'sortableColumn'     => 'dateCreatedSort',
                                        'pagerWidth'    => 60,
                                        'filterHaving' => 'DATE_FORMAT(candidate.date_created, \'%y-%m-%d\')',
                                        'filterTypes'   => '=>'),
                
                'Date To' =>       array('select'   => 'DATE_FORMAT(candidate.date_created, \'%m-%d-%y\') AS dateTo',
                                         'pagerRender'      => 'return $rsData[\'dateTo\'];',
                                         'sortableColumn'     => 'dateToSort',
                                         'pagerWidth'    => 60,
                                         'filterHaving' => 'DATE_FORMAT(candidate.date_created, \'%y-%m-%d\')',
                                         'filterTypes'   => '=<'),
    
                'Modified' =>      array('select'   => 'DATE_FORMAT(candidate.date_modified, \'%m-%d-%y\') AS dateModified',
                                         'pagerRender'      => 'return $rsData[\'dateModified\'];',
                                         'sortableColumn'     => 'dateModifiedSort',
                                         'pagerWidth'    => 60,
                                         'pagerOptional' => false,
                                         'filterHaving' => 'DATE_FORMAT(candidate.date_modified, \'%m-%d-%y\')'),
                                    
                'Scheduled' =>      array('select'   => 'DATE_FORMAT(candidate.schedule_date, \'%m-%d-%y %r\') AS dateScheduled',
                                         'pagerRender'      => 'return $rsData[\'dateScheduled\'];',
                                         'sortableColumn'     => 'dateScheduledSort',
                                         'pagerWidth'    => 150,
                                         'pagerOptional' => true,
                                         'filterHaving' => 'DATE_FORMAT(candidate.schedule_date, \'%m-%d-%y %r\')'),
    
                /* This one only works when called from the saved list view.  Thats why it is not optional, filterable, or exportable.
                 * FIXME:  Somehow make this defined in the associated savedListDataGrid class child.
                 */
                'Added To List' =>  array('select'   => 'DATE_FORMAT(saved_list_entry.date_created, \'%m-%d-%y\') AS dateAddedToList,
                                                         saved_list_entry.date_created AS dateAddedToListSort',
                                         'pagerRender'      => 'return $rsData[\'dateAddedToList\'];',
                                         'sortableColumn'     => 'dateAddedToListSort',
                                         'pagerWidth'    => 60,
                                         'pagerOptional' => false,
                                         'filterable' => false,
                                         'exportable' => false),
    
                'OwnerID' =>       array('select'    => '',
                                         'filter'    => 'candidate.owner',
                                         'pagerOptional' => false,
                                         'filterable' => false,
                                         'filterDescription' => 'Only My Candidates'),
    
                'IsHot' =>         array('select'    => '',
                                         'filter'    => 'candidate.is_hot',
                                         'pagerOptional' => false,
                                         'filterable' => false,
                                         'filterDescription' => 'Only Hot Candidates'),
                            'Campaign' => $GLOBALS['sheesh']
            );
    
            if (US_ZIPS_ENABLED)
            {
                $this->_classColumns['Near Zipcode'] =
                                   array('select'  => 'candidate.zip AS zip',
                                         'filter' => 'candidate.zip',
                                         'pagerOptional' => false,
                                         'filterTypes'   => '=@');
            }
    
            /* Extra fields get added as columns here. */
            $candidates = new Candidates($this->_siteID);
            $extraFieldsRS = $candidates->extraFields->getSettings();
            foreach ($extraFieldsRS as $index => $data)
            {
                $fieldName = $data['fieldName'];
    
                if (!isset($this->_classColumns[$fieldName]))
                {
                    $columnDefinition = $candidates->extraFields->getDataGridDefinition($index, $data, $this->_db);
    
                    /* Return false for extra fields that should not be columns. */
                    if ($columnDefinition !== false)
                    {
                        $this->_classColumns[$fieldName] = $columnDefinition;
                    }
                }
            }
        
        }
        else if ($_REQUEST['a']=='search' ) {
            /* Search Section Of candidate Tab Starts   */
        
        
             $mode = trim($_REQUEST['mode']);
             $query = trim($_GET['wildCardString']);
             
             $wildCardString = str_replace('*', '%', $query) ;
             $wildCardString = "%".$wildCardString."%";
             $wildCardString = $this->_db->makeQueryString($wildCardString);
             if($_GET['start_date']!="")
                {
                    $StartDateParts=explode("-",$_REQUEST["start_date"]);
                    $strt = $StartDateParts[2]."-".$StartDateParts[0]."-".$StartDateParts[1].' 00:00:00';
                    $EndDateParts=explode("-",$_REQUEST["end_date"]);
                    $end1 = $EndDateParts[2]."-".$EndDateParts[0]."-".$EndDateParts[1].' 23:59:59';
                }
                if (isset($strt) and isset($end1)) {
                     $this->_assignedCriterion .= " and (candidate.date_created Between '$strt' AND '$end1')";
                } else {
                     $this->_assignedCriterion .= " and (candidate.date_created Between date_sub(now(), interval 6 month) and now())";
                }
        
            /* Search mode case  */
            switch ($mode) {
                case 'searchByFullName':
                    $wildCardString1 = str_replace('*', '%', $query)."%" ;
                    $wildCardString1 = $this->_db->makeQueryString($wildCardString1);
                    $this->_assignedCriterion .= " and (
                        CONCAT(candidate.first_name, ' ', candidate.last_name) LIKE ".$wildCardString1."
                        OR CONCAT(candidate.last_name, ' ', candidate.first_name) LIKE ".$wildCardString1."
                        OR CONCAT(candidate.last_name, ', ', candidate.first_name) LIKE ".$wildCardString1."
                    )";
                break;    
                case 'searchByKeySkills':
                    $this->_assignedCriterion .= " and candidate.key_skills LIKE ".$wildCardString;
                    $keySkillsWildCardString = $query;
                break;
                case 'searchByCandidateNotes':
                    $this->_assignedCriterion .= " and candidate.notes LIKE ".$wildCardString;
                    $keySkillsWildCardString = $query;
                break;
                case 'searchByResume':
                    $this->_assignedCriterion .= " and ((candidate.notes LIKE ".$wildCardString.") OR (candidate.key_skills LIKE ".$wildCardString.") OR (attachment.text LIKE ".$wildCardString."))";
                    /* and attachment.data_item_id=candidate.candidate_id */
                    $resumeWildCardString = $query;
                break;
                case 'phoneNumber':
                    $wildCardString = str_replace( array('.', '-', '(', ')'), '', $wildCardString );
                    $this->_assignedCriterion .= " and (
                REPLACE(
                    REPLACE(
                        REPLACE(
                            REPLACE(candidate.phone_home, '-', ''),
                        '.', ''),
                    ')', ''),
                '(', '') LIKE ".$wildCardString."
                OR REPLACE(
                    REPLACE(
                        REPLACE(
                            REPLACE(candidate.phone_cell, '-', ''),
                        '.', ''),
                    ')', ''),
                '(', '') LIKE ".$wildCardString.")";
                    $phoneNumberWildCardString = $query;
                break;
                case 'searchByTags':
                    $wildCardString = str_replace('*', '%', $wildCardString);
                    $this->_assignedCriterion .= " and candidate.web_site LIKE ".$wildCardString;
                    $tagsWildCardString = $query;
                break;
                case 'searchByEmail':
                    $wildCardString2 = str_replace('*', '%', $query) ;
                    $wildCardString2 = $this->_db->makeQueryString($wildCardString2);
                    $this->_assignedCriterion .= " and candidate.email1 LIKE ".$wildCardString2;
                    $emailWildCardString = $query;
                break;
                case 'searchByDetailResume':
                    // this is our problem...we need to figure out how to make this return faster
                    //$this->_assignedCriterion .= " AND (candidate.candidate_id IN (SELECT data_item_id FROM extra_field WHERE field_name = 'Detail Resume' AND value LIKE ".$wildCardString."))";

                    // the biggest speed increase is simply in only looking in extra_field rows that belong to our user!!
                    // but secondly we are increasing speed by only allowing people to go back 6 months by default

                    $date1 = (empty($strt)) ? 'date_sub(now(), interval 6 month)' : '"'.mysql_escape_string($strt).'"';
                    $date2 = (empty($end1)) ? 'now()'                             : '"'.mysql_escape_string($end1).'"';
                    $user__id =& $_SESSION['RESUFLO']->getUserID();

                    $this->_assignedCriterion .= " AND (candidate.candidate_id in (select dr.candidate_id from candidate left outer join candidate_detailed_resume dr on candidate.candidate_id = dr.candidate_id where candidate.entered_by = $user__id and candidate.date_created Between $date1 and $date2 and dr.detailed_resume like $wildCardString))";

                break;    
                default:
                    $this->_assignedCriterion = "";
                    return;
            }
 
            $this->_dataItemIDColumn = 'candidate.candidate_id';
               $this->_classColumns = array(
                'Attachments' => array('select' => 'IF(candidate_joborder_submitted.candidate_joborder_id, 1, 0) AS submitted,
                                                    IF(attachment_id, 1, 0) AS attachmentPresent',
    
                                         'pagerRender' => 'if ($rsData[\'submitted\'] == 1)
                                                        {
                                                            $return = \'<img src="images/job_orders.gif" alt="" width="16" height="16" title="Submitted for a Job Order" />\';
                                                        }
                                                        else
                                                        {
                                                            $return = \'<img src="images/mru/blank.gif" alt="" width="16" height="16" />\';
                                                        }
    
                                                        if ($rsData[\'attachmentPresent\'] == 1)
                                                        {
                                                            $return .= \'<img src="images/paperclip.gif" alt="" width="16" height="16" title="Attachment Present" />\';
                                                        }
                                                        else
                                                        {
                                                            $return .= \'<img src="images/mru/blank.gif" alt="" width="16" height="16" />\';
                                                        }
    
                                                        return $return;
                                                       ',
    
                                         'join'     => 'LEFT JOIN attachment
                                                            ON candidate.candidate_id = attachment.data_item_id
                                                                                                                    AND attachment.data_item_type = '.DATA_ITEM_CANDIDATE.'
                                                        LEFT JOIN candidate_joborder AS candidate_joborder_submitted
                                                            ON candidate_joborder_submitted.candidate_id = candidate.candidate_id
                                                            AND candidate_joborder_submitted.status >= '.PIPELINE_STATUS_SUBMITTED.'
                                                            AND candidate_joborder_submitted.site_id = '.$this->_siteID.'
                                                            AND candidate_joborder_submitted.status != '.PIPELINE_STATUS_NOTINCONSIDERATION,
                                         'pagerWidth'    => 34,
                                         'pagerOptional' => true,
                                         'pagerNoTitle' => true,
                                         'sizable'  => false,
                                         'exportable' => false,
                                         'filterable' => false),
    
                'First Name' =>     array('select'         => 'candidate.first_name AS firstName',
                                          'pagerRender'    => 'if ($rsData[\'isHot\'] == 1) $className =  \'jobLinkHot\'; else $className = \'jobLinkCold\'; return \'<a href="'.RESUFLOUtility::getIndexName().'?m=candidates&amp;list='.$this->get['a'].'&amp;a=show&amp;candidateID=\'.$rsData[\'candidateID\'].\'&amp;view=1" class="\'.$className.\'">\'.htmlspecialchars($rsData[\'firstName\']).\'</a>\';',
                                          'sortableColumn' => 'firstName',
                                          'pagerWidth'     => 75,
                                          'pagerOptional'  => false,
                                          'alphaNavigation'=> true,
                                          'filter'         => 'candidate.first_name'),
    
                'Last Name' =>      array('select'         => 'candidate.last_name AS lastName',
                                         'sortableColumn'  => 'lastName',
                                         'pagerRender'     => 'if ($rsData[\'isHot\'] == 1) $className =  \'jobLinkHot\'; else $className = \'jobLinkCold\'; return \'<a href="'.RESUFLOUtility::getIndexName().'?m=candidates&amp;list='.$this->get['a'].'&amp;a=show&amp;candidateID=\'.$rsData[\'candidateID\'].\'&amp;view=1" class="\'.$className.\'">\'.htmlspecialchars($rsData[\'lastName\']).\'</a>\';',
                                         'pagerWidth'      => 85,
                                         'pagerOptional'   => false,
                                         'alphaNavigation' => true,
                                         'filter'         => 'candidate.last_name'),
    
                'E-Mail' =>         array('select'   => 'candidate.email1 AS email1',
                                         'sortableColumn'     => 'email1',
                                         'pagerWidth'    => 80,
                                         'filter'         => 'candidate.email1'),
    
                '2nd E-Mail' =>     array('select'   => 'candidate.email2 AS email2',
                                         'sortableColumn'     => 'email2',
                                         'pagerWidth'    => 80,
                                         'filter'         => 'candidate.email2'),
    
                'Home Phone' =>     array('select'   => 'candidate.phone_home AS phoneHome',
                                         'sortableColumn'     => 'phoneHome',
                                         'pagerWidth'    => 80,
                                         'filter'         => 'candidate.phone_home'),
    
                'Cell Phone' =>     array('select'   => 'candidate.phone_cell AS phoneCell',
                                         'sortableColumn'     => 'phoneCell',
                                         'pagerWidth'    => 80,
                                         'filter'         => 'candidate.phone_cell'),
    
                'Work Phone' =>     array('select'   => 'candidate.phone_work AS phoneWork',
                                         'sortableColumn'     => 'phoneWork',
                                         'pagerWidth'    => 80),
    
                'Address' =>        array('select'   => 'candidate.address AS address',
                                         'sortableColumn'     => 'address',
                                         'pagerWidth'    => 250,
                                         'alphaNavigation' => true,
                                         'filter'         => 'candidate.address'),
    
                'City' =>           array('select'   => 'candidate.city AS city',
                                         'sortableColumn'     => 'city',
                                         'pagerWidth'    => 80,
                                         'alphaNavigation' => true,
                                         'filter'         => 'candidate.city'),
                              'Company' =>           array('select'   => 'Case When candidate.entered_by = '.$_SESSION['RESUFLO']->getUserId().' then "'.$_SESSION['RESUFLO']->getFirstName().'" else (select first_name from user where user_id =(select entered_by from user where user_id=candidate.entered_by) ) End as company',
                                         'sortableColumn'     => 'company',
                                         'pagerWidth'    => 80,
                                         'alphaNavigation' => true,
                                         'filter'         => 'candidate.entered_by'),
    
    
                'State' =>          array('select'   => 'candidate.state AS state',
                                         'sortableColumn'     => 'state',
                                         'filterType' => 'dropDown',
                                         'pagerWidth'    => 50,
                                         'alphaNavigation' => true,
                                         'filter'         => 'candidate.state'),
    
                'Zip' =>            array('select'  => 'candidate.zip AS zip',
                                         'sortableColumn'    => 'zip',
                                         'pagerWidth'   => 50,
                                         'filter'         => 'candidate.zip'),
    
                'Misc Notes' =>     array('select'  => 'candidate.notes AS notes',
                                         'sortableColumn'    => 'notes',
                                         'pagerWidth'   => 300,
                                         'filter'         => 'candidate.notes'),
    
                'Web Site' =>      array('select'  => 'candidate.web_site AS webSite',
                                         'pagerRender'     => 'return \'<a href="\'.htmlspecialchars($rsData[\'webSite\']).\'">\'.htmlspecialchars($rsData[\'webSite\']).\'</a>\';',
                                         'sortableColumn'    => 'webSite',
                                         'pagerWidth'   => 80,
                                         'filter'         => 'candidate.web_site'),
    
                'Key Skills' =>    array('select'  => 'candidate.key_skills AS keySkills',
                                         'pagerRender' => 'return substr(trim($rsData[\'keySkills\']), 0, 30) . (strlen(trim($rsData[\'keySkills\'])) > 30 ? \'...\' : \'\');',
                                         'sortableColumn'    => 'keySkills',
                                         'pagerWidth'   => 210,
                                         'filter'         => 'candidate.key_skills'),
    
                'Recent Status' => array('select'  => '(
                                                        SELECT
                                                            CONCAT(
                                                                \'<a href="'.RESUFLOUtility::getIndexName().'?m=joborders&amp;list='.$this->get['a'].'&amp;a=show&amp;jobOrderID=\',
                                                                joborder.joborder_id,
                                                                \'" title="\',
                                                                joborder.title,
                                                                \' (\',
                                                                company.name,
                                                                \')">\',
                                                                candidate_joborder_status.short_description,
                                                                \'</a>\'
                                                            )
                                                        FROM
                                                            candidate_joborder
                                                        LEFT JOIN candidate_joborder_status
                                                            ON candidate_joborder_status.candidate_joborder_status_id = candidate_joborder.status
                                                        LEFT JOIN joborder
                                                            ON joborder.joborder_id = candidate_joborder.joborder_id
                                                        LEFT JOIN company
                                                            ON joborder.company_id = company.company_id
                                                        WHERE
                                                            candidate_joborder.candidate_id = candidate.candidate_id
                                                        ORDER BY
                                                            candidate_joborder.date_modified DESC
                                                        LIMIT 1
                                                    ) AS lastStatus
                                                    ',
                                         'sort'    => 'lastStatus',
                                         'pagerRender'     => 'return $rsData[\'lastStatus\'];',
                                         'exportRender'     => 'return $rsData[\'lastStatus\'];',
                                         'pagerWidth'   => 140,
                                         'exportable' => false,
                                         'filterHaving'  => 'lastStatus',
                                         'filterTypes'   => '=~'),
                                         
                'Activity Notes' =>     array('select'  => 'candidate.activity_notes AS activity_notes',
                                                     'sortableColumn'    => 'activity_notes',
                                                     'pagerWidth'   => 300,
                                                     'filter'         => 'candidate.activity_notes'),
                                     
                'Recent Status (Extended)' => array('select'  => '(
                                                        SELECT
                                                            CONCAT(
                                                                candidate_joborder_status.short_description,
                                                                \'<br />\',
                                                                \'<a href="'.RESUFLOUtility::getIndexName().'?m=companies&amp;list='.$this->get['a'].'&amp;a=show&amp;companyID=\',
                                                                company.company_id,
                                                                \'">\',
                                                                company.name,
                                                                \'</a> - \',
                                                                \'<a href="'.RESUFLOUtility::getIndexName().'?m=joborders&amp;list='.$this->get['a'].'&amp;a=show&amp;jobOrderID=\',
                                                                joborder.joborder_id,
                                                                \'">\',
                                                                joborder.title,
                                                                \'</a>\'
                                                            )
                                                        FROM
                                                            candidate_joborder
                                                        LEFT JOIN candidate_joborder_status
                                                            ON candidate_joborder_status.candidate_joborder_status_id = candidate_joborder.status
                                                        LEFT JOIN joborder
                                                            ON joborder.joborder_id = candidate_joborder.joborder_id
                                                        LEFT JOIN company
                                                            ON joborder.company_id = company.company_id
                                                        WHERE
                                                            candidate_joborder.candidate_id = candidate.candidate_id
                                                        ORDER BY
                                                            candidate_joborder.date_modified DESC
                                                        LIMIT 1
                                                    ) AS lastStatusLong
                                                    ',
                                         'sortableColumn'    => 'lastStatusLong',
                                         'pagerRender'     => 'return $rsData[\'lastStatusLong\'];',
                                         'pagerWidth'   => 310,
                                         'exportable' => false,
                                         'filterable' => false),
    
                'Source' =>        array('select'  => 'candidate.source AS source',
                                         'sortableColumn'    => 'source',
                                         'pagerWidth'   => 140,
                                         'alphaNavigation' => true,
                                         'filter'         => 'candidate.source'),
    
                'Available' =>     array('select'   => 'DATE_FORMAT(candidate.date_available, \'%m-%d-%y\') AS dateAvailable',
                                         'sortableColumn'     => 'dateAvailable',
                                         'pagerWidth'    => 60),
    
                'Current Employer' => array('select'  => 'candidate.current_employer AS currentEmployer',
                                         'sortableColumn'    => 'currentEmployer',
                                         'pagerWidth'   => 125,
                                         'alphaNavigation' => true,
                                         'filter'         => 'candidate.current_employer'),
    
                'Current Pay' => array('select'  => 'candidate.current_pay AS currentPay',
                                         'sortableColumn'    => 'currentPay',
                                         'pagerWidth'   => 125,
                                         'filter'         => 'candidate.current_pay',
                                         'filterTypes'   => '===>=<'),
    
                'Desired Pay' => array('select'  => 'candidate.desired_pay AS desiredPay',
                                         'sortableColumn'    => 'desiredPay',
                                         'pagerWidth'   => 125,
                                         'filter'         => 'candidate.desired_pay',
                                         'filterTypes'   => '===>=<'),
    
                'Can Relocate'  => array('select'  => 'candidate.can_relocate AS canRelocate',
                                         'pagerRender'     => 'return ($rsData[\'canRelocate\'] == 0 ? \'No\' : \'Yes\');',
                                         'exportRender'     => 'return ($rsData[\'canRelocate\'] == 0 ? \'No\' : \'Yes\');',
                                         'sortableColumn'    => 'canRelocate',
                                         'pagerWidth'   => 80,
                                         'filter'         => 'candidate.can_relocate'),
    
                'Recruiter' =>         array('select'   => 'owner_user.first_name AS ownerFirstName,' .
                                                       'owner_user.last_name AS ownerLastName,' .
                                                       'CONCAT(owner_user.last_name, owner_user.first_name) AS ownerSort',
                                         'join'     => 'LEFT JOIN user AS owner_user ON candidate.owner = owner_user.user_id',
                                         'pagerRender'      => 'return StringUtility::makeInitialName($rsData[\'ownerFirstName\'], $rsData[\'ownerLastName\'], false, LAST_NAME_MAXLEN);',
                                         'exportRender'     => 'return $rsData[\'ownerFirstName\'] . " " .$rsData[\'ownerLastName\'];',
                                         'sortableColumn'     => 'ownerSort',
                                         'pagerWidth'    => 75,
                                         'alphaNavigation' => true,
                                         'filter'         => 'CONCAT(owner_user.first_name, owner_user.last_name)'),
    
                'Created' =>       array('select'   => 'DATE_FORMAT(candidate.date_created, \'%m-%d-%y\') AS dateCreated',
                                         'pagerRender'      => 'return $rsData[\'dateCreated\'];',
                                         'sortableColumn'     => 'dateCreatedSort',
                                         'pagerWidth'    => 60,
                                         'filterHaving' => 'DATE_FORMAT(candidate.date_created, \'%m-%d-%y\')'),
                
                'Date From' =>       array('select'   => 'DATE_FORMAT(candidate.date_created, \'%m-%d-%y\') AS dateCreated',
                                        'pagerRender'      => 'return $rsData[\'dateCreated\'];',
                                        'sortableColumn'     => 'dateCreatedSort',
                                        'pagerWidth'    => 60,
                                        'filterHaving' => 'DATE_FORMAT(candidate.date_created, \'%y-%m-%d\')',
                                        'filterTypes'   => '=>'),
                
                'Date To' =>       array('select'   => 'DATE_FORMAT(candidate.date_created, \'%m-%d-%y\') AS dateTo',
                                         'pagerRender'      => 'return $rsData[\'dateTo\'];',
                                         'sortableColumn'     => 'dateToSort',
                                         'pagerWidth'    => 60,
                                         'filterHaving' => 'DATE_FORMAT(candidate.date_created, \'%y-%m-%d\')',
                                         'filterTypes'   => '=<'),
    
                'Modified' =>      array('select'   => 'DATE_FORMAT(candidate.date_modified, \'%m-%d-%y\') AS dateModified',
                                         'pagerRender'      => 'return $rsData[\'dateModified\'];',
                                         'sortableColumn'     => 'dateModifiedSort',
                                         'pagerWidth'    => 60,
                                         'pagerOptional' => false,
                                         'filterHaving' => 'DATE_FORMAT(candidate.date_modified, \'%m-%d-%y\')'),
                
                'Scheduled' =>      array('select'   => 'DATE_FORMAT(candidate.schedule_date, \'%m-%d-%y %r\') AS dateScheduled',
                                         'pagerRender'      => 'return $rsData[\'dateScheduled\'];',
                                         'sortableColumn'     => 'dateScheduledSort',
                                         'pagerWidth'    => 150,
                                         'pagerOptional' => true,
                                         'filterHaving' => 'DATE_FORMAT(candidate.schedule_date, \'%m-%d-%y %r\')'),
    
                /* This one only works when called from the saved list view.  Thats why it is not optional, filterable, or exportable.
                 * FIXME:  Somehow make this defined in the associated savedListDataGrid class child.
                 */
                'Added To List' =>  array('select'   => 'DATE_FORMAT(saved_list_entry.date_created, \'%m-%d-%y\') AS dateAddedToList,
                                                         saved_list_entry.date_created AS dateAddedToListSort',
                                         'pagerRender'      => 'return $rsData[\'dateAddedToList\'];',
                                         'sortableColumn'     => 'dateAddedToListSort',
                                         'pagerWidth'    => 60,
                                         'pagerOptional' => false,
                                         'filterable' => false,
                                         'exportable' => false),
    
                'OwnerID' =>       array('select'    => '',
                                         'filter'    => 'candidate.owner',
                                         'pagerOptional' => false,
                                         'filterable' => false,
                                         'filterDescription' => 'Only My Candidates'),
    
                'IsHot' =>         array('select'    => '',
                                         'filter'    => 'candidate.is_hot',
                                         'pagerOptional' => false,
                                         'filterable' => false,
                                         'filterDescription' => 'Only Hot Candidates'),
                            'Campaign' => $GLOBALS['sheesh']
            );
    
            if (US_ZIPS_ENABLED)
            {
                $this->_classColumns['Near Zipcode'] =
                                   array('select'  => 'candidate.zip AS zip',
                                         'filter' => 'candidate.zip',
                                         'pagerOptional' => false,
                                         'filterTypes'   => '=@');
            }
    
            /* Extra fields get added as columns here. */
            $candidates = new Candidates($this->_siteID);
            $extraFieldsRS = $candidates->extraFields->getSettings();
            foreach ($extraFieldsRS as $index => $data)
            {
                $fieldName = $data['fieldName'];
    
                if (!isset($this->_classColumns[$fieldName]))
                {
                    $columnDefinition = $candidates->extraFields->getDataGridDefinition($index, $data, $this->_db);
    
                    /* Return false for extra fields that should not be columns. */
                    if ($columnDefinition !== false)
                    {
                        $this->_classColumns[$fieldName] = $columnDefinition;
                    }
                }
            }
        
        } else if($_REQUEST['a']=='PipeLine' || $_REQUEST['delrec']=='PipeLine') {
            /* Search Section Of candidate Tab Ends   */
            
            //$this->_assignedCriterion .= " and candidate.view_date = '0000-00-00 00:00:00'";
            $this->_assignedCriterion .= " 
            and (candidate.candidate_id IN (SELECT candidate_id FROM candidate_joborder where added_by='".$userid."')
            or candidate.candidate_id IN (SELECT candidate_id FROM candidate_joborder where added_by IN (SELECT user_id FROM user where entered_by='".$_SESSION['RESUFLO']->getUserID()."')) )
            and candidate.unsubscribe = '0' 
            and candidate.list_id = '0'";
            $this->_dataItemIDColumn = 'candidate.candidate_id';

            $this->_classColumns = array(
            'Attachments' => array('select' => 'IF(candidate_joborder_submitted.candidate_joborder_id, 1, 0) AS submitted,
                                                IF(attachment_id, 1, 0) AS attachmentPresent',

                                     'pagerRender' => 'if ($rsData[\'submitted\'] == 1)
                                                    {
                                                        $return = \'<img src="images/job_orders.gif" alt="" width="16" height="16" title="Submitted for a Job Order" />\';
                                                    }
                                                    else
                                                    {
                                                        $return = \'<img src="images/mru/blank.gif" alt="" width="16" height="16" />\';
                                                    }

                                                    if ($rsData[\'attachmentPresent\'] == 1)
                                                    {
                                                        $return .= \'<img src="images/paperclip.gif" alt="" width="16" height="16" title="Attachment Present" />\';
                                                    }
                                                    else
                                                    {
                                                        $return .= \'<img src="images/mru/blank.gif" alt="" width="16" height="16" />\';
                                                    }

                                                    return $return;
                                                   ',

                                     'join'     => 'LEFT JOIN attachment
                                                        ON candidate.candidate_id = attachment.data_item_id
                                                        AND attachment.data_item_type = '.DATA_ITEM_CANDIDATE.'
                                                    LEFT JOIN candidate_joborder AS candidate_joborder_submitted
                                                        ON candidate_joborder_submitted.candidate_id = candidate.candidate_id
                                                        AND candidate_joborder_submitted.status >= '.PIPELINE_STATUS_SUBMITTED.'
                                                        AND candidate_joborder_submitted.site_id = '.$this->_siteID.'
                                                        AND candidate_joborder_submitted.status != '.PIPELINE_STATUS_NOTINCONSIDERATION,
                                     'pagerWidth'    => 34,
                                     'pagerOptional' => true,
                                     'pagerNoTitle' => true,
                                     'sizable'  => false,
                                     'exportable' => false,
                                     'filterable' => false),

            'First Name' =>     array('select'         => 'candidate.first_name AS firstName',
                                      'pagerRender'    => 'if ($rsData[\'isHot\'] == 1) $className =  \'jobLinkHot\'; else $className = \'jobLinkCold\'; return \'<a href="'.RESUFLOUtility::getIndexName().'?m=candidates&amp;list='.$this->get['a'].'&amp;a=show&amp;candidateID=\'.$rsData[\'candidateID\'].\'" class="\'.$className.\'">\'.htmlspecialchars($rsData[\'firstName\']).\'</a>\';',
                                      'sortableColumn' => 'firstName',
                                      'pagerWidth'     => 75,
                                      'pagerOptional'  => false,
                                      'alphaNavigation'=> true,
                                      'filter'         => 'candidate.first_name'),

            'Last Name' =>      array('select'         => 'candidate.last_name AS lastName',
                                     'sortableColumn'  => 'lastName',
                                     'pagerRender'     => 'if ($rsData[\'isHot\'] == 1) $className =  \'jobLinkHot\'; else $className = \'jobLinkCold\'; return \'<a href="'.RESUFLOUtility::getIndexName().'?m=candidates&amp;list='.$this->get['a'].'&amp;a=show&amp;candidateID=\'.$rsData[\'candidateID\'].\'" class="\'.$className.\'">\'.htmlspecialchars($rsData[\'lastName\']).\'</a>\';',
                                     'pagerWidth'      => 85,
                                     'pagerOptional'   => false,
                                     'alphaNavigation' => true,
                                     'filter'         => 'candidate.last_name'),

            'E-Mail' =>         array('select'   => 'candidate.email1 AS email1',
                                     'sortableColumn'     => 'email1',
                                     'pagerWidth'    => 80,
                                     'filter'         => 'candidate.email1'),

            '2nd E-Mail' =>     array('select'   => 'candidate.email2 AS email2',
                                     'sortableColumn'     => 'email2',
                                     'pagerWidth'    => 80,
                                     'filter'         => 'candidate.email2'),

            'Home Phone' =>     array('select'   => 'candidate.phone_home AS phoneHome',
                                     'sortableColumn'     => 'phoneHome',
                                     'pagerWidth'    => 80,
                                     'filter'         => 'candidate.phone_home'),

            'Cell Phone' =>     array('select'   => 'candidate.phone_cell AS phoneCell',
                                     'sortableColumn'     => 'phoneCell',
                                     'pagerWidth'    => 80,
                                     'filter'         => 'candidate.phone_cell'),

            'Work Phone' =>     array('select'   => 'candidate.phone_work AS phoneWork',
                                     'sortableColumn'     => 'phoneWork',
                                     'pagerWidth'    => 80),

            'Address' =>        array('select'   => 'candidate.address AS address',
                                     'sortableColumn'     => 'address',
                                     'pagerWidth'    => 250,
                                     'alphaNavigation' => true,
                                     'filter'         => 'candidate.address'),

            'City' =>           array('select'   => 'candidate.city AS city',
                                     'sortableColumn'     => 'city',
                                     'pagerWidth'    => 80,
                                     'alphaNavigation' => true,
                                     'filter'         => 'candidate.city'),
              'Company' =>           array('select'   => 'Case When candidate.entered_by = '.$_SESSION['RESUFLO']->getUserId().' then "'.$_SESSION['RESUFLO']->getFirstName().'" else (select first_name from user where user_id =(select entered_by from user where user_id=candidate.entered_by) ) End as company',
                                     'sortableColumn'     => 'company',
                                     'pagerWidth'    => 80,
                                     'alphaNavigation' => true,
                                     'filter'         => 'candidate.entered_by'),


            'State' =>          array('select'   => 'candidate.state AS state',
                                     'sortableColumn'     => 'state',
                                     'filterType' => 'dropDown',
                                     'pagerWidth'    => 50,
                                     'alphaNavigation' => true,
                                     'filter'         => 'candidate.state'),

            'Zip' =>            array('select'  => 'candidate.zip AS zip',
                                     'sortableColumn'    => 'zip',
                                     'pagerWidth'   => 50,
                                     'filter'         => 'candidate.zip'),

            'Misc Notes' =>     array('select'  => 'candidate.notes AS notes',
                                     'sortableColumn'    => 'notes',
                                     'pagerWidth'   => 300,
                                     'filter'         => 'candidate.notes'),

            'Web Site' =>      array('select'  => 'candidate.web_site AS webSite',
                                     'pagerRender'     => 'return \'<a href="\'.htmlspecialchars($rsData[\'webSite\']).\'">\'.htmlspecialchars($rsData[\'webSite\']).\'</a>\';',
                                     'sortableColumn'    => 'webSite',
                                     'pagerWidth'   => 80,
                                     'filter'         => 'candidate.web_site'),

            'Key Skills' =>    array('select'  => 'candidate.key_skills AS keySkills',
                                     'pagerRender' => 'return substr(trim($rsData[\'keySkills\']), 0, 30) . (strlen(trim($rsData[\'keySkills\'])) > 30 ? \'...\' : \'\');',
                                     'sortableColumn'    => 'keySkills',
                                     'pagerWidth'   => 210,
                                     'filter'         => 'candidate.key_skills'),

            'Recent Status' => array('select'  => '(
                                                    SELECT
                                                        CONCAT(
                                                            \'<a href="'.RESUFLOUtility::getIndexName().'?m=joborders&amp;list='.$this->get['a'].'&amp;a=show&amp;jobOrderID=\',
                                                            joborder.joborder_id,
                                                            \'" title="\',
                                                            joborder.title,
                                                            \' (\',
                                                            company.name,
                                                            \')">\',
                                                            candidate_joborder_status.short_description,
                                                            \'</a>\'
                                                        )
                                                    FROM
                                                        candidate_joborder
                                                    LEFT JOIN candidate_joborder_status
                                                        ON candidate_joborder_status.candidate_joborder_status_id = candidate_joborder.status
                                                    LEFT JOIN joborder
                                                        ON joborder.joborder_id = candidate_joborder.joborder_id
                                                    LEFT JOIN company
                                                        ON joborder.company_id = company.company_id
                                                    WHERE
                                                        candidate_joborder.candidate_id = candidate.candidate_id
                                                    ORDER BY
                                                        candidate_joborder.date_modified DESC
                                                    LIMIT 1
                                                ) AS lastStatus
                                                ',
                                     'sort'    => 'lastStatus',
                                     'pagerRender'     => 'return $rsData[\'lastStatus\'];',
                                     'exportRender'     => 'return $rsData[\'lastStatus\'];',
                                     'pagerWidth'   => 140,
                                     'exportable' => false,
                                     'filterHaving'  => 'lastStatus',
                                     'filterTypes'   => '=~'),
            
'Activity Notes' =>     array('select'  => 'candidate.activity_notes AS activity_notes',
                                                     'sortableColumn'    => 'activity_notes',
                                                     'pagerWidth'   => 300,
                                                     'filter'         => 'candidate.activity_notes'),
                                     
            'Recent Status (Extended)' => array('select'  => '(
                                                    SELECT
                                                        CONCAT(
                                                            candidate_joborder_status.short_description,
                                                            \'<br />\',
                                                            \'<a href="'.RESUFLOUtility::getIndexName().'?m=companies&amp;list='.$this->get['a'].'&amp;a=show&amp;companyID=\',
                                                            company.company_id,
                                                            \'">\',
                                                            company.name,
                                                            \'</a> - \',
                                                            \'<a href="'.RESUFLOUtility::getIndexName().'?m=joborders&amp;list='.$this->get['a'].'&amp;a=show&amp;jobOrderID=\',
                                                            joborder.joborder_id,
                                                            \'">\',
                                                            joborder.title,
                                                            \'</a>\'
                                                        )
                                                    FROM
                                                        candidate_joborder
                                                    LEFT JOIN candidate_joborder_status
                                                        ON candidate_joborder_status.candidate_joborder_status_id = candidate_joborder.status
                                                    LEFT JOIN joborder
                                                        ON joborder.joborder_id = candidate_joborder.joborder_id
                                                    LEFT JOIN company
                                                        ON joborder.company_id = company.company_id
                                                    WHERE
                                                        candidate_joborder.candidate_id = candidate.candidate_id
                                                    ORDER BY
                                                        candidate_joborder.date_modified DESC
                                                    LIMIT 1
                                                ) AS lastStatusLong
                                                ',
                                     'sortableColumn'    => 'lastStatusLong',
                                     'pagerRender'     => 'return $rsData[\'lastStatusLong\'];',
                                     'pagerWidth'   => 310,
                                     'exportable' => false,
                                     'filterable' => false),

            'Source' =>        array('select'  => 'candidate.source AS source',
                                     'sortableColumn'    => 'source',
                                     'pagerWidth'   => 140,
                                     'alphaNavigation' => true,
                                     'filter'         => 'candidate.source'),

            'Available' =>     array('select'   => 'DATE_FORMAT(candidate.date_available, \'%m-%d-%y\') AS dateAvailable',
                                     'sortableColumn'     => 'dateAvailable',
                                     'pagerWidth'    => 60),

            'Current Employer' => array('select'  => 'candidate.current_employer AS currentEmployer',
                                     'sortableColumn'    => 'currentEmployer',
                                     'pagerWidth'   => 125,
                                     'alphaNavigation' => true,
                                     'filter'         => 'candidate.current_employer'),

            'Current Pay' => array('select'  => 'candidate.current_pay AS currentPay',
                                     'sortableColumn'    => 'currentPay',
                                     'pagerWidth'   => 125,
                                     'filter'         => 'candidate.current_pay',
                                     'filterTypes'   => '===>=<'),

            'Desired Pay' => array('select'  => 'candidate.desired_pay AS desiredPay',
                                     'sortableColumn'    => 'desiredPay',
                                     'pagerWidth'   => 125,
                                     'filter'         => 'candidate.desired_pay',
                                     'filterTypes'   => '===>=<'),

            'Can Relocate'  => array('select'  => 'candidate.can_relocate AS canRelocate',
                                     'pagerRender'     => 'return ($rsData[\'canRelocate\'] == 0 ? \'No\' : \'Yes\');',
                                     'exportRender'     => 'return ($rsData[\'canRelocate\'] == 0 ? \'No\' : \'Yes\');',
                                     'sortableColumn'    => 'canRelocate',
                                     'pagerWidth'   => 80,
                                     'filter'         => 'candidate.can_relocate'),

            'Recruiter' =>         array('select'   => 'owner_user.first_name AS ownerFirstName,' .
                                                   'owner_user.last_name AS ownerLastName,' .
                                                   'CONCAT(owner_user.last_name, owner_user.first_name) AS ownerSort',
                                     'join'     => 'LEFT JOIN user AS owner_user ON candidate.owner = owner_user.user_id',
                                     'pagerRender'      => 'return StringUtility::makeInitialName($rsData[\'ownerFirstName\'], $rsData[\'ownerLastName\'], false, LAST_NAME_MAXLEN);',
                                     'exportRender'     => 'return $rsData[\'ownerFirstName\'] . " " .$rsData[\'ownerLastName\'];',
                                     'sortableColumn'     => 'ownerSort',
                                     'pagerWidth'    => 75,
                                     'alphaNavigation' => true,
                                     'filter'         => 'CONCAT(owner_user.first_name, owner_user.last_name)'),

            'Created' =>       array('select'   => 'DATE_FORMAT(candidate.date_created, \'%m-%d-%y\') AS dateCreated',
                                     'pagerRender'      => 'return $rsData[\'dateCreated\'];',
                                     'sortableColumn'     => 'dateCreatedSort',
                                     'pagerWidth'    => 60,
                                     'filterHaving' => 'DATE_FORMAT(candidate.date_created, \'%m-%d-%y\')'),
            
            'Date From' =>       array('select'   => 'DATE_FORMAT(candidate.date_created, \'%m-%d-%y\') AS dateCreated',
                                     'pagerRender'      => 'return $rsData[\'dateCreated\'];',
                                     'sortableColumn'     => 'dateCreatedSort',
                                     'pagerWidth'    => 60,
                                     'filterHaving' => 'DATE_FORMAT(candidate.date_created, \'%y-%m-%d\')',
                                     'filterTypes'   => '=>'),
                
            'Date To' =>       array('select'   => 'DATE_FORMAT(candidate.date_created, \'%m-%d-%y\') AS dateTo',
                                        'pagerRender'      => 'return $rsData[\'dateTo\'];',
                                        'sortableColumn'     => 'dateToSort',
                                        'pagerWidth'    => 60,
                                        'filterHaving' => 'DATE_FORMAT(candidate.date_created, \'%y-%m-%d\')',
                                        'filterTypes'   => '=<'),

            'Modified' =>      array('select'   => 'DATE_FORMAT(candidate.date_modified, \'%m-%d-%y\') AS dateModified',
                                     'pagerRender'      => 'return $rsData[\'dateModified\'];',
                                     'sortableColumn'     => 'dateModifiedSort',
                                     'pagerWidth'    => 60,
                                     'pagerOptional' => false,
                                     'filterHaving' => 'DATE_FORMAT(candidate.date_modified, \'%m-%d-%y\')'),
            
            'Scheduled' =>      array('select'   => 'DATE_FORMAT(candidate.schedule_date, \'%m-%d-%y %r\') AS dateScheduled',
                                         'pagerRender'      => 'return $rsData[\'dateScheduled\'];',
                                         'sortableColumn'     => 'dateScheduledSort',
                                         'pagerWidth'    => 150,
                                         'pagerOptional' => true,
                                         'filterHaving' => 'DATE_FORMAT(candidate.schedule_date, \'%m-%d-%y %r\')'),

            /* This one only works when called from the saved list view.  Thats why it is not optional, filterable, or exportable.
             * FIXME:  Somehow make this defined in the associated savedListDataGrid class child.
             */
            'Added To List' =>  array('select'   => 'DATE_FORMAT(saved_list_entry.date_created, \'%m-%d-%y\') AS dateAddedToList,
                                                     saved_list_entry.date_created AS dateAddedToListSort',
                                     'pagerRender'      => 'return $rsData[\'dateAddedToList\'];',
                                     'sortableColumn'     => 'dateAddedToListSort',
                                     'pagerWidth'    => 60,
                                     'pagerOptional' => false,
                                     'filterable' => false,
                                     'exportable' => false),

            'OwnerID' =>       array('select'    => '',
                                     'filter'    => 'candidate.owner',
                                     'pagerOptional' => false,
                                     'filterable' => false,
                                     'filterDescription' => 'Only My Candidates'),

            'IsHot' =>         array('select'    => '',
                                     'filter'    => 'candidate.is_hot',
                                     'pagerOptional' => false,
                                     'filterable' => false,
                                     'filterDescription' => 'Only Hot Candidates'),
                            'Campaign' => $GLOBALS['sheesh']
        );

        if (US_ZIPS_ENABLED)
        {
            $this->_classColumns['Near Zipcode'] =
                               array('select'  => 'candidate.zip AS zip',
                                     'filter' => 'candidate.zip',
                                     'pagerOptional' => false,
                                     'filterTypes'   => '=@');
        }

        /* Extra fields get added as columns here. */
        $candidates = new Candidates($this->_siteID);
        $extraFieldsRS = $candidates->extraFields->getSettings();
        foreach ($extraFieldsRS as $index => $data)
        {
            $fieldName = $data['fieldName'];

            if (!isset($this->_classColumns[$fieldName]))
            {
                $columnDefinition = $candidates->extraFields->getDataGridDefinition($index, $data, $this->_db);

                /* Return false for extra fields that should not be columns. */
                if ($columnDefinition !== false)
                {
                    $this->_classColumns[$fieldName] = $columnDefinition;
                }
            }
          }
        } 
        else if( ($_REQUEST['a']=='view_all' || $_REQUEST['delrec']=='view_all' || $_REQUEST['listrec']=='view_all') && $_REQUEST['m']!='lists') {
        $this->_dataItemIDColumn = 'candidate.candidate_id';

        $this->_classColumns = array(
            'Attachments' => array('select' => 'IF(candidate_joborder_submitted.candidate_joborder_id, 1, 0) AS submitted,
                                                IF(attachment_id, 1, 0) AS attachmentPresent',

                                     'pagerRender' => 'if ($rsData[\'submitted\'] == 1)
                                                    {
                                                        $return = \'<img src="images/job_orders.gif" alt="" width="16" height="16" title="Submitted for a Job Order" />\';
                                                    }
                                                    else
                                                    {
                                                        $return = \'<img src="images/mru/blank.gif" alt="" width="16" height="16" />\';
                                                    }

                                                    if ($rsData[\'attachmentPresent\'] == 1)
                                                    {
                                                        $return .= \'<img src="images/paperclip.gif" alt="" width="16" height="16" title="Attachment Present" />\';
                                                    }
                                                    else
                                                    {
                                                        $return .= \'<img src="images/mru/blank.gif" alt="" width="16" height="16" />\';
                                                    }

                                                    return $return;
                                                   ',

                                     'join'     => 'LEFT JOIN attachment
                                                        ON candidate.candidate_id = attachment.data_item_id
                                                        AND attachment.data_item_type = '.DATA_ITEM_CANDIDATE.'
                                                    LEFT JOIN candidate_joborder AS candidate_joborder_submitted
                                                        ON candidate_joborder_submitted.candidate_id = candidate.candidate_id
                                                        AND candidate_joborder_submitted.status >= '.PIPELINE_STATUS_SUBMITTED.'
                                                        AND candidate_joborder_submitted.site_id = '.$this->_siteID.'
                                                        AND candidate_joborder_submitted.status != '.PIPELINE_STATUS_NOTINCONSIDERATION,
                                     'pagerWidth'    => 34,
                                     'pagerOptional' => true,
                                     'pagerNoTitle' => true,
                                     'sizable'  => false,
                                     'exportable' => false,
                                     'filterable' => false),

            'First Name' =>     array('select'         => 'candidate.first_name AS firstName',
                                      'pagerRender'    => 'if ($rsData[\'isHot\'] == 1) $className =  \'jobLinkHot\'; else $className = \'jobLinkCold\'; return \'<a href="'.RESUFLOUtility::getIndexName().'?m=candidates&amp;list='.$this->get['a'].'&amp;a=show&amp;candidateID=\'.$rsData[\'candidateID\'].\'" class="\'.$className.\'">\'.htmlspecialchars($rsData[\'firstName\']).\'</a>\';',
                                      'sortableColumn' => 'firstName',
                                      'pagerWidth'     => 75,
                                      'pagerOptional'  => false,
                                      'alphaNavigation'=> true,
                                      'filter'         => 'candidate.first_name'),

            'Last Name' =>      array('select'         => 'candidate.last_name AS lastName',
                                     'sortableColumn'  => 'lastName',
                                     'pagerRender'     => 'if ($rsData[\'isHot\'] == 1) $className =  \'jobLinkHot\'; else $className = \'jobLinkCold\'; return \'<a href="'.RESUFLOUtility::getIndexName().'?m=candidates&amp;list='.$this->get['a'].'&amp;a=show&amp;candidateID=\'.$rsData[\'candidateID\'].\'" class="\'.$className.\'">\'.htmlspecialchars($rsData[\'lastName\']).\'</a>\';',
                                     'pagerWidth'      => 85,
                                     'pagerOptional'   => false,
                                     'alphaNavigation' => true,
                                     'filter'         => 'candidate.last_name'),

            'E-Mail' =>         array('select'   => 'candidate.email1 AS email1',
                                     'sortableColumn'     => 'email1',
                                     'pagerWidth'    => 80,
                                     'filter'         => 'candidate.email1'),

            '2nd E-Mail' =>     array('select'   => 'candidate.email2 AS email2',
                                     'sortableColumn'     => 'email2',
                                     'pagerWidth'    => 80,
                                     'filter'         => 'candidate.email2'),

            'Home Phone' =>     array('select'   => 'candidate.phone_home AS phoneHome',
                                     'sortableColumn'     => 'phoneHome',
                                     'pagerWidth'    => 80,
                                     'filter'         => 'candidate.phone_home'),

            'Cell Phone' =>     array('select'   => 'candidate.phone_cell AS phoneCell',
                                     'sortableColumn'     => 'phoneCell',
                                     'pagerWidth'    => 80,
                                     'filter'         => 'candidate.phone_cell'),

            'Work Phone' =>     array('select'   => 'candidate.phone_work AS phoneWork',
                                     'sortableColumn'     => 'phoneWork',
                                     'pagerWidth'    => 80),

            'Address' =>        array('select'   => 'candidate.address AS address',
                                     'sortableColumn'     => 'address',
                                     'pagerWidth'    => 250,
                                     'alphaNavigation' => true,
                                     'filter'         => 'candidate.address'),

            'City' =>           array('select'   => 'candidate.city AS city',
                                     'sortableColumn'     => 'city',
                                     'pagerWidth'    => 80,
                                     'alphaNavigation' => true,
                                     'filter'         => 'candidate.city'),
              'Company' =>           array('select'   => 'Case When candidate.entered_by = '.$_SESSION['RESUFLO']->getUserId().' then "'.$_SESSION['RESUFLO']->getFirstName().'" else (select first_name from user where user_id =(select entered_by from user where user_id=candidate.entered_by) ) End as company',
                                     'sortableColumn'     => 'company',
                                     'pagerWidth'    => 80,
                                     'alphaNavigation' => true,
                                     'filter'         => 'candidate.entered_by'),


            'State' =>          array('select'   => 'candidate.state AS state',
                                     'sortableColumn'     => 'state',
                                     'filterType' => 'dropDown',
                                     'pagerWidth'    => 50,
                                     'alphaNavigation' => true,
                                     'filter'         => 'candidate.state'),

            'Zip' =>            array('select'  => 'candidate.zip AS zip',
                                     'sortableColumn'    => 'zip',
                                     'pagerWidth'   => 50,
                                     'filter'         => 'candidate.zip'),

            'Misc Notes' =>     array('select'  => 'candidate.notes AS notes',
                                     'sortableColumn'    => 'notes',
                                     'pagerWidth'   => 300,
                                     'filter'         => 'candidate.notes'),

            'Web Site' =>      array('select'  => 'candidate.web_site AS webSite',
                                     'pagerRender'     => 'return \'<a href="\'.htmlspecialchars($rsData[\'webSite\']).\'">\'.htmlspecialchars($rsData[\'webSite\']).\'</a>\';',
                                     'sortableColumn'    => 'webSite',
                                     'pagerWidth'   => 80,
                                     'filter'         => 'candidate.web_site'),

            'Key Skills' =>    array('select'  => 'candidate.key_skills AS keySkills',
                                     'pagerRender' => 'return substr(trim($rsData[\'keySkills\']), 0, 30) . (strlen(trim($rsData[\'keySkills\'])) > 30 ? \'...\' : \'\');',
                                     'sortableColumn'    => 'keySkills',
                                     'pagerWidth'   => 210,
                                     'filter'         => 'candidate.key_skills'),

            'Recent Status' => array('select'  => '(
                                                    SELECT
                                                        CONCAT(
                                                            \'<a href="'.RESUFLOUtility::getIndexName().'?m=joborders&amp;list='.$this->get['a'].'&amp;a=show&amp;jobOrderID=\',
                                                            joborder.joborder_id,
                                                            \'" title="\',
                                                            joborder.title,
                                                            \' (\',
                                                            company.name,
                                                            \')">\',
                                                            candidate_joborder_status.short_description,
                                                            \'</a>\'
                                                        )
                                                    FROM
                                                        candidate_joborder
                                                    LEFT JOIN candidate_joborder_status
                                                        ON candidate_joborder_status.candidate_joborder_status_id = candidate_joborder.status
                                                    LEFT JOIN joborder
                                                        ON joborder.joborder_id = candidate_joborder.joborder_id
                                                    LEFT JOIN company
                                                        ON joborder.company_id = company.company_id
                                                    WHERE
                                                        candidate_joborder.candidate_id = candidate.candidate_id
                                                    ORDER BY
                                                        candidate_joborder.date_modified DESC
                                                    LIMIT 1
                                                ) AS lastStatus
                                                ',
                                     'sort'    => 'lastStatus',
                                     'pagerRender'     => 'return $rsData[\'lastStatus\'];',
                                     'exportRender'     => 'return $rsData[\'lastStatus\'];',
                                     'pagerWidth'   => 140,
                                     'exportable' => false,
                                     'filterHaving'  => 'lastStatus',
                                     'filterTypes'   => '=~'),

'Activity Notes' =>     array('select'  => 'candidate.activity_notes AS activity_notes',
                                                     'sortableColumn'    => 'activity_notes',
                                                     'pagerWidth'   => 300,
                                                     'filter'         => 'candidate.activity_notes'),
                                     
            'Recent Status (Extended)' => array('select'  => '(
                                                    SELECT
                                                        CONCAT(
                                                            candidate_joborder_status.short_description,
                                                            \'<br />\',
                                                            \'<a href="'.RESUFLOUtility::getIndexName().'?m=companies&amp;list='.$this->get['a'].'&amp;a=show&amp;companyID=\',
                                                            company.company_id,
                                                            \'">\',
                                                            company.name,
                                                            \'</a> - \',
                                                            \'<a href="'.RESUFLOUtility::getIndexName().'?m=joborders&amp;list='.$this->get['a'].'&amp;a=show&amp;jobOrderID=\',
                                                            joborder.joborder_id,
                                                            \'">\',
                                                            joborder.title,
                                                            \'</a>\'
                                                        )
                                                    FROM
                                                        candidate_joborder
                                                    LEFT JOIN candidate_joborder_status
                                                        ON candidate_joborder_status.candidate_joborder_status_id = candidate_joborder.status
                                                    LEFT JOIN joborder
                                                        ON joborder.joborder_id = candidate_joborder.joborder_id
                                                    LEFT JOIN company
                                                        ON joborder.company_id = company.company_id
                                                    WHERE
                                                        candidate_joborder.candidate_id = candidate.candidate_id
                                                    ORDER BY
                                                        candidate_joborder.date_modified DESC
                                                    LIMIT 1
                                                ) AS lastStatusLong
                                                ',
                                     'sortableColumn'    => 'lastStatusLong',
                                     'pagerRender'     => 'return $rsData[\'lastStatusLong\'];',
                                     'pagerWidth'   => 310,
                                     'exportable' => false,
                                     'filterable' => false),

            'Source' =>        array('select'  => 'candidate.source AS source',
                                     'sortableColumn'    => 'source',
                                     'pagerWidth'   => 140,
                                     'alphaNavigation' => true,
                                     'filter'         => 'candidate.source'),

            'Available' =>     array('select'   => 'DATE_FORMAT(candidate.date_available, \'%m-%d-%y\') AS dateAvailable',
                                     'sortableColumn'     => 'dateAvailable',
                                     'pagerWidth'    => 60),

            'Current Employer' => array('select'  => 'candidate.current_employer AS currentEmployer',
                                     'sortableColumn'    => 'currentEmployer',
                                     'pagerWidth'   => 125,
                                     'alphaNavigation' => true,
                                     'filter'         => 'candidate.current_employer'),

            'Current Pay' => array('select'  => 'candidate.current_pay AS currentPay',
                                     'sortableColumn'    => 'currentPay',
                                     'pagerWidth'   => 125,
                                     'filter'         => 'candidate.current_pay',
                                     'filterTypes'   => '===>=<'),

            'Desired Pay' => array('select'  => 'candidate.desired_pay AS desiredPay',
                                     'sortableColumn'    => 'desiredPay',
                                     'pagerWidth'   => 125,
                                     'filter'         => 'candidate.desired_pay',
                                     'filterTypes'   => '===>=<'),

            'Can Relocate'  => array('select'  => 'candidate.can_relocate AS canRelocate',
                                     'pagerRender'     => 'return ($rsData[\'canRelocate\'] == 0 ? \'No\' : \'Yes\');',
                                     'exportRender'     => 'return ($rsData[\'canRelocate\'] == 0 ? \'No\' : \'Yes\');',
                                     'sortableColumn'    => 'canRelocate',
                                     'pagerWidth'   => 80,
                                     'filter'         => 'candidate.can_relocate'),

            'Recruiter' =>         array('select'   => 'owner_user.first_name AS ownerFirstName,' .
                                                   'owner_user.last_name AS ownerLastName,' .
                                                   'CONCAT(owner_user.last_name, owner_user.first_name) AS ownerSort',
                                     'join'     => 'LEFT JOIN user AS owner_user ON candidate.owner = owner_user.user_id',
                                     'pagerRender'      => 'return StringUtility::makeInitialName($rsData[\'ownerFirstName\'], $rsData[\'ownerLastName\'], false, LAST_NAME_MAXLEN);',
                                     'exportRender'     => 'return $rsData[\'ownerFirstName\'] . " " .$rsData[\'ownerLastName\'];',
                                     'sortableColumn'     => 'ownerSort',
                                     'pagerWidth'    => 75,
                                     'alphaNavigation' => true,
                                     'filter'         => 'CONCAT(owner_user.first_name, owner_user.last_name)'),

            'Created' =>       array('select'   => 'DATE_FORMAT(candidate.date_created, \'%m-%d-%y\') AS dateCreated',
                                     'pagerRender'      => 'return $rsData[\'dateCreated\'];',
                                     'sortableColumn'     => 'dateCreatedSort',
                                     'pagerWidth'    => 60,
                                     'filterHaving' => 'DATE_FORMAT(candidate.date_created, \'%m-%d-%y\')'),
            
            'Date From' =>       array('select'   => 'DATE_FORMAT(candidate.date_created, \'%m-%d-%y\') AS dateCreated',
                                     'pagerRender'      => 'return $rsData[\'dateCreated\'];',
                                     'sortableColumn'     => 'dateCreatedSort',
                                     'pagerWidth'    => 60,
                                     'filterHaving' => 'DATE_FORMAT(candidate.date_created, \'%y-%m-%d\')',
                                     'filterTypes'   => '=>'),
                
            'Date To' =>       array('select'   => 'DATE_FORMAT(candidate.date_created, \'%m-%d-%y\') AS dateTo',
                                     'pagerRender'      => 'return $rsData[\'dateTo\'];',
                                     'sortableColumn'     => 'dateToSort',
                                     'pagerWidth'    => 60,
                                     'filterHaving' => 'DATE_FORMAT(candidate.date_created, \'%y-%m-%d\')',
                                     'filterTypes'   => '=<'),

            'Modified' =>      array('select'   => 'DATE_FORMAT(candidate.date_modified, \'%m-%d-%y\') AS dateModified',
                                     'pagerRender'      => 'return $rsData[\'dateModified\'];',
                                     'sortableColumn'     => 'dateModifiedSort',
                                     'pagerWidth'    => 60,
                                     'pagerOptional' => false,
                                     'filterHaving' => 'DATE_FORMAT(candidate.date_modified, \'%m-%d-%y\')'),
            
            'Scheduled' =>      array('select'   => 'DATE_FORMAT(candidate.schedule_date, \'%m-%d-%y %r\') AS dateScheduled',
                                         'pagerRender'      => 'return $rsData[\'dateScheduled\'];',
                                         'sortableColumn'     => 'dateScheduledSort',
                                         'pagerWidth'    => 150,
                                         'pagerOptional' => true,
                                         'filterHaving' => 'DATE_FORMAT(candidate.schedule_date, \'%m-%d-%y %r\')'),

            /* This one only works when called from the saved list view.  Thats why it is not optional, filterable, or exportable.
             * FIXME:  Somehow make this defined in the associated savedListDataGrid class child.
             */
            'Added To List' =>  array('select'   => 'DATE_FORMAT(saved_list_entry.date_created, \'%m-%d-%y\') AS dateAddedToList,
                                                     saved_list_entry.date_created AS dateAddedToListSort',
                                     'pagerRender'      => 'return $rsData[\'dateAddedToList\'];',
                                     'sortableColumn'     => 'dateAddedToListSort',
                                     'pagerWidth'    => 60,
                                     'pagerOptional' => false,
                                     'filterable' => false,
                                     'exportable' => false),

            'OwnerID' =>       array('select'    => '',
                                     'filter'    => 'candidate.owner',
                                     'pagerOptional' => false,
                                     'filterable' => false,
                                     'filterDescription' => 'Only My Candidates'),

            'IsHot' =>         array('select'    => '',
                                     'filter'    => 'candidate.is_hot',
                                     'pagerOptional' => false,
                                     'filterable' => false,
                                     'filterDescription' => 'Only Hot Candidates'),
                            'Campaign' => $GLOBALS['sheesh']
        );

        if (US_ZIPS_ENABLED)
        {
            $this->_classColumns['Near Zipcode'] =
                               array('select'  => 'candidate.zip AS zip',
                                     'filter' => 'candidate.zip',
                                     'pagerOptional' => false,
                                     'filterTypes'   => '=@');
        }

        /* Extra fields get added as columns here. */
        $candidates = new Candidates($this->_siteID);
        $extraFieldsRS = $candidates->extraFields->getSettings();
        foreach ($extraFieldsRS as $index => $data)
        {
            $fieldName = $data['fieldName'];

            if (!isset($this->_classColumns[$fieldName]))
            {
                $columnDefinition = $candidates->extraFields->getDataGridDefinition($index, $data, $this->_db);

                /* Return false for extra fields that should not be columns. */
                if ($columnDefinition !== false)
                {
                    $this->_classColumns[$fieldName] = $columnDefinition;
                }
            }
          }

        } 
        else if($_REQUEST['m']=='lists' && $_REQUEST['listrec']=='view_all') {
            $listId = $_REQUEST['savedListID'];
            $this->_dataItemIDColumn = 'candidate.candidate_id';
    
            $this->_classColumns = array(
            'Attachments' => array('select' => 'IF(candidate_joborder_submitted.candidate_joborder_id, 1, 0) AS submitted,
                                                IF(attachment_id, 1, 0) AS attachmentPresent',

                                     'pagerRender' => 'if ($rsData[\'submitted\'] == 1)
                                                    {
                                                        $return = \'<img src="images/job_orders.gif" alt="" width="16" height="16" title="Submitted for a Job Order" />\';
                                                    }
                                                    else
                                                    {
                                                        $return = \'<img src="images/mru/blank.gif" alt="" width="16" height="16" />\';
                                                    }

                                                    if ($rsData[\'attachmentPresent\'] == 1)
                                                    {
                                                        $return .= \'<img src="images/paperclip.gif" alt="" width="16" height="16" title="Attachment Present" />\';
                                                    }
                                                    else
                                                    {
                                                        $return .= \'<img src="images/mru/blank.gif" alt="" width="16" height="16" />\';
                                                    }

                                                    return $return;
                                                   ',

                                     'join'     => 'LEFT JOIN attachment
                                                        ON candidate.candidate_id = attachment.data_item_id
                                                        AND attachment.data_item_type = '.DATA_ITEM_CANDIDATE.'
                                                    LEFT JOIN candidate_joborder AS candidate_joborder_submitted
                                                        ON candidate_joborder_submitted.candidate_id = candidate.candidate_id
                                                        AND candidate_joborder_submitted.status >= '.PIPELINE_STATUS_SUBMITTED.'
                                                        AND candidate_joborder_submitted.site_id = '.$this->_siteID.'
                                                        AND candidate_joborder_submitted.status != '.PIPELINE_STATUS_NOTINCONSIDERATION,
                                     'pagerWidth'    => 34,
                                     'pagerOptional' => true,
                                     'pagerNoTitle' => true,
                                     'sizable'  => false,
                                     'exportable' => false,
                                     'filterable' => false),

            'First Name' =>     array('select'         => 'candidate.first_name AS firstName',
                                      'pagerRender'    => 'if ($rsData[\'isHot\'] == 1) $className =  \'jobLinkHot\'; else $className = \'jobLinkCold\'; return \'<a href="'.RESUFLOUtility::getIndexName().'?m=candidates&amp;list='.$this->get['a'].'&amp;a=show&amp;list='.$listId.'&amp;candidateID=\'.$rsData[\'candidateID\'].\'" class="\'.$className.\'">\'.htmlspecialchars($rsData[\'firstName\']).\'</a>\';',
                                      'sortableColumn' => 'firstName',
                                      'pagerWidth'     => 75,
                                      'pagerOptional'  => false,
                                      'alphaNavigation'=> true,
                                      'filter'         => 'candidate.first_name'),

            'Last Name' =>      array('select'         => 'candidate.last_name AS lastName',
                                     'sortableColumn'  => 'lastName',
                                     'pagerRender'     => 'if ($rsData[\'isHot\'] == 1) $className =  \'jobLinkHot\'; else $className = \'jobLinkCold\'; return \'<a href="'.RESUFLOUtility::getIndexName().'?m=candidates&amp;list='.$this->get['a'].'&amp;a=show&amp;list='.$listId.'&amp;candidateID=\'.$rsData[\'candidateID\'].\'" class="\'.$className.\'">\'.htmlspecialchars($rsData[\'lastName\']).\'</a>\';',
                                     'pagerWidth'      => 85,
                                     'pagerOptional'   => false,
                                     'alphaNavigation' => true,
                                     'filter'         => 'candidate.last_name'),

            'E-Mail' =>         array('select'   => 'candidate.email1 AS email1',
                                     'sortableColumn'     => 'email1',
                                     'pagerWidth'    => 80,
                                     'filter'         => 'candidate.email1'),

            '2nd E-Mail' =>     array('select'   => 'candidate.email2 AS email2',
                                     'sortableColumn'     => 'email2',
                                     'pagerWidth'    => 80,
                                     'filter'         => 'candidate.email2'),

            'Home Phone' =>     array('select'   => 'candidate.phone_home AS phoneHome',
                                     'sortableColumn'     => 'phoneHome',
                                     'pagerWidth'    => 80,
                                     'filter'         => 'candidate.phone_home'),

            'Cell Phone' =>     array('select'   => 'candidate.phone_cell AS phoneCell',
                                     'sortableColumn'     => 'phoneCell',
                                     'pagerWidth'    => 80,
                                     'filter'         => 'candidate.phone_cell'),

            'Work Phone' =>     array('select'   => 'candidate.phone_work AS phoneWork',
                                     'sortableColumn'     => 'phoneWork',
                                     'pagerWidth'    => 80),

            'Address' =>        array('select'   => 'candidate.address AS address',
                                     'sortableColumn'     => 'address',
                                     'pagerWidth'    => 250,
                                     'alphaNavigation' => true,
                                     'filter'         => 'candidate.address'),

            'City' =>           array('select'   => 'candidate.city AS city',
                                     'sortableColumn'     => 'city',
                                     'pagerWidth'    => 80,
                                     'alphaNavigation' => true,
                                     'filter'         => 'candidate.city'),
              'Company' =>           array('select'   => 'Case When candidate.entered_by = '.$_SESSION['RESUFLO']->getUserId().' then "'.$_SESSION['RESUFLO']->getFirstName().'" else (select first_name from user where user_id =(select entered_by from user where user_id=candidate.entered_by) ) End as company',
                                     'sortableColumn'     => 'company',
                                     'pagerWidth'    => 80,
                                     'alphaNavigation' => true,
                                     'filter'         => 'candidate.entered_by'),


            'State' =>          array('select'   => 'candidate.state AS state',
                                     'sortableColumn'     => 'state',
                                     'filterType' => 'dropDown',
                                     'pagerWidth'    => 50,
                                     'alphaNavigation' => true,
                                     'filter'         => 'candidate.state'),

            'Zip' =>            array('select'  => 'candidate.zip AS zip',
                                     'sortableColumn'    => 'zip',
                                     'pagerWidth'   => 50,
                                     'filter'         => 'candidate.zip'),

            'Misc Notes' =>     array('select'  => 'candidate.notes AS notes',
                                     'sortableColumn'    => 'notes',
                                     'pagerWidth'   => 300,
                                     'filter'         => 'candidate.notes'),

            'Web Site' =>      array('select'  => 'candidate.web_site AS webSite',
                                     'pagerRender'     => 'return \'<a href="\'.htmlspecialchars($rsData[\'webSite\']).\'">\'.htmlspecialchars($rsData[\'webSite\']).\'</a>\';',
                                     'sortableColumn'    => 'webSite',
                                     'pagerWidth'   => 80,
                                     'filter'         => 'candidate.web_site'),

            'Key Skills' =>    array('select'  => 'candidate.key_skills AS keySkills',
                                     'pagerRender' => 'return substr(trim($rsData[\'keySkills\']), 0, 30) . (strlen(trim($rsData[\'keySkills\'])) > 30 ? \'...\' : \'\');',
                                     'sortableColumn'    => 'keySkills',
                                     'pagerWidth'   => 210,
                                     'filter'         => 'candidate.key_skills'),

            'Recent Status' => array('select'  => '(
                                                    SELECT
                                                        CONCAT(
                                                            \'<a href="'.RESUFLOUtility::getIndexName().'?m=joborders&amp;list='.$this->get['a'].'&amp;a=show&amp;jobOrderID=\',
                                                            joborder.joborder_id,
                                                            \'" title="\',
                                                            joborder.title,
                                                            \' (\',
                                                            company.name,
                                                            \')">\',
                                                            candidate_joborder_status.short_description,
                                                            \'</a>\'
                                                        )
                                                    FROM
                                                        candidate_joborder
                                                    LEFT JOIN candidate_joborder_status
                                                        ON candidate_joborder_status.candidate_joborder_status_id = candidate_joborder.status
                                                    LEFT JOIN joborder
                                                        ON joborder.joborder_id = candidate_joborder.joborder_id
                                                    LEFT JOIN company
                                                        ON joborder.company_id = company.company_id
                                                    WHERE
                                                        candidate_joborder.candidate_id = candidate.candidate_id
                                                    ORDER BY
                                                        candidate_joborder.date_modified DESC
                                                    LIMIT 1
                                                ) AS lastStatus
                                                ',
                                     'sort'    => 'lastStatus',
                                     'pagerRender'     => 'return $rsData[\'lastStatus\'];',
                                     'exportRender'     => 'return $rsData[\'lastStatus\'];',
                                     'pagerWidth'   => 140,
                                     'exportable' => false,
                                     'filterHaving'  => 'lastStatus',
                                     'filterTypes'   => '=~'),

'Activity Notes' =>     array('select'  => 'candidate.activity_notes AS activity_notes',
                                                     'sortableColumn'    => 'activity_notes',
                                                     'pagerWidth'   => 300,
                                                     'filter'         => 'candidate.activity_notes'),
                                     
            'Recent Status (Extended)' => array('select'  => '(
                                                    SELECT
                                                        CONCAT(
                                                            candidate_joborder_status.short_description,
                                                            \'<br />\',
                                                            \'<a href="'.RESUFLOUtility::getIndexName().'?m=companies&amp;list='.$this->get['a'].'&amp;a=show&amp;companyID=\',
                                                            company.company_id,
                                                            \'">\',
                                                            company.name,
                                                            \'</a> - \',
                                                            \'<a href="'.RESUFLOUtility::getIndexName().'?m=joborders&amp;list='.$this->get['a'].'&amp;a=show&amp;jobOrderID=\',
                                                            joborder.joborder_id,
                                                            \'">\',
                                                            joborder.title,
                                                            \'</a>\'
                                                        )
                                                    FROM
                                                        candidate_joborder
                                                    LEFT JOIN candidate_joborder_status
                                                        ON candidate_joborder_status.candidate_joborder_status_id = candidate_joborder.status
                                                    LEFT JOIN joborder
                                                        ON joborder.joborder_id = candidate_joborder.joborder_id
                                                    LEFT JOIN company
                                                        ON joborder.company_id = company.company_id
                                                    WHERE
                                                        candidate_joborder.candidate_id = candidate.candidate_id
                                                    ORDER BY
                                                        candidate_joborder.date_modified DESC
                                                    LIMIT 1
                                                ) AS lastStatusLong
                                                ',
                                     'sortableColumn'    => 'lastStatusLong',
                                     'pagerRender'     => 'return $rsData[\'lastStatusLong\'];',
                                     'pagerWidth'   => 310,
                                     'exportable' => false,
                                     'filterable' => false),

            'Source' =>        array('select'  => 'candidate.source AS source',
                                     'sortableColumn'    => 'source',
                                     'pagerWidth'   => 140,
                                     'alphaNavigation' => true,
                                     'filter'         => 'candidate.source'),

            'Available' =>     array('select'   => 'DATE_FORMAT(candidate.date_available, \'%m-%d-%y\') AS dateAvailable',
                                     'sortableColumn'     => 'dateAvailable',
                                     'pagerWidth'    => 60),

            'Current Employer' => array('select'  => 'candidate.current_employer AS currentEmployer',
                                     'sortableColumn'    => 'currentEmployer',
                                     'pagerWidth'   => 125,
                                     'alphaNavigation' => true,
                                     'filter'         => 'candidate.current_employer'),

            'Current Pay' => array('select'  => 'candidate.current_pay AS currentPay',
                                     'sortableColumn'    => 'currentPay',
                                     'pagerWidth'   => 125,
                                     'filter'         => 'candidate.current_pay',
                                     'filterTypes'   => '===>=<'),

            'Desired Pay' => array('select'  => 'candidate.desired_pay AS desiredPay',
                                     'sortableColumn'    => 'desiredPay',
                                     'pagerWidth'   => 125,
                                     'filter'         => 'candidate.desired_pay',
                                     'filterTypes'   => '===>=<'),

            'Can Relocate'  => array('select'  => 'candidate.can_relocate AS canRelocate',
                                     'pagerRender'     => 'return ($rsData[\'canRelocate\'] == 0 ? \'No\' : \'Yes\');',
                                     'exportRender'     => 'return ($rsData[\'canRelocate\'] == 0 ? \'No\' : \'Yes\');',
                                     'sortableColumn'    => 'canRelocate',
                                     'pagerWidth'   => 80,
                                     'filter'         => 'candidate.can_relocate'),

            'Recruiter' =>         array('select'   => 'owner_user.first_name AS ownerFirstName,' .
                                                   'owner_user.last_name AS ownerLastName,' .
                                                   'CONCAT(owner_user.last_name, owner_user.first_name) AS ownerSort',
                                     'join'     => 'LEFT JOIN user AS owner_user ON candidate.owner = owner_user.user_id',
                                     'pagerRender'      => 'return StringUtility::makeInitialName($rsData[\'ownerFirstName\'], $rsData[\'ownerLastName\'], false, LAST_NAME_MAXLEN);',
                                     'exportRender'     => 'return $rsData[\'ownerFirstName\'] . " " .$rsData[\'ownerLastName\'];',
                                     'sortableColumn'     => 'ownerSort',
                                     'pagerWidth'    => 75,
                                     'alphaNavigation' => true,
                                     'filter'         => 'CONCAT(owner_user.first_name, owner_user.last_name)'),

            'Created' =>       array('select'   => 'DATE_FORMAT(candidate.date_created, \'%m-%d-%y\') AS dateCreated',
                                     'pagerRender'      => 'return $rsData[\'dateCreated\'];',
                                     'sortableColumn'     => 'dateCreatedSort',
                                     'pagerWidth'    => 60,
                                     'filterHaving' => 'DATE_FORMAT(candidate.date_created, \'%m-%d-%y\')'),
            
            'Date From' =>       array('select'   => 'DATE_FORMAT(candidate.date_created, \'%m-%d-%y\') AS dateCreated',
                                     'pagerRender'      => 'return $rsData[\'dateCreated\'];',
                                     'sortableColumn'     => 'dateCreatedSort',
                                     'pagerWidth'    => 60,
                                     'filterHaving' => 'DATE_FORMAT(candidate.date_created, \'%y-%m-%d\')',
                                     'filterTypes'   => '=>'),
                
            'Date To' =>       array('select'   => 'DATE_FORMAT(candidate.date_created, \'%m-%d-%y\') AS dateTo',
                                     'pagerRender'      => 'return $rsData[\'dateTo\'];',
                                     'sortableColumn'     => 'dateToSort',
                                     'pagerWidth'    => 60,
                                     'filterHaving' => 'DATE_FORMAT(candidate.date_created, \'%y-%m-%d\')',
                                     'filterTypes'   => '=<'),

            'Modified' =>      array('select'   => 'DATE_FORMAT(candidate.date_modified, \'%m-%d-%y\') AS dateModified',
                                     'pagerRender'      => 'return $rsData[\'dateModified\'];',
                                     'sortableColumn'     => 'dateModifiedSort',
                                     'pagerWidth'    => 60,
                                     'pagerOptional' => false,
                                     'filterHaving' => 'DATE_FORMAT(candidate.date_modified, \'%m-%d-%y\')'),
                
            'Scheduled' =>      array('select'   => 'DATE_FORMAT(candidate.schedule_date, \'%m-%d-%y %r\') AS dateScheduled',
                                         'pagerRender'      => 'return $rsData[\'dateScheduled\'];',
                                         'sortableColumn'     => 'dateScheduledSort',
                                         'pagerWidth'    => 150,
                                         'pagerOptional' => true,
                                         'filterHaving' => 'DATE_FORMAT(candidate.schedule_date, \'%m-%d-%y %r\')'),

            /* This one only works when called from the saved list view.  Thats why it is not optional, filterable, or exportable.
             * FIXME:  Somehow make this defined in the associated savedListDataGrid class child.
             */
            'Added To List' =>  array('select'   => 'DATE_FORMAT(saved_list_entry.date_created, \'%m-%d-%y\') AS dateAddedToList,
                                                     saved_list_entry.date_created AS dateAddedToListSort',
                                     'pagerRender'      => 'return $rsData[\'dateAddedToList\'];',
                                     'sortableColumn'     => 'dateAddedToListSort',
                                     'pagerWidth'    => 60,
                                     'pagerOptional' => false,
                                     'filterable' => false,
                                     'exportable' => false),

            'OwnerID' =>       array('select'    => '',
                                     'filter'    => 'candidate.owner',
                                     'pagerOptional' => false,
                                     'filterable' => false,
                                     'filterDescription' => 'Only My Candidates'),

            'IsHot' =>         array('select'    => '',
                                     'filter'    => 'candidate.is_hot',
                                     'pagerOptional' => false,
                                     'filterable' => false,
                                     'filterDescription' => 'Only Hot Candidates'),
                            'Campaign' => $GLOBALS['sheesh']
        );

        if (US_ZIPS_ENABLED)
        {
            $this->_classColumns['Near Zipcode'] =
                               array('select'  => 'candidate.zip AS zip',
                                     'filter' => 'candidate.zip',
                                     'pagerOptional' => false,
                                     'filterTypes'   => '=@');
        }

        /* Extra fields get added as columns here. */
        $candidates = new Candidates($this->_siteID);
        $extraFieldsRS = $candidates->extraFields->getSettings();
        foreach ($extraFieldsRS as $index => $data)
        {
            $fieldName = $data['fieldName'];

            if (!isset($this->_classColumns[$fieldName]))
            {
                $columnDefinition = $candidates->extraFields->getDataGridDefinition($index, $data, $this->_db);

                /* Return false for extra fields that should not be columns. */
                if ($columnDefinition !== false)
                {
                    $this->_classColumns[$fieldName] = $columnDefinition;
                }
            }
          }

        } 
        else if($_REQUEST['a']=='view_all' || $_REQUEST['delrec']=='view_all' || $_REQUEST['listrec']=='view_all') {
        $this->_dataItemIDColumn = 'candidate.candidate_id';

        $this->_classColumns = array(
            'Attachments' => array('select' => 'IF(candidate_joborder_submitted.candidate_joborder_id, 1, 0) AS submitted,
                                                IF(attachment_id, 1, 0) AS attachmentPresent',

                                     'pagerRender' => 'if ($rsData[\'submitted\'] == 1)
                                                    {
                                                        $return = \'<img src="images/job_orders.gif" alt="" width="16" height="16" title="Submitted for a Job Order" />\';
                                                    }
                                                    else
                                                    {
                                                        $return = \'<img src="images/mru/blank.gif" alt="" width="16" height="16" />\';
                                                    }

                                                    if ($rsData[\'attachmentPresent\'] == 1)
                                                    {
                                                        $return .= \'<img src="images/paperclip.gif" alt="" width="16" height="16" title="Attachment Present" />\';
                                                    }
                                                    else
                                                    {
                                                        $return .= \'<img src="images/mru/blank.gif" alt="" width="16" height="16" />\';
                                                    }

                                                    return $return;
                                                   ',

                                     'join'     => 'LEFT JOIN attachment
                                                        ON candidate.candidate_id = attachment.data_item_id
                                                        AND attachment.data_item_type = '.DATA_ITEM_CANDIDATE.'
                                                    LEFT JOIN candidate_joborder AS candidate_joborder_submitted
                                                        ON candidate_joborder_submitted.candidate_id = candidate.candidate_id
                                                        AND candidate_joborder_submitted.status >= '.PIPELINE_STATUS_SUBMITTED.'
                                                        AND candidate_joborder_submitted.site_id = '.$this->_siteID.'
                                                        AND candidate_joborder_submitted.status != '.PIPELINE_STATUS_NOTINCONSIDERATION,
                                     'pagerWidth'    => 34,
                                     'pagerOptional' => true,
                                     'pagerNoTitle' => true,
                                     'sizable'  => false,
                                     'exportable' => false,
                                     'filterable' => false),

            'First Name' =>     array('select'         => 'candidate.first_name AS firstName',
                                      'pagerRender'    => 'if ($rsData[\'isHot\'] == 1) $className =  \'jobLinkHot\'; else $className = \'jobLinkCold\'; return \'<a href="'.RESUFLOUtility::getIndexName().'?m=candidates&amp;list='.$this->get['a'].'&amp;a=show&amp;candidateID=\'.$rsData[\'candidateID\'].\'" class="\'.$className.\'">\'.htmlspecialchars($rsData[\'firstName\']).\'</a>\';',
                                      'sortableColumn' => 'firstName',
                                      'pagerWidth'     => 75,
                                      'pagerOptional'  => false,
                                      'alphaNavigation'=> true,
                                      'filter'         => 'candidate.first_name'),

            'Last Name' =>      array('select'         => 'candidate.last_name AS lastName',
                                     'sortableColumn'  => 'lastName',
                                     'pagerRender'     => 'if ($rsData[\'isHot\'] == 1) $className =  \'jobLinkHot\'; else $className = \'jobLinkCold\'; return \'<a href="'.RESUFLOUtility::getIndexName().'?m=candidates&amp;list='.$this->get['a'].'&amp;a=show&amp;candidateID=\'.$rsData[\'candidateID\'].\'" class="\'.$className.\'">\'.htmlspecialchars($rsData[\'lastName\']).\'</a>\';',
                                     'pagerWidth'      => 85,
                                     'pagerOptional'   => false,
                                     'alphaNavigation' => true,
                                     'filter'         => 'candidate.last_name'),

            'E-Mail' =>         array('select'   => 'candidate.email1 AS email1',
                                     'sortableColumn'     => 'email1',
                                     'pagerWidth'    => 80,
                                     'filter'         => 'candidate.email1'),

            '2nd E-Mail' =>     array('select'   => 'candidate.email2 AS email2',
                                     'sortableColumn'     => 'email2',
                                     'pagerWidth'    => 80,
                                     'filter'         => 'candidate.email2'),

            'Home Phone' =>     array('select'   => 'candidate.phone_home AS phoneHome',
                                     'sortableColumn'     => 'phoneHome',
                                     'pagerWidth'    => 80,
                                     'filter'         => 'candidate.phone_home'),

            'Cell Phone' =>     array('select'   => 'candidate.phone_cell AS phoneCell',
                                     'sortableColumn'     => 'phoneCell',
                                     'pagerWidth'    => 80,
                                     'filter'         => 'candidate.phone_cell'),

            'Work Phone' =>     array('select'   => 'candidate.phone_work AS phoneWork',
                                     'sortableColumn'     => 'phoneWork',
                                     'pagerWidth'    => 80),

            'Address' =>        array('select'   => 'candidate.address AS address',
                                     'sortableColumn'     => 'address',
                                     'pagerWidth'    => 250,
                                     'alphaNavigation' => true,
                                     'filter'         => 'candidate.address'),

            'City' =>           array('select'   => 'candidate.city AS city',
                                     'sortableColumn'     => 'city',
                                     'pagerWidth'    => 80,
                                     'alphaNavigation' => true,
                                     'filter'         => 'candidate.city'),
              'Company' =>           array('select'   => 'Case When candidate.entered_by = '.$_SESSION['RESUFLO']->getUserId().' then "'.$_SESSION['RESUFLO']->getFirstName().'" else (select first_name from user where user_id =(select entered_by from user where user_id=candidate.entered_by) ) End as company',
                                     'sortableColumn'     => 'company',
                                     'pagerWidth'    => 80,
                                     'alphaNavigation' => true,
                                     'filter'         => 'candidate.entered_by'),


            'State' =>          array('select'   => 'candidate.state AS state',
                                     'sortableColumn'     => 'state',
                                     'filterType' => 'dropDown',
                                     'pagerWidth'    => 50,
                                     'alphaNavigation' => true,
                                     'filter'         => 'candidate.state'),

            'Zip' =>            array('select'  => 'candidate.zip AS zip',
                                     'sortableColumn'    => 'zip',
                                     'pagerWidth'   => 50,
                                     'filter'         => 'candidate.zip'),

            'Misc Notes' =>     array('select'  => 'candidate.notes AS notes',
                                     'sortableColumn'    => 'notes',
                                     'pagerWidth'   => 300,
                                     'filter'         => 'candidate.notes'),

            'Web Site' =>      array('select'  => 'candidate.web_site AS webSite',
                                     'pagerRender'     => 'return \'<a href="\'.htmlspecialchars($rsData[\'webSite\']).\'">\'.htmlspecialchars($rsData[\'webSite\']).\'</a>\';',
                                     'sortableColumn'    => 'webSite',
                                     'pagerWidth'   => 80,
                                     'filter'         => 'candidate.web_site'),

            'Key Skills' =>    array('select'  => 'candidate.key_skills AS keySkills',
                                     'pagerRender' => 'return substr(trim($rsData[\'keySkills\']), 0, 30) . (strlen(trim($rsData[\'keySkills\'])) > 30 ? \'...\' : \'\');',
                                     'sortableColumn'    => 'keySkills',
                                     'pagerWidth'   => 210,
                                     'filter'         => 'candidate.key_skills'),

            'Recent Status' => array('select'  => '(
                                                    SELECT
                                                        CONCAT(
                                                            \'<a href="'.RESUFLOUtility::getIndexName().'?m=joborders&amp;list='.$this->get['a'].'&amp;a=show&amp;jobOrderID=\',
                                                            joborder.joborder_id,
                                                            \'" title="\',
                                                            joborder.title,
                                                            \' (\',
                                                            company.name,
                                                            \')">\',
                                                            candidate_joborder_status.short_description,
                                                            \'</a>\'
                                                        )
                                                    FROM
                                                        candidate_joborder
                                                    LEFT JOIN candidate_joborder_status
                                                        ON candidate_joborder_status.candidate_joborder_status_id = candidate_joborder.status
                                                    LEFT JOIN joborder
                                                        ON joborder.joborder_id = candidate_joborder.joborder_id
                                                    LEFT JOIN company
                                                        ON joborder.company_id = company.company_id
                                                    WHERE
                                                        candidate_joborder.candidate_id = candidate.candidate_id
                                                    ORDER BY
                                                        candidate_joborder.date_modified DESC
                                                    LIMIT 1
                                                ) AS lastStatus
                                                ',
                                     'sort'    => 'lastStatus',
                                     'pagerRender'     => 'return $rsData[\'lastStatus\'];',
                                     'exportRender'     => 'return $rsData[\'lastStatus\'];',
                                     'pagerWidth'   => 140,
                                     'exportable' => false,
                                     'filterHaving'  => 'lastStatus',
                                     'filterTypes'   => '=~'),

'Activity Notes' =>     array('select'  => 'candidate.activity_notes AS activity_notes',
                                                     'sortableColumn'    => 'activity_notes',
                                                     'pagerWidth'   => 300,
                                                     'filter'         => 'candidate.activity_notes'),
                                     
            'Recent Status (Extended)' => array('select'  => '(
                                                    SELECT
                                                        CONCAT(
                                                            candidate_joborder_status.short_description,
                                                            \'<br />\',
                                                            \'<a href="'.RESUFLOUtility::getIndexName().'?m=companies&amp;list='.$this->get['a'].'&amp;a=show&amp;companyID=\',
                                                            company.company_id,
                                                            \'">\',
                                                            company.name,
                                                            \'</a> - \',
                                                            \'<a href="'.RESUFLOUtility::getIndexName().'?m=joborders&amp;list='.$this->get['a'].'&amp;a=show&amp;jobOrderID=\',
                                                            joborder.joborder_id,
                                                            \'">\',
                                                            joborder.title,
                                                            \'</a>\'
                                                        )
                                                    FROM
                                                        candidate_joborder
                                                    LEFT JOIN candidate_joborder_status
                                                        ON candidate_joborder_status.candidate_joborder_status_id = candidate_joborder.status
                                                    LEFT JOIN joborder
                                                        ON joborder.joborder_id = candidate_joborder.joborder_id
                                                    LEFT JOIN company
                                                        ON joborder.company_id = company.company_id
                                                    WHERE
                                                        candidate_joborder.candidate_id = candidate.candidate_id
                                                    ORDER BY
                                                        candidate_joborder.date_modified DESC
                                                    LIMIT 1
                                                ) AS lastStatusLong
                                                ',
                                     'sortableColumn'    => 'lastStatusLong',
                                     'pagerRender'     => 'return $rsData[\'lastStatusLong\'];',
                                     'pagerWidth'   => 310,
                                     'exportable' => false,
                                     'filterable' => false),

            'Source' =>        array('select'  => 'candidate.source AS source',
                                     'sortableColumn'    => 'source',
                                     'pagerWidth'   => 140,
                                     'alphaNavigation' => true,
                                     'filter'         => 'candidate.source'),

            'Available' =>     array('select'   => 'DATE_FORMAT(candidate.date_available, \'%m-%d-%y\') AS dateAvailable',
                                     'sortableColumn'     => 'dateAvailable',
                                     'pagerWidth'    => 60),

            'Current Employer' => array('select'  => 'candidate.current_employer AS currentEmployer',
                                     'sortableColumn'    => 'currentEmployer',
                                     'pagerWidth'   => 125,
                                     'alphaNavigation' => true,
                                     'filter'         => 'candidate.current_employer'),

            'Current Pay' => array('select'  => 'candidate.current_pay AS currentPay',
                                     'sortableColumn'    => 'currentPay',
                                     'pagerWidth'   => 125,
                                     'filter'         => 'candidate.current_pay',
                                     'filterTypes'   => '===>=<'),

            'Desired Pay' => array('select'  => 'candidate.desired_pay AS desiredPay',
                                     'sortableColumn'    => 'desiredPay',
                                     'pagerWidth'   => 125,
                                     'filter'         => 'candidate.desired_pay',
                                     'filterTypes'   => '===>=<'),

            'Can Relocate'  => array('select'  => 'candidate.can_relocate AS canRelocate',
                                     'pagerRender'     => 'return ($rsData[\'canRelocate\'] == 0 ? \'No\' : \'Yes\');',
                                     'exportRender'     => 'return ($rsData[\'canRelocate\'] == 0 ? \'No\' : \'Yes\');',
                                     'sortableColumn'    => 'canRelocate',
                                     'pagerWidth'   => 80,
                                     'filter'         => 'candidate.can_relocate'),

            'Recruiter' =>         array('select'   => 'owner_user.first_name AS ownerFirstName,' .
                                                   'owner_user.last_name AS ownerLastName,' .
                                                   'CONCAT(owner_user.last_name, owner_user.first_name) AS ownerSort',
                                     'join'     => 'LEFT JOIN user AS owner_user ON candidate.owner = owner_user.user_id',
                                     'pagerRender'      => 'return StringUtility::makeInitialName($rsData[\'ownerFirstName\'], $rsData[\'ownerLastName\'], false, LAST_NAME_MAXLEN);',
                                     'exportRender'     => 'return $rsData[\'ownerFirstName\'] . " " .$rsData[\'ownerLastName\'];',
                                     'sortableColumn'     => 'ownerSort',
                                     'pagerWidth'    => 75,
                                     'alphaNavigation' => true,
                                     'filter'         => 'CONCAT(owner_user.first_name, owner_user.last_name)'),

            'Created' =>       array('select'   => 'DATE_FORMAT(candidate.date_created, \'%m-%d-%y\') AS dateCreated',
                                     'pagerRender'      => 'return $rsData[\'dateCreated\'];',
                                     'sortableColumn'     => 'dateCreatedSort',
                                     'pagerWidth'    => 60,
                                     'filterHaving' => 'DATE_FORMAT(candidate.date_created, \'%m-%d-%y\')'),
            
            'Date From' =>       array('select'   => 'DATE_FORMAT(candidate.date_created, \'%m-%d-%y\') AS dateCreated',
                                     'pagerRender'      => 'return $rsData[\'dateCreated\'];',
                                     'sortableColumn'     => 'dateCreatedSort',
                                     'pagerWidth'    => 60,
                                     'filterHaving' => 'DATE_FORMAT(candidate.date_created, \'%y-%m-%d\')',
                                     'filterTypes'   => '=>'),
                
            'Date To' =>       array('select'   => 'DATE_FORMAT(candidate.date_created, \'%m-%d-%y\') AS dateTo',
                                     'pagerRender'      => 'return $rsData[\'dateTo\'];',
                                     'sortableColumn'     => 'dateToSort',
                                     'pagerWidth'    => 60,
                                     'filterHaving' => 'DATE_FORMAT(candidate.date_created, \'%y-%m-%d\')',
                                     'filterTypes'   => '=<'),

            'Modified' =>      array('select'   => 'DATE_FORMAT(candidate.date_modified, \'%m-%d-%y\') AS dateModified',
                                     'pagerRender'      => 'return $rsData[\'dateModified\'];',
                                     'sortableColumn'     => 'dateModifiedSort',
                                     'pagerWidth'    => 60,
                                     'pagerOptional' => false,
                                     'filterHaving' => 'DATE_FORMAT(candidate.date_modified, \'%m-%d-%y\')'),
            
            'Scheduled' =>      array('select'   => 'DATE_FORMAT(candidate.schedule_date, \'%m-%d-%y %r\') AS dateScheduled',
                                         'pagerRender'      => 'return $rsData[\'dateScheduled\'];',
                                         'sortableColumn'     => 'dateScheduledSort',
                                         'pagerWidth'    => 150,
                                         'pagerOptional' => true,
                                         'filterHaving' => 'DATE_FORMAT(candidate.schedule_date, \'%m-%d-%y %r\')'),

            /* This one only works when called from the saved list view.  Thats why it is not optional, filterable, or exportable.
             * FIXME:  Somehow make this defined in the associated savedListDataGrid class child.
             */
            'Added To List' =>  array('select'   => 'DATE_FORMAT(saved_list_entry.date_created, \'%m-%d-%y\') AS dateAddedToList,
                                                     saved_list_entry.date_created AS dateAddedToListSort',
                                     'pagerRender'      => 'return $rsData[\'dateAddedToList\'];',
                                     'sortableColumn'     => 'dateAddedToListSort',
                                     'pagerWidth'    => 60,
                                     'pagerOptional' => false,
                                     'filterable' => false,
                                     'exportable' => false),

            'OwnerID' =>       array('select'    => '',
                                     'filter'    => 'candidate.owner',
                                     'pagerOptional' => false,
                                     'filterable' => false,
                                     'filterDescription' => 'Only My Candidates'),

            'IsHot' =>         array('select'    => '',
                                     'filter'    => 'candidate.is_hot',
                                     'pagerOptional' => false,
                                     'filterable' => false,
                                     'filterDescription' => 'Only Hot Candidates'),
                            'Campaign' => $GLOBALS['sheesh']
        );

        if (US_ZIPS_ENABLED)
        {
            $this->_classColumns['Near Zipcode'] =
                               array('select'  => 'candidate.zip AS zip',
                                     'filter' => 'candidate.zip',
                                     'pagerOptional' => false,
                                     'filterTypes'   => '=@');
        }

        /* Extra fields get added as columns here. */
        $candidates = new Candidates($this->_siteID);
        $extraFieldsRS = $candidates->extraFields->getSettings();
        foreach ($extraFieldsRS as $index => $data)
        {
            $fieldName = $data['fieldName'];

            if (!isset($this->_classColumns[$fieldName]))
            {
                $columnDefinition = $candidates->extraFields->getDataGridDefinition($index, $data, $this->_db);

                /* Return false for extra fields that should not be columns. */
                if ($columnDefinition !== false)
                {
                    $this->_classColumns[$fieldName] = $columnDefinition;
                }
            }
        }

        } 
        else if($_REQUEST['a']=='new_candidates' || $_REQUEST['list']=='new_candidates' || $_REQUEST['delrec']=='new_candidates' || ((strpos($_REQUEST['i'], 'candidatesListByViewDataGrid') !== false) && $_REQUEST['view'] != 'all' )) {

                    $this->_assignedCriterion .= " 
                    and candidate.candidate_id NOT IN (SELECT candidate_id FROM candidate_joborder where added_by='".$userid."') 
                    and candidate.candidate_id NOT IN (SELECT candidate_id FROM candidate_joborder where added_by IN (SELECT user_id FROM user where entered_by='".$_SESSION['RESUFLO']->getUserID()."')) 
                    and candidate.unsubscribe = '0' 
                    and candidate.follow_up = '0' 
                    and candidate.list_id = '0'  ";
                    $this->_dataItemIDColumn = 'candidate.candidate_id';

                    $this->_classColumns = array(
                'Attachments' => array('select' => 'IF(candidate_joborder_submitted.candidate_joborder_id, 1, 0) AS submitted,
                                                    IF(attachment_id, 1, 0) AS attachmentPresent',
    
                                         'pagerRender' => 'if ($rsData[\'submitted\'] == 1)
                                                        {
                                                            $return = \'<img src="images/job_orders.gif" alt="" width="16" height="16" title="Submitted for a Job Order" />\';
                                                        }
                                                        else
                                                        {
                                                            $return = \'<img src="images/mru/blank.gif" alt="" width="16" height="16" />\';
                                                        }
    
                                                        if ($rsData[\'attachmentPresent\'] == 1)
                                                        {
                                                            $return .= \'<img src="images/paperclip.gif" alt="" width="16" height="16" title="Attachment Present" />\';
                                                        }
                                                        else
                                                        {
                                                            $return .= \'<img src="images/mru/blank.gif" alt="" width="16" height="16" />\';
                                                        }
    
                                                        return $return;
                                                       ',
    
                                         'join'     => 'LEFT JOIN attachment
                                                            ON candidate.candidate_id = attachment.data_item_id
                                                                                                                    AND attachment.data_item_type = '.DATA_ITEM_CANDIDATE.'
                                                        LEFT JOIN candidate_joborder AS candidate_joborder_submitted
                                                            ON candidate_joborder_submitted.candidate_id = candidate.candidate_id
                                                            AND candidate_joborder_submitted.status >= '.PIPELINE_STATUS_SUBMITTED.'
                                                            AND candidate_joborder_submitted.site_id = '.$this->_siteID.'
                                                            AND candidate_joborder_submitted.status != '.PIPELINE_STATUS_NOTINCONSIDERATION,
                                         'pagerWidth'    => 34,
                                         'pagerOptional' => true,
                                         'pagerNoTitle' => true,
                                         'sizable'  => false,
                                         'exportable' => false,
                                         'filterable' => false),
    
                'First Name' =>     array('select'         => 'candidate.first_name AS firstName',
                                          'pagerRender'    => 'if ($rsData[\'isHot\'] == 1) $className =  \'jobLinkHot\'; else $className = \'jobLinkCold\'; return \'<a href="'.RESUFLOUtility::getIndexName().'?m=candidates&amp;list='.$this->get['a'].'&amp;a=show&amp;candidateID=\'.$rsData[\'candidateID\'].\'&amp;view=1" class="\'.$className.\'">\'.htmlspecialchars($rsData[\'firstName\']).\'</a>\';',
                                          'sortableColumn' => 'firstName',
                                          'pagerWidth'     => 75,
                                          'pagerOptional'  => false,
                                          'alphaNavigation'=> true,
                                          'filter'         => 'candidate.first_name'),
    
                'Last Name' =>      array('select'         => 'candidate.last_name AS lastName',
                                         'sortableColumn'  => 'lastName',
                                         'pagerRender'     => 'if ($rsData[\'isHot\'] == 1) $className =  \'jobLinkHot\'; else $className = \'jobLinkCold\'; return \'<a href="'.RESUFLOUtility::getIndexName().'?m=candidates&amp;list='.$this->get['a'].'&amp;a=show&amp;candidateID=\'.$rsData[\'candidateID\'].\'&amp;view=1" class="\'.$className.\'">\'.htmlspecialchars($rsData[\'lastName\']).\'</a>\';',
                                         'pagerWidth'      => 85,
                                         'pagerOptional'   => false,
                                         'alphaNavigation' => true,
                                         'filter'         => 'candidate.last_name'),
    
                'E-Mail' =>         array('select'   => 'candidate.email1 AS email1',
                                         'sortableColumn'     => 'email1',
                                         'pagerWidth'    => 80,
                                         'filter'         => 'candidate.email1'),
    
                '2nd E-Mail' =>     array('select'   => 'candidate.email2 AS email2',
                                         'sortableColumn'     => 'email2',
                                         'pagerWidth'    => 80,
                                         'filter'         => 'candidate.email2'),
    
                'Home Phone' =>     array('select'   => 'candidate.phone_home AS phoneHome',
                                         'sortableColumn'     => 'phoneHome',
                                         'pagerWidth'    => 80,
                                         'filter'         => 'candidate.phone_home'),
    
                'Cell Phone' =>     array('select'   => 'candidate.phone_cell AS phoneCell',
                                         'sortableColumn'     => 'phoneCell',
                                         'pagerWidth'    => 80,
                                         'filter'         => 'candidate.phone_cell'),
    
                'Work Phone' =>     array('select'   => 'candidate.phone_work AS phoneWork',
                                         'sortableColumn'     => 'phoneWork',
                                         'pagerWidth'    => 80),
    
                'Address' =>        array('select'   => 'candidate.address AS address',
                                         'sortableColumn'     => 'address',
                                         'pagerWidth'    => 250,
                                         'alphaNavigation' => true,
                                         'filter'         => 'candidate.address'),
    
                'City' =>           array('select'   => 'candidate.city AS city',
                                         'sortableColumn'     => 'city',
                                         'pagerWidth'    => 80,
                                         'alphaNavigation' => true,
                                         'filter'         => 'candidate.city'),
			    'Company' =>           array('select'   => 'Case When candidate.entered_by = '.$_SESSION['RESUFLO']->getUserId().' then "'.$_SESSION['RESUFLO']->getFirstName().'" else (select first_name from user where user_id =(select entered_by from user where user_id=candidate.entered_by) ) End as company',
										 'sortableColumn'     => 'company',
										 'pagerWidth'    => 80,
										 'alphaNavigation' => true,
										 'filter'         => 'candidate.entered_by'),
    
    
                'State' =>          array('select'   => 'candidate.state AS state',
                                         'sortableColumn'     => 'state',
                                         'filterType' => 'dropDown',
                                         'pagerWidth'    => 50,
                                         'alphaNavigation' => true,
                                         'filter'         => 'candidate.state'),
    
                'Zip' =>            array('select'  => 'candidate.zip AS zip',
                                         'sortableColumn'    => 'zip',
                                         'pagerWidth'   => 50,
                                         'filter'         => 'candidate.zip'),
    
                'Misc Notes' =>     array('select'  => 'candidate.notes AS notes',
                                         'sortableColumn'    => 'notes',
                                         'pagerWidth'   => 300,
                                         'filter'         => 'candidate.notes'),
    
                'Web Site' =>      array('select'  => 'candidate.web_site AS webSite',
                                         'pagerRender'     => 'return \'<a href="\'.htmlspecialchars($rsData[\'webSite\']).\'">\'.htmlspecialchars($rsData[\'webSite\']).\'</a>\';',
                                         'sortableColumn'    => 'webSite',
                                         'pagerWidth'   => 80,
                                         'filter'         => 'candidate.web_site'),
    
                'Key Skills' =>    array('select'  => 'candidate.key_skills AS keySkills',
                                         'pagerRender' => 'return substr(trim($rsData[\'keySkills\']), 0, 30) . (strlen(trim($rsData[\'keySkills\'])) > 30 ? \'...\' : \'\');',
                                         'sortableColumn'    => 'keySkills',
                                         'pagerWidth'   => 210,
                                         'filter'         => 'candidate.key_skills'),
    
                'Recent Status' => array('select'  => '(
                                                        SELECT
                                                            CONCAT(
                                                                \'<a href="'.RESUFLOUtility::getIndexName().'?m=joborders&amp;list='.$this->get['a'].'&amp;a=show&amp;jobOrderID=\',
                                                                joborder.joborder_id,
                                                                \'" title="\',
                                                                joborder.title,
                                                                \' (\',
                                                                company.name,
                                                                \')">\',
                                                                candidate_joborder_status.short_description,
                                                                \'</a>\'
                                                            )
                                                        FROM
                                                            candidate_joborder
                                                        LEFT JOIN candidate_joborder_status
                                                            ON candidate_joborder_status.candidate_joborder_status_id = candidate_joborder.status
                                                        LEFT JOIN joborder
                                                            ON joborder.joborder_id = candidate_joborder.joborder_id
                                                        LEFT JOIN company
                                                            ON joborder.company_id = company.company_id
                                                        WHERE
                                                            candidate_joborder.candidate_id = candidate.candidate_id
                                                        ORDER BY
                                                            candidate_joborder.date_modified DESC
                                                        LIMIT 1
                                                    ) AS lastStatus
                                                    ',
                                         'sort'    => 'lastStatus',
                                         'pagerRender'     => 'return $rsData[\'lastStatus\'];',
                                         'exportRender'     => 'return $rsData[\'lastStatus\'];',
                                         'pagerWidth'   => 140,
                                         'exportable' => false,
                                         'filterHaving'  => 'lastStatus',
                                         'filterTypes'   => '=~'),
    
'Activity Notes' =>     array('select'  => 'candidate.activity_notes AS activity_notes',
                                                     'sortableColumn'    => 'activity_notes',
                                                     'pagerWidth'   => 300,
                                                     'filter'         => 'candidate.activity_notes'),
                                                                          
                'Recent Status (Extended)' => array('select'  => '(
                                                        SELECT
                                                            CONCAT(
                                                                candidate_joborder_status.short_description,
                                                                \'<br />\',
                                                                \'<a href="'.RESUFLOUtility::getIndexName().'?m=companies&amp;list='.$this->get['a'].'&amp;a=show&amp;companyID=\',
                                                                company.company_id,
                                                                \'">\',
                                                                company.name,
                                                                \'</a> - \',
                                                                \'<a href="'.RESUFLOUtility::getIndexName().'?m=joborders&amp;list='.$this->get['a'].'&amp;a=show&amp;jobOrderID=\',
                                                                joborder.joborder_id,
                                                                \'">\',
                                                                joborder.title,
                                                                \'</a>\'
                                                            )
                                                        FROM
                                                            candidate_joborder
                                                        LEFT JOIN candidate_joborder_status
                                                            ON candidate_joborder_status.candidate_joborder_status_id = candidate_joborder.status
                                                        LEFT JOIN joborder
                                                            ON joborder.joborder_id = candidate_joborder.joborder_id
                                                        LEFT JOIN company
                                                            ON joborder.company_id = company.company_id
                                                        WHERE
                                                            candidate_joborder.candidate_id = candidate.candidate_id
                                                        ORDER BY
                                                            candidate_joborder.date_modified DESC
                                                        LIMIT 1
                                                    ) AS lastStatusLong
                                                    ',
                                         'sortableColumn'    => 'lastStatusLong',
                                         'pagerRender'     => 'return $rsData[\'lastStatusLong\'];',
                                         'pagerWidth'   => 310,
                                         'exportable' => false,
                                         'filterable' => false),
    
                'Source' =>        array('select'  => 'candidate.source AS source',
                                         'sortableColumn'    => 'source',
                                         'pagerWidth'   => 140,
                                         'alphaNavigation' => true,
                                         'filter'         => 'candidate.source'),
    
                'Available' =>     array('select'   => 'DATE_FORMAT(candidate.date_available, \'%m-%d-%y\') AS dateAvailable',
                                         'sortableColumn'     => 'dateAvailable',
                                         'pagerWidth'    => 60),
    
                'Current Employer' => array('select'  => 'candidate.current_employer AS currentEmployer',
                                         'sortableColumn'    => 'currentEmployer',
                                         'pagerWidth'   => 125,
                                         'alphaNavigation' => true,
                                         'filter'         => 'candidate.current_employer'),
    
                'Current Pay' => array('select'  => 'candidate.current_pay AS currentPay',
                                         'sortableColumn'    => 'currentPay',
                                         'pagerWidth'   => 125,
                                         'filter'         => 'candidate.current_pay',
                                         'filterTypes'   => '===>=<'),
    
                'Desired Pay' => array('select'  => 'candidate.desired_pay AS desiredPay',
                                         'sortableColumn'    => 'desiredPay',
                                         'pagerWidth'   => 125,
                                         'filter'         => 'candidate.desired_pay',
                                         'filterTypes'   => '===>=<'),
    
                'Can Relocate'  => array('select'  => 'candidate.can_relocate AS canRelocate',
                                         'pagerRender'     => 'return ($rsData[\'canRelocate\'] == 0 ? \'No\' : \'Yes\');',
                                         'exportRender'     => 'return ($rsData[\'canRelocate\'] == 0 ? \'No\' : \'Yes\');',
                                         'sortableColumn'    => 'canRelocate',
                                         'pagerWidth'   => 80,
                                         'filter'         => 'candidate.can_relocate'),
    
                'Recruiter' =>         array('select'   => 'owner_user.first_name AS ownerFirstName,' .
                                                       'owner_user.last_name AS ownerLastName,' .
                                                       'CONCAT(owner_user.last_name, owner_user.first_name) AS ownerSort',
                                         'join'     => 'LEFT JOIN user AS owner_user ON candidate.owner = owner_user.user_id',
                                         'pagerRender'      => 'return StringUtility::makeInitialName($rsData[\'ownerFirstName\'], $rsData[\'ownerLastName\'], false, LAST_NAME_MAXLEN);',
                                         'exportRender'     => 'return $rsData[\'ownerFirstName\'] . " " .$rsData[\'ownerLastName\'];',
                                         'sortableColumn'     => 'ownerSort',
                                         'pagerWidth'    => 75,
                                         'alphaNavigation' => true,
                                         'filter'         => 'CONCAT(owner_user.first_name, owner_user.last_name)'),
    
                'Created' =>       array('select'   => 'DATE_FORMAT(candidate.date_created, \'%m-%d-%y\') AS dateCreated',
                                         'pagerRender'      => 'return $rsData[\'dateCreated\'];',
                                         'sortableColumn'     => 'dateCreatedSort',
                                         'pagerWidth'    => 60,
                                         'filterHaving' => 'DATE_FORMAT(candidate.date_created, \'%m-%d-%y\')'),
                        
                'Date From' =>       array('select'   => 'DATE_FORMAT(candidate.date_created, \'%m-%d-%y\') AS dateCreated',
                                     'pagerRender'      => 'return $rsData[\'dateCreated\'];',
                                     'sortableColumn'     => 'dateCreatedSort',
                                     'pagerWidth'    => 60,
                                     'filterHaving' => 'DATE_FORMAT(candidate.date_created, \'%y-%m-%d\')',
                                     'filterTypes'   => '=>'),
                
                'Date To' =>       array('select'   => 'DATE_FORMAT(candidate.date_created, \'%m-%d-%y\') AS dateTo',
                                         'pagerRender'      => 'return $rsData[\'dateTo\'];',
                                         'sortableColumn'     => 'dateToSort',
                                         'pagerWidth'    => 60,
                                         'filterHaving' => 'DATE_FORMAT(candidate.date_created, \'%y-%m-%d\')',
                                         'filterTypes'   => '=<'),
                        
                'Modified' =>      array('select'   => 'DATE_FORMAT(candidate.date_modified, \'%m-%d-%y\') AS dateModified',
                                         'pagerRender'      => 'return $rsData[\'dateModified\'];',
                                         'sortableColumn'     => 'dateModifiedSort',
                                         'pagerWidth'    => 60,
                                         'pagerOptional' => false,
                                         'filterHaving' => 'DATE_FORMAT(candidate.date_modified, \'%m-%d-%y\')'),
                        
                'Scheduled' =>      array('select'   => 'DATE_FORMAT(candidate.schedule_date, \'%m-%d-%y %r\') AS dateScheduled',
                                         'pagerRender'      => 'return $rsData[\'dateScheduled\'];',
                                         'sortableColumn'     => 'dateScheduledSort',
                                         'pagerWidth'    => 150,
                                         'pagerOptional' => true,
                                         'filterHaving' => 'DATE_FORMAT(candidate.schedule_date, \'%m-%d-%y %r\')'),
    
                /* This one only works when called from the saved list view.  Thats why it is not optional, filterable, or exportable.
                 * FIXME:  Somehow make this defined in the associated savedListDataGrid class child.
                 */
                'Added To List' =>  array('select'   => 'DATE_FORMAT(saved_list_entry.date_created, \'%m-%d-%y\') AS dateAddedToList,
                                                         saved_list_entry.date_created AS dateAddedToListSort',
                                         'pagerRender'      => 'return $rsData[\'dateAddedToList\'];',
                                         'sortableColumn'     => 'dateAddedToListSort',
                                         'pagerWidth'    => 60,
                                         'pagerOptional' => false,
                                         'filterable' => false,
                                         'exportable' => false),
    
                'OwnerID' =>       array('select'    => '',
                                         'filter'    => 'candidate.owner',
                                         'pagerOptional' => false,
                                         'filterable' => false,
                                         'filterDescription' => 'Only My Candidates'),
    
                'IsHot' =>         array('select'    => '',
                                         'filter'    => 'candidate.is_hot',
                                         'pagerOptional' => false,
                                         'filterable' => false,
                                         'filterDescription' => 'Only Hot Candidates'),
                            'Campaign' => $GLOBALS['sheesh']
            );
    
            if (US_ZIPS_ENABLED)
            {
                $this->_classColumns['Near Zipcode'] =
                                   array('select'  => 'candidate.zip AS zip',
                                         'filter' => 'candidate.zip',
                                         'pagerOptional' => false,
                                         'filterTypes'   => '=@');
            }
    
            /* Extra fields get added as columns here. */
            $candidates = new Candidates($this->_siteID);
            $extraFieldsRS = $candidates->extraFields->getSettings();
            foreach ($extraFieldsRS as $index => $data)
            {
                $fieldName = $data['fieldName'];
    
                if (!isset($this->_classColumns[$fieldName]))
                {
					
                    $columnDefinition = $candidates->extraFields->getDataGridDefinition($index, $data, $this->_db);
    
                    /* Return false for extra fields that should not be columns. */
                    if ($columnDefinition !== false)
                    {
                        $this->_classColumns[$fieldName] = $columnDefinition;
                    }
                }
            }
                    
        } 
        else {
            // $this->_assignedCriterion .= " and candidate.unsubscribe = '0'";
        $this->_dataItemIDColumn = 'candidate.candidate_id';

             $this->_classColumns = array(
            'Attachments' => array('select' => 'IF(candidate_joborder_submitted.candidate_joborder_id, 1, 0) AS submitted,
                                                IF(attachment_id, 1, 0) AS attachmentPresent',

                                     'pagerRender' => 'if ($rsData[\'submitted\'] == 1)
                                                    {
                                                        $return = \'<img src="images/job_orders.gif" alt="" width="16" height="16" title="Submitted for a Job Order" />\';
                                                    }
                                                    else
                                                    {
                                                        $return = \'<img src="images/mru/blank.gif" alt="" width="16" height="16" />\';
                                                    }

                                                    if ($rsData[\'attachmentPresent\'] == 1)
                                                    {
                                                        $return .= \'<img src="images/paperclip.gif" alt="" width="16" height="16" title="Attachment Present" />\';
                                                    }
                                                    else
                                                    {
                                                        $return .= \'<img src="images/mru/blank.gif" alt="" width="16" height="16" />\';
                                                    }

                                                    return $return;
                                                   ',

                                     'join'     => 'LEFT JOIN attachment
                                                        ON candidate.candidate_id = attachment.data_item_id
                                                        AND attachment.data_item_type = '.DATA_ITEM_CANDIDATE.'
                                                    LEFT JOIN candidate_joborder AS candidate_joborder_submitted
                                                        ON candidate_joborder_submitted.candidate_id = candidate.candidate_id
                                                        AND candidate_joborder_submitted.status >= '.PIPELINE_STATUS_SUBMITTED.'
                                                        AND candidate_joborder_submitted.site_id = '.$this->_siteID.'
                                                        AND candidate_joborder_submitted.status != '.PIPELINE_STATUS_NOTINCONSIDERATION,
                                     'pagerWidth'    => 34,
                                     'pagerOptional' => true,
                                     'pagerNoTitle' => true,
                                     'sizable'  => false,
                                     'exportable' => false,
                                     'filterable' => false),

            'First Name' =>     array('select'         => 'candidate.first_name AS firstName',
                                      'pagerRender'    => 'if ($rsData[\'isHot\'] == 1) $className =  \'jobLinkHot\'; else $className = \'jobLinkCold\'; return \'<a href="'.RESUFLOUtility::getIndexName().'?m=candidates&amp;list='.$this->get['a'].'&amp;a=show&amp;candidateID=\'.$rsData[\'candidateID\'].\'&amp;view=1" class="\'.$className.\'">\'.htmlspecialchars($rsData[\'firstName\']).\'</a>\';',
                                      'sortableColumn' => 'firstName',
                                      'pagerWidth'     => 75,
                                      'pagerOptional'  => false,
                                      'alphaNavigation'=> true,
                                      'filter'         => 'candidate.first_name'),

            'Last Name' =>      array('select'         => 'candidate.last_name AS lastName',
                                     'sortableColumn'  => 'lastName',
                                     'pagerRender'     => 'if ($rsData[\'isHot\'] == 1) $className =  \'jobLinkHot\'; else $className = \'jobLinkCold\'; return \'<a href="'.RESUFLOUtility::getIndexName().'?m=candidates&amp;list='.$this->get['a'].'&amp;a=show&amp;candidateID=\'.$rsData[\'candidateID\'].\'&amp;view=1" class="\'.$className.\'">\'.htmlspecialchars($rsData[\'lastName\']).\'</a>\';',
                                     'pagerWidth'      => 85,
                                     'pagerOptional'   => false,
                                     'alphaNavigation' => true,
                                     'filter'         => 'candidate.last_name'),

            'E-Mail' =>         array('select'   => 'candidate.email1 AS email1',
                                     'sortableColumn'     => 'email1',
                                     'pagerWidth'    => 80,
                                     'filter'         => 'candidate.email1'),

            '2nd E-Mail' =>     array('select'   => 'candidate.email2 AS email2',
                                     'sortableColumn'     => 'email2',
                                     'pagerWidth'    => 80,
                                     'filter'         => 'candidate.email2'),

            'Home Phone' =>     array('select'   => 'candidate.phone_home AS phoneHome',
                                     'sortableColumn'     => 'phoneHome',
                                     'pagerWidth'    => 80,
                                     'filter'         => 'candidate.phone_home'),

            'Cell Phone' =>     array('select'   => 'candidate.phone_cell AS phoneCell',
                                     'sortableColumn'     => 'phoneCell',
                                     'pagerWidth'    => 80,
                                     'filter'         => 'candidate.phone_cell'),

            'Work Phone' =>     array('select'   => 'candidate.phone_work AS phoneWork',
                                     'sortableColumn'     => 'phoneWork',
                                     'pagerWidth'    => 80),

            'Address' =>        array('select'   => 'candidate.address AS address',
                                     'sortableColumn'     => 'address',
                                     'pagerWidth'    => 250,
                                     'alphaNavigation' => true,
                                     'filter'         => 'candidate.address'),

            'City' =>           array('select'   => 'candidate.city AS city',
                                     'sortableColumn'     => 'city',
                                     'pagerWidth'    => 80,
                                     'alphaNavigation' => true,
                                     'filter'         => 'candidate.city'),
              'Company' =>           array('select'   => 'Case When candidate.entered_by = '.$_SESSION['RESUFLO']->getUserId().' then "'.$_SESSION['RESUFLO']->getFirstName().'" else (select first_name from user where user_id =(select entered_by from user where user_id=candidate.entered_by) ) End as company',
                                     'sortableColumn'     => 'company',
                                     'pagerWidth'    => 80,
                                     'alphaNavigation' => true,
                                     'filter'         => 'candidate.entered_by'),


            'State' =>          array('select'   => 'candidate.state AS state',
                                     'sortableColumn'     => 'state',
                                     'filterType' => 'dropDown',
                                     'pagerWidth'    => 50,
                                     'alphaNavigation' => true,
                                     'filter'         => 'candidate.state'),

            'Zip' =>            array('select'  => 'candidate.zip AS zip',
                                     'sortableColumn'    => 'zip',
                                     'pagerWidth'   => 50,
                                     'filter'         => 'candidate.zip'),

            'Misc Notes' =>     array('select'  => 'candidate.notes AS notes',
                                     'sortableColumn'    => 'notes',
                                     'pagerWidth'   => 300,
                                     'filter'         => 'candidate.notes'),

            'Web Site' =>      array('select'  => 'candidate.web_site AS webSite',
                                     'pagerRender'     => 'return \'<a href="\'.htmlspecialchars($rsData[\'webSite\']).\'">\'.htmlspecialchars($rsData[\'webSite\']).\'</a>\';',
                                     'sortableColumn'    => 'webSite',
                                     'pagerWidth'   => 80,
                                     'filter'         => 'candidate.web_site'),

            'Key Skills' =>    array('select'  => 'candidate.key_skills AS keySkills',
                                     'pagerRender' => 'return substr(trim($rsData[\'keySkills\']), 0, 30) . (strlen(trim($rsData[\'keySkills\'])) > 30 ? \'...\' : \'\');',
                                     'sortableColumn'    => 'keySkills',
                                     'pagerWidth'   => 210,
                                     'filter'         => 'candidate.key_skills'),

            'Recent Status' => array('select'  => '(
                                                    SELECT
                                                        CONCAT(
                                                            \'<a href="'.RESUFLOUtility::getIndexName().'?m=joborders&amp;list='.$this->get['a'].'&amp;a=show&amp;jobOrderID=\',
                                                            joborder.joborder_id,
                                                            \'" title="\',
                                                            joborder.title,
                                                            \' (\',
                                                            company.name,
                                                            \')">\',
                                                            candidate_joborder_status.short_description,
                                                            \'</a>\'
                                                        )
                                                    FROM
                                                        candidate_joborder
                                                    LEFT JOIN candidate_joborder_status
                                                        ON candidate_joborder_status.candidate_joborder_status_id = candidate_joborder.status
                                                    LEFT JOIN joborder
                                                        ON joborder.joborder_id = candidate_joborder.joborder_id
                                                    LEFT JOIN company
                                                        ON joborder.company_id = company.company_id
                                                    WHERE
                                                        candidate_joborder.candidate_id = candidate.candidate_id
                                                    ORDER BY
                                                        candidate_joborder.date_modified DESC
                                                    LIMIT 1
                                                ) AS lastStatus
                                                ',
                                     'sort'    => 'lastStatus',
                                     'pagerRender'     => 'return $rsData[\'lastStatus\'];',
                                     'exportRender'     => 'return $rsData[\'lastStatus\'];',
                                     'pagerWidth'   => 140,
                                     'exportable' => false,
                                     'filterHaving'  => 'lastStatus',
                                     'filterTypes'   => '=~'),
                                     
 'Activity Notes' =>     array('select'  => 'candidate.activity_notes AS activity_notes',
                                                     'sortableColumn'    => 'activity_notes',
                                                     'pagerWidth'   => 300,
                                                     'filter'         => 'candidate.activity_notes'),

            'Recent Status (Extended)' => array('select'  => '(
                                                    SELECT
                                                        CONCAT(
                                                            candidate_joborder_status.short_description,
                                                            \'<br />\',
                                                            \'<a href="'.RESUFLOUtility::getIndexName().'?m=companies&amp;list='.$this->get['a'].'&amp;a=show&amp;companyID=\',
                                                            company.company_id,
                                                            \'">\',
                                                            company.name,
                                                            \'</a> - \',
                                                            \'<a href="'.RESUFLOUtility::getIndexName().'?m=joborders&amp;list='.$this->get['a'].'&amp;a=show&amp;jobOrderID=\',
                                                            joborder.joborder_id,
                                                            \'">\',
                                                            joborder.title,
                                                            \'</a>\'
                                                        )
                                                    FROM
                                                        candidate_joborder
                                                    LEFT JOIN candidate_joborder_status
                                                        ON candidate_joborder_status.candidate_joborder_status_id = candidate_joborder.status
                                                    LEFT JOIN joborder
                                                        ON joborder.joborder_id = candidate_joborder.joborder_id
                                                    LEFT JOIN company
                                                        ON joborder.company_id = company.company_id
                                                    WHERE
                                                        candidate_joborder.candidate_id = candidate.candidate_id
                                                    ORDER BY
                                                        candidate_joborder.date_modified DESC
                                                    LIMIT 1
                                                ) AS lastStatusLong
                                                ',
                                     'sortableColumn'    => 'lastStatusLong',
                                     'pagerRender'     => 'return $rsData[\'lastStatusLong\'];',
                                     'pagerWidth'   => 310,
                                     'exportable' => false,
                                     'filterable' => false),

            'Source' =>        array('select'  => 'candidate.source AS source',
                                     'sortableColumn'    => 'source',
                                     'pagerWidth'   => 140,
                                     'alphaNavigation' => true,
                                     'filter'         => 'candidate.source'),

            'Available' =>     array('select'   => 'DATE_FORMAT(candidate.date_available, \'%m-%d-%y\') AS dateAvailable',
                                     'sortableColumn'     => 'dateAvailable',
                                     'pagerWidth'    => 60),

            'Current Employer' => array('select'  => 'candidate.current_employer AS currentEmployer',
                                     'sortableColumn'    => 'currentEmployer',
                                     'pagerWidth'   => 125,
                                     'alphaNavigation' => true,
                                     'filter'         => 'candidate.current_employer'),

            'Current Pay' => array('select'  => 'candidate.current_pay AS currentPay',
                                     'sortableColumn'    => 'currentPay',
                                     'pagerWidth'   => 125,
                                     'filter'         => 'candidate.current_pay',
                                     'filterTypes'   => '===>=<'),

            'Desired Pay' => array('select'  => 'candidate.desired_pay AS desiredPay',
                                     'sortableColumn'    => 'desiredPay',
                                     'pagerWidth'   => 125,
                                     'filter'         => 'candidate.desired_pay',
                                     'filterTypes'   => '===>=<'),

            'Can Relocate'  => array('select'  => 'candidate.can_relocate AS canRelocate',
                                     'pagerRender'     => 'return ($rsData[\'canRelocate\'] == 0 ? \'No\' : \'Yes\');',
                                     'exportRender'     => 'return ($rsData[\'canRelocate\'] == 0 ? \'No\' : \'Yes\');',
                                     'sortableColumn'    => 'canRelocate',
                                     'pagerWidth'   => 80,
                                     'filter'         => 'candidate.can_relocate'),

            'Recruiter' =>         array('select'   => 'owner_user.first_name AS ownerFirstName,' .
                                                   'owner_user.last_name AS ownerLastName,' .
                                                   'CONCAT(owner_user.last_name, owner_user.first_name) AS ownerSort',
                                     'join'     => 'LEFT JOIN user AS owner_user ON candidate.owner = owner_user.user_id',
                                     'pagerRender'      => 'return StringUtility::makeInitialName($rsData[\'ownerFirstName\'], $rsData[\'ownerLastName\'], false, LAST_NAME_MAXLEN);',
                                     'exportRender'     => 'return $rsData[\'ownerFirstName\'] . " " .$rsData[\'ownerLastName\'];',
                                     'sortableColumn'     => 'ownerSort',
                                     'pagerWidth'    => 75,
                                     'alphaNavigation' => true,
                                     'filter'         => 'CONCAT(owner_user.first_name, owner_user.last_name)'),

            'Created' =>       array('select'   => 'DATE_FORMAT(candidate.date_created, \'%m-%d-%y\') AS dateCreated',
                                     'pagerRender'      => 'return $rsData[\'dateCreated\'];',
                                     'sortableColumn'     => 'dateCreatedSort',
                                     'pagerWidth'    => 60,
                                     'filterHaving' => 'DATE_FORMAT(candidate.date_created, \'%m-%d-%y\')'),
                 
            'Date From' =>       array('select'   => 'DATE_FORMAT(candidate.date_created, \'%m-%d-%y\') AS dateCreated',
                                 'pagerRender'      => 'return $rsData[\'dateCreated\'];',
                                 'sortableColumn'     => 'dateCreatedSort',
                                 'pagerWidth'    => 60,
                                 'filterHaving' => 'DATE_FORMAT(candidate.date_created, \'%y-%m-%d\')',
                                 'filterTypes'   => '=>'),

            'Date To' =>       array('select'   => 'DATE_FORMAT(candidate.date_created, \'%m-%d-%y\') AS dateTo',
                                     'pagerRender'      => 'return $rsData[\'dateTo\'];',
                                     'sortableColumn'     => 'dateToSort',
                                     'pagerWidth'    => 60,
                                     'filterHaving' => 'DATE_FORMAT(candidate.date_created, \'%y-%m-%d\')',
                                     'filterTypes'   => '=<'),

            'Modified' =>      array('select'   => 'DATE_FORMAT(candidate.date_modified, \'%m-%d-%y\') AS dateModified',
                                     'pagerRender'      => 'return $rsData[\'dateModified\'];',
                                     'sortableColumn'     => 'dateModifiedSort',
                                     'pagerWidth'    => 60,
                                     'pagerOptional' => false,
                                     'filterHaving' => 'DATE_FORMAT(candidate.date_modified, \'%m-%d-%y\')'),
                 
            'Scheduled' =>      array('select'   => 'DATE_FORMAT(candidate.schedule_date, \'%m-%d-%y %r\') AS dateScheduled',
                                         'pagerRender'      => 'return $rsData[\'dateScheduled\'];',
                                         'sortableColumn'     => 'dateScheduledSort',
                                         'pagerWidth'    => 150,
                                         'pagerOptional' => true,
                                         'filterHaving' => 'DATE_FORMAT(candidate.schedule_date, \'%m-%d-%y %r\')'),

            /* This one only works when called from the saved list view.  Thats why it is not optional, filterable, or exportable.
             * FIXME:  Somehow make this defined in the associated savedListDataGrid class child.
             */
            'Added To List' =>  array('select'   => 'DATE_FORMAT(saved_list_entry.date_created, \'%m-%d-%y\') AS dateAddedToList,
                                                     saved_list_entry.date_created AS dateAddedToListSort',
                                     'pagerRender'      => 'return $rsData[\'dateAddedToList\'];',
                                     'sortableColumn'     => 'dateAddedToListSort',
                                     'pagerWidth'    => 60,
                                     'pagerOptional' => false,
                                     'filterable' => false,
                                     'exportable' => false),

            'OwnerID' =>       array('select'    => '',
                                     'filter'    => 'candidate.owner',
                                     'pagerOptional' => false,
                                     'filterable' => false,
                                     'filterDescription' => 'Only My Candidates'),

            'IsHot' =>         array('select'    => '',
                                     'filter'    => 'candidate.is_hot',
                                     'pagerOptional' => false,
                                     'filterable' => false,
                                     'filterDescription' => 'Only Hot Candidates'),
                            'Campaign' => $GLOBALS['sheesh']
        );

        if (US_ZIPS_ENABLED)
        {
            $this->_classColumns['Near Zipcode'] =
                               array('select'  => 'candidate.zip AS zip',
                                     'filter' => 'candidate.zip',
                                     'pagerOptional' => false,
                                     'filterTypes'   => '=@');
        }

        /* Extra fields get added as columns here. */
        $candidates = new Candidates($this->_siteID);
        $extraFieldsRS = $candidates->extraFields->getSettings();
        foreach ($extraFieldsRS as $index => $data)
        {
            $fieldName = $data['fieldName'];

            if (!isset($this->_classColumns[$fieldName]))
            {
                $columnDefinition = $candidates->extraFields->getDataGridDefinition($index, $data, $this->_db);

                /* Return false for extra fields that should not be columns. */
                if ($columnDefinition !== false)
                {
                    $this->_classColumns[$fieldName] = $columnDefinition;
                }
            }
          }
        }    
        parent::__construct($instanceName, $parameters, $misc);
    }

    /**
     * Returns the sql statment for the pager.
     *
     * @return array Candidates data
     */
    public function getSQL($selectSQL, $joinSQL, $whereSQL, $havingSQL, $orderSQL, $limitSQL, $distinct = '')
    {
		
        // echo "1=".$selectSQL."<br>2=".$joinSQL."<br>3=".$whereSQL."<br>4=".$havingSQL."<br>5=".$orderSQL."<br>6=".$limitSQL."<br>7=".$distinct = '';
        // FIXME: Factor out Session dependency.
        if ($_SESSION['RESUFLO']->isLoggedIn() && $_SESSION['RESUFLO']->getAccessLevel() < ACCESS_LEVEL_MULTI_SA)
        {
            $adminHiddenCriterion = 'AND candidate.is_admin_hidden = 0';
        }
        else
        {
            $adminHiddenCriterion = '';
        }

        if ($this->getMiscArgument() != 0)
        {
            $savedListID = (int) $this->getMiscArgument();
            $joinSQL  .= ' INNER JOIN saved_list_entry
                                    ON saved_list_entry.data_item_type = '.DATA_ITEM_CANDIDATE.'
                                    AND saved_list_entry.data_item_id = candidate.candidate_id
                                    AND saved_list_entry.site_id = '.$this->_siteID.'
                                    AND saved_list_entry.saved_list_id = '.$savedListID;
        }
        else
        {
            $joinSQL  .= ' LEFT JOIN saved_list_entry
                                    ON saved_list_entry.data_item_type = '.DATA_ITEM_CANDIDATE.'
                                    AND saved_list_entry.data_item_id = candidate.candidate_id
                                    AND saved_list_entry.site_id = '.$this->_siteID;         
        }
        
            $sql = sprintf(
            "SELECT SQL_CALC_FOUND_ROWS %s
                 candidate.candidate_id AS candidateID,
                 candidate.candidate_id AS exportID,
                 candidate.is_hot AS isHot,
                 candidate.date_modified AS dateModifiedSort,
                 candidate.date_created AS dateCreatedSort,
                 candidate.schedule_date AS dateScheduledSort,
             %s
             FROM
                 candidate
             %s
             WHERE
                 candidate.site_id = %s
             %s
             %s
             %s
             GROUP BY candidate.candidate_id
             %s
             %s
             %s",
             $distinct,
             $selectSQL,
             $joinSQL,
             $this->_siteID,
             $adminHiddenCriterion,
             (strlen($whereSQL) > 0) ? ' AND ' . $whereSQL : '',
             $this->_assignedCriterion,
             (strlen($havingSQL) > 0) ? ' HAVING ' . $havingSQL : '',
             $orderSQL.', candidate.candidate_id desc',
             $limitSQL
         );
        //echo $sql;    
        return $sql;
    }
}

/**
 *  EEO Settings Library
 *  @package    RESUFLO
 *  @subpackage Library
 */
class EEOSettings
{
    private $_db;
    private $_siteID;
    private $_userID;


    public function __construct($siteID)
    {
        $this->_siteID = $siteID;
        // FIXME: Factor out Session dependency.
        $this->_userID = $_SESSION['RESUFLO']->getUserID();
        $this->_db = DatabaseConnection::getInstance();
    }

 
    /**
     * Returns all EEO settings for a site.
     *
     * @return array (setting => value)
     */
    public function getAll()
    {
        /* Default values. */
        $settings = array(
            'enabled' => '0',
            'genderTracking' => '0',
            'ethnicTracking' => '0',
            'veteranTracking' => '0',
            'veteranTracking' => '0',
            'disabilityTracking' => '0',
            'canSeeEEOInfo' => false
        );

        $sql = sprintf(
            "SELECT
                settings.setting AS setting,
                settings.value AS value,
                settings.site_id AS siteID
            FROM
                settings
            WHERE
                settings.site_id = %s
            AND
                settings.settings_type = %s",
            $this->_siteID,
            SETTINGS_EEO
        );
        $rs = $this->_db->getAllAssoc($sql);

        /* Override default settings with settings from the database. */
        foreach ($rs as $rowIndex => $row)
        {
            foreach ($settings as $setting => $value)
            {
                if ($row['setting'] == $setting)
                {
                    $settings[$setting] = $row['value'];
                }
            }
        }

        $settings['canSeeEEOInfo'] = $_SESSION['RESUFLO']->canSeeEEOInfo();

        return $settings;
    }

    /**
     * Sets an EEO setting for a site.
     *
     * @param string Setting name
     * @param string Setting value
     * @return void
     */
    public function set($setting, $value)
    {
        $sql = sprintf(
            "DELETE FROM
                settings
            WHERE
                settings.setting = %s
            AND
                site_id = %s
            AND
                settings_type = %s",
            $this->_db->makeQueryStringOrNULL($setting),
            $this->_siteID,
            SETTINGS_EEO
        );
        $this->_db->query($sql);

        $sql = sprintf(
            "INSERT INTO settings (
                setting,
                value,
                site_id,
                settings_type
            )
            VALUES (
                %s,
                %s,
                %s,
                %s
            )",
            $this->_db->makeQueryStringOrNULL($setting),
            $this->_db->makeQueryStringOrNULL($value),
            $this->_siteID,
            SETTINGS_EEO
         );
         $this->_db->query($sql);
    }
}
