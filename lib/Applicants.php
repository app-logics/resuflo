<?php
/**
 * RESUFLO
 * Candidates Library
 *
 * Copyright (C) 2005 - 2007 Econn Technologies, Inc.
 *
 *
 * The contents of this file are subject to the RESUFLO Public License
 * Version 1.1a (the "License"); you may not use this file except in
 * compliance with the License. You may obtain a copy of the License at
 *
 * Software distributed under the License is distributed on an "AS IS"
 * basis, WITHOUT WARRANTY OF ANY KIND, either express or implied. See the
 * License for the specific language governing rights and limitations
 * under the License.
 *
 * The Original Code is "RESUFLO Standard Edition".
 *
 * The Initial Developer of the Original Code is Econn Technologies, Inc.
 * Portions created by the Initial Developer are Copyright (C) 2005 - 2007
 * (or from the year in which this file was created to the year 2007) by
 * Econn Technologies, Inc. All Rights Reserved.
 *
 *
 * @package    RESUFLO
 * @subpackage Library
 * @copyright Copyright (C) 2005 - 2007 Econn Technologies, Inc.
 * @version    $Id: Candidates.php 3813 2007-12-05 23:16:22Z brian $
 */

include_once('./lib/Attachments.php');
include_once('./lib/Pipelines.php');
include_once('./lib/History.php');
include_once('./lib/SavedLists.php');
include_once('./lib/ExtraFields.php');
include_once('./lib/Importoptout.php');
include_once('lib/DataGrid.php');
require_once('asset/constants.php');

include_once('lib/phpmailer6/vendor/autoload.php');
include_once('lib/UserMailer.php');

/**
 *  Candidates Library
 *  @package    RESUFLO
 *  @subpackage Library
 */
class Applicants
{
    private $_db;
    private $_siteID;

    public $extraFields;


    public function __construct($siteID)
    {
        $this->_siteID = $siteID;
        $this->_db = DatabaseConnection::getInstance();
        $this->extraFields = new ExtraFields($siteID, DATA_ITEM_CANDIDATE);
    }

    /**
     * Adds a candidate to the database and returns its candidate ID.
     *
     * @param string First name.
     * @param string Middle name / initial.
     * @param string Last name.
     * @param string Primary e-mail address.
     * @param string Secondary e-mail address.
     * @param string Home phone number.
     * @param string Mobile phone number.
     * @param string Work phone number.
     * @param string Address (can be multiple lines).
     * @param string City.
     * @param string State / province.
     * @param string Postal code.
     * @param string Source where this candidate was found.
     * @param string Key skills.
     * @param string Date available.
     * @param string Current employer.
     * @param boolean Is this candidate willing to relocate?
     * @param string Current pay rate / salary.
     * @param string Desired pay rate / salary.
     * @param string Misc. candidate notes.
     * @param string Candidate's personal web site.
     * @param integer Entered-by user ID.
     * @param integer Owner user ID.
     * @param string EEO gender, or '' to not specify.
     * @param string EEO gender, or '' to not specify.
     * @param string EEO veteran status, or '' to not specify.
     * @param string EEO disability status, or '' to not specify.
     * @param boolean Skip creating a history entry?
     * @return integer Candidate ID of new candidate, or -1 on failure.
     */
    public function add($firstName, $middleName, $lastName, $email1, $email2,
        $phoneHome, $phoneCell, $phoneWork, $address, $city, $state, $zip,
        $source, $keySkills, $dateAvailable, $currentEmployer, $canRelocate,
        $currentPay, $desiredPay, $notes, $webSite, $bestTimeToCall, $enteredBy, $owner,
        $gender = '', $race = '', $veteran = '', $disability = '',
        $skipHistory = false)
    {

		// import opt-out trumps all other things
		if (Importoptout::is_optout_email($email1) || Importoptout::is_optout_email($email2)) {
			return 0;
		}
 

       $userid= $_SESSION['RESUFLO']->getuserid();   
		if($email1 != '')
	   {
		  $sql="select count(*) as countno from candidate where (email1='".$email1."' or email2='".$email1."') and entered_by= '$userid'";
			
		   $query=mysql_query($sql);
		   $resourcedetail= mysql_fetch_assoc($query);
			if($resourcedetail['countno']>0)
			{
			 
					  // $this->_template->display('./modules/candidates/Add.tpl');
					   return -2;
				
			}
			else if(!eregi("^[_a-z0-9-]+(\.[_a-z0-9-]+)*@[a-z0-9-]+(\.[a-z0-9-]+)*(\.[a-z]{2,4})$", $email1))
				{
					return -4;
				}
			else
			{
			}	
		}
		if($email2 != ''){
			$sql1="select count(*) as countno from candidate where (email2='".$email2."' or email1='".$email2."') and entered_by='$userid'";
	 
		   $query=mysql_query($sql1);
		   $resourcedetail= mysql_fetch_assoc($query);
			if($resourcedetail['countno']>0)
			{
			 
					  // $this->_template->display('./modules/candidates/Add.tpl');
					   return -3;
				
			}	
			else if(!eregi("^[_a-z0-9-]+(\.[_a-z0-9-]+)*@[a-z0-9-]+(\.[a-z0-9-]+)*(\.[a-z]{2,4})$", $email2))
				{
					return -4;
				}
			else
			{
			}
        }
         $sql = sprintf(
            "INSERT INTO candidate (
                first_name,
                middle_name,
                last_name,
                email1,
                email2,
                phone_home,
                phone_cell,
                phone_work,
                address,
                city,
                state,
                zip,
                source,
                key_skills,
                date_available,
                current_employer,
                can_relocate,
                current_pay,
                desired_pay,
                notes,
                web_site,
                best_time_to_call,
                entered_by,
                is_hot,
                owner,
                site_id,
                date_created,
                date_modified,
                eeo_ethnic_type_id,
                eeo_veteran_type_id,
                eeo_disability_status,
                eeo_gender
            )
            VALUES (
                %s,
                %s,
                %s,
                %s,
                %s,
                ExtractNumber(%s),
                ExtractNumber(%s),
                ExtractNumber(%s),
                %s,
                %s,
                %s,
                %s,
                %s,
                %s,
                %s,
                %s,
                %s,
                %s,
                %s,
                %s,
                %s,
                %s,
                %s,
                0,
                %s,
                %s,
                NOW(),
                NOW(),
                %s,
                %s,
                %s,
                %s
            )",
            $this->_db->makeQueryString(trim($firstName)),
            $this->_db->makeQueryString(trim($middleName)),
            $this->_db->makeQueryString(trim($lastName)),
            $this->_db->makeQueryString(trim($email1)),
            $this->_db->makeQueryString(trim($email2)),
            $this->_db->makeQueryString(trim($phoneHome)),
            $this->_db->makeQueryString(trim($phoneCell)),
            $this->_db->makeQueryString(trim($phoneWork)),
            $this->_db->makeQueryString(trim($address)),
            $this->_db->makeQueryString(trim($city)),
            $this->_db->makeQueryString(trim($state)),
            $this->_db->makeQueryString(trim($zip)),
            $this->_db->makeQueryString(trim($source)),
            $this->_db->makeQueryString(trim($keySkills)),
            $this->_db->makeQueryStringOrNULL($dateAvailable),
            $this->_db->makeQueryString($currentEmployer),
            ($canRelocate ? '1' : '0'),
            $this->_db->makeQueryString(trim($currentPay)),
            $this->_db->makeQueryString(trim($desiredPay)),
            $this->_db->makeQueryString(trim($notes)),
            $this->_db->makeQueryString(trim($webSite)),
            $this->_db->makeQueryString(trim($bestTimeToCall)),
            $this->_db->makeQueryInteger(trim($enteredBy)),
            $this->_db->makeQueryInteger(trim($owner)),
            $this->_siteID,
            $this->_db->makeQueryInteger(trim($race)),
            $this->_db->makeQueryInteger(trim($veteran)),
            $this->_db->makeQueryString(trim($disability)),
            $this->_db->makeQueryString(trim($gender))
        );

        $queryResult = $this->_db->query($sql);
        if (!$queryResult)
        {
            return -1;
        }
        $rsEmail =  $this->_db->getAssoc('SELECT email FROM user WHERE site_id = '.$this->_siteID.' AND access_level = 500 LIMIT 1');
		$to = $rsEmail['email'];			
		$subject = "New Candidate Added";
		/* To send HTML mail, you can set the Content-type header. */
		$headers  = "MIME-Version: 1.0\n";
		$headers .= "Content-type: text/html; charset=iso-8859-1\n";
		$headers .= "From: www.RESUFLO.net \n";
		
		$mailmessage = "<html>
					<body>
					<table>
					<tr><td>Hello Admin</td></tr>
					<tr><td>&nbsp;</td></tr>
					<tr><td>
					This is to inform you that the following new candidate is added on RESUFLO.net.<br /><br />

Name : ".$firstName." ".$lastName."<br />
Email : ".$email1."<br>
Phone : ".$phoneCell."
					</td></tr>
					<tr><td>&nbsp;</td></tr>
					<tr><td>Thanks<br> www.RESUFLO.net</td></tr>
					</table>
					</body>
					</html>";	
    // je decommissioned the "New Candidate Added" email
		//mail($to, $subject, $mailmessage, $headers);

//        adding the php mailer start
        
        

 $userid = $_SESSION['RESUFLO']->getuserid(); 
$sql ="select candidate.email1, candidate.first_name, candidate.last_name,user.user_id,user.first_name as clientname,dripmarket_configuration.email_type, candidate.phone_home, candidate.date_created,dripmarket_compaign.cname,dripmarket_configuration.smtphost,dripmarket_configuration.smtppassword ,dripmarket_configuration.smtpport ,dripmarket_configuration.fromname,dripmarket_questionaire.id as quesid ,dripmarket_configuration.fromemail,dripmarket_configuration.bccemail,dripmarket_configuration.smtpuser,candidate.candidate_id,dripmarket_compaignemail.subject,dripmarket_compaignemail.creationdate,dripmarket_compaignemail.hour,dripmarket_compaignemail.min,dripmarket_compaignemail.Message,dripmarket_compaignemail.Timezone,dripmarket_compaignemail.sendemailon,dripmarket_compaign.id ,dripmarket_compaign.userid FROM `candidate` inner join user on user.user_id =candidate.entered_by INNER JOIN dripmarket_compaign ON user.user_id = dripmarket_compaign.userid inner JOIN dripmarket_compaignemail ON dripmarket_compaign.id = dripmarket_compaignemail.compaignid left JOIN dripmarket_questionaire ON dripmarket_questionaire.userid = dripmarket_compaign.userid inner join dripmarket_configuration on dripmarket_compaign.userid=dripmarket_configuration.userid where candidate.ishot=0 and candidate.unsubscribe=0 and candidate.candidate_id=(select max(candidate_id) from candidate) and dripmarket_compaignemail.sendemailon=0";

         $resourcedetail = $this->_db->query($sql);
         $sql="select max(candidate_id)as candid from candidate";
         $resourcedetailmax = $this->_db->query($sql);
         $maxid=$resourcedetailmax['candidate_id'];
        $date=date("y-m-d H:i:s");


while($row = mysql_fetch_array($resourcedetail))
{
		 $Subject    = $row['subject'];
		$bodytext=stripslashes($row['Message']);
		$questionaireurl='<a href="'.questionaireurl.'?quesid='.$row['quesid'].'">Please click here</a>';
		$quesidencode=base64_encode($row['quesid']);
			$cidencode=base64_encode($row['candidate_id']);
		 $candidate_id =$row['candidate_id'];
		
		$unsubscribeurl='To remove your name from our candidate list <a href="'.unsubscribeurl.'?quesid='.$quesidencode.'&cid='.$cidencode.'">Please click here</a>';

		$datefunc=date("d-M-y");
		$bodytext=str_replace('$$NAME$$',$row['first_name'],$bodytext);
                $bodytext=str_replace('$$FIRSTNAME$$',$row['first_name'],$bodytext);
                $bodytext=str_replace('$$LASTNAME$$',$row['last_name'],$bodytext);

		$bodytext=str_replace('$$EMAIL$$',$row['email1'],$bodytext);
		$bodytext=str_replace('$$PHONE$$',$row['phone_home'],$bodytext);
		$bodytext=str_replace('$$STARTDATE$$',$row['date_created'],$bodytext);
		$bodytext=str_replace('$$CURDATE$$',$datefunc,$bodytext);
		$bodytext=str_replace('$$QUESTIONNAIRE$$',$questionaireurl,$bodytext);
		$bodytext=str_replace('$$UNSUBSCRIBE$$',$unsubscribeurl,$bodytext);

		// import opt-out trumps all other things
		if (Importoptout::is_optout_email($row['email1'])) {
			return 0;
		}
                try{
                    file_put_contents('C:/Windows/Temp/ResufloLog/UserMailerCaller.txt', PHP_EOL.(new DateTime())->format('d m Y H:i:s').'===== START applicants', FILE_APPEND);
                    $email = new UserMailer($row['user_id']);
                    $r = $email->SendEmail(array($row['email1']), null, $Subject, $bodytext);
                    file_put_contents('C:/Windows/Temp/ResufloLog/UserMailerCaller.txt', '=> END ====='.$r, FILE_APPEND);
                }
                catch (Exception $e) {
                    file_put_contents('C:/Windows/Temp/ResufloLog/UserMailerCaller.txt', (new DateTime())->format('d m Y H:i:s').'applicants ====='.$e->getMessage().PHP_EOL, FILE_APPEND);
                }
			
/*		if(!$mail->Send()){
			echo "<script type='text/javascript'>alert('There is some error.Please contact administrator.');</script>";
		}else{
			echo "<script type='text/javascript'>alert('Please check your email to get the credentials');</script>";
		}*/
$sql = mysql_query("INSERT INTO activity (data_item_id, data_item_type, joborder_id, entered_by, type, notes, site_id, date_created, date_modified ) 		       			 VALUES ( '$candidate_id', '100', '0', '$userid', '200', '$Subject', '$this->_siteID', NOW(), NOW() ) ");
}
  
	if($sql =='1')
	{
		return $candidate_id;	
	}
$candidateID = $this->_db->getLastInsertID();

        if (!$skipHistory)
        {
            $history = new History($this->_siteID);
            $history->storeHistoryNew(DATA_ITEM_CANDIDATE, $candidateID);
        }

        return $candidateID;
    }
	

/*		if(!$mail->Send()){
			echo "<script type='text/javascript'>alert('There is some error.Please contact administrator.');</script>";
		}else{
			echo "<script type='text/javascript'>alert('Please check your email to get the credentials');</script>";
		}*/

	
	/*
	*function to insert extra field data while uploading a zip
	*/
	public function InsertExtraFields($xmlArr,$candidateId){
		$siteid = $this->_siteID;
		if($xmlArr['Category']!=''){
		$sql = "INSERT INTO extra_field (data_item_id,field_name,value,import_id,site_id,data_item_type) 
				values('".$candidateId."','Category','".$xmlArr['Category']."','0','".$siteid."','100')";
		$queryResult = $this->_db->query($sql);
		}
		if($xmlArr['DateOfBirth']!=''){
		$sql = "INSERT INTO extra_field (data_item_id,field_name,value,import_id,site_id,data_item_type) 
				values('".$candidateId."','Date Of Birth','".$xmlArr['DateOfBirth']."','0','".$siteid."','100')";
		$queryResult = $this->_db->query($sql);
		}
		if($xmlArr['Gender']!=''){
		$sql = "INSERT INTO extra_field (data_item_id,field_name,value,import_id,site_id,data_item_type) 
				values('".$candidateId."','Gender','".$xmlArr['Gender']."','0','".$siteid."','100')";
		$queryResult = $this->_db->query($sql);
		}
		if($xmlArr['FatherName']!=''){
		$sql = "INSERT INTO extra_field (data_item_id,field_name,value,import_id,site_id,data_item_type) 
				values('".$candidateId."','Father Name','".$xmlArr['FatherName']."','0','".$siteid."','100')";
		$queryResult = $this->_db->query($sql);
		}
		if($xmlArr['MotherName']!=''){
		$sql = "INSERT INTO extra_field (data_item_id,field_name,value,import_id,site_id,data_item_type) 
				values('".$candidateId."','Mother Name','".$xmlArr['MotherName']."','0','".$siteid."','100')";
		$queryResult = $this->_db->query($sql);
		}
		if($xmlArr['MaritalStatus']!=''){
		$sql = "INSERT INTO extra_field (data_item_id,field_name,value,import_id,site_id,data_item_type) 
				values('".$candidateId."','Marital Status','".$xmlArr['MaritalStatus']."','0','".$siteid."','100')";
		$queryResult = $this->_db->query($sql);
		}
		if($xmlArr['PassportNo']!=''){
		$sql = "INSERT INTO extra_field (data_item_id,field_name,value,import_id,site_id,data_item_type) 
				values('".$candidateId."','Passport No','".$xmlArr['PassportNo']."','0','".$siteid."','100')";
		$queryResult = $this->_db->query($sql);
		}
		if($xmlArr['Nationality']!=''){
		$sql = "INSERT INTO extra_field (data_item_id,field_name,value,import_id,site_id,data_item_type) 
				values('".$candidateId."','Nationality','".$xmlArr['Nationality']."','0','".$siteid."','100')";
		$queryResult = $this->_db->query($sql);
		}
		if($xmlArr['WorkedPeriod']!=''){
		$sql = "INSERT INTO extra_field (data_item_id,field_name,value,import_id,site_id,data_item_type) 
				values('".$candidateId."','Worked Period','".$xmlArr['WorkedPeriod']."','0','".$siteid."','100')";
		$queryResult = $this->_db->query($sql);
		}
		if($xmlArr['GapPeriod']!=''){
		$sql = "INSERT INTO extra_field (data_item_id,field_name,value,import_id,site_id,data_item_type) 
				values('".$candidateId."','Gap Period','".$xmlArr['GapPeriod']."','0','".$siteid."','100')";
		$queryResult = $this->_db->query($sql);
		}
		if($xmlArr['FaxNo']!=''){
		$sql = "INSERT INTO extra_field (data_item_id,field_name,value,import_id,site_id,data_item_type) 
				values('".$candidateId."','Fax No','".$xmlArr['FaxNo']."','0','".$siteid."','100')";
		$queryResult = $this->_db->query($sql);
		}
		if($xmlArr['LicenseNo']!=''){
		$sql = "INSERT INTO extra_field (data_item_id,field_name,value,import_id,site_id,data_item_type) 
				values('".$candidateId."','License No','".$xmlArr['LicenseNo']."','0','".$siteid."','100')";
		$queryResult = $this->_db->query($sql);
		}
		if($xmlArr['Hobbies']!=''){
		$sql = "INSERT INTO extra_field (data_item_id,field_name,value,import_id,site_id,data_item_type) 
				values('".$candidateId."','Hobbies','".$xmlArr['Hobbies']."','0','".$siteid."','100')";
		$queryResult = $this->_db->query($sql);
		}
		if($xmlArr['Qualification']!=''){
		$sql = "INSERT INTO extra_field (data_item_id,field_name,value,import_id,site_id,data_item_type) 
				values('".$candidateId."','Qualification','".$xmlArr['Qualification']."','0','".$siteid."','100')";
		$queryResult = $this->_db->query($sql);
		}
		if($xmlArr['Achievments']!=''){
		$sql = "INSERT INTO extra_field (data_item_id,field_name,value,import_id,site_id,data_item_type) 
				values('".$candidateId."','Achievements','".$xmlArr['Achievments']."','0','".$siteid."','100')";
		$queryResult = $this->_db->query($sql);
		}
		if($xmlArr['Objectives']!=''){
		$sql = "INSERT INTO extra_field (data_item_id,field_name,value,import_id,site_id,data_item_type) 
				values('".$candidateId."','Objectives','".$xmlArr['Objectives']."','0','".$siteid."','100')";
		$queryResult = $this->_db->query($sql);
		}
		if($xmlArr['Experience']!=''){
		$sql = "INSERT INTO extra_field (data_item_id,field_name,value,import_id,site_id,data_item_type) 
				values('".$candidateId."','Experience','".$xmlArr['Experience']."','0','".$siteid."','100')";
		$queryResult = $this->_db->query($sql);
		}
		if($xmlArr['References']!=''){
		$sql = "INSERT INTO extra_field (data_item_id,field_name,value,import_id,site_id,data_item_type) 
				values('".$candidateId."','References','".$xmlArr['References']."','0','".$siteid."','100')";
		$queryResult = $this->_db->query($sql);
		}
		if($xmlArr['JobProfile']!=''){
		$sql = "INSERT INTO extra_field (data_item_id,field_name,value,import_id,site_id,data_item_type) 
				values('".$candidateId."','Job Profile','".$xmlArr['JobProfile']."','0','".$siteid."','100')";
		$queryResult = $this->_db->query($sql);
		}
	}



		
	public function SaveZipRecordForProcessing($filename,$key,$clientid=''){
		$to = $_SESSION['RESUFLO']->getEmail();
		$siteid = $this->_siteID;
		if($clientid==0 || $clientid==''){
		$userid = $_SESSION['resumeinsertuserid'];
		}else{
		
		$userid = $clientid;
		}
		$sql = "INSERT INTO processzip(filename,inqueue,emailalert,siteid,countrykey,userid) values('".$filename."','N','".$to."','".$siteid."','".$key."','".$userid."')";
		$queryResult = $this->_db->query($sql);
	}
	
    /**
     * Updates a candidate.
     *
     * @param integer Candidate ID to update.
     * @param string First name.
     * @param string Middle name / initial.
     * @param string Last name.
     * @param string Primary e-mail address.
     * @param string Secondary e-mail address.
     * @param string Home phone number.
     * @param string Mobile phone number.
     * @param string Work phone number.
     * @param string Address (can be multiple lines).
     * @param string City.
     * @param string State / province.
     * @param string Postal code.
     * @param string Source where this candidate was found.
     * @param string Key skills.
     * @param string Date available.
     * @param string Current employer.
     * @param boolean Is this candidate willing to relocate?
     * @param string Current pay rate / salary.
     * @param string Desired pay rate / salary.
     * @param string Misc. candidate notes.
     * @param string Candidate's personal web site.
     * @param integer Owner user ID.
     * @param string EEO gender, or '' to not specify.
     * @param string EEO gender, or '' to not specify.
     * @param string EEO veteran status, or '' to not specify.
     * @param string EEO disability status, or '' to not specify.
     * @return boolean True if successful; false otherwise.
     */
    public function update($candidateID, $isActive, $firstName, $middleName, $lastName,
        $email1, $email2, $phoneHome, $phoneCell, $phoneWork, $address,
        $city, $state, $zip, $source, $keySkills, $dateAvailable,
        $currentEmployer, $canRelocate, $currentPay, $desiredPay,
        $notes, $webSite, $bestTimeToCall, $owner, $isHot, $email, $emailAddress,
        $gender = '', $race = '', $veteran = '', $disability = '')
    {
        $sql = sprintf(
            "UPDATE
                candidate
            SET
                is_active             = %s,
                first_name            = %s,
                middle_name           = %s,
                last_name             = %s,
                email1                = %s,
                email2                = %s,
                phone_home            = ExtractNumber(%s),
                phone_work            = ExtractNumber(%s),
                phone_cell            = ExtractNumber(%s),
                address               = %s,
                city                  = %s,
                state                 = %s,
                zip                   = %s,
                source                = %s,
                key_skills            = %s,
                date_available        = %s,
                current_employer      = %s,
                current_pay           = %s,
                desired_pay           = %s,
                can_relocate          = %s,
                is_hot                = %s,
                notes                 = %s,
                web_site              = %s,
                best_time_to_call     = %s,
                owner                 = %s,
                date_modified         = NOW(),
                eeo_ethnic_type_id    = %s,
                eeo_veteran_type_id   = %s,
                eeo_disability_status = %s,
                eeo_gender            = %s
            WHERE
                candidate_id = %s
            AND
                site_id = %s",
            ($isActive ? '1' : '0'),
            $this->_db->makeQueryString($firstName),
            $this->_db->makeQueryString($middleName),
            $this->_db->makeQueryString($lastName),
            $this->_db->makeQueryString($email1),
            $this->_db->makeQueryString($email2),
            $this->_db->makeQueryString($phoneHome),
            $this->_db->makeQueryString($phoneWork),
            $this->_db->makeQueryString($phoneCell),
            $this->_db->makeQueryString($address),
            $this->_db->makeQueryString($city),
            $this->_db->makeQueryString($state),
            $this->_db->makeQueryString($zip),
            $this->_db->makeQueryString($source),
            $this->_db->makeQueryString($keySkills),
            $this->_db->makeQueryStringOrNULL($dateAvailable),
            $this->_db->makeQueryString($currentEmployer),
            $this->_db->makeQueryString($currentPay),
            $this->_db->makeQueryString($desiredPay),
            ($canRelocate ? '1' : '0'),
            ($isHot ? '1' : '0'),
            $this->_db->makeQueryString($notes),
            $this->_db->makeQueryString($webSite),
            $this->_db->makeQueryString($bestTimeToCall),
            $this->_db->makeQueryInteger($owner),
            $this->_db->makeQueryInteger($race),
            $this->_db->makeQueryInteger($veteran),
            $this->_db->makeQueryString($disability),
            $this->_db->makeQueryString($gender),
            $this->_db->makeQueryInteger($candidateID),
            $this->_siteID
        );

        $preHistory = $this->get($candidateID);
        $queryResult = $this->_db->query($sql);
        $postHistory = $this->get($candidateID);

        $history = new History($this->_siteID);
        $history->storeHistoryChanges(
            DATA_ITEM_CANDIDATE, $candidateID, $preHistory, $postHistory
        );

        if (!$queryResult)
        {
            return false;
        }

        if (!empty($emailAddress))
        {
            /* Send e-mail notification. */
            //FIXME: Make subject configurable.
            $mailer = new Mailer($this->_siteID);
            $mailerStatus = $mailer->sendToOne(
                array($emailAddress, ''),
                'RESUFLO Notification: Candidate Ownership Change',
                $email,
                true
            );
        }

        return true;
    }

    /**
     * Removes a candidate and all associated records from the system.
     *
     * @param integer Candidate ID to delete.
     * @return void
     */
    public function delete($candidateID)
    {
        
         $userid= $_SESSION['RESUFLO']->getuserid();
        
     
        /* Delete the candidate from candidate. */
         //$sql = "delete from dripmarket_quesans where cid=$candidateID and quesid in (select dripmarket_questable.id from dripmarket_questable left join dripmarket_questionaire on dripmarket_questionaire.id=dripmarket_questable.questionaireid where dripmarket_questionaire.userid=$userid)";
		 
		 $sql = "delete from candidate where candidate_id=$candidateID";
 
        $this->_db->query($sql);

       
    }

    /**
     * Returns all relevent candidate information for a given candidate ID.
     *
     * @param integer Candidate ID.
     * @return array Associative result set array of candidate data, or array()
     *               if no records were returned.
     */
    public function get($candidateID)
    {
        $sql = sprintf(
            "SELECT
                candidate.candidate_id AS candidateID,
                candidate.is_active AS isActive,
                candidate.first_name AS firstName,
                candidate.middle_name AS middleName,
                candidate.last_name AS lastName,
                candidate.email1 AS email1,
                candidate.email2 AS email2,
                candidate.phone_home AS phoneHome,
                candidate.phone_work AS phoneWork,
                candidate.phone_cell AS phoneCell,
                candidate.address AS address,
                candidate.city AS city,
                candidate.state AS state,
                candidate.zip AS zip,
                candidate.source AS source,
                candidate.key_skills AS keySkills,
                candidate.current_employer AS currentEmployer,
                candidate.current_pay AS currentPay,
                candidate.desired_pay AS desiredPay,
                candidate.notes AS notes,
                candidate.owner AS owner,
                candidate.can_relocate AS canRelocate,
                candidate.web_site AS webSite,
                candidate.best_time_to_call AS bestTimeToCall,
                candidate.is_hot AS isHot,
                candidate.is_admin_hidden AS isAdminHidden,
                DATE_FORMAT(
                    candidate.date_created, '%%m-%%d-%%y (%%h:%%i %%p)'
                ) AS dateCreated,
                DATE_FORMAT(
                    candidate.date_modified, '%%m-%%d-%%y (%%h:%%i %%p)'
                ) AS dateModified,
                COUNT(
                    candidate_joborder.joborder_id
                ) AS pipeline,
                (
                    SELECT
                        COUNT(*)
                    FROM
                        candidate_joborder_status_history
                    WHERE
                        candidate_id = %s
                    AND
                        status_to = %s
                    AND
                        site_id = %s
                ) AS submitted,
                CONCAT(
                    candidate.first_name, ' ', candidate.last_name
                ) AS candidateFullName,
                CONCAT(
                    entered_by_user.first_name, ' ', entered_by_user.last_name
                ) AS enteredByFullName,
                CONCAT(
                    owner_user.first_name, ' ', owner_user.last_name
                ) AS ownerFullName,
                owner_user.email AS owner_email,
                DATE_FORMAT(
                    candidate.date_available, '%%m-%%d-%%y'
                ) AS dateAvailable,
                eeo_ethnic_type.type AS eeoEthnicType,
                eeo_veteran_type.type AS eeoVeteranType,
                candidate.eeo_disability_status AS eeoDisabilityStatus,
                candidate.eeo_gender AS eeoGender,
                IF (candidate.eeo_gender = 'm',
                    'Male',
                    IF (candidate.eeo_gender = 'f',
                        'Female',
                        ''))
                     AS eeoGenderText
            FROM
                candidate
            LEFT JOIN user AS entered_by_user
                ON candidate.entered_by = entered_by_user.user_id
            LEFT JOIN user AS owner_user
                ON candidate.owner = owner_user.user_id
            LEFT JOIN candidate_joborder
                ON candidate.candidate_id = candidate_joborder.candidate_id
            LEFT JOIN eeo_ethnic_type
                ON eeo_ethnic_type.eeo_ethnic_type_id = candidate.eeo_ethnic_type_id
            LEFT JOIN eeo_veteran_type
                ON eeo_veteran_type.eeo_veteran_type_id = candidate.eeo_veteran_type_id
            WHERE
                candidate.candidate_id = %s
            AND
                candidate.site_id = %s
            GROUP BY
                candidate.candidate_id",
            $this->_db->makeQueryInteger($candidateID),
            PIPELINE_STATUS_SUBMITTED,
            $this->_siteID,
            $this->_db->makeQueryInteger($candidateID),
            $this->_siteID
        );

        return $this->_db->getAssoc($sql);
    }

    /**
     * Returns all candidate information relevent for the Edit Candidate page
     * for a given candidate ID.
     *
     * @param integer Candidate ID.
     * @return array Associative result set array of candidate data, or array()
     *               if no records were returned.
     */
    public function getForEditing($candidateID)
    {
        $sql = sprintf(
            "SELECT
                candidate.candidate_id AS candidateID,
                candidate.is_active AS isActive,
                candidate.first_name AS firstName,
                candidate.middle_name AS middleName,
                candidate.last_name AS lastName,
                candidate.email1 AS email1,
                candidate.email2 AS email2,
                candidate.phone_home AS phoneHome,
                candidate.phone_work AS phoneWork,
                candidate.phone_cell AS phoneCell,
                candidate.address AS address,
                candidate.city AS city,
                candidate.state AS state,
                candidate.zip AS zip,
                candidate.source AS source,
                candidate.key_skills AS keySkills,
                candidate.current_employer AS currentEmployer,
                candidate.current_pay AS currentPay,
                candidate.desired_pay AS desiredPay,
                candidate.notes AS notes,
                candidate.owner AS owner,
                candidate.can_relocate AS canRelocate,
                candidate.web_site AS webSite,
                candidate.best_time_to_call AS bestTimeToCall,
                candidate.is_hot AS isHot,
                candidate.eeo_ethnic_type_id AS eeoEthnicTypeID,
                candidate.eeo_veteran_type_id AS eeoVeteranTypeID,
                candidate.eeo_disability_status AS eeoDisabilityStatus,
                candidate.eeo_gender AS eeoGender,
                candidate.is_admin_hidden AS isAdminHidden,
                DATE_FORMAT(
                    candidate.date_available, '%%m-%%d-%%y'
                ) AS dateAvailable
            FROM
                candidate
            WHERE
                candidate.candidate_id = %s
            AND
                candidate.site_id = %s",
            $this->_db->makeQueryInteger($candidateID),
            $this->_siteID
        );

        return $this->_db->getAssoc($sql);
    }

    // FIXME: Document me.
    public function getExport($IDs)
    {
        if (count($IDs) != 0)
        {
            $IDsValidated = array();
            
            foreach ($IDs as $id)
            {
                $IDsValidated[] = $this->_db->makeQueryInteger($id);
            }
            
            $criterion = 'AND candidate.candidate_id IN ('.implode(',', $IDsValidated).')';
        }
        else
        {
            $criterion = '';
        }

        $sql = sprintf(
            "SELECT
                candidate.candidate_id AS candidateID,
                candidate.last_name AS lastName,
                candidate.first_name AS firstName,
                candidate.phone_home AS phoneHome,
                candidate.phone_cell AS phoneCell,
                candidate.email1 AS email1,
                candidate.key_skills as keySkills
            FROM
                candidate
            WHERE
                candidate.site_id = %s
                %s
            ORDER BY
                candidate.last_name ASC,
                candidate.first_name ASC",
            $this->_siteID,
            $criterion
        );

        return $this->_db->getAllAssoc($sql);
    }

    /**
     * Returns a candidate ID that matches the specified e-mail address.
     *
     * @param string Candidate e-mail address,
     * @return integer Candidate ID, or -1 if no matching candidates were
     *                 found.
     */
    public function getIDByEmail($email)
    {
        $sql = sprintf(
            "SELECT
                candidate.candidate_id AS candidateID
            FROM
                candidate
            WHERE
            (
                candidate.email1 = %s
                OR candidate.email2 = %s
            )
            AND
                candidate.site_id = %s",
            $this->_db->makeQueryString($email),
            $this->_db->makeQueryString($email),
            $this->_siteID
        );
        $rs = $this->_db->getAssoc($sql);

        if (empty($rs))
        {
            return -1;
        }

        return $rs['candidateID'];
    }

    /**
     * Returns the number of candidates in the system.  Useful
     * for determining if the friendly "no candidates in system"
     * should be displayed rather than the datagrid.
     *
     * @param boolean Include administratively hidden candidates?
     * @return integer Number of Candidates in site.
     */
    public function getCount($allowAdministrativeHidden = false)
    {
        if (!$allowAdministrativeHidden)
        {
            $adminHiddenCriterion = 'AND candidate.is_admin_hidden = 0';
        }
        else
        {
            $adminHiddenCriterion = '';
        }

        $sql = sprintf(
            "SELECT
                COUNT(*) AS totalCandidates
            FROM
                candidate
            WHERE
                candidate.site_id = %s
            %s",
            $this->_siteID,
            $adminHiddenCriterion
        );

        return $this->_db->getColumn($sql, 0, 0);
    }

    /**
     * Returns the entire candidates list.
     *
     * @param boolean Include administratively hidden candidates?
     * @return array Multi-dimensional associative result set array of
     *               candidates data, or array() if no records were returned.
     */
    public function getAll($allowAdministrativeHidden = false)
    {
        if (!$allowAdministrativeHidden)
        {
            $adminHiddenCriterion = 'AND candidate.is_admin_hidden = 0';
        }
        else
        {
            $adminHiddenCriterion = '';
        }

        $sql = sprintf(
            "SELECT
                candidate.candidate_id AS candidateID,
                candidate.last_name AS lastName,
                candidate.first_name AS firstName,
                candidate.phone_home AS phoneHome,
                candidate.phone_cell AS phoneCell,
                candidate.email1 AS email1,
                candidate.key_skills AS keySkills,
                candidate.is_hot AS isHot,
                DATE_FORMAT(
                    candidate.date_created, '%%m-%%d-%%y'
                ) AS dateCreated,
                DATE_FORMAT(
                    candidate.date_modified, '%%m-%%d-%%y'
                ) AS dateModified,
                candidate.date_created AS dateCreatedSort,
                owner_user.first_name AS ownerFirstName,
                owner_user.last_name AS ownerLastName
            FROM
                candidate
            LEFT JOIN user AS owner_user
                ON candidate.entered_by = user.user_id
            WHERE
                candidate.site_id = %s
            %s
            ORDER BY
                candidate.last_name ASC,
                candidate.first_name ASC",
            $this->_siteID,
            $adminHiddenCriterion
        );

        return $this->_db->getAllAssoc($sql);
    }

    /**
     * Returns all resumes for a candidate.
     *
     * @param integer Candidate ID.
     * @return array Multi-dimensional associative result set array of
     *               candidate attachments data, or array() if no records were
     *               returned.
     */
    public function getResumes($candidateID)
    {
        $sql = sprintf(
            "SELECT
                attachment.attachment_id AS attachmentID,
                attachment.data_item_id AS candidateID,
                attachment.title AS title,
                attachment.text AS text
            FROM
                attachment
            WHERE
                resume = 1
            AND
                attachment.data_item_type = %s
            AND
                attachment.data_item_id = %s
            AND
                attachment.site_id = %s",
            DATA_ITEM_CANDIDATE,
            $this->_db->makeQueryInteger($candidateID),
            $this->_siteID
        );

        return $this->_db->getAllAssoc($sql);
    }

    /**
     * Returns a candidate resume attachment by attachment.
     *
     * @param integer Attachment ID.
     * @return array Associative result set array of candidate / attachment
     *               data, or array() if no records were returned.
     */
    public function getResume($attachmentID)
    {
        $sql = sprintf(
            "SELECT
                attachment.attachment_id AS attachmentID,
                attachment.data_item_id AS candidateID,
                attachment.title AS title,
                attachment.text AS text,
                candidate.first_name AS firstName,
                candidate.last_name AS lastName
            FROM
                attachment
            LEFT JOIN candidate
                ON attachment.data_item_id = candidate.candidate_id
                AND attachment.site_id = candidate.site_id
            WHERE
                attachment.resume = 1
            AND
                attachment.attachment_id = %s
            AND
                attachment.site_id = %s",
            $this->_db->makeQueryInteger($attachmentID),
            $this->_siteID
        );

        return $this->_db->getAssoc($sql);
    }

    /**
     * Returns an array of job orders data (jobOrderID, title, companyName)
     * for the specified candidate ID.
     *
     * @param integer Candidate ID,
     * @return array Multi-dimensional associative result set array of
     *               job orders data, or array() if no records were returned.
     */
    public function getJobOrdersArray($candidateID)
    {
        $sql = sprintf(
            "SELECT
                joborder.joborder_id AS jobOrderID,
                joborder.title AS title,
                company.name AS companyName
            FROM
                joborder
            LEFT JOIN company
                ON joborder.company_id = company.company_id
            LEFT JOIN candidate_joborder
                ON joborder.joborder_id = candidate_joborder.joborder_id
            WHERE
                candidate_joborder.candidate_id = %s
            AND
                joborder.site_id = %s
            ORDER BY
                title ASC",
            $this->_db->makeQueryInteger($candidateID),
            $this->_siteID
        );

        return $this->_db->getAllAssoc($sql);
     }

    /**
     * Updates a candidate's modified timestamp.
     *
     * @param integer Candidate ID.
     * @return boolean Boolean was the query executed successfully?
     */
    public function updateModified($candidateID)
    {
        $sql = sprintf(
            "UPDATE
                candidate
            SET
                date_modified = NOW()
            WHERE
                candidate_id = %s
            AND
                site_id = %s",
            $this->_db->makeQueryInteger($candidateID),
            $this->_siteID
        );

        return (boolean) $this->_db->query($sql);
    }

    /**
     * Returns all upcoming events for the candidate.
     *
     * @param integer Candidate ID.
     * @return array Multi-dimensional associative result set array of
     *               candidate events data, or array() if no records were
     *               returned.
     */
    public function getUpcomingEvents($candidateID)
    {
        $calendar = new Calendar($this->_siteID);
        return $calendar->getUpcomingEventsByDataItem(
            DATA_ITEM_CANDIDATE, $candidateID
        );
    }

    /**
     * Gets all possible source suggestions for a site.
     *
     * @return array Multi-dimensional associative result set array of
     *               candidate sources data.
     */
    public function getPossibleSources()
    {
        $sql = sprintf(
            "SELECT
                candidate_source.source_id AS sourceID,
                candidate_source.name AS name
            FROM
                candidate_source
            WHERE
                candidate_source.site_id = %s
            ORDER BY
                candidate_source.name ASC",
            $this->_siteID
        );

        return $this->_db->getAllAssoc($sql);
    }

    /**
     * Updates a sites possible sources with an array generated
     * by getDifferencesFromList (ListEditor.php).
     *
     * @param array Result of ListEditor::getDifferencesFromList().
     * @return void
     */
    public function updatePossibleSources($updates)
    {
        $history = new History($this->_siteID);

        foreach ($updates as $update)
        {
            switch ($update[2])
            {
                case LIST_EDITOR_ADD:
                    $sql = sprintf(
                        "INSERT INTO candidate_source (
                            name,
                            site_id,
                            date_created
                         )
                         VALUES (
                            %s,
                            %s,
                            NOW()
                         )",
                         $this->_db->makeQueryString($update[0]),
                         $this->_siteID
                    );
                    $this->_db->query($sql);

                    break;

                case LIST_EDITOR_REMOVE:
                    $sql = sprintf(
                        "DELETE FROM
                            candidate_source
                         WHERE
                            source_id = %s
                         AND
                            site_id = %s",
                         $update[1],
                         $this->_siteID
                    );
                    $this->_db->query($sql);

                    break;

                case LIST_EDITOR_MODIFY:
                    $sql = sprintf(
                        "SELECT
                            name
                         FROM
                            candidate_source
                         WHERE
                            source_id = %s
                         AND
                            site_id = %s",
                         $this->_db->makeQueryInteger($update[1]),
                         $this->_siteID
                    );
                    $firstSource = $this->_db->getAssoc($sql);

                    $sql = sprintf(
                        "UPDATE
                            candidate
                         SET
                            source = %s
                         WHERE
                            source = %s
                         AND
                            site_id = %s",
                         $update[1],
                         $this->_db->makeQueryString($firstSource['name']),
                         $this->_siteID
                    );
                    $this->_db->query($sql);

                    $sql = sprintf(
                        "UPDATE
                            candidate_source
                         SET
                            name = %s
                         WHERE
                            source_id = %s
                         AND
                            site_id = %s",
                         $this->_db->makeQueryString($update[0]),
                         $this->_db->makeQueryInteger($update[1]),
                         $this->_siteID
                    );
                    $this->_db->query($sql);

                    break;

                default:
                    break;
            }
        }
    }

    /**
     * Changes the administrative hide / show flag.
     * Only can be accessed by a MSA or higher user.
     *
     * @param integer Candidate ID.
     * @param boolean Administratively hide this candidate?
     * @return boolean Was the query executed successfully?
     */    
    public function administrativeHideShow($candidateID, $state)
    {
        $sql = sprintf(
            "UPDATE
                candidate
            SET
                is_admin_hidden = %s
            WHERE
                candidate_id = %s
            AND
                site_id = %s",
            ($state ? 1 : 0),
            $this->_db->makeQueryInteger($candidateID),
            $this->_siteID
        );

        return (boolean) $this->_db->query($sql);
    }
}


class ApplicantsDataGrid extends DataGrid
{
    protected $_siteID;
	
    // FIXME: Fix ugly indenting - ~400 character lines = bad.
    public function __construct($instanceName, $siteID, $parameters, $misc = 0)
    {
        $userid= $_SESSION['RESUFLO']->getuserid(); 
        $this->_db = DatabaseConnection::getInstance();
        $this->_siteID = $siteID;
		if($_SESSION['RESUFLO']->getClientLogin()==1 && $_SESSION['RESUFLO']->getAccessLevel()==400){
       		
                if($_GET['m'] == 'Applicants' && $_GET['view'] == 'all'){
                    $this->_assignedCriterion = "and candidate.entered_by=".$_SESSION['RESUFLO']->getUserID();
                }else{
                    $this->_assignedCriterion = "and (candidate.entered_by IN (SELECT user_id FROM user where user_id=".$_SESSION['RESUFLO']->getUserID()." or entered_by=".$_SESSION['RESUFLO']->getUserID()."))";
                }
                
                }else if($_SESSION['RESUFLO']->getClientLogin()==1 && $_SESSION['RESUFLO']->getAccessLevel()!=400){
			$this->_assignedCriterion = "and candidate.entered_by=".$_SESSION['RESUFLO']->getUserID();
		}else if($_SESSION['RESUFLO']->getAccessLevel()==500){
			$this->_assignedCriterion = "";
		}else{
			$this->_assignedCriterion = "and candidate.entered_by=".$_SESSION['RESUFLO']->getUserID();
		}
		
            $this->_assignedCriterion .= " and follow_up = '0' and candidate.job_id is not null  ";
            $this->_dataItemIDColumn = 'candidate.candidate_id';
                
             $this->_classColumns = array(
			 
			  'Attachments' => array('select' => 'IF(candidate_joborder_submitted.candidate_joborder_id, 1, 0) AS submitted,
                                                IF(attachment_id, 1, 0) AS attachmentPresent',

                                     'pagerRender' => 'if ($rsData[\'submitted\'] == 1)
                                                    {
                                                        $return = \'<img src="images/job_orders.gif" alt="" width="16" height="16" title="Submitted for a Job Order" />\';
                                                    }
                                                    else
                                                    {
                                                        $return = \'<img src="images/mru/blank.gif" alt="" width="16" height="16" />\';
                                                    }

                                                    if ($rsData[\'attachmentPresent\'] == 1)
                                                    {
                                                        $return .= \'<img src="images/paperclip.gif" alt="" width="16" height="16" title="Attachment Present" />\';
                                                    }
                                                    else
                                                    {
                                                        $return .= \'<img src="images/mru/blank.gif" alt="" width="16" height="16" />\';
                                                    }

                                                    return $return;
                                                   ',

                                     'join'     => 'LEFT JOIN attachment
                                                        ON candidate.candidate_id = attachment.data_item_id
														AND attachment.data_item_type = '.DATA_ITEM_CANDIDATE.'
                                                    LEFT JOIN candidate_joborder AS candidate_joborder_submitted
                                                        ON candidate_joborder_submitted.candidate_id = candidate.candidate_id
                                                        AND candidate_joborder_submitted.status >= '.PIPELINE_STATUS_SUBMITTED.'
                                                        AND candidate_joborder_submitted.site_id = '.$this->_siteID.'
                                                        AND candidate_joborder_submitted.status != '.PIPELINE_STATUS_NOTINCONSIDERATION,
                                     'pagerWidth'    => 34,
                                     'pagerOptional' => true,
                                     'pagerNoTitle' => true,
                                     'sizable'  => false,
                                     'exportable' => false,
                                     'filterable' => false),
            'Job' =>     array('select'         => 'job.title AS jobTitle',
                                      'pagerRender'    => 'if ($rsData[\'isHot\'] == 1) $className =  \'jobLinkHot\'; else $className = \'jobLinkCold\'; return \'<a href="'.RESUFLOUtility::getIndexName().'?m=Applicants&amp;a=show&amp;candidateID=\'.$rsData[\'candidateID\'].\'&amp;view=1" class="\'.$className.\'">\'.htmlspecialchars($rsData[\'jobTitle\']).\'</a>\';',
                                      'sortableColumn' => 'jobTitle',
                                      'pagerWidth'     => 200,
                                      'pagerOptional'  => false,
                                      'alphaNavigation'=> true,
                                      'filter'         => 'job.title'),
          
            'First Name' =>     array('select'         => 'candidate.first_name AS firstName',
                                      'pagerRender'    => 'if ($rsData[\'isHot\'] == 1) $className =  \'jobLinkHot\'; else $className = \'jobLinkCold\'; return \'<a href="'.RESUFLOUtility::getIndexName().'?m=Applicants&amp;a=show&amp;candidateID=\'.$rsData[\'candidateID\'].\'&amp;view=1" class="\'.$className.\'">\'.htmlspecialchars($rsData[\'firstName\']).\'</a>\';',
                                      'sortableColumn' => 'firstName',
                                      'pagerWidth'     => 75,
                                      'pagerOptional'  => false,
                                      'alphaNavigation'=> true,
                                      'filter'         => 'candidate.first_name'),

            'Last Name' =>      array('select'         => 'candidate.last_name AS lastName',
                                     'sortableColumn'  => 'lastName',
                                     'pagerRender'     => 'if ($rsData[\'isHot\'] == 1) $className =  \'jobLinkHot\'; else $className = \'jobLinkCold\'; return \'<a href="'.RESUFLOUtility::getIndexName().'?m=Applicants&amp;a=show&amp;candidateID=\'.$rsData[\'candidateID\'].\'&amp;view=1" class="\'.$className.\'">\'.htmlspecialchars($rsData[\'lastName\']).\'</a>\';',
                                     'pagerWidth'      => 85,
                                     'pagerOptional'   => false,
                                     'alphaNavigation' => true,
                                     'filter'         => 'candidate.last_name'),

									 'E-Mail' =>         array('select'   => 'candidate.email1 AS email1',
                                     'sortableColumn'     => 'email1',
                                     'pagerWidth'    => 80,
               

            'E-Mail' =>         array('select'   => 'candidate.email1 AS email1',
                                     'sortableColumn'     => 'email1',
                                      'pagerOptional'   => false,
                                     'pagerWidth'    => 80,
                                     'filter'         => 'candidate.email1'),			   'filter'         => 'candidate.email1'),

            '2nd E-Mail' =>     array('select'   => 'candidate.email2 AS email2',
                                     'sortableColumn'     => 'email2',
                                     'pagerWidth'    => 80,
                                     'filter'         => 'candidate.email2'),

            'Home Phone' =>     array('select'   => 'candidate.phone_home AS phoneHome',
                                     'sortableColumn'     => 'phoneHome',
                                     'pagerWidth'    => 80,
                                     'filter'         => 'candidate.phone_home'),

            'Cell Phone' =>     array('select'   => 'candidate.phone_cell AS phoneCell',
                                     'sortableColumn'     => 'phoneCell',
                                     'pagerWidth'    => 80,
                                     'filter'         => 'candidate.phone_cell'),

            'Work Phone' =>     array('select'   => 'candidate.phone_work AS phoneWork',
                                     'sortableColumn'     => 'phoneWork',
                                     'pagerWidth'    => 80),

							

            'Address' =>        array('select'   => 'candidate.address AS address',
                                     'sortableColumn'     => 'address',
                                     'pagerWidth'    => 250,
                                     'alphaNavigation' => true,
                                     'filter'         => 'candidate.address'),

            'City' =>           array('select'   => 'candidate.city AS city',
                                     'sortableColumn'     => 'city',
                                     'pagerWidth'    => 80,
                                     'alphaNavigation' => true,
                                     'filter'         => 'candidate.city'),
			  'Company' =>           array('select'   => 'Case When candidate.entered_by = '.$_SESSION['RESUFLO']->getUserId().' then "'.$_SESSION['RESUFLO']->getFirstName().'" else (select first_name from user where user_id =(select entered_by from user where user_id=candidate.entered_by) ) End as company',
                                     'sortableColumn'     => 'company',
                                     'pagerWidth'    => 80,
                                     'alphaNavigation' => true,
                                     'filter'         => 'candidate.entered_by'),


            'State' =>          array('select'   => 'candidate.state AS state',
                                     'sortableColumn'     => 'state',
                                     'filterType' => 'dropDown',
                                     'pagerWidth'    => 50,
                                     'alphaNavigation' => true,
                                     'filter'         => 'candidate.state'),

            'Zip' =>            array('select'  => 'candidate.zip AS zip',
                                     'sortableColumn'    => 'zip',
                                     'pagerWidth'   => 50,
                                     'filter'         => 'candidate.zip'),

            'Misc Notes' =>     array('select'  => 'candidate.notes AS notes',
                                     'sortableColumn'    => 'notes',
                                     'pagerWidth'   => 300,
                                     'filter'         => 'candidate.notes'),
				 'Web Site' =>      array('select'  => 'candidate.web_site AS webSite',
                                     'pagerRender'     => 'return \'<a href="\'.htmlspecialchars($rsData[\'webSite\']).\'">\'.htmlspecialchars($rsData[\'webSite\']).\'</a>\';',
                                     'sortableColumn'    => 'webSite',
                                     'pagerWidth'   => 80,
                                     'filter'         => 'candidate.web_site'),
									 
            'Key Skills' =>    array('select'  => 'candidate.key_skills AS keySkills',
                                     'pagerRender' => 'return substr(trim($rsData[\'keySkills\']), 0, 30) . (strlen(trim($rsData[\'keySkills\'])) > 30 ? \'...\' : \'\');',
                                     'sortableColumn'    => 'keySkills',
                                     'pagerWidth'   => 210,
                                     'filter'         => 'candidate.key_skills'),
				
            'Recent Status' => array('select'  => '(
                                                    SELECT
                                                        CONCAT(
                                                            \'<a href="'.RESUFLOUtility::getIndexName().'?m=joborders&amp;a=show&amp;jobOrderID=\',
                                                            joborder.joborder_id,
                                                            \'" title="\',
                                                            joborder.title,
                                                            \' (\',
                                                            company.name,
                                                            \')">\',
                                                            candidate_joborder_status.short_description,
                                                            \'</a>\'
                                                        )
                                                    FROM
                                                        candidate_joborder
                                                    LEFT JOIN candidate_joborder_status
                                                        ON candidate_joborder_status.candidate_joborder_status_id = candidate_joborder.status
                                                    LEFT JOIN joborder
                                                        ON joborder.joborder_id = candidate_joborder.joborder_id
                                                    LEFT JOIN company
                                                        ON joborder.company_id = company.company_id
                                                    WHERE
                                                        candidate_joborder.candidate_id = candidate.candidate_id
                                                    ORDER BY
                                                        candidate_joborder.date_modified DESC
                                                    LIMIT 1
                                                ) AS lastStatus
                                                ',
                                     'sort'    => 'lastStatus',
                                     'pagerRender'     => 'return $rsData[\'lastStatus\'];',
                                     'exportRender'     => 'return $rsData[\'lastStatus\'];',
                                     'pagerWidth'   => 140,
                                     'exportable' => false,
                                     'filterHaving'  => 'lastStatus',
                                     'filterTypes'   => '=~'),
									 
			'Activity Notes' =>     array('select'  => 'candidate.activity_notes AS activity_notes',
                                                     'sortableColumn'    => 'activity_notes',
                                                     'pagerWidth'   => 300,
                                                     'filter'         => 'candidate.activity_notes'),

													 
		    'Recent Status (Extended)' => array('select'  => '(
                                                    SELECT
                                                        CONCAT(
                                                            candidate_joborder_status.short_description,
                                                            \'<br />\',
                                                            \'<a href="'.RESUFLOUtility::getIndexName().'?m=companies&amp;a=show&amp;companyID=\',
                                                            company.company_id,
                                                            \'">\',
                                                            company.name,
                                                            \'</a> - \',
                                                            \'<a href="'.RESUFLOUtility::getIndexName().'?m=joborders&amp;a=show&amp;jobOrderID=\',
                                                            joborder.joborder_id,
                                                            \'">\',
                                                            joborder.title,
                                                            \'</a>\'
                                                        )
                                                    FROM
                                                        candidate_joborder
                                                    LEFT JOIN candidate_joborder_status
                                                        ON candidate_joborder_status.candidate_joborder_status_id = candidate_joborder.status
                                                    LEFT JOIN joborder
                                                        ON joborder.joborder_id = candidate_joborder.joborder_id
                                                    LEFT JOIN company
                                                        ON joborder.company_id = company.company_id
                                                    WHERE
                                                        candidate_joborder.candidate_id = candidate.candidate_id
                                                    ORDER BY
                                                        candidate_joborder.date_modified DESC
                                                    LIMIT 1
                                                ) AS lastStatusLong
                                                ',
                                     'sortableColumn'    => 'lastStatusLong',
                                     'pagerRender'     => 'return $rsData[\'lastStatusLong\'];',
                                     'pagerWidth'   => 310,
                                     'exportable' => false,
                                     'filterable' => false),
									 
									 'Source' =>        array('select'  => 'candidate.source AS source',
                                     'sortableColumn'    => 'source',
                                     'pagerWidth'   => 140,
                                     'alphaNavigation' => true,
                                     'filter'         => 'candidate.source'),

            'Available' =>     array('select'   => 'DATE_FORMAT(candidate.date_available, \'%m-%d-%y\') AS dateAvailable',
                                     'sortableColumn'     => 'dateAvailable',
                                     'pagerWidth'    => 60),

            'Current Employer' => array('select'  => 'candidate.current_employer AS currentEmployer',
                                     'sortableColumn'    => 'currentEmployer',
                                     'pagerWidth'   => 125,
                                     'alphaNavigation' => true,
                                     'filter'         => 'candidate.current_employer'),

            'Current Pay' => array('select'  => 'candidate.current_pay AS currentPay',
                                     'sortableColumn'    => 'currentPay',
                                     'pagerWidth'   => 125,
                                     'filter'         => 'candidate.current_pay',
                                     'filterTypes'   => '===>=<'),

            'Desired Pay' => array('select'  => 'candidate.desired_pay AS desiredPay',
                                     'sortableColumn'    => 'desiredPay',
                                     'pagerWidth'   => 125,
                                     'filter'         => 'candidate.desired_pay',
                                     'filterTypes'   => '===>=<'),
									 
									 'Can Relocate'  => array('select'  => 'candidate.can_relocate AS canRelocate',
                                     'pagerRender'     => 'return ($rsData[\'canRelocate\'] == 0 ? \'No\' : \'Yes\');',
                                     'exportRender'     => 'return ($rsData[\'canRelocate\'] == 0 ? \'No\' : \'Yes\');',
                                     'sortableColumn'    => 'canRelocate',
                                     'pagerWidth'   => 80,
                                     'filter'         => 'candidate.can_relocate'),

            'Recruiter' =>         array('select'   => 'owner_user.first_name AS ownerFirstName,' .
                                                   'owner_user.last_name AS ownerLastName,' .
                                                   'CONCAT(owner_user.last_name, owner_user.first_name) AS ownerSort',
                                     'join'     => 'LEFT JOIN user AS owner_user ON candidate.owner = owner_user.user_id',
                                     'pagerRender'      => 'return StringUtility::makeInitialName($rsData[\'ownerFirstName\'], $rsData[\'ownerLastName\'], false, LAST_NAME_MAXLEN);',
                                     'exportRender'     => 'return $rsData[\'ownerFirstName\'] . " " .$rsData[\'ownerLastName\'];',
                                     'sortableColumn'     => 'ownerSort',
                                     'pagerWidth'    => 75,
                                     'alphaNavigation' => true,
                                     'filter'         => 'CONCAT(owner_user.first_name, owner_user.last_name)'),

            'Created' =>       array('select'   => 'DATE_FORMAT(candidate.date_created, \'%m-%d-%y\') AS dateCreated',
                                     'pagerRender'      => 'return $rsData[\'dateCreated\'];',
                                     'sortableColumn'     => 'dateCreatedSort',
                                     'pagerWidth'    => 60,
                                     'filterHaving' => 'DATE_FORMAT(candidate.date_created, \'%m-%d-%y\')'),

            'Modified' =>      array('select'   => 'DATE_FORMAT(candidate.date_modified, \'%m-%d-%y\') AS dateModified',
                                     'pagerRender'      => 'return $rsData[\'dateModified\'];',
                                     'sortableColumn'     => 'dateModifiedSort',
                                     'pagerWidth'    => 60,
                                     'pagerOptional' => false,
                                     'filterHaving' => 'DATE_FORMAT(candidate.date_modified, \'%m-%d-%y\')'),
									 
             'SubmissionDate' => array('select'   => 'DATE_FORMAT(candidate.date_created, "%Y-%m-%d ") AS dateCreatedSort',
                                     'sortableColumn'     => 'dateCreatedSort',
                                     'pagerWidth'    => 80,
                                        'pagerOptional'   => false,
                                     'filter'         => 'candidate.date_created'),
		         
		

          

          

            /* This one only works when called from the saved list view.  Thats why it is not optional, filterable, or exportable.
             * FIXME:  Somehow make this defined in the associated savedListDataGrid class child.
             */
'Added To List' =>  array('select'   => 'DATE_FORMAT(saved_list_entry.date_created, \'%m-%d-%y\') AS dateAddedToList,
                                                     saved_list_entry.date_created AS dateAddedToListSort',
                                     'pagerRender'      => 'return $rsData[\'dateAddedToList\'];',
                                     'sortableColumn'     => 'dateAddedToListSort',
                                     'pagerWidth'    => 60,
                                     'pagerOptional' => false,
                                     'filterable' => false,
                                     'exportable' => false),

            'OwnerID' =>       array('select'    => '',
                                     'filter'    => 'candidate.owner',
                                     'pagerOptional' => false,
                                     'filterable' => false,
                                     'filterDescription' => 'Only My Candidates'),

            'IsHot' =>         array('select'    => '',
                                     'filter'    => 'candidate.is_hot',
                                     'pagerOptional' => false,
                                     'filterable' => false,
                                     'filterDescription' => 'Only Hot Candidates')
        );

        

        if (US_ZIPS_ENABLED)
        {
            $this->_classColumns['Near Zipcode'] =
                               array('select'  => 'candidate.zip AS zip',
                                     'filter' => 'candidate.zip',
                                     'pagerOptional' => false,
                                     'filterTypes'   => '=@');
        }

        /* Extra fields get added as columns here. */
        $candidates = new Applicants($this->_siteID);
        $extraFieldsRS = $candidates->extraFields->getSettings();
        foreach ($extraFieldsRS as $index => $data)
        {
            $fieldName = $data['fieldName'];

            if (!isset($this->_classColumns[$fieldName]))
            {
                $columnDefinition = $candidates->extraFields->getDataGridDefinition($index, $data, $this->_db);

                /* Return false for extra fields that should not be columns. */
                if ($columnDefinition !== false)
                {
                    $this->_classColumns[$fieldName] = $columnDefinition;
                }
            }
        }
		

        parent::__construct($instanceName, $parameters, $misc);
    }

    /**
     * Returns the sql statment for the pager.
     *
     * @return array Candidates data
     */
    public function getSQL($selectSQL, $joinSQL, $whereSQL, $havingSQL, $orderSQL, $limitSQL, $distinct = '', $instanceName='')
    {
        // FIXME: Factor out Session dependency.
        if ($_SESSION['RESUFLO']->isLoggedIn() && $_SESSION['RESUFLO']->getAccessLevel() < ACCESS_LEVEL_MULTI_SA)
        {
            $adminHiddenCriterion = 'AND candidate.is_admin_hidden = 0';
        }
        else
        {
            $adminHiddenCriterion = '';
        }
    //    echo $whereSQL;die();
        $joinSQL  .=' LEFT JOIN job on job.job_id=candidate.job_id '
            .' LEFT JOIN dripmarket_quesans on dripmarket_quesans.cid=candidate.candidate_id '
            . 'LEFT JOIN dripmarket_questable on dripmarket_quesans.quesid=dripmarket_questable.id '
            . 'LEFT JOIN dripmarket_questionaire on dripmarket_questionaire.id=dripmarket_questable.questionaireid '
            . 'LEFT JOIN user on user.user_id=candidate.entered_by ';

        if ($this->getMiscArgument() != 0)
        {
            $savedListID = (int) $this->getMiscArgument();
            $joinSQL  .= ' INNER JOIN saved_list_entry
                            ON saved_list_entry.data_item_type = '.DATA_ITEM_CANDIDATE.'
                            AND saved_list_entry.data_item_id = candidate.candidate_id
                            AND saved_list_entry.site_id = '.$this->_siteID.'
                            AND saved_list_entry.saved_list_id = '.$savedListID;
        }
        else
        {
            $joinSQL  .= ' LEFT JOIN saved_list_entry
                                    ON saved_list_entry.data_item_type = '.DATA_ITEM_CANDIDATE.'
                                    AND saved_list_entry.data_item_id = candidate.candidate_id
                                    AND saved_list_entry.site_id = '.$this->_siteID;         
        }

        $sql = sprintf(
            "SELECT SQL_CALC_FOUND_ROWS %s
                job.title AS JobTitle,
                candidate.candidate_id AS candidateID,
                candidate.candidate_id AS exportID,
                candidate.is_hot AS isHot,
                candidate.date_modified AS dateModifiedSort,
               
            %s
            FROM
                candidate
            %s
            WHERE
                candidate.site_id = %s
            %s
            %s
            %s
            GROUP BY candidate.candidate_id
            %s
            %s
            %s",
            $distinct,
            $selectSQL,
            $joinSQL,
            $this->_siteID,
            $adminHiddenCriterion,
            (strlen($whereSQL) > 0) ? ' AND ' . $whereSQL : '',
            $this->_assignedCriterion,
            (strlen($havingSQL) > 0) ? ' HAVING ' . $havingSQL : '',
            $orderSQL,
            $limitSQL
        );
        //echo $sql;
        return $sql;
    }
}

/**
 *  EEO Settings Library
 *  @package    RESUFLO
 *  @subpackage Library
 */
class EEOSettingsappilcant
{
    private $_db;
    private $_siteID;
    private $_userID;


    public function __construct($siteID)
    {
        $this->_siteID = $siteID;
        // FIXME: Factor out Session dependency.
        $this->_userID = $_SESSION['RESUFLO']->getUserID();
        $this->_db = DatabaseConnection::getInstance();
    }

 
    /**
     * Returns all EEO settings for a site.
     *
     * @return array (setting => value)
     */
    public function getAll()
    {
        /* Default values. */
        $settings = array(
            'enabled' => '0',
            'genderTracking' => '0',
            'ethnicTracking' => '0',
            'veteranTracking' => '0',
            'veteranTracking' => '0',
            'disabilityTracking' => '0',
            'canSeeEEOInfo' => false
        );

        $sql = sprintf(
            "SELECT
                settings.setting AS setting,
                settings.value AS value,
                settings.site_id AS siteID
            FROM
                settings
            WHERE
                settings.site_id = %s
            AND
                settings.settings_type = %s",
            $this->_siteID,
            SETTINGS_EEO
        );
        $rs = $this->_db->getAllAssoc($sql);

        /* Override default settings with settings from the database. */
        foreach ($rs as $rowIndex => $row)
        {
            foreach ($settings as $setting => $value)
            {
                if ($row['setting'] == $setting)
                {
                    $settings[$setting] = $row['value'];
                }
            }
        }

        $settings['canSeeEEOInfo'] = $_SESSION['RESUFLO']->canSeeEEOInfo();

        return $settings;
    }

    /**
     * Sets an EEO setting for a site.
     *
     * @param string Setting name
     * @param string Setting value
     * @return void
     */
    public function set($setting, $value)
    {
        $sql = sprintf(
            "DELETE FROM
                settings
            WHERE
                settings.setting = %s
            AND
                site_id = %s
            AND
                settings_type = %s",
            $this->_db->makeQueryStringOrNULL($setting),
            $this->_siteID,
            SETTINGS_EEO
        );
        $this->_db->query($sql);

        $sql = sprintf(
            "INSERT INTO settings (
                setting,
                value,
                site_id,
                settings_type
            )
            VALUES (
                %s,
                %s,
                %s,
                %s
            )",
            $this->_db->makeQueryStringOrNULL($setting),
            $this->_db->makeQueryStringOrNULL($value),
            $this->_siteID,
            SETTINGS_EEO
         );
         $this->_db->query($sql);
    }
}


?>
