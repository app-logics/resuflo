<?php

/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */

/**
 * Description of JobPost
 *
 * @author SoftLogics1
 */
class JobPost {
    //put your code here
    public $_db;
    public $_siteID;
    
    public function __construct($siteID = 0)
    {
        if($siteID == 0){
            $this->_siteID = $_SESSION['RESUFLO']->getSiteID();
        }
        else{
            $this->_siteID = $siteID;
        }
        $this->_db = DatabaseConnection::getInstance();
    }
    
    public function getJobBoard()
    {
        $sql = sprintf(
            "SELECT
                *
            FROM
                job_board"
        );

        return $this->_db->getAllAssoc($sql);
    }
    
    public function getJobBoardByJobBoardId($jobBoardId)
    {
        $sql = sprintf(
            "SELECT
                *
            FROM
                job_board
            WHERE
                jobBoardId = %d",
            $this->_db->makeQueryInteger($jobBoardId)
        );

        return $this->_db->getAssoc($sql);
    }
    
    public function getJobBoardWithPost($entered_by, $job_id)
    {
        $sql = sprintf(
            "SELECT 
                jb.*, MAX(jp.jobPostId) jobPostId, MAX(jp.dateCreated) PostDate 
            FROM job_board jb left join 
                job_post jp on jb.jobBoardId = jp.jobBoardId and jp.entered_by = %d and jp.jobId = %d
                group by jobboardid",
                $this->_db->makeQueryInteger($entered_by),
                $this->_db->makeQueryInteger($job_id)
        );
        return $this->_db->getAllAssoc($sql);
    }
    
    public function postJob($entered_by, $jobId, $jobBoardId)
    {
        $sql = sprintf(
            "INSERT INTO job_post (
                entered_by,
                jobId,
                jobBoardId,
                isPost,
                dateCreated
                ) 
                VALUES (
                    %s,
                    %s,
                    %s,
                    1,
                    NOW()
                )",
                $this->_db->makeQueryInteger($entered_by),
                $this->_db->makeQueryInteger($jobId),
                $this->_db->makeQueryInteger($jobBoardId)
        );
        $queryResult = $this->_db->query($sql);
        if (!$queryResult)
        {
            return -1;
        }

        $jobPostId = $this->_db->getLastInsertID();
        return $jobPostId;
    }
    
    public function repostJob($jobPostId) {
        $sql = sprintf(
                "UPDATE job_post SET dateCreated = NOW() WHERE jobPostId = %d",
                $this->_db->makeQueryInteger($jobPostId)
                );
        $queryResult = $this->_db->query($sql);

        if (!$queryResult)
        {
            return false;
        }
        return true;
    }
    public function ChargeCard($firstName, $LastName, $cardNumber, $expiryYear, $expiryMonth, $cvv, $payment)
    {
        $response = chargeCreditCard($payment);
        if ($response != null) {
            // Check to see if the API request was successfully received and acted upon
            if ($response->getMessages()->getResultCode() == \Configuration\Setting::AuthorizeNet_RESPONSE_OK) {
                // Since the API request was successful, look for a transaction response
                // and parse it to display the results of authorizing the card
                $tresponse = $response->getTransactionResponse();

                if ($tresponse != null && $tresponse->getMessages() != null) {
                    //echo " Successfully created transaction with Transaction ID: " . $tresponse->getTransId() . "\n";
                    //echo " Transaction Response Code: " . $tresponse->getResponseCode() . "\n";
                    //echo " Message Code: " . $tresponse->getMessages()[0]->getCode() . "\n";
                    //echo " Auth Code: " . $tresponse->getAuthCode() . "\n";
                    //echo " Description: " . $tresponse->getMessages()[0]->getDescription() . "\n";
                    return "OK:TransactionID=".$tresponse->getTransId().":Payment=".$payment.":ResponseCode=".$tresponse->getMessages()[0]->getCode().":Description=".$tresponse->getMessages()[0]->getDescription();
                } else {
                    //echo "Transaction Failed \n";
                    //if ($tresponse->getErrors() != null) {
                        //echo " Error Code  : " . $tresponse->getErrors()[0]->getErrorCode() . "</br>";
                        //echo " Error Message : " . $tresponse->getErrors()[0]->getErrorText() . "</br>";
                    //}
                    return "Error:Code=".$tresponse->getErrors()[0]->getErrorCode().":Message=".$tresponse->getErrors()[0]->getErrorText();
                }
                // Or, print errors if the API request wasn't successful
            } else {
//                echo "Transaction Failed </br>";
                $tresponse = $response->getTransactionResponse();

                if ($tresponse != null && $tresponse->getErrors() != null) {
//                    echo " Error Code  : " . $tresponse->getErrors()[0]->getErrorCode() . "</br>";
//                    echo " Error Message : " . $tresponse->getErrors()[0]->getErrorText() . "</br>";
                    return "Error:Code=".$tresponse->getErrors()[0]->getErrorCode().":Message=".$tresponse->getErrors()[0]->getErrorText();
                } else {
//                    echo " Error Code  : " . $response->getMessages()->getMessage()[0]->getCode() . "</br>";
//                    echo " Error Message : " . $response->getMessages()->getMessage()[0]->getText() . "</br>";
                    return "Error:Code=".$response->getMessages()->getMessage()[0]->getCode().":Message=".$response->getMessages()->getMessage()[0]->getText();
                }
            }
        } 
        else {
            return "OK:TransactionID=N/A:ResponseCode=N/A:MessageCode=N/A";
        }
    }
    
}
