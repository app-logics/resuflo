<?php

$dir = str_replace('\lib\\'.basename(__FILE__), '', __FILE__);
if (!class_exists('DatabaseConnection')) {
  include_once("$dir\lib\DatabaseConnection.php");
}

include_once("$dir\config.php");
include_once("$dir\constants.php");

class Importoptout {
    static function is_optout_email($email, $siteId = 1) {
        $_db = DatabaseConnection::getInstance($siteId);
        $query = 'select count(*) cnt from import_optout where email = "'.trim($email).'"';
        $rs = $_db->getAssoc($query);
        return $rs['cnt'];
    }
}
