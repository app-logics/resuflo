<?php

class AcuityScheduling {
    private $_userID;
    private $_key;
    private $_appointmentTypeId;
    public function __construct($userId, $key, $appointmentTypeId)
    {
        $this->_userID = $userId;
        $this->_key = $key;
        $this->_appointmentTypeId = $appointmentTypeId;
    }
    public function getAppointments(){
        // URL for all appointments (just an example):
        $url = 'https://acuityscheduling.com/api/v1/appointments.json';

        // Initiate curl:
        // GET request, so no need to worry about setting post vars:
        $ch = curl_init();
        curl_setopt($ch, CURLOPT_URL, $url);

        // Grab response as string:
        curl_setopt($ch, CURLOPT_RETURNTRANSFER, true);

        // HTTP auth:
        curl_setopt($ch, CURLOPT_USERPWD, "$this->_userID:$this->_key");

        // Execute request:
        $result = curl_exec($ch);

        // Don't forget to close the connection!
        curl_close($ch);

        $data = json_decode($result, true);
        //print_r($data);
        return $data;
    }
    public function getAppointmentTypes(){
        // URL for all appointments (just an example):
        $url = 'https://acuityscheduling.com/api/v1/appointment-types';

        // Initiate curl:
        // GET request, so no need to worry about setting post vars:
        $ch = curl_init();
        curl_setopt($ch, CURLOPT_URL, $url);

        // Grab response as string:
        curl_setopt($ch, CURLOPT_RETURNTRANSFER, true);

        // HTTP auth:
        curl_setopt($ch, CURLOPT_USERPWD, "$this->_userID:$this->_key");

        // Execute request:
        $result = curl_exec($ch);

        // Don't forget to close the connection!
        curl_close($ch);

        //for testing
        //$result = "[{\"id\": \"33230169\",\"name\": \"In Person, Individual Career Information Meeting\"},{\"id\": \"52073573\",\"name\": \"Career Information Session, In Person\"}]";
        $data = json_decode($result, true);
        //print_r($data);
        return array_reverse($data);
    }
    public function getAvailableDates($date, $appointmentTypeId){
        // URL for all appointments (just an example):
        $url = 'https://acuityscheduling.com/api/v1/availability/dates?month='.$date.'&appointmentTypeID='.$appointmentTypeId;

        // Initiate curl:
        // GET request, so no need to worry about setting post vars:
        $ch = curl_init();
        curl_setopt($ch, CURLOPT_URL, $url);

        // Grab response as string:
        curl_setopt($ch, CURLOPT_RETURNTRANSFER, true);

        // HTTP auth:
        curl_setopt($ch, CURLOPT_USERPWD, "$this->_userID:$this->_key");

        // Execute request:
        $result = curl_exec($ch);

        // Don't forget to close the connection!
        curl_close($ch);

        $data = json_decode($result, true);
        //print_r($data);
        return $data;
    }
    public function getAvailableTimes($date, $appointmentTypeId){
        // URL for all appointments (just an example):
        $url = 'https://acuityscheduling.com/api/v1/availability/times?date='.$date.'&appointmentTypeID='.$appointmentTypeId;

        // Initiate curl:
        // GET request, so no need to worry about setting post vars:
        $ch = curl_init();
        curl_setopt($ch, CURLOPT_URL, $url);

        // Grab response as string:
        curl_setopt($ch, CURLOPT_RETURNTRANSFER, true);

        // HTTP auth:
        curl_setopt($ch, CURLOPT_USERPWD, "$this->_userID:$this->_key");

        // Execute request:
        $result = curl_exec($ch);

        // Don't forget to close the connection!
        curl_close($ch);

        $data = json_decode($result, true);
        //print_r($data);
        return $data;
    }
    public function getAcuitySchedulingEvent(){
        $currentDate = new DateTime();
        $currentDate->add(new DateInterval('P3D'));
        
        $AppointmentTypes = $this->getAppointmentTypes();
        foreach ($AppointmentTypes as $Appointment) {
            if($Appointment["id"] == $this->_appointmentTypeId)
            {
                $AppointmentName = $Appointment["name"];
                $AppointmentDescription = $Appointment["description"];
                $AppointmentDuration = $Appointment["duration"];
            }
        }
        $times = array();
        for ($i = 0; $i < 18; $i++) {
            $times[] = $this->getAvailableTimes($currentDate->format('Y-m-d'), $this->_appointmentTypeId);
            $currentDate->add(new DateInterval('P1D'));
        }
        $acuityEvents = array();
        foreach($times as $time){
            foreach($time as $t){
                $newDate = date_create_from_format("Y-m-d H:i:s-u", str_replace("T"," ",$t['time']));
                $TimeZone = substr($t['time'], 19);
                $acuityEvents[] = array("Name" => $AppointmentName, "Description" => $AppointmentDescription, "Duration" => $AppointmentDuration, "Date" => $newDate, "TimeZone" => $TimeZone);
            }
        }
        return $acuityEvents;
    }
    public function createAppointment($firstName, $lastName, $email, $phone, $notes, $date)
    {
        $url = 'https://acuityscheduling.com/api/v1/appointments?admin=true';
        
        $curl = curl_init();
        curl_setopt($curl, CURLOPT_URL, $url);
        curl_setopt($curl, CURLOPT_RETURNTRANSFER, true);
        curl_setopt($curl, CURLOPT_USERPWD, "$this->_userID:$this->_key");
        curl_setopt($curl, CURLOPT_CUSTOMREQUEST, "POST");
        curl_setopt($curl, CURLOPT_POSTFIELDS, "{\"datetime\":\"$date\",\"appointmentTypeID\":$this->_appointmentTypeId,\"firstName\":\"$firstName\",\"lastName\":\"$lastName\",\"email\":\"$email\",\"phone\":\"$phone\",\"notes\":\"$notes\"}");

        $result = curl_exec($curl);
        $err = curl_error($curl);

        curl_close($curl);

        $data = json_decode($result, true);
        //print_r($data);
        return $data;
    }
}
