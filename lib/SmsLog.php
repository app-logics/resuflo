<?php

include_once('./lib/Candidates.php');
class SmsLog
{
    private $_db;
    private $_siteID;
    private $_userID;

    public function __construct($userId = 0, $siteID = 1)
    {
        $this->_siteID = $siteID;
        if($userId == 0){
            $this->_userID = $_SESSION['RESUFLO']->getUserID();
        }
        else
        {
            $this->_userID = $userId;
        }
        $this->_db = DatabaseConnection::getInstance($siteID);
    }
    public function AddToDripSmsReply($CandidateId, $userid, $MessageSid, $twillioFromNumber, $candidatePhone, $Body){
        $sql = sprintf(
            "INSERT INTO drip_sms_reply (
                CandidateId,
                UserId,
                MessageSid,
                `From`,
                `To`,
                Body,
                Direction
            )
            VALUES (
                %s,
                %s,
                %s,
                %s,
                %s,
                %s,
                1
            )",
            $this->_db->makeQueryInteger(trim($CandidateId)),
            $this->_db->makeQueryInteger(trim($userid)),
            $this->_db->makeQueryString(trim($MessageSid)),
            $this->_db->makeQueryString(trim($twillioFromNumber)),
            $this->_db->makeQueryString(trim($candidatePhone)),
            $this->_db->makeQueryString(trim($Body))
        );
        mysql_query($sql);
    }
    public function UpdateDripSmsReply($DripSmsReplyId, $MessageSid){
        $sql = sprintf(
            "UPDATE drip_sms_reply SET MessageSid = %s WHERE DripSmsReplyId = %d",
            $this->_db->makeQueryString(trim($MessageSid)),
            $this->_db->makeQueryInteger(trim($DripSmsReplyId))
        );
        mysql_query($sql);
    }
    public function GetSmsLogByUserId($UserId, $Search = null, $IsRead = 0, $Page = 1, $PageSize = 50)
    {
        if(!empty($Search))
        {
            $HavingCaluse = ($IsRead == null || $IsRead == 0)? "" : " Having IsRead > 0 ";
            $UnsubCandidates = ($IsRead == -1)? " AND c.unsubscribe = 1 " : " AND c.unsubscribe != 1 ";
            $Search = '%'.$Search.'%';
            $sql = sprintf(
                "SELECT 
                    c.candidate_id CandidateId, (SELECT d.Body FROM drip_sms_reply d WHERE d.CandidateId = c.candidate_id LIMIT 1) Body, (SELECT COUNT(*) FROM drip_sms_reply ds WHERE ds.CandidateId = c.candidate_id AND ds.Direction = 2 AND (ds.IsRead IS NULL OR ds.IsRead = 0) ) IsRead, c.first_name, c.last_name, c.phone_cell 
                FROM 
                    candidate c 
                WHERE c.entered_by = %d %s AND CONCAT(c.first_name, ' ', c.last_name) LIKE %s
                %s
                LIMIT %d OFFSET %d",
                $this->_db->makeQueryInteger($UserId),
                $UnsubCandidates,
                $this->_db->makeQueryString($Search),
                $HavingCaluse,
                $this->_db->makeQueryInteger($PageSize),
                $this->_db->makeQueryInteger(($PageSize * ($Page-1)))
            );
            $SmsLogs = $this->_db->getAllAssoc($sql);
            return $SmsLogs;
        }
        else
        {
            $HavingCaluse = ($IsRead == null || $IsRead == 0)? "" : " Having IsRead > 0 ";
            $UnsubCandidates = ($IsRead == -1)? " AND cc.unsubscribe = 1 " : " AND cc.unsubscribe != 1 ";
            $sql = sprintf(
                "SELECT 
                    d.CandidateId, d.Body, (SELECT COUNT(*) FROM drip_sms_reply ds WHERE ds.CandidateId = c.candidate_id AND ds.Direction = 2 AND (ds.IsRead IS NULL OR ds.IsRead = 0) ) IsRead, c.first_name, c.last_name, c.phone_cell 
                FROM 
                    drip_sms_reply d LEFT JOIN
                    candidate c on d.CandidateId = c.candidate_id  
                WHERE 
                    DripSmsReplyId IN (SELECT MAX(dd.DripSmsReplyId) FROM drip_sms_reply dd INNER JOIN candidate cc on dd.CandidateId = cc.candidate_id WHERE dd.UserId = %d %s GROUP BY dd.CandidateId) 
                %s
                ORDER BY DripSmsReplyId DESC 
                LIMIT %d OFFSET %d",
                $this->_db->makeQueryInteger($UserId),
                $UnsubCandidates,
                $HavingCaluse,
                $this->_db->makeQueryInteger($PageSize),
                $this->_db->makeQueryInteger(($PageSize * ($Page-1)))
            );
            $SmsLogs = $this->_db->getAllAssoc($sql);
            return $SmsLogs;
        }
    }
    public function GetSmsLogByUserIdCount($UserId, $Search = null, $IsRead = 0)
    {
        if(!empty($Search))
        {
            $UnsubCandidates = ($IsRead == -1)? " AND c.unsubscribe = 1 " : " AND c.unsubscribe != 1 ";
            $Search = '%'.$Search.'%';
            if($IsRead == null || $IsRead == 0)
            {
                $sql = sprintf(
                    "SELECT COUNT(*) Count FROM candidate c WHERE c.entered_by = %d %s AND CONCAT(c.first_name, ' ', c.last_name) LIKE %s",
                    $this->_db->makeQueryInteger($UserId),
                    $UnsubCandidates,
                    $this->_db->makeQueryString($Search)
                );
            }
            else
            {
                $sql = sprintf(
                    "SELECT
                        COUNT(*) Count
                    FROM
                        (SELECT 
                            c.candidate_id CandidateId, (SELECT d.Body FROM drip_sms_reply d WHERE d.CandidateId = c.candidate_id LIMIT 1) Body, (SELECT COUNT(*) FROM drip_sms_reply ds WHERE ds.CandidateId = c.candidate_id AND ds.Direction = 2 AND (ds.IsRead IS NULL OR ds.IsRead = 0) ) IsRead, c.first_name, c.last_name, c.phone_cell 
                        FROM 
                            candidate c 
                        WHERE c.entered_by = %d %s AND CONCAT(c.first_name, ' ', c.last_name) LIKE %s
                        HAVING IsRead > 0) Candidates;",
                    $this->_db->makeQueryInteger($UserId),
                    $UnsubCandidates,
                    $this->_db->makeQueryString($Search)
                );   
            }
            $SmsLogCount = $this->_db->getAllAssoc($sql);
            return $SmsLogCount;
        }
        else
        {
            $HavingCaluse = "";
            if($IsRead == null || $IsRead == 0)
            {
                $HavingCaluse = " AND c.unsubscribe != 1 ";
            }
            else if($IsRead == 1)
            {
                $HavingCaluse = " AND Direction = 2 AND (IsRead IS NULL OR IsRead = 0) AND c.unsubscribe != 1 ";
            }
            else if($IsRead == -1)
            {
                $HavingCaluse = " AND c.unsubscribe = 1 ";
            }
            $sql = sprintf(
                "SELECT
                    COUNT(*) Count
                FROM 
                    (SELECT COUNT(*) FROM drip_sms_reply d LEFT JOIN candidate c ON d.CandidateId = c.candidate_id WHERE d.UserId = %d %s GROUP BY d.CandidateId) Candidates;",
                $this->_db->makeQueryInteger($UserId),
                $HavingCaluse
            );
            $SmsLogCount = $this->_db->getAllAssoc($sql);
            return $SmsLogCount;
        }
    }
    public function GetUnReadSmsLogByUserIdCount($UserId){
        $sql = sprintf(
            "SELECT 
                COUNT(*) Count 
            FROM 
                drip_sms_reply d LEFT JOIN 
                candidate c on d.CandidateId = c.candidate_id 
            WHERE 
                d.UserId = %d AND d.Direction = 2 AND (d.IsRead IS NULL OR d.IsRead = 0) AND (c.unsubscribe is null OR c.unsubscribe = 0)",
               $this->_db->makeQueryInteger($UserId)
        );
        $SmsLogCount = $this->_db->getAllAssoc($sql);
        return $SmsLogCount;
    }
    public function GetSmsLogByCandidateId($UserId, $CandidateId)
    {
        $sql = sprintf(
            "SELECT
                COUNT(*) 
            FROM 
                drip_sms_reply 
            WHERE 
                UserId = %d AND CandidateId = %d",
                $this->_db->makeQueryInteger($UserId),
                $this->_db->makeQueryInteger($CandidateId)
        );
        $SmsLogCount = $this->_db->getAllAssoc($sql);
        return $SmsLogCount;
    }
    public function GetSmsLogBySmsLogId($DripSmsReplyId)
    {
        $sql = sprintf(
            "SELECT
                *
            FROM 
                drip_sms_reply 
            WHERE 
                DripSmsReplyId = %d",
                $this->_db->makeQueryInteger($DripSmsReplyId)
        );
        $SmsLog = $this->_db->getAllAssoc($sql);
        return $SmsLog;
    }
}
