<?php
/**
 * RESUFLO
 * Job Orders Library
 *
 * Copyright (C) 2005 - 2007 Econn Technologies, Inc.
 *
 *
 * The contents of this file are subject to the RESUFLO Public License
 * Version 1.1a (the "License"); you may not use this file except in
 * compliance with the License. You may obtain a copy of the License at
 * http://www.econnoisseur.com/.
 *
 * Software distributed under the License is distributed on an "AS IS"
 * basis, WITHOUT WARRANTY OF ANY KIND, either express or implied. See the
 * License for the specific language governing rights and limitations
 * under the License.
 *
 * The Original Code is "RESUFLO Standard Edition".
 *
 * The Initial Developer of the Original Code is Econn Technologies, Inc.
 * Portions created by the Initial Developer are Copyright (C) 2005 - 2007
 * (or from the year in which this file was created to the year 2007) by
 * Econn Technologies, Inc. All Rights Reserved.
 *
 *
 * @package    RESUFLO
 * @subpackage Library
 * @copyright Copyright (C) 2005 - 2007 Econn Technologies, Inc.
 * @version    $Id: JobOrders.php 3829 2007-12-11 21:17:46Z brian $
 */


include_once('./lib/Pipelines.php');
include_once('./lib/Calendar.php');
include_once('./lib/Pager.php');
include_once('./lib/History.php');
include_once('./lib/DataGrid.php');

/**
 *	Job Orders Library
 *	@package    RESUFLO
 *	@subpackage Library
 */
class Job
{
    private $_db;
    private $_siteID;
    
    public $job_id;
    public $entered_by;
    public $status;
    public $title;
    public $category;
    public $type;
    public $experience;
    public $email;
    public $phone;
    public $salary;
    public $company_name;
    public $company_url;
    public $address;
    public $zip_code;
    public $city;
    public $state;
    public $country;
    public $description;

    public function __construct($siteID = 0)
    {
        if($siteID == 0){
            $this->_siteID = $_SESSION['RESUFLO']->getSiteID();
        }
        else{
            $this->_siteID = $siteID;
        }
        $this->_db = DatabaseConnection::getInstance();
        $this->entered_by = $_SESSION['RESUFLO']->getUserID();
    }
    
    public function loadFromPost()
    {
        $this->job_id = $_POST['job_id'];
        $this->status = $_POST['status'];
        $this->title = $_POST['title'];
        $this->category = $_POST['category'];
        $this->type = $_POST['type'];
        $this->experience = $_POST['experience'];
        $this->email = $_POST['email'];
        $this->phone = $_POST['phone'];
        $this->salary = $_POST['salary'];
        $this->company_name = $_POST['company_name'];
        $this->company_url = $_POST['company_url'];
        $this->address = $_POST['address'];
        $this->zip_code = $_POST['zip_code'];
        $this->city = $_POST['city'];
        $this->state = $_POST['state'];
        $this->country = 'USA';
        $this->description = $_POST['description'];
    }

    public function add()
    {
        $sql = sprintf(
            "INSERT INTO `job`
            (
            `site_id`,
            `entered_by`,
            `title`,
            `email`,
            `phone`,
            `company_name`,
            `company_url`,
            `address`,
            `city`,
            `state`,
            `country`,
            `zip_code`,
            `salary`,
            `category`,
            `experience`,
            `type`,
            `description`,
            `is_active`,
            `status`,
            `date_created`)
            VALUES
            (
            %s,
            %s,
            %s,
            %s,
            %s,
            %s,
            %s,
            %s,
            %s,
            %s,
            'USA',
            %s,
            %s,
            %s,
            %s,
            %s,
            %s,
            1,
            %s,
            NOW()
            );",
            $this->_siteID,
            $this->_db->makeQueryInteger($this->entered_by),
            $this->_db->makeQueryString($this->title),
            $this->_db->makeQueryString($this->email),
            $this->_db->makeQueryString($this->phone),
            $this->_db->makeQueryString($this->company_name),
            $this->_db->makeQueryString($this->company_url),
            $this->_db->makeQueryString($this->address),
            $this->_db->makeQueryString($this->city),
            $this->_db->makeQueryString($this->state),
            $this->_db->makeQueryString($this->zip_code),
            $this->_db->makeQueryString($this->salary),
            $this->_db->makeQueryInteger($this->category),
            $this->_db->makeQueryInteger($this->experience),
            $this->_db->makeQueryInteger($this->type),
            $this->_db->makeQueryString($this->description),
            $this->_db->makeQueryInteger($this->status)
        );
        $queryResult = $this->_db->query($sql);
        if (!$queryResult)
        {
            return -1;
        }

        $jobID = $this->_db->getLastInsertID();
        return $jobID;
    }
	
    public function update()
    {
       
        $sql = sprintf(
            "UPDATE `job`
            SET
            `title` = %s,
            `email` = %s,
            `phone` = %s,
            `company_name` = %s,
            `company_url` = %s,
            `address` = %s,
            `city` = %s,
            `state` = %s,
            `zip_code` = %s,
            `salary` = %s,
            `category` = %s,
            `experience` = %s,
            `type` = %s,
            `description` = %s,
            `status` = %s,
            `date_modified` = NOW()
            WHERE job_id = %d AND entered_by = %d",
            $this->_db->makeQueryString($this->title),
            $this->_db->makeQueryString($this->email),
            $this->_db->makeQueryString($this->phone),
            $this->_db->makeQueryString($this->company_name),
            $this->_db->makeQueryString($this->company_url),
            $this->_db->makeQueryString($this->address),
            $this->_db->makeQueryString($this->city),
            $this->_db->makeQueryString($this->state),
            $this->_db->makeQueryString($this->zip_code),
            $this->_db->makeQueryString($this->salary),
            $this->_db->makeQueryInteger($this->category),
            $this->_db->makeQueryInteger($this->experience),
            $this->_db->makeQueryInteger($this->type),
            $this->_db->makeQueryString($this->description),
            $this->_db->makeQueryInteger($this->status),
            $this->_db->makeQueryInteger($this->job_id),
            $this->_db->makeQueryInteger($this->entered_by)
        );
        $queryResult = $this->_db->query($sql);

        if (!$queryResult)
        {
            return false;
        }
        return true;
    }
    
    public function getAll()
    {
        $sql = sprintf(
            "SELECT
                *
            FROM
                job
            
            WHERE
                job_id > 1 and is_active = 1 and job.entered_by = %s
            AND
                job.site_id = %s",
            $this->_db->makeQueryInteger($this->entered_by),
            $this->_siteID
        );
        return $this->_db->getAllAssoc($sql);
    }
    public function getJobById()
    {
        $sql = sprintf(
            "SELECT
                *
            FROM
                job
            
            WHERE
                job_id = %d and job.entered_by = %s
            AND
                job.site_id = %s",
            $this->_db->makeQueryInteger($this->job_id),
            $this->_db->makeQueryInteger($this->entered_by),
            $this->_siteID
        );
        return $this->_db->getAssoc($sql);
    }
    public function get($enteredBy, $siteId = 1)
    {
        if($siteId != 1){
            $this->_db = DatabaseConnection::getInstance($siteId);
        }
        $sql = sprintf(
            "SELECT
                *
            FROM
                job
            
            WHERE
                job.entered_by = %s
            AND
                job.site_id = %s",
            $this->_db->makeQueryInteger($enteredBy),
            $this->_siteID
        );
        return $this->_db->getAssoc($sql);
    }
    public function delete(){
        $sql = sprintf(
            "UPDATE
                job
            SET 
                is_active = 0
            WHERE
                job_id = %d
            AND
                entered_by = %d",
            $this->_db->makeQueryInteger($this->job_id),
            $this->_db->makeQueryInteger($this->entered_by)
        );
        $queryResult = $this->_db->query($sql);
        if (!$queryResult)
        {
            return -1;
        }    
        return 1;
    }
}

?>
