<?php


/**
 * RESUFLO
 * Calendar Library
 *
 * Copyright (C) 2005 - 2007 Econn Technologies, Inc.
 *
 *
 * The contents of this file are subject to the RESUFLO Public License
 * Version 1.1a (the "License"); you may not use this file except in
 * compliance with the License. You may obtain a copy of the License at
 * http://www.econnoisseur.com/.
 *
 * Software distributed under the License is distributed on an "AS IS"
 * basis, WITHOUT WARRANTY OF ANY KIND, either express or implied. See the
 * License for the specific language governing rights and limitations
 * under the License.
 *
 * The Original Code is "RESUFLO Standard Edition".
 *
 * The Initial Developer of the Original Code is Econn Technologies, Inc.
 * Portions created by the Initial Developer are Copyright (C) 2005 - 2007
 * (or from the year in which this file was created to the year 2007) by
 * Econn Technologies, Inc. All Rights Reserved.
 *
 *
 * @package    RESUFLO
 * @subpackage Library
 * @copyright Copyright (C) 2005 - 2007 Econn Technologies, Inc.
 * @version    $Id: Calendar.php 3595 2007-11-13 17:41:18Z andrew $
 */

/* Calendar event type flags. */
define('CALENDAR_EVENT_CALL',      100);
define('CALENDAR_EVENT_EMAIL',     200);
define('CALENDAR_EVENT_MEETING',   300);
define('CALENDAR_EVENT_INTERVIEW', 400);
define('CALENDAR_EVENT_PERSONAL',  500);
define('CALENDAR_EVENT_OTHER',     600);


include_once('./lib/ResultSetUtility.php');
include_once('./lib/DateUtility.php');
include_once('./lib/Companies.php');
include_once('./lib/Candidates.php');
include_once('./lib/JobOrders.php');
include_once('./lib/Contacts.php');
include_once('./lib/Users.php');
include_once('./lib/UserMailer.php');

class LinkedIn
{
    private $_db;
    private $_siteID;
    private $_userID;


    public function __construct($siteID)
    {
        $this->_siteID = $siteID;
        // FIXME: Factor out Session dependency.
        $this->_userID = $_SESSION['RESUFLO']->getUserID();
        $this->_db = DatabaseConnection::getInstance();
    }


    /**
     * Returns all calendar settings for a site.
     *
     * @return array (setting => value)
     */
    public function getAllMessages()
    {
        $sql = sprintf(
            "SELECT a.is_read, c.import_id, l.profile_title, l.profile_linkedin_url, c.candidate_id, c.last_name, c.first_name, a.date_created FROM activity a LEFT JOIN candidate c ON a.data_item_id = c.candidate_id LEFT JOIN linkedin_messenger l on c.import_id = l.profile_id WHERE (type = 800 or type = 900) group by a.data_item_id ORDER BY a.activity_id DESC"
        );
        $profiles = $this->_db->getAllAssoc($sql);
        return $profiles;
    }
    
    public function getProfiles()
    {
        $sql = sprintf(
            "SELECT 
                *
            FROM 
                linkedin_messenger
            WHERE
                account is null
            GROUP BY
                profile_id
            ORDER BY 
                    date_created DESC"
        );
        $profiles = $this->_db->getAllAssoc($sql);
        return $profiles;
    }
    
    public function getConversation($CandidateId)
    {
        mysql_query("UPDATE activity SET is_read = 1 WHERE data_item_id=$CandidateId");
        $sql = sprintf(
            "SELECT 
                c.first_name, a.notes, a.date_created
            FROM 
                activity a
            LEFT JOIN
                candidate c on a.data_item_id = c.candidate_id
            WHERE
                data_item_id = %d
            ORDER BY 
                activity_id DESC",
            $CandidateId
        );
        $messages = $this->_db->getAllAssoc($sql);
        return $messages;
    }
    public function GetMessageThread($ProfileId)
    {
        $sql = sprintf(
            "SELECT 
                message_thread
            FROM 
                linkedin_messenger
            WHERE
                profile_id = $ProfileId and message_thread IS NOT NULL LIMIT 1"
        );
        $MessageThreadResult = mysql_query($sql);
        $result = mysql_result($MessageThreadResult, 0);
        return $result;
    }
    
    public function getUnreadCount()
    {
        $sql = sprintf(
            "SELECT 
                count(*)
            FROM 
                activity
            WHERE
                type = 800 and is_read is null"
        );
        $reminderTemplateCount = mysql_query($sql);
        $result = mysql_result($reminderTemplateCount, 0);
        return $result;
    }
}

?>
