<?php
/**
 * RESUFLO
 * Pipelines Library
 *
 * Copyright (C) 2005 - 2007 Econn Technologies, Inc.
 *
 *
 * The contents of this file are subject to the RESUFLO Public License
 * Version 1.1a (the "License"); you may not use this file except in
 * compliance with the License. You may obtain a copy of the License at
 *
 * Software distributed under the License is distributed on an "AS IS"
 * basis, WITHOUT WARRANTY OF ANY KIND, either express or implied. See the
 * License for the specific language governing rights and limitations
 * under the License.
 *
 * The Original Code is "RESUFLO Standard Edition".
 *
 * The Initial Developer of the Original Code is Econn Technologies, Inc.
 * Portions created by the Initial Developer are Copyright (C) 2005 - 2007
 * (or from the year in which this file was created to the year 2007) by
 * Econn Technologies, Inc. All Rights Reserved.
 *
 *
 * @package    RESUFLO
 * @subpackage Library
 * @copyright Copyright (C) 2005 - 2007 Econn Technologies, Inc.
 * @version    $Id: Pipelines.php 3593 2007-11-13 17:36:57Z andrew $
 */

include_once('./lib/History.php');
include_once('./lib/Importoptout.php');
include_once('./lib/UserMailer.php');

//use PHPMailer\PHPMailer\PHPMailer;
//include_once('phpmailer6/vendor/autoload.php');

/**
 *	Pipelines Library
 *	@package    RESUFLO
 *	@subpackage Library
 */
class Pipelines
{
    private $_db;
    private $_siteID;


    public function __construct($siteID)
    {
        $this->_siteID = $siteID;
        $this->_db = DatabaseConnection::getInstance();
        $this->dead = 0;
        $this->future = 0;
    }


    /**
     * Adds a candidate to the pipeline for a job order.
     *
     * @param integer job order ID
     * @param integer candidate ID
     * @return true on success; false otherwise.
     */
    public function add($candidateID, $jobOrderID, $userID = 0)
    {
        $sql = sprintf(
            "SELECT
                COUNT(candidate_id) AS candidateIDCount
            FROM
                candidate_joborder
            WHERE
                candidate_id = %s
            AND
                joborder_id = %s
            AND
                site_id = %s",
            $this->_db->makeQueryInteger($candidateID),
            $this->_db->makeQueryInteger($jobOrderID),
            $this->_siteID
        );
        $rs = $this->_db->getAssoc($sql);

        if (empty($rs))
        {
            return false;
        }

        $count = $rs['candidateIDCount'];
        if ($count > 0)
        {
            /* Candidate already exists in the pipeline. */
            return false;
        }

        $extraFields = '';
        $extraValues = '';

        if (!eval(Hooks::get('PIPELINES_ADD_SQL'))) return;

        $sql = sprintf(
            "INSERT INTO candidate_joborder (
                site_id,
                joborder_id,
                candidate_id,
                status,
                added_by,
                date_created,
                date_modified%s
            )
            VALUES (
                %s,
                %s,
                %s,
                0,
                %s,
                NOW(),
                NOW()%s
            )",
            $extraFields,
            $this->_siteID,
            $this->_db->makeQueryInteger($jobOrderID),
            $this->_db->makeQueryInteger($candidateID),
            $this->_db->makeQueryInteger($userID),
            $extraValues
        );

        $queryResult = $this->_db->query($sql);
		// insert candidate to history table as well when added to pipeline ** Nikhil Malhotra **
		$InsertJoborderHistory = "INSERT INTO candidate_joborder_status_history (
					joborder_id,
					candidate_id,
					site_id,
					date,
					status_to,
					status_from
				)
				VALUES (".$jobOrderID.",
					".$candidateID.",
					$this->_siteID,
					NOW(),
					200,
					0
				)";
				mysql_query($InsertJoborderHistory) or die(mysql_error());
		// end Nikhil Malhotra
        if (!$queryResult)
        {
            return false;
        }
		
        return true;
    }

    /**
     * Removes a candidate from the pipeline for a job order.
     *
     * @param integer candidate ID
     * @param integer job order ID
     * @return void
     */
    public function remove($candidateID, $jobOrderID)
    {
        $sql = sprintf(
            "DELETE FROM
                candidate_joborder
            WHERE
                joborder_id = %s
            AND
                candidate_id = %s
            AND
                site_id = %s",
            $this->_db->makeQueryInteger($jobOrderID),
            $this->_db->makeQueryInteger($candidateID),
            $this->_siteID
        );
        $this->_db->query($sql);

        $sql = sprintf(
            "DELETE FROM
                candidate_joborder_status_history
            WHERE
                joborder_id = %s
            AND
                candidate_id = %s
            AND
                site_id = %s",
            $this->_db->makeQueryInteger($jobOrderID),
            $this->_db->makeQueryInteger($candidateID),
            $this->_siteID
        );
        $this->_db->query($sql);

        $history = new History($this->_siteID);
        $history->storeHistoryData(
            DATA_ITEM_CANDIDATE,
            $candidateID,
            'PIPELINE',
            $jobOrderID,
            '(DELETE)',
            '(USER) deleted candidate from pipeline for job order ' . $jobOrderID . '.'
        );
        $history->storeHistoryData(
            DATA_ITEM_JOBORDER,
            $jobOrderID,
            'PIPELINE',
            $candidateID,
            '(DELETE)',
            '(USER) deleted job order from pipeline for candidate ' . $candidateID . '.'
        );
    }

    /**
     * Returns a single pipeline row.
     *
     * @param integer candidate ID
     * @param integer job order ID
     * @return array pipeline data
     */
    public function get($candidateID, $jobOrderID)
    {
        $sql = sprintf(
            "SELECT
                candidate_joborder.candidate_joborder_id as candidateJobOrderID,
                company.company_id AS companyID,
                company.name AS companyName,
                joborder.joborder_id AS jobOrderID,
                joborder.title AS title,
                joborder.type AS type,
                joborder.duration AS duration,
                joborder.rate_max AS maxRate,
                joborder.status AS jobOrderStatus,
                joborder.salary AS salary,
                joborder.is_hot AS isHot,
                joborder.openings AS openings,
                joborder.openings_available AS openingsAvailable,
                DATE_FORMAT(
                    joborder.start_date, '%%m-%%d-%%y'
                ) AS start_date,
                DATE_FORMAT(
                    joborder.date_created, '%%m-%%d-%%y'
                ) AS dateCreated,
                candidate.candidate_id AS candidateID,
                candidate.email1 AS candidateEmail,
                candidate_joborder_status.candidate_joborder_status_id AS statusID,
                candidate_joborder_status.short_description AS status,
                owner_user.first_name AS ownerFirstName,
                owner_user.last_name AS ownerLastName
            FROM
                candidate_joborder
            LEFT JOIN candidate
                ON candidate_joborder.candidate_id = candidate.candidate_id
            LEFT JOIN joborder
                ON candidate_joborder.joborder_id = joborder.joborder_id
            LEFT JOIN company
                ON company.company_id = joborder.company_id
            LEFT JOIN user AS owner_user
                ON joborder.owner = owner_user.user_id
            LEFT JOIN candidate_joborder_status
                ON candidate_joborder.status = candidate_joborder_status.candidate_joborder_status_id
            WHERE
                candidate.candidate_id = %s
            AND
                joborder.joborder_id = %s
            AND
                candidate.site_id = %s
            AND
                joborder.site_id = %s
            AND
                company.site_id = %s",
            $this->_db->makeQueryInteger($candidateID),
            $this->_db->makeQueryInteger($jobOrderID),
            $this->_siteID,
            $this->_siteID,
            $this->_siteID
        );

        return $this->_db->getAssoc($sql);
    }

    /**
     * Returns a pipeline entry's candidate-joborder ID from the specified
     * candidate ID and job order ID; -1 if not found.
     *
     * @param integer candidate ID
     * @param integer job order ID
     * @return integer candidate-joborder ID or -1 if not found
     */
    public function getCandidateJobOrderID($candidateID, $jobOrderID)
    {
        $sql = sprintf(
            "SELECT
                candidate_joborder_id AS candidateJobOrderID
            FROM
                candidate_joborder
            WHERE
                candidate_id = %s
            AND
                joborder_id = %s
            AND
                site_id = %s",
            $this->_db->makeQueryInteger($candidateID),
            $this->_db->makeQueryInteger($jobOrderID),
            $this->_siteID
        );
        $rs = $this->_db->getAssoc($sql);

        if (empty($rs))
        {
            return -1;
        }

        return (int) $rs['candidateJobOrderID'];
    }

    // FIXME: Document me.
    public function setStatus($candidateID, $jobOrderID, $statusID,
                              $emailAddress, $emailText,$statusAttachmentPath , $statusAttachmentName)
    {
        /* Get existing status. */
        $sql = sprintf(
            "SELECT
                status AS oldStatusID,
                candidate_joborder_id AS candidateJobOrderID
            FROM
                candidate_joborder
            WHERE
                joborder_id = %s
            AND
                candidate_id = %s
            AND
                site_id = %s",
            $this->_db->makeQueryInteger($jobOrderID),
            $this->_db->makeQueryInteger($candidateID),
            $this->_siteID
        );
        $rs = $this->_db->getAssoc($sql);

        if (empty($rs))
        {
            return;
        }
        
        $candidateJobOrderID = $rs['candidateJobOrderID'];
        $oldStatusID         = $rs['oldStatusID'];
		
		

        if ($oldStatusID == $statusID)
        {
            /* No need to update the database and scew the history if there is
             * no actual change.
             */
            return;
        }
        
        if ($oldStatusID > $statusID)
        {
            $sqlstatushistory = sprintf(
                "DELETE
                FROM
                    candidate_joborder_status_history
                WHERE
                    status_from > %s
                AND
                    candidate_id = %s
                AND 
                    joborder_id = %s
                AND
                    site_id = %s",
                $this->_db->makeQueryInteger($statusID),
                $this->_db->makeQueryInteger($candidateID),
                $this->_db->makeQueryInteger($jobOrderID),
                $this->_siteID
            );
            $this->_db->query($sqlstatushistory);
        }
		
        /* Change status. */
        $sql = sprintf(
            "UPDATE
                candidate_joborder
            SET
                status        = %s,
                date_modified = NOW()
            WHERE
                candidate_joborder_id = %s
            AND
                site_id = %s",
            $this->_db->makeQueryInteger($statusID),
            $this->_db->makeQueryInteger($candidateJobOrderID),
            $this->_siteID
        );
        $this->_db->query($sql);
		
		
        /* Add history. 
		$GetNextIdQuery=mysql_query("select candidate_joborder_status_id as NextId from 
				   candidate_joborder_status cjs,candidate_joborder cj 
				   join joborder j on cj.joborder_id=j.joborder_id
					where cj.joborder_id=157 and cj.candidate_id=40 and cjs.candidate_joborder_status_id>cj.status limit 0,1");
		$GetNextId=mysql_fetch_array($GetNextIdQuery);
		$NextId=$GetNextId["NextId"];
        // $historyID = $this->addStatusHistory($candidateID, $jobOrderID, $, );
		*/
		
		
		/* (New Code). */
		//$stagesArray = array(0,200,250,300,400,500,520,540,560,580,600,700,750,800);
		
		$stagesArray = "";
		$sqlstages = sprintf("select candidate_joborder_status_id from candidate_joborder_status order by candidate_joborder_status_id");
		$resourcestages =$this->_db->query($sqlstages);
		while($rowstages = mysql_fetch_array($resourcestages))
		{
			$stagesArray[] = $rowstages['candidate_joborder_status_id'];
		}
		$keyStartStage = array_search($oldStatusID, $stagesArray);
		$keyEndStage = array_search($statusID, $stagesArray);
		for($i=$keyStartStage;$i<$keyEndStage;$i++)
		{
				$j=$i+1;
				/* Add history */
				$historyID = $this->addStatusHistory(
					$candidateID, $jobOrderID, $stagesArray[$j+1], $stagesArray[$j]
				);
			 
				/* Code changed to make an entry for placed candidate
				Changed on 08.08.2013 by Charanjit Singh.*/
				$s1=mysql_query("select * from joborder where joborder_id=".$jobOrderID);
				$fs1=mysql_fetch_array($s1);
				for($countofstages=0;$countofstages<=13;$countofstages++)
				{
					if(strlen($fs1["stage".$countofstages])>0)
					{
						$arrayfilledstages[] = $fs1["stage".$countofstages];
					}
				}
				$sqlstages = sprintf("select candidate_joborder_status_id from candidate_joborder_status where candidate_joborder_status_id<=(select candidate_joborder_status_id from candidate_joborder_status where short_description='stage".count($arrayfilledstages)."')order by candidate_joborder_status_id");
				$resourcestages =$this->_db->query($sqlstages);
				while($rowstages = mysql_fetch_array($resourcestages))
				{
					$stagesArray[] = $rowstages['candidate_joborder_status_id'];
				}
				if($stagesArray[$j] == $stagesArray[count($arrayfilledstages)])
				{
					$historyID = $this->addStatusHistory(
						$candidateID, $jobOrderID, $stagesArray[count($arrayfilledstages)], $stagesArray[count($arrayfilledstages)]
					);
				}
		}
		
        /* Add auditing history. */
        $historyDescription = '(USER) changed pipeline status of candidate '
            . $candidateID . ' for job order ' . $jobOrderID . '.';
        $history = new History($this->_siteID);
        $history->storeHistoryData(
            DATA_ITEM_PIPELINE,
            $candidateJobOrderID,
            'PIPELINE',
            $oldStatusID,
            $statusID,
            $historyDescription
        );
		$userid=$_SESSION['RESUFLO']->getUserID();
		if (!empty($emailAddress))
                {
                    $emailReceiver = $emailAddress;
                    $arrmail = explode(",", $emailReceiver);
                    $subject = CANDIDATE_STATUSCHANGE_SUBJECT;
                    $msg = nl2br($emailText);
                    $body = eregi_replace("[\]",'',$msg);

                    //Need to fix this
//                        if($statusAttachmentName!=''){
//                            $mail->AddAttachment($statusAttachmentPath,$statusAttachmentName);
//                        }

                    try{
                        file_put_contents('C:/Windows/Temp/ResufloLog/UserMailerCaller.txt', PHP_EOL.(new DateTime())->format('d m Y H:i:s').'===== START pipeline', FILE_APPEND);
                        $email = new UserMailer($userid);
                        $result = $email->SendEmail($arrmail, null, $subject, $body);
                        file_put_contents('C:/Windows/Temp/ResufloLog/UserMailerCaller.txt', '=> END ====='.$result, FILE_APPEND);
                    }
                    catch (Exception $e) {
                        file_put_contents('C:/Windows/Temp/ResufloLog/UserMailerCaller.txt', (new DateTime())->format('d m Y H:i:s').'pipeline ====='.$e->getMessage().PHP_EOL, FILE_APPEND);
                    }
                }
        /*if (!empty($emailAddress))
        {
            // Send e-mail notification. 
            //FIXME: Make subject configurable.
            $mailer = new Mailer($this->_siteID);
            $mailerStatus = $mailer->sendToOne(
                array($emailAddress, ''),
                CANDIDATE_STATUSCHANGE_SUBJECT,
                $emailText,
                true
            );
        }*/
    }

    // FIXME: Document me.
    public function getStatuses()
    {
        $sql = sprintf(
            "SELECT
                candidate_joborder_status_id AS statusID,
                short_description AS status,
                can_be_scheduled AS canBeScheduled,
                triggers_email AS triggersEmail
            FROM
                candidate_joborder_status
            WHERE
                is_enabled = 1
            ORDER BY
                candidate_joborder_status_id ASC",
            $this->_siteID
        );

        return $this->_db->getAllAssoc($sql);
    }

    // FIXME: Document me.
    // Throws out No Status.
    public function getStatusesForPicking2($selectedJobOrderID)
    {
        $sql = sprintf(
            "SELECT
                candidate_joborder_status_id AS statusID,
                short_description AS status,
                can_be_scheduled AS canBeScheduled,
                triggers_email AS triggersEmail
            FROM
                candidate_joborder_status
            WHERE
                is_enabled = 1
            AND
                candidate_joborder_status_id != 0
            ORDER BY
                candidate_joborder_status_id ASC",
            $this->_siteID
        );
        $result=$this->_db->getAllAssoc($sql);
	
			for($i=0;$i<13;$i++)
			{
				$stat[]=$result[$i]['status'];
			}
				$sql2 = sprintf(
				"SELECT 
				%s,
				%s,
				%s,
				%s,
				%s,
				%s,
				%s,
				%s,
				%s,
				%s,
				%s,
				%s,
				%s
				FROM joborder where joborder.joborder_id=%s",
				$stat[0],
				$stat[1],
				$stat[2],
				$stat[3],
				$stat[4],
				$stat[5],
				$stat[6],
				$stat[7],
				$stat[8],
				$stat[9],
				$stat[10],
				$stat[11],
				$stat[12],
				$selectedJobOrderID
			);
				$result2=$this->_db->getAllAssoc($sql2);
				
						$data[0]['status']=$result2[0]['stage1'];
						$data[1]['status']=$result2[0]['stage2'];
						$data[2]['status']=$result2[0]['stage3'];
						$data[3]['status']=$result2[0]['stage4'];
						$data[4]['status']=$result2[0]['stage5'];
						$data[5]['status']=$result2[0]['stage6'];
						$data[6]['status']=$result2[0]['stage7'];
						$data[7]['status']=$result2[0]['stage8'];
						$data[8]['status']=$result2[0]['stage9'];
						$data[9]['status']=$result2[0]['stage10'];
						$data[10]['status']=$result2[0]['stage11'];
						$data[11]['status']=$result2[0]['stage12'];
						$data[12]['status']=$result2[0]['stage13'];
						for($i=0;$i<13;$i++)
						{
							if(!empty($data[$i]['status']))
							{
								$result3[$i]['statusID']=$result[$i]['statusID'];
								$result3[$i]['status']=$data[$i]['status'];
								$result3[$i]['canBeScheduled']=$result[$i]['canBeScheduled'];
								$result3[$i]['triggersEmail']=$result[$i]['triggersEmail'];	
							}
						}
						return $result3;

    }
	public function getStatusesForPicking()
    {
        $sql = sprintf(
            "SELECT
                candidate_joborder_status_id AS statusID,
                short_description AS status,
                can_be_scheduled AS canBeScheduled,
                triggers_email AS triggersEmail
            FROM
                candidate_joborder_status
            WHERE
                is_enabled = 1
            AND
                candidate_joborder_status_id != 0
            ORDER BY
                candidate_joborder_status_id ASC",
            $this->_siteID
        );
		return $result=$this->_db->getAllAssoc($sql);
	}

    // FIXME: Document me.
    public function addStatusHistory($candidateID, $jobOrderID, $statusToID,
                                     $statusFromID)
    {
		 $selJobOrderHistory = "select * from candidate_joborder_status_history where joborder_id = ".$jobOrderID." and candidate_id = ".$candidateID." and status_to=".$statusToID;
			
			$resJobOrderHistory = mysql_query($selJobOrderHistory) or die(mysql_error());
			$numJobOrderHistory = mysql_num_rows($resJobOrderHistory);
			if($numJobOrderHistory <= 0)
			{
			
				$sql = sprintf(
					"INSERT INTO candidate_joborder_status_history (
						joborder_id,
						candidate_id,
						site_id,
						date,
						status_to,
						status_from
					)
					VALUES (
						%s,
						%s,
						%s,
						NOW(),
						%s,
						%s
					)",
					$this->_db->makeQueryInteger($jobOrderID),
					$this->_db->makeQueryInteger($candidateID),
					$this->_siteID,
					$this->_db->makeQueryInteger($statusToID),
					$this->_db->makeQueryInteger($statusFromID)
				);

				$queryResult = $this->_db->query($sql);
				if (!$queryResult)
				{
					return -1;
				}
				
				return $this->_db->getLastInsertID();
			}
			
    }

    /**
     * Returns a candidate's pipeline.
     *
     * @param integer candidate ID
     * @return array pipeline data
     */
    //  jj says this is in place on this link: http://staging.resuflocrm.com/index.php?m=joborders&a=show&jobOrderID=171
    // but only when the little window is launched by clicking action 
    public function getCandidatePipeline($candidateID)
    {
        $sql = sprintf(
            "SELECT
                company.company_id AS companyID,
                company.name AS companyName,
                joborder.joborder_id AS jobOrderID,
                joborder.title AS title,
                joborder.type AS type,
                joborder.duration AS duration,
                joborder.rate_max AS maxRate,
                joborder.status AS jobOrderStatus,
                joborder.salary AS salary,
                joborder.is_hot AS isHot,
                DATE_FORMAT(
                    joborder.start_date, '%%m-%%d-%%y'
                ) AS start_date,
                DATE_FORMAT(
                    joborder.date_created, '%%m-%%d-%%y'
                ) AS dateCreated,
                candidate.candidate_id AS candidateID,
                candidate.email1 AS candidateEmail,
                candidate_joborder_status.candidate_joborder_status_id AS statusID,
                candidate_joborder_status.short_description AS status,
                candidate_joborder.candidate_joborder_id AS candidateJobOrderID,
                candidate_joborder.rating_value AS ratingValue,
                owner_user.first_name AS ownerFirstName,
                owner_user.last_name AS ownerLastName,
                added_user.first_name AS addedByFirstName,
                added_user.last_name AS addedByLastName
            FROM
                candidate_joborder
            INNER JOIN candidate
                ON candidate_joborder.candidate_id = candidate.candidate_id
            INNER JOIN joborder
                ON candidate_joborder.joborder_id = joborder.joborder_id
            INNER JOIN company
                ON company.company_id = joborder.company_id
            LEFT JOIN user AS owner_user
                ON joborder.owner = owner_user.user_id
            LEFT JOIN user AS added_user
                ON candidate_joborder.added_by = added_user.user_id
            INNER JOIN candidate_joborder_status
                ON candidate_joborder.status = candidate_joborder_status.candidate_joborder_status_id
            WHERE
                candidate.candidate_id = %s
            AND
                candidate.site_id = %s
            AND
                joborder.site_id = %s
            AND
                company.site_id = %s",
            $this->_db->makeQueryInteger($candidateID),
            $this->_siteID,
            $this->_siteID,
            $this->_siteID
        );

$a = $this->_db->getAllAssoc($sql);
//die(__FILE__.' line '.__LINE__.'<pre>'.print_r($a,true));
        return $a;
    }

    /**
     * Returns a job order's pipeline.
     *
     * @param integer job order ID
     * @return array pipeline data
     */
    //  jj says this is in place on this link: http://staging.resuflocrm.com/index.php?m=joborders&a=show&jobOrderID=171
    // also called by this: http://staging.resuflocrm.com/ajax.php from within the same above
    public function getJobOrderPipeline($jobOrderID, $orderBy = '')
    {
        /* FIXME: CONCAT() stuff is a very ugly hack, but I don't think there
         * is a way to return multiple values from a subquery.
         */
        
         $userid=$_SESSION['RESUFLO']->getUserID();
         /*
         if($userid==1)
         {
            $condset="  AND candidate.unsubscribe ='0'";
            
         }
         else{
            
            $condset="and candidate_joborder.added_by='".$userid."'   AND candidate.unsubscribe ='0'";
            
         }
		 */
		 if($_SESSION['RESUFLO']->getAccessLevel() == "500"){
			// $condset = " AND candidate.unsubscribe ='0'";
			$condset = "";
		}
		else if($_SESSION['RESUFLO']->getClientLogin()==1 && $_SESSION['RESUFLO']->getAccessLevel()==400){
        	/* $condset = " 
			AND (candidate_joborder.added_by IN (SELECT user_id FROM user where entered_by='".$_SESSION['RESUFLO']->getUserID()."') OR candidate_joborder.added_by ='".$_SESSION['RESUFLO']->getuserid()."') 
			AND candidate.unsubscribe ='0'"; */
			$condset = " 
			AND (candidate_joborder.added_by IN (SELECT user_id FROM user where entered_by='".$_SESSION['RESUFLO']->getUserID()."') OR candidate_joborder.added_by ='".$_SESSION['RESUFLO']->getuserid()."')";
		}
		 else {
		    // $condset = " AND candidate_joborder.added_by = '".$_SESSION['RESUFLO']->getuserid()."'  AND candidate.unsubscribe ='0'";
			$condset = " AND candidate_joborder.added_by = '".$_SESSION['RESUFLO']->getuserid()."'";
		}
       
        
        $deadtxt = (empty($this->dead)) ? ' AND candidate_joborder.dead = 0 ' : '';
        $futuretxt = (empty($this->future)) ? ' AND candidate_joborder.future = 0 ' : '';
        $dead_future = $deadtxt.$futuretxt;

        $sql = sprintf(
            "SELECT
                IF(attachment_id, 1, 0) AS attachmentPresent,
                candidate.candidate_id AS candidateID,
                candidate.first_name AS firstName,
                candidate.last_name AS lastName,
                candidate.state As state,
                candidate.email1 AS candidateEmail,
                candidate_joborder.status AS jobOrderStatus,
                DATE_FORMAT(
                    candidate_joborder.date_created, '%%m-%%d-%%y'
                ) AS dateCreated,
                UNIX_TIMESTAMP(candidate_joborder.date_created) AS dateCreatedInt,
                candidate_joborder_status.short_description AS status,
                candidate_joborder.candidate_joborder_id AS candidateJobOrderID,
                candidate_joborder.rating_value AS ratingValue,
                owner_user.first_name AS ownerFirstName,
                owner_user.last_name AS ownerLastName,
                (
                    SELECT
                        CONCAT(
                            '<strong>',
                            DATE_FORMAT(activity.date_created, '%%m-%%d-%%y'),
                            ' (',
                            entered_by_user.first_name,
                            ' ',
                            entered_by_user.last_name,
                            '):</strong> ',
                            IF(
                                ISNULL(activity.notes) OR activity.notes = '',
                                '(No Notes)',
                                activity.notes
                            )
                        )
                    FROM
                        activity
                    LEFT JOIN activity_type
                        ON activity.type = activity_type.activity_type_id
                    LEFT JOIN user AS entered_by_user
                        ON activity.entered_by = entered_by_user.user_id
                    WHERE
                        activity.data_item_id = candidate.candidate_id
                    AND
                        activity.joborder_id = %s
                    ORDER BY
                        activity.date_created DESC
                    LIMIT 1
                ) AS lastActivity,
                IF((
                    SELECT
                        COUNT(*)
                    FROM
                        candidate_joborder_status_history
                    WHERE
                        joborder_id = %s
                    AND
                        candidate_id = candidate.candidate_id
                    AND
                        status_to = %s
                    AND
                        site_id = %s
                ) > 1, 1, 0) AS submitted,
                added_user.first_name AS addedByFirstName,
                added_user.last_name AS addedByLastName
            FROM
                candidate_joborder
            LEFT JOIN candidate
                ON candidate_joborder.candidate_id = candidate.candidate_id
            LEFT JOIN user AS owner_user
                ON candidate.owner = owner_user.user_id
            LEFT JOIN user AS added_user
                ON candidate_joborder.added_by = added_user.user_id
            LEFT JOIN attachment
                ON candidate.candidate_id = attachment.data_item_id
            LEFT JOIN candidate_joborder_status
                ON candidate_joborder.status = candidate_joborder_status.candidate_joborder_status_id
            WHERE
                candidate_joborder.joborder_id = %s
                ".$condset."
            AND
                candidate_joborder.site_id = %s
                ".$dead_future."
            AND
                candidate.site_id = %s
            GROUP BY
                candidate_joborder.candidate_id
            %s",
            $this->_db->makeQueryInteger($jobOrderID),
            $this->_db->makeQueryInteger($jobOrderID),
            PIPELINE_STATUS_SUBMITTED,
            $this->_siteID,
            $this->_db->makeQueryInteger($jobOrderID),
            $this->_siteID,
            $this->_siteID,
            $orderBy
        );
//echo $sql;
$a = $this->_db->getAllAssoc($sql);
//die(__FILE__.' line '.__LINE__.'<pre>'.print_r($a,true));
        return $a;
    }
	 public function getJobOrderStatusName($jobOrderID,$stage)
    {
		if(!empty($stage))
		{
			if($stage!=="In Pipeline")
			{
			 $sql = sprintf(
				"SELECT %s from joborder where joborder_id=%s",$stage,$jobOrderID);
				 $status=$this->_db->getAllAssoc($sql);
				foreach ($status as $rowIndex => $row)
				{
					 foreach ($row as $row2 =>$rowData)
						{
							$status=$rowData;
							
						}
				
				}
				return $status;
			}	
			else
			{
				return "In Pipeline";
			}
		}
		else
		{
			return "";
		}
			
	}

    // FIXME: Document me.
    public function updateRatingValue($candidateJobOrderID, $value)
    {
        $sql = sprintf(
            "UPDATE
                candidate_joborder
            SET
                rating_value = %s
            WHERE
                candidate_joborder.candidate_joborder_id = %s
            AND
                candidate_joborder.site_id = %s",
            $this->_db->makeQueryInteger($value),
            $this->_db->makeQueryInteger($candidateJobOrderID),
            $this->_siteID
        );

        $queryResult = $this->_db->query($sql);
        if (!$queryResult)
        {
            return -1;
        }
    }

    // FIXME: Document me.
    public function getRatingValue($candidateJobOrderID)
    {
        $sql = sprintf(
            "SELECT
                rating_value AS ratingValue
            FROM
                candidate_joborder
            WHERE
                candidate_joborder_id = %s
            AND
                site_id = %s",
            $this->_db->makeQueryInteger($candidateJobOrderID),
            $this->_siteID
        );
        $rs = $this->_db->getAssoc($sql);

        if (!isset($rs['ratingValue']) || empty($rs['ratingValue']))
        {
            return 0;
        }

        return $rs['ratingValue'];
    }

    //FIXME: Document me.
    public function getPipelineDetails($candidateJobOrderID)
    {
        $sql = sprintf(
            "SELECT
                candidate.first_name AS firstName,
                candidate.last_name AS lastName,
                candidate.email1 AS candidateEmail,
                candidate_joborder.status AS jobOrderStatus,
                activity.notes AS notes,
                DATE_FORMAT(
                    candidate_joborder.date_created, '%%m-%%d-%%y'
                ) AS dateCreated,
                candidate_joborder.candidate_joborder_id AS candidateJobOrderID,
                candidate_joborder.rating_value AS ratingValue,
                entered_by_user.first_name AS enteredByFirstName,
                entered_by_user.last_name AS enteredByLastName,
                DATE_FORMAT(activity.date_modified, '%%m-%%d-%%y (%%h:%%i:%%s %%p)') AS dateModified
            FROM
                candidate_joborder
            LEFT JOIN candidate
                ON candidate_joborder.candidate_id = candidate.candidate_id
            INNER JOIN activity
                ON activity.joborder_id = candidate_joborder.joborder_id
            LEFT JOIN user AS entered_by_user
                ON entered_by_user.user_id = activity.entered_by
            WHERE
                candidate_joborder.candidate_joborder_id = %s
            AND
                activity.data_item_type = %s
            AND
                activity.data_item_id = candidate_joborder.candidate_id
            AND
                candidate_joborder.site_id = %s
            ",
            $this->_db->makeQueryInteger($candidateJobOrderID),
            DATA_ITEM_CANDIDATE,
            $this->_siteID
        );

        return $this->_db->getAllAssoc($sql);
    }

}

?>
