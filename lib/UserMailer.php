<?php

/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */

//namespace UserMailer;

/**
 * Description of UserMailer
 *
 * @author Soft Logics 1
 */
include_once('./asset/config.php');
include_once('./asset/constants.php');
include_once('./lib/Mailer.php');
include_once('./lib/Users.php');
include_once('./lib/SmsLog.php');
include_once('./Setting.php');
include_once('./../Setting.php');

use PHPMailer\PHPMailer\PHPMailer;
include_once('./lib/phpmailer6/vendor/autoload.php');

include_once('./que/twilio-php-master/Twilio/autoload.php');
use Twilio\Rest\Client;


class UserMailer {
    //put your code here
    private $_userID;
    private $_siteID;
    private $_db;
    public function __construct($userId, $SiteId = 1)
    {
        $this->_userID = $userId;
        $this->_siteID = $SiteId;
        $this->_db = DatabaseConnection::getInstance($SiteId);
    }
    public  function SendEmail($toArray, $bccArray, $subject, $message, $attachment = null){
        sleep(1);
        $Retry = false;
        Retry:
        $query="select * from dripmarket_configuration where userid=".$this->_userID;
        $resourcedetail=mysql_query($query);
        $row = mysql_fetch_array($resourcedetail);
        
        $mail = new PHPMailer();
        $mail->Subject = $subject;
        $mail->MsgHTML($message);
        if($attachment != null){
            $mail->AddAttachment($attachment);
        }
        if($bccArray != null){
            foreach ($bccArray as &$bcc) {
                $mail->AddBcc(trim($bcc, " "));
            }
        }
        foreach ($toArray as &$to) {
            $mail->AddAddress(trim($to, " "));
        }
        $IsGoogleOAuth = false;
        $IsSendGrid = false;
        $AccessToken = "";
        $RefreshToken = "";
        
        if($row['toggle_smtp'] == 1)
        {
            if (!empty($row['bccemail2'])){ 
                $mail->AddBcc($row['bccemail2']);
            }
            if($row['sendgrid2'] == 1){
                $IsSendGrid = true;
                $mail->SetFrom($row['fromemail2'], $row['fromname2']);
            }
            else if(!empty($row['google_email2'])){
                $IsGoogleOAuth = true;
                $AccessToken = $row['google_access_token2'];
                $mail->SetFrom($row['google_email2'], $row['google_name2']);
            }
            else{
                $mail->Host       = $row['smtphost2'];
                $mail->Port       =  $row['smtpport2'];
                $mail->Username   =  $row['smtpuser2'];
                $mail->Password   = $row['smtppassword2'];
                $mail->SetFrom($row['fromemail2'], $row['fromname2']);
                if($row['email_type2']!='none') {
                   $mail->SMTPSecure =$row['email_type2'];
                }
            }
        }
        else
        {
            if (!empty($row['bccemail'])){
                $mail->AddBcc($row['bccemail']);
            }
            if($row['sendgrid'] == 1){
                $IsSendGrid = true;
                $mail->SetFrom($row['fromemail'], $row['fromname']);
            }
            else if(!empty($row['google_email'])){
                $IsGoogleOAuth = true;
                $AccessToken = $row['google_access_token'];
                $RefreshToken = $row['google_refresh_token'];
                $mail->SetFrom($row['google_email'], $row['google_name']);
            }
            else{
                $mail->Host       = $row['smtphost'];
                $mail->Port       =  $row['smtpport'];    
                $mail->Username   =  $row['smtpuser'];
                $mail->Password   = $row['smtppassword'];
                $mail->SetFrom($row['fromemail'], $row['fromname']);
                if($row['email_type']!='none') {
                    $mail->SMTPSecure =$row['email_type']; 
                }
            }
        }
        if($IsGoogleOAuth == true){
            $mail->preSend();
            $mime = $mail->getSentMIMEMessage();
            $base64String = base64_encode($mime);
            //$base64String = str_replace(array('+','/','=') , array('-','_','') , $base64String);
            
            require_once(__DIR__.'/../GoogleCalAPI/GoogleAPIClient.php');
            require_once(__DIR__.'/../GoogleCalAPI/vendor/autoload.php');
            try{
                $googleAPI = new GoogleAPIClient();
                $googleAPI->GetGmailClient($AccessToken);
                $result = $googleAPI->sendMessage($base64String);
                return $result; 
            }
            catch (Exception $e) {
                return  $e->getMessage();
            }
        }
        else{
            $mail->IsSMTP();
            $mail->SMTPAuth   = true;
            if($mail->Host == 'smtp.sendgrid.net' ){
                $mail->SMTPSecure = 'ssl';
            }
            else{
                $mail->SMTPOptions = array(
                    'ssl' => array(
                        'verify_peer' => false,
                        'verify_peer_name' => false,
                        'allow_self_signed' => true
                    )
                );
            }
            if($IsSendGrid == 1 || $IsSendGrid == true || $mail->Host == 'smtp.sendgrid.net' || $mail->Host == 'sendgrid' )
            {
                require_once __DIR__ . './../sendgrid/sendgrid-php.php';
                $sgemail = new \SendGrid\Mail\Mail(); 
                $sgemail->setFrom($mail->From, $mail->FromName);
                $sgemail->setSubject($mail->Subject);

                //$bccArray[] = "sflogicstest@gmail.com";
                //$recipients[] = 'malikabiid@gmail.com';
                //$recipients[] = 'malikabiid@yahoo.com';
                //$toArray[] = $firstTestEmail;
                $emails = array();
                foreach ($toArray as $EmailAddress){
                    $emails = array_merge($emails, array(trim($EmailAddress, " ") => ""));    
                }
                $sgemail->addTos($emails);
                
                if($bccArray != null)
                {
                    $bccemails = array();
                    foreach ($bccArray as $EmailAddress){
                        $bccemails = array_merge($bccemails, array(trim($EmailAddress, " ") => ""));    
                    }
                    //$sgemail->addTo("malikabiid@gmail.com", "Example User");
                    //$sgemail->addBcc("malikabiid@yahoo.com", "Example User");
                    $sgemail->addBccs($bccemails);
                }
                if($attachment != null)
                {
                    $Attachment = new SendGrid\Mail\Attachment();
                    $fileContent = file_get_contents($attachment);
                    $Attachment->setContent(base64_encode($fileContent));
                    $Attachment->setFilename(basename($attachment));
                    $sgemail->addAttachment($Attachment);
                }
                //$sgemail->addContent("text/plain", "and easy to do anywhere, even with PHP");
                $sgemail->addContent(
                    "text/html", $message
                );
                $sendgrid = new \SendGrid(\Configuration\Setting::SendGridAPI);
                try {
                    $response = $sendgrid->send($sgemail);
                    $result = $response->statusCode() == true;
                    //print $response->statusCode() . "\n";
                    //print $response->body() . "\n";
                } catch (Exception $e) {
                    //echo 'Caught exception: '. $e->getMessage() ."\n";
                }
            }
            else
            {
                $result = $mail->Send();
            }
            if($result){
                return true;
            }
            else{
                $query = 'update `dripmarket_configuration` set toggle_smtp='.(($row['toggle_smtp'] == 1)?0:1).' where userid = '.$this->_userID;
                mysql_query($query);
            }
        }
        if($Retry == false){
            $Retry = true;
            goto Retry;
        }
        if($IsGoogleOAuth == true){
            if(count($responseJson) == 2 && isset($responseJson['error']) && isset($responseJson['error_description'])){
                return $responseJson['error'].'=>'.$responseJson['error_description'];
            }
            else{
                return $responseJson['error']['code'].'=>'.$responseJson['error']['message'];
            }
        }
        else{
            return $mail->ErrorInfo;
        }
    }
    
    public  function SendQueueEmail($to, $subject, $body){
        // TODO: put a constaint on the table, for now...if there is an 
        // application logic error, use the most recent config (order by id desc)
        $query="select * from dripmarket_configuration where userid=".$this->_userID;
        $resourcedetail=mysql_query($query);

        $row = mysql_fetch_array($resourcedetail);
        $mail= new PHPMailer();
        $mail->IsSMTP(); // telling the class to use SMTP
        $mail->SMTPAuth   = true;                  // enable SMTP authentication
        $mail->Host       = $row['smtphost'];
        $mail->Port       =  $row['smtpport'];    
        $mail->Username   =  $row['smtpuser'];   // SMTP account username
        $mail->Password   = $row['smtppassword'];

        if($row['email_type']!='none') {
            //$mail->SMTPSecure = 'ssl';  
            $mail->SMTPSecure =$row['email_type']; 
        }

        $mail->SetFrom( $row['fromemail'], $row['fromname']);    
        $mail->Subject    = $subject;

        $mail->MsgHTML($body);

        $mail->AddAddress($to,'');

        try
        {
            // use just in case of ssl issue
            $mail->SMTPOptions = array(
                'ssl' => array(
                    'verify_peer' => false,
                    'verify_peer_name' => false,
                    'allow_self_signed' => true
                )
            );
            $result = $mail->Send();
            if($result){
                return 'Success';
            }
            else{
                return $mail->ErrorInfo;
            }
        }
        catch (Exception $e) {
            return  $e->getMessage();
        }
    }
    
    public  function TestSMTP($sendgrid,$smtpHost,$smtpUser,$smtpPass,$smtpPort,$fromName,$fromEmail,$bccEmail,$emailType,$firstTestEmail){
        // TODO: put a constaint on the table, for now...if there is an 
        // application logic error, use the most recent config (order by id desc)
        $mail= new PHPMailer();
        $mail->IsSMTP(); // telling the class to use SMTP
        $mail->SMTPAuth   = true;                  // enable SMTP authentication
        $mail->Host       = $smtpHost;
        $mail->Port       =  $smtpPort;
        $mail->Username   =  $smtpUser;   // SMTP account username
        $mail->Password   = $smtpPass;

        if($row['email_type']!='none') {
            $mail->SMTPSecure = $emailType; 
        }

        $mail->SetFrom( $fromEmail, $fromName);    
        $mail->Subject    = "Resuflo SMTP Test";

        $mail->MsgHTML("SMTP test passed");

        $mail->AddAddress($firstTestEmail,'');
        try
        {
            // use just in case of ssl issue
            $mail->SMTPOptions = array(
                'ssl' => array(
                    'verify_peer' => false,
                    'verify_peer_name' => false,
                    'allow_self_signed' => true
                )
            );
            if($sendgrid == 1 || $sendgrid == true || $mail->Host == 'smtp.sendgrid.net' || $mail->Host == 'sendgrid' )
            {
                require_once __DIR__ . '/../sendgrid/sendgrid-php.php';
                $sgemail = new \SendGrid\Mail\Mail(); 
                $sgemail->setFrom($mail->From, $mail->FromName);
                $sgemail->setSubject($mail->Subject);

                $bcc[] = "sflogicstest@gmail.com";
                //$recipients[] = 'malikabiid@gmail.com';
                //$recipients[] = 'malikabiid@yahoo.com';
                $recipients[] = $firstTestEmail;
                $emails = array();
                foreach ($recipients as $EmailAddress){
                    $emails = array_merge($emails, array($EmailAddress => ""));    
                }
                $bccemails = array();
                foreach ($bcc as $EmailAddress){
                    $bccemails = array_merge($bccemails, array($EmailAddress => ""));    
                }

                $sgemail->addTos($emails);
                //$sgemail->addTo("malikabiid@gmail.com", "Example User");
                //$sgemail->addBcc("malikabiid@yahoo.com", "Example User");
                $sgemail->addBccs($bccemails);
                //$attachment = new SendGrid\Mail\Attachment();
                //$fileContent = file_get_contents("C:\Users\App Logics\Downloads\DELETING USERS.pdf");
                //$attachment->setContent(base64_encode($fileContent));
                //$attachment->setFilename("DELETING USERS.pdf");
                //$sgemail->addAttachment($attachment);
                //$sgemail->addContent("text/plain", "and easy to do anywhere, even with PHP");
                $sgemail->addContent(
                    "text/html", "SMTP test passed"
                );
                $sendgrid = new \SendGrid(\Configuration\Setting::SendGridAPI);
                try {
                    $response = $sendgrid->send($sgemail);
                    $result = $response->statusCode() == true;
                    //print $response->statusCode() . "\n";
                    //print $response->body() . "\n";
                } catch (Exception $e) {
                    //echo 'Caught exception: '. $e->getMessage() ."\n";
                }
            }
            else
            {
                $result = $mail->Send();
            }
            if($result){
                return 'Success';
            }
            else{
                return $mail->ErrorInfo;
            }
        }
        catch (Exception $e) {
            return  $e->getMessage();
        }
    }
    
    public  function SendWithSMTP($smtpHost,$smtpUser,$smtpPass,$smtpPort,$fromName,$fromEmail,$bccEmail,$encryptType,$subject,$body,$to,$attachDoc,$attachDocNew){
        // TODO: put a constaint on the table, for now...if there is an 
        // application logic error, use the most recent config (order by id desc)
        $mail= new PHPMailer();
        $mail->IsSMTP(); // telling the class to use SMTP
        $mail->SMTPAuth   = true;                  // enable SMTP authentication
        $mail->Host       = $smtpHost;
        $mail->Port       =  $smtpPort;
        $mail->Username   =  $smtpUser;   // SMTP account username
        $mail->Password   = $smtpPass;

        if($encryptType!='none') {
            $mail->SMTPSecure = $encryptType; 
        }

        $mail->SetFrom( $fromEmail, $fromName);    
        $mail->Subject    = $subject;

        $mail->MsgHTML($body);
        if(!empty($attachDoc) && !empty($attachDocNew)){
            $mail->AddAttachment($attachDoc, $attachDocNew);
        }

        $addresses = explode(';', $to);
        $mail->AddAddress($addresses[0], '');
        if(count($addresses) > 1)
        {
            $mail->AddAddress($addresses[1], '');
        }
        
        try
        {
            // use just in case of ssl issue
            $mail->SMTPOptions = array(
                'ssl' => array(
                    'verify_peer' => false,
                    'verify_peer_name' => false,
                    'allow_self_signed' => true
                )
            );
            $result = $mail->Send();
            if($result){
                return '1';
            }
            else{
                return $mail->ErrorInfo;
            }
        }
        catch (Exception $e) {
            return  $e->getMessage();
        }
    }
    
    public  function SendWithUser($subject,$body,$to,$attachmentPath,$attachmentName){


        $query="select * from dripmarket_configuration where userid=".$this->_userID;
        $resourcedetail=mysql_query($query);
        $row = mysql_fetch_array($resourcedetail);

        // TODO: put a constaint on the table, for now...if there is an 
        // application logic error, use the most recent config (order by id desc)
        $mail= new PHPMailer();
        $mail->IsSMTP(); // telling the class to use SMTP
        $mail->SMTPAuth   = true;                  // enable SMTP authentication
        $mail->Host       = $row['smtphost'];
        $mail->Port       =  $row['smtpport'];    
        $mail->Username   =  $row['smtpuser'];   // SMTP account username
        $mail->Password   = $row['smtppassword'];

        if($row['email_type']!='none') {
            //$mail->SMTPSecure = 'ssl';  
            $mail->SMTPSecure =$row['email_type']; 
        }

        $mail->SetFrom($row['fromemail'], $row['fromname']);
        $mail->Subject    = $subject;

        $mail->MsgHTML($body);

        if(!empty($attachmentPath) && !empty($attachmentName)){
            $mail->AddAttachment($attachmentPath, $attachmentName);
        }

        if (strstr($to, ';')) {
            $addresses = explode(';', $to);
            $mail->AddAddress($addresses[0], '');
            if(count($addresses) > 1)
            {
                $mail->AddAddress($addresses[1], '');
            }
        }
        else if (strstr($to, ',')) {
            $attendee = '';
            $addresses = explode(',', $to);
            $mail->AddAddress($addresses[0], '');

            if(count($addresses) > 1)
            {
                $mail->AddAddress($addresses[1], '');

            }
        }
        else{
            $mail->AddAddress($to, '');
        }
        
        try
        {
            // use just in case of ssl issue
            $mail->SMTPOptions = array(
                'ssl' => array(
                    'verify_peer' => false,
                    'verify_peer_name' => false,
                    'allow_self_signed' => true
                )
            );

            $result = $mail->Send();
            if($result){
                return '1';
            }
            else{
                return $mail->ErrorInfo;
            }
        }
        catch (Exception $e) {
            return  $e->getMessage();
        }

    }
    
    public function SendSms($CandidateId, $To, $Body){
        if(strpos($Body, "Reply with STOP to un-subscribe from service") !== true){
            $Body = $Body."\r\n Reply with STOP to un-subscribe from service";
        }
        $User = new Users($this->_siteID);
        $userInfo = $User->get($this->_userID);
        $SmsLog = new SmsLog($this->_userID, $this->_siteID);
        $client = new Client($userInfo['twillioSID'], $userInfo['twillioToken']);
        try {
            $result = $client->messages->create(
                // the number you'd like to send the message to
                $To,
                array(
                    // A Twilio phone number you purchased at twilio.com/console
                    'from' => $userInfo['twillioFromNumber'],
                    // the body of the text message you'd like to send
                    'body' => $Body
                )
            );
            $SmsLog->AddToDripSmsReply($CandidateId, $this->_userID, null, $userInfo['twillioFromNumber'], $To, $Body);
            return true;
        }
        catch(Exception $e) {
            $SmsLog->AddToDripSmsReply($CandidateId, $this->_userID, $e->getMessage(), $userInfo['twillioFromNumber'], $To, $Body);
            return $e->getMessage();
        }
    }
    public function ResendSms($DripSmsReplyId){
        $User = new Users($this->_siteID);
        $userInfo = $User->get($this->_userID);
        $SmsLog = new SmsLog($this->_userID, $this->_siteID);
        $SmsLogRecord = $SmsLog->GetSmsLogBySmsLogId($DripSmsReplyId);
        if(empty($SmsLogRecord[0]['MessageSid'])){
            return true;
        }
        $To = $SmsLogRecord[0]['To'];
        $Body = $SmsLogRecord[0]['Body'];
        $client = new Client($userInfo['twillioSID'], $userInfo['twillioToken']);
        try {
            $result = $client->messages->create(
                // the number you'd like to send the message to
                $To,
                array(
                    // A Twilio phone number you purchased at twilio.com/console
                    'from' => $userInfo['twillioFromNumber'],
                    // the body of the text message you'd like to send
                    'body' => $Body
                )
            );
            $SmsLog->UpdateDripSmsReply($DripSmsReplyId, null);
            return true;
        }
        catch(Exception $e) {
            $SmsLog->UpdateDripSmsReply($DripSmsReplyId, $e->getMessage());
            return $e->getMessage();
        }
    }
}
