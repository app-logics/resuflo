<?php


/**
 * RESUFLO
 * Calendar Library
 *
 * Copyright (C) 2005 - 2007 Econn Technologies, Inc.
 *
 *
 * The contents of this file are subject to the RESUFLO Public License
 * Version 1.1a (the "License"); you may not use this file except in
 * compliance with the License. You may obtain a copy of the License at
 * http://www.econnoisseur.com/.
 *
 * Software distributed under the License is distributed on an "AS IS"
 * basis, WITHOUT WARRANTY OF ANY KIND, either express or implied. See the
 * License for the specific language governing rights and limitations
 * under the License.
 *
 * The Original Code is "RESUFLO Standard Edition".
 *
 * The Initial Developer of the Original Code is Econn Technologies, Inc.
 * Portions created by the Initial Developer are Copyright (C) 2005 - 2007
 * (or from the year in which this file was created to the year 2007) by
 * Econn Technologies, Inc. All Rights Reserved.
 *
 *
 * @package    RESUFLO
 * @subpackage Library
 * @copyright Copyright (C) 2005 - 2007 Econn Technologies, Inc.
 * @version    $Id: Calendar.php 3595 2007-11-13 17:41:18Z andrew $
 */

/* Calendar event type flags. */
define('CALENDAR_EVENT_CALL',      100);
define('CALENDAR_EVENT_EMAIL',     200);
define('CALENDAR_EVENT_MEETING',   300);
define('CALENDAR_EVENT_INTERVIEW', 400);
define('CALENDAR_EVENT_PERSONAL',  500);
define('CALENDAR_EVENT_OTHER',     600);


include_once('./lib/ResultSetUtility.php');
include_once('./lib/DateUtility.php');
include_once('./lib/Companies.php');
include_once('./lib/Candidates.php');
include_once('./lib/JobOrders.php');
include_once('./lib/Contacts.php');
include_once('./lib/Users.php');
include_once('./lib/UserMailer.php');

class Ticket
{
    private $_db;
    private $_siteID;
    private $_userID;


    public function __construct($siteID)
    {
        $this->_siteID = $siteID;
        // FIXME: Factor out Session dependency.
        $this->_userID = $_SESSION['RESUFLO']->getUserID();
        $this->_db = DatabaseConnection::getInstance();
    }


    /**
     * Returns all calendar settings for a site.
     *
     * @return array (setting => value)
     */
    public function getAll()
    {
        $sql = sprintf(
            "SELECT 
                ticket.*, user.user_name,
                (SELECT 
                        userId 
                FROM 
                        ticketresponse tr 
                WHERE
                        tr.ticketId = ticket.ticketId 
                ORDER BY 
                        ticketResponseId DESC 
                limit 1) lastUpdatedBy 
            FROM 
                    ticket LEFT JOIN
                    user ON ticket.userid = user.user_id
            WHERE
                    resolved = 0
            ORDER BY 
                    date_created DESC"
        );
        $tickets = $this->_db->getAllAssoc($sql);
        return $tickets;
    }
    
    public function getAllUserTickets()
    {
        $sql = sprintf(
            "SELECT 
                ticket.*, user.user_name,
                (SELECT 
                        userId 
                FROM 
                        ticketresponse tr 
                WHERE
                        tr.ticketId = ticket.ticketId 
                ORDER BY 
                        ticketResponseId DESC 
                limit 1) lastUpdatedBy 
            FROM 
                ticket LEFT JOIN
                user ON ticket.userid = user.user_id
            WHERE
                userid = %s
                and resolved = 0
            ORDER BY 
                date_created DESC",
            $this->_userID
        );
        $tickets = $this->_db->getAllAssoc($sql);
        return $tickets;
    }
    
    public  function getTicketById($ticketId){
        $sql = sprintf(
            "SELECT
                *
            FROM
                ticket
            WHERE
                ticketId = %s",
               $ticketId
        );
        $tickets = $this->_db->getAllAssoc($sql);
        return $tickets;
    }

    public function getAllTicketResponse($ticketId)
    {
        $sql = sprintf(
            "SELECT
                ticketresponse.*, user.user_name
            FROM
                ticketresponse left join 
                user on ticketresponse.userid = user.user_id 
            WHERE
                ticketId = %s",
               $ticketId
        );
        $response = $this->_db->getAllAssoc($sql);
        return $response;
    }
    
    public  function addTicket($userId, $title, $type){
        $sql = sprintf(
            "INSERT INTO ticket (
                userId,
                title,
                type
            )
            VALUES (
                %s,
                %s,
                %s
            )",
            $this->_db->makeQueryInteger($userId),
            $this->_db->makeQueryString($title),
            $this->_db->makeQueryString($type)
        );

        $queryResult = $this->_db->query($sql);
        if (!$queryResult)
        {
            return -1;
        }

        return $this->_db->getLastInsertID();
    }
    public  function addTicketResponse($ticketId, $userId, $response){
        $sql = sprintf(
            "INSERT INTO ticketresponse (
                ticketid,
                userId,
                response
            )
            VALUES (
                %s,
                %s,
                %s
            )",
            $this->_db->makeQueryInteger($ticketId),
            $this->_db->makeQueryInteger($userId),
            $this->_db->makeQueryString($response)
        );

        $queryResult = $this->_db->query($sql);
        if (!$queryResult)
        {
            return -1;
        }
        $ticketResponseId = $this->_db->getLastInsertID();
        //send email to client if admin update ticket
        if($userId == 1){
            $clientId = $this->getTicketById($ticketId)[0][userId];
            $user = new Users(1);
            $clientEmail = $user->get($clientId)[email];
            try{
                file_put_contents('C:/Windows/Temp/ResufloLog/UserMailerCaller.txt', PHP_EOL.(new DateTime())->format('d m Y H:i:s').'===== START ticket', FILE_APPEND);
                $email = new UserMailer(1);
                $result = $email->SendEmail(array($clientEmail), null, 'Ticket Update', 'A has been updated by admin. Please have look to it on <a href="https://resuflocrm.com">resuflocrm.com</a>');
                file_put_contents('C:/Windows/Temp/ResufloLog/UserMailerCaller.txt', '=> END ====='.$result, FILE_APPEND);
            }
            catch (Exception $e) {
                file_put_contents('C:/Windows/Temp/ResufloLog/UserMailerCaller.txt', (new DateTime())->format('d m Y H:i:s').'ticket ====='.$e->getMessage().PHP_EOL, FILE_APPEND);
            }
                
        }
        //send email to admin if user create/update
        else
        {
            $siteID = $_SESSION['RESUFLO']->getSiteID();
            $type = $this->getTicketById($ticketId)[0][type];
            $email = new UserMailer(1);
            try{
                file_put_contents('C:/Windows/Temp/ResufloLog/UserMailerCaller.txt', PHP_EOL.(new DateTime())->format('d m Y H:i:s').'===== START ticket', FILE_APPEND);
                if($type == 'general'){
                    $result = $email->SendEmail(array(GeneralReport), null, 'Ticket Update - General', 'Admin'.$siteID.'<br/>Ticket has been updated by user. Please have look to it on <a href="https://resuflocrm.com">resuflocrm.com</a>');
                }
                else{
                    $result = $email->SendEmail(array(TechnicalReport), null, 'Ticket Update - Technical', 'Admin'.$siteID.'<br/>Ticket has been updated by user. Please have look to it on <a href="https://resuflocrm.com">resuflocrm.com</a>');
                }
                file_put_contents('C:/Windows/Temp/ResufloLog/UserMailerCaller.txt', '=> END ====='.$result, FILE_APPEND);
            }
            catch (Exception $e) {
                file_put_contents('C:/Windows/Temp/ResufloLog/UserMailerCaller.txt', (new DateTime())->format('d m Y H:i:s').'ticket ====='.$e->getMessage().PHP_EOL, FILE_APPEND);
            }
        }

        return $ticketResponseId;
    }
    
    public  function addTicketResponseImage($ticketResponseId, $image){
        $sql = sprintf(
            "UPDATE
                ticketresponse
            SET
                image             = %s
            WHERE
                ticketResponseId = %s",
            $this->_db->makeQueryString($image),
            $this->_db->makeQueryInteger($ticketResponseId)
        );

        return (boolean) $this->_db->query($sql);
    }
    
    public  function markResolved($ticketId, $whoClosed){
        //report to admin if closed by user
        if($whoClosed == 1){
            $type = $this->getTicketById($ticketId)[0][type];
            $email = new UserMailer(1);
            try{
                file_put_contents('C:/Windows/Temp/ResufloLog/UserMailerCaller.txt', PHP_EOL.(new DateTime())->format('d m Y H:i:s').'===== START ticket', FILE_APPEND);
                if($type == 'general'){
                    $result = $email->SendEmail(array(GeneralReport), null, 'Ticket Closed - General', 'Ticket has been closed by user.');
                }
                else{
                    $result = $email->SendEmail(array(TechnicalReport), null, 'Ticket Closed - Technical', 'Ticket ticket has been closed by user.');
                }
                file_put_contents('C:/Windows/Temp/ResufloLog/UserMailerCaller.txt', '=> END ====='.$result, FILE_APPEND);
            }
            catch (Exception $e) {
                file_put_contents('C:/Windows/Temp/ResufloLog/UserMailerCaller.txt', (new DateTime())->format('d m Y H:i:s').'ticket ====='.$e->getMessage().PHP_EOL, FILE_APPEND);
            }
        }
        //report to client if admin closed the ticket. $whoClosed = 2
        else{
            $clientId = $this->getTicketById($ticketId)[0][userId];
            $user = new Users(1);
            $clientEmail = $user->get($clientId)[email];
            try{
                file_put_contents('C:/Windows/Temp/ResufloLog/UserMailerCaller.txt', PHP_EOL.(new DateTime())->format('d m Y H:i:s').'===== START ticket', FILE_APPEND);
                $email = new UserMailer(1);
                $result = $email->SendEmail(array($clientEmail), null, 'Ticket Closed', 'Ticket ticket has been closed by admin due to no response.');
                file_put_contents('C:/Windows/Temp/ResufloLog/UserMailerCaller.txt', '=> END ====='.$result, FILE_APPEND);
            }
            catch (Exception $e) {
                file_put_contents('C:/Windows/Temp/ResufloLog/UserMailerCaller.txt', (new DateTime())->format('d m Y H:i:s').'ticket ====='.$e->getMessage().PHP_EOL, FILE_APPEND);
            }
        }
        $sql = sprintf(
            "UPDATE
                ticket
            SET
                resolved             = %s
            WHERE
                ticketId = %s",
            $this->_db->makeQueryInteger($whoClosed),
            $this->_db->makeQueryInteger($ticketId)
        );

        return (boolean) $this->_db->query($sql);
    }
}

?>
