<html>
<head>
<title>With Gmail SMTP</title>
</head>
<body>


<?php
echo !extension_loaded('openssl')?"SSL Ext. Not Loaded!<br/>":"SSL Ext Loaded!<br/>";

/**
 * This example shows how to send via Google's Gmail servers using XOAUTH2 authentication.
 */

//Import PHPMailer classes into the global namespace
use PHPMailer\PHPMailer\PHPMailer;
use PHPMailer\PHPMailer\OAuth;

// Alias the League Google OAuth2 provider class
use League\OAuth2\Client\Provider\Google;

//SMTP needs accurate times, and the PHP time zone MUST be set
//This should be done in your php.ini, but this is how to do it if you don't have access to that
//date_default_timezone_set('Etc/UTC');

//Load dependencies from composer
//If this causes an error, run 'composer install'
require '../../vendor/autoload.php';

//Create a new PHPMailer instance
$mail = new PHPMailer;
//$mail->SMTPDebug = SMTP::DEBUG_SERVER(2);
//$mail->SMTPDebug = 4;

//Tell PHPMailer to use SMTP
$mail->isSMTP();

//$mail->SMTPAutoTLS = false;
//$mail->Host       = "ssl://smtp.gmail.com";      // sets GMAIL as the SMTP server

$mail->Host       = "smtp.gmail.com";      // sets GMAIL as the SMTP server
$mail->SMTPAuth   = true;                 // enable SMTP authentication
//$mail->SMTPAutoTLS = false;
$mail->Username   = "sftestacc111@gmail.com";  // GMAIL username
$mail->Password   = "whatisthis21"; // GMAIL password
$mail->SMTPSecure = "ssl";   	// sets the prefix to the servier
$mail->Port       = 465;                   // set the SMTP port for the GMAIL server

$mail->SetLanguage("tr", "phpmailer/language");
$mail->CharSet ="utf-8";
$mail->Encoding="base64";

$mail->SetFrom('sftestacc111@gmail.com', 'Soft Logics');
$to = "macafzal@gmail.com";
$mail->AddAddress($to);
$mail->isHTML(true);
$mail->AddReplyTo("sftestacc111@gmail.com","Soft Logics");
$mail->Subject    = "PHPMailer 6.0 Gmail 465 Test";
$mail->MsgHTML('Message Here...');

// use just in case of ssl issue
$mail->SMTPOptions = array(
    'ssl' => array(
        'verify_peer' => false,
        'verify_peer_name' => false,
        'allow_self_signed' => true
    )
);

if(!$mail->Send()) {

  echo "Mailer Error: " . $mail->ErrorInfo;

} else {

  echo "Message sent!";

}

?>

</body>
</html>
