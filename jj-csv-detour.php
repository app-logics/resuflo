<?php

// the request from je 2014-10-14 was to make the
// csv upload be parsed by the new jj-parse-infog
// parser so we're going to put a switch here, so
// when the csv is in this request jj-parse-infog
// parses it but when there is no csv in the POST
// then we just move on and allow the old, legacy
// stuff to do whatever it was pretending to when
// we got here in the first place. just one line!

if (!empty($_FILES)) {
    // these lines conglomerated from jj-parse-infogist.php
    $base_path = rtrim($_SERVER['DOCUMENT_ROOT'], '/').'/';
    $resume_dir = $base_path.'upload/resume/';
    chdir($base_path); // fixes issues with legacy code
    require_once($base_path.'lib/Candidates.php');
    require_once($base_path.'config.php');
    require_once($base_path.'constants.php');
    require_once($base_path.'jp.class.php');
    $db_jj = DatabaseConnection::getInstance();
    $debug = true;
    $debug_annoying = true;
//    if ($debug==true) ob_start(); // this too :/
    $jjp = new jjp($db_jj,$debug,$debug_annoying);
    $jjp->base_path = $base_path;
    $jjp->resume_dir = $resume_dir;

    // get the user
    $user_name = $_SESSION['RESUFLO']->getUsername();
    $user_id = $_SESSION['RESUFLO']->getUserid();
    $email = $_SESSION['RESUFLO']->getEmail();
    $email_debug = $email.',jay@toptal.com'; // email the user
    $folder = $resume_dir.$user_name;

    $f = $_FILES['resumecontent']['tmp_name'];
    $filename = basename($_FILES['resumecontent']['name']);
    
    // make the user dir if it doesn't exit
    while (is_dir($folder)) {
        // parsing must be going on...let's wait a minute
        if ($debug) echo 'sleeping! *'.$folder.' could not be made*: does that folder exist? '.((file_exists($folder))?'Yes':'No')."\n";
        sleep(30);
    }
    mkdir($folder);

    // copy the csv into a folder (something that is precompleted in jj-parse-infogist)
    if ($debug) echo 'copying '.$filename.' (1 csv upload file)'."\n";
    $newf = $folder.'/'.$filename;
    if (!move_uploaded_file($f, $newf)) {
        die('there was a fatal error moving '.$f.' <strong>TO</strong> '.$newf.' - please attempt to report this so a developer can fix it - this is not normal but can be fixed quite easily.  sorry for the poor message, but honestly nobody should ever end up here during normal operations (it is just an ownership problem with the web server and the upload directory probably');
    }
    $f = $newf; unset($newf);
    // class.parseresume will attempt to move this if we don't unset it
    $_FILES['resumecontent'] = null;
    unset($_FILES['resumecontent']);

    // let's get crack-alackin!
    if ($debug) echo 'processing '.$user_name."\n";
    $files = array($f);
    $jjp->process_files($files, $user_name, $folder.'/*');
        $message = ob_get_contents();
echo 'm:'.$message;
ob_end_clean();
die();


    if (!empty($email_debug)) {
        $message = ob_get_contents();
        if (!empty($message)) mail($email_debug, 'jjparse: '.basename($f).' '.date('Y-m-d'), $message);
    }

    unlink($f);
    rmdir($folder);

    ob_end_clean();
}
