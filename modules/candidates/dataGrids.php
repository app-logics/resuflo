<?php

//TODO: License
error_reporting(E_ALL ^ E_NOTICE);
include_once('lib/Candidates.php');


class candidatesListByViewDataGrid extends CandidatesDataGrid
{
    public function __construct($siteID, $parameters, $misc)
    {
        /* Pager configuration. */
        $this->_tableWidth = 915;
        $this->_defaultAlphabeticalSortBy = 'firstName';
        $this->ajaxMode = false;
        $this->showExportCheckboxes = true; //BOXES WILL NOT APPEAR UNLESS SQL ROW exportID IS RETURNED!
        $this->showActionArea = true;
        $this->showChooseColumnsBox = true;
        $this->allowResizing = true;

        $this->defaultSortBy = 'dateCreatedSort';
        $this->defaultSortDirection = 'DESC';
	   $this->_defaultColumns = array(
            array('name' => 'Attachments', 'width' => 31),
            array('name' => 'First Name', 'width' => 75),
            array('name' => 'Last Name', 'width' => 85),
            array('name' => 'Company', 'width' => 75),
            array('name' => 'State', 'width' => 50),
            array('name' => 'Key Skills', 'width' => 215),
            array('name' => 'Recruiter', 'width' => 65),
            array('name' => 'Created', 'width' => 60),
            array('name' => 'Modified', 'width' => 60),
            array('name' => 'Scheduled', 'width' => 60),
        );

         parent::__construct("candidates:candidatesListByViewDataGrid",
                             $siteID, $parameters, $misc
                        );
    }


    /**
     * Adds more options to the action area on the pager.  Overloads
     * DataGrid Inner Action Area function.
     *
     * @return html innerActionArea commands.
     */
    public function getInnerActionArea()
    {
        //TODO: Add items:
        //  - Add to List
        //  - Add to Pipeline
        //  - Mass set rank (depends on each candidate having their own personal rank - are we going to do this?)
		$access_level = $_SESSION['RESUFLO']->getAccessLevel();
        $html = '';

        //$html .= $this->getInnerActionAreaItemPopup('Add To List', RESUFLOUtility::getIndexName().'?m=lists&amp;a=addToListFromDatagridModal&amp;dataItemType='.DATA_ITEM_CANDIDATE, 450, 350);
       /* $html .= $this->getInnerActionAreaItemPopup('Add To Pipeline', RESUFLOUtility::getIndexName().'?m=candidates&amp;a=considerForJobSearch', 750, 460);
        if(MAIL_MAILER != 0)
        {
            $html .= $this->getInnerActionAreaItem('Send E-Mail', RESUFLOUtility::getIndexName().'?m=candidates&amp;a=emailCandidates');
        }	*/																						
        //$html .= $this->getInnerActionAreaItem('Export', RESUFLOUtility::getIndexName().'?m=export&amp;a=exportByDataGrid');

	if($_REQUEST['a']=='Inactive' )
	{
            $html .= $this->getInnerActionAreaItemPopup('Add To List', RESUFLOUtility::getIndexName().'?m=lists&amp;a=addToListFromDatagridModal&amp;delrec=Inactive&amp;dataItemType='.DATA_ITEM_CANDIDATE, 450, 350);
            $html .= $this->getInnerActionAreaItemPopup('Add To Pipeline', RESUFLOUtility::getIndexName().'?m=candidates&amp;a=considerForJobSearch&amp;delrec=Inactive', 750, 460);
            if(MAIL_MAILER != 0)
            {
                $html .= $this->getInnerActionAreaItem('Send E-Mail', RESUFLOUtility::getIndexName().'?m=candidates&amp;a=emailCandidates&amp;delrec=Inactive');
            }	
	    $html .= $this->getInnerActionAreaItem('Delete', RESUFLOUtility::getIndexName().'?m=candidates&amp;a=delete_Candidates&amp;delrec=Inactive');
            $html .= $this->getInnerActionAreaItem('Export', RESUFLOUtility::getIndexName().'?m=export&amp;a=exportByDataGrid&amp;delrec=Inactive');
	}
	else if($_REQUEST['a']=='follow' )
	{
            $html .= $this->getInnerActionAreaItemPopup('Add To List', RESUFLOUtility::getIndexName().'?m=lists&amp;a=addToListFromDatagridModal&amp;delrec=follow&amp;dataItemType='.DATA_ITEM_CANDIDATE, 450, 350);
            $html .= $this->getInnerActionAreaItemPopup('Add To Pipeline', RESUFLOUtility::getIndexName().'?m=candidates&amp;a=considerForJobSearch&amp;delrec=follow', 750, 460);
            $html .= $this->getInnerActionAreaItemPopup('Add to Recruiter account', RESUFLOUtility::getIndexName().'?m=candidates&amp;a=addToRecruiter', 750, 460);
            if(MAIL_MAILER != 0)
            {
                $html .= $this->getInnerActionAreaItem('Send E-Mail', RESUFLOUtility::getIndexName().'?m=candidates&amp;a=emailCandidates&amp;delrec=follow');
            }	
	    $html .= $this->getInnerActionAreaItem('Delete', RESUFLOUtility::getIndexName().'?m=candidates&amp;a=delete_Candidates&amp;delrec=follow');
            $html .= $this->getInnerActionAreaItem('Export', RESUFLOUtility::getIndexName().'?m=export&amp;a=exportByDataGrid&amp;delrec=follow');
	}
	else if($_REQUEST['a']=='PipeLine' )
	{
            $html .= $this->getInnerActionAreaItemPopup('Add To List', RESUFLOUtility::getIndexName().'?m=lists&amp;a=addToListFromDatagridModal&amp;delrec=PipeLine&amp;dataItemType='.DATA_ITEM_CANDIDATE, 450, 350);
            $html .= $this->getInnerActionAreaItemPopup('Add To Pipeline', RESUFLOUtility::getIndexName().'?m=candidates&amp;a=considerForJobSearch&amp;delrec=PipeLine', 750, 460);
            $html .= $this->getInnerActionAreaItem('Add To Follow Up', RESUFLOUtility::getIndexName().'?m=candidates&amp;a=addToFollow&amp;follow=1&amp;delrec=PipeLine');
            if(MAIL_MAILER != 0)
            {
                $html .= $this->getInnerActionAreaItem('Send E-Mail', RESUFLOUtility::getIndexName().'?m=candidates&amp;a=emailCandidates&amp;delrec=PipeLine');
            }	
	    $html .= $this->getInnerActionAreaItem('Delete', RESUFLOUtility::getIndexName().'?m=candidates&amp;a=delete_Candidates&amp;delrec=PipeLine');
            $html .= $this->getInnerActionAreaItem('Export', RESUFLOUtility::getIndexName().'?m=export&amp;a=exportByDataGrid&amp;delrec=PipeLine');
	}
	else if($access_level == '400'){
            $view = "&amp;view=all";
            if($_REQUEST['a']=='new_candidates'){
                $view = "";
            }
            
            $html .= $this->getInnerActionAreaItemPopup('Add To Campaign', RESUFLOUtility::getIndexName().'?m=lists&amp;a=addToCampaignFromDatagridModal'.$view.'&amp;dataItemType='.DATA_ITEM_CANDIDATE, 450, 350);
            $html .= $this->getInnerActionAreaItemPopup('Add To List', RESUFLOUtility::getIndexName().'?m=lists&amp;a=addToListFromDatagridModal'.$view.'&amp;dataItemType='.DATA_ITEM_CANDIDATE, 450, 350);
            $html .= $this->getInnerActionAreaItemPopup('Add To Pipeline', RESUFLOUtility::getIndexName().'?m=candidates&amp;a=considerForJobSearch'.$view, 750, 460);
            if(MAIL_MAILER != 0)
            {
                $html .= $this->getInnerActionAreaItem('Send E-Mail', RESUFLOUtility::getIndexName().'?m=candidates&amp;a=emailCandidates'.$view);
            }	
            $html .= $this->getInnerActionAreaItem('Delete', RESUFLOUtility::getIndexName().'?m=candidates&amp;a=delete_Candidates'.$view);
			
            $html .= $this->getInnerActionAreaItemPopup('Add to Recruiter account', RESUFLOUtility::getIndexName().'?m=candidates&amp;a=addToRecruiter'.$view, 750, 460);
            $html .= $this->getInnerActionAreaItem('Export', RESUFLOUtility::getIndexName().'?m=export&amp;a=exportByDataGrid'.$view);
	}
	else{
                $view = "&amp;view=all";
                if($_REQUEST['a']=='new_candidates'){
                    $view = "";
                }
		$html .= $this->getInnerActionAreaItemPopup('Add To Campaign', RESUFLOUtility::getIndexName().'?m=lists&amp;a=addToCampaignFromDatagridModal'.$view.'&amp;dataItemType='.DATA_ITEM_CANDIDATE, 450, 350);
		$html .= $this->getInnerActionAreaItemPopup('Add To List', RESUFLOUtility::getIndexName().'?m=lists&amp;a=addToListFromDatagridModal'.$view.'&amp;dataItemType='.DATA_ITEM_CANDIDATE, 450, 350);
                $html .= $this->getInnerActionAreaItemPopup('Add To Pipeline', RESUFLOUtility::getIndexName().'?m=candidates&amp;a=considerForJobSearch'.$view, 750, 460);
                if(MAIL_MAILER != 0)
                {
                    $html .= $this->getInnerActionAreaItem('Send E-Mail', RESUFLOUtility::getIndexName().'?m=candidates&amp;a=emailCandidates'.$view);
                }	
		$html .= $this->getInnerActionAreaItem('Delete', RESUFLOUtility::getIndexName().'?m=candidates&amp;a=delete_Candidates'.$view);
		if($_REQUEST['a']=='new_candidates')
		{
                    $html .= $this->getInnerActionAreaItem('Export', RESUFLOUtility::getIndexName().'?m=export&amp;v=new&amp;a=exportByDataGrid'.$view);
		}
		else
		{
                    $html .= $this->getInnerActionAreaItem('Export', RESUFLOUtility::getIndexName().'?m=export&amp;a=exportByDataGrid'.$view);
		}
	}
        $html .= parent::getInnerActionArea();

        return $html;
    }
}

class candidatesSavedListByViewDataGrid extends CandidatesDataGrid
{
    public function __construct($siteID, $parameters, $misc)
    {
        $this->_tableWidth = 915;
        $this->_defaultAlphabeticalSortBy = 'firstName';
        $this->ajaxMode = false;
        $this->showExportCheckboxes = true; //BOXES WILL NOT APPEAR UNLESS SQL ROW exportID IS RETURNED!
        $this->showActionArea = true;
        $this->showChooseColumnsBox = true;
        $this->allowResizing = true;
		
        $this->defaultSortBy = 'dateCreatedSort';
        $this->defaultSortDirection = 'DESC';

        $this->_defaultColumns = array(
            array('name' => 'Attachments', 'width' => 31),
            array('name' => 'First Name', 'width' => 75),
            array('name' => 'Last Name', 'width' => 85),
            array('name' => 'Company', 'width' => 75),
            array('name' => 'State', 'width' => 50),
            array('name' => 'Key Skills', 'width' => 200),
            array('name' => 'Recruiter', 'width' => 65),
            array('name' => 'Modified', 'width' => 60),
            array('name' => 'Scheduled', 'width' => 60),
            array('name' => 'Added To List', 'width' => 75),
        );

         parent::__construct("candidates:candidatesSavedListByViewDataGrid",
                             $siteID, $parameters, $misc
                        );
    }

    /**
     * Adds more options to the action area on the pager.  Overloads
     * DataGrid Inner Action Area function.
     *
     * @return html innerActionArea commands.
     */
    public function getInnerActionArea()
    {
        //TODO: Add items:
        //  - Add to List
        //  - Add to Pipeline
        //  - Mass set rank (depends on each candidate having their own personal rank - are we going to do this?)
        $html = '';
        $html .= $this->getInnerActionAreaItem('Remove From This Lists', RESUFLOUtility::getIndexName().'?m=lists&amp;a=removeFromListDatagrid&amp;dataItemType='.DATA_ITEM_CANDIDATE.'&amp;delrec=view_all&amp;savedListID='.$this->getMiscArgument(), true);
        $html .= $this->getInnerActionAreaItemPopup('Move To List', RESUFLOUtility::getIndexName().'?m=lists&amp;a=addToListFromDatagridModal&amp;dataItemType='.DATA_ITEM_CANDIDATE, 450, 350);
        $html .= $this->getInnerActionAreaItemPopup('Add To Campaign', RESUFLOUtility::getIndexName().'?m=lists&amp;a=addToCampaignFromDatagridModal&amp;dataItemType='.DATA_ITEM_CANDIDATE, 450, 350);
        $html .= $this->getInnerActionAreaItemPopup('Add To Pipeline', RESUFLOUtility::getIndexName().'?m=candidates&amp;a=considerForJobSearch', 750, 460);
        $html .= $this->getInnerActionAreaItemPopup('Add to Recruiter account', RESUFLOUtility::getIndexName().'?m=candidates&amp;a=addToRecruiter', 750, 460);
        if(MAIL_MAILER != 0)
        {
            $html .= $this->getInnerActionAreaItem('Send E-Mail', RESUFLOUtility::getIndexName().'?m=candidates&amp;a=emailCandidates');
        }
        $html .= $this->getInnerActionAreaItem('Export', RESUFLOUtility::getIndexName().'?m=export&amp;a=exportByDataGrid');

        $html .= parent::getInnerActionArea();

        return $html;
    }
}


?>
