<?php
/*
 * RESUFLO
 * Candidates Module
 *
 * Copyright (C) 2005 - 2007 Econn Technologies, Inc.
 *
 *
 * The contents of this file are subject to the RESUFLO Public License
 * Version 1.1a (the "License"); you may not use this file except in
 * compliance with the License. You may obtain a copy of the License at
 *
 * Software distributed under the License is distributed on an "AS IS"
 * basis, WITHOUT WARRANTY OF ANY KIND, either express or implied. See the
 * License for the specific language governing rights and limitations
 * under the License.
 *
 * The Original Code is "RESUFLO Standard Edition".
 *
 * The Initial Developer of the Original Code is Econn Technologies, Inc.
 * Portions created by the Initial Developer are Copyright (C) 2005 - 2007
 * (or from the year in which this file was created to the year 2007) by
 * Econn Technologies, Inc. All Rights Reserved.
 *
 *
 * $Id: CandidatesUI.php 3810 2007-12-05 19:13:25Z brian $
 */

include_once('./lib/Statistics.php');
include_once('./lib/FileUtility.php');
include_once('./lib/StringUtility.php');
include_once('./lib/ResultSetUtility.php');
include_once('./lib/DateUtility.php'); /* Depends on StringUtility. */
include_once('./lib/Candidates.php');
include_once('./lib/Pipelines.php');
include_once('./lib/Attachments.php');
include_once('./lib/ActivityEntries.php');
include_once('./lib/JobOrders.php');
include_once('./lib/Export.php');
include_once('./lib/ExtraFields.php');
include_once('./lib/Calendar.php');
include_once('./lib/SavedLists.php');
include_once('./lib/EmailTemplates.php');
include_once('./lib/DocumentToText.php');
include_once('./lib/DatabaseSearch.php');
include_once('./lib/CommonErrors.php');
include_once('./lib/License.php');
include_once('./lib/ParseUtility.php');
include_once('./lib/Questionnaire.php');
include_once('./lib/SmsLog.php');

include_once('./lib/UserMailer.php');

class CandidatesUI extends UserInterface
{
    /* Maximum number of characters of the candidate notes to show without the
     * user clicking "[More]"
     */
    const NOTES_MAXLEN = 500;

    /* Maximum number of characters of the candidate name to show on the main
     * contacts listing.
     */
    const TRUNCATE_KEYSKILLS = 30;


    public function __construct()
    { 
	    parent::__construct();
		
		
            $this->_authenticationRequired = true;
            $this->_moduleDirectory = 'candidates';
            $this->_moduleName = 'candidates';
            $this->_moduleTabText = 'Candidates';
            $this->_subTabs = array( 
			/*'All Candidates' => RESUFLOUtility::getIndexName() . '?m=candidates&amp;a=view_all',*/
                        'Add Candidate'     => RESUFLOUtility::getIndexName() . '?m=candidates&amp;a=add*al=' . ACCESS_LEVEL_EDIT,
			/*'New Candidates' => RESUFLOUtility::getIndexName() . '?m=candidates&amp;a=new_candidates',*/
			/* Changed (New to All) by Charanjit Singh (02.08.2013) */
			'All Candidates' => RESUFLOUtility::getIndexName() . '?m=candidates',
			'PipeLine/Active' => RESUFLOUtility::getIndexName() . '?m=candidates&amp;a=PipeLine',
			'Follow up' => RESUFLOUtility::getIndexName() . '?m=candidates&amp;a=follow',
			'Inactive' => RESUFLOUtility::getIndexName() . '?m=candidates&amp;a=Inactive',
                        'Career Page Applicants' => RESUFLOUtility::getIndexName() . '?m=candidates&amp;a=CareerPage',
			'Search Candidates' => RESUFLOUtility::getIndexName() . '?m=candidates&amp;a=search',
                        'LinkedIn' => RESUFLOUtility::getIndexName() . '?m=candidates&amp;a=LinkedIn',
            );
    }
   
    public function handleRequest()
    {
        if (!eval(Hooks::get('CANDIDATES_HANDLE_REQUEST'))) return;
        
        $action = $this->getAction();

        switch ($action)
        {
            case 'show':
                $this->show();
                break;
            case 'filter':
                $filterid = $_GET['filterid'];
                if($filterid != null && $filterid != "")
                {
                    $candidates = new Candidates($this->_siteID);
                    $result = $candidates->deleteFilter($filterid);
                    echo $result;
                }
                else
                {
                    $name = $_GET['name'];
                    $params = $_GET['filterArea'];
                    $candidates = new Candidates($this->_siteID);
                    $loginedUser = $_SESSION['RESUFLO']->getUserID();
                    $result = $candidates->addFilter($loginedUser, $name, $params);
                    echo $result;
                }
                break;

            case 'add':

                // this is where je and jj decided to cheat a bit
                //require(rtrim($_SERVER['DOCUMENT_ROOT'], '/').'/jj-csv-detour.php');

                if ($this->isPostBack())
                {
                    $this->onAdd();
                }
                else
                {
                    $this->add();
                }

                break;

            case 'edit':
                if ($this->isPostBack())
                {
                    $this->onEdit();
                }
                else
                {
                    $this->edit();
                }

                break;
            
            case 'sms':
                if ($this->isPostBack())
                {
                    $this->onSendSms();
                }
                else
                {
                    $this->sms();
                }

                break;
            
            case 'smsapp':
                if ($this->isPostBack())
                {
                    $this->onSendSms();
                }
                else
                {
                    $this->smsapp();
                }
                break;

            case 'delete':
                $this->onDelete();
                break;
            case 'pdfdisplay':
                $this->pdfdisplay();
                break;
			
            /*  case 'search':
                include_once('./lib/Search.php');

                if ($this->isGetBack())
                {
                    $this->onSearch();
                }
                else
                {
                    $this->search();
                }

                break; 
             */

            case 'viewResume':
                include_once('./lib/Search.php');

                $this->viewResume();
                break;

            /*
             * Search for a job order (in the modal window) for which to
             * consider a candidate.
             */
            case 'considerForJobSearch':
                include_once('./lib/Search.php');

                $this->considerForJobSearch();

                break;
            case 'addToRecruiter':
                include_once('./lib/Search.php');

                $this->addToRecruiter();

                break;

            /*
             * Add candidate to pipeline after selecting a job order for which
             * to consider a candidate (in the modal window).
             */
            case 'addToPipeline':
                $this->onAddToPipeline();
                break;
				
            case 'addToFollow':
                $this->addToFollow();
                break; 

            /* Change candidate-joborder status. */
            case 'addActivityChangeStatus':
                if ($this->isPostBack())
                {
					
                    $this->onAddActivityChangeStatus();	
					
                }
                else
                {
                    $this->addActivityChangeStatus();
                }

                break;

            /* Remove a candidate from a pipeline. */
            case 'removeFromPipeline':
                $this->onRemoveFromPipeline();
                break;

            case 'addEditImage':
                if ($this->isPostBack())
                {
                    $this->onAddEditImage();
                }
                else
                {
                    $this->addEditImage();
                }

                break;

            /* Add an attachment to the candidate. */
            case 'createAttachment':
                include_once('./lib/DocumentToText.php');

                if ($this->isPostBack())
                {
                    $this->onCreateAttachment();
                }
                else
                {
                    $this->createAttachment();
                }

                break;

            /* Administrators can hide a candidate from a site with this action. */
            case 'administrativeHideShow':
                $this->administrativeHideShow();
                break;

            /* Delete a candidate attachment */
            case 'deleteAttachment':
                $this->onDeleteAttachment();
                break;
            
            /* Delete a candidate reminder */
            case 'cancelReminder':
                $this->onCancelReminder();
                break;
            
            /* Delete a candidate thanks */
            case 'cancelThanks':
                $this->onCancelThanks();
                break;
            
            /* Hot List Page */
            case 'savedLists':
                $this->savedList();
                break;

            case 'emailCandidates':
                $this->onEmailCandidates();
                break;
			
            case 'emailcandidate':
                $this->get_status_email_template();
                $this->onEmailcandidate();
                break;
            case 'show_questionnaire':
                $this->onShowQuestionnaire();
                break;
            case 'delete_Candidates':
                $this->deleteCandidates();
		        break;
						
			
            /* Main candidates page. */
            case 'listByView':
            default:
				
                $this->listByView();
                break;
        }
    }
	
	
	  /*
     * Called by external modules for deleting bulk candidates.
     */
	 
    public function deleteCandidates()
    {
        
              //  echo $_REQUEST['a'];die();
		
		if ($this->_accessLevel < ACCESS_LEVEL_DELETE)
                {
                    CommonErrors::fatal(COMMONERROR_PERMISSION, $this, 'Invalid user level for action.');
                }

                /* Bail out if we don't have a valid candidate ID. */
		
		$dataGrid = DataGrid::getFromRequest();
                $candidateIDs = $dataGrid->getExportIDs();
               
		$del=array();
           
		foreach($candidateIDs as $val =>$key){
			$del['m'] = 'candidates';
			$del['a'] = 'delete_Candidates';
			$del['candidateID'] = $key;
		
                        if (!$this->isRequiredIDValid('candidateID', $del))
			{
				CommonErrors::fatal(COMMONERROR_BADINDEX, $this, 'Invalid candidate ID.');
			}
	
			$candidateID = $del['candidateID'];
	
			if (!eval(Hooks::get('CANDIDATE_DELETE'))) return;
	
			$candidates = new Candidates($this->_siteID);
			$candidates->delete($candidateID);
	
			/* Delete the MRU entry if present. */
			$_SESSION['RESUFLO']->getMRU()->removeEntry(
				DATA_ITEM_CANDIDATE, $candidateID
			);
			
		}
                  if($_REQUEST['delrec']=='Inactive')
                {
                    RESUFLOUtility::transferRelativeURI('m=candidates&a=Inactive');
                }
                else if($_REQUEST['delrec']=='follow' )
                {
                    RESUFLOUtility::transferRelativeURI('m=candidates&a=follow');
                   
                }
                else if($_REQUEST['delrec']=='PipeLine' )
                {
                    RESUFLOUtility::transferRelativeURI('m=candidates&a=PipeLine');
                   
                }              
                else{
                       RESUFLOUtility::transferRelativeURI('m=candidates&a=listByView');
                }
             
		
		/*$userid = $_SESSION['RESUFLO']->getuserid();  
		        if ($this->_accessLevel < ACCESS_LEVEL_EDIT)
                {
                    CommonErrors::fatal(COMMONERROR_BADINDEX, $this, 'Invalid user level for action.');
                    return;
                }
				$dataGrid = DataGrid::getFromRequest();
                $candidateIDs = $dataGrid->getExportIDs();
		for($i = 0 ; $i<count($candidateIDs) ; $i++ )
                {
                        if($this->_accessLevel == 500){
							$sql=" DELETE FROM candidate where candidate_id = '$candidateIDs[$i]'";
						}else {
							$sql=" DELETE FROM candidate where candidate_id = '$candidateIDs[$i]' AND entered_by ='$userid'";
						}
						
                        $result=mysql_query($sql);
                }
    
       $_SESSION['RESUFLO']->getMRU()->removeEntry(
            DATA_ITEM_CANDIDATE, $candidateID
        );
      

      
       
		RESUFLOUtility::transferRelativeURI('m=candidates&a=listByView');*/
	 }
	 
    /*
     * Called by external modules for adding candidates.
     */
    public function publicAddCandidate($isModal, $transferURI, $moduleDirectory)
    {
        if ($this->_accessLevel < ACCESS_LEVEL_EDIT)
        {
            CommonErrors::fatal(COMMONERROR_BADINDEX, $this, 'Invalid user level for action.');
            return;
        }

        $candidateID = $this->_addCandidate($isModal, $moduleDirectory);

        if ($candidateID <= 0)
        {
            CommonErrors::fatalModal(COMMONERROR_RECORDERROR, $this, 'Failed to add candidate.');
        }
    
        $transferURI = str_replace(
            '__CANDIDATE_ID__', $candidateID, $transferURI
        );
        RESUFLOUtility::transferRelativeURI($transferURI);
    }

    /*
     * Called by external modules for processing the log activity / change
     * status dialog.
     */
    public function publicAddActivityChangeStatus($isJobOrdersMode, $regardingID, $moduleDirectory)
    {
		
        if ($this->_accessLevel < ACCESS_LEVEL_EDIT)
        {
            CommonErrors::fatal(COMMONERROR_PERMISSION, $this, 'Invalid user level for action.');
        }

        $this->_AddActivityChangeStatus(
            $isJobOrdersMode, $regardingID, $moduleDirectory
        );
    }

    /*
     * Called by handleRequest() to process loading the list / main page.
     */
    private function listByView($errMessage = '')
    {
        // Log message that shows up on the top of the list page
        $topLog = '';


        $dataGridProperties = DataGrid::getRecentParamaters("candidates:candidatesListByViewDataGrid");
		//die($dataGridProperties);


        /* If this is the first time we visited the datagrid this session, the recent paramaters will
         * be empty.  Fill in some default values. */
        if ($dataGridProperties == array() || $dataGridProperties[maxResults] == 9999999)
        {
            $dataGridProperties = array('rangeStart'    => 0,
                                        'maxResults'    => 15,
                                        'filterVisible' => false);
        }

        $dataGrid = DataGrid::get("candidates:candidatesListByViewDataGrid", $dataGridProperties);
        $userId = $_SESSION['RESUFLO']->getUserID();
        $User  = new Users($this->_siteID);
        $currentUser = $User->get($userId);
        $dataGrid->_isSourceVisible = $currentUser['isSourceVisible'];
        $candidates = new Candidates($this->_siteID);
        $this->_template->assign('totalCandidates', $candidates->getCount());

        $this->_template->assign('active', $this);
        $this->_template->assign('dataGrid', $dataGrid);
        $this->_template->assign('userID', $userId);
        $this->_template->assign('errMessage', $errMessage);
        $this->_template->assign('topLog', $topLog);
		
		
		
		

        if (!eval(Hooks::get('CANDIDATE_LIST_BY_VIEW'))) return;
		

        /* Check for Search Template   */
		if($_REQUEST['a']=='search' ){
		$mode = trim($_REQUEST['mode']);
			 $query = trim($_GET['wildCardString']);
			 
		  include_once('./lib/Search.php');
		$savedSearches = new SavedSearches($this->_siteID);
        $savedSearches->add(
            DATA_ITEM_CANDIDATE,
            $query,
            $_SERVER['REQUEST_URI'],
            false);
        
	
        $savedSearchRS = $savedSearches->get(DATA_ITEM_CANDIDATE);
        $this->_template->assign('savedSearchRS', $savedSearchRS);
        $this->_template->assign('active', $this);
        $this->_template->assign('wildCardString', $query);
        $this->_template->assign('resumeWildCardString', $resumeWildCardString);
        $this->_template->assign('keySkillsWildCardString', $keySkillsWildCardString);
        $this->_template->assign('fullNameWildCardString', $fullNameWildCardString);
        $this->_template->assign('phoneNumberWildCardString', $phoneNumberWildCardString);
		$this->_template->assign('tagsWildCardString', $tagsWildCardString);
		$this->_template->assign('emailWildCardString', $emailWildCardString);
        $this->_template->assign('mode', $mode);
		$this->_template->assign('strt', $strt);
		$this->_template->assign('end1', $end1);
       
		$this->_template->display('./modules/candidates/Candidates_search.tpl');
		}
		
		else
		{
		
		$this->_template->display('./modules/candidates/Candidates.tpl');
		}
    }
    
    
    
    //pdf display
    
    
    
        private function pdfdisplay()
    {
        $userid=$_SESSION['RESUFLO']->getUserID();
			
			$_accessLevel = $this->_accessLevel;	
		$this->_template->assign('recipients', $_accessLevel);
        /* Is this a popup? */
        if (isset($_GET['display']) && $_GET['display'] == 'popup')
        {
            $isPopup = true;
        }
        else
        {
            $isPopup = false;
        }

        /* Bail out if we don't have a valid candidate ID. */
        if (!$this->isRequiredIDValid('candidateID', $_GET) && !isset($_GET['email']))
        {
            CommonErrors::fatal(COMMONERROR_BADINDEX, $this, 'Invalid candidate ID.');
        }

        $candidates = new Candidates($this->_siteID);

        if (isset($_GET['candidateID']))
        {
            $candidateID = $_GET['candidateID'];
        }
        else
        {
            $candidateID = $candidates->getIDByEmail($_GET['email']);
        }
		
        $data = $candidates->get($candidateID);

        /* Bail out if we got an empty result set. */
        if (empty($data))
        {
            CommonErrors::fatal(COMMONERROR_BADINDEX, $this, 'The specified candidate ID could not be found.');
            return;
        }

        if ($data['isAdminHidden'] == 1 && $this->_accessLevel < ACCESS_LEVEL_MULTI_SA)
        {
            $this->listByView('This candidate is hidden - only a RESUFLO Administrator can unlock the candidate.');
            return;
        }

        /* We want to handle formatting the city and state here instead
         * of in the template.
         */
        $data['cityAndState'] = StringUtility::makeCityStateString(
            $data['city'], $data['state']
        );

        /*
         * Replace newlines with <br />, fix HTML "special" characters, and
         * strip leading empty lines and spaces.
         */
        $data['notes'] = trim(
            nl2br(htmlspecialchars($data['notes'], ENT_QUOTES))
        );

        /* Chop $data['notes'] to make $data['shortNotes']. */
        if (strlen($data['notes']) > self::NOTES_MAXLEN)
        {
            $data['shortNotes']  = substr(
                $data['notes'], 0, self::NOTES_MAXLEN
            );
            $isShortNotes = true;
        }
        else
        {
            $data['shortNotes'] = $data['notes'];
            $isShortNotes = false;
        }

        /* Format "can relocate" status. */
        if ($data['canRelocate'] == 1)
        {
            $data['canRelocate'] = 'Yes';
        }
        else
        {
            $data['canRelocate'] = 'No';
        }

        if ($data['isHot'] == 1)
        {
            $data['titleClass'] = 'jobTitleHot';
        }
        else
        {
            $data['titleClass'] = 'jobTitleCold';
        }

        $attachments = new Attachments($this->_siteID);
        $attachmentsRS = $attachments->getAll(
            DATA_ITEM_CANDIDATE, $candidateID
        );

        foreach ($attachmentsRS as $rowNumber => $attachmentsData)
        {
            /* If profile image is not local, force it to be local. */
            if ($attachmentsData['isProfileImage'] == 1)
            {
                $attachments->forceAttachmentLocal($attachmentsData['attachmentID']);
            }

            /* Show an attachment icon based on the document's file type. */
            $attachmentIcon = strtolower(
                FileUtility::getAttachmentIcon(
                    $attachmentsRS[$rowNumber]['originalFilename']
                )
            );

            $attachmentsRS[$rowNumber]['attachmentIcon'] = $attachmentIcon;

            /* If the text field has any text, show a preview icon. */
            if ($attachmentsRS[$rowNumber]['hasText'])
            {
                $attachmentsRS[$rowNumber]['previewLink'] = sprintf(
                    '<a href="#" onclick="window.open(\'%s?m=candidates&amp;a=viewResume&amp;attachmentID=%s\', \'viewResume\', \'scrollbars=1,width=800,height=760\')"><img width="15" height="15" style="border: none;" src="images/search.gif" alt="(Preview)" /></a>',
                    RESUFLOUtility::getIndexName(),
                    $attachmentsRS[$rowNumber]['attachmentID']
                );
            }
            else
            {
                $attachmentsRS[$rowNumber]['previewLink'] = '&nbsp;';
            }
        }
        $pipelines = new Pipelines($this->_siteID);
        $pipelinesRS = $pipelines->getCandidatePipeline($candidateID);

        $sessionCookie = $_SESSION['RESUFLO']->getCookie();

        /* Format pipeline data. */

        //die('<pre>'.print_r($pipelinesRS,true));
        //jobLinkCold in CandidatesUI::pdfdisplay()

        foreach ($pipelinesRS as $rowIndex => $row)
        {
            /* Hot jobs [can] have different title styles than normal
             * jobs.
             */
            if ($row['isHot'] == 1)
            {
                $pipelinesRS[$rowIndex]['linkClass'] = 'jobLinkHot';
            }
            else
            {
                $pipelinesRS[$rowIndex]['linkClass'] = 'jobLinkCold';
            }

            $pipelinesRS[$rowIndex]['ownerAbbrName'] = StringUtility::makeInitialName(
                $pipelinesRS[$rowIndex]['ownerFirstName'],
                $pipelinesRS[$rowIndex]['ownerLastName'],
                false,
                LAST_NAME_MAXLEN
            );

            $pipelinesRS[$rowIndex]['addedByAbbrName'] = StringUtility::makeInitialName(
                $pipelinesRS[$rowIndex]['addedByFirstName'],
                $pipelinesRS[$rowIndex]['addedByLastName'],
                false,
                LAST_NAME_MAXLEN
            );

            $pipelinesRS[$rowIndex]['ratingLine'] = TemplateUtility::getRatingObject(
                $pipelinesRS[$rowIndex]['ratingValue'],
                $pipelinesRS[$rowIndex]['candidateJobOrderID'],
                $sessionCookie
            );
        }

        $activityEntries = new ActivityEntries($this->_siteID);
        $activityRS = $activityEntries->getAllByDataItem($candidateID, DATA_ITEM_CANDIDATE, $userid);
        if (!empty($activityRS))
        {
            foreach ($activityRS as $rowIndex => $row)
            {
                if (empty($activityRS[$rowIndex]['notes']))
                {
                    $activityRS[$rowIndex]['notes'] = '(No Notes)';
                }

                if (empty($activityRS[$rowIndex]['jobOrderID']) ||
                    empty($activityRS[$rowIndex]['regarding']))
                {
                    $activityRS[$rowIndex]['regarding'] = 'General';
                }

                $activityRS[$rowIndex]['enteredByAbbrName'] = StringUtility::makeInitialName(
                    $activityRS[$rowIndex]['enteredByFirstName'],
                    $activityRS[$rowIndex]['enteredByLastName'],
                    false,
                    LAST_NAME_MAXLEN
                );
            }
        }

        /* Get upcoming calendar entries. */
        $calendarRS = $candidates->getUpcomingEvents($candidateID);
        if (!empty($calendarRS))
        {
            foreach ($calendarRS as $rowIndex => $row)
            {
                $calendarRS[$rowIndex]['enteredByAbbrName'] = StringUtility::makeInitialName(
                    $calendarRS[$rowIndex]['enteredByFirstName'],
                    $calendarRS[$rowIndex]['enteredByLastName'],
                    false,
                    LAST_NAME_MAXLEN
                );
            }
        }

        /* Get extra fields. */
        $extraFieldRS = $candidates->extraFields->getValuesForShow($candidateID);
		//echo "<pre>";
		//print_r($extraFieldRS);
        /* Add an MRU entry. */
        $_SESSION['RESUFLO']->getMRU()->addEntry(
            DATA_ITEM_CANDIDATE, $candidateID, $data['firstName'] . ' ' . $data['lastName']
        );

        /* Is the user an admin - can user see history? */
        if ($this->_accessLevel < ACCESS_LEVEL_DEMO)
        {
            $privledgedUser = false;
        }
        else
        {
            $privledgedUser = true;
        }

        $EEOSettings = new EEOSettings($this->_siteID);
        $EEOSettingsRS = $EEOSettings->getAll();
        $EEOValues = array();

        /* Make a list of all EEO related values so they can be positioned by index
         * rather than static positioning (like extra fields). */
        if ($EEOSettingsRS['enabled'] == 1)
        {
            if ($EEOSettingsRS['genderTracking'] == 1)
            {
                $EEOValues[] = array('fieldName' => 'Gender', 'fieldValue' => $data['eeoGenderText']);
            }
            if ($EEOSettingsRS['ethnicTracking'] == 1)
            {
                $EEOValues[] = array('fieldName' => 'Ethnicity', 'fieldValue' => $data['eeoEthnicType']);
            }
            if ($EEOSettingsRS['veteranTracking'] == 1)
            {
                $EEOValues[] = array('fieldName' => 'Veteran Status', 'fieldValue' => $data['eeoVeteranType']);
            }
            if ($EEOSettingsRS['disabilityTracking'] == 1)
            {
                $EEOValues[] = array('fieldName' => 'Disability Status', 'fieldValue' => $data['eeoDisabilityStatus']);
            }
        }

        $questionnaire = new Questionnaire($this->_siteID);
        $questionnaires = $questionnaire->getCandidateQuestionnaires($candidateID);

        $this->_template->assign('active', $this);
        $this->_template->assign('questionnaires', $questionnaires);
        $this->_template->assign('data', $data);
        $this->_template->assign('isShortNotes', $isShortNotes);
        $this->_template->assign('attachmentsRS', $attachmentsRS);
        $this->_template->assign('pipelinesRS', $pipelinesRS);
        $this->_template->assign('activityRS', $activityRS);
        $this->_template->assign('calendarRS', $calendarRS);
        $this->_template->assign('extraFieldRS', $extraFieldRS);
        $this->_template->assign('candidateID', $candidateID);
        $this->_template->assign('isPopup', $isPopup);
        $this->_template->assign('EEOSettingsRS', $EEOSettingsRS);
        $this->_template->assign('EEOValues', $EEOValues);
        $this->_template->assign('privledgedUser', $privledgedUser);
        $this->_template->assign('sessionCookie', $_SESSION['RESUFLO']->getCookie());
        $this->_template->assign('prevcandid', $prevcandid);
        $this->_template->assign('nextcandid', $nextcandid);
		$this->_template->assign('list', $_REQUEST["list"]);
		$this->_template->assign('candidate_id_view', $candidate_id_view);
        if (!eval(Hooks::get('CANDIDATE_SHOW'))) return;

        $this->_template->display('./modules/candidates/pdf.php');
    }

    //pdf display


    /*
     * Called by handleRequest() to process loading the details page.
     */
    private function show()
    {	
        //apply for pipeline
        if($_REQUEST['list']=="PipeLine"){
            $_REQUEST['a']="PipeLine";
        }
        else if($_REQUEST['list']=="follow"){
            $_REQUEST['a']="follow";
        }
        // Log message that shows up on the top of the list page
        $topLog = '';
        $dataGridProperties = DataGrid::getRecentParamaters("candidates:candidatesListByViewDataGrid");
		//die($dataGridProperties);
        /* If this is the first time we visited the datagrid this session, the recent paramaters will
         * be empty.  Fill in some default values. */
        if ($dataGridProperties == array() || $_REQUEST['a']=="follow" || $_REQUEST['a']=="PipeLine" || (isset($_REQUEST['list']) && intval($_REQUEST['list']) > 0))
        {
            //9999999999 so that filter can fetch all
            $dataGridProperties = array('rangeStart'    => 0,
                                        'maxResults'    => 9999999,
                                        'filterVisible' => false);
        }
        //Used datagrid for next/previous buttons as they are not working on filters
        $dataGrid = DataGrid::get("candidates:candidatesListByViewDataGrid", $dataGridProperties);
        
		$pipelineEntriesPerPage = $_SESSION['RESUFLO']->getPipelineEntriesPerPage();
		$sessionCookie = $_SESSION['RESUFLO']->getCookie();
		$candidateID=$_REQUEST['candidateID'];
		$userid= $_SESSION['RESUFLO']->getuserid();
		
		// code for follow upe
		$follow=$_REQUEST['follow'];		
		if($follow == '1') {
			$follow="update candidate set follow_up = '1' where candidate_id = '$candidateID' ";
			$result = mysql_query($follow);
		}
					
		// code for subscribe/unsubscribe
		$val=$_REQUEST['val'];
		if($val == '0') {
			$subscribe="update candidate set is_hot = '0', ishot = '0',  unsubscribe = '1', follow_up = '0' where candidate_id = '$candidateID' ";
			$unsubscribe_date1 = mysql_query("Select unsubscribe_date from candidate where candidate_id = '$candidateID'");
			$row = mysql_fetch_assoc($unsubscribe_date1);
			if($row['unsubscribe_date'] == '0000-00-00 00:00:00' || $row['unsubscribe_date'] == '' ) {	
				$update_unsubscribe_date = "update candidate set unsubscribe_date = NOW() where candidate_id = '$candidateID' ";
				$result = mysql_query($update_unsubscribe_date);
			}
		}
		else if($val == '1') {
			$subscribe="update candidate set unsubscribe = '0', follow_up = '0', unsubscribe_date = '0000-00-00 00:00:00'  where candidate_id = '$candidateID' ";
		}
		$result = mysql_query($subscribe);
				
		////code foer viewed candidates
		$view=$_REQUEST['view'];
		if($view == '1'){
			$sql = " update candidate set view_date =NOW() where candidate_id = '$candidateID'";
			$result = mysql_query($sql);
		}	
		if($userid == '1') {
			$viewed = "SELECT candidate_id FROM candidate WHERE view_date != '0000-00-00 00:00:00'  ORDER BY view_date desc limit 1,10  ";
		} 
		else {
			$viewed = "SELECT candidate_id FROM candidate where entered_by = '$userid' AND view_date != '0000-00-00 00:00:00' ORDER BY view_date desc limit 1,10  ";
		}
		
		$viewed_candidate = mysql_query($viewed);
		$candidate_id_view=array();				
		while($row = mysql_fetch_array($viewed_candidate)) {
			$candidate_id_view[]=$row['candidate_id'];
		}
				
               if((isset($_REQUEST['list']) && intval($_REQUEST['list']) > 0))
               {
                   $_REQUEST['m'] = 'lists';
                   $_REQUEST['savedListID']=944;
                   $_REQUEST['list']=944;
                   $_REQUEST['a']='showList';
                   $_REQUEST['listrec']='view_all';
                   
                   $_REQUEST['m']='lists';
                   $_REQUEST['a']='showList';
                   $_REQUEST['listrec']='view_all';
                   $_REQUEST['savedListID']=944;
                   
                   $dataGridProperties = array('rangeStart'    => 0,
                                        'maxResults'    => 9999999,
                                        'filterVisible' => false);
               }
		//code for prev and next button
        $dataGridProperties = DataGrid::getRecentParamaters("candidates:candidatesListByViewDataGrid");
        $dataGridFilter = $dataGridProperties['filter'];
        $sortbycand= $dataGridProperties['sortBy'];
        $sortdirectioncand=$dataGridProperties['sortDirection'];
       
        if($sortbycand=="lastName") {
			$sortbycand='last_name';
		}
        if($sortbycand=="firstName") {
			$sortbycand='first_name';
        }
		if($sortbycand=="dateCreatedSort") {
			$sortbycand='date_created';
        }
		if($sortbycand=="dateModifiedSort") {
			$sortbycand='date_modified';
        }
		if($sortbycand=="ownerSort") {
			$sortbycand='owner';
        }
		if($sortbycand=="keySkills") {
            $sortbycand='key_skills';
        }
		if($sortbycand=="company") {
			$sortbycand='entered_by';
		} else{
			$sortbycand = 'date_created';
		}
        
        $userid=$_SESSION['RESUFLO']->getUserID();

		$query="select candidate_id from candidate where entered_by=$userid ORDER BY $sortbycand $sortdirectioncand ";

		if($userid==1) {
			$query="select candidate_id from candidate  ORDER BY $sortbycand $sortdirectioncand ";
		} else if(isset($_REQUEST['list'])){
			$list_id = (integer) $_REQUEST['list'];
			if (!empty($list_id)) {
				$query="select candidate_id from candidate cand, saved_list_entry list where cand.entered_by=$userid AND cand.candidate_id = list.data_item_id AND list.saved_list_id = ".$_REQUEST['list']." ORDER BY cand.$sortbycand $sortdirectioncand ";
			} else {
				switch($_REQUEST['list']) {
					case 'new_candidates':
						$query="select candidate_id from candidate where entered_by=$userid AND follow_up = 0 AND campaign_id = 0 AND list_id = 0 and is_active = 1 and unsubscribe = 0 ORDER BY $sortbycand $sortdirectioncand ";
					break;
				}
			}
		}

        
		$result = mysql_query($query);
//        Next and previous was not working for filter so disabled that and used datagrid to apply filter        
//		$dataarray=array();
//		while($row = mysql_fetch_array($result)) {
//			$dataarray[]=$row['candidate_id'];
//		}
        $dataarray =  $dataGrid->candidateList();
        $candid=$_REQUEST['candidateID'];
        $key= array_search($candid,$dataarray);
        $prev=$key-1;
        $next=$key+1;
		
		$prevcandid=$dataarray[$prev];
		$nextcandid=$dataarray[$next];
		//  echo "<pre>";print_r($dataarray);
		
		//code for prev and next button
		$_accessLevel = $this->_accessLevel;	
		$this->_template->assign('recipients', $_accessLevel);
        /* Is this a popup? */
        if (isset($_GET['display']) && $_GET['display'] == 'popup') {
            $isPopup = true;
        }
        else {
            $isPopup = false;
        }

        /* Bail out if we don't have a valid candidate ID. */
        if (!$this->isRequiredIDValid('candidateID', $_GET) && !isset($_GET['email'])) {
            CommonErrors::fatal(COMMONERROR_BADINDEX, $this, 'Invalid candidate ID.');
        }

        $candidates = new Candidates($this->_siteID);

        if (isset($_GET['candidateID'])) {
            $candidateID = $_GET['candidateID'];
        }
        else {
            $candidateID = $candidates->getIDByEmail($_GET['email']);
        }

        $data = $candidates->get($candidateID);
        $data['camp_name'] = CandidatesUI::get_campaign_name($data['camp_id'], $userid);

        /* Bail out if we got an empty result set. */
        if (empty($data))
        {
            CommonErrors::fatal(COMMONERROR_BADINDEX, $this, 'The specified candidate ID could not be found.');
            return;
        }

        if ($data['isAdminHidden'] == 1 && $this->_accessLevel < ACCESS_LEVEL_MULTI_SA)
        {
            $this->listByView('This candidate is hidden - only a RESUFLO Administrator can unlock the candidate.');
            return;
        }

        /* We want to handle formatting the city and state here instead
         * of in the template.
         */
        $data['cityAndState'] = StringUtility::makeCityStateString(
            $data['city'], $data['state']
        );

        /*
         * Replace newlines with <br />, fix HTML "special" characters, and
         * strip leading empty lines and spaces.
         */
        $data['notes'] = trim(
            nl2br(htmlspecialchars($data['notes'], ENT_QUOTES))
        );

        /* Chop $data['notes'] to make $data['shortNotes']. */
        if (strlen($data['notes']) > self::NOTES_MAXLEN)
        {
            $data['shortNotes']  = substr(
                $data['notes'], 0, self::NOTES_MAXLEN
            );
            $isShortNotes = true;
        }
        else
        {
            $data['shortNotes'] = $data['notes'];
            $isShortNotes = false;
        }

        /* Format "can relocate" status. */
        if ($data['canRelocate'] == 1)
        {
            $data['canRelocate'] = 'Yes';
        }
        else
        {
            $data['canRelocate'] = 'No';
        }

        if ($data['isHot'] == 1)
        {
            $data['titleClass'] = 'jobTitleHot';
        }
        else
        {
            $data['titleClass'] = 'jobTitleCold';
        }

        $attachments = new Attachments($this->_siteID);
        $attachmentsRS = $attachments->getAll(
            DATA_ITEM_CANDIDATE, $candidateID
        );

        foreach ($attachmentsRS as $rowNumber => $attachmentsData)
        {
            /* If profile image is not local, force it to be local. */
            if ($attachmentsData['isProfileImage'] == 1)
            {
                $attachments->forceAttachmentLocal($attachmentsData['attachmentID']);
            }

            /* Show an attachment icon based on the document's file type. */
            $attachmentIcon = strtolower(
                FileUtility::getAttachmentIcon(
                    $attachmentsRS[$rowNumber]['originalFilename']
                )
            );

            $attachmentsRS[$rowNumber]['attachmentIcon'] = $attachmentIcon;

            /* If the text field has any text, show a preview icon. */
            if ($attachmentsRS[$rowNumber]['hasText'])
            {
                $attachmentsRS[$rowNumber]['previewLink'] = sprintf(
                    '<a href="#" onclick="window.open(\'%s?m=candidates&amp;a=viewResume&amp;attachmentID=%s\', \'viewResume\', \'scrollbars=1,width=800,height=760\')"><img width="15" height="15" style="border: none;" src="images/search.gif" alt="(Preview)" /></a>',
                    RESUFLOUtility::getIndexName(),
                    $attachmentsRS[$rowNumber]['attachmentID']
                );
            }
            else
            {
                $attachmentsRS[$rowNumber]['previewLink'] = '&nbsp;';
            }
        }
        $pipelines = new Pipelines($this->_siteID);
        $pipelinesRS = $pipelines->getCandidatePipeline($candidateID);
        $sessionCookie = $_SESSION['RESUFLO']->getCookie();

        /* Format pipeline data. */

        //die('<pre>'.print_r($pipelinesRS,true));
        //jobLinkCold in CandidatesUI::show()

        foreach ($pipelinesRS as $rowIndex => $row)
        {
			$pipelinesRS[$rowIndex]['status'] = $pipelines->getJobOrderStatusName($row['jobOrderID'],$row['status']);
            /* Hot jobs [can] have different title styles than normal
             * jobs.
             */
            if ($row['isHot'] == 1)
            {
                $pipelinesRS[$rowIndex]['linkClass'] = 'jobLinkHot';
            }
            else
            {
                $pipelinesRS[$rowIndex]['linkClass'] = 'jobLinkCold';
            }

            $pipelinesRS[$rowIndex]['ownerAbbrName'] = StringUtility::makeInitialName(
                $pipelinesRS[$rowIndex]['ownerFirstName'],
                $pipelinesRS[$rowIndex]['ownerLastName'],
                false,
                LAST_NAME_MAXLEN
            );

            $pipelinesRS[$rowIndex]['addedByAbbrName'] = StringUtility::makeInitialName(
                $pipelinesRS[$rowIndex]['addedByFirstName'],
                $pipelinesRS[$rowIndex]['addedByLastName'],
                false,
                LAST_NAME_MAXLEN
            );

            $pipelinesRS[$rowIndex]['ratingLine'] = TemplateUtility::getRatingObject(
                $pipelinesRS[$rowIndex]['ratingValue'],
                $pipelinesRS[$rowIndex]['candidateJobOrderID'],
                $sessionCookie
            );
        }

        $activityEntries = new ActivityEntries($this->_siteID);
        $activityRS = $activityEntries->getAllByDataItem($candidateID, DATA_ITEM_CANDIDATE, $userid);
        if (!empty($activityRS))
        {
            foreach ($activityRS as $rowIndex => $row)
            {
                if (empty($activityRS[$rowIndex]['notes']))
                {
                    $activityRS[$rowIndex]['notes'] = '(No Notes)';
                }

                if (empty($activityRS[$rowIndex]['jobOrderID']) ||
                    empty($activityRS[$rowIndex]['regarding']))
                {
                    $activityRS[$rowIndex]['regarding'] = 'General';
                }

                $activityRS[$rowIndex]['enteredByAbbrName'] = StringUtility::makeInitialName(
                    $activityRS[$rowIndex]['enteredByFirstName'],
                    $activityRS[$rowIndex]['enteredByLastName'],
                    false,
                    LAST_NAME_MAXLEN
                );
            }
        }

        /* Get upcoming calendar entries. */
        $calendarRS = $candidates->getUpcomingEvents($candidateID);
        /* Get reminder, thanks and phone reminder Events*/
        $calendarReminderThanksPhoneRS = $candidates->getReminderThanksPhoneEvents($candidateID);
        if (!empty($calendarRS))
        {
            foreach ($calendarRS as $rowIndex => $row)
            {
                $calendarRS[$rowIndex]['enteredByAbbrName'] = StringUtility::makeInitialName(
                    $calendarRS[$rowIndex]['enteredByFirstName'],
                    $calendarRS[$rowIndex]['enteredByLastName'],
                    false,
                    LAST_NAME_MAXLEN
                );
            }
        }

        /* Get extra fields. */
        $extraFieldRS = $candidates->extraFields->getValuesForShow($candidateID);
		//echo "<pre>";
		//print_r($extraFieldRS);
        /* Add an MRU entry. */
        $_SESSION['RESUFLO']->getMRU()->addEntry(
            DATA_ITEM_CANDIDATE, $candidateID, $data['firstName'] . ' ' . $data['lastName']
        );

        /* Is the user an admin - can user see history? */
        if ($this->_accessLevel < ACCESS_LEVEL_DEMO)
        {
            $privledgedUser = false;
        }
        else
        {
            $privledgedUser = true;
        }

        $EEOSettings = new EEOSettings($this->_siteID);
        $EEOSettingsRS = $EEOSettings->getAll();
        $EEOValues = array();

        /* Make a list of all EEO related values so they can be positioned by index
         * rather than static positioning (like extra fields). */
        if ($EEOSettingsRS['enabled'] == 1)
        {
            if ($EEOSettingsRS['genderTracking'] == 1)
            {
                $EEOValues[] = array('fieldName' => 'Gender', 'fieldValue' => $data['eeoGenderText']);
            }
            if ($EEOSettingsRS['ethnicTracking'] == 1)
            {
                $EEOValues[] = array('fieldName' => 'Ethnicity', 'fieldValue' => $data['eeoEthnicType']);
            }
            if ($EEOSettingsRS['veteranTracking'] == 1)
            {
                $EEOValues[] = array('fieldName' => 'Veteran Status', 'fieldValue' => $data['eeoVeteranType']);
            }
            if ($EEOSettingsRS['disabilityTracking'] == 1)
            {
                $EEOValues[] = array('fieldName' => 'Disability Status', 'fieldValue' => $data['eeoDisabilityStatus']);
            }
        }

        $questionnaire = new Questionnaire($this->_siteID);
        $questionnaires = $questionnaire->getCandidateQuestionnaires($candidateID);
        $users = new Users($this->_siteID);
        $userSearch = $users->get($this->_userID);
        
        $this->_template->assign('active', $this);
        $this->_template->assign('questionnaires', $questionnaires);
        $this->_template->assign('data', $data);
        $this->_template->assign('isShortNotes', $isShortNotes);
        $this->_template->assign('attachmentsRS', $attachmentsRS);
        $this->_template->assign('pipelinesRS', $pipelinesRS);
        $this->_template->assign('activityRS', $activityRS);
        $this->_template->assign('calendarRS', $calendarRS);
        $this->_template->assign('calendarReminderThanksPhoneRS', $calendarReminderThanksPhoneRS);
        $this->_template->assign('extraFieldRS', $extraFieldRS);
        $this->_template->assign('candidateID', $candidateID);
        $this->_template->assign('isPopup', $isPopup);
        $this->_template->assign('EEOSettingsRS', $EEOSettingsRS);
        $this->_template->assign('EEOValues', $EEOValues);
        $this->_template->assign('privledgedUser', $privledgedUser);
        $this->_template->assign('pipelineEntriesPerPage', $pipelineEntriesPerPage);
        $this->_template->assign('sessionCookie', $_SESSION['RESUFLO']->getCookie());
        $this->_template->assign('list', $_REQUEST["list"]);
        $this->_template->assign('prevcandid', $prevcandid);
        $this->_template->assign('nextcandid', $nextcandid);
        $this->_template->assign('candidate_id_view', $candidate_id_view);
        $this->_template->assign('dataGridFilter', $dataGridFilter);
        $this->_template->assign('searchCriteria', $userSearch["searchCriteria"]);
        $this->_template->assign('isTextMessageEnable', $userSearch["isTextMessageEnable"]);
        //var_dump($_SESSION['RESUFLO']);
        $session = (array)$_SESSION['RESUFLO'];
        //var_dump($session);
        //print_r((array)$_SESSION['RESUFLO']);
        $listId = $_GET["list"];
        if(!empty($listId)){
            $dataGridSavedListProperties = DataGrid::getRecentParamaters("candidates:candidatesSavedListByViewDataGrid", $listId);
            //var_dump($dataGridSavedListProperties['filter']);
            $this->_template->assign('listId', $listId);
            $this->_template->assign('dataGridSavedListFilter', $dataGridSavedListProperties['filter']);
        }
        
        if (!eval(Hooks::get('CANDIDATE_SHOW'))) return;

        $this->_template->display('./modules/candidates/Show.tpl');
    }

    /*
     * Called by handleRequest() to process loading the add page.
     *
     * The user could have already added a resume to the system
     * before this page is displayed.  They could have indicated
     * that they want to use a bulk resume, or a text resume
     * stored in the  session.  These ocourances are looked
     * for here, and the Add.tpl file displays the results.
     */
    private function add($contents = '', $fields = array())
    {

      
      $candidates = new Candidates($this->_siteID);
		
        /* Get possible sources. */
        $sourcesRS = $candidates->getPossibleSources();
        $sourcesString = ListEditor::getStringFromList($sourcesRS, 'name');
		
        /* Get extra fields. */
        $extraFieldRS = $candidates->extraFields->getValuesForAdd();

        /* Get passed variables. */
        $preassignedFields = $_GET;
		
        if (count($fields) > 0)
        {
            $preassignedFields = array_merge($preassignedFields, $fields);
        }

        /* Get preattached resume, if any. */
        if ($this->isRequiredIDValid('attachmentID', $_GET))
        {
            $associatedAttachment = $_GET['attachmentID'];

            $attachments = new Attachments($this->_siteID);
            $associatedAttachmentRS = $attachments->get($associatedAttachment);

            /* Show an attachment icon based on the document's file type. */
            $attachmentIcon = strtolower(
                FileUtility::getAttachmentIcon(
                    $associatedAttachmentRS['originalFilename']
                )
            );

            $associatedAttachmentRS['attachmentIcon'] = $attachmentIcon;

            /* If the text field has any text, show a preview icon. */
            if ($associatedAttachmentRS['hasText'])
            {
                $associatedAttachmentRS['previewLink'] = sprintf(
                    '<a href="#" onclick="window.open(\'%s?m=candidates&amp;a=viewResume&amp;attachmentID=%s\', \'viewResume\', \'scrollbars=1,width=800,height=760\')"><img width="15" height="15" style="border: none;" src="images/popup.gif" alt="(Preview)" /></a>',
                    RESUFLOUtility::getIndexName(),
                    $associatedAttachmentRS['attachmentID']
                );
            }
            else
            {
                $associatedAttachmentRS['previewLink'] = '&nbsp;';
            }
        }
        else
        {
            $associatedAttachment = 0;
            $associatedAttachmentRS = array();
        }

        /* Get preuploaded resume text, if any */
        if ($this->isRequiredIDValid('resumeTextID', $_GET, true))
        {
            $associatedTextResume = $_SESSION['RESUFLO']->retrieveData($_GET['resumeTextID']);
        }
        else
        {
            $associatedTextResume = false;
        }

        /* Get preuploaded resume file (unattached), if any */
        if ($this->isRequiredIDValid('resumeFileID', $_GET, true))
        {
            $associatedFileResume = $_SESSION['RESUFLO']->retrieveData($_GET['resumeFileID']);
            $associatedFileResume['id'] = $_GET['resumeFileID'];
            $associatedFileResume['attachmentIcon'] = strtolower(
                FileUtility::getAttachmentIcon(
                    $associatedFileResume['filename']
                )
            );
        }
        else
        {
            $associatedFileResume = false;
        }

        $EEOSettings = new EEOSettings($this->_siteID);
        $EEOSettingsRS = $EEOSettings->getAll();


        if (!eval(Hooks::get('CANDIDATE_ADD'))) return;

        /* If parsing is not enabled server-wide, say so. */
        if (!LicenseUtility::isParsingEnabled())
        {
            $isParsingEnabled = false;
        }
        /* For RESUFLO Toolbar, if e-mail has been sent and it wasn't set by
         * parser, it's toolbar and it needs the old format.
         */
        else if (!isset($preassignedFields['email']))
        {
            $isParsingEnabled = true;
        }
        else if (empty($preassignedFields['email']))
        {
            $isParsingEnabled = true;
        }
        else if (isset($preassignedFields['isFromParser']) && $preassignedFields['isFromParser'])
        {
            $isParsingEnabled = true;
        }
        else
        {
            $isParsingEnabled = false;
        }

        if (is_array($parsingStatus = LicenseUtility::getParsingStatus()) &&
            isset($parsingStatus['parseLimit']))
        {
            $parsingStatus['parseLimit'] = $parsingStatus['parseLimit'] - 1;
        }

        $this->_template->assign('parsingStatus', $parsingStatus);
        $this->_template->assign('isParsingEnabled', $isParsingEnabled);
        $this->_template->assign('contents', $contents);
        $this->_template->assign('extraFieldRS', $extraFieldRS);
        $this->_template->assign('active', $this);
        $this->_template->assign('subActive', 'Add Candidate');
        $this->_template->assign('sourcesRS', $sourcesRS);
        $this->_template->assign('sourcesString', $sourcesString);
        $this->_template->assign('preassignedFields', $preassignedFields);
        $this->_template->assign('associatedAttachment', $associatedAttachment);
        $this->_template->assign('associatedAttachmentRS', $associatedAttachmentRS);
        $this->_template->assign('associatedTextResume', $associatedTextResume);
        $this->_template->assign('associatedFileResume', $associatedFileResume);
        $this->_template->assign('EEOSettingsRS', $EEOSettingsRS);
        $this->_template->assign('isModal', false);

        
        

        /* REMEMBER TO ALSO UPDATE JobOrdersUI::addCandidateModal() IF
         * APPLICABLE.
         */
		 
        $this->_template->display('./modules/candidates/Add.tpl');
    }

    public function checkParsingFunctions()
    {
        if (LicenseUtility::isParsingEnabled())
        {
            if (isset($_POST['documentText'])) $contents = $_POST['documentText'];
            else $contents = '';

            // Retain all field data since this isn't done over AJAX (yet)
            $fields = array(
                'firstName'       => $this->getTrimmedInput('firstName', $_POST),
                'middleName'      => $this->getTrimmedInput('middleName', $_POST),
                'lastName'        => $this->getTrimmedInput('lastName', $_POST),
                'email1'          => $this->getTrimmedInput('email1', $_POST),
                'email2'          => $this->getTrimmedInput('email2', $_POST),
                'phoneHome'       => $this->getTrimmedInput('phoneHome', $_POST),
                'phoneCell'       => $this->getTrimmedInput('phoneCell', $_POST),
                'phoneWork'       => $this->getTrimmedInput('phoneWork', $_POST),
                'address'         => $this->getTrimmedInput('address', $_POST),
                'city'            => $this->getTrimmedInput('city', $_POST),
                'state'           => $this->getTrimmedInput('state', $_POST),
                'zip'             => $this->getTrimmedInput('zip', $_POST),
                'source'          => $this->getTrimmedInput('source', $_POST),
                'keySkills'       => $this->getTrimmedInput('keySkills', $_POST),
                'currentEmployer' => $this->getTrimmedInput('currentEmployer', $_POST),
                'currentPay'      => $this->getTrimmedInput('currentPay', $_POST),
                'desiredPay'      => $this->getTrimmedInput('desiredPay', $_POST),
                'notes'           => $this->getTrimmedInput('notes', $_POST),
                'canRelocate'     => $this->getTrimmedInput('canRelocate', $_POST),
                'webSite'         => $this->getTrimmedInput('webSite', $_POST),
                'bestTimeToCall'  => $this->getTrimmedInput('bestTimeToCall', $_POST),
                'gender'          => $this->getTrimmedInput('gender', $_POST),
                'race'            => $this->getTrimmedInput('race', $_POST),
                'veteran'         => $this->getTrimmedInput('veteran', $_POST),
                'disability'      => $this->getTrimmedInput('disability', $_POST),
				'lifeLic'         => $this->getTrimmedInput('lifeLic', $_POST),
				'propLic'         => $this->getTrimmedInput('propLic', $_POST),
				'ser6'            => $this->getTrimmedInput('ser6', $_POST),
				'ser63'           => $this->getTrimmedInput('ser63', $_POST),
				'ser7'            => $this->getTrimmedInput('ser7', $_POST),
                'documentTempFile'=> $this->getTrimmedInput('documentTempFile', $_POST),
                'isFromParser'    => true
            );

            /**
             * User is loading a resume from a document. Convert it to a string and paste the contents
             * into the textarea field on the add candidate page after validating the form.
             */
            if (isset($_POST['loadDocument']) && $_POST['loadDocument'] == 'true')
            {
                // Get the upload file from the post data
                $newFileName = FileUtility::getUploadFileFromPost(
                    $this->_siteID, // The site ID
                    'addcandidate', // Sub-directory of the site's upload folder
                    'documentFile'  // The DOM "name" from the <input> element
                );

                if ($newFileName !== false)
                {
                    // Get the relative path to the file (to perform operations on)
                    $newFilePath = FileUtility::getUploadFilePath(
                        $this->_siteID, // The site ID
                        'addcandidate', // The sub-directory
                        $newFileName
                    );

                    $documentToText = new DocumentToText();
                    $doctype = $documentToText->getDocumentType($newFilePath);

                    if ($documentToText->convert($newFilePath, $doctype))
                    {
                        $contents = $documentToText->getString();
                        if ($doctype == DOCUMENT_TYPE_DOC)
                        {
                            $contents = str_replace('|', "\n", $contents);
                        }

                        // Remove things like _rDOTr for ., etc.
                        $contents = DatabaseSearch::fulltextDecode($contents);
                    }
                    else
                    {
                        $contents = @file_get_contents($newFilePath);
                        $fields['binaryData'] = true;
                    }

                    // Save the short (un-pathed) name
                    $fields['documentTempFile'] = $newFileName;

                    if (isset($_COOKIE['RESUFLO_SP_TEMP_FILE']) && ($oldFile = $_COOKIE['RESUFLO_SP_TEMP_FILE']) != '' &&
                        strcasecmp($oldFile, $newFileName))
                    {
                        // Get the safe, old file they uploaded and didn't use (if exists) and delete
                        $oldFilePath = FileUtility::getUploadFilePath($this->_siteID, 'addcandidate', $oldFile);

                        if ($oldFilePath !== false)
                        {
                            @unlink($oldFilePath);
                        }
                    }

                    // Prevent users from creating more than 1 temp file for single parsing (sp)
                    setcookie('RESUFLO_SP_TEMP_FILE', $newFileName, time() + (60*60*24*7));
                }

                if (isset($_POST['parseDocument']) && $_POST['parseDocument'] == 'true' && $contents != '')
                {
                    // ...
                }
                else
                {
                    return array($contents, $fields);
                }
            }

            /**
             * User is parsing the contents of the textarea field on the add candidate page.
             */
            if (isset($_POST['parseDocument']) && $_POST['parseDocument'] == 'true' && $contents != '')
            {
                $pu = new ParseUtility();
                if ($res = $pu->documentParse('untitled', strlen($contents), '', $contents))
                {
                    if (isset($res['first_name'])) $fields['firstName'] = $res['first_name']; else $fields['firstName'] = '';
                    if (isset($res['last_name'])) $fields['lastName'] = $res['last_name']; else $fields['lastName'] = '';
                    $fields['middleName'] = '';
                    if (isset($res['email_address'])) $fields['email1'] = $res['email_address']; else $fields['email1'] = '';
                    $fields['email2'] = '';
                    if (isset($res['us_address'])) $fields['address'] = $res['us_address']; else $fields['address'] = '';
                    if (isset($res['city'])) $fields['city'] = $res['city']; else $fields['city'] = '';
                    if (isset($res['state'])) $fields['state'] = $res['state']; else $fields['state'] = '';
                    if (isset($res['zip_code'])) $fields['zip'] = $res['zip_code']; else $fields['zip'] = '';
                    if (isset($res['phone_number'])) $fields['phoneHome'] = $res['phone_number']; else $fields['phoneHome'] = '';
                    $fields['phoneWork'] = $fields['phoneCell'] = '';
                    if (isset($res['skills'])) $fields['keySkills'] = str_replace("\n", ' ', str_replace('"', '\'\'', $res['skills']));
                }

                return array($contents, $fields);
            }
        }

        return false;
    }

    /*
     * Called by handleRequest() to process saving / submitting the add page.
     */
    private function onAdd()
    {
        if ($this->_accessLevel < ACCESS_LEVEL_EDIT)
        {
            CommonErrors::fatal(COMMONERROR_PERMISSION, $this, 'Invalid user level for action.');
        }

        if (is_array($mp = $this->checkParsingFunctions()))
        {
            return $this->add($mp[0], $mp[1]);
        }

      $candidateID = $this->_addCandidate(false);
    
        if($candidateID==-2)
        {
              RESUFLOUtility::transferRelativeURI(
            'm=candidates&a=add&er=exists'
        );
        }
		if($candidateID==-3)
        {
              RESUFLOUtility::transferRelativeURI(
            'm=candidates&a=add&er=optout'
        );
        }
		if($candidateID==-4)
        {
              RESUFLOUtility::transferRelativeURI(
            'm=candidates&a=add&er=invalid'
        );
        }
		if($_REQUEST["list"]!='') {
			if($candidateID == '$candidate_id')
			{
				  RESUFLOUtility::transferRelativeURI(
				'm=candidates&a=show&candidateID=' . $candidateID.'&list=' . $_REQUEST["list"]
			);
			}
		}
		else
		{
			if($candidateID == '$candidate_id')
			{
				  RESUFLOUtility::transferRelativeURI(
				'm=candidates&a=show&candidateID=' . $candidateID
			);
			}
		}		
		
        if ($candidateID <= 0)
        {
            CommonErrors::fatal(COMMONERROR_RECORDERROR, $this, 'Failed to add candidate.');
        }
    

		if($_REQUEST["list"]!='') {
			RESUFLOUtility::transferRelativeURI(
				'm=candidates&a=show&candidateID=' . $candidateID.'&list=' . $_REQUEST["list"]
			);
		}
		else
		{
                    //Need to fix this
                    //echo 'Hi';
                    $url = 'm=candidates&a=show&candidateID=' . $candidateID;
                    //echo $url;
                    try{
                    RESUFLOUtility::transferRelativeURI($url);
                    }catch (Exception $e) {
                        $error = ' GENERAL ERROR:'.$e->getMessage()."\n";
                        echo $error;
                    }
                    //echo $url;
		}		
    }

    /*
     * Called by handleRequest() to process loading the edit page.
     */
    private function edit()
    {
        /* Bail out if we don't have a valid candidate ID. */
        if (!$this->isRequiredIDValid('candidateID', $_GET))
        {
            CommonErrors::fatal(COMMONERROR_BADINDEX, $this, 'Invalid candidate ID.');
        }

        $candidateID = $_GET['candidateID'];
		$list = $_GET['list'];

        $candidates = new Candidates($this->_siteID);
        $data = $candidates->getForEditing($candidateID);

        /* Bail out if we got an empty result set. */
        if (empty($data))
        {
            CommonErrors::fatal(COMMONERROR_BADINDEX, $this, 'The specified candidate ID could not be found.');
        }

        if ($data['isAdminHidden'] == 1 && $this->_accessLevel < ACCESS_LEVEL_MULTI_SA)
        {
            $this->listByView('This candidate is hidden - only a RESUFLO Administrator can unlock the candidate.');
            return;
        }

        $users = new Users($this->_siteID);
        $usersRS = $users->getSelectList('yes');

        /* Add an MRU entry. */
        $_SESSION['RESUFLO']->getMRU()->addEntry(
            DATA_ITEM_CANDIDATE, $candidateID, $data['firstName'] . ' ' . $data['lastName']
        );

        /* Get extra fields. */
        $extraFieldRS = $candidates->extraFields->getValuesForEdit($candidateID);

        /* Get possible sources. */
        $sourcesRS = $candidates->getPossibleSources();
        $sourcesString = ListEditor::getStringFromList($sourcesRS, 'name');

        /* Is current source a possible source? */
        // FIXME: Use array search functions!
        $sourceInRS = false;
        foreach ($sourcesRS as $sourceData)
        {
            if ($sourceData['name'] == $data['source'])
            {
                $sourceInRS = true;
            }
        }

        if ($this->_accessLevel == ACCESS_LEVEL_DEMO)
        {
            $canEmail = false;
        }
        else
        {
            $canEmail = true;
        }

        $emailTemplates = new EmailTemplates($this->_siteID);
        $statusChangeTemplateRS = $emailTemplates->getByTag(
            'EMAIL_TEMPLATE_OWNERSHIPASSIGNCANDIDATE'
        );
        if ($statusChangeTemplateRS['disabled'] == 1)
        {
            $emailTemplateDisabled = true;
        }
        else
        {
            $emailTemplateDisabled = false;
        }

        /* Date format for DateInput()s. */
        if ($_SESSION['RESUFLO']->isDateDMY())
        {
            $data['dateAvailableMDY'] = DateUtility::convert(
                '-', $data['dateAvailable'], DATE_FORMAT_DDMMYY, DATE_FORMAT_MMDDYY
            );
        }
        else			
        {				
            $data['dateAvailableMDY'] = $data['dateAvailable'];
        }

        if (!eval(Hooks::get('CANDIDATE_EDIT'))) return;

        $EEOSettings = new EEOSettings($this->_siteID);
        $EEOSettingsRS = $EEOSettings->getAll();

        $this->_template->assign('active', $this);
        $this->_template->assign('data', $data);
        $this->_template->assign('usersRS', $usersRS);
        $this->_template->assign('extraFieldRS', $extraFieldRS);
        $this->_template->assign('sourcesRS', $sourcesRS);
        $this->_template->assign('sourcesString', $sourcesString);
        $this->_template->assign('sourceInRS', $sourceInRS);
        $this->_template->assign('candidateID', $candidateID);
        $this->_template->assign('canEmail', $canEmail);
        $this->_template->assign('EEOSettingsRS', $EEOSettingsRS);
        $this->_template->assign('emailTemplateDisabled', $emailTemplateDisabled);
        $this->_template->display('./modules/candidates/Edit.tpl');
    }
    private function sms()
    {
        /* Bail out if we don't have a valid candidate ID. */
        if (!$this->isRequiredIDValid('candidateID', $_GET))
        {
            CommonErrors::fatal(COMMONERROR_BADINDEX, $this, 'Invalid candidate ID.');
        }

        $candidateID = $_GET['candidateID'];
        $list = $_GET['list'];

        $candidates = new Candidates($this->_siteID);
        $currentCandidate = $candidates->get($candidateID);
        $data = $candidates->getSms($candidateID, $this->_userID);
        $users = new Users($this->_siteID);
        $usersRS = $users->getSelectList('yes');

        if (!eval(Hooks::get('CANDIDATE_EDIT'))) return;
        $this->_template->assign('active', $this);
        $this->_template->assign('data', $data);
        $this->_template->assign('usersRS', $usersRS);
        $this->_template->assign('CandidatePhoneCell', $currentCandidate['phoneCell']);
        $this->_template->assign('candidateID', $candidateID);
        $this->_template->display('./modules/candidates/Sms.tpl');
    }
    private function onSendSms(){
        return true;
    }
    private function smsapp()
    {
        $candidateID = $_GET['candidateID'];
        $Search = $_GET['Search'];
        $IsRead = $_GET['IsRead'];
        $list = $_GET['list'];
        $SmsLog = new SmsLog($this->_userID, $this->_siteID);
        if(isset($_GET['msgid']) && !empty(isset($_GET['msgid']))){
            $userMailer = new UserMailer($this->_userID, $this->_siteID);
            $userMailer->ResendSms($_GET['msgid']);
        }

        $Page = isset($_GET['Page'])? $_GET['Page'] : 1;
        $SmsLogList = $SmsLog->GetSmsLogByUserId($this->_userID, $Search,$IsRead, $Page);
        $SmsLogListCount = $SmsLog->GetSmsLogByUserIdCount($this->_userID, $Search, $IsRead);
        
        if($candidateID == null && count($SmsLogList) > 0){
            $candidateID = $SmsLogList[0]['CandidateId'];
        }
        $candidates = new Candidates($this->_siteID);
        $currentCandidate = $candidates->get($candidateID);
        $data = $candidates->getSms($candidateID, $this->_userID);
        $users = new Users($this->_siteID);
        $usersRS = $users->getSelectList('yes');
        

        if (!eval(Hooks::get('CANDIDATE_EDIT'))) return;
        $this->_template->assign('active', $this);
        $this->_template->assign('data', $data);
        $this->_template->assign('SmsLogList', $SmsLogList);
        $this->_template->assign('Page', $Page);
        $this->_template->assign('SmsLogListCount', $SmsLogListCount[0]['Count']);
        $this->_template->assign('usersRS', $usersRS);
        $this->_template->assign('CandidatePhoneCell', $currentCandidate['phoneCell']);
        $this->_template->assign('CandidateName', $currentCandidate['firstName'].'&nbsp;'.$currentCandidate['lastName']);
        $this->_template->assign('unsubscribe', $currentCandidate['unsubscribe']);
        $this->_template->assign('candidateID', $candidateID);
        $this->_template->assign('NavLink', 'index.php?m=candidates&amp;a=smsapp&amp;candidateID='.$_GET['candidateID'].'&amp;Search='.$_GET['Search'].'&amp;IsRead='.$_GET['IsRead']);
        $this->_template->display('./modules/candidates/SmsApp.tpl');
    }

    /*
     * Called by handleRequest() to process saving / submitting the edit page.
     */
    private function onEdit()
    {
        if ($this->_accessLevel < ACCESS_LEVEL_EDIT)
        {
            CommonErrors::fatal(COMMONERROR_PERMISSION, $this, 'Invalid user level for action.');
        }

        $candidates = new Candidates($this->_siteID);

        /* Bail out if we don't have a valid candidate ID. */
        if (!$this->isRequiredIDValid('candidateID', $_POST))
        {
            CommonErrors::fatalModal(COMMONERROR_BADINDEX, $this, 'Invalid candidate ID.');
            return;
        }

        /* Bail out if we don't have a valid owner user ID. */
        if (!$this->isOptionalIDValid('owner', $_POST))
        {
            CommonErrors::fatal(COMMONERROR_BADINDEX, $this, 'Invalid owner user ID.');
        }

        /* Bail out if we received an invalid availability date; if not, go
         * ahead and convert the date to MySQL format.
         */
        $dateAvailable = $this->getTrimmedInput('dateAvailable', $_POST);
        if (!empty($dateAvailable))
        {
            if (!DateUtility::validate('-', $dateAvailable, DATE_FORMAT_MMDDYY))
            {
                CommonErrors::fatal(COMMONERROR_MISSINGFIELDS, $this, 'Invalid availability date.');
            }

            /* Convert start_date to something MySQL can understand. */
            $dateAvailable = DateUtility::convert(
                '-', $dateAvailable, DATE_FORMAT_MMDDYY, DATE_FORMAT_YYYYMMDD
            );
        }
        
        $scheduledDate = $this->getTrimmedInput('scheduledDate', $_POST);
        if (!empty($scheduledDate))
        {
//            if (!DateUtility::validate('-', $scheduledDate, DATE_FORMAT_YYYYMMDD))
//            {
//                CommonErrors::fatal(COMMONERROR_MISSINGFIELDS, $this, 'Invalid scheduled date.');
//            }
        }

        $formattedPhoneHome = StringUtility::extractPhoneNumber(
            $this->getTrimmedInput('phoneHome', $_POST)
        );
        if (!empty($formattedPhoneHome))
        {
            $phoneHome = $formattedPhoneHome;
        }
        else
        {
            $phoneHome = $this->getTrimmedInput('phoneHome', $_POST);
        }

        $formattedPhoneCell = StringUtility::extractPhoneNumber(
            $this->getTrimmedInput('phoneCell', $_POST)
        );
        if (!empty($formattedPhoneCell))
        {
            $phoneCell = $formattedPhoneCell;
        }
        else
        {
            $phoneCell = $this->getTrimmedInput('phoneCell', $_POST);
        }

        $formattedPhoneWork = StringUtility::extractPhoneNumber(
            $this->getTrimmedInput('phoneWork', $_POST)
        );
        if (!empty($formattedPhoneWork))
        {
            $phoneWork = $formattedPhoneWork;
        }
        else
        {
            $phoneWork = $this->getTrimmedInput('phoneWork', $_POST);
        }

        $candidateID = $_POST['candidateID'];
        $owner       = $_POST['owner'];
		if($owner!='$this->_userID'){
			$enteredBy = $owner;
		}
		else{
			$enteredBy = $this->_userID;
		}

        /* Can Relocate */
        $canRelocate = $this->isChecked('canRelocate', $_POST);

        $isHot = $this->isChecked('isHot', $_POST);

        /* Change ownership email? */
        if ($this->isChecked('ownershipChange', $_POST) && $owner > 0)
        {
            $candidateDetails = $candidates->get($candidateID);

            $users = new Users($this->_siteID);
            $ownerDetails = $users->get($owner);

            if (!empty($ownerDetails))
            {
                $emailAddress = $ownerDetails['email'];

                /* Get the change status email template. */
                $emailTemplates = new EmailTemplates($this->_siteID);
                $statusChangeTemplateRS = $emailTemplates->getByTag(
                    'EMAIL_TEMPLATE_OWNERSHIPASSIGNCANDIDATE'
                );

                if (empty($statusChangeTemplateRS) ||
                    empty($statusChangeTemplateRS['textReplaced']))
                {
                    $statusChangeTemplate = '';
                }
                else
                {
                    $statusChangeTemplate = $statusChangeTemplateRS['textReplaced'];
                }
                /* Replace e-mail template variables. */
                $stringsToFind = array(
                    '%CANDOWNER%',
                    '%CANDFIRSTNAME%',
                    '%CANDFULLNAME%',
                    '%CANDRESUFLOURL%'
                );
                $replacementStrings = array(
                    $ownerDetails['fullName'],
                    $candidateDetails['firstName'],
                    $candidateDetails['firstName'] . ' ' . $candidateDetails['lastName'],
                    '<a href="http://' . $_SERVER['HTTP_HOST'] . substr($_SERVER['REQUEST_URI'], 0, strpos($_SERVER['REQUEST_URI'], '?')) . '?m=candidates&amp;a=show&amp;candidateID=' . $candidateID . '">'.
                        'http://' . $_SERVER['HTTP_HOST'] . substr($_SERVER['REQUEST_URI'], 0, strpos($_SERVER['REQUEST_URI'], '?')) . '?m=candidates&amp;a=show&amp;candidateID=' . $candidateID . '</a>'
                );
                $statusChangeTemplate = str_replace(
                    $stringsToFind,
                    $replacementStrings,
                    $statusChangeTemplate
                );

                $email = $statusChangeTemplate;
            }
            else
            {
                $email = '';
                $emailAddress = '';
            }
        }
        else
        {
            $email = '';
            $emailAddress = '';
        }

        $isActive        = $this->isChecked('isActive', $_POST);
        $firstName       = $this->getTrimmedInput('firstName', $_POST);
        $middleName      = $this->getTrimmedInput('middleName', $_POST);
        $lastName        = $this->getTrimmedInput('lastName', $_POST);
        $email1          = $this->getTrimmedInput('email1', $_POST);
        $email2          = $this->getTrimmedInput('email2', $_POST);
        $address         = $this->getTrimmedInput('address', $_POST);
        $city            = $this->getTrimmedInput('city', $_POST);
        $state           = $this->getTrimmedInput('state', $_POST);
        $zip             = $this->getTrimmedInput('zip', $_POST);
        $source          = $this->getTrimmedInput('source', $_POST);
        $keySkills       = $this->getTrimmedInput('keySkills', $_POST);
        $currentEmployer = $this->getTrimmedInput('currentEmployer', $_POST);
        $currentPay      = $this->getTrimmedInput('currentPay', $_POST);
        $desiredPay      = $this->getTrimmedInput('desiredPay', $_POST);
        $notes           = $this->getTrimmedInput('notes', $_POST);
        $webSite         = $this->getTrimmedInput('webSite', $_POST);
        $bestTimeToCall  = $this->getTrimmedInput('bestTimeToCall', $_POST);
        $gender          = $this->getTrimmedInput('gender', $_POST);
        $race            = $this->getTrimmedInput('race', $_POST);
        $veteran         = $this->getTrimmedInput('veteran', $_POST);
        $disability      = $this->getTrimmedInput('disability', $_POST);
		
		$lifeLic        = $this->getTrimmedInput('lifeLic', $_POST);
		$propLic        = $this->getTrimmedInput('propLic', $_POST);
		$ser6           = $this->getTrimmedInput('ser6', $_POST);
		$ser63          = $this->getTrimmedInput('ser63', $_POST);
		$ser7           = $this->getTrimmedInput('ser7', $_POST);
		$camp_id        = $this->getTrimmedInput('camp_id', $_POST);

        /* Candidate source list editor. */
		$sourceCSV = $this->getTrimmedInput('sourceCSV', $_POST);
		
		
			
		 /* Bail out if any of the required fields are empty. */
        if (empty($firstName) || empty($lastName))
        {
            CommonErrors::fatal(COMMONERROR_MISSINGFIELDS, $this, 'Required fields are missing.');
        }

        $userid=$_SESSION['RESUFLO']->getUserID();
     	if($email1 != '')
			{
	 			$sql =mysql_query(" SELECT count(*) as countno FROM candidate where (((email1='$email1') and (candidate_id != '$candidateID'))  or email2 ='$email1')  and entered_by=$userid");
       
				 $resourcedetail= mysql_fetch_assoc($sql);
					if($resourcedetail['countno']>0)
					{
					
					RESUFLOUtility::transferRelativeURI("m=candidates&a=edit&candidateID=$candidateID&list=$list&er=exists");
					}
				
				
				if(!eregi("^[_a-z0-9-]+(\.[_a-z0-9-]+)*@[a-z0-9-]+(\.[a-z0-9-]+)*(\.[a-z]{2,4})$", $email1))
				{
					RESUFLOUtility::transferRelativeURI("m=candidates&a=edit&candidateID=$candidateID&list=$list&ermsg=invalid");
				}
			
		 
			}
		if($email2 != '')
		   { 
			  $sql1 =" SELECT count(*) as countno FROM candidate where (((email2 ='$email2') and (candidate_id != '$candidateID')) or email1='$email2') and entered_by=$userid";
			
				$res = mysql_query($sql1);
				 $resourcedetail= mysql_fetch_assoc($res);
					if($resourcedetail['countno']>0)
					{
						RESUFLOUtility::transferRelativeURI("m=candidates&a=edit&candidateID=$candidateID&er=exists");
						
					
					} 
					if(!eregi("^[_a-z0-9-]+(\.[_a-z0-9-]+)*@[a-z0-9-]+(\.[a-z0-9-]+)*(\.[a-z]{2,4})$", $email2))
				{
					RESUFLOUtility::transferRelativeURI("m=candidates&a=edit&candidateID=$candidateID&ermsg=invalid");
				}
			 
			}
		
		
	
	
	     if (!eval(Hooks::get('CANDIDATE_ON_EDIT_PRE'))) return;

        /* Update the candidate record. */
        $updateSuccess = $candidates->update(
            $candidateID,
            $isActive,
            $firstName,
            $middleName,
            $lastName,
            $email1,
            $email2,
            $phoneHome,
            $phoneCell,
            $phoneWork,
            $address,
            $city,
            $state,
            $zip,
            $source,
            $keySkills,
            $dateAvailable,
            $scheduledDate,
            $currentEmployer,
            $canRelocate,
            $currentPay,
            $desiredPay,
            $notes,
            $webSite,
            $bestTimeToCall,
			$enteredBy,
            $owner,
            $isHot,
            $email,
            $emailAddress,
            $gender,
            $race,
            $veteran,
            $disability,
			$lifeLic,
			$propLic,
			$ser6,
			$ser63,
			$ser7,
			$camp_id

        );
        if (!$updateSuccess)
        {
            CommonErrors::fatal(COMMONERROR_RECORDERROR, $this, 'Failed to update candidate.');
        }

        /* Update extra fields. */
        $candidates->extraFields->setValuesOnEdit($candidateID);

        /* Update possible source list */
        $sources = $candidates->getPossibleSources();
        $sourcesDifferences = ListEditor::getDifferencesFromList(
            $sources, 'name', 'sourceID', $sourceCSV
        );

        $candidates->updatePossibleSources($sourcesDifferences);

        if (!eval(Hooks::get('CANDIDATE_ON_EDIT_POST'))) return;
      if($_REQUEST["list"]!='') {
        RESUFLOUtility::transferRelativeURI(
            'm=candidates&a=show&candidateID=' . $candidateID.'&list=' . $_REQUEST["list"]
        );
		} else {
		RESUFLOUtility::transferRelativeURI(
            'm=candidates&a=show&candidateID=' . $candidateID
        );	
		}
	
    }
	
    /*
     * Called by handleRequest() to process deleting a candidate.
     */
    private function onDelete()
    {
       
        if ($this->_accessLevel < ACCESS_LEVEL_DELETE)
        {
            CommonErrors::fatal(COMMONERROR_PERMISSION, $this, 'Invalid user level for action.');
        }

        /* Bail out if we don't have a valid candidate ID. */
        if (!$this->isRequiredIDValid('candidateID', $_GET))
        {
            CommonErrors::fatal(COMMONERROR_BADINDEX, $this, 'Invalid candidate ID.');
        }

        $candidateID = $_GET['candidateID'];

        if (!eval(Hooks::get('CANDIDATE_DELETE'))) return;

        $candidates = new Candidates($this->_siteID);
        $candidates->delete($candidateID);

        /* Delete the MRU entry if present. */
        $_SESSION['RESUFLO']->getMRU()->removeEntry(
            DATA_ITEM_CANDIDATE, $candidateID
        );

        // RESUFLOUtility::transferRelativeURI('m=candidates&a=listByView');
		if($_REQUEST["list"]!='') {
        RESUFLOUtility::transferRelativeURI(
			'm=lists&a=showList&listrec=view_all&savedListID='.$_REQUEST["list"]
        );
		} else {
		RESUFLOUtility::transferRelativeURI(
            'm=candidates&a=listByView'
        );	
		}
    }

    /*
     * Called by handleRequest() to handle processing an "Add to a Job Order
     * Pipeline" search and displaying the results in the modal dialog, or
     * to show the initial dialog.
     */
    private function considerForJobSearch($candidateIDArray = array())
    {
        /* Get list of candidates. */
        if (isset($_REQUEST['candidateIDArrayStored']) && $this->isRequiredIDValid('candidateIDArrayStored', $_REQUEST, true))
        {
            $candidateIDArray = $_SESSION['RESUFLO']->retrieveData($_REQUEST['candidateIDArrayStored']);
        }
        else if($this->isRequiredIDValid('candidateID', $_REQUEST))
        {
            $candidateIDArray = array($_REQUEST['candidateID']);
        }
        else if ($candidateIDArray === array())
        {
            $dataGrid = DataGrid::getFromRequest();

            $candidateIDArray = $dataGrid->getExportIDs();
        }

        if (!is_array($candidateIDArray))
        {
            CommonErrors::fatalModal(COMMONERROR_BADINDEX, $this, 'Invalid variable type.');
            return;
        }

        /* Validate each ID */
        foreach ($candidateIDArray as $index => $candidateID)
        {
            if (!$this->isRequiredIDValid($index, $candidateIDArray))
            {
                echo('&'.$candidateID.'>');

                CommonErrors::fatalModal(COMMONERROR_BADINDEX, $this, 'Invalid candidate ID.');
                return;
            }
        }

        /* Bail out to prevent an error if the POST string doesn't even contain
         * a field named 'wildCardString' at all.
         */
        if (!isset($_POST['wildCardString']) && isset($_POST['mode']))
        {
            CommonErrors::fatal(COMMONERROR_WILDCARDSTRING, $this, 'No wild card string specified.');
        }

        $query = $this->getTrimmedInput('wildCardString', $_POST);
        $mode  = $this->getTrimmedInput('mode', $_POST);

        /* Execute the search. */
        $search = new SearchJobOrders($this->_siteID);
        switch ($mode)
        {
            case 'searchByJobTitle':
                $rs = $search->byTitle($query, 'title', 'ASC', true);
                $resultsMode = true;
                break;

            case 'searchByCompanyName':
                $rs = $search->byCompanyName($query, 'title', 'ASC', true);
                $resultsMode = true;
                break;

            default:
                $rs = $search->recentlyModified('DESC', true, 5);
                $resultsMode = false;
                break;
        }

        $pipelines = new Pipelines($this->_siteID);
        $pipelinesRS = $pipelines->getCandidatePipeline($candidateIDArray[0]);


        //die('<pre>'.print_r($rs,true));
        //jobLinkCold in CandidatesUI::considerForJobSearch()

        foreach ($rs as $rowIndex => $row)
        {
            if (ResultSetUtility::findRowByColumnValue($pipelinesRS,
                'jobOrderID', $row['jobOrderID']) !== false && count($candidateIDArray) == 1)
            {
                $rs[$rowIndex]['inPipeline'] = true;
            }
            else
            {
                $rs[$rowIndex]['inPipeline'] = false;
            }

            /* Convert '00-00-00' dates to empty strings. */
            $rs[$rowIndex]['startDate'] = DateUtility::fixZeroDate(
                $row['startDate']
            );

            if ($row['isHot'] == 1)
            {
                $rs[$rowIndex]['linkClass'] = 'jobLinkHot';
            }
            else
            {
                $rs[$rowIndex]['linkClass'] = 'jobLinkCold';
            }

            $rs[$rowIndex]['recruiterAbbrName'] = StringUtility::makeInitialName(
                $row['recruiterFirstName'],
                $row['recruiterLastName'],
                false,
                LAST_NAME_MAXLEN
            );

            $rs[$rowIndex]['ownerAbbrName'] = StringUtility::makeInitialName(
                $row['ownerFirstName'],
                $row['ownerLastName'],
                false,
                LAST_NAME_MAXLEN
            );
        }

        if (!eval(Hooks::get('CANDIDATE_ON_CONSIDER_FOR_JOB_SEARCH'))) return;
        $this->_template->assign('rs', $rs);
        $this->_template->assign('isFinishedMode', false);
        $this->_template->assign('isResultsMode', $resultsMode);
        $this->_template->assign('candidateIDArray', $candidateIDArray);
        $this->_template->assign('candidateIDArrayStored', $_SESSION['RESUFLO']->storeData($candidateIDArray));
        $this->_template->display('./modules/candidates/ConsiderSearchModal.tpl');
    }

    /*
     * Called by handleRequest() to process adding a candidate to a pipeline
     * in the modal dialog.
     */
	 
	 
	 private function addToRecruiter($candidateIDArray = array())
    	{
		
		if (isset($_POST['submit']))
		{
			$recruiter_id = $this->getTrimmedInput('recruiter_id', $_POST);
			
		}
       
	   	$dataGrid = DataGrid::getFromRequest();
		$candidateIDArray = $dataGrid->getExportIDs();
		//print_r($candidateIDArray);
        /* Validate each ID */
        foreach ($candidateIDArray as $index => $candidateID)
        {
            if (!$this->isRequiredIDValid($index, $candidateIDArray))
            {
                echo('&'.$candidateID.'>');

                CommonErrors::fatalModal(COMMONERROR_BADINDEX, $this, 'Invalid candidate ID.');
                return;
            }
        }

		foreach ($candidateIDArray as $index => $candidateID)
        {
			if(isset($recruiter_id))
			{
				$sql = "UPDATE candidate set entered_by = '$recruiter_id', owner = '$recruiter_id'";
                                
                                $getSubUsers = mysql_query("select user.*, dripmarket_compaign.id as dripmarket_compaignid,  dripmarket_compaignemail.id as dripmarket_compaignemailid from user left join dripmarket_compaign on user.user_id = dripmarket_compaign.userid and dripmarket_compaign.default = 1 left join dripmarket_compaignemail on dripmarket_compaign.id = dripmarket_compaignemail.compaignid and dripmarket_compaignemail.sendemailon = 0 where user.user_id = '$recruiter_id' and user.access_level != 0");
                                $subUser = mysql_fetch_array($getSubUsers);
                                if($subUser["dripmarket_compaignid"]>0)
                                {
                                    $sql = $sql.", campaign_id =".$subUser["dripmarket_compaignid"];
                                }
                                $sql = $sql." WHERE candidate_id = ".$candidateID;
                                $updateCandidate= mysql_query($sql);
                    
                                if($subUser["dripmarket_compaignid"] != '' && $subUser["dripmarket_compaignid"]>0)
                                {
                                    $dripQuery = 'insert into drip_queue set smtp_error="CandidatesUI", scheduledDate = Now(), candidate_id = '.$candidateID.', drip_campaign_id = '.$subUser["dripmarket_compaignid"].', drip_campemail_id = '.$subUser["dripmarket_compaignemailid"].', entered_by = '.$recruiter_id.', date_created = now(), date_modified = now()';
                                    mysql_query($dripQuery);
                                }
			}
		}
        /* Bail out to prevent an error if the POST string doesn't even contain
         * a field named 'wildCardString' at all.
         */
        $userid= $_SESSION['RESUFLO']->getuserid(); 
		if($_REQUEST['postback']=='postback'){
			$postback = 1;
		}
        $this->_template->assign('rs', $rs);
		$this->_template->assign('userid', $userid);
		$this->_template->assign('postback', $postback);
        $this->_template->assign('candidateIDArray', $candidateIDArray);
        $this->_template->assign('candidateIDArrayStored', $_SESSION['RESUFLO']->storeData($candidateIDArray));
        $this->_template->display('./modules/candidates/addToRecruiters.tpl');
    }
	
	 private function addToFollow(){
	 	$follow = $_REQUEST['follow'];
		$dataGrid = DataGrid::getFromRequest();
		$candidateIDs = $dataGrid->getExportIDs();
		//print_r($candidateIDs);
		if($follow == '1')
		{
			foreach($candidateIDs as $val =>$key){
				if (!$this->isRequiredIDValid('candidateID', $key))
				{
					CommonErrors::fatal(COMMONERROR_BADINDEX, $this, 'Invalid candidate ID.');
				}
				$follow="update candidate set follow_up = '1',unsubscribe = '0' where candidate_id = '$key' ";
				$result = mysql_query($follow);	
				$deleteJobOrder = mysql_query("DELETE FROM candidate_joborder WHERE candidate_id = '$key'");		
			}
		}
		
		if($_REQUEST['delrec']=='PipeLine' ){
		 RESUFLOUtility::transferRelativeURI('m=candidates&a=PipeLine');
		 }
	 }
    private function onAddToPipeline()
    {
        if ($this->_accessLevel < ACCESS_LEVEL_EDIT)
        {
            CommonErrors::fatal(COMMONERROR_PERMISSION, $this, 'Invalid user level for action.');
        }

        /* Bail out if we don't have a valid job order ID. */
        if (!$this->isRequiredIDValid('jobOrderID', $_GET))
        {
            CommonErrors::fatalModal(COMMONERROR_BADINDEX, $this, 'Invalid job order ID.');
        }

        if (isset($_GET['candidateID']))
        {
            /* Bail out if we don't have a valid candidate ID. */
            if (!$this->isRequiredIDValid('candidateID', $_GET))
            {
                CommonErrors::fatalModal(COMMONERROR_BADINDEX, $this, 'Invalid candidate ID.');
            }

            $candidateIDArray = array($_GET['candidateID']);
        }
        else
        {
            if (!isset($_REQUEST['candidateIDArrayStored']) || !$this->isRequiredIDValid('candidateIDArrayStored', $_REQUEST, true))
            {
                CommonErrors::fatalModal(COMMONERROR_BADINDEX, $this, 'Invalid candidateIDArrayStored parameter.');
                return;
            }

            $candidateIDArray = $_SESSION['RESUFLO']->retrieveData($_REQUEST['candidateIDArrayStored']);

            if (!is_array($candidateIDArray))
            {
                CommonErrors::fatalModal(COMMONERROR_BADINDEX, $this, 'Invalid variable type.');
                return;
            }

            /* Validate each ID */
            foreach ($candidateIDArray as $index => $candidateID)
            {
                if (!$this->isRequiredIDValid($index, $candidateIDArray))
                {
                    echo ($dataItemID);

                    CommonErrors::fatalModal(COMMONERROR_BADINDEX, $this, 'Invalid candidate ID.');
                    return;
                }
            }
        }


        $jobOrderID  = $_GET['jobOrderID'];

        if (!eval(Hooks::get('CANDIDATE_ADD_TO_PIPELINE_PRE'))) return;

        $pipelines = new Pipelines($this->_siteID);
        $activityEntries = new ActivityEntries($this->_siteID);

        /* Drop candidate ID's who are already in the pipeline */
        $pipelinesRS = $pipelines->getJobOrderPipeline($jobOrderID);

        foreach($pipelinesRS as $data)
        {
            $arrayPos = array_search($data['candidateID'], $candidateIDArray);
            if ($arrayPos !== false)
            {
                unset($candidateIDArray[$arrayPos]);
            }
        }

        /* Add to pipeline */
        foreach($candidateIDArray as $candidateID)
        {
            if (!$pipelines->add($candidateID, $jobOrderID, $this->_userID))
            {
                CommonErrors::fatalModal(COMMONERROR_RECORDERROR, $this, 'Failed to add candidate to pipeline.');
            }
			//$updateSubs = "update candidate set unsubscribe = '0' ,follow_up = '0',unsubscribe_date = '0000-00-00 00:00:00' where candidate_id = '$candidateID'";
			$updateSubs = "update candidate set is_hot = '0', ishot = '0', unsubscribe = '0', campaign_id = 'none' ,follow_up = '0',unsubscribe_date = '0000-00-00 00:00:00' where candidate_id = '$candidateID'";
			$resultSubs = mysql_query($updateSubs);

            $activityID = $activityEntries->add(
                $candidateID,
                DATA_ITEM_CANDIDATE,
                400,
                'Added candidate to pipeline.',
                $this->_userID,
                $jobOrderID
            );

            if (!eval(Hooks::get('CANDIDATE_ADD_TO_PIPELINE_POST_IND'))) return;
        }

        if (!eval(Hooks::get('CANDIDATE_ADD_TO_PIPELINE_POST'))) return;

        $this->_template->assign('isFinishedMode', true);
        $this->_template->assign('jobOrderID', $jobOrderID);
        $this->_template->assign('candidateIDArray', $candidateIDArray);
        $this->_template->display(
            './modules/candidates/ConsiderSearchModal.tpl'
        );
    }

    private function addActivityChangeStatus()
    {
        /* Bail out if we don't have a valid candidate ID. */
        if (!$this->isRequiredIDValid('candidateID', $_GET))
        {
            CommonErrors::fatalModal(COMMONERROR_BADINDEX, $this, 'Invalid candidate ID.');
        }

        /* Bail out if we don't have a valid job order ID. */
        if (!$this->isOptionalIDValid('jobOrderID', $_GET))
        {
            CommonErrors::fatalModal(COMMONERROR_BADINDEX, $this, 'Invalid job order ID.');
        }

        $selectedJobOrderID = $_GET['jobOrderID'];
        $candidateID        = $_GET['candidateID'];
		
        $candidates = new Candidates($this->_siteID);
	
        $candidateData = $candidates->get($candidateID);
		if(empty($candidateData['joborder_id']))
		{
			$candidateData['joborder_id']=-1;
		}
        /* Bail out if we got an empty result set. */
        if (empty($candidateData))
        {
            CommonErrors::fatalModal(COMMONERROR_BADINDEX, $this);
            return;
            /*$this->fatalModal(
                'The specified candidate ID could not be found.'
            );*/
        }

        $pipelines = new Pipelines($this->_siteID);
        $pipelineRS = $pipelines->getCandidatePipeline($candidateID);
        $statusRS = $pipelines->getStatusesForPicking2($candidateData['joborder_id']);
		
        if ($selectedJobOrderID != -1)
        {
            $selectedStatusID = ResultSetUtility::getColumnValueByIDValue(
                $pipelineRS, 'jobOrderID', $selectedJobOrderID, 'statusID'
            );
        }
        else
        {
            $selectedStatusID = -1;
        }

        /* Get the change status email template. */
        $emailTemplates = new EmailTemplates($this->_siteID);
        $statusChangeTemplateRS = $emailTemplates->getStatusEmailByTag(
            'EMAIL_TEMPLATE_STATUSCHANGE'
        );
        if (empty($statusChangeTemplateRS) ||
            empty($statusChangeTemplateRS['textReplaced']))
        {
            $statusChangeTemplate = '';
            $emailDisabled = '1';
        }
        else
        {
            $statusChangeTemplate = $statusChangeTemplateRS['textReplaced'];
            $emailDisabled = $statusChangeTemplateRS['disabled'];
        }

        /* Replace e-mail template variables. '%CANDSTATUS%', '%JBODTITLE%',
         * '%JBODCLIENT%' are replaced by JavaScript.
         */
        $stringsToFind = array(
            '%CANDOWNER%',
            '%CANDFIRSTNAME%',
            '%CANDFULLNAME%'
        );
        $replacementStrings = array(
            $candidateData['ownerFullName'],
            $candidateData['firstName'],
            $candidateData['firstName'] . ' ' . $candidateData['lastName'],
            $candidateData['firstName'],
            $candidateData['firstName']
        );
        $statusChangeTemplate = str_replace(
            $stringsToFind,
            $replacementStrings,
            $statusChangeTemplate
        );

        /* Are we in "Only Schedule Event" mode? */
        $onlyScheduleEvent = $this->isChecked('onlyScheduleEvent', $_GET);

        $calendar = new Calendar($this->_siteID);

        $calendarEventTypes = $calendar->getAllEventTypes();

        if (!eval(Hooks::get('CANDIDATE_ADD_ACTIVITY_CHANGE_STATUS'))) return;

        if (SystemUtility::isSchedulerEnabled() && !$_SESSION['RESUFLO']->isDemo())
        {
            $allowEventReminders = true;
        }
        else
        {
            $allowEventReminders = false;
        }

		$siteName = $_SESSION['RESUFLO']->getSiteName();
        $ownerfullName = $_SESSION['RESUFLO']->getFullName();
		$dateFormat = date('d M,Y h:i');

        $sql = 'select dead, future from candidate_joborder where candidate_id = "'.$candidateID.'"';
        $res = mysql_query($sql);
        $row = mysql_fetch_assoc($res);
        $dead = $row['dead'];
        $future = $row['future'];

        //$add_calendar_requests = $calendar->addInterviewSchedule('','','','','','','','','','');

        $this->_template->assign('dead', $dead);
        $this->_template->assign('future', $future);

        $this->_template->assign('candidateID', $candidateID);
		$this->_template->assign('siteName', $siteName);
		$this->_template->assign('ownerfullName', $ownerfullName);
		$this->_template->assign('dateFormat', $dateFormat);
		
		$this->_template->assign('candidateFullName', $candidateData['firstName'] . ' ' . $candidateData['lastName']);
        $this->_template->assign('pipelineRS', $pipelineRS);
        $this->_template->assign('statusRS', $statusRS);
        $this->_template->assign('selectedJobOrderID', $selectedJobOrderID);
        $this->_template->assign('selectedStatusID', $selectedStatusID);
        $this->_template->assign('allowEventReminders', $allowEventReminders);
        $this->_template->assign('userEmail', $_SESSION['RESUFLO']->getEmail());
        $this->_template->assign('calendarEventTypes', $calendarEventTypes);
        $this->_template->assign('statusChangeTemplate', $statusChangeTemplate);
        $this->_template->assign('onlyScheduleEvent', $onlyScheduleEvent);
        $this->_template->assign('emailDisabled', $emailDisabled);
        $this->_template->assign('isFinishedMode', false);
        $this->_template->assign('isJobOrdersMode', false);
		$this->_template->assign('list', true);
        $this->_template->display(
            './modules/candidates/AddActivityChangeStatusModal.tpl'
        );
    }

    private function onAddActivityChangeStatus()
    {
        if ($this->_accessLevel < ACCESS_LEVEL_EDIT)
        {
            CommonErrors::fatal(COMMONERROR_PERMISSION, $this, 'Invalid user level for action.');
        }

        /* Bail out if we don't have a valid regardingjob order ID. */
        if (!$this->isOptionalIDValid('regardingID', $_POST))
        {
            CommonErrors::fatalModal(COMMONERROR_BADINDEX, $this, 'Invalid job order ID.');
        }

        $regardingID = $_POST['regardingID'];

        $this->_addActivityChangeStatus(false, $regardingID);
    }

    /*
     * Called by handleRequest() to process removing a candidate from the
     * pipeline for a job order.
     */
    private function onRemoveFromPipeline()
    {
        if ($this->_accessLevel < ACCESS_LEVEL_DELETE)
        {
            CommonErrors::fatal(COMMONERROR_PERMISSION, $this, 'Invalid user level for action.');
        }

        /* Bail out if we don't have a valid candidate ID. */
        if (!$this->isRequiredIDValid('candidateID', $_GET))
        {
            CommonErrors::fatal(COMMONERROR_BADINDEX, $this, 'Invalid candidate ID.');
        }

        /* Bail out if we don't have a valid job order ID. */
        if (!$this->isRequiredIDValid('jobOrderID', $_GET))
        {
            CommonErrors::fatal(COMMONERROR_BADINDEX, $this, 'Invalid job order ID.');
        }

        $candidateID = $_GET['candidateID'];
        $jobOrderID  = $_GET['jobOrderID'];

        if (!eval(Hooks::get('CANDIDATE_REMOVE_FROM_PIPELINE_PRE'))) return;

        $pipelines = new Pipelines($this->_siteID);
        $pipelines->remove($candidateID, $jobOrderID);
		$updateUnsub ="update candidate set unsubscribe  = '0' where candidate_id = '$candidateID' ";
		$result = mysql_query($updateUnsub);

        if (!eval(Hooks::get('CANDIDATE_REMOVE_FROM_PIPELINE_POST'))) return;
if($_REQUEST["list"]!='') {
        RESUFLOUtility::transferRelativeURI(
            'm=candidates&a=show&candidateID=' . $candidateID.'&list=' . $_REQUEST["list"]
        );
		}
		else
		{
			RESUFLOUtility::transferRelativeURI(
            'm=candidates&a=show&candidateID=' . $candidateID
        );
		}
    }

    /*
     * Called by handleRequest() to process loading the search page.
     */
    private function search()
    {
        $savedSearches = new SavedSearches($this->_siteID);
        $savedSearchRS = $savedSearches->get(DATA_ITEM_CANDIDATE);

        if (!eval(Hooks::get('CANDIDATE_SEARCH'))) return;

        $this->_template->assign('wildCardString', '');
        $this->_template->assign('savedSearchRS', $savedSearchRS);
        $this->_template->assign('active', $this);
        $this->_template->assign('subActive', 'Search Candidates');
        $this->_template->assign('isResultsMode', false);
        $this->_template->assign('isResumeMode', false);
        $this->_template->assign('resumeWildCardString', '');
        $this->_template->assign('keySkillsWildCardString', '');
        $this->_template->assign('fullNameWildCardString', '');
        $this->_template->assign('phoneNumberWildCardString', '');
		$this->_template->assign('tagsWildCardString', '');
		
        $this->_template->assign('mode', '');
		$this->_template->assign('strt', $strt);
		$this->_template->assign('end1', $end1);
        $this->_template->display('./modules/candidates/Search.tpl');
    }

    /*
     * Called by handleRequest() to process displaying the search results.
     */
    private function onSearch()
    {
        /* Bail out to prevent an error if the GET string doesn't even contain
         * a field named 'wildCardString' at all.
         */
        if (!isset($_GET['wildCardString']))
        {
            $this->listByView('No wild card string specified.');
            return;
        }

        $query = trim($_GET['wildCardString']);
		
		if($_GET['start_date']!="")
		{
			$StartDateParts=explode("-",$_REQUEST["start_date"]);
			$strt = $StartDateParts[2]."-".$StartDateParts[0]."-".$StartDateParts[1].' 00:00:00';
			$EndDateParts=explode("-",$_REQUEST["end_date"]);
			$end1 = $EndDateParts[2]."-".$EndDateParts[0]."-".$EndDateParts[1].' 23:59:59';
		}
		
        /* Initialize stored wildcard strings to safe default values. */
        $resumeWildCardString      = '';
        $keySkillsWildCardString   = '';
        $phoneNumberWildCardString = '';
        $fullNameWildCardString    = '';
		$tagsWildCardString		   = '';
		$emailWildCardString	   = '';

        /* Set up sorting. */
        if ($this->isRequiredIDValid('page', $_GET))
        {
            $currentPage = $_GET['page'];
        }
        else
        {
            $currentPage = 1;
        }

        $searchPager = new SearchPager(
            CANDIDATES_PER_PAGE, $currentPage, $this->_siteID
        );

        if ($searchPager->isSortByValid('sortBy', $_GET))
        {
            $sortBy = $_GET['sortBy'];
        }
        else
        {
            $sortBy = 'lastName';
        }

        if ($searchPager->isSortDirectionValid('sortDirection', $_GET))
        {
            $sortDirection = $_GET['sortDirection'];
        }
        else
        {
            $sortDirection = 'ASC';
        }

        $baseURL = RESUFLOUtility::getFilteredGET(
            array('sortBy', 'sortDirection', 'page'), '&amp;'
        );
        $searchPager->setSortByParameters($baseURL, $sortBy, $sortDirection);

        $candidates = new Candidates($this->_siteID);

        /* Get our current searching mode. */
        $mode = $this->getTrimmedInput('mode', $_GET);

        /* Execute the search. */
        $search = new SearchCandidates($this->_siteID);
	
        switch ($mode)
        {
            case 'searchByFullName':
                $rs = $search->byFullName($query, $sortBy, $sortDirection);

                foreach ($rs as $rowIndex => $row)
                {
                    if (!empty($row['ownerFirstName']))
                    {
                        $rs[$rowIndex]['ownerAbbrName'] = StringUtility::makeInitialName(
                            $row['ownerFirstName'],
                            $row['ownerLastName'],
                            false,
                            LAST_NAME_MAXLEN
                        );
                    }
                    else
                    {
                        $rs[$rowIndex]['ownerAbbrName'] = 'None';
                    }

                    $rsResume = $candidates->getResumes($row['candidateID']);
                    if (isset($rsResume[0]))
                    {
                        $rs[$rowIndex]['resumeID'] = $rsResume[0]['attachmentID'];
                    }
                }

                $isResumeMode = false;

                $fullNameWildCardString = $query;
                break;

            case 'searchByKeySkills':
                $rs = $search->byKeySkills($query,$strt,$end1,$sortBy,$sortDirection);

                foreach ($rs as $rowIndex => $row)
                {
                    if (!empty($row['ownerFirstName']))
                    {
                        $rs[$rowIndex]['ownerAbbrName'] = StringUtility::makeInitialName(
                            $row['ownerFirstName'],
                            $row['ownerLastName'],
                            false,
                            LAST_NAME_MAXLEN
                        );
                    }
                    else
                    {
                        $rs[$rowIndex]['ownerAbbrName'] = 'None';
                    }

                    $rsResume = $candidates->getResumes($row['candidateID']);
                    if (isset($rsResume[0]))
                    {
                        $rs[$rowIndex]['resumeID'] = $rsResume[0]['attachmentID'];
                    }
                }

                $isResumeMode = false;

                $keySkillsWildCardString = $query;

                break;
				
			case 'searchByCandidateNotes':
                $rs = $search->byCandidateNotes($query,$strt,$end1,$sortBy,$sortDirection);

                foreach ($rs as $rowIndex => $row)
                {
                    if (!empty($row['ownerFirstName']))
                    {
                        $rs[$rowIndex]['ownerAbbrName'] = StringUtility::makeInitialName(
                            $row['ownerFirstName'],
                            $row['ownerLastName'],
                            false,
                            LAST_NAME_MAXLEN
                        );
                    }
                    else
                    {
                        $rs[$rowIndex]['ownerAbbrName'] = 'None';
                    }

                    $rsResume = $candidates->getResumes($row['candidateID']);
                    if (isset($rsResume[0]))
                    {
                        $rs[$rowIndex]['resumeID'] = $rsResume[0]['attachmentID'];
                    }
                }

                $isResumeMode = false;

                $keySkillsWildCardString = $query;

                break;
			
			
            case 'searchByResume':
                $searchPager = new SearchByResumePager(
                    20,
                    $currentPage,
                    $this->_siteID,
                    $query,
                    $sortBy,
                    $sortDirection
                );

                $baseURL = 'm=candidates&amp;a=search&amp;getback=getback&amp;mode=searchByResume&amp;wildCardString='
                    . urlencode($query)
                    . '&amp;searchByResume=Search';

                $searchPager->setSortByParameters(
                    $baseURL, $sortBy, $sortDirection
                );

                $rs = $searchPager->getPage();
				
                $currentPage = $searchPager->getCurrentPage();
                $totalPages  = $searchPager->getTotalPages();

                $pageStart = $searchPager->getThisPageStartRow() + 1;

                if (($searchPager->getThisPageStartRow() + 20) <= $searchPager->getTotalRows())
                {
                    $pageEnd = $searchPager->getThisPageStartRow() + 20;
                }
                else
                {
                    $pageEnd = $searchPager->getTotalRows();
                }

                foreach ($rs as $rowIndex => $row)
                {
                    $rs[$rowIndex]['excerpt'] = SearchUtility::searchExcerpt(
                        $query, $row['text']
                    );

                    if (!empty($row['ownerFirstName']))
                    {
                        $rs[$rowIndex]['ownerAbbrName'] = StringUtility::makeInitialName(
                            $row['ownerFirstName'],
                            $row['ownerLastName'],
                            false,
                            LAST_NAME_MAXLEN
                        );
                    }
                    else
                    {
                        $rs[$rowIndex]['ownerAbbrName'] = 'None';
                    }
                }

                $isResumeMode = true;

                $this->_template->assign('active', $this);
                $this->_template->assign('currentPage', $currentPage);
                $this->_template->assign('pageStart', $pageStart);
                $this->_template->assign('totalResults', $searchPager->getTotalRows());
                $this->_template->assign('pageEnd', $pageEnd);
                $this->_template->assign('totalPages', $totalPages);

                $resumeWildCardString = $query;
                break;

            case 'phoneNumber':
                $rs = $search->byPhone($query, $sortBy, $sortDirection);

                foreach ($rs as $rowIndex => $row)
                {
                    if (!empty($row['ownerFirstName']))
                    {
                        $rs[$rowIndex]['ownerAbbrName'] = StringUtility::makeInitialName(
                            $row['ownerFirstName'],
                            $row['ownerLastName'],
                            false,
                            LAST_NAME_MAXLEN
                        );
                    }
                    else
                    {
                        $rs[$rowIndex]['ownerAbbrName'] = 'None';
                    }

                    $rsResume = $candidates->getResumes($row['candidateID']);
                    if (isset($rsResume[0]))
                    {
                        $rs[$rowIndex]['resumeID'] = $rsResume[0]['attachmentID'];
                    }
                }

                $isResumeMode = false;

                $phoneNumberWildCardString = $query;
                break;
			
			 case 'searchByTags':
                $rs = $search->byTags($query, $sortBy, $sortDirection);

                foreach ($rs as $rowIndex => $row)
                {
                    if (!empty($row['ownerFirstName']))
                    {
                        $rs[$rowIndex]['ownerAbbrName'] = StringUtility::makeInitialName(
                            $row['ownerFirstName'],
                            $row['ownerLastName'],
                            false,
                            LAST_NAME_MAXLEN
                        );
                    }
                    else
                    {
                        $rs[$rowIndex]['ownerAbbrName'] = 'None';
                    }

                    $rsResume = $candidates->getResumes($row['candidateID']);
                    if (isset($rsResume[0]))
                    {
                        $rs[$rowIndex]['resumeID'] = $rsResume[0]['attachmentID'];
                    }
                }

                $isResumeMode = false;

                $tagsWildCardString = $query;
                break;
			case 'searchByEmail':
                $rs = $search->byEmail($query, $sortBy, $sortDirection);

                foreach ($rs as $rowIndex => $row)
                {
                    if (!empty($row['ownerFirstName']))
                    {
                        $rs[$rowIndex]['ownerAbbrName'] = StringUtility::makeInitialName(
                            $row['ownerFirstName'],
                            $row['ownerLastName'],
                            false,
                            LAST_NAME_MAXLEN
                        );
                    }
                    else
                    {
                        $rs[$rowIndex]['ownerAbbrName'] = 'None';
                    }

                    $rsResume = $candidates->getResumes($row['candidateID']);
                    if (isset($rsResume[0]))
                    {
                        $rs[$rowIndex]['resumeID'] = $rsResume[0]['attachmentID'];
                    }
                }

                $isResumeMode = false;

                $emailWildCardString = $query;
                break;
			case 'searchByDetailResume':
                $rs = $search->byDetailResume($query, $strt,$end1,$sortBy, $sortDirection);

                foreach ($rs as $rowIndex => $row)
                {
                    if (!empty($row['ownerFirstName']))
                    {
                        $rs[$rowIndex]['ownerAbbrName'] = StringUtility::makeInitialName(
                            $row['ownerFirstName'],
                            $row['ownerLastName'],
                            false,
                            LAST_NAME_MAXLEN
                        );
                    }
                    else
                    {
                        $rs[$rowIndex]['ownerAbbrName'] = 'None';
                    }

                    $rsResume = $candidates->getResumes($row['candidateID']);
                    if (isset($rsResume[0]))
                    {
                        $rs[$rowIndex]['resumeID'] = $rsResume[0]['attachmentID'];
                    }
                }

                $isResumeMode = false;

                $detailresumeWildCardString = $query;
                break;	
            default:
                $this->listByView('Invalid search mode.');
                return;
                break;
        }

        $candidateIDs = implode(',', ResultSetUtility::getColumnValues($rs, 'candidateID'));
        $exportForm = ExportUtility::getForm(
            DATA_ITEM_CANDIDATE, $candidateIDs, 32, 9
        );

        if (!eval(Hooks::get('CANDIDATE_ON_SEARCH'))) return;

        /* Save the search. */
        $savedSearches = new SavedSearches($this->_siteID);
        $savedSearches->add(
            DATA_ITEM_CANDIDATE,
            $query,
            $_SERVER['REQUEST_URI'],
            false
        );
	
        $savedSearchRS = $savedSearches->get(DATA_ITEM_CANDIDATE);
        $this->_template->assign('savedSearchRS', $savedSearchRS);
        $this->_template->assign('exportForm', $exportForm);
        $this->_template->assign('active', $this);
        $this->_template->assign('rs', $rs);
        $this->_template->assign('pager', $searchPager);
        $this->_template->assign('isResultsMode', true);
        $this->_template->assign('isResumeMode', $isResumeMode);
        $this->_template->assign('wildCardString', $query);
        $this->_template->assign('resumeWildCardString', $resumeWildCardString);
        $this->_template->assign('keySkillsWildCardString', $keySkillsWildCardString);
        $this->_template->assign('fullNameWildCardString', $fullNameWildCardString);
        $this->_template->assign('phoneNumberWildCardString', $phoneNumberWildCardString);
		$this->_template->assign('tagsWildCardString', $tagsWildCardString);
		$this->_template->assign('emailWildCardString', $emailWildCardString);
        $this->_template->assign('mode', $mode);
		$this->_template->assign('strt', $strt);
		$this->_template->assign('end1', $end1);
        $this->_template->display('./modules/candidates/Search.tpl');
    }

    /*
     * Called by handleRequest() to process showing a resume preview.
     */
    private function viewResume()
    {
        /* Bail out if we don't have a valid candidate ID. */
        if (!$this->isRequiredIDValid('attachmentID', $_GET))
        {
            CommonErrors::fatal(COMMONERROR_BADINDEX, $this, 'Invalid attachment ID.');
        }

        $attachmentID = $_GET['attachmentID'];

        /* Get the search string. */
        $query = $this->getTrimmedInput('wildCardString', $_GET);

        /* Get resume text. */
        $candidates = new Candidates($this->_siteID);
        $data = $candidates->getResume($attachmentID);

        if (!empty($data))
        {
            /* Keyword highlighting. */
            $data['text'] = SearchUtility::makePreview($query, $data['text']);
        }

        if (!eval(Hooks::get('CANDIDATE_VIEW_RESUME'))) return;

        $this->_template->assign('active', $this);
        $this->_template->assign('data', $data);
        $this->_template->display('./modules/candidates/ResumeView.tpl');
    }

    private function addEditImage()
    {
        /* Bail out if we don't have a valid candidate ID. */
        if (!$this->isRequiredIDValid('candidateID', $_GET))
        {
            CommonErrors::fatalModal(COMMONERROR_BADINDEX, $this, 'Invalid candidate ID.');
        }

        $candidateID = $_GET['candidateID'];

        $attachments = new Attachments($this->_siteID);
        $attachmentsRS = $attachments->getAll(
            DATA_ITEM_CANDIDATE, $candidateID
        );

        if (!eval(Hooks::get('CANDIDATE_ADD_EDIT_IMAGE'))) return;

        $this->_template->assign('isFinishedMode', false);
        $this->_template->assign('candidateID', $candidateID);
        $this->_template->assign('attachmentsRS', $attachmentsRS);
        $this->_template->display(
            './modules/candidates/CreateImageAttachmentModal.tpl'
        );
    }

    /*
     * Called by handleRequest() to process creating an attachment.
     */
    private function onAddEditImage()
    {
        if ($this->_accessLevel < ACCESS_LEVEL_EDIT)
        {
            CommonErrors::fatalModal(COMMONERROR_PERMISSION, $this, 'Invalid user level for action.');
        }

        /* Bail out if we don't have a valid candidate ID. */
        if (!$this->isRequiredIDValid('candidateID', $_POST))
        {
            CommonErrors::fatalModal(COMMONERROR_BADINDEX, $this, 'Invalid candidate ID.');
        }

        $candidateID = $_POST['candidateID'];

        if (!eval(Hooks::get('CANDIDATE_ON_ADD_EDIT_IMAGE_PRE'))) return;

        $attachmentCreator = new AttachmentCreator($this->_siteID);
        $attachmentCreator->createFromUpload(
            DATA_ITEM_CANDIDATE, $candidateID, 'file', true, false
        );

        if ($attachmentCreator->isError())
        {
            CommonErrors::fatalModal(COMMONERROR_FILEERROR, $this, $attachmentCreator->getError());
            return;
            //$this->fatalModal($attachmentCreator->getError());
        }

        if (!eval(Hooks::get('CANDIDATE_ON_ADD_EDIT_IMAGE_POST'))) return;

        $this->_template->assign('isFinishedMode', true);
        $this->_template->assign('candidateID', $candidateID);
        $this->_template->display(
            './modules/candidates/CreateImageAttachmentModal.tpl'
        );
    }

    /*
     * Called by handleRequest() to process loading the create attachment
     * modal dialog.
     */
    private function createAttachment()
    {
        /* Bail out if we don't have a valid candidate ID. */
        if (!$this->isRequiredIDValid('candidateID', $_GET))
        {
            CommonErrors::fatalModal(COMMONERROR_BADINDEX, $this, 'Invalid candidate ID.');
        }

        $candidateID = $_GET['candidateID'];

        if (!eval(Hooks::get('CANDIDATE_CREATE_ATTACHMENT'))) return;

        $this->_template->assign('isFinishedMode', false);
        $this->_template->assign('candidateID', $candidateID);
        $this->_template->display(
            './modules/candidates/CreateAttachmentModal.tpl'
        );
    }

    /*
     * Called by handleRequest() to process creating an attachment.
     */
    private function onCreateAttachment()
    {
        if ($this->_accessLevel < ACCESS_LEVEL_EDIT)
        {
            CommonErrors::fatal(COMMONERROR_PERMISSION, $this, 'Invalid user level for action.');
        }

        /* Bail out if we don't have a valid candidate ID. */
        if (!$this->isRequiredIDValid('candidateID', $_POST))
        {
            CommonErrors::fatalModal(COMMONERROR_BADINDEX, $this, 'Invalid candidate ID.');
        }

        /* Bail out if we don't have a valid resume status. */
        if (!$this->isRequiredIDValid('resume', $_POST, true) ||
            $_POST['resume'] < 0 || $_POST['resume'] > 1)
        {
            CommonErrors::fatalModal(COMMONERROR_RECORDERROR, $this, 'Invalid resume status.');
        }

        $candidateID = $_POST['candidateID'];

        if ($_POST['resume'] == '1')
        {
            $isResume = true;
        }
        else
        {
            $isResume = false;
        }

        if (!eval(Hooks::get('CANDIDATE_ON_CREATE_ATTACHMENT_PRE'))) return;

        $attachmentCreator = new AttachmentCreator($this->_siteID);
        $attachmentCreator->createFromUpload(
            DATA_ITEM_CANDIDATE, $candidateID, 'file', false, $isResume
        );

        if ($attachmentCreator->isError())
        {
            CommonErrors::fatalModal(COMMONERROR_FILEERROR, $this, $attachmentCreator->getError());
            return;
            //$this->fatalModal($attachmentCreator->getError());
        }

        if ($attachmentCreator->duplicatesOccurred())
        {
            $this->fatalModal(
                'This attachment has already been added to this candidate.'
            );
        }

        $isTextExtractionError = $attachmentCreator->isTextExtractionError();
        $textExtractionErrorMessage = $attachmentCreator->getTextExtractionError();
        $resumeText = $attachmentCreator->getExtractedText();

        if (!eval(Hooks::get('CANDIDATE_ON_CREATE_ATTACHMENT_POST'))) return;

        $this->_template->assign('resumeText', $resumeText);
        $this->_template->assign('isFinishedMode', true);
        $this->_template->assign('candidateID', $candidateID);
        $this->_template->display(
            './modules/candidates/CreateAttachmentModal.tpl'
        );
    }
    
    /*
     * Called by handleRequest() to process cancel an reminder.
     */
    private function onCancelReminder()
    {
        if ($this->_accessLevel < ACCESS_LEVEL_DELETE)
        {
            CommonErrors::fatal(COMMONERROR_PERMISSION, $this, 'Invalid user level for action.');
        }

        /* Bail out if we don't have a valid attachment ID. */
        if (!$this->isRequiredIDValid('eventID', $_GET))
        {
            CommonErrors::fatalModal(COMMONERROR_BADINDEX, $this, 'Invalid attachment ID.');
        }

        /* Bail out if we don't have a valid candidate ID. */
        if (!$this->isRequiredIDValid('candidateID', $_GET))
        {
            CommonErrors::fatalModal(COMMONERROR_BADINDEX, $this, 'Invalid candidate ID.');
        }

        $candidateID  = $_GET['candidateID'];
        $eventID = $_GET['eventID'];
        $calendar = new Calendar($this->_siteID);
        $calendar->cancelReminderEvents($eventID);

        if($_REQUEST["list"]!='') {
            RESUFLOUtility::transferRelativeURI(
                'm=candidates&a=show&candidateID=' . $candidateID.'&list=' . $_REQUEST["list"]
            );
        }
        else {
            RESUFLOUtility::transferRelativeURI(
                'm=candidates&a=show&candidateID=' . $candidateID
            );
        }
    }
    
    /*
     * Called by handleRequest() to process cancel an reminder.
     */
    private function onCancelThanks()
    {
        if ($this->_accessLevel < ACCESS_LEVEL_DELETE)
        {
            CommonErrors::fatal(COMMONERROR_PERMISSION, $this, 'Invalid user level for action.');
        }

        /* Bail out if we don't have a valid attachment ID. */
        if (!$this->isRequiredIDValid('eventID', $_GET))
        {
            CommonErrors::fatalModal(COMMONERROR_BADINDEX, $this, 'Invalid attachment ID.');
        }

        /* Bail out if we don't have a valid candidate ID. */
        if (!$this->isRequiredIDValid('candidateID', $_GET))
        {
            CommonErrors::fatalModal(COMMONERROR_BADINDEX, $this, 'Invalid candidate ID.');
        }

        $candidateID  = $_GET['candidateID'];
        $eventID = $_GET['eventID'];
        $calendar = new Calendar($this->_siteID);
        $calendar->cancelThanksEvents($eventID);

        if($_REQUEST["list"]!='') {
            RESUFLOUtility::transferRelativeURI(
                'm=candidates&a=show&candidateID=' . $candidateID.'&list=' . $_REQUEST["list"]
            );
        }
        else {
            RESUFLOUtility::transferRelativeURI(
                'm=candidates&a=show&candidateID=' . $candidateID
            );
        }
    }

    /*
     * Called by handleRequest() to process deleting an attachment.
     */
    private function onDeleteAttachment()
    {
        if ($this->_accessLevel < ACCESS_LEVEL_DELETE)
        {
            CommonErrors::fatal(COMMONERROR_PERMISSION, $this, 'Invalid user level for action.');
        }

        /* Bail out if we don't have a valid attachment ID. */
        if (!$this->isRequiredIDValid('attachmentID', $_GET))
        {
            CommonErrors::fatalModal(COMMONERROR_BADINDEX, $this, 'Invalid attachment ID.');
        }

        /* Bail out if we don't have a valid candidate ID. */
        if (!$this->isRequiredIDValid('candidateID', $_GET))
        {
            CommonErrors::fatalModal(COMMONERROR_BADINDEX, $this, 'Invalid candidate ID.');
        }

        $candidateID  = $_GET['candidateID'];
        $attachmentID = $_GET['attachmentID'];

        if (!eval(Hooks::get('CANDIDATE_ON_DELETE_ATTACHMENT_PRE'))) return;

        $attachments = new Attachments($this->_siteID);
        $attachments->delete($attachmentID);

        if (!eval(Hooks::get('CANDIDATE_ON_DELETE_ATTACHMENT_POST'))) return;
if($_REQUEST["list"]!='') {
        RESUFLOUtility::transferRelativeURI(
            'm=candidates&a=show&candidateID=' . $candidateID.'&list=' . $_REQUEST["list"]
        );
		}
		else {
			RESUFLOUtility::transferRelativeURI(
            'm=candidates&a=show&candidateID=' . $candidateID
        );
		}
    }

    //TODO: Document me.
    //Only accessable by MSA users - hides this job order from everybody by
    private function administrativeHideShow()
    {
        if ($this->_accessLevel < ACCESS_LEVEL_MULTI_SA)
        {
            CommonErrors::fatal(COMMONERROR_PERMISSION, $this, 'Invalid user level for action.');
        }

        /* Bail out if we don't have a valid joborder ID. */
        if (!$this->isRequiredIDValid('candidateID', $_GET))
        {
            CommonErrors::fatal(COMMONERROR_BADINDEX, $this, 'Invalid Job Order ID.');
        }

        /* Bail out if we don't have a valid status ID. */
        if (!$this->isRequiredIDValid('state', $_GET, true))
        {
            CommonErrors::fatal(COMMONERROR_BADINDEX, $this, 'Invalid state ID.');
        }

        $candidateID = $_GET['candidateID'];

        // FIXME: Checkbox?
        $state = (boolean) $_GET['state'];

        $candidates = new Candidates($this->_siteID);
        $candidates->administrativeHideShow($candidateID, $state);
if($_REQUEST["list"]!='') {
        RESUFLOUtility::transferRelativeURI('m=candidates&a=show&candidateID='.$candidateID.'&list='.$_REQUEST["list"]);
		} else {
			RESUFLOUtility::transferRelativeURI('m=candidates&a=show&candidateID='.$candidateID);
		}
    }

    /**
     * Formats SQL result set for display. This is factored out for code
     * clarity.
     *
     * @param array result set from listByView()
     * @return array formatted result set
     */
    private function _formatListByViewResults($resultSet)
    {
        if (empty($resultSet))
        {
            return $resultSet;
        }


        //die('<pre>'.print_r($resultSet,true));
        //jobLinkCold in CandidatesUI::_formatListByViewResults()

        foreach ($resultSet as $rowIndex => $row)
        {
            if ($resultSet[$rowIndex]['isHot'] == 1)
            {
                $resultSet[$rowIndex]['linkClass'] = 'jobLinkHot';
            }
            else
            {
                $resultSet[$rowIndex]['linkClass'] = 'jobLinkCold';
            }

            if (!empty($resultSet[$rowIndex]['ownerFirstName']))
            {
                $resultSet[$rowIndex]['ownerAbbrName'] = StringUtility::makeInitialName(
                    $resultSet[$rowIndex]['ownerFirstName'],
                    $resultSet[$rowIndex]['ownerLastName'],
                    false,
                    LAST_NAME_MAXLEN
                );
            }
            else
            {
                $resultSet[$rowIndex]['ownerAbbrName'] = 'None';
            }

            if ($resultSet[$rowIndex]['submitted'] == 1)
            {
                $resultSet[$rowIndex]['iconTag'] = '<img src="images/job_orders.gif" alt="" width="16" height="16" title="Submitted for a Job Order" />';
            }
            else
            {
                $resultSet[$rowIndex]['iconTag'] = '<img src="images/mru/blank.gif" alt="" width="16" height="16" />';
            }

            if ($resultSet[$rowIndex]['attachmentPresent'] == 1)
            {
                $resultSet[$rowIndex]['iconTag'] .= '<img src="images/paperclip.gif" alt="" width="16" height="16" title="Attachment Present" />';
            }
            else
            {
                $resultSet[$rowIndex]['iconTag'] .= '<img src="images/mru/blank.gif" alt="" width="16" height="16" />';
            }


            if (empty($resultSet[$rowIndex]['keySkills']))
            {
                $resultSet[$rowIndex]['keySkills'] = '&nbsp;';
            }
            else
            {
                $resultSet[$rowIndex]['keySkills'] = htmlspecialchars(
                    $resultSet[$rowIndex]['keySkills']
                );
            }

            /* Truncate Key Skills to fit the column width */
            if (strlen($resultSet[$rowIndex]['keySkills']) > self::TRUNCATE_KEYSKILLS)
            {
                $resultSet[$rowIndex]['keySkills'] = substr(
                    $resultSet[$rowIndex]['keySkills'],
                    0,
                    self::TRUNCATE_KEYSKILLS
                ) . "...";
            }
        }

        return $resultSet;
    }

    /**
     * Adds a candidate. This is factored out for code clarity.
     *
     * @param boolean is modal window
     * @param string module directory
     * @return integer candidate ID
     */
    private function _addCandidate($isModal, $directoryOverride = '')
    {
        /* Module directory override for fatal() calls. */
        if ($directoryOverride != '')
        {
            $moduleDirectory = $directoryOverride;
        }
        else
        {
            $moduleDirectory = $this->_moduleDirectory;
        }

        /* Modal override for fatal() calls. */
        if ($isModal)
        {
            $fatal = 'fatalModal';
        }
        else
        {
            $fatal = 'fatal';
        }

        /* Bail out if we received an invalid availability date; if not, go
         * ahead and convert the date to MySQL format.
         */
        $dateAvailable = $this->getTrimmedInput('dateAvailable', $_POST);
        if (!empty($dateAvailable))
        {
            if (!DateUtility::validate('-', $dateAvailable, DATE_FORMAT_MMDDYY))
            {
                $this->$fatal('Invalid availability date.', $moduleDirectory);
            }

            /* Convert start_date to something MySQL can understand. */
            $dateAvailable = DateUtility::convert(
                '-', $dateAvailable, DATE_FORMAT_MMDDYY, DATE_FORMAT_YYYYMMDD
            );
        }

        $formattedPhoneHome = StringUtility::extractPhoneNumber(
            $this->getTrimmedInput('phoneHome', $_POST)
        );
        if (!empty($formattedPhoneHome))
        {
            $phoneHome = $formattedPhoneHome;
        }
        else
        {
            $phoneHome = $this->getTrimmedInput('phoneHome', $_POST);
        }

        $formattedPhoneCell = StringUtility::extractPhoneNumber(
            $this->getTrimmedInput('phoneCell', $_POST)
        );
        if (!empty($formattedPhoneCell))
        {
            $phoneCell = $formattedPhoneCell;
        }
        else
        {
            $phoneCell = $this->getTrimmedInput('phoneCell', $_POST);
        }

        $formattedPhoneWork = StringUtility::extractPhoneNumber(
            $this->getTrimmedInput('phoneWork', $_POST)
        );
        if (!empty($formattedPhoneWork))
        {
            $phoneWork = $formattedPhoneWork;
        }
        else
        {
            $phoneWork = $this->getTrimmedInput('phoneWork', $_POST);
        }

        /* Can Relocate */
        $canRelocate = $this->isChecked('canRelocate', $_POST);

        $lastName        = $this->getTrimmedInput('lastName', $_POST);
        $middleName      = $this->getTrimmedInput('middleName', $_POST);
        $firstName       = $this->getTrimmedInput('firstName', $_POST);
        $email1          = $this->getTrimmedInput('email1', $_POST);
        $email2          = $this->getTrimmedInput('email2', $_POST);
        $address         = $this->getTrimmedInput('address', $_POST);
        $city            = $this->getTrimmedInput('city', $_POST);
        $state           = $this->getTrimmedInput('state', $_POST);
        $zip             = $this->getTrimmedInput('zip', $_POST);
        $source          = $this->getTrimmedInput('source', $_POST);
        $keySkills       = $this->getTrimmedInput('keySkills', $_POST);
        $currentEmployer = $this->getTrimmedInput('currentEmployer', $_POST);
        $currentPay      = $this->getTrimmedInput('currentPay', $_POST);
        $desiredPay      = $this->getTrimmedInput('desiredPay', $_POST);
        $notes           = $this->getTrimmedInput('notes', $_POST);
        $webSite         = $this->getTrimmedInput('webSite', $_POST);
        $bestTimeToCall  = $this->getTrimmedInput('bestTimeToCall', $_POST);
        $gender          = $this->getTrimmedInput('gender', $_POST);
        $race            = $this->getTrimmedInput('race', $_POST);
        $veteran         = $this->getTrimmedInput('veteran', $_POST);
        $disability      = $this->getTrimmedInput('disability', $_POST);
		$clientid      = $this->getTrimmedInput('clientid', $_POST);
		
		$lifeLic        = $this->getTrimmedInput('lifeLic', $_POST);
		$propLic        = $this->getTrimmedInput('propLic', $_POST);
		$ser6           = $this->getTrimmedInput('ser6', $_POST);
		$ser63          = $this->getTrimmedInput('ser63', $_POST);
		$ser7           = $this->getTrimmedInput('ser7', $_POST);
		$camp_id        = $this->getTrimmedInput('camp_id', $_POST);

        /* Candidate source list editor. */
        $sourceCSV = $this->getTrimmedInput('sourceCSV', $_POST);

        /* Text resume. */
        $textResumeBlock = $this->getTrimmedInput('textResumeBlock', $_POST);
        $textResumeFilename = $this->getTrimmedInput('textResumeFilename', $_POST);

        /* File resume. */
        $associatedFileResumeID = $this->getTrimmedInput('associatedbFileResumeID', $_POST);

        /* Bail out if any of the required fields are empty. */
        if (empty($firstName) || empty($lastName))
        {
            CommonErrors::fatal(COMMONERROR_MISSINGFIELDS, $this);
        }

        if (!eval(Hooks::get('CANDIDATE_ON_ADD_PRE'))) return;
		if($clientid==0 || $clientid==''){
		$setuser = $this->_userID;
		}else{
		$setuser = $clientid;
		}
        $candidates = new Candidates($this->_siteID);
        $candidateID = $candidates->add(
            $firstName,
            $middleName,
            $lastName,
            $email1,
            $email2,
            $phoneHome,
            $phoneCell,
            $phoneWork,
            $address,
            $city,
            $state,
            $zip,
            $source,
            $keySkills,
            $dateAvailable,
            $currentEmployer,
            $canRelocate,
            $currentPay,
            $desiredPay,
            $notes,
            $webSite,
            $bestTimeToCall,
            $setuser,
            $setuser,
            $gender,
            $race,
            $veteran,
            $disability,
			$lifeLic,
			$propLic,
			$ser6,
			$ser63,
			$ser7,
			$camp_id
        );

        if ($candidateID <= 0)
        {
            return $candidateID;
        }
            if ($candidateID =="exists")
        {
            return "exists";
        }

        /* Update extra fields. */
        $candidates->extraFields->setValuesOnEdit($candidateID);

        /* Update possible source list. */
        $sources = $candidates->getPossibleSources();
        $sourcesDifferences = ListEditor::getDifferencesFromList(
            $sources, 'name', 'sourceID', $sourceCSV
        );
        $candidates->updatePossibleSources($sourcesDifferences);

        /* Associate an exsisting resume if the user created a candidate with one. (Bulk) */
        if (isset($_POST['associatedAttachment']))
        {
            $attachmentID = $_POST['associatedAttachment'];

            $attachments = new Attachments($this->_siteID);
            $attachments->setDataItemID($attachmentID, $candidateID, DATA_ITEM_CANDIDATE);
        }

        /* Attach a resume if the user uploaded one. (http POST) */
        /* NOTE: This function cannot be called if parsing is enabled */
        else if (isset($_FILES['file']) && !empty($_FILES['file']['name']))
        {
            if (!eval(Hooks::get('CANDIDATE_ON_CREATE_ATTACHMENT_PRE'))) return;

            $attachmentCreator = new AttachmentCreator($this->_siteID);
            $attachmentCreator->createFromUpload(
                DATA_ITEM_CANDIDATE, $candidateID, 'file', false, true
            );

            if ($attachmentCreator->isError())
            {
                CommonErrors::fatal(COMMONERROR_FILEERROR, $this, $attachmentCreator->getError());
            }


            if ($attachmentCreator->duplicatesOccurred())
            {
                $this->listByView(
                    'This attachment has already been added to this candidate.'
                );
                return;
            }

            $isTextExtractionError = $attachmentCreator->isTextExtractionError();
            $textExtractionErrorMessage = $attachmentCreator->getTextExtractionError();

            // FIXME: Show parse errors!

            if (!eval(Hooks::get('CANDIDATE_ON_CREATE_ATTACHMENT_POST'))) return;
        }

        /**
         * User has loaded and/or parsed a resume. The attachment is saved in a temporary
         * file already and just needs to be attached. The attachment has also successfully
         * been DocumentToText converted, so we know it's a good file.
         */
        else if (LicenseUtility::isParsingEnabled())
        {
            /**
             * Description: User clicks "browse" and selects a resume file. User doesn't click
             * upload. The resume file is STILL uploaded.
             * Controversial: User uploads a resume, parses, etc. User selects a new file with
             * "Browse" but doesn't click "Upload". New file is accepted.
             * It's technically correct either way, I'm opting for the "use whats in "file"
             * box over what's already uploaded method to avoid losing resumes on candidate
             * additions.
             */
            $newFile = FileUtility::getUploadFileFromPost($this->_siteID, 'addcandidate', 'documentFile');

            if ($newFile !== false)
            {
                $newFilePath = FileUtility::getUploadFilePath($this->_siteID, 'addcandidate', $newFile);

                $tempFile = $newFile;
                $tempFullPath = $newFilePath;
            }
            else
            {
                $attachmentCreated = false;

                $tempFile = false;
                $tempFullPath = false;

                if (isset($_POST['documentTempFile']) && !empty($_POST['documentTempFile']))
                {
                    $tempFile = $_POST['documentTempFile'];
                    // Get the path of the file they uploaded already to attach
                    $tempFullPath = FileUtility::getUploadFilePath(
                        $this->_siteID,   // ID of the containing site
                        'addcandidate',   // Sub-directory in their storage
                        $tempFile         // Name of the file (not pathed)
                    );
                }
            }

            if ($tempFile !== false && $tempFullPath !== false)
            {
                if (!eval(Hooks::get('CANDIDATE_ON_CREATE_ATTACHMENT_PRE'))) return;

                $attachmentCreator = new AttachmentCreator($this->_siteID);
                $attachmentCreator->createFromFile(
                    DATA_ITEM_CANDIDATE, $candidateID, $tempFullPath, $tempFile, '', true, true
                );

                if ($attachmentCreator->isError())
                {
                    CommonErrors::fatal(COMMONERROR_FILEERROR, $this, $attachmentCreator->getError());
                }


                if ($attachmentCreator->duplicatesOccurred())
                {
                    $this->listByView(
                        'This attachment has already been added to this candidate.'
                    );
                    return;
                }

                $isTextExtractionError = $attachmentCreator->isTextExtractionError();
                $textExtractionErrorMessage = $attachmentCreator->getTextExtractionError();

                if (!eval(Hooks::get('CANDIDATE_ON_CREATE_ATTACHMENT_POST'))) return;

                // Remove the cleanup cookie since the file no longer exists
                setcookie('RESUFLO_SP_TEMP_FILE', '');

                $attachmentCreated = true;
            }

            if (!$attachmentCreated && isset($_POST['documentText']) && !empty($_POST['documentText']))
            {
                // Resume was pasted into the form and not uploaded from a file

                if (!eval(Hooks::get('CANDIDATE_ON_CREATE_ATTACHMENT_PRE'))) return;

                $attachmentCreator = new AttachmentCreator($this->_siteID);
                $attachmentCreator->createFromText(
                    DATA_ITEM_CANDIDATE, $candidateID, $_POST['documentText'], 'MyResume.txt', true
                );

                if ($attachmentCreator->isError())
                {
                    CommonErrors::fatal(COMMONERROR_FILEERROR, $this, $attachmentCreator->getError());
                }

                if ($attachmentCreator->duplicatesOccurred())
                {
                    $this->listByView(
                        'This attachment has already been added to this candidate.'
                    );
                    return;
                }

                if (!eval(Hooks::get('CANDIDATE_ON_CREATE_ATTACHMENT_POST'))) return;
            }
        }

        /* Create a text resume if the user posted one. (automated tool) */
        else if (!empty($textResumeBlock))
        {
            $attachmentCreator = new AttachmentCreator($this->_siteID);
            $attachmentCreator->createFromText(
                DATA_ITEM_CANDIDATE, $candidateID, $textResumeBlock, $textResumeFilename, true
            );

            if ($attachmentCreator->isError())
            {
                CommonErrors::fatal(COMMONERROR_FILEERROR, $this, $attachmentCreator->getError());
                return;
                //$this->fatal($attachmentCreator->getError());
            }
            $isTextExtractionError = $attachmentCreator->isTextExtractionError();
            $textExtractionErrorMessage = $attachmentCreator->getTextExtractionError();

            // FIXME: Show parse errors!
        }


        if (!eval(Hooks::get('CANDIDATE_ON_ADD_POST'))) return;

        return $candidateID;
    }

    /**
     * Processes an Add Activity / Change Status form and displays
     * candidates/AddActivityChangeStatusModal.tpl. This is factored out
     * for code clarity.
     *
     * @param boolean from joborders module perspective
     * @param integer "regarding" job order ID or -1
     * @param string module directory
     * @return void
     */
    private function _addActivityChangeStatus($isJobOrdersMode, $regardingID,
        $directoryOverride = '', $list='')
    {
        $notificationHTML = '';

        $pipelines = new Pipelines($this->_siteID);
        $statusRS = $pipelines->getStatusesForPicking();
		// Getting attachment path for scheduling events
		if(isset($_REQUEST['template_id'])){
			$schTemplate_id = $_REQUEST['template_id'];
			$sqlSchTemplate = "SELECT attachment FROM email_template WHERE email_template_id = $schTemplate_id";
			$exeSchTemplate = mysql_fetch_assoc(mysql_query($sqlSchTemplate));
			$schAttachmentName = $exeSchTemplate['attachment'];
			//$schAttachmentPath = "./status_attachment/".$schAttachmentName;
			$schAttachmentPath = STATUS_EMAIL_ATTACHMENT_PATH.$schAttachmentName;
		}
		
		// Getting attachment path for status change
		if(isset($_REQUEST['email_template_id'])){
			$statusTemplate_id = $_REQUEST['email_template_id'];
			$sqlStatusTemplate = "SELECT attachment FROM email_template WHERE email_template_id = $statusTemplate_id";
			$exeStatusTemplate = mysql_fetch_assoc(mysql_query($sqlStatusTemplate));
			$statusAttachmentName = $exeStatusTemplate['attachment'];
			//$statusAttachmentPath = "./status_attachment/".$statusAttachmentName;
			$statusAttachmentPath = STATUS_EMAIL_ATTACHMENT_PATH.$statusAttachmentName;
		}

        /* Module directory override for fatal() calls. */
        if ($directoryOverride != '')
        {
            $moduleDirectory = $directoryOverride;
        }
        else
        {
            $moduleDirectory = $this->_moduleDirectory;
        }

        /* Bail out if we don't have a valid candidate ID. */
        if (!$this->isRequiredIDValid('candidateID', $_POST))
        {
            CommonErrors::fatalModal(COMMONERROR_BADINDEX, $this, 'Invalid candidate ID.');
        }

        /* Do we have a valid status ID. */
        /*if (!$this->isOptionalIDValid('statusID', $_POST))
        {
            $statusID = -1;
        }
        else
        {
            $statusID = $_POST['statusID'];
        }*/

		if (!$this->isOptionalIDValid('statusID', $_POST))
        {
            $statusID = -1;
        }
        else
        {
            $statusID = $_POST['statusID'];
        }
		
        $candidateID = (integer) $_POST['candidateID'];
		$statusID = $_POST['statusID'];

        if (!eval(Hooks::get('CANDIDATE_ON_ADD_ACTIVITY_CHANGE_STATUS_PRE'))) return;

        // TODO, find where to put this in the hook system :/
        $dead = (integer) $_POST['dead'];
        $future = (integer) $_POST['future'];
        $sql = 'update candidate_joborder set dead = "'.$dead.'", future = "'.$future.'" where candidate_id = "'.$candidateID.'"';
        $res = mysql_query($sql);
        $deadfutureedChanged = mysql_affected_rows();

        if ($this->isChecked('addActivity', $_POST))
        {
            /* Bail out if we don't have a valid job order ID. */
            if (!$this->isOptionalIDValid('activityTypeID', $_POST))
            {
                CommonErrors::fatalModal(COMMONERROR_BADINDEX, $this, 'Invalid activity type ID.');
            }

            $activityTypeID = $_POST['activityTypeID'];

            $activityNote = $this->getTrimmedInput('activityNote', $_POST);

            $activityNote = htmlspecialchars($activityNote);

            // FIXME: Move this to a highlighter-method? */
            if (strpos($activityNote, 'Status change: ') === 0)
            {
                foreach ($statusRS as $data)
                {
                    $activityNote = StringUtility::replaceOnce(
                        $data['status'],
                        '<span style="color: #ff6c00;">' . $data['status'] . '</span>',
                        $activityNote
                    );
                }
            }

            /* Add the activity entry. */
            $activityEntries = new ActivityEntries($this->_siteID);
            $activityID = $activityEntries->add(
                $candidateID,
                DATA_ITEM_CANDIDATE,
                $activityTypeID,
                $activityNote,
                $this->_userID,
                $regardingID
            );
            $activityTypes = $activityEntries->getTypes();
            $activityTypeDescription = ResultSetUtility::getColumnValueByIDValue(
                $activityTypes, 'typeID', $activityTypeID, 'type'
            );

            $activityAdded = true;
        }
        else
        {
            $activityAdded = false;
            $activityNote = '';
            $activityTypeDescription = '';
        }

        //if ($regardingID <= 0 || $statusID == -1)
		if ($regardingID <= 0 || $statusID == -1)
        {
            $statusChanged = false;
            $oldStatusDescription = '';
            $newStatusDescription = '';
        }
        else
        {
            $data = $pipelines->get($candidateID, $regardingID);

            /* Bail out if we got an empty result set. */
            if (empty($data))
            {
                $this->fatalModal(
                    'The specified pipeline entry could not be found.'
                );
            }
			
            $validStatus = ResultSetUtility::findRowByColumnValue(
                $statusRS, 'statusID', $statusID
            );

			/* If the status is invalid or unchanged, don't mess with it. */
            if ($validStatus === false || $statusID == $data['status'])
            {
                $oldStatusDescription = '';
                $newStatusDescription = '';
                $statusChanged = false;
            }
            else
            {
                $oldStatusDescription = $data['status'];
                $newStatusDescription = ResultSetUtility::getColumnValueByIDValue(
                    $statusRS, 'statusID', $statusID, 'status'
                );

                if ($oldStatusDescription != $newStatusDescription)
                {
                    $statusChanged = true;
                }
                else
                {
                    $statusChanged = false;
                }
            }

            if ($statusChanged && $this->isChecked('triggerEmail', $_POST))
            {
                $customMessage = $this->getTrimmedInput('customMessage', $_POST);

                // FIXME: Actually validate the e-mail address?
                if (empty($data['candidateEmail']))
                {
                    $email = '';
                    $notificationHTML = '<p><span class="bold">Error:</span> An E-mail notification'
                        . ' could not be sent to the candidate because the candidate'
                        . ' does not have a valid e-mail address.</p>';
                }
                else if (empty($customMessage))
                {
                    $email = '';
                    $notificationHTML = '<p><span class="bold">Error:</span> An E-mail notification'
                        . ' will not be sent because the message text specified was blank.</p>';
                }
                else if ($this->_accessLevel == ACCESS_LEVEL_DEMO)
                {
                    $email = '';
                    $notificationHTML = '<p><span class="bold">Error:</span> Demo users can not send'
                        . ' E-Mails.  No E-Mail was sent.</p>';
                }
                else
                {
                    $email = $data['candidateEmail'];
                    $notificationHTML = '<p>An E-mail notification has been sent to the candidate for Status change.</p>';
                }
            }
            else
            {
                $email = '';
                $customMessage = '';
                $notificationHTML = '<p>No E-mail notification has been sent to the candidate for Status change.</p>';
            }

            /* Set the pipeline entry's status, but don't send e-mails for now. */
            $pipelines->setStatus(
                $candidateID, $regardingID, $statusID, $email, $customMessage ,$statusAttachmentPath , $statusAttachmentName
            );

            /* If status = placed, and open positions > 0, reduce number of open positions by one. */
            if ($statusID == PIPELINE_STATUS_PLACED && is_numeric($data['openingsAvailable']) && $data['openingsAvailable'] > 0)
            {
                $jobOrders = new JobOrders($this->_siteID);
                $jobOrders->updateOpeningsAvailable($regardingID, $data['openingsAvailable'] - 1);
            }
        }

        if ($this->isChecked('scheduleEvent', $_POST))
        {
            /* Bail out if we received an invalid date. */
            $trimmedDate = $this->getTrimmedInput('dateAdd', $_POST);
            if (empty($trimmedDate) ||
                !DateUtility::validate('-', $trimmedDate, DATE_FORMAT_MMDDYY))
            {
                CommonErrors::fatalModal(COMMONERROR_MISSINGFIELDS, $this, 'Invalid date.');
            }

            /* Bail out if we don't have a valid event type. */
            if (!$this->isRequiredIDValid('eventTypeID', $_POST))
            {
                CommonErrors::fatalModal(COMMONERROR_BADINDEX, $this, 'Invalid event type ID.');
            }

            /* Bail out if we don't have a valid time format ID. */
            if (!isset($_POST['allDay']) ||
                ($_POST['allDay'] != '0' && $_POST['allDay'] != '1'))
            {
                CommonErrors::fatalModal(COMMONERROR_MISSINGFIELDS, $this, 'Invalid time format ID.');
            }

            $eventTypeID = $_POST['eventTypeID'];

            if ($_POST['allDay'] == 1)
            {
                $allDay = true;
            }
            else
            {
                $allDay = false;
            }

            $publicEntry = $this->isChecked('publicEntry', $_POST);
            $candidatePhoneReminderSend = $this->isChecked('candidatePhoneReminderSend', $_POST);
            $reminderEnabled = $this->isChecked('reminderToggle', $_POST);
            $reminderEmail = $this->getTrimmedInput('sendEmail', $_POST);
            $reminderTime  = $this->getTrimmedInput('reminderTime', $_POST);
            $duration = $this->getTrimmedInput('duration', $_POST);;

            /* Is this a scheduled event or an all day event? */
            if ($allDay)
            {
                $date = DateUtility::convert(
                    '-', $trimmedDate, DATE_FORMAT_MMDDYY, DATE_FORMAT_YYYYMMDD
                );

                $hour = 12;
                $minute = 0;
                $meridiem = 'AM';
            }
            else
            {
                /* Bail out if we don't have a valid hour. */
                if (!isset($_POST['hour']))
                {
                    CommonErrors::fatalModal(COMMONERROR_MISSINGFIELDS, $this, 'Invalid hour.');
                }

                /* Bail out if we don't have a valid minute. */
                if (!isset($_POST['minute']))
                {
                    CommonErrors::fatalModal(COMMONERROR_MISSINGFIELDS, $this, 'Invalid minute.');
                }

                /* Bail out if we don't have a valid meridiem value. */
                if (!isset($_POST['meridiem']) ||
                    ($_POST['meridiem'] != 'AM' && $_POST['meridiem'] != 'PM'))
                {
                    $this->fatalModal(
                        'Invalid meridiem value.', $moduleDirectory
                    );
                }

                $hour     = $_POST['hour'];
                $minute   = $_POST['minute'];
                $meridiem = $_POST['meridiem'];

                /* Convert formatted time to UNIX timestamp. */
                $time = strtotime(
                    sprintf('%s:%s %s', $hour, $minute, $meridiem)
                );

                /* Create MySQL date string w/ 24hr time (YYYY-MM-DD HH:MM:SS). */
                $date = sprintf(
                    '%s %s',
                    DateUtility::convert(
                        '-',
                        $trimmedDate,
                        DATE_FORMAT_MMDDYY,
                        DATE_FORMAT_YYYYMMDD
                    ),
                    date('H:i:00', $time)
                );
            }

            $description = $this->getTrimmedInput('description', $_POST);
            $title       = $this->getTrimmedInput('title', $_POST);
            
            $venue       = $this->getTrimmedInput('venue', $_POST);
            $tomeet       = $this->getTrimmedInput('tomeet', $_POST);
            $contactnumber = $this->getTrimmedInput('contactnumber', $_POST);
            $emailid       = $this->getTrimmedInput('emailid', $_POST);
                        
            $reminderToCandidateDays       = $this->getTrimmedInput('reminderToCandidateDays', $_POST);
            $reminderTemplateId       = $this->getTrimmedInput('reminder_template_id', $_POST);
            $reminderEmailMessage       = $this->getTrimmedInput('reminderEmailMessage', $_POST);
            $thanksToCandidateDays       = $this->getTrimmedInput('thanksToCandidateDays', $_POST);
            $thanksTemplateId       = $this->getTrimmedInput('thanks_template_id', $_POST);
            $thanksEmailMessage       = $this->getTrimmedInput('thanksEmailMessage', $_POST);
                        
            /* Bail out if any of the required fields are empty. */
            if (empty($title))
            {
                CommonErrors::fatalModal(COMMONERROR_MISSINGFIELDS, $this);
                return;
                /*$this->fatalModal(
                    'Required fields are missing.', $moduleDirectory
                );*/
            }

            if ($regardingID > 0)
            {
                $eventJobOrderID = $regardingID;
            }
            else
            {
                $eventJobOrderID = -1;
            }
            $calendar = new Calendar($this->_siteID);
            $eventID = $calendar->addEventWithReminder(
                $eventTypeID, $date, $description, $allDay, $this->_userID,
                $candidateID, DATA_ITEM_CANDIDATE, $eventJobOrderID, $title,$venue,$tomeet,$contactnumber,$emailid,
                $duration, $reminderEnabled, $reminderEmail, $reminderTime,
                $publicEntry, $_SESSION['RESUFLO']->getTimeZoneOffset(), 0, $reminderToCandidateDays, $reminderEmailMessage,
                0, $thanksToCandidateDays, $thanksEmailMessage, $candidatePhoneReminderSend
            );
			$titleBackup = $title;
			//Adding event in google Calender
			//include_once('./google calender/config.php');
			// Fetching event details from Database for corresponding IDs
			$sqlEventType = "SELECT *FROM calendar_event_type WHERE calendar_event_type_id = $eventTypeID";
			$exeEventType = mysql_fetch_assoc(mysql_query($sqlEventType));
			$eventType = $exeEventType['short_description'];

			// Fetching candidate details from Database for corresponding IDs
			$sqlCand = "SELECT first_name, last_name, email1 FROM candidate WHERE candidate_id = $candidateID";
			$exesqlCand = mysql_fetch_assoc(mysql_query($sqlCand));
			$candName = $exesqlCand['first_name']." ".$exesqlCand['last_name']." <".$exesqlCand['email1'].">";

			// generate query to get event list


			$explodeDate = explode("-",$trimmedDate);
			$smonth = $explodeDate[0];
			$sdate = $explodeDate[1];
			$syear = $explodeDate[2];
			$eventHour = date('H',$time);
			$eventMin = date('i',$time);
			//$title = htmlentities("Event Type:".$eventType."\n\rCandidate Name:".$candName."\n\r".$title."\n\r".$venue."\n\r".$tomeet."\n\r".$contactnumber);
			$title = ("EventType: ".$eventType."\n\r CandidateName: ".$candName." \n\r".$title." \n\r".$venue." \n\r".$tomeet." \n\r".$contactnumber);
			$start = date(DATE_ATOM, mktime($eventHour, $eventMin,$_POST['minute'], $smonth, $sdate, $syear));
			if($_POST['duration']>60)
			{
				$endHours=round($_POST['duration']/60);
				$endMins =round($_POST['duration']%60);
			}
			else
			{
				$endHours=0;
				$endMins =$_POST['duration'];
				
			}
		   // $end = date(DATE_ATOM, mktime($_POST['hour'], $_POST['minute']+1,$_POST['minute']+1, $smonth, $sdate, $syear));

			$end = date(DATE_ATOM, mktime($eventHour+$endHours,$eventMin+$endMins,10, $smonth, $sdate, $syear));
			// construct event object
			// save to server      
			$tzOffset = substr($_POST['TimeZone'],3,3);
				
				
				  // construct event object
				  // save to server      
				  try {
					$user=$this->_userID;
						$start = substr($start,0,19);
						//$start = $start.$tzOffset.":00";
						$end   = substr($end,0,19);
						//$end   = $end.$tzOffset.":00";
						$when->startTime = $start;
						$when->endTime = $end;
						$event->when = array($when);        
						$event_id = $createdEvent->id->text;  
			} catch (Zend_Gdata_App_Exception $e) {
			echo "Error: " . $e->getResponse();
			}
            if ($eventID <= 0)
            {
                $this->fatalModal(
                    'Failed to add calendar event.', $moduleDirectory
                );
            }
			
			 /* Extract the date parts from the specified date. */
            $parsedDate = strtotime($date);
            $formattedDate = date('l, F jS, Y g:i', $parsedDate)." ".$meridiem ;

            $calendar = new Calendar($this->_siteID);
            $calendarEventTypes = $calendar->getAllEventTypes();

            $eventTypeDescription = ResultSetUtility::getColumnValueByIDValue(
                $calendarEventTypes, 'typeID', $eventTypeID, 'description'
            );

			// Mail sending code starts
			$candidateID = $_POST['candidateID'];
			$currentDateFormat = date('M d,Y g:i');
			$username = $_SESSION['RESUFLO']->getFullName();
			$candidates = new Candidates($this->_siteID);
			$data = $candidates->get($candidateID);
			$schedultEventTemplate =  $_REQUEST['emailMessage'];
			$stringsToFind = array(
            '%CANDOWNER%',
            '%CANDFIRSTNAME%',
            '%CANDFULLNAME%',
            '%CSOURCE%',
			'%EVENTTYPE%',
			'%TITLE%',
			'%VENUE%',
			'%TOMEET%',
			'%DATETIMEEVENT%',
			'%DESC%',
			'%USERFULLNAME%',
			'%DATETIME%'
        );
        $replacementStrings = array(
            $data['ownerFullName'],
            $data['firstName'],
            $data['firstName'] . ' ' . $data['lastName'],
            $data['source'],
			$eventTypeDescription,
			$title,
			$venue,
			$tomeet,
			$formattedDate,
			$description,
			$username,
			$currentDateFormat
						
        );
        $schedultEventTemplate = str_replace(
            $stringsToFind,
            $replacementStrings,
            $schedultEventTemplate
        );
		
			
			if ($this->isChecked('emailcheck', $_POST)){

                $userid=$_SESSION['RESUFLO']->getUserID();

                $emailReceiver = $data['email1'];
                $arrmail = explode(",", $emailReceiver);
                $subject = "Scheduled Event Details";
                $msg = nl2br($schedultEventTemplate);

                $sqlSignature = "select signature from user where user_id = $userid";
                $exeSignature = mysql_fetch_assoc(mysql_query($sqlSignature));
                $msg = $msg.'<br><br>'.$exeSignature['signature'];
                $body = eregi_replace("[\]",'',$msg);
                try{
                    file_put_contents('C:/Windows/Temp/ResufloLog/UserMailerCaller.txt', PHP_EOL.(new DateTime())->format('d m Y H:i:s').'===== START candidate', FILE_APPEND);
                    $email = new UserMailer($userid);
                    $emailSent = $email->SendEmail($arrmail, null, $subject, $body, $schAttachmentPath);
                    file_put_contents('C:/Windows/Temp/ResufloLog/UserMailerCaller.txt', '=> END ====='.$emailSent, FILE_APPEND);
                }
                catch (Exception $e) {
                    file_put_contents('C:/Windows/Temp/ResufloLog/UserMailerCaller.txt', (new DateTime())->format('d m Y H:i:s').'candidate ====='.$e->getMessage().PHP_EOL, FILE_APPEND);
                }
                
                $eventHTML = sprintf(
                    '<p>An event of type <span class="bold">%s</span> has been scheduled on <span class="bold">%s</span>.</p>
					<p>An E-mail notification has been sent to the candidate for Scheduled event.</p>',
                    htmlspecialchars($eventTypeDescription),
                    htmlspecialchars($formattedDate)

                );


            }// Mail code ends

			else {
				$eventHTML = sprintf(
					'<p>An event of type <span class="bold">%s</span> has been scheduled on <span class="bold">%s</span>.</p>',
					htmlspecialchars($eventTypeDescription),
					htmlspecialchars($formattedDate)
	
				);
			}

            $eventScheduled = true;
        }
        else
        {
            $eventHTML = '<p>No event has been scheduled.</p>';
            $eventScheduled = false;
        }

        if (isset($_GET['onlyScheduleEvent']))
        {

            $onlyScheduleEvent = true;
            $userid=$_SESSION['RESUFLO']->getUserID();
            $sqlEmailConf = "SELECT * FROM dripmarket_configuration WHERE userid = $userid";
            $exeEmailConf = mysql_fetch_assoc(mysql_query($sqlEmailConf));
            $emailSender = $exeEmailConf['fromemail'];
            $smtphost = $exeEmailConf['smtphost'];
            $smtpport = $exeEmailConf['smtpport'];
            $smtpUsername = $exeEmailConf['smtpuser'];
            $smtpPassword = $exeEmailConf['smtppassword'];
            $fromName = $exeEmailConf['fromname'];
            $emailType = $exeEmailConf['email_type'];
            $bccemail = $exeEmailConf['bccemail'];
            
            include_once('./OutlookMailer/index.php'); 
            $gdate = str_replace(" ","T",$date);
            $endtime = date("Y-m-d H:i:s",strtotime($date." +".$duration." minutes"));
            $gendtime = str_replace(" ","T",$endtime);
            $timezoneValue = $calendar->getTimezone();

            //$timezoneValue = str_replace("GMT","",$timezoneValue);
            //$endtime = $endtime.' '.$timezoneValue;
            //$endtime = $endtime.str_replace("+","L",$timezoneValue);

            //$date = str_replace(" ","T",$date);
            //$date = $date.' '.$timezoneValue;
            //$date = $date.str_replace("+","L",$timezoneValue);

            $description = '<p>Candidate Name: '. $exesqlCand['first_name'].' '.$exesqlCand['last_name'].'</p>'
                    . '<p>Person to meet: '. $tomeet.PHP_EOL.'</p>'
                    . '<p>Contact Number: '. $contactnumber.PHP_EOL.'</p>'
                    . '<p>Email Id: '. $emailid.PHP_EOL.'</p>'
                    . '<p>Duration: '.$duration.PHP_EOL.'</p>'
                    . '<p>Description: '.$description.PHP_EOL.'</p>'
                    . '<p><a href="https://resuflocrm.com/index.php?m=candidates&list=new_candidates&a=show&candidateID='.$candidateID.'">View Candidate</a> </p>';

            $userid = $_SESSION['RESUFLO']->getUserID();
            $getQuery = mysql_query("SELECT outlook_username, google_username, google_access_token, google_pass FROM user WHERE user_id = '$userid'");
            $result = mysql_fetch_assoc($getQuery);
            $to = $result['outlook_username'];
            $accessToken = $result['google_access_token'];

            //$sendEmail = SendWithSMTP($exeEmailConf['smtphost'],$exeEmailConf['smtpuser'],$exeEmailConf['smtppassword'],$exeEmailConf['smtpport'],$exeEmailConf['fromname'],$exeEmailConf['fromemail'],$exeEmailConf['bccemail'],$exeEmailConf['email_type'],$subject,$body,$to,'','');

//            $UserMailer = new UserMailer($userid);
//            $UserMailer->SendWithUser('REM event', $description, $to);
            
            // UnSerializing Constant Array
            $offsets_timezone = unserialize (OffsetsTimezone);
            $offset = '';
            foreach($offsets_timezone as $key => $value){
                $i = 0;
                if($timezoneValue == $value[$i+1]){
                    $offset = $value[$i];
                }
                $i++;
            }
//            include_once('./lib/UserMailer.php');
//            $email = new UserMailer($userid);
//            $emailSent = $email->SendEmail($to, $titleBackup, $description);
//            
//            //########################### Removed temporarily for outlook was crashing site #########################################
//            try{
//                $OutlookResult = AddEventToOutlook($smtphost, $smtpport, $smtpUsername, $smtpPassword, $emailSender, 'REM event', $emailType, $bccemail, $to, $titleBackup, $venue, $date, $endtime, $allDay, $description, $description , $timezoneValue, $offset);
//
//                echo $OutlookResult;
//            } catch (Exception $ex) {
//               echo "Outlook: ".$ex->getMessage();
//            }
            //########################### Removed temporarily for Google OAuth, enable once app is approved #########################################
            try{
                include_once('./GoogleCalAPI/GoogleCalendar.php');
                $newAccessToken = getNewAccessToken($accessToken);
                $GoogleResult = AddEventToGoogle($newAccessToken, $titleBackup, $venue, $gdate, $gendtime, $timezoneValue, $allDay, $description, $description);
                echo $GoogleResult;
            } catch (Exception $ex) {
                echo "GoogleCal: ".$ex->getMessage();
            }
        }

        else
        {
            $onlyScheduleEvent = false;
        }

        if (!$statusChanged && !$activityAdded && !$eventScheduled && empty($deadfutureedChanged))
        {
            $changesMade = false;
        }
        else
        {
            $changesMade = true;
        }

        if (!eval(Hooks::get('CANDIDATE_ON_ADD_ACTIVITY_CHANGE_STATUS_POST'))) return;

        $sql = 'select dead, future from candidate_joborder where candidate_id = "'.$candidateID.'"';
        $res = mysql_query($sql);
        $row = mysql_fetch_assoc($res);
        $dead = $row['dead'];
        $future = $row['future'];
        $this->_template->assign('dead', $dead);
        $this->_template->assign('future', $future);

        $this->_template->assign('candidateID', $candidateID);
        $this->_template->assign('regardingID', $regardingID);
        $this->_template->assign('oldStatusDescription', $oldStatusDescription);
        $this->_template->assign('newStatusDescription', $newStatusDescription);
        if (!empty($deadfutureedChanged)) $statusChanged = $deadfutureedChanged;
        $this->_template->assign('statusChanged', $statusChanged);
        $this->_template->assign('activityAdded', $activityAdded);
        $this->_template->assign('activityDescription', $activityNote);
        $this->_template->assign('activityType', $activityTypeDescription);
        $this->_template->assign('eventScheduled', $eventScheduled);
        $this->_template->assign('eventHTML', $eventHTML);
        $this->_template->assign('notificationHTML', $notificationHTML);
        $this->_template->assign('onlyScheduleEvent', $onlyScheduleEvent);
        $this->_template->assign('changesMade', $changesMade);
        $this->_template->assign('isFinishedMode', true);
        $this->_template->assign('isJobOrdersMode', $isJobOrdersMode);
		 $this->_template->assign('list', true);
		$this->_template->assign('list', $this->list);
		$this->_template->assign('list1', true);
		$this->_template->assign('list1', $_REQUEST["list"]);
        $this->_template->display(
            './modules/candidates/AddActivityChangeStatusModal.tpl'
        );
    }

    /*
     * Sends mass emails from the datagrid
     */
    private function onEmailCandidates()
    {
		file_put_contents('C:/log/UserMailerCaller.txt', PHP_EOL.(new DateTime())->format('d m Y H:i:s').'===== START candidate', FILE_APPEND);
		$userid= $_SESSION['RESUFLO']->getuserid(); 
                $username = $_SESSION['RESUFLO']->getFullName();
		$useremail = $_SESSION['RESUFLO']->getEmail();
		if ($this->_accessLevel == ACCESS_LEVEL_DEMO)
        {
            CommonErrors::fatal(COMMONERROR_PERMISSION, $this, 'Sorry, but demo accounts are not allowed to send e-mails.');
        }

        if (isset($_POST['postback']))
			{	
				$sql ="select dripmarket_configuration.email_type,dripmarket_configuration.smtphost,dripmarket_configuration.smtppassword ,dripmarket_configuration.smtpport ,dripmarket_configuration.fromname,dripmarket_configuration.fromemail,dripmarket_configuration.bccemail, dripmarket_configuration.smtpuser FROM dripmarket_configuration WHERE userid = $userid";
			
			$res = mysql_query($sql);
			$row = mysql_fetch_assoc($res);
			$emailSubject = $_POST['emailSubject'];
                        $bodytext=$_POST['emailBody'];
		
			$candidate_ID = $_POST['candidateIDs'];
			
			$candidate_IDs = explode(',', $candidate_ID); 
			$out = array_pop($candidate_IDs);
			$emailTo = $_POST['emailTo'];
                        $tmpDestination = explode(",", $emailTo);
			$target_path = "fileupload/";
			$target_path = $target_path . basename( $_FILES['uploadedfile']['name']);
			$fileName = basename( $_FILES['uploadedfile']['name']);
			if($fileName){
				move_uploaded_file($_FILES['uploadedfile']['tmp_name'], $target_path);
				$filePath = "./fileupload/".$fileName;
			}
                        try{
                            file_put_contents('C:/inetpub/vhosts/resuflocrm.com/httpdocs/log/UserMailerCaller.txt', PHP_EOL.(new DateTime())->format('d m Y H:i:s').'===== START candidate', FILE_APPEND);
                            $email = new UserMailer($userid);
                            foreach($tmpDestination as &$toAddress)
                            {
                                $emailSent = $email->SendEmail(array($toAddress), null, $emailSubject, $bodytext, $filePath);
                                file_put_contents('C:/inetpub/vhosts/resuflocrm.com/httpdocs/log/UserMailerCaller.txt', '=> END ====='.$emailSent, FILE_APPEND);
                            }
                        }
                        catch (Exception $e) {
                            file_put_contents('C:/Windows/Temp/ResufloLog/UserMailerCaller.txt', (new DateTime())->format('d m Y H:i:s').'candidate ====='.$e->getMessage().PHP_EOL, FILE_APPEND);
                        }
		 
			foreach ($candidate_IDs as &$value) {
					$sql = mysql_query("select last_name,  first_name from candidate where  candidate_id  = '$value' ");
					$row = mysql_fetch_assoc($sql);
					$first_name= $row['first_name'];
					$last_name= $row['last_name']; 
					$name = $first_name." ".$last_name;
					$sql = mysql_query("INSERT INTO activity (data_item_id, data_item_type, joborder_id, entered_by, type, notes, site_id, date_created, date_modified ) 		       			 VALUES ( '$value', '100', '0', '$userid', '200', '$emailSubject', '.$this->_siteID.', NOW(), NOW() ) ");
				 
				 $this->_template->assign('candidate_IDs', $candidate_ID);
			}
			if($candidate_IDs){	
				unset($candidate_IDs);
			}
			if($filePath)
			{
				unlink($filePath);
			}
			   $this->_template->assign('active', $this);
			   $this->_template->assign('success', true);
			  
			   $this->_template->assign('success_to', $emailTo);
			   $this->_template->display('./modules/candidates/SendEmail.tpl');
			}
        else
        {
                 $dataGrid = DataGrid::getFromRequest();

            	 $candidateIDs = $dataGrid->getExportIDs();	

                 if(empty($candidateIDs)){
                     $candidateIDs = $_SESSION['RESUFLO']->emailCandidateID;
                 }else if (!empty($_SESSION['RESUFLO']->emailCandidateID) && $candidateIDs != $_SESSION['RESUFLO']->emailCandidateID){
                     $candidateIDs = $_SESSION['RESUFLO']->emailCandidateID;
                 }
                    
                    /* Validate each ID */
                    foreach ($candidateIDs as $index => $candidateID)
                    {
                        if (!$this->isRequiredIDValid($index, $candidateIDs))
                        {
                            CommonErrors::fatalModal(COMMONERROR_BADINDEX, $this, 'Invalid candidate ID.');
                            return;
                        }
                    }

                    $db_str = implode(", ", $candidateIDs);

                    $db = DatabaseConnection::getInstance();

                    $rs = $db->getAllAssoc(sprintf(
                        'SELECT candidate_id, email1, email2 '
                        . 'FROM candidate '
                        . 'WHERE candidate_id IN (%s)',
                        $db_str
                    ));
                    $userid= $_SESSION['RESUFLO']->getuserid(); 
                    $rs1 = mysql_query("SELECT signature FROM user WHERE user_id ='$userid'");
                    $row = mysql_fetch_assoc($rs1);  
                    $signature = $row['signature'];
                    //$this->_template->assign('privledgedUser', $privledgedUser);
                    $this->_template->assign('active', $this);
                    $this->_template->assign('success', false);
                    $this->_template->assign('recipient', $signature );
                    $this->_template->assign('candidateIDs', $candidateIDs );
                    $this->_template->assign('recipients', $rs);
                    //Removing session value for the next selection
                    $_SESSION['RESUFLO']->emailCandidateID = NULL;
                    $this->_template->display('./modules/candidates/SendEmail.tpl');
        }
    }
    private function get_status_email_template()
    {	
        $loginedUser = $_SESSION['RESUFLO']->getUserID();
        $getQuery = mysql_query("SELECT email_template_id,name,text,in_use  FROM email_template WHERE entered_by = '$loginedUser' and (tag = 'EMAIL_TEMPLATE_STATUSCHANGE' OR tag = 'EMAIL_TEMPLATE_SCHEDULEEVENT')");
        $emailArray=array();
        $i=0;
        while($row = mysql_fetch_array($getQuery))
        {
           $emailArray[$i]=$row;
           $i++;
        }
        $this->_template->assign('active', $this);
        $this->_template->assign('emailArray', $emailArray);
    }
	 private function onEmailcandidate()
    {	 
		
	//$resourcedetail = $this->_db->query($sql);
		$username = $_SESSION['RESUFLO']->getFullName();
		$useremail = $_SESSION['RESUFLO']->getEmail();
		
		$userid = $_SESSION['RESUFLO']->getuserid(); 
			function curPageURL() {
			$pageURL = 'http';
			if ($_SERVER["HTTPS"] == "on") {$pageURL .= "s";}
			$pageURL .= "://";
			if ($_SERVER["SERVER_PORT"] != "80") {
			$pageURL .= $_SERVER["SERVER_NAME"].":".$_SERVER["SERVER_PORT"].$_SERVER["REQUEST_URI"];
			 } else {
			 $pageURL .= $_SERVER["SERVER_NAME"].$_SERVER["REQUEST_URI"];
			 }
			 return $pageURL;
		}
		$url = curPageURL();
		$url = urldecode($url); 
		$email_id = end(explode("&", $url));
		
		if ($this->_accessLevel == ACCESS_LEVEL_DEMO)
        {
            CommonErrors::fatal(COMMONERROR_PERMISSION, $this, 'Sorry, but demo accounts are not allowed to send e-mails.');
        }
        if (isset($_POST['postback']))
        { 
			
			$sql ="select dripmarket_configuration.email_type,dripmarket_configuration.smtphost,dripmarket_configuration.smtppassword ,dripmarket_configuration.smtpport ,dripmarket_configuration.fromname,dripmarket_configuration.fromemail,dripmarket_configuration.bccemail, dripmarket_configuration.smtpuser FROM dripmarket_configuration WHERE userid = $userid";
		
		$res = mysql_query($sql);
		$row = mysql_fetch_assoc($res);
		
		//print_r($row);
		//print_r($_POST);
		$candidate_id = $_POST['candidate_id'];
		$candidate_name = $_POST['candidate_name'];
		$subject = $_POST['emailSubject'];
                $bodytext=$_POST['emailBody'];
		$emailTo = $_POST['emailTo'];
                $tmpDestination = explode(",", $emailTo);
		$target_path = "fileupload/";
		$target_path = $target_path . basename( $_FILES['uploadedfile']['name']);
		$fileName = basename( $_FILES['uploadedfile']['name']);;
		if($fileName){
			move_uploaded_file($_FILES['uploadedfile']['tmp_name'], $target_path);
			$filePath = "./fileupload/".$fileName;
		}		
                try{
                    file_put_contents('C:/Windows/Temp/ResufloLog/UserMailerCaller.txt', PHP_EOL.(new DateTime())->format('d m Y H:i:s').'===== START candidate', FILE_APPEND);
                    $email = new UserMailer($userid);
                    $emailSent = $email->SendEmail($tmpDestination, null, $subject, $bodytext, $filePath);
                    file_put_contents('C:/Windows/Temp/ResufloLog/UserMailerCaller.txt', '=> END ====='.$emailSent, FILE_APPEND);
                }
                catch (Exception $e) {
                    file_put_contents('C:/Windows/Temp/ResufloLog/UserMailerCaller.txt', (new DateTime())->format('d m Y H:i:s').'candidate ====='.$e->getMessage().PHP_EOL, FILE_APPEND);
                }
	   	
		$datefunc=date("d-M-y");
		
		for($i =0 ; $i < count($tmpDestination) ; $i ++){
			$sql = mysql_query("INSERT INTO activity (data_item_id, data_item_type, joborder_id, entered_by, type, notes, site_id, date_created, date_modified ) 		       			 VALUES ( '$candidate_id', '100', '0', '$userid', '200', '$subject', '.$this->_siteID.', NOW(), NOW() ) ");
			
	}
	$this->_template->assign('candidate_id', $candidate_id);
		if($candidate_id){	
			unset($candidate_id);
		}
		if($filePath)
		{
			unlink($filePath);
		}
		
		$this->_template->assign('active', $this);
        $this->_template->assign('success', true);
		
        $this->_template->assign('candidate_name', $candidate_name);
	    $this->_template->assign('success_to', $emailTo);
        $this->_template->display('./modules/candidates/SendEmailTo.tpl');


die();
			$emailTo = $_POST['emailTo'];
            $emailSubject = $_POST['emailSubject'];
            $emailBody = $_POST['emailBody'];
			$filename = "./fileupload/".$_POST['filename'];
			if($filename=='')
				$type='not';
			else
				$type='pp';
			$tmpDestination = explode(",", $emailTo);
		    $destination = array();
		    foreach($tmpDestination as $emailDest)
            {
              $destination[] = array($emailDest, $emailDest);
            }
				$file = fopen($filename,'r'); 
				$data = fread($file,filesize($filename)); 
				fclose($file); 
				$semi_rand = md5(time()); 

				$mime_boundary = "==Multipart_Boundary_x{$semi_rand}x"; 
				$headers .="\nMIME-Version: 1.0\n" . 
				"Content-Type: multipart/mixed;\n" . 
				" boundary=\"{$mime_boundary}\""; 
				
				$emailBody = "This is a multi-part message in MIME format.\n\n" . 
										"--{$mime_boundary}\n" . 
										"Content-Type:text/html; charset=\"iso-8859-1\"\n" . 
										"Content-Transfer-Encoding: 7bit\n\n" . 
										$email_txt . "\n\n"; 
										
				$data =chunk_split(base64_encode($data)); 
				if($type=='pp')
					{
								$emailBody .= "--{$mime_boundary}\n" . 
								"Content-Type: {$fileatt_type};\n" . 
								" name=\"{$fileatt_name}\"\n" . 
								"Content-Transfer-Encoding: base64\n\n" . 
								$data . "\n\n" . 
				        		"--{$mime_boundary}--\n"; 
				 }
            $mailer = new Mailer(RESUFLO_ADMIN_SITE);
			
            // FIXME: Use sendToOne()?
           $mailerStatus = $mailer->send(
			  array($_SESSION['RESUFLO']->getEmail(), $_SESSION['RESUFLO']->getEmail()),
               
				$destination,
                $emailSubject,
                $emailBody,
				true,
                true
            );
			
            $this->_template->assign('active', $this);
            $this->_template->assign('success', true);
            $this->_template->assign('success_to', $emailTo);
            $this->_template->display('./modules/candidates/SendEmailTo.tpl');
        }
        else
        {
			
			//print_r($email_id);
      		$rs1 = mysql_query("SELECT signature FROM user WHERE user_id ='$userid'");
            $row = mysql_fetch_assoc($rs1);  
 		 	$signature = $row['signature'];
			$this->_template->assign('active', $this);
            $this->_template->assign('success', false);
			$this->_template->assign('userid', $userid );
            $this->_template->assign('recipients', $email_id );
			$this->_template->assign('recipient', $signature );
			$this->_template->display('./modules/candidates/SendEmailTo.tpl');
      
       }
  	}

    private function onShowQuestionnaire()
    {
        $candidateID = isset($_GET[$id='candidateID']) ? $_GET[$id] : false;
        $title = isset($_GET[$id='questionnaireTitle']) ? urldecode($_GET[$id]) : false;
        $printOption = isset($_GET[$id='print']) ? $_GET[$id] : '';
        $printValue = !strcasecmp($printOption, 'yes') ? true : false;

        if (!$candidateID || !$title)
        {
            CommonErrors::fatal(COMMONERROR_BADINDEX);
        }

        $candidates = new Candidates($this->_siteID);
        $cData = $candidates->get($candidateID);

        $questionnaire = new Questionnaire($this->_siteID);
        $qData = $questionnaire->getCandidateQuestionnaire($candidateID, $title);

        $attachment = new Attachments($this->_siteID);
        $attachments = $attachment->getAll(DATA_ITEM_CANDIDATE, $candidateID);
        if (!empty($attachments))
        {
            $resume = $candidates->getResume($attachments[0]['attachmentID']);
            $this->_template->assign('resumeText', str_replace("\n", "<br \>\n", htmlentities(DatabaseSearch::fulltextDecode($resume['text']))));
            $this->_template->assign('resumeTitle', htmlentities($resume['title']));
        }

        $this->_template->assign('active', $this);
        $this->_template->assign('candidateID', $candidateID);
        $this->_template->assign('title', $title);
        $this->_template->assign('cData', $cData);
        $this->_template->assign('qData', $qData);
        $this->_template->assign('print', $printValue);

        $this->_template->display('./modules/candidates/Questionnaire.tpl');
    }

    static function get_campaign_name($camp_id, $userid) {
        if ($camp_id == 'none') {
          $campName = "No campaign selected";
        } else if ($camp_id == '0') {
          $sql = "SELECT cname FROM dripmarket_compaign WHERE  `default` = '1' and `userid` = '$userid'";
          $resource= mysql_query($sql);
          $row = mysql_fetch_assoc($resource);
          $campName = $row['cname']." (Default campaign)";
        } else {
          $sql = "SELECT cname FROM dripmarket_compaign WHERE  `id`= '$camp_id'";
          $resource= mysql_query($sql);
          $row = mysql_fetch_assoc($resource);
          $campName = $row['cname'];
        }
        return $campName;
    }
}

?>
