/*
 * RESUFLO
 * Candidates Form Validation
 *
 * Copyright (C) 2005 - 2007 Econn Technologies, Inc.
 * All rights reserved.
 *
 * $Id: activityvalidator.js 2336 2007-04-14 22:01:51Z will $
 */

function checkActivityForm(form)
{
    var errorMessage = '';

    errorMessage += checkEventTitle();
	 errorMessage += checkEventEmail();

    if (errorMessage != '')
    {
        alert("Form Error:\n" + errorMessage);
        return false;
    }

    return true;
}

function checkEventTitle()
{
    var errorMessage = '';

    scheduleEvent = document.getElementById('scheduleEvent').checked;
    if (!scheduleEvent)
    {
        return '';
    }

    fieldValue = document.getElementById('title').value;
    fieldLabel = document.getElementById('titleLabel');
    if (fieldValue == '')
    {
        errorMessage = "    - You must enter an event title.\n";

        fieldLabel.style.color = '#ff0000';
    }
    else
    {
        fieldLabel.style.color = '#000';
    }

    return errorMessage;
}
function checkEventTimezone()
{
    var errorMessage = '';

    timezone = document.getElementById('TimeZone').value;
	fieldLabel = document.getElementById('TimeZoneLabel');
    if (timezone == 'Select TimeZone')
    {
        errorMessage = "    - You must Select TimeZone.\n";

        fieldLabel.style.color = '#ff0000';
    }
    else
    {
        fieldLabel.style.color = '#000';
    }

    return errorMessage;
}


function checkEventEmail()
{
    var errorMessage = '';
	var emailExp = /^[\w\-\.\+]+\@[a-zA-Z0-9\.\-]+\.[a-zA-z]{2,4}$/;
    scheduleEvent = document.getElementById('scheduleEvent').checked;
    if (!scheduleEvent)
    {
        return '';
    }

    fieldValue = document.getElementById('emailid').value;
    fieldLabel = document.getElementById('emailLabel');
   if (fieldValue!='' && !fieldValue.match(emailExp))
    {
        errorMessage = "    - You must enter valid Email Id.\n";

        fieldLabel.style.color = '#ff0000';
    }
    else
    {
        fieldLabel.style.color = '#000';
    }

    return errorMessage;
}
