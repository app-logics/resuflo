<?php /* $Id: AddActivityChangeStatusModal.tpl 3799 2007-12-04 17:54:36Z brian $ */ ?>
<script src="js/jquery-1.5.1.js"></script>

<?php if ($this->isJobOrdersMode): ?>
    <?php TemplateUtility::printModalHeader('Job Orders', array('modules/candidates/activityvalidator.js', 'js/activity.js'), 'Job Orders: Log Activity'); ?>
<?php elseif ($this->onlyScheduleEvent): ?>
    <?php TemplateUtility::printModalHeader('Candidates', array('modules/candidates/activityvalidator.js', 'js/activity.js'), 'Candidates: Schedule Event'); ?>
<?php else: ?>
    <?php TemplateUtility::printModalHeader('Candidates', array('modules/candidates/activityvalidator.js', 'js/activity.js'), 'Candidates: Log Activity'); ?>
<?php endif; ?>
<?php if (!$this->isFinishedMode): ?>

<script type="text/javascript">
    <?php if ($this->isJobOrdersMode): ?>
        statusesArray = new Array(1);
        jobOrdersArray = new Array(1);
        statusesArrayString = new Array(1);
        jobOrdersArrayStringTitle = new Array(1);
        jobOrdersArrayStringCompany = new Array(1);
        statusesArray[0] = <?php echo($this->pipelineData['statusID']); ?>;
        statusesArrayString[0] = '<?php echo($this->pipelineData['status']); ?>';
        jobOrdersArray[0] = <?php echo($this->pipelineData['jobOrderID']); ?>;
        jobOrdersArrayStringTitle[0] = '<?php echo(str_replace("'", "\\'", $this->pipelineData['title'])); ?>';
        jobOrdersArrayStringCompany[0] = '<?php echo(str_replace("'", "\\'", $this->pipelineData['companyName'])); ?>';
    <?php else: ?>
        <?php $count = count($this->pipelineRS); ?>
        statusesArray = new Array(<?php echo($count); ?>);
        jobOrdersArray = new Array(<?php echo($count); ?>);
        statusesArrayString = new Array(<?php echo($count); ?>);
        jobOrdersArrayStringTitle = new Array(<?php echo($count); ?>);
        jobOrdersArrayStringCompany = new Array(<?php echo($count); ?>);
        <?php for ($i = 0; $i < $count; ++$i): ?>
            statusesArray[<?php echo($i); ?>] = <?php echo($this->pipelineRS[$i]['statusID']); ?>;
            statusesArrayString[<?php echo($i); ?>] = '<?php echo($this->pipelineRS[$i]['status']); ?>';
            jobOrdersArray[<?php echo($i); ?>] = <?php echo($this->pipelineRS[$i]['jobOrderID']); ?>;
            jobOrdersArrayStringTitle[<?php echo($i); ?>] = '<?php echo(str_replace("'", "\\'", $this->pipelineRS[$i]['title'])); ?>';
            jobOrdersArrayStringCompany[<?php echo($i); ?>] = '<?php echo(str_replace("'", "\\'", $this->pipelineRS[$i]['companyName'])); ?>';
        <?php endfor; ?>
    <?php endif; ?>
    statusTriggersEmailArray = new Array(<?php echo(count($this->statusRS)); ?>);
    <?php foreach ($this->statusRS as $rowNumber => $statusData): ?>
       statusTriggersEmailArray[<?php echo($rowNumber); ?>] = <?php echo($statusData['triggersEmail']); ?>;
    <?php endforeach; ?>
</script>

    <form name="changePipelineStatusForm" id="changePipelineStatusForm" action="<?php echo(RESUFLOUtility::getIndexName()); ?>?m=<?php if ($this->isJobOrdersMode): ?>joborders<?php else: ?>candidates<?php endif; ?>&amp;a=addActivityChangeStatus<?php if ($this->onlyScheduleEvent): ?>&amp;onlyScheduleEvent=true<?php endif; ?>" method="post" onsubmit="return checkActivityForm(document.changePipelineStatusForm);" autocomplete="off">
        <input type="hidden" name="postback" id="postback" value="postback" />
        <input type="hidden" id="candidateID" name="candidateID" value="<?php echo($this->candidateID); ?>" />
        <input type="hidden" id="list" name="list" value="<?php echo $_REQUEST["list"];?>" />
		
		
        <?php if ($this->isJobOrdersMode): ?>
            <input type="hidden" id="regardingID" name="regardingID" value="<?php echo($this->selectedJobOrderID); ?>" />
        <?php endif; ?>
            <table class="editTable" width="560">
                <tr id="visibleTR" <?php if ($this->onlyScheduleEvent): ?>style="display:none;"<?php endif; ?>>
                    <td class="tdVertical">
                        <label id="regardingIDLabel" for="regardingID">Regarding:</label>
                    </td>
                    <td class="tdData">
<!--<?php if ($this->isJobOrdersMode): ?>
                    <span><?php $this->_($this->pipelineData['title']); ?></span>
<?php else: ?>
                    <select id="regardingID" name="regardingID" class="inputbox" style="width: 150px;" onchange="AS_onRegardingChange(statusesArray, jobOrdersArray, 'regardingID', 'statusID', 'statusTR', 'sendEmailCheckTR', 'triggerEmail', 'triggerEmailSpan', 'changeStatus', 'changeStatusSpanA', 'changeStatusSpanB');">
                        <option value="-1">General</option>

                        <?php foreach ($this->pipelineRS as $rowNumber => $pipelinesData): ?>
                            <?php if ($this->selectedJobOrderID == $pipelinesData['jobOrderID']): ?>
                                <option selected="selected" value="<?php $this->_($pipelinesData['jobOrderID']) ?>"><?php $this->_($pipelinesData['title']) ?></option>
                            <?php else: ?>
                                <option value="<?php $this->_($pipelinesData['jobOrderID']) ?>"><?php $this->_($pipelinesData['title']) ?> (<?php $this->_($pipelinesData['companyName']) ?>)</option>
                            <?php endif; ?>
                        <?php endforeach; ?>
                    </select>
<?php endif; ?>
-->     
                        <select id="regardingID" name="regardingID" class="inputbox" style="width: 150px;" onchange="AS_onRegardingChange(statusesArray, jobOrdersArray, 'regardingID', 'statusID', 'statusTR', 'sendEmailCheckTR', 'triggerEmail', 'triggerEmailSpan', 'changeStatus', 'changeStatusSpanA', 'changeStatusSpanB');">
                            <option value="-1">General</option>

                            <?php foreach ($this->pipelineRS as $rowNumber => $pipelinesData): ?>
                                <?php if ($this->selectedJobOrderID == $pipelinesData['jobOrderID']): ?>
                                    <option selected="selected" value="<?php $this->_($pipelinesData['jobOrderID']) ?>"><?php $this->_($pipelinesData['title']) ?></option>
                                <?php else: ?>
                                    <option value="<?php $this->_($pipelinesData['jobOrderID']) ?>"><?php $this->_($pipelinesData['title']) ?> (<?php $this->_($pipelinesData['companyName']) ?>)</option>
                                <?php endif; ?>
                            <?php endforeach; ?>
                        </select>
                    </td>
                </tr>
                <tr id="statusTR" <?php if ($this->onlyScheduleEvent): ?>style="display:none;"<?php endif; ?>>
                    <td class="tdVertical">
                        <label id="regardingIDLabel" for="regardingID">Select Template:</label>
                    </td>
                    <td class="tdData">
                        <select id="email_template_id" name="email_template_id" class="inputbox" style="width: 150px;" onchange="AS_onStatusChange(statusesArray, jobOrdersArray, 'regardingID', 'statusID', 'sendEmailCheckTR', 'triggerEmailSpan', 'activityNote', 'activityTypeID', <?php if ($this->isJobOrdersMode): echo $this->selectedJobOrderID; else: ?>null<?php endif; ?>, 'customMessage', 'origionalCustomMessage', 'triggerEmail', statusesArrayString, jobOrdersArrayStringTitle, jobOrdersArrayStringCompany, statusTriggersEmailArray, 'emailIsDisabled','candidateFullName','siteName','ownerfullName','dateFormat');" disabled>
                            <option value="-1">Default Template</option>
                                <?php 
                                $loginedUser = $_SESSION['RESUFLO']->getUserID();
                                $getQuery = mysql_query("SELECT email_template_id,name,text  FROM email_template WHERE entered_by = '$loginedUser' and tag = 'EMAIL_TEMPLATE_STATUSCHANGE'"); 
                                while($row = mysql_fetch_array($getQuery))
                                {
                                 $name = $row['name'];
                                 $id = $row['email_template_id'];
                                 $text = $row['text'];
                                  ?>
                                 <option value="<?php echo $id ?>"><?php echo $name; ?></option>
                                 <?php
                                }
                                ?>
                        </select>
                            <br />
                            <?php $getQuery = mysql_query("SELECT email_template_id,text  FROM email_template WHERE entered_by = '$loginedUser' and tag = 'EMAIL_TEMPLATE_STATUSCHANGE'"); 
                                while($row = mysql_fetch_array($getQuery))
                                {
                                 $id = $row['email_template_id'];
                                 //$text = strip_tags($row['text']);
                                  $text = htmlentities($row['text'],ENT_COMPAT );
                                  ?>
                                 <input type="hidden" id="<?php echo $id; ?>" value="<?php echo $text; ?>" />
                                 <?php
                                }
                            ?>
                    </td>
                </tr>
                <tr id="statusTR" <?php if ($this->onlyScheduleEvent): ?>style="display:none;"<?php endif; ?>>
                    <td class="tdVertical">
                        <label id="statusIDLabel" for="statusID">Status:</label>
                    </td>
                    <td class="tdData">
                        <input type="checkbox" name="changeStatus" id="changeStatus" style="margin-left: 0px" onclick="check();AS_onChangeStatusChange('changeStatus', 'statusID', 'changeStatusSpanB');"<?php if ($this->selectedJobOrderID == -1 || $this->onlyScheduleEvent): ?> disabled<?php endif; ?> />
                        <span id="changeStatusSpanA"<?php if ($this->selectedJobOrderID == -1): ?> style="color: #aaaaaa;"<?php endif;?>>Change Status</span><br />

                        <div id="changeStatusDiv" style="margin-top: 4px;">
                            <select id="statusID" name="statusID" class="inputbox" style="width: 150px;" onchange="AS_onStatusChange(statusesArray, jobOrdersArray, 'regardingID', 'statusID', 'sendEmailCheckTR', 'triggerEmailSpan', 'activityNote', 'activityTypeID', <?php if ($this->isJobOrdersMode): echo $this->selectedJobOrderID; else: ?>null<?php endif; ?>, 'customMessage', 'origionalCustomMessage', 'triggerEmail', statusesArrayString, jobOrdersArrayStringTitle, jobOrdersArrayStringCompany, statusTriggersEmailArray, 'emailIsDisabled','candidateFullName','siteName','ownerfullName','dateFormat');" disabled>
                                <option value="-1">(Select a Status)</option>

                                <?php if ($this->selectedStatusID == -1): ?>
                                    <?php foreach ($this->statusRS as $rowNumber => $statusData): ?>
                                        <option value="<?php $this->_($statusData['statusID']) ?>"><?php $this->_($statusData['status']) ?></option>
                                    <?php endforeach; ?>
                                <?php else: ?>
                                    <?php foreach ($this->statusRS as $rowNumber => $statusData): ?>
                                        <option <?php if ($this->selectedStatusID == $statusData['statusID']): ?>selected <?php endif; ?>value="<?php $this->_($statusData['statusID']) ?>"><?php $this->_($statusData['status']) ?></option>

                                    <?php endforeach; ?>
                                <?php endif; ?>
                            </select>

                                                    <span id="changeStatusSpanB" style="color: #aaaaaa;">&nbsp;*</span>&nbsp;&nbsp;
                            <span id="triggerEmailSpan" style="display: none;"><input type="checkbox" name="triggerEmail" id="triggerEmail" onclick="AS_onSendEmailChange('triggerEmail', 'sendEmailCheckTR', 'visibleTR');" />Send status change E-Mail Notification to Candidate</span>
                        </div>
                    </td>
                </tr>
			
			
                <tr id="statusTRarray" <?php if ($this->onlyScheduleEvent): ?>style="display:none;"<?php endif; ?>>
                    <td class="tdVertical">
                    <label id="statusIDarrayLabel" for="statusIDarray">Stages Completed:</label>
                </td>
                <td class="tdData">
					
						<table><tr>
								<?php
									$i=0;
									$count=1;
									foreach ($this->statusRS as $rowNumber => $statusData):
									$i++;
								?><td>
											<input type="checkbox" name="statusID"
											<?php if ($this->selectedStatusID >= $statusData['statusID']): ?>
													checked = "checked"
											<?php endif; ?>
											id="Checkbox<?php echo $i; ?>"
											onclick="javascript:randomcheck(<?php echo $i; ?>);AS_onStatusChange_test(statusesArray, jobOrdersArray, 'Checkbox<?php echo $i; ?>', 'statusID', 'sendEmailCheckTR', 'triggerEmailSpan', 'activityNote', 'activityTypeID', <?php if ($this->isJobOrdersMode): echo $this->selectedJobOrderID; else: ?>null<?php endif; ?>, 'customMessage', 'origionalCustomMessage', 'triggerEmail', statusesArrayString, jobOrdersArrayStringTitle, jobOrdersArrayStringCompany, statusTriggersEmailArray, 'emailIsDisabled','candidateFullName','siteName','ownerfullName','dateFormat')"
											value="<?php echo $this->_($statusData['statusID']); ?>" class="chkGroup">
											<?php $this->_($statusData['status']) ?>
									</td>
									<!--<td>
											<input type="checkbox" name="statusIDarray"
											<?php /* if ($this->selectedStatusID >= $statusData['statusID']): ?> 
												checked = "checked" disabled = "disabled" class="chkNoGroup"
											<?php else : 
												$i++;
											?>
												id="Checkbox<?php echo $i; ?>"
												onclick="javascript:randomcheck(<?php echo $i; ?>);AS_onStatusChange_test(statusesArray, jobOrdersArray, 'Checkbox<?php echo $i; ?>', 'statusID', 'sendEmailCheckTR', 'triggerEmailSpan', 'activityNote', 'activityTypeID', <?php if ($this->isJobOrdersMode): echo $this->selectedJobOrderID; else: ?>null<?php endif; ?>, 'customMessage', 'origionalCustomMessage', 'triggerEmail', statusesArrayString, jobOrdersArrayStringTitle, jobOrdersArrayStringCompany, statusTriggersEmailArray, 'emailIsDisabled','candidateFullName','siteName','ownerfullName','dateFormat')"
											<?php endif; ?>
											value="<?php echo $this->_($statusData['statusID']); ?>" disabled="disabled" class="chkGroup">
											<?php $this->_($statusData['status']) */ ?>
										</td>-->
								<?php
									if($count%3==0)
										echo "</tr><tr>";
									$count++;
									endforeach;
								?></tr>
<tr>
<td>ARCHIVE:</td>
<td>
  <input type="hidden" name="dead" value="0">
  <input type="checkbox" name="dead" value="1"<?php if ($this->dead): ?> checked="checked"<?php endif; ?>>
  Dead
</td>
<td>
  <input type="hidden" name="future" value="0">
  <input type="checkbox" name="future" value="1"<?php if ($this->future): ?> checked="checked"<?php endif; ?>>
  Future
</td>
</tr>

						</table>
						
				</td>
			</tr>
			
			
            <tr id="sendEmailCheckTR" style="display: none;">
                <td class="tdVertical">
                    <label id="triggerEmailLabel" for="triggerEmail">E-Mail:</label>
                </td>
                <td class="tdData">
                    Custom Message 
                    <input type="hidden" id="origionalCustomMessage" value="" />
					<input type="hidden" id="defaultMessage" value="* Auto generated message. Please DO NOT reply *
%DATETIME%

Dear %CANDFULLNAME%,

This E-Mail is a notification that your status in our database has been changed for the position %JBODTITLE% (%JBODCLIENT%).

Your previous status was <B>%CANDPREVSTATUS%</B>.
Your new status is <B>%CANDSTATUS%</B>.

Take care,
%USERFULLNAME%" />
                    <input type="hidden" id="emailIsDisabled" value="<?php echo($this->emailDisabled); ?>" />
					<input type="hidden" id="candidateFullName" value="<?php echo($this->candidateFullName); ?>" />
					<input type="hidden" id="siteName" value="<?php echo($this->siteName); ?>" />
					<input type="hidden" id="ownerfullName" value="<?php echo($this->ownerfullName); ?>" />
					<input type="hidden" id="dateFormat" value="<?php echo($this->dateFormat); ?>" />
                    <textarea style="height:135px; width:375px;" name="customMessage" id="customMessage" cols="50" class="inputbox"></textarea>
                </td>
            </tr>
           <tr id="addActivityTR" <?php if ($this->onlyScheduleEvent): ?>style="display:none;"<?php endif; ?>>
                <td class="tdVertical">
                    <label id="addActivityLabel" for="addActivity">Activity:</label>
                </td>
                <td class="tdData">
                    <input type="checkbox" name="addActivity" id="addActivity" style="margin-left: 0px;"<?php if (!$this->onlyScheduleEvent): ?> checked="checked"<?php endif; ?> onclick="AS_onAddActivityChange('addActivity', 'activityTypeID', 'activityNote', 'addActivitySpanA', 'addActivitySpanB');" />Log an Activity<br />
                    <div id="activityNoteDiv" style="margin-top: 4px;">
                        <span id="addActivitySpanA">Activity Type</span><br />
                        <select id="activityTypeID" name="activityTypeID" class="inputbox" style="width: 150px; margin-bottom: 4px;">
                            <option selected="selected" value="<?php echo(ACTIVITY_CALL); ?>">Call</option>
                            <option value="<?php echo(ACTIVITY_CALL_TALKED); ?>">Call (Talked)</option>
                            <option value="<?php echo(ACTIVITY_CALL_LVM); ?>">Call (LVM)</option>
                            <option value="<?php echo(ACTIVITY_CALL_MISSED); ?>">Call (Missed)</option>
                            <option value="<?php echo(ACTIVITY_EMAIL); ?>">E-Mail</option>
                            <option value="<?php echo(ACTIVITY_MEETING); ?>">Meeting</option>
                            <option value="<?php echo(ACTIVITY_OTHER); ?>">Other</option>
                        </select><br />
                        <span id="addActivitySpanB">Activity Notes</span><br />
                        <textarea name="activityNote" id="activityNote" cols="50" style="margin-bottom: 4px;" class="inputbox"></textarea>
                    </div>
                </td>
            </tr>
			
            <tr id="scheduleEventTR">
                <td class="tdVertical">
                    <label id="scheduleEventLabel" for="scheduleEvent">Schedule Event:</label>
                </td>
                <td class="tdData">
                    <input type="checkbox" name="scheduleEvent" id="scheduleEvent" style="margin-left: 0px; <?php if ($this->onlyScheduleEvent): ?>display:none;<?php endif; ?>" onclick="AS_onScheduleEventChange('scheduleEvent', 'scheduleEventDiv');onlyScheduleEvent(this);"<?php if ($this->onlyScheduleEvent): ?> checked="checked"<?php endif; ?> /><?php if (!$this->onlyScheduleEvent): ?>Schedule Event<?php endif; ?>
                    <div id="scheduleEventDiv" <?php if (!$this->onlyScheduleEvent): ?>style="display:none;"<?php endif; ?>>
                        <table style="border: none; margin: 0px; padding: 0px;">
                            <tr>
                                <td valign="top">
                                    <div style="margin-bottom: 4px;">
                                        <select id="eventTypeID" name="eventTypeID" class="inputbox" style="width: 150px;">
                                            <?php foreach ($this->calendarEventTypes as $eventType): ?>
                                                <option <?php if ($eventType['typeID'] == CALENDAR_EVENT_INTERVIEW): ?>selected="selected" <?php endif; ?>value="<?php echo($eventType['typeID']); ?>"><?php $this->_($eventType['description']); ?></option>
                                            <?php endforeach; ?>
                                        </select>
                                    </div>

                                    <div style="margin-bottom: 4px;">
                                        <script type="text/javascript">DateInput('dateAdd', true, 'MM-DD-YY', '', -1);</script>
                                    </div>

                                    <div style="margin-bottom: 4px;">
									
                                        <input type="radio" name="allDay" id="allDay0" value="0" style="margin-left: 0px" checked="checked" onchange="AS_onEventAllDayChange('allDay1');" />
                                        <select id="hour" name="hour" class="inputbox" style="width: 40px;">
                                            <?php for ($i = 1; $i <= 12; ++$i): ?>
                                                <option value="<?php echo($i); ?>"><?php echo(sprintf('%02d', $i)); ?></option>
                                            <?php endfor; ?>
                                        </select>&nbsp;
                                        <select id="minute" name="minute" class="inputbox" style="width: 40px;">
                                            <?php for ($i = 0; $i <= 59; $i = $i+1): ?>
                                                <option value="<?php echo(sprintf('%02d', $i)); ?>">
                                                    <?php echo(sprintf('%02d', $i)); ?>
                                                </option>
                                            <?php endfor; ?>
                                        </select>&nbsp;
                                        <select id="meridiem" name="meridiem" class="inputbox" style="width: 45px;">
                                            <option value="AM">AM</option>
                                            <option value="PM">PM</option>
                                        </select>
                                    </div>
									<div style="margin-bottom: 4px;">
									
									</div>
                                    <div style="margin-bottom: 4px;">
                                        <input type="radio" name="allDay" id="allDay1" value="1" style="margin-left: 0px" onchange="AS_onEventAllDayChange('allDay1');" />All Day / No Specific Time<br />
                                    </div>

                                    <div style="margin-bottom: 4px;">
                                        <!--<input type="checkBox" name="publicEntry" id="publicEntry" style="margin-left: 0px" />Public Entry-->
                                    </div>
                                </td>

                                <td valign="top">
                                    <div style="margin-bottom: 4px;" align="left">
                                        <input type="checkbox" class="inputbox" name="emailcheck" id="emailcheck" />
                                        <label id="emailcheckLabel" for="title">Send e-mail&nbsp;*</label>
                                    </div>
                                    <?php $getQuery = mysql_query("SELECT twillioSID, twillioToken, twillioFromNumber FROM user WHERE user_id = '$loginedUser'"); 
                                        $row = mysql_fetch_array($getQuery);
                                        if($row && !empty($row['twillioSID']) && !empty($row['twillioToken']) && !empty($row['twillioFromNumber']))
                                        {?>
                                     
                                            <div style="margin-bottom: 4px;" align="left">
                                                <input type="checkbox" class="inputbox" name="candidatePhoneReminderSend" id="candidatePhoneReminderSend" />
                                                <label id="candidatePhoneReminderSendLabel" for="candidatePhoneReminderSend">Send text message</label>
                                            </div>
                                    <?php }?>
									<div style="margin-bottom: 4px;">
                                        <label id="titleLabel" for="title">Title&nbsp;*</label><br />
                                        <input type="text" class="inputbox" name="title" id="title" style="width: 180px;" />
                                    </div>
 									<div style="margin-bottom: 4px;">
                                        <label id="titleLabel" for="title">Venue&nbsp;</label><br />
                                        <input type="text" class="inputbox" name="venue" id="venue" style="width: 180px;" />
                                    </div>
									<div style="margin-bottom: 4px;">
                                        <label id="titleLabel" for="title">Person To Meet&nbsp;</label><br />
                                        <input type="text" class="inputbox" name="tomeet" id="tomeet" style="width: 180px;" />
                                    </div>
									<div style="margin-bottom: 4px;">
                                        <label id="titleLabel" for="title">Contact Number&nbsp;</label><br />
                                        <input type="text" class="inputbox" name="contactnumber" id="contactnumber" style="width: 180px;" />
                                    </div>
									<div style="margin-bottom: 4px;">
                                        <label id="emailLabel" for="title">Email Id&nbsp;</label><br />
                                        <input type="text" class="inputbox" name="emailid" id="emailid" style="width: 180px;" />
                                    </div>
                                    <div style="margin-bottom: 4px;">
                                        <label id="durationLabel" for="duration">Length:</label>
                                        <br />
                                        <select id="duration" name="duration" class="inputbox" style="width: 180px;">
                                            <option value="15">15 minutes</option>
                                            <option value="30">30 minutes</option>
                                            <option value="45">45 minutes</option>
                                            <option value="60" selected="selected">1 hour</option>
                                            <option value="90">1.5 hours</option>
                                            <option value="120">2 hours</option>
                                            <option value="180">3 hours</option>
                                            <option value="240">4 hours</option>
                                            <option value="300">More than 4 hours</option>
                                        </select>
                                    </div>
                                    
                                    <div style="margin-bottom: 4px;">
                                        <label id="descriptionLabel" for="description">Description</label><br />
                                        <textarea name="description" id="description" cols="20" class="inputbox" style="width: 180px; height:60px;"></textarea>
                                    </div>

                                    <div <?php if (!$this->allowEventReminders): ?>style="display:none;"<?php endif; ?>>
                                        <input type="checkbox" name="reminderToggle" onclick="if (this.checked) document.getElementById('reminderArea').style.display = ''; else document.getElementById('reminderArea').style.display = '';">&nbsp;<label>Set Reminder</label><br />
                                    </div>
                                    
                                    <div style="display:none;" id="reminderArea">
                                        <div>
                                            <label>E-Mail To:</label><br />
                                            <input type="text" id="sendEmail" name="sendEmail" class="inputbox" style="width: 150px" value="<?php $this->_($this->userEmail); ?>" />
                                        </div>
                                        <div>
                                            <label>Time:</label><br />
                                            <select id="reminderTime" name="reminderTime" style="width: 150px">
                                                <option value="15">15 min early</option>
                                                <option value="30">30 min early</option>
                                                <option value="45">45 min early</option>
                                                <option value="60">1 hour early</option>
                                                <option value="120">2 hours early</option>
                                                <option value="1440">1 day early</option>
                                            </select>
                                        </div>
                                    </div>
						
                                </td>
                            </tr>
                            <tr>
                                <td valign="top">E-Mail Template</td>
                                <td>
                                    <select id="template_id" name="template_id" class="inputbox" style="width: 150px;" onchange="getSelectedEmailTemplate('template_id');" >
                                        <option value="-1">Default Template</option>
                                            <?php 
                                            $loginedUser = $_SESSION['RESUFLO']->getUserID();
                                            $getQuery = mysql_query("SELECT email_template_id,name,text  FROM email_template WHERE entered_by = '$loginedUser' and tag = 'EMAIL_TEMPLATE_SCHEDULEEVENT'"); 
                                            while($row = mysql_fetch_array($getQuery))
                                            {
                                                $name = $row['name'];
                                                $id = $row['email_template_id'];
                                                $text = $row['text'];
                                                ?>
                                                <option value="<?php echo $id ?>"><?php echo $name; ?></option>
                                                <?php
                                            }
                                        ?>
                                    </select><br />
                                    <?php $getQuery = mysql_query("SELECT email_template_id,text  FROM email_template WHERE entered_by = '$loginedUser'"); 
                                        while($row = mysql_fetch_array($getQuery))
                                        {
                                         $id = $row['email_template_id'];
                                          //$text = strip_tags($row['text']);
                                          $text = htmlentities($row['text'],ENT_COMPAT );
                                          ?>
                                         <input type="hidden" id="<?php echo $id; ?>" value="<?php echo $text; ?>" />
                                         <?php
                                        }

                                        ?>
                                        <?php
                                        $selectDisable = mysql_query("SELECT * FROM email_template WHERE entered_by = 0 AND tag = 'EMAIL_TEMPLATE_SCHEDULEEVENT'");
                                        $result = mysql_fetch_assoc($selectDisable);
                                        $text = $result['text'];
                                    ?>
                                    <textarea style="height:135px; width:375px;" name="emailMessage" id="emailMessage" cols="50" class="inputbox"><?php echo $text; ?></textarea>
                                    <input type="hidden" id="defaultMail" value="<?php echo $text; ?>" />
                                    <input type="hidden" id="candidateFullName" value="<?php echo($this->candidateFullName); ?>" />
                                    <input type="hidden" id="siteName" value="<?php echo($this->siteName); ?>" />
                                    <input type="hidden" id="ownerfullName" value="<?php echo($this->ownerfullName); ?>" />
                                    <input type="hidden" id="dateFormat" value="<?php echo($this->dateFormat); ?>" />
                                </td>
                            </tr>
                            <tr>
                                <td valign="top">Candidate Reminder</td>
                                <td>
                                    <input id="reminderToCandidate" name="reminderToCandidateDays" type="text" style="width: 150px;"><span>(No. of days before event)</span>
                                    <br />
                                    <select id="reminder_template_id" name="reminder_template_id" class="inputbox" style="width: 150px;" onchange="populateSelectedEmailTemplate('reminder_template_id', 'reminderEmailMessage');" >
                                        <option value="-1" reminderDays="0">Default Template</option>
                                            <?php 
                                            $loginedUser = $_SESSION['RESUFLO']->getUserID();
                                            $getQuery = mysql_query("SELECT email_template_id,name,text,reminder_days  FROM email_template WHERE entered_by = '$loginedUser' and tag = 'SCHEDULER_EMAIL_TEMPLATE_REMINDER'"); 
                                            while($row = mysql_fetch_array($getQuery))
                                            {
                                                $name = $row['name'];
                                                $id = $row['email_template_id'];
                                                $text = $row['text'];
                                                $reminder_days = $row['reminder_days'];
                                                ?>
                                                <option value="<?php echo $id ?>" reminderDays="<?php echo $reminder_days ?>"><?php echo $name; ?></option>
                                                <?php
                                            }
                                        ?>
                                    </select>
                                    <br />
                                    <?php
                                        $selectDisable = mysql_query("SELECT * FROM email_template WHERE entered_by = 0 AND tag = 'EMAIL_TEMPLATE_SCHEDULEEVENT'");
                                        $result = mysql_fetch_assoc($selectDisable);
                                        $text = $result['text'];
                                    ?>
                                    <textarea style="height:135px; width:375px;" name="reminderEmailMessage" id="reminderEmailMessage" cols="50" class="inputbox"><?php echo $text; ?></textarea>
                                </td>
                            </tr>
                            <tr>
                                <td valign="top">Thank You Message</td>
                                <td>
                                    <input id="thanksToCandidate" name="thanksToCandidateDays" type="text" style="width: 150px;"><span>(No. of days after event)</span>
                                    <br />
                                    <select id="thanks_template_id" name="thanks_template_id" class="inputbox" style="width: 150px;" onchange="populateSelectedEmailTemplate('thanks_template_id', 'thanksEmailMessage');" >
                                        <option value="-1">Default Template</option>
                                            <?php 
                                            $loginedUser = $_SESSION['RESUFLO']->getUserID();
                                            $getQuery = mysql_query("SELECT email_template_id,name,text  FROM email_template WHERE entered_by = '$loginedUser' and tag = 'EMAIL_TEMPLATE_SCHEDULEEVENT'"); 
                                            while($row = mysql_fetch_array($getQuery))
                                            {
                                                $name = $row['name'];
                                                $id = $row['email_template_id'];
                                                $text = $row['text'];
                                                ?>
                                                <option value="<?php echo $id ?>"><?php echo $name; ?></option>
                                                <?php
                                            }
                                        ?>
                                    </select>
                                    <br />
                                    <?php
                                        $selectDisable = mysql_query("SELECT * FROM email_template WHERE entered_by = 0 AND tag = 'EMAIL_TEMPLATE_SCHEDULEEVENT'");
                                        $result = mysql_fetch_assoc($selectDisable);
                                        $text = $result['text'];
                                    ?>
                                    <textarea style="height:135px; width:375px;" name="thanksEmailMessage" id="thanksEmailMessage" cols="50" class="inputbox"><?php echo $text; ?></textarea>
                                </td>
                            </tr>
                        </table>
                    </div>
                </td>
            </tr>
			

        </table>
        <input type="submit" class="button" name="submit" id="submit" value="Save" />&nbsp;
<?php if ($this->isJobOrdersMode): ?>
        <input type="button" class="button" name="close" value="Cancel" onclick="parentGoToURL('<?php echo(RESUFLOUtility::getIndexName()); ?>?m=joborders&amp;a=show&amp;jobOrderID=<?php echo($this->selectedJobOrderID); ?>');" />
		
<?php else: ?>
        <!--<input type="button" class="button" name="close" value="Cancel" onclick="parentGoToURL('<?php echo(RESUFLOUtility::getIndexName()); ?>?m=candidates&amp;a=show&amp;candidateID=<?php echo($this->candidateID); ?>');" />-->
	<?php if($_REQUEST["list"]) { ?>
		<input type="button" class="button" name="close" value="Cancel" onclick="parentGoToURL('<?php echo(RESUFLOUtility::getIndexName()); ?>?m=candidates&amp;a=show&amp;candidateID=<?php echo($this->candidateID); ?>&amp;list=<?php echo($_REQUEST["list"]); ?>');" />
	<?php } else { ?>
			<input type="button" class="button" name="close" value="Cancel" onclick="parentGoToURL('<?php echo(RESUFLOUtility::getIndexName()); ?>?m=candidates&amp;a=show&amp;candidateID=<?php echo($this->candidateID); ?>');" />
	<?php } ?>
<?php endif; ?>
    </form>

    <script type="text/javascript">
        document.changePipelineStatusForm.activityNote.focus();
        function onlyScheduleEvent(pointer){
            if($(pointer).is(":checked"))
            {
                var action = $('#changePipelineStatusForm').attr('action')+"&onlyScheduleEvent=true";
                $('#changePipelineStatusForm').attr({'action': action});
            }
            else{
                var action = $('#changePipelineStatusForm').attr('action');
                action = action.replace("&onlyScheduleEvent=true", "")
                $('#changePipelineStatusForm').attr({'action': action});
            }
        }
        $(function() {
            $('#reminder_template_id').on('change', function() {
                var reminderDays = $('option:selected', this).attr('reminderDays');
                $("#reminderToCandidate").val(reminderDays);
            });
        });
    </script>

<?php else: ?>
    <?php if (!$this->changesMade): ?>
        <p>No changes have been made.</p>
    <?php else: ?>
         <?php if (!$this->onlyScheduleEvent): ?>
            <?php //FIXME: E-mail stuff. ?>
            <?php if ($this->statusChanged): ?>
                <p>The candidate's status has been changed<?php if ($this->oldStatusDescription != $this->newStatusDescription): ?> from <span class="bold"><?php $this->_($this->oldStatusDescription); ?></span> to <span class="bold"><?php $this->_($this->newStatusDescription); ?></span><?php else: ?>, the special statuses Dead/Future are now updated.</span><?php endif; ?>.</p>
            <?php else: ?>
                <p>The candidate's status has not been changed.</p>
            <?php endif; ?>

            <?php if ($this->activityAdded): ?>
                <?php if (!empty($this->activityDescription)): ?>
                    <p>An activity entry of type <span class="bold"><?php $this->_($this->activityType); ?></span> has been added with the following note: &quot;<?php echo($this->activityDescription); ?>&quot;.</p>
                <?php else: ?>
                    <p>An activity entry of type <span class="bold"><?php $this->_($this->activityType); ?></span> has been added with no notes.</p>
                <?php endif; ?>
            <?php else: ?>
                <p>No activity entries have been added.</p>
            <?php endif; ?>
        <?php endif; ?>
    <?php endif; ?>

    <?php echo($this->eventHTML); ?>

    <?php echo($this->notificationHTML); ?>

    <form>
<?php if ($this->isJobOrdersMode): ?>
        <input type="button" name="close" class="button" value="Close" onclick="parentGoToURL('<?php echo(RESUFLOUtility::getIndexName()); ?>?m=joborders&amp;a=show&amp;jobOrderID=<?php echo($this->regardingID); ?>');" />
<?php else: ?>
	<?php if($_REQUEST["list"]) { ?>
        <input type="button" name="close" class="button" value="Close" onclick="parentGoToURL('<?php echo(RESUFLOUtility::getIndexName()); ?>?m=candidates&amp;a=show&amp;candidateID=<?php echo($this->candidateID); ?>&amp;list=<?php echo($_REQUEST["list"]); ?>');" />
	<?php } else { ?>
		<input type="button" name="close" class="button" value="Close" onclick="parentGoToURL('<?php echo(RESUFLOUtility::getIndexName()); ?>?m=candidates&amp;a=show&amp;candidateID=<?php echo($this->candidateID); ?>');" />
	<?php } ?>	
		
<?php endif; ?>
    </form>
<?php endif; ?>

    </body>
</html>
