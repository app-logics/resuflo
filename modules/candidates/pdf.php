<?php

require_once('tcpdf/config/lang/eng.php');
require_once('tcpdf/tcpdf.php');


// create new PDF document

$pdf = new TCPDF(PDF_PAGE_ORIENTATION, PDF_UNIT, PDF_PAGE_FORMAT, true, 'UTF-8', false);
  $title = $this->data['firstName']."  ".$this->data['middleName']." ".$this->data['lastName']; 

   // set document information
$pdf->SetCreator(PDF_CREATOR);
$pdf->SetAuthor('Nicola Asuni');
$pdf->SetTitle($title);
$pdf->SetSubject('TCPDF Tutorial');
$pdf->SetKeywords('TCPDF, PDF, example, test, guide');
	

// set default header data
//$pdf->SetHeaderData(PDF_HEADER_LOGO, PDF_HEADER_LOGO_WIDTH, PDF_HEADER_TITLE, PDF_HEADER_STRING);

// set header and footer fonts
$pdf->setHeaderFont(Array(PDF_FONT_NAME_MAIN, '', PDF_FONT_SIZE_MAIN));
$pdf->setFooterFont(Array(PDF_FONT_NAME_DATA, '', PDF_FONT_SIZE_DATA));

// set default monospaced font
$pdf->SetDefaultMonospacedFont(PDF_FONT_MONOSPACED);

//set margins
$pdf->SetMargins(PDF_MARGIN_LEFT, PDF_MARGIN_TOP, PDF_MARGIN_RIGHT);
$pdf->SetHeaderMargin(PDF_MARGIN_HEADER);
//$pdf->SetFooterMargin(PDF_MARGIN_FOOTER);

//set auto page breaks
$pdf->SetAutoPageBreak(TRUE, PDF_MARGIN_BOTTOM);

//set image scale factor
$pdf->setImageScale(PDF_IMAGE_SCALE_RATIO); 

//set some language-dependent strings
$pdf->setLanguageArray($l); 

// ---------------------------------------------------------
//$pdf->SetDisplayMode(real,'two');
//$pdf->SetFont('dejavusans','B',5);

//$pdf->SetFont('Courier', '', 10);
// set font
//$pdf->SetFont('dejavusans', '', 8);
$pdf->SetFont('helvetica', '', 10);
// add a page
$pdf->AddPage();

$html = '<div style="text-align:center;" ><b>RESUME</b><br/>'.$title.'</div>';

//$pdf->writeHTML($html, true, false, true, false, '');
//$pdf->Output('pdf.php', 'I');
//die();
// Soft logics commnected below loop
// for ($i = ((intval(count($this->extraFieldRS)))-2); $i < (count($this->extraFieldRS)); $i++){
//       
//              $htmlcontent = $this->extraFieldRS[$i]['display'];      
//  			  
//   }
   $htmlcontent = $this->extraFieldRS[20]['display'];
//  echo $view;
// create some HTML content

//die($htmlcontent);
$html .= '<table><tr><td>  '.str_replace('<br />','&nbsp;<br />',html_entity_decode($htmlcontent)).'</td></tr><tr><td></td></tr></table>';
/* $html = '<table><tr><td>  Ricky Harrison<br />
 Portland, OR 97212<br />
 US<br />
 rickyharrison80@123456yahoo.com<br />
 RESUME<br />
 Monster resume #w9n5dzcu43h7zfki<br />

 Resume Headline: Ricky Harrison<br />
 Resume Wizard<br />
 2804 N.E. 7th AVENUE<br />
 Portland, Oregon 97212<br />
 Phone: 971-230-8290<br />
 Ricky A. Harrison<br />

 Objective <br />
 My goal Is to obtain a position with your organization where I can offer my skills and experiences.&nbsp;<br />
 Skills<br />
      Excellent communication skills<br />
      Keen listening and quick thinking abilities<br />
      Patient, empathetic and convincing<br />

      Pleasant and cheerful voice quality<br />
  <br />
  <br />
  <br />
  <br />
 Work Experience<br />
 Jan.2011-Present     HBW Leads                                       Portland, OR<br />

  <br />
 TRS/Customer service<br />
          Worked in inbound and outbound call center. Excelled in a highly <br />
 Competitive fast paced environment. Auto dialer and customer service skills.<br />
  <br />
  <br />
 Jan. 2007- Nov. 07                City Center Parking              Portland, OR<br />

                                       <br />
 Valet/Driver<br />
 Worked with diverse population that helped me gain face-to-face customer service experience in working with people from various social-economic backgrounds. Cashier experience. Gained management experience through operating various parking lots.  <br />
               Jan. 2004- July 2006                  Morganton Maintenance              Morganton, NC<br />
                                                                              Company no longer in business<br />
 Laborer<br />

                                       Assisted in ordering and purchasing materials that was essential to the each job site that included post construction sites/businesses clean up, landscaping, and janitorial services.  In this job I gained various skills in working among a team and independently to accomplish company goals. <br />
  <br />
  <br />
  <br />
               Mar. 2003- Dec. 2004    Live Bridge                            Portland, OR<br />
                                                                              <br />
 TRS<br />

                                      Duties included using telephone customer service skills to encourage customers to purchase products and conducting research surveys.  Gained computer data entry skills, verbatim script experience, inbound and outbound calls, and reaching weekly quota. <br />
  <br />
  <br />
  <br />
  <br />
               Mar. 2002   Oct. 2002    Enterprise                            Portland, OR<br />
 Car Prep/Customer Service<br />

 Duties included working positively with various clienteles to transport them safely to and from designated location and detailing cars. <br />
  <br />
               Mar. 1995- Sept. 1997    Fred Meyer                            Portland, OR<br />
                                                                             <br />
 Food Parcel<br />
                                      Customer service experience, bagging groceries, stocking shelves, and assisting managers set up side displays. Restock un-purchased products and general store maintenance.  <br />
  <br />

  <br />
  <br />
  <br />
  <br />
  <br />
  <br />
  <br />
  <br />
  <br />

 Education       Sept 1997 - June 1998      Lakes High School Tacoma. A High School Diploma<br />
  <br />
  <br />
  <br />
  <br />
  <br />
 REFERENCES FURNISHED UPON REQUEST<br />
  <br />

  <br />
  <br />
  <br />
  <br />
 SUMMARY<br />
 Current Career Level: Experienced (Non-Manager)<br />
 WORK STATUS: <br />

  US - I am authorized to work in this country for any employer.<br />
 Citizenship: <br />
         None<br />
       <br />
 TARGET LOCATIONS:<br />
  Selected Locations: US-OR-Portland<br />

 <br />
  Basic Profile for Resume Posted: 2011-08-14 21:45:00 <br />
 <br />
 Candidate Name: Ricky Harrison<br />
 Location: Portland, OR 97212<br />
 Email: rickyharrison80@123456yahoo.com<br />
 Highest Degree: High School or equivalent<br />

 Recent Salary:</td></tr>
 <tr>
  <td colspan="3" class="deatils">&nbsp;</td>
</tr>
<tr>
  <td colspan="3" class="deatils">&nbsp;</td>
</tr>
</table>';*/
 
$pdf->writeHTML($html, true, false, true, false, '');
// output some RTL HTML content
// reset pointer to the last page
$pdf->lastPage();

// - - - - - - - - - - - - - - - - - - - - - - - - - - - - -
// Print a table


//Close and output PDF document
$pdf->Output('pdf.php', 'I');
die("Hello");
//============================================================+
// END OF FILE                                                 
//============================================================+
