<?php /* $Id: Candidates.tpl 3445 2007-11-06 23:17:04Z will $ */ ?>
<?php TemplateUtility::printHeader('Candidates', array( 'js/highlightrows.js', 'js/export.js', 'js/dataGrid.js','js/validation.js')); ?>
<?php TemplateUtility::printHeaderBlock(); ?>
<?php TemplateUtility::printTabs($this->active); ?>
    <style type="text/css">
    div.addCandidateButton { background: #4172E3 url(images/nodata/candidatesButton.jpg); cursor: pointer; width: 337px; height: 67px; }
    div.addCandidateButton:hover { background: #4172E3 url(images/nodata/candidateButton-o.jpg); cursor: pointer; width: 337px; height: 67px; }
    div.addMassImportButton { background: #4172E3 url(images/nodata/addMassImport.jpg); cursor: pointer; width: 337px; height: 67px; }
    div.addMassImportButton:hover { background: #4172E3 url(images/nodata/addMassImport-o.jpg); cursor: pointer; width: 337px; height: 67px; }
    </style>
    <script type="text/javascript">
        function SaveFilter(param){
            //alert(param);
            //console.log(param);
            var filterName = prompt("Filter Name", "");
            if(filterName != null && filterName != "")
            {
                $.ajax({
                    type: "GET",
                    url: "index.php?m=candidates&a=filter&name="+filterName+"&filterArea="+param,
                    data: "data",
                    success: function(result){
                        if(alert == -1)
                        {
                            alert("Filter couldn't save successfully.")
                        }
                        else
                        {
                            //alert(result);
                            location.reload();
                        }
                    }
                });
            }
        }
        function DeleteFilter(filterid)
        {
            $.ajax({
                type: "GET",
                url: "index.php?m=candidates&a=filter&filterid="+filterid,
                data: "data",
                success: function(result){
                    if(alert == -1)
                    {
                        alert("Filter couldn't removed successfully.")
                    }
                    else
                    {
                        //alert(result);
                        location.reload();
                    }
                }
            });
        }
    </script>
    <div id="main">
        <?php TemplateUtility::printQuickSearch(); ?>

        <div id="contents"<?php echo !$this->totalCandidates ? ' style="background-color: #E6EEFF; padding: 0px;"' : ''; ?>>
            <?php if ($this->totalCandidates): ?>
            <table width="100%">
                <tr>
                    <td width="3%">
                        <img src="images/candidate.gif" width="24" height="24" alt="Candidates" style="border: none; margin-top: 3px;" />&nbsp;
                    </td>
                    <td><h2><?php if($_REQUEST['a'] == 'new_candidates') {echo "Candidates: New Candidates";} else if($_REQUEST['a'] == 'PipeLine') {echo "Candidates: Pipeline Candidates"; } else if($_REQUEST['a'] == 'follow') { echo "Candidates: Follow up Candidates";} else if($_REQUEST['a'] == 'Inactive'){ echo "Candidates: Inactive Candidates";} else if($_REQUEST['a'] == 'CareerPage'){ echo "Candidates: Career Page";} else if($_REQUEST['a'] == 'LinkedIn'){ echo "Candidates: LinkedIn";}  else if($_REQUEST['a'] == 'HotCandidates'){ echo "Candidates: Hot Candidates";} else { echo "Candidates: All Candidates" ;} ?> </h2></td>
                    <?php if($_REQUEST['a'] != 'LinkedIn'){?>
                    <td align="right">
                        <form name="candidatesViewSelectorForm" id="candidatesViewSelectorForm" action="<?php echo(RESUFLOUtility::getIndexName()); ?>" method="get">
                            <input type="hidden" name="m" value="candidates" />
                            <input type="hidden" name="a" value="listByView" />

                            <table class="viewSelector">
                                <tr>
                                    <td valign="top" align="right" nowrap="nowrap">
                               <?php $this->dataGrid->printNavigation(false); ?>
                                    </td>
                                    <td valign="top" align="right" nowrap="nowrap">
                                        <input type="checkbox" name="onlyMyCandidates" id="onlyMyCandidates" <?php if ($this->dataGrid->getFilterValue('OwnerID') ==  $this->userID): ?>checked<?php endif; ?> onclick="<?php echo $this->dataGrid->getJSAddRemoveFilterFromCheckbox('OwnerID', '==',  $this->userID); ?>" />
                                        Only My Candidates&nbsp;
                                    </td>
                                    <td valign="top" align="right" nowrap="nowrap">
                                        <input type="checkbox" name="onlyHotCandidates" id="onlyHotCandidates" <?php if ($this->dataGrid->getFilterValue('IsHot') == '1'): ?>checked<?php endif; ?> onclick="<?php echo $this->dataGrid->getJSAddRemoveFilterFromCheckbox('IsHot', '==', '\'1\''); ?>" />
                                        <label for="onlyHotCandidates">Only Hot Candidates</label>&nbsp;
                                    </td>
                                </tr>
                            </table>
                        </form>
                    </td>
                    <?php } else if ($_REQUEST['a'] == 'LinkedIn'){ ?>
                    <td style="width: 180px;">
                        <style>
                            .menu{
                                padding: 5px 5px;
                                text-align: center;
                            }
                            .menu img{
                                vertical-align: bottom;
                            }
                            .active, .active:hover{
                                color: #fff;
                                background-color: #3bc0c3;
                            }
                            .menu > a{
                                color: #fff;
                                font-weight: bold;
                                text-decoration: none;
                                display: block;
                            }
                            .menu > a:hover{
                                opacity: 0.8;
                            }
                            .hide{
                                display: none !important;
                            }
                        </style>
                        <div style="background-color: #1f2944; color: white; padding: 0px;">
                            <table width="100%" style="border-spacing: 0px;">
                                <tr>
                                    <td class="menu active"><a href="#profile"><img src="images/mru/candidate.gif" /> Profiles</a></td>
                                    <td class="menu"><a href="#inbox"><img src="images/actions/email.gif" /> Inbox (<?php echo LinkedIn::getUnreadCount(); ?>) </a></td>
                                </tr>
                            </table>
                        </div>
                    </td>
                    <?php }?>
                </tr>
            </table>
            <?php if ($_REQUEST['a'] == 'LinkedIn'){ ?>
            <div class="hide inbox">
                <iframe style="width: 100%;height: 655px; border: none;" src="<?php echo RESUFLOUtility::getIndexName(); ?>?m=linkedin" ></iframe>
            </div> 
            <?php } ?>

            <?php if ($this->topLog != ''): ?>
            <div style="margin: 20px 0px 20px 0px;">
                <?php echo $this->topLog; ?>
            </div>
            <?php endif; ?>

            <?php if ($this->errMessage != ''): ?>
            <div id="errorMessage" style="padding: 25px 0px 25px 0px; border-top: 1px solid #800000; border-bottom: 1px solid #800000; background-color: #f7f7f7;margin-bottom: 15px;">
            <table>
                <tr>
                    <td align="left" valign="center" style="padding-right: 5px;">
                        <img src="images/large_error.gif" align="left">
                    </td>
                    <td align="left" valign="center">
                        <span style="font-size: 12pt; font-weight: bold; color: #800000; line-height: 12pt;">There was a problem with your request:</span>
                        <div style="font-size: 10pt; font-weight: bold; padding: 3px 0px 0px 0px;"><?php echo $this->errMessage; ?></div>
                    </td>
                </tr>
            </table>
            </div>
            <?php endif; ?>

            <p class="note">
                <span style="float:left;">Candidates - Page <?php echo($this->dataGrid->getCurrentPageHTML()); ?> (<?php echo($this->dataGrid->getNumberOfRows()); ?> Items)</span>
                <span style="float:right;">
                    <?php $this->dataGrid->drawRowsPerPageSelector(); ?>
                    <?php $this->dataGrid->drawShowFilterControl(); ?>
                </span>&nbsp;
            </p>

            <?php $this->dataGrid->drawFilterArea(); ?>
            <?php $this->dataGrid->draw();  ?>

            <div style="display:block;">
                <span style="float:left;">
                    <?php $this->dataGrid->printActionArea(); ?>
                </span>
                <span style="float:right;">
                    <?php $this->dataGrid->printNavigation(true); ?>
                </span>&nbsp;
            </div>

            <?php else: ?>

            <br /><br /><br /><br />
            <div style="height: 95px; background: #E6EEFF url(images/nodata/candidatesTop.jpg);">
                &nbsp;
            </div>
            <br /><br />
            <table cellpadding="0" cellspacing="0" border="0" width="956">
                <tr>
                <td style="padding-left: 62px;" align="center" valign="center">

                    <div style="text-align: center; width: 600px; line-height: 22px; font-size: 18px; font-weight: bold; color: #666666; padding-bottom: 20px;">
                    Add candidates to keep track of possible applicants you can consider for your job orders.
                    </div>

                    <table cellpadding="10" cellspacing="0" border="0">
                        <tr>
                            <td style="padding-right: 20px;">
                                <a href="<?php echo RESUFLOUtility::getIndexName(); ?>?m=candidates&amp;a=add">
                                <div class="addCandidateButton">&nbsp;</div>
                                </a>
                            </td>
                            <td>
                                <a href="<?php echo RESUFLOUtility::getIndexName(); ?>?m=import&amp;a=massImport">
                                <div class="addMassImportButton">&nbsp;</div>
                                </a>
                            </td>
                        </tr>
                    </table>
                </td>

                </tr>
            </table>

            <?php endif; ?>
        </div>
    </div>

    <div id="bottomShadow"></div>
<?php TemplateUtility::printFooter(); ?>
<script type="text/javascript">
    $(document).ready(function(){
        $(".menu").click(function(){
            $(".menu").removeClass("active");
            $(this).addClass("active");
            if($(this).find("a").attr("href")=="#inbox"){
                $(".note").addClass("hide");
                $("div[id*='OverflowDiv']").addClass("hide");
                $("div[id*='OverflowDiv']").next("div").addClass("hide");
                
                $(".inbox").removeClass("hide");
            }
            else if($(this).find("a").attr("href")=="#profile"){
                $(".inbox").addClass("hide");
                
                $(".note").removeClass("hide");
                $("div[id*='OverflowDiv']").removeClass("hide");
                $("div[id*='OverflowDiv']").next("div").removeClass("hide");
            }
        });
    });
</script>