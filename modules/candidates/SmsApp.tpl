<?php /* $Id: Edit.tpl 3695 2007-11-26 22:01:04Z brian $ */ ?>
<?php TemplateUtility::printHeader('Candidates', array('modules/candidates/validator.js', 'js/sweetTitles.js', 'js/listEditor.js', 'js/doubleListEditor.js')); ?>
<?php TemplateUtility::printHeaderBlock(); ?>
<?php TemplateUtility::printTabs($this->active); ?>
<div id="main">
    <?php TemplateUtility::printQuickSearch(); ?>
    <script>
        function SearchSmsLog(pointer) {
            var SearchValue = $(pointer).val();
            $(".chat-name").each(function () {
                var CandidateName = $(this).html();
                console.log(CandidateName);
                if (CandidateName.toLowerCase().indexOf(SearchValue.toLowerCase()) >= 0) {
                    $(this).closest("a").removeClass("hide");
                }
                else {
                    $(this).closest("a").addClass("hide");
                }
            });
        };
    </script>
    <div id="contents" style="padding: 0px;">
        <div id="sendMessage">
            <table style="border-collapse: collapse;" class="width100">
                <tr class="bb-lg">
                    <td class="br-lg" style="width: 300px;">
                        <form id="SearchForm" name="SearchForm" action="<?php echo(RESUFLOUtility::getIndexName()); ?>" method="get">
                            <input type="hidden" id="m" name="m" value="candidates">
                            <input type="hidden" id="a" name="a" value="smsapp">
                            <input type="hidden" id="candidateID" name="candidateID" value="<?php echo $_GET['candidateID']; ?>">
                            <table class="width100">
                                <tr>
                                    <td>
                                        <select name="IsRead" onchange="$('#SearchForm').submit();">
                                            <option value="0" <?php echo (($_GET['IsRead'] == "0")? "selected" : ""); ?>>All</option>
                                            <option value="1" <?php echo (($_GET['IsRead'] == "1")? "selected" : ""); ?>>Unread</option>
                                            <option value="-1" <?php echo (($_GET['IsRead'] == "-1")? "selected" : ""); ?>>Stopped</option>
                                        </select>
                                    </td>
                                    <td><input class="nofocus" type="text" id="Search" name="Search" onkeyup="SearchSmsLog(this)" value="<?php echo $_GET['Search']; ?>" style="border: none;padding: 15px 5px;" placeholder="Search Name"/></td>
                                    <td onclick="$('#SearchForm').submit();" style="text-align: center;cursor: pointer;"><img src="./images/search.gif" /></td>
                                </tr>
                            </table>
                        </form>
                    </td>
                    <td>
                        <table class="width100">
                            <tr>
                                <td class="width50"><?php echo $this->CandidatePhoneCell;?></td>
                                <td class="width50"><a href ="<?php echo(RESUFLOUtility::getIndexName()); ?>?m=candidates&amp;a=show&amp;candidateID=<?php echo $this->candidateID; ?>&amp;view=1"><?php echo $this->CandidateName;?></a></td>
                            </tr>
                        </table>
                    </td>
                </tr>
                <tr>
                    <td class="br-lg" style="padding: 0px;">
                        <div style="height: 735px; overflow-y: auto;">
                            <?php foreach ($this->SmsLogList as $smslog){ ?>
                            <a class="no-link" href="<?php echo(RESUFLOUtility::getIndexName()); ?>?m=candidates&amp;a=smsapp&amp;candidateID=<?php echo $smslog['CandidateId']; ?>&Search=<?php echo $_GET['Search']; ?>&IsRead=<?php echo $_GET['IsRead']; ?>">
                                <div class="hover-bg-lg bb-lg <?php echo ($smslog['CandidateId']==$_GET['candidateID'])? 'bg-lg' : ''; ?>" style="padding: 5px;">
                                    <div class="<?php echo (($smslog['IsRead'] == 0)? '' : 'text-red'); ?> chat-name"><?php echo $smslog['first_name'].' '.$smslog['last_name'] ?></div>
                                    <div style="color: gray;"><?php echo (strlen($smslog['Body']) > 42)? substr($smslog['Body'],0, 42).'...' : $smslog['Body']  ?></div>
                                </div>
                            </a>
                            <?php } ?>
                        </div>
                        <div><?php TemplateUtility::printSmallNavigation($this->SmsLogListCount, 50, $this->Page, $this->NavLink); ?></div>
                    </td>
                    <td>
                        <table style="border-collapse: collapse;" class="width100">
                            <tr>
                                <td colspan="2">
                                   <div id='messageList' style="height: 650px; overflow-y: auto;">
                                        <?php foreach ($this->data as $sms){ ?>
                                        <div style="margin: 2px; text-align: <?php if($sms['Direction'] == 1){echo 'right;';} else{ echo 'left'; } ?>;">
                                        </div>
                                        <table style="width: 100%;">
                                            <tr>
                                                <?php if($sms['Direction'] == 1){echo '<td style=\'width: 200px;\'>&nbsp;</td>';}?>
                                                <td>
                                                    <div style="padding: 10px; background-color:<?php if($sms['Direction'] == 1){echo 'skyblue;float: right;';} else{ echo 'lightgray;float: left;'; } ?>">
                                                        <?php   echo str_replace("\r\n","<br/>",$sms['Body']); ?>
                                                        <div style="color: white; text-align: <?php if($sms['Direction'] == 1){echo 'right;';} else{ echo 'left'; } ?>;">
                                                            <?php   echo $sms['date_created']; ?>
                                                        </div>
                                                    </div>
                                                    <?php if($sms['Direction'] == 1 && !empty($sms['MessageSid'])){ ?>
                                                        <a title="<?php echo $sms['MessageSid']; ?>" href="<?php echo(RESUFLOUtility::getIndexName()); ?>?m=candidates&amp;a=smsapp&amp;msgid=<?php echo $sms['DripSmsReplyId']; ?>&amp;candidateID=<?php echo $_GET['candidateID']; ?>&Search=<?php echo $_GET['Search']; ?>&IsRead=<?php echo $_GET['IsRead']; ?>" style="float: right; color: red;">Failed, Resend?</a>
                                                    <?php } ?>
                                                </td>
                                                <?php if($sms['Direction'] == 2){echo '<td style=\'width: 200px;\'>&nbsp;</td>';}?>
                                            </tr>
                                        </table>
                                        <br>
                                        <?php } ?>
                                    </div> 
                                </td>
                            </tr>
                            <tr>
                                <td colspan="2">
                                    <table style="border-collapse: collapse;" class="width100">
                                        <tr>
                                            <td colspan="3">
                                                <div id="Tags" class="hide" style="color: gray;">
                                                    $$NAME$$, $$FIRSTNAME$$, $$LASTNAME$$, $$EMAIL$$, $$PHONE$$, $$CSOURCE$$, $$CURDATE$$
                                                </div>
                                            </td>
                                        </tr>
                                        <tr class="bt-lg">
                                            <?php if($this->unsubscribe == 1){ ?>
                                                <td style="width: 100%;">
                                                    candidate unsubscribed from service...
                                                </td>
                                            <?php } else{ ?>
                                                <td style="width: 100%;">
                                                    <textarea class="nofocus noborder" id='Message' placeholder="Write something" style="width: 100%; overflow-y: auto; resize: none; height: 60px;"></textarea>
                                                </td>
                                                <td>
                                                    <button id='sendButton' style="padding: 10px;" onclick="SendMessage()">SEND</button>
                                                </td>
                                                <td>
                                                    <button id='sendButton' style="padding: 10px 2px;" onclick="$('#Tags').toggleClass('hide');">Tags</button>
                                                </td>
                                            <?php } ?>
                                        </tr>
                                    </table>
                                </td>
                            </tr>
                        </table>
                    </td>
                </tr>
            </table>
        </div>
        <script type="text/javascript">
            var check = true;
            $("#messageList").prop({ scrollTop: $("#messageList").prop("scrollHeight") });
            function SendMessage(){
                $('#sendButton').html('Please wait...');
                var Message = document.getElementById('Message').value;
                if(Message != '' && check == true){
                    check = false;
                    /* FIXME: Spinner? */
                    var http = AJAX_getXMLHttpObject();
                    /* Build HTTP POST data. */
                    var POSTData = '&CandidateId='+'<?php echo $this->candidateID;?>'+'&To='+'<?php echo $this->CandidatePhoneCell;?>'+'&Message=' + Message;
                    console.log(POSTData);
                    /* Anonymous callback function triggered when HTTP response is received. */
                    var callBack = function ()
                    {
                        if (http.readyState != 4)
                        {
                            return;
                        }
                        if(http.responseXML != null){
                            /* Return if we have any errors. */
                            var errorCodeNode    = http.responseXML.getElementsByTagName('errorcode').item(0);
                            var errorMessageNode = http.responseXML.getElementsByTagName('errormessage').item(0);
                            if (!errorCodeNode.firstChild || errorCodeNode.firstChild.nodeValue != '0')
                            {
                                if (errorMessageNode.firstChild)
                                {
                                    var errorMessage = "An error occurred while receiving a response from the server.\n\n"
                                                     + errorMessageNode.firstChild.nodeValue;
                                    alert(errorMessage);
                                }
                                else{
                                    location.reload();
                                    //alert(http.responseXML.getElementsByTagName('response')[0].innerHTML);
                                }
                                return;
                            }else{
                                location.reload();
                                //alert(http.responseXML.getElementsByTagName('response')[0].innerHTML);
                            }
                        }
                        else{
                            location.reload();
                            //alert(http.response);
                        }
                    }

                    AJAX_callRESUFLOFunction(
                        http,
                        'sendSms',
                        POSTData,
                        callBack,
                        0,
                        null,
                        false,
                        false
                    );
                }
            }
        </script>
    </div>
</div>
<div id="bottomShadow"></div>
<?php TemplateUtility::printFooter(); ?>
