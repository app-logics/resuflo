
<?php /* $Id: SendEmail.tpl 3078 2007-09-21 20:25:28Z will $ */ ?>
<?php TemplateUtility::printHeader('Candidates', array('modules/candidates/validator.js', 'js/searchSaved.js', 'js/sweetTitles.js', 'js/searchAdvanced.js', 'js/highlightrows.js', 'js/export.js')); ?>
<?php TemplateUtility::printHeaderBlock(); ?>
<?php TemplateUtility::printTabs($this->active); ?>

<!-- TinyMCE -->
<!--<script src="jscripts/tinymce/tinymce.min.js" type="text/javascript"></script>-->
<!-- /TinyMCE -->

<?php
	
/*	if ((($_FILES["file"]["type"] == "image/doc")|| ($_FILES["file"]["type"] == "image/jpeg")|| ($_FILES["file"]["type"] == "image/pjpeg"))	&& ($_FILES["file"]["size"] < 200))
	  {
		 if ($_FILES["file"]["error"] > 0)
			{
				echo "Return Code: " . $_FILES["file"]["error"] . "<br />";
			}
		 else
			{
			echo "Upload: " . $_FILES["file"]["name"] . "<br />";
			echo "Type: " . $_FILES["file"]["type"] . "<br />";
			echo "Size: " . ($_FILES["file"]["size"] / 1024) . " Kb<br />";
			echo "Temp file: " . $_FILES["file"]["tmp_name"] . "<br />";
		
			 if (file_exists("upload/" . $_FILES["file"]["name"]))
				  {
				  echo $_FILES["file"]["name"] . " already exists. ";
				  }
				else
				  {
					  move_uploaded_file($_FILES["file"]["tmp_name"],
					  "upload/" . $_FILES["file"]["name"]);
					echo "Stored in: " . "upload/" . $_FILES["file"]["name"];
				  }
		  }
    }
	else
	  {
	  echo "Invalid file";
	  }

*/
 ?> 



    <div id="main">
        <?php TemplateUtility::printQuickSearch(); ?>

        <div id="contents">
		<?php
 		if($this->success == true)
            {
				 $candidate_name = $this->candidate_name;
				 $candidateID = $this->candidate_id;
               
			
			
               ?> <br />
                <span style="font-size: 12pt; font-weight: 900;">
                Your e-mail has been successfully sent to the following recipients:
                <blockquote>
               <a href ="<?php echo(RESUFLOUtility::getIndexName()); ?>?m=candidates&amp;a=show&amp;candidateID=<?php echo $candidateID; ?>" > <?php echo $candidate_name; ?> </a>
                </blockquote>


                <?php
            }
            else
            {
			 $userid= $this->userid;
		     $email = $this->recipients;
			 if($email)
			 {	
				$sql = mysql_query("select  last_name,  first_name, candidate_id  from candidate where email1 = '$email' AND  entered_by = '$userid' ");
				$row = mysql_fetch_assoc($sql);
				$candidate_id = $row['candidate_id']; 
				$last_name = $row['last_name']; 
				$first_name = $row['first_name']; 
			 }
			$signature = $this->recipient;
			 
			   $tabIndex = 1;          
				   
                ?>
				  <table>
                <tr>
                    <td width="3%">
                        <img src="images/candidate.gif" width="24" height="24" border="0" alt="Candidates" style="margin-top: 3px;" />&nbsp;
                    </td>
                    <td><h2>Candidates: Send E-mail</h2></td>
                </tr>
            </table>

            <p class="note">Send Candidates E-mail</p>
				
            <table class="editTable" width="100%">
            
			    <tr>
                    <td>
                        <form name="emailForm" id="emailForm" action="<?php echo(RESUFLOUtility::getIndexName()); ?>?m=candidates&amp;a=emailcandidate" method="post" onsubmit="return checkEmailForm(document.emailForm);" autocomplete="off" enctype="multipart/form-data">
                      	  <input type="hidden" name="countryKey" id="countryKey" value="<?php echo($_SESSION['RESUFLO']->getCountryKey())?>"  />
               <?php if(isset($_FILES['resumecontent'])){ echo $parsedhiddeninfo;}?>
					    <input type="hidden" name="postback" id="postback" value="postback" />
                       	<input type="hidden" name="candidate_id" id="candidate_id" value="<?php echo $candidate_id;?>" />
						<input type="hidden" name="candidate_name" id="candidate_name" value="<?php echo $first_name.' '.$last_name;?>" />
                       	<input type="hidden" name="filename" value="<?php echo $filename; ?>" />
					    <table>
                            <tr>
                                <td class="tdVertical" style="text-align: right;">
                                    To
                                </td>
                                <td class="tdData">
                                  <textarea class="inputbox" name="emailTo" id="emailTo" rows="2", cols="90" tabindex="99" style="width: 600px;" readonly><?php echo $email; ?></textarea>
                                </td>
                            </tr>
                            <tr>
                                <td class="tdVertical" style="text-align: right;">
                                    <label id="emailSubjectLabel" for="emailSubject">Email Template</label>
                                </td>
                                <td>
                                    <?php foreach($this->emailArray as $row){ ?>
                                        <div id=Message<?php echo $row['email_template_id']; ?> style="display: none;"><?php echo $row['text']; ?></div>
                                        <div id=Subject<?php echo $row['email_template_id']; ?> style="display: none;"><?php echo $row['name']; ?></div>
                                    <?php } ?>
                                    <select id="emailTemplate" class="inputbox" style="width: 602px;">
                                        <option>Select Email Template</option>
                                        <?php foreach($this->emailArray as $row){ ?>
                                            <option value=<?php echo $row['email_template_id']; ?>><?php echo $row['name']; ?></option>
                                            <div id=<?php echo $row['email_template_id']; ?> style="display: none;"><?php echo $row['text']; ?></div>
                                        <?php } ?>
                                    </select>
                                </td>
                            </tr>
                            <tr>
                                <td class="tdVertical" style="text-align: right;">
                                    <label id="emailSubjectLabel" for="emailSubject">Subject</label>
                                </td>
                                <td class="tdData">
                                    <input id="emailSubject" tabindex="<?php echo($tabIndex++); ?>" type="text" maxlength="50" name="emailSubject" class="inputbox" style="width: 600px;" />
                                </td>
                            </tr>
							 <tr>
			   					<td class="tdVertical" style="text-align: right;">
                                    <label id="emailBodyLabel" for="emailBody">Attach Document</label>
									
									</td>
									  <td class="tdData">
									<input type="file" name="uploadedfile" id="uploadedfile"  tabindex="<?php echo($tabIndex++); ?>" class="inputbox" style="width: 600px;" />
									<!--<input type="submit" name="sub1" value="Upload" /><?php if($msg){ echo "<br>".$msg ;} ?>-->
								
								
								</td>
			    				</tr>
							<tr>
							
                                <td class="tdVertical" style="text-align: right;">
                                    <label id="emailBodyLabel" for="emailBody">Body</label>
                                </td>
                                <td class="tdData" style="background:#fff;">
                                    <textarea id="emailBody" tabindex="<?php echo($tabIndex++); ?>" name="emailBody" rows="10" cols="90" style="width: 600px;" class="mceEditor" ><?php 
 echo "<br><br>" ; echo $signature; ?> </textarea >
                                </td>
								
                            </tr>
						     <tr>
                                <td align="right" valign="top" colspan="2">
									
                                    <input type="submit" tabindex="<?php echo($tabIndex++); ?>" class="button" value="Send E-Mail" />&nbsp;
                                    <input type="reset"  tabindex="<?php echo($tabIndex++); ?>" class="button" value="Reset" />&nbsp;
                                </td>
                            </tr>
							
                        </table>

                        </form>
					
						
                        <script type="text/javascript">
                        document.emailForm.emailSubject.focus();
                        $("#emailTemplate").change(function(){
                            CKEDITOR.instances['emailBody'].insertHtml($("#Message"+$(this).val()).html());
                            //nicEditors.findEditor( "emailBody" ).setContent($("#Message"+$(this).val()).html());
                            $("#emailSubject").val($("#Subject"+$(this).val()).html());
                            //alert($(this).val());
                        });
                        </script>
                    </td>
                </tr>
            </table>
           <?php
           }
            ?>
        </div>
    </div>
    <div id="bottomShadow"></div>
<script src="jscripts/ckeditor/ckeditor.js" type="text/javascript"></script> 
<script type="text/javascript">
    CKEDITOR.replace('emailBody');
</script>
<?php TemplateUtility::printFooter(); ?>
