<?php /* $Id: Edit.tpl 3695 2007-11-26 22:01:04Z brian $ */ ?>
<?php TemplateUtility::printHeader('Candidates', array('modules/candidates/validator.js', 'js/sweetTitles.js', 'js/listEditor.js', 'js/doubleListEditor.js')); ?>
<?php TemplateUtility::printHeaderBlock(); ?>
<?php TemplateUtility::printTabs($this->active); ?>
<div id="main">
    <?php TemplateUtility::printQuickSearch(); ?>
    <div id="contents">
        <div id="sendMessage">
            <table>
                <tr>
                    <td colspan="2">
                       <div id='messageList' style="height: 600px; overflow-y: auto;">
                            <?php foreach ($this->data as $sms){ ?>
                            <div style="margin: 2px; text-align: <?php if($sms['Direction'] == 1){echo 'right;';} else{ echo 'left'; } ?>;">
                                <?php   if($sms['Direction'] == 1){echo 'You (';} else{ ?>
                                    <a href ="<?php echo(RESUFLOUtility::getIndexName()); ?>?m=candidates&amp;a=show&amp;candidateID=<?php echo $this->candidateID; ?>&amp;view=1">
                                        <?php echo $sms['first_name'].' '.$sms['last_name']; ?>
                                    </a>
                                <?php echo ' ('; }; echo $sms['date_created'].')'; ?>
                            </div>
                            <table style="width: 100%;">
                                <tr>
                                    <?php if($sms['Direction'] == 1){echo '<td style=\'width: 200px;\'>&nbsp;</td>';}?>
                                    <td>
                                        <div style="padding: 10px; background-color:<?php if($sms['Direction'] == 1){echo 'skyblue;float: right;';} else{ echo 'lightgray;float: left;'; } ?>">
                                            <?php   echo $sms['Body']; ?>
                                        </div>
                                    </td>
                                    <?php if($sms['Direction'] == 2){echo '<td style=\'width: 200px;\'>&nbsp;</td>';}?>
                                </tr>
                            </table>
                            <br>
                            <?php } ?>
                        </div> 
                    </td>
                </tr>
                <tr>
                    <td style="width: 100%;">
                        <textarea id='Message' style="width: 100%; overflow-y: auto; resize: none; height: 60px;"></textarea>
                    </td>
                    <td>
                        <button id='sendButton' style="height: 68px; width: 100px;" onclick="SendMessage()">SEND</button>
                    </td>
                </tr>
            </table>
        </div>
        <script type="text/javascript">
            var check = true;
            $("#messageList").prop({ scrollTop: $("#messageList").prop("scrollHeight") });
            function SendMessage(){
                $('#sendButton').html('Please wait...');
                var Message = document.getElementById('Message').value;
                if(Message != '' && check == true){
                    check = false;
                    /* FIXME: Spinner? */
                    var http = AJAX_getXMLHttpObject();
                    /* Build HTTP POST data. */
                    var POSTData = '&CandidateId='+'<?php echo $this->candidateID;?>'+'&To='+'<?php echo $this->CandidatePhoneCell;?>'+'&Message=' + Message;
                    console.log(POSTData);
                    /* Anonymous callback function triggered when HTTP response is received. */
                    var callBack = function ()
                    {
                        if (http.readyState != 4)
                        {
                            return;
                        }
                        if(http.responseXML != null){
                            /* Return if we have any errors. */
                            var errorCodeNode    = http.responseXML.getElementsByTagName('errorcode').item(0);
                            var errorMessageNode = http.responseXML.getElementsByTagName('errormessage').item(0);
                            if (!errorCodeNode.firstChild || errorCodeNode.firstChild.nodeValue != '0')
                            {
                                if (errorMessageNode.firstChild)
                                {
                                    var errorMessage = "An error occurred while receiving a response from the server.\n\n"
                                                     + errorMessageNode.firstChild.nodeValue;
                                    alert(errorMessage);
                                }
                                else{
                                    location.reload();
                                    //alert(http.responseXML.getElementsByTagName('response')[0].innerHTML);
                                }
                                return;
                            }else{
                                location.reload();
                                //alert(http.responseXML.getElementsByTagName('response')[0].innerHTML);
                            }
                        }
                        else{
                            location.reload();
                            //alert(http.response);
                        }
                    }

                    AJAX_callRESUFLOFunction(
                        http,
                        'sendSms',
                        POSTData,
                        callBack,
                        0,
                        null,
                        false,
                        false
                    );
                }
            }
        </script>
    </div>
</div>
<div id="bottomShadow"></div>
<?php TemplateUtility::printFooter(); ?>
