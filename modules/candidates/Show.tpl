<?php /* $Id: Show.tpl 3814 2007-12-06 17:54:28Z brian $ */ ?>
<link rel="stylesheet" href="css/jquery.ui.all.css">
<style>
    .highlight {
        background-color: yellow;
    }
</style>
<script src="js/jquery-1.5.1.js"></script>
<script src="js/ui/jquery.ui.core.js"></script>
<script src="js/ui/jquery.ui.widget.js"></script>
<script src="js/ui/jquery.ui.accordion.js"></script>
<link rel="stylesheet" href="css/demos.css">
<script>
    $(function() {
        $( "#accordion" ).accordion({ 
            autoHeight: false,
            navigation: true,
            collapsible: true
        });
    });
</script>









<?php if ($this->isPopup): ?>
<?php TemplateUtility::printHeader('Candidate - '.$this->data['firstName'].' '.$this->data['lastName'], array( 'js/activity.js', 'js/sorttable.js', 'js/match.js', 'js/lib.js', 'js/pipeline.js', 'js/attachmentss.js')); ?>
<?php else: ?>
<?php TemplateUtility::printHeader('Candidate - '.$this->data['firstName'].' '.$this->data['lastName'], array( 'js/activity.js', 'js/sorttable.js', 'js/match.js', 'js/lib.js', 'js/pipeline.js', 'js/attachment.js')); ?>
<?php TemplateUtility::printHeaderBlock(); ?>
<?php TemplateUtility::printTabs($this->active); ?>
<div id="main">
    <?php TemplateUtility::printQuickSearch(); ?>
    <?php endif; ?>
    <?php
    function curPageURL() {
    $pageURL = 'http';
    if ($_SERVER["HTTPS"] == "on") {$pageURL .= "s";}
    $pageURL .= "://";
    if ($_SERVER["SERVER_PORT"] != "80") {
    $pageURL .= $_SERVER["SERVER_NAME"].":".$_SERVER["SERVER_PORT"].$_SERVER["REQUEST_URI"];
    } else {
    $pageURL .= $_SERVER["SERVER_NAME"].$_SERVER["REQUEST_URI"];
    }
    return $pageURL;
    }
    $page=curPageURL();
    $getpage = substr($page, 0, strpos($page, 'candidateID='));
    $candidate_id_for_subscribe = $_REQUEST['candidateID'];;
    $userid=$_SESSION['RESUFLO']->getUserID();
    
    ?>
    
    
    
    <style type="text/css">
        
        body {
            margin-left: 0px;
            margin-top: 0px;
            margin-right: 0px;
            margin-bottom: 0px;
        }
        .top_heading{ background:#00568b; font-family:Arial, Helvetica, sans-serif; font-size:12px; color:#fff; padding:5px; font-weight:bold;}
        
        .detail_heading{ background:#f0f0f0; font-family:Arial, Helvetica, sans-serif; font-size:12px; color:#282626; padding:10px; font-weight:bold; border-bottom:1px solid #ccc; vertical-align:top;}
        .deatils{  font-family:Arial, Helvetica, sans-serif; font-size:12px; color:#282626; padding:10px; border-bottom:1px solid #ccc; vertical-align:top;}
        
        .top_heading a{ background:#00568b; font-family:Arial, Helvetica, sans-serif; font-size:12px; color:#fff; padding:5px; font-weight:bold;}
        #ResumeDetail span.highlight{
            background-color: yellow;
        }
        
    </style>
    <script type="text/javascript">
                <!--
        function printContent(id){
            str=document.getElementById(id).innerHTML;
			
            newwin=window.open('','','location=no,titlebar=no,status=no,fullscreen=no');
            newwin.document.write('<HTML>\n<HEAD>\n')
            newwin.document.write('<title></title><script>\n')
            newwin.document.write('function chkstate(){\n')
            newwin.document.write('if(document.readyState=="complete"){\n')
            newwin.document.write('window.close()\n')
            newwin.document.write('}\n')
            newwin.document.write('else{\n')
            newwin.document.write('setTimeout("chkstate()",2000)\n')
            newwin.document.write('}\n')
            newwin.document.write('}\n')
                    newwin.document.write('function print_win(){\n')
            newwin.document.write('window.print();\n')
            newwin.document.write('chkstate();\n')
            newwin.document.write('}\n')
                    newwin.document.write('<\/script>\n')
                    newwin.document.write('</HEAD>\n')
                    newwin.document.write('<BODY onload="print_win()">\n')
                    newwin.document.write(str)
                    newwin.document.write('</BODY>\n')
                    newwin.document.write('</HTML>\n')
                    newwin.document.close()
                }
		
                //-->
    </script>
    <script type="text/javascript">
                <!--
        function viewpdf(id){
            data=document.getElementById(id).innerHTML;
			
            document.write(data);
        }
		
        //-->
    </script>
    <script>
        function highlightSearch(content, container) {
            if(content == '')
            {
                return;
            }
            var query = new RegExp("(\\b" + content + "\\b)", "ig");
            var e = document.getElementById(container).innerHTML;
            //console.log(content);
            str=e;
            output=str.replace(query,'<span class="highlight">'+content+'</span>'); 


            //var enew = e.replace(/(<span>|<\/span>)/igm, "");
            //document.getElementById(container).innerHTML = enew;
            //var newe = enew.replace(query, "<span>$1</span>");
            //alert(newe);
            if(output.length != e.length){
                document.getElementById(container).innerHTML = output;
            }
            else
            {
                //alert(content);
            }

        }
        
        function Hilitor(id, tag)
        {

          var targetNode = document.getElementById(id) || document.body;
          var hiliteTag = tag || "EM";
          var skipTags = new RegExp("^(?:" + hiliteTag + "|SCRIPT|FORM|SPAN)$");
          var colors = ["#ff6", "#ff6", "#ff6", "#ff6", "#ff6"];
          //var colors = ["#ff6", "#a0ffff", "#9f9", "#f99", "#f6f"];
          var wordColor = [];
          var colorIdx = 0;
          var matchRegex = "";
          var openLeft = false;
          var openRight = false;

          this.setMatchType = function(type)
          {
            switch(type)
            {
              case "left":
                this.openLeft = false;
                this.openRight = true;
                break;
              case "right":
                this.openLeft = true;
                this.openRight = false;
                break;
              case "open":
                this.openLeft = this.openRight = true;
                break;
              default:
                this.openLeft = this.openRight = false;
            }
          };

          this.setRegex = function(input)
          {
              //input = input.replace(/^[^\w]+|[^\w]+$/g, "").replace(/[^\w'-]+/g, "|");find sub words also
            input = input.replace(/^[^\w]+|[^\w]+$/g, "");
            var re = "(" + input + ")";
            if(!this.openLeft) re = "\\b" + re;
            if(!this.openRight) re = re + "\\b";
            matchRegex = new RegExp(re, "i");
          };

          this.getRegex = function()
          {
            var retval = matchRegex.toString();
            retval = retval.replace(/(^\/(\\b)?|\(|\)|(\\b)?\/i$)/g, "");
            retval = retval.replace(/\|/g, " ");
            return retval;
          };

          // recursively apply word highlighting
          this.hiliteWords = function(node)
          {
            if(node === undefined || !node) return;
            if(!matchRegex) return;
            if(skipTags.test(node.nodeName)) return;

            if(node.hasChildNodes()) {
              for(var i=0; i < node.childNodes.length; i++)
                this.hiliteWords(node.childNodes[i]);
            }
            if(node.nodeType == 3) { // NODE_TEXT
              if((nv = node.nodeValue) && (regs = matchRegex.exec(nv))) {
                if(!wordColor[regs[0].toLowerCase()]) {
                  wordColor[regs[0].toLowerCase()] = colors[colorIdx++ % colors.length];
                }

                var match = document.createElement(hiliteTag);
                match.appendChild(document.createTextNode(regs[0]));
                match.style.backgroundColor = wordColor[regs[0].toLowerCase()];
                match.style.fontStyle = "inherit";
                match.style.color = "#000";

                var after = node.splitText(regs.index);
                after.nodeValue = after.nodeValue.substring(regs[0].length);
                node.parentNode.insertBefore(match, after);
              }
            };
          };

          // remove highlighting
          this.remove = function()
          {
            var arr = document.getElementsByTagName(hiliteTag);
            while(arr.length && (el = arr[0])) {
              var parent = el.parentNode;
              parent.replaceChild(el.firstChild, el);
              parent.normalize();
            }
          };

          // start highlighting at target node
          this.apply = function(input)
          {
            //this.remove();
            if(input === undefined || !input) return;
            this.setRegex(input);
            this.hiliteWords(targetNode);
          };

        }
        function searchAndHighlight(searchTerm, selector, highlightClass, removePreviousHighlights) 
        {
            if (searchTerm) {
                //var wholeWordOnly = new RegExp("\\g"+searchTerm+"\\g","ig"); //matches whole word only
                //var anyCharacter = new RegExp("\\g["+searchTerm+"]\\g","ig"); //matches any word with any of search chars characters
                var selector = selector || "body",                             //use body as selector if none provided
                searchTermRegEx = new RegExp("(" + searchTerm + ")", "gi"),
                matches = 0,
                helper = {};
                helper.doHighlight = function (node, searchTerm) {
                    if (node.nodeType === 3) {
                        if (node.nodeValue.match(searchTermRegEx)) {
                            matches++;
                            var tempNode = document.createElement('span');
                            tempNode.innerHTML = node.nodeValue.replace(searchTermRegEx, "<span class=" + highlightClass + ">$1</span>");
                            node.parentNode.insertBefore(tempNode, node);
                            node.parentNode.removeChild(node);
                        }
                    }
                    else if (node.nodeType === 1 && node.childNodes && !/(style|script)/i.test(node.tagName)) {
                        $.each(node.childNodes, function (i, v) {
                            helper.doHighlight(node.childNodes[i], searchTerm);
                        });
                    }
                };
                if (removePreviousHighlights) {
                    $('.' + highlightClass).each(function (i, v) {
                        var $parent = $(this).parent();
                        $(this).contents().unwrap();
                        $parent.get(0).normalize();
                    });
                }
                //helper.doHighlight($(selector), searchTerm);
                $.each($(selector).children(), function (index, val) {
                    helper.doHighlight(this, searchTerm);
                });
                return matches;
            }
            return false;
        }
        
        $.fn.highlight = function(what,spanClass) {
            return this.each(function(){
                var container = this,
                    content = container.innerHTML,
                    pattern = new RegExp('(>[^<.]*)(' + what + ')([^<.]*)','gi'),
                    replaceWith = '$1<span ' + ( spanClass ? 'class="' + spanClass + '"' : '' ) + '">$2</span>$3',
                    highlighted = content.replace(pattern,replaceWith);
                container.innerHTML = highlighted;
            });
        }

        $(document).ready(function () {
            var myHilitor2;
            myHilitor2 = new Hilitor("ResumeDetail");
            myHilitor2.setMatchType("left");

            var filter = "<?php echo $this->dataGridFilter; ?>";
            var listId = "<?php echo $this->listId; ?>";
            if(!isNaN(listId)){
                filter = "<?php echo $this->dataGridSavedListFilter; ?>";
            }
            //console.log(filter);
            //Highlighter when filter is applied
            if(filter != '')
            {
                var filterValues = filter.split(',');
                $.each(filterValues, function(index, value) { 
                    if(value.indexOf("=")> 0){
                        var highlight = value.substring(value.indexOf("=")+2, value.length);
                        //console.log(highlight);
                        //highlight = decodeURIComponent(highlight);
                        if(highlight.indexOf("%")> 0){
                            var subFilterValues = highlight.split('%');
                            $.each(subFilterValues, function(subIndex, subValue) { 
                                //console.log(subValue.trim());
                                //highlightSearch(subValue.trim(), 'ResumeDetail');
                                
                                //Hiltor was showing substring also
                                //myHilitor2.apply(highlight);
                                myHilitor2.apply(subValue.trim());
                                //searchAndHighlight(subValue, "#contents", 'highlight');
                                //$('#ResumeDetail').highlight(highlight,'highlight');
                            });
                        }
                        else{
                            //highlightSearch(highlight, 'ResumeDetail');
                            
                            //Hiltor was showing substring also
                            myHilitor2.apply(highlight.trim());
                            //searchAndHighlight(highlight, "#contents", 'highlight');
                            //$('#ResumeDetail').highlight(highlight,'highlight');
                        }
                    }
                });
            }
            else
            {
                //ResumeDetail
                var searchCriteria = "<?php echo $this->searchCriteria; ?>";
                //console.log(searchCriteria);
                //searchCriteria = "extremely, sales, solicitor";
                if(searchCriteria != '')
                {
                    var searchCriterias = searchCriteria.split(',');
                    $.each(searchCriterias, function(index, value) { 
                        if(value != '')
                        {
                            var highlight = value.trim();
                            //highlight = decodeURIComponent(highlight);
                            if(highlight.indexOf("%")> 0){
                                var subFilterValues = highlight.split('%');
                                $.each(subFilterValues, function(subIndex, subValue) { 
                                    //highlightSearch(subValue, 'ResumeDetail');
                                    myHilitor2.apply(subValue.trim());
                                    //searchAndHighlight(subValue, "#ResumeDetail", 'highlight');
                                    //$('#ResumeDetail').highlight(subValue,'highlight');
                                });
                            }
                            else{
                                //highlightSearch(highlight, 'ResumeDetail');
                                myHilitor2.apply(highlight);
                                //searchAndHighlight(highlight, "#ResumeDetail", 'highlight');
                                //$('#ResumeDetail').highlight(highlight,'highlight');
                            }
                        }
                    });
                }
            }
        });
        $(function() {
            //$( "#accordion" ).accordion();
        });
    </script>
    <form id="form1" name="form1" method="post" action="">
        <div id="contents">
            <?php if($userid != '1') { ?>
            <table width="100%" cellpadding="1" cellspacing="1"  class="detailsOutside" >	
                <tr>
                    <td class="detail_heading"  >
                        Last viewed :&nbsp;
                        
                        <?php
                        $numItems = count($this->candidate_id_view);
                        $i = 0;
                        foreach ($this->candidate_id_view as &$candidate_id_view) {
                        
                        $sql = mysql_query("select last_name,  first_name from candidate where  candidate_id  = '$candidate_id_view' ");
                        $row = mysql_fetch_assoc($sql);
                        $first_name= $row['first_name'];
                        $last_name= $row['last_name']; 
                        $name = $first_name." ".$last_name;						
                        ?> <a href ="<?php echo(RESUFLOUtility::getIndexName()); ?>?m=candidates&amp;a=show&amp;candidateID=<?php echo $candidate_id_view; ?>&amp;view=1"  > <img src="images/user.png" width="10" height="10"  border="0" /> <?php echo $name; ?> </a>
                        <?php if($i+1 != $numItems) {
                        echo "|";}
                        $i++;
                        }
                        
                        ?> </td>
                </tr>
            </table>
            <?php } ?>
            
            <table class="detailsOutside" width="925">
                
                <tr class="detail_heading" >
                    
                    
                    <td style="width:50%"  >
                        <?php
                        if($this->nextcandid !='' )
                        { if($this->list!='') { ?>
                        <a href="<?php echo $getpage.'candidateID='.$this->nextcandid.'&list='.$this->list;?>&amp;view=1" ><< Previous</a>
                        <?php } else { ?>
                        <a href="<?php echo $getpage.'candidateID='.$this->nextcandid;?>&amp;view=1" ><< Previous</a>
                        <?php } } ?>
                    </td>
                    
                    
                    <td style="" align="right">
                        <?php
                        
                        
                        if($this->prevcandid !='' )
                        { if($this->list!='') { ?>
                        
                        <a href="<?php echo $getpage.'candidateID='.$this->prevcandid.'&list='.$this->list;?>&amp;view=1" >Next>></a>
                        <?php } else { ?>
                        <a href="<?php echo $getpage.'candidateID='.$this->prevcandid;?>&amp;view=1" >Next>></a>
                        <?php } } ?>
                        
                        
                        
                    </td>
                    
                    
                    
                </tr>
            </table>										
            <table>
                <tr>
                    <td width="3%">
                        <img src="images/candidate.gif" width="24" height="24" border="0" alt="Candidates" style="margin-top: 3px;" />&nbsp;
                    </td>
                    <td><h2>Candidates: Candidate Details</h2></td>
                    
                </tr>
            </table>
            <table width="100%" cellpadding="0" cellspacing="0">	
                <tr >
                    <td > <?php if ($this->accessLevel >= ACCESS_LEVEL_EDIT): ?>
                        <a href="#" onclick="showPopWin('<?php echo(RESUFLOUtility::getIndexName()); ?>?m=candidates&amp;a=considerForJobSearch&amp;candidateID=<?php echo($this->candidateID); ?>', 750, 390, null); return false;">
                            <img src="images/consider.gif" width="16" height="16" class="absmiddle" alt="Add to Pipeline" border="0" />&nbsp;Add to Pipeline
                        </a>
                        <?php endif; ?></td>
                    
                    
                    <td>
                        <?php
                        //	echo "SELECT follow_up FROM `candidate` WHERE `candidate_id` ='$candidate_id_for_subscribe'";
                        $sql = mysql_query("SELECT follow_up FROM `candidate` WHERE `candidate_id` = ' $candidate_id_for_subscribe'");
                        $row = mysql_fetch_assoc($sql);
                        $subcribe = $row['follow_up'];
                        if($subcribe == '0')
                        $msg =  "Add to Inactive ";	
                        else
                        {
                        $msg = "Add to Active ";
                        
                        }
                        
                        ?>			
                        <?php if($_REQUEST["list"]) { ?>
                        <a href ="<?php echo(RESUFLOUtility::getIndexName()); ?>?m=candidates&amp;a=show&amp;candidateID=<?php echo($this->candidateID);?>&amp;list=<?php echo($_REQUEST["list"]);?>&amp;follow=1" ><img src="images/subscribe.jpeg" width="16" height="16" border="0" alt="follow" class="absmiddle" />&nbsp;Add to Follow up</a>  
                        <?php } else { ?>
                        <a href ="<?php echo(RESUFLOUtility::getIndexName()); ?>?m=candidates&amp;a=show&amp;candidateID=<?php echo($this->candidateID);?>&amp;follow=1" ><img src="images/subscribe.jpeg" width="16" height="16" border="0" alt="follow" class="absmiddle" />&nbsp;Add to Follow up</a>  
                        <?php } ?>			
                    </td>
                    
                    <td >
                        <?php
                        //	echo "SELECT unsubscribe FROM `candidate` WHERE `candidate_id` ='$candidate_id_for_subscribe'";
                        $sql = mysql_query("SELECT unsubscribe FROM `candidate` WHERE `candidate_id` = ' $candidate_id_for_subscribe'");
                        $row = mysql_fetch_assoc($sql);
                        $subcribe = $row['unsubscribe'];
                        if($subcribe == '0')
                        $msg =  "Add to Inactive ";	
                        else
                        {
                        $msg = "Add to Active ";
                        
                        }
                        
                        ?>
                        
                        <?php if ($_REQUEST["list"]) { ?>					
                        <a href ="<?php echo(RESUFLOUtility::getIndexName()); ?>?m=candidates&amp;a=show&amp;list=<?php echo($_REQUEST["list"]);?>&amp;candidateID=<?php echo($this->candidateID);?>&amp;val=<?php echo $subcribe; ?>"  >
                           <?php } else { ?>
                           <a href ="<?php echo(RESUFLOUtility::getIndexName()); ?>?m=candidates&amp;a=show&amp;candidateID=<?php echo($this->candidateID);?>&amp;val=<?php echo $subcribe; ?>"  >
                                <?php } ?>		
                                <img src="images/subscribe.jpeg" width="16" height="16" border="0" alt="subscribe/unsubscribe" class="absmiddle" />&nbsp;<?php echo $msg; ?></a>  
                    </td>
                    <td > <?php if ($this->accessLevel >= ACCESS_LEVEL_EDIT): ?>
                        <?php if($_REQUEST["list"]) { ?>
                        <a href="#" id="addActivityLink" onclick="showPopWin('<?php echo(RESUFLOUtility::getIndexName()); ?>?m=candidates&amp;a=addActivityChangeStatus&amp;candidateID=<?php echo($this->candidateID); ?>&amp;list=<?php echo($_REQUEST["list"]); ?>&amp;jobOrderID=<?php if(count($this->pipelinesRS) > 0){echo $this->pipelinesRS[0]["jobOrderID"];}else{echo -1;}?>', 650, 480, null); return false;">
                           <?php } else { ?>
                           <a href="#" id="addActivityLink" onclick="showPopWin('<?php echo(RESUFLOUtility::getIndexName()); ?>?m=candidates&amp;a=addActivityChangeStatus&amp;candidateID=<?php echo($this->candidateID); ?>&amp;jobOrderID=<?php if(count($this->pipelinesRS) > 0){echo $this->pipelinesRS[0]["jobOrderID"];}else{echo -1;}?>', 650, 480, null); return false;">			
                                <?php } ?>
                                <img src="images/new_activity_inline.gif" width="16" height="16" class="absmiddle" title="Log an Activity / Change Status" alt="Log an Activity / Change Status" border="0" />&nbsp;Log an Activity
                            </a>
                            <?php endif; ?></td>
                    
                    
                    <td><?php if ($this->accessLevel >= ACCESS_LEVEL_EDIT): ?>
                        <?php if($_REQUEST["list"]) { ?>
                        <a href="#" onclick="showPopWin('<?php echo(RESUFLOUtility::getIndexName()); ?>?m=candidates&amp;a=addActivityChangeStatus&amp;candidateID=<?php echo($this->candidateID); ?>&amp;list=<?php echo $_REQUEST["list"]; ?>&amp;jobOrderID=-1&amp;onlyScheduleEvent=true', 850, 650, null); return false;">
                           <?php } else { ?>
                           <a href="#" onclick="showPopWin('<?php echo(RESUFLOUtility::getIndexName()); ?>?m=candidates&amp;a=addActivityChangeStatus&amp;candidateID=<?php echo($this->candidateID); ?>&amp;jobOrderID=-1&amp;onlyScheduleEvent=true', 650, 480, null); return false;">
                                <?php } ?>						
                                <img src="images/calendar_add.gif" width="16" height="16" border="0" alt="Schedule Event" class="absmiddle" />&nbsp;Schedule Event
                            </a>
                            <?php endif; ?>
                    </td>
                    <td>
                        <a href="<?php echo(RESUFLOUtility::getIndexName()); ?>?m=candidates&amp;a=pdfdisplay&amp;candidateID=<?php echo $candidate_id_for_subscribe; ?>" target="_blank" ><img src="images/printer.jpeg" width="20" height="20" class="absmiddle" title="Click Here To view PDF" alt="Click Here To view PDF" border="0" />&nbsp;View PDF</a></td>
                    <!--<td>
                        <a href="#" onclick="showPopWin('<?php echo(RESUFLOUtility::getIndexName()); ?>?m=candidates&amp;a=addToRecruiter&i=candidates%3AcandidatesListByViewDataGrid', 750, 460, null); return false;">
                             <img src="images/calendar_add.gif" width="16" height="16" border="0" alt="Schedule Event" class="absmiddle" />&nbsp;Add to Recruitre Account
                        </a>
                    </td>-->
                    <!--	<td><a href="javascript:void(0)" onclick="printContent('print_div1')" ><img src="images/printer.jpeg" width="20" height="20" class="absmiddle" title="Click Here To Print Resume" alt="Click Here To Print Resume" border="0" />&nbsp;Print Resume</a></td>-->
                </tr>
            </table>
            <br/>
            
            <br/>
            <DIV >
                
                
                <?php if ($this->data['isAdminHidden'] == 1): ?>
                <p class="warning">This Candidate is hidden.  Only RESUFLO Administrators can view it or search for it.  To make it visible by the site users, click <a href="<?php echo(RESUFLOUtility::getIndexName()); ?>?m=candidates&amp;a=administrativeHideShow&amp;candidateID=<?php echo($this->candidateID); ?>&amp;state=0" style="font-weight:bold;">Here.</a></p>
                <?php endif; ?>
                <?php if (!$this->isPopup): ?>
                <?php if ($this->list): ?>
                <?php if ($this->accessLevel >= ACCESS_LEVEL_EDIT): ?>
                <a id="edit_link" href="<?php echo(RESUFLOUtility::getIndexName()); ?>?m=candidates&amp;a=edit&amp;candidateID=<?php echo($this->candidateID); ?>&amp;list=<?php echo($this->list); ?>">
                    <img src="images/actions/edit.gif" width="16" height="16" class="absmiddle" alt="edit" border="0" />&nbsp;Edit
                </a>
                &nbsp;&nbsp;&nbsp;&nbsp;
                <?php endif; ?>
                <?php if ($this->accessLevel >= ACCESS_LEVEL_DELETE): ?>
                <a id="delete_link" href="<?php echo(RESUFLOUtility::getIndexName()); ?>?m=candidates&amp;a=delete&amp;candidateID=<?php echo($this->candidateID); ?>&amp;list=<?php echo($this->list); ?>" onclick="javascript:return confirm('Delete this candidate?');">
                    <img src="images/actions/delete.gif" width="16" height="16" class="absmiddle" alt="delete" border="0" />&nbsp;Delete
                </a>
                &nbsp;&nbsp;&nbsp;&nbsp;
                <?php endif; ?>
                <?php endif; ?>
                <?php if (!$this->list): ?>
                <?php if ($this->accessLevel >= ACCESS_LEVEL_EDIT): ?>
                <a id="edit_link" href="<?php echo(RESUFLOUtility::getIndexName()); ?>?m=candidates&amp;a=edit&amp;candidateID=<?php echo($this->candidateID); ?>">
                    <img src="images/actions/edit.gif" width="16" height="16" class="absmiddle" alt="edit" border="0" />&nbsp;Edit
                </a>
                &nbsp;&nbsp;&nbsp;&nbsp;
                <?php endif; ?>
                <?php if ($this->accessLevel >= ACCESS_LEVEL_DELETE): ?>
                <a id="delete_link" href="<?php echo(RESUFLOUtility::getIndexName()); ?>?m=candidates&amp;a=delete&amp;candidateID=<?php echo($this->candidateID); ?>" onclick="javascript:return confirm('Delete this candidate?');">
                    <img src="images/actions/delete.gif" width="16" height="16" class="absmiddle" alt="delete" border="0" />&nbsp;Delete
                </a>
                &nbsp;&nbsp;&nbsp;&nbsp;
                <?php endif; ?>
                <?php endif; ?>
                
                <?php if ($this->privledgedUser): ?>
                <a id="history_link" href="<?php echo(RESUFLOUtility::getIndexName()); ?>?m=settings&amp;a=viewItemHistory&amp;dataItemType=100&amp;dataItemID=<?php echo($this->candidateID); ?>">
                    <img src="images/icon_clock.gif" width="16" height="16" class="absmiddle"  border="0" />&nbsp;View History
                </a>
                &nbsp;&nbsp;&nbsp;&nbsp;
                <?php endif; ?>
                <?php if ($this->isTextMessageEnable):?>
                <a target="_blank" href="<?php echo(RESUFLOUtility::getIndexName()); ?>?m=candidates&amp;a=sms&amp;candidateID=<?php echo($this->candidateID); ?>">
                    <img src="images/sms.png" width="16" height="16" class="absmiddle"  border="0" />&nbsp;View Text Messages
                </a>
                <?php endif; ?>
                <?php if ($this->accessLevel >= ACCESS_LEVEL_MULTI_SA): ?>
                <?php if ($this->data['isAdminHidden'] == 1): ?>
                <a href="<?php echo(RESUFLOUtility::getIndexName()); ?>?m=candidates&amp;a=administrativeHideShow&amp;candidateID=<?php echo($this->candidateID); ?>&amp;state=0">
                    <img src="images/resume_preview_inline.gif" width="16" height="16" class="absmiddle" alt="delete" border="0" />&nbsp;Administrative Show
                </a>
                <?php else: ?>
                <a href="<?php echo(RESUFLOUtility::getIndexName()); ?>?m=candidates&amp;a=administrativeHideShow&amp;candidateID=<?php echo($this->candidateID); ?>&amp;state=1">
                    <img src="images/resume_preview_inline.gif" width="16" height="16" class="absmiddle" alt="delete" border="0" />&nbsp;Administrative Hide
                </a>
                <?php endif; ?>
                &nbsp;&nbsp;&nbsp;&nbsp;
                <?php endif; ?>
                <?php endif; ?>		
                <br/>
                <br/>
                
                <div class="demo11" >
                    
                    <div id="accordion11">
                        <table width="925" border="0"  cellspacing="0" cellpadding="0" style="border:1px solid #cccccc;">
                            <tr>
                                <td colspan="4" class="top_heading"><!--<a href="#Candidate Contact Details" >-->Candidate Contact Details </td>
                            </tr>
                        </table>
                        <div>
                            <table width="925" border="0" cellspacing="0" cellpadding="0" style="border:1px solid #cccccc;">
                                
                                <tr>
                                    <td width="195" class="detail_heading">Name:</td>
                                    <td width="344" class="deatils">
                                        <?php
                                        //                      die( "SELECT unsubscribe FROM `candidate` WHERE `candidate_id` ='$candidate_id_for_subscribe'" 0;
                                        $sql = mysql_query("SELECT unsubscribe FROM `candidate` WHERE `candidate_id` = ' $candidate_id_for_subscribe'");
                                        $row = mysql_fetch_assoc($sql);
                                        $subcribe = $row['unsubscribe'];
                                        if($subcribe == '0')
                                        $status =  "Active";  
                                        else
                                        {
                                        $status = "Inactive";
                                        
                                        }
                                        
                                        ?>      
                                        
                                        
                                        
                                        <span style="font-weight: bold;" class="<?php echo($this->data['titleClass']); ?>">
                                            &nbsp;<?php $this->_($this->data['firstName']); ?>
                                            <?php $this->_($this->data['middleName']); ?>
                                            <?php $this->_($this->data['lastName']); ?>                 
                                            &nbsp; &nbsp;<span style="color:orange;"><?php echo "(".$status.")"; ?></span> &nbsp;
                                            
                                            <?php TemplateUtility::printSingleQuickActionMenu(DATA_ITEM_CANDIDATE, $this->data['candidateID']); ?>
                                            
                                        </span>			</td>
                                    <td width="173" class="detail_heading">Cell Phone :</td>
                                    <td width="218" class="deatils"> &nbsp;<?php $this->_($this->data['phoneCell']); ?></td>
                                </tr>
                                <tr>
                                    <td width="190" class="detail_heading">E-Mail :</td>
                                    <td width="344" class="deatils">
                                        <a href="<?php echo(RESUFLOUtility::getIndexName()); ?>?m=candidates&amp;a=emailcandidate&amp;<?php echo $this->data['email1']; ?> " >
                                            &nbsp; <?php $this->_(str_replace(",",",\n",$this->data['email1'])); ?>
                                        </a>			 </td>
                                    
                                    <td class="detail_heading">Home Phone :</td>
                                    <td class="deatils"> &nbsp;<?php $this->_($this->data['phoneHome']); ?></td>
                                </tr>
                                <tr>
                                    <td class="detail_heading">2nd E-Mail :</td>
                                    <td class="deatils">
                                        <a href="<?php echo(RESUFLOUtility::getIndexName()); ?>?m=candidates&amp;a=emailcandidate&amp;<?php echo $this->data['email2']; ?> ">
                                            &nbsp; <?php $this->_($this->data['email2']); ?>
                                        </a>
                                    </td>
                                    <td class="detail_heading">Work Phone :</td>
                                    <td class="deatils"> &nbsp;<?php $this->_($this->data['phoneWork']); ?></td>
                                </tr>
                                <tr>
                                    <td class="detail_heading">Address :</td>
                                    <td class="deatils"> &nbsp;<?php echo(nl2br(htmlspecialchars($this->data['address']))); ?>  &nbsp;&nbsp;<?php $this->_($this->data['cityAndState']); ?> 
                                        <?php $this->_($this->data['zip']); ?></td>
                                    <td class="detail_heading">Best Time To Call :</td>
                                    <td class="deatils">&nbsp; <?php $this->_($this->data['bestTimeToCall']); ?></td>
                                </tr>
                                <tr>
                                    <?php if($this->data['isSourceVisible'] == '1'){ ?>
                                    <td class="detail_heading">Source :</td>
                                    <td class="deatils">&nbsp; <?php $this->_($this->data['source']); ?></td>
                                    <?php } ?>
                                    <td class="detail_heading">Scheduled Time :</td>
                                    <td class="deatils">&nbsp; <?php $this->_($this->data['dateScheduled']); ?></td>
                                </tr>
                                <!--   <tr>
                                     <td class="detail_heading">Tags :</td>
                                     <td class="deatils">&nbsp; <?php if (!empty($this->data['webSite'])): ?>
                                                               <?php $this->_($this->data['webSite']); ?>
                                                             <?php endif; ?></td>
                                                                                                 <td class="detail_heading"> Source:</td>
                                                                                                 <td class="deatils">&nbsp;<?php $this->_($this->data['source']); ?></td>
                                                                   </tr>
                                                                                                 
                                   <tr>
                                             <td class="detail_heading">Category :</td>
                                     <td class="deatils"> &nbsp;<?php echo($this->extraFieldRS[0]['display']); ?></td>
                                  
                                     <td class="detail_heading">Date Of Birth :</td>
                                     <td class="deatils"> &nbsp;<?php echo($this->extraFieldRS[1]['display']); ?></td>
                                     </tr>
                                   <tr>
                                             <td class="detail_heading">Gender  :</td>
                                     <td class="deatils"> &nbsp;<?php echo($this->extraFieldRS[2]['display']); ?></td>
                                   
                                     <td class="detail_heading">Father Name :</td>
                                     <td class="deatils"> &nbsp;<?php echo($this->extraFieldRS[3]['display']); ?></td>
                                   </tr>
                                   <tr>
                                             <td class="detail_heading">Mother Name :</td>
                                     <td class="deatils"> &nbsp;<?php echo($this->extraFieldRS[4]['display']); ?></td>
                                  
                                     <td class="detail_heading">Marital Status :</td>
                                     <td class="deatils"> &nbsp;<?php echo($this->extraFieldRS[5]['display']); ?></td>
                                    </tr>
                                   <tr>
                                             <td class="detail_heading">Passport No :</td>
                                     <td class="deatils"> &nbsp;<?php echo($this->extraFieldRS[6]['display']); ?></td>
                                  
                                     <td class="detail_heading">Nationality  :</td>
                                     <td class="deatils"> &nbsp;<?php echo($this->extraFieldRS[7]['display']); ?></td>
                                  
                                             
                                   </tr>-->
                            </table>
                        </div>
                        
                        <table width="925" border="0"  cellspacing="0" cellpadding="0" style="border:1px solid #cccccc;">
                            <tr>
                                <td colspan="4" class="top_heading"><!--<a href="#Candidate Contact Details" >-->License Information </td>
                            </tr>
                        </table>
                        <div>
                            <table width="925" border="0" cellspacing="0" cellpadding="0" style="border:1px solid #cccccc;">
                                
                                <tr>
                                    <td width="195" class="detail_heading">Life and Health License:</td>
                                    <td width="344" class="deatils">
                                        <?php if($this->data['lifeLic']=='none'): ?>
                                        <?php $this->data['lifeLic'] = ''; ?>
                                        <?php endif; ?>				
                                        &nbsp;<?php $this->_($this->data['lifeLic']); ?>
                                        
                                        </span>			</td>
                                    <td width="173" class="detail_heading">Property Casualty License :</td>
                                    <td width="218" class="deatils">
                                        <?php if($this->data['propLic']=='none'): ?>
                                        <?php $this->data['propLic'] = ''; ?>
                                        <?php endif; ?>
                                        &nbsp;<?php $this->_($this->data['propLic']); ?></td>
                                </tr>
                                <tr>
                                    <td width="190" class="detail_heading">Series 6 :</td>
                                    <td width="344" class="deatils">
                                        <?php if($this->data['ser6']=='none'): ?>
                                        <?php $this->data['ser6'] = ''; ?>
                                        <?php endif; ?>
                                        &nbsp; <?php $this->_($this->data['ser6']); ?>
                                    </td>
                                    
                                    <td class="detail_heading">Series 63 :</td>
                                    <td class="deatils">
                                        <?php if($this->data['ser63']=='none'): ?>
                                        <?php $this->data['ser63'] = ''; ?>
                                        <?php endif; ?>
                                        &nbsp;<?php $this->_($this->data['ser63']); ?></td>
                                </tr>
                                <tr>
                                    <td class="detail_heading">Series 7 :</td>
                                    <td class="deatils">
                                        <?php if($this->data['ser7']=='none'): ?>
                                        <?php $this->data['ser7'] = ''; ?>
                                        <?php endif; ?>
                                        &nbsp; <?php $this->_($this->data['ser7']); ?>
                                    </td>
                                    <td class="detail_heading"></td>
                                    <td class="deatils">&nbsp; </td>
                                </tr>
                                
                            </table>
                        </div>
                        <table width="925" border="0"  cellspacing="0" cellpadding="0" style="border:1px solid #cccccc;">
                            <tr>
                                <td colspan="4" class="top_heading"><!--<a href="#Candidate Contact Details" >-->Drip Campaign Assigned </td>
                            </tr>
                        </table>
                        <div>
                            <table width="925" border="0" cellspacing="0" cellpadding="0" style="border:1px solid #cccccc;">
                                
                                <tr>
                                    <td width="195" class="detail_heading">Drip campaign:</td>
                                    <td width="344" class="deatils">
                                        &nbsp;<?php echo $this->data['camp_name']; ?>
                                    </td>
                                    <td width="173" class="detail_heading"></td>
                                    <td width="218" class="deatils">&nbsp;</td>
                                </tr>
                            </table>
                        </div>
                        
                        <table width="925" border="0" cellspacing="0" cellpadding="0" style="margin-top:1px;" >
                            <tr>
                                <td colspan="4" class="top_heading"><!--<a href="#Key Skills " >-->Questionnaire Message </td>
                            </tr>
                        </table>
                        <div>
                            <table width="925" border="0" cellspacing="0" cellpadding="0" style="border:1px solid #cccccc;">
                                <tr>
                                    <td width="180" class="detail_heading">Questionnaire Message:</td>
                                    <td width="757" colspan="3" class="deatils" valign="top"><?php
                                        
                                        
                                        
                                        
                                        $sql ="SELECT  candidate.candidate_id ,candidate.email1,candidate.entered_by,dripmarket_questionaire.message,candidate.first_name,dripmarket_quesans.cid, dripmarket_questable.questionaireid  from candidate inner join dripmarket_quesans on dripmarket_quesans.cid=candidate.candidate_id inner join dripmarket_questable on dripmarket_quesans.quesid=dripmarket_questable.id  inner join dripmarket_questionaire on dripmarket_questionaire.id=dripmarket_questable.questionaireid inner join user on user.user_id=candidate.entered_by  where user.user_id=$userid and candidate.candidate_id=$candidate_id_for_subscribe and dripmarket_questionaire.userid =$userid  group by candidate.candidate_id";
                                        //echo $sql;
                                        //echo "<br/>";
                                        
                                        // $this->_db->makeQueryInteger($activityID),
                                        $resource= mysql_query($sql);
                                        $campaignarray=array();
                                        $i=0;
                                        
                                        if((mysql_num_rows($resource))>0)
                                        {
                                        while($row = mysql_fetch_array($resource))
                                        {
                                        $campaignarray[$i]=$row;
                                        $firstname = $row['first_name'];
                                        $email = $row['email1'];
                                        $message = $row['message'];
                                        $i++;
                                        }
                                        }
                                        if($campaignarray[0]['questionaireid'] != '')
                                        {
                                        $sql ="SELECT  dripmarket_questionaire.message ,dripmarket_questable.quesname,candidate.candidate_id,dripmarket_questable.questionaireid ,dripmarket_questable.questionaireid,dripmarket_quesans.answer   from  dripmarket_quesans inner join dripmarket_questable on dripmarket_quesans.quesid=dripmarket_questable.id  inner join dripmarket_questionaire on dripmarket_questionaire.id=dripmarket_questable.questionaireid inner join candidate on dripmarket_quesans.cid=candidate.candidate_id where dripmarket_questionaire.id=".$campaignarray[0]['questionaireid']." and dripmarket_quesans.cid=".$candidate_id_for_subscribe ." and candidate.entered_by = $userid order by dripmarket_questable.id asc" ;
                                        //echo $sql;
                                        //echo "<br/>";
                                        // $this->_db->makeQueryInteger($activityID),
                                        $resource= mysql_query($sql);
                                        $campaignarrayques=array();
                                        $i=0;
                                        
                                        if((mysql_num_rows($resource))>0)
                                        {
                                        while($row = mysql_fetch_array($resource))
                                        {
                                        $campaignarrayques[$i]=$row;
                                        $i++;
                                        }
                                        }
                                        ?>
                                        <table width="100%" border="0" cellspacing="2" cellpadding="2" >
                                            <tr>
                                                <!-- <td width="29%"><b>Candidate Name </b></td>
                                                 <td width="71%"><table><tr><td style="width:40%"><?php echo $firstname; ?></td><td style="width:60%">
                                                 </td></tr></table>
                                         
                                                          </td>
                                               </tr>
                                               <tr>
                                                 <td><b>Email</b> </td>
                                                 <td>
                                                 <?php echo $email; ?>
                                                     
                                                 </td>
                                               </tr>
                                               <tr>
                                                 <td><b>Questionnaire Message </b></td>-->
                                                <td><?php echo substr($message,0,100); ?>
                                                </td>
                                            </tr>
                                            <tr><td></td></tr> <tr><td></td></tr>
                                            <tr><td colspan="2"><table width="100%" cellpadding="0" cellspacing="4" >
                                                        <?php foreach($campaignarrayques as $row){ ?>
                                                        <tr>
                                                            <td width="50" style="width:50px;color:#282626; font-weight:bold; font-size:12px; font-family:Arial, Helvetica, sans-serif;">Ques:</td>
                                                            <td width="680" style="width:600px; font-size:12px; font-family:Arial, Helvetica, sans-serif;"><label><b>
                                                                        <?php echo $row['quesname']; ?></b>
                                                                </label></td>
                                                        </tr>
                                                        <tr>
                                                            <td style="width:50px;color:#282626; font-size:12px; font-family:Arial, Helvetica, sans-serif;">Ans:</td>
                                                            <td width="680" style="width:600px; font-size:12px; font-family:Arial, Helvetica, sans-serif;"><label>
                                                                    <?php echo $row['answer']; ?>
                                                                </label></td>
                                                        </tr> 
                                                        <?php } ?></table></td></tr> </table>
                                        <?php } ?>
                                        
                                    </td>
                                </tr>
                                
                            </table>
                        </div>
                        <!--	<table width="925" border="0" cellspacing="0" cellpadding="0" style="margin-top:10px;">
                                  <tr>
                                    <td colspan="4" class="top_heading"><a href="#Key Skills " >Key Skills  </a></td>
                                  </tr>
                                </table>
                                <div>
                                        <table width="925" border="0" cellspacing="0" cellpadding="0">
                                            <tr>
                                    <td width="130" class="detail_heading">Key Skills :</td>
                                    <td colspan="3" class="deatils"> &nbsp;<?php $this->_($this->data['keySkills']); ?></td>
                                    </tr>
                                  
                                </table>
                                </div>
                        <table width="925" border="0" cellspacing="0" cellpadding="0" style="margin-top:10px;">
                                  <tr>
                                    <td colspan="4" class="top_heading"><a href="#section3" >Achievements  </a></td>
                                  </tr>
                                </table>
                                <div>
                                        <table width="925" border="0" cellspacing="0" cellpadding="0">
                                  
                                  <tr>
                                    <td width="130" class="detail_heading">Achievements:</td>
                                    <td colspan="3" class="deatils"> &nbsp;<?php echo($this->extraFieldRS[15]['display']); ?></td>
                                    </tr>
                                  
                                </table>
                                        
                                </div>
                                <table width="925" border="0" cellspacing="0" cellpadding="0" style="margin-top:10px;">
                                  <tr>
                                    <td colspan="4" class="top_heading"><a href="#section3" >Qualification  </a></td>
                                  </tr>
                                </table>
                                <div>
                                        <table width="925" border="0" cellspacing="0" cellpadding="0">
                                  
                                  <tr>
                                    <td class="detail_heading">Qualification:</td>
                                    <td class="deatils">&nbsp;<?php echo($this->extraFieldRS[14]['display']); ?></td>
                                    </tr>
                                  
                                </table>
                                        
                                </div> 
                        <table width="925" border="0" cellspacing="0" cellpadding="0" style="margin-top:10px;">
                                  <tr>
                                    <td colspan="4" class="top_heading"><a href="#section4" >Objectives </a></td>
                                  </tr>
                                </table>
                                <div>
                                        <table width="925" border="0" cellspacing="0" cellpadding="0">
                                
                                  <tr>
                                    <td width="130" class="detail_heading">Objectives:</td>
                                    <td colspan="3" class="deatils"> &nbsp;<?php echo($this->extraFieldRS[16]['display']); ?></td>
                                    </tr>
                                  
                                </table>
                                </div>
                        
                        <table width="925" border="0" cellspacing="0" cellpadding="0" style="margin-top:10px;">
                                  <tr>
                                    <td colspan="4" class="top_heading"><a href="#section5" >Job Profile </a></td>
                                  </tr>
                                </table>
                                <div>
                                        <table width="925" border="0" cellspacing="0" cellpadding="0">
                                  
                                  <tr>
                                   <td width="180" class="detail_heading">Job Profile :</td>
                                    <td colspan="3" class="deatils">&nbsp;<?php echo($this->extraFieldRS[19]['display']); ?></td>
                                  </tr>
                                </table>
                                </div>
                        <table width="925" border="0" cellspacing="0" cellpadding="0" style="margin-top:10px;">
                                  <tr>
                                    <td colspan="4" class="top_heading"><a href="#section6" >Experience  </a></td>
                                  </tr>
                                </table>
                                <div>
                                        <table width="925" border="0" cellspacing="0" cellpadding="0">
                                 
                                  <tr>
                                    <td width="180" class="detail_heading">Experience : </td>
                                    <td colspan="3" class="deatils"> &nbsp;<?php echo($this->extraFieldRS[17]['display']); ?></td>
                                  </tr> 
                                </table>
                                </div>-->
                        <table width="925" border="0" cellspacing="0" cellpadding="0" style="margin-top:1px;">
                            <tr>
                                <td colspan="4" class="top_heading"><!--<a href="#section7" >-->Detail Resume  </td>
                            </tr>
                        </table>
                        <div>
                            <table width="925" border="0" cellspacing="0" cellpadding="0" style="border:1px solid #cccccc;">
                                
                                <tr>
                                    <td width="180" class="detail_heading">Detail Resume :</td>
                                    <td colspan="3" id="ResumeDetail" class="deatils"> &nbsp;<?php echo html_entity_decode($this->extraFieldRS[20]['display']); ?></td>
                                </tr>
                            </table>
                        </div>
                        <table width="925" border="0" cellspacing="0" cellpadding="0" style="margin-top:1px;" >
                            <tr>
                                <td colspan="4" class="top_heading"><!--<a href="#section8" >-->Others <!--</a>--></td>
                            </tr>
                        </table>
                        <div>
                            <table width="925" border="0" cellspacing="0" cellpadding="0" style="border:1px solid #cccccc;">
                                
                                <tr>
                                    <td width="198" class="detail_heading">Gap Period :</td>
                                    <td width="342" class="deatils"> &nbsp;<?php echo($this->extraFieldRS[9]['display']); ?></td>
                                    <td width="170" class="detail_heading">Worked Period :</td>
                                    <td width="215" class="deatils"> &nbsp;<?php echo($this->extraFieldRS[8]['display']); ?> </td>
                                    
                                </tr>
                                <tr>
                                    <td class="detail_heading">Date Available :</td>
                                    <td class="deatils">&nbsp;<?php $this->_($this->data['dateAvailable']); ?></td>
                                    <td class="detail_heading">Current Employer :</td>
                                    <td class="deatils">&nbsp;<?php $this->_($this->data['currentEmployer']); ?></td>
                                </tr>
                                <tr>
                                    <td class="detail_heading">Can Relocate :</td>
                                    <td class="deatils"> &nbsp;<?php $this->_($this->data['canRelocate']); ?></td>
                                    <td class="detail_heading">Current Pay :</td>
                                    <td class="deatils"> &nbsp;<?php $this->_($this->data['currentPay']); ?></td>
                                </tr>
                                <tr>
                                    <td class="detail_heading">Desired Pay:</td>
                                    <td class="deatils"> &nbsp;<?php $this->_($this->data['desiredPay']); ?></td>
                                    <td class="detail_heading">Pipeline:</td>
                                    <td class="deatils"> &nbsp;<?php $this->_($this->data['pipeline']); ?></td>
                                </tr>
                                <tr>
                                    <td class="detail_heading">Created:</td>
                                    <td class="deatils"> &nbsp;<?php $this->_($this->data['dateCreated']); ?> (<?php $this->_($this->data['enteredByFullName']); ?>)</td>
                                    <td class="detail_heading">Submitted:</td>
                                    <td class="deatils"> &nbsp;<?php $this->_($this->data['submitted']); ?></td>
                                </tr>
                                <tr>
                                    <td class="detail_heading">Owner:</td>
                                    <td class="deatils"> &nbsp;<?php $this->_($this->data['ownerFullName']); ?></td>
                                    <td class="detail_heading">Fax No :</td>
                                    <td class="deatils">&nbsp;<?php echo($this->extraFieldRS[10]['display']); ?></td>
                                </tr>
                                <tr>
                                    <td class="detail_heading">Language Known:</td>
                                    <td class="deatils">&nbsp;<?php echo($this->extraFieldRS[12]['display']); ?></td>
                                    <td class="detail_heading">License No:</td>
                                    <td class="deatils">&nbsp;<?php echo($this->extraFieldRS[15]['display']); ?></td>
                                </tr>
                                <tr>
                                    <td class="detail_heading">References :</td>
                                    <td class="deatils">&nbsp;<?php echo($this->extraFieldRS[18]['display']); ?></td>
                                    <td class="detail_heading">Hobbies:</td>
                                    <td class="deatils">&nbsp;<?php echo($this->extraFieldRS[13]['display']); ?></td>
                                </tr>
                                
                                
                            </table>
                        </div>
                        
                    </div>
                </div>
                
                
                
                <!-- <table class="detailsOutside" width="925">
                               <tr style="vertical-align:top;">
                                   <?php $profileImage = false; ?>
                                   <?php foreach ($this->attachmentsRS as $rowNumber => $attachmentsData): ?>
                                        <?php if ($attachmentsData['isProfileImage'] == '1'): ?>
                                            <?php $profileImage = true; ?>
                                        <?php endif; ?>
                                   <?php endforeach; ?>
                                   <?php if ($profileImage): ?>
                                       <td width="390" height="100%">
                                   <?php else: ?>
                                       </td><td width="50%" height="100%">
                                   <?php endif; ?>
                                       <table class="detailsInside" height="100%" style="border:#333333 0px solid;">
                                           <tr>
                                               <td class="vertical">Name:</td>
                                              <td class="data" >
                                                   <span style="font-weight: bold;" class="<?php echo($this->data['titleClass']); ?>">
                                                       <?php $this->_($this->data['firstName']); ?>
                                                       <?php $this->_($this->data['middleName']); ?>
                                                       <?php $this->_($this->data['lastName']); ?>
                                                       <?php if ($this->data['isActive'] != 1): ?>
                                                           &nbsp;<span style="color:orange;">(INACTIVE)</span>
                                                       <?php endif; ?>
                                                       <?php TemplateUtility::printSingleQuickActionMenu(DATA_ITEM_CANDIDATE, $this->data['candidateID']); ?>
                                                   </span>
                                             </td>
                                           </tr>
               
                                           <tr>
                                               <td class="vertical">E-Mail:</td>
                                               <td class="data">
                                                               <a href="<?php echo(RESUFLOUtility::getIndexName()); ?>?m=candidates&amp;a=emailcandidate&amp;<?php echo $this->data['email1']; ?> " >
                                                       <?php $this->_(str_replace(",",",\n",$this->data['email1'])); ?>
                                                   </a>
                                               </td>
                                           </tr>
                                           <tr>
                                               <td class="vertical">2nd E-Mail:</td>
                                               <td class="data">
                                                   <a href="<?php echo(RESUFLOUtility::getIndexName()); ?>?m=candidates&amp;a=emailcandidate&amp;<?php echo $this->data['email2']; ?> ">
                                                       <?php $this->_($this->data['email2']); ?>
                                                   </a>
                                               </td>
                                           </tr>
               
                                           <tr>
                                               <td class="vertical">Home Phone:</td>
                                               <td class="data"><?php $this->_($this->data['phoneHome']); ?></td>
                                           </tr>
               
                                           <tr>
                                               <td class="vertical">Cell Phone:</td>
                                               <td class="data"><?php $this->_($this->data['phoneCell']); ?></td>
                                           </tr>
               
                                           <tr>
                                               <td class="vertical">Work Phone:</td>
                                               <td class="data"><?php $this->_($this->data['phoneWork']); ?></td>
                                           </tr>
               
                                           <tr>
                                               <td class="vertical">Best Time To Call:</td>
                                               <td class="data"><?php $this->_($this->data['bestTimeToCall']); ?></td>
                                           </tr>
               
                                           <tr>
                                               <td class="vertical">Address:</td>
                                               <td class="data"><?php echo(nl2br(htmlspecialchars($this->data['address']))); ?></td>
                                           </tr>
               
                                           <tr>
                                               <td class="vertical">&nbsp;</td>
                                               <td class="data">
                                                   <?php $this->_($this->data['cityAndState']); ?>
                                                   <?php $this->_($this->data['zip']); ?>
                                               </td>
                                           </tr>
               
                                           <tr>
                                               <td class="vertical">Tags:</td>
                                               <td class="data">
                                                   <?php if (!empty($this->data['webSite'])): ?>
                                                    <?php $this->_($this->data['webSite']); ?>
                                                   <?php endif; ?>
                                               </td>
                                           </tr>
               
                                           <tr>
                                             <td class="vertical"><?php $this->_($this->data['source']); ?></td>
                                           </tr>
               
                                           <?php for ($i = 0; $i < intval(count($this->extraFieldRS)/2); $i++): ?>
                                               <tr>
                                                   <td class="vertical"><?php $this->_($this->extraFieldRS[$i]['fieldName']);?> :</td>
                                                                                       <td class="data">  <?php echo($this->extraFieldRS[$i]['display']); ?></td>
                                               
                                               </tr>
                                           <?php endfor; ?>
               
                                           <tr>
                                              
                                           </tr>
                                       </table>
                                   </td>
               
                                   <?php if ($profileImage): ?>
                                       <td width="390" height="100%" valign="top">
                                   <?php else: ?>
                                       </td><td width="50%" height="100%" valign="top">
                                   <?php endif; ?>
                                       <table class="detailsInside" height="100%">
                                           <tr>
                                               <td class="vertical">Date Available:</td>
                                               <td class="data"><?php $this->_($this->data['dateAvailable']); ?></td>
                                           </tr>
               
                                           <tr>
                                               <td class="vertical">Current Employer:</td>
                                               <td class="data"><?php $this->_($this->data['currentEmployer']); ?></td>
                                           </tr>
               
                                           <tr>
                                               <td class="vertical">Key Skills:</td>
                                               <td class="data"><?php $this->_($this->data['keySkills']); ?></td>
                                           </tr>
               
                                           <tr>
                                               <td class="vertical">Can Relocate:</td>
                                               <td class="data"><?php $this->_($this->data['canRelocate']); ?></td>
                                           </tr>
               
                                           <tr>
                                               <td class="vertical">Current Pay:</td>
                                               <td class="data"><?php $this->_($this->data['currentPay']); ?></td>
                                           </tr>
               
                                           <tr>
                                               <td class="vertical">Desired Pay:</td>
                                               <td class="data"><?php $this->_($this->data['desiredPay']); ?></td>
                                           </tr>
               
                                           <tr>
                                               <td class="vertical">Pipeline:</td>
                                               <td class="data"><?php $this->_($this->data['pipeline']); ?></td>
                                           </tr>
               
                                           <tr>
                                               <td class="vertical">Submitted:</td>
                                               <td class="data"><?php $this->_($this->data['submitted']); ?></td>
                                           </tr>
               
                                           <tr>
                                               <td class="vertical">Created:</td>
                                               <td class="data"><?php $this->_($this->data['dateCreated']); ?> (<?php $this->_($this->data['enteredByFullName']); ?>)</td>
                                           </tr>
               
                                           <tr>
                                               <td class="vertical">Owner:</td>
                                               <td class="data"><?php $this->_($this->data['ownerFullName']); ?></td>
                                           </tr>
               
                                           <?php for ($i = (intval(count($this->extraFieldRS))/2); $i < (count($this->extraFieldRS)); $i++): ?>
                                               <tr>
                                                   <td class="vertical"><?php $this->_($this->extraFieldRS[$i]['fieldName']); ?>
                                                   :</td>
                                                   <td class="data"><?php echo($this->extraFieldRS[$i]['display']); ?></td>
                                               </tr>
                                           <?php endfor; ?>
                                       </table>
                                   </td>
                                   <?php foreach ($this->attachmentsRS as $rowNumber => $attachmentsData): ?>
                                        <?php if ($attachmentsData['isProfileImage'] == '1'): ?>
                                           <td width="135" height="100%"  valign="top">
                                               <table class="detailsInside">
                                                   <tr>
                                                       <td style="text-align:center;" class="vertical">
                                                           <?php if (!$this->isPopup): ?>
                                                               <?php if ($this->accessLevel >= ACCESS_LEVEL_DELETE): ?>
                                                                   <a href="<?php echo(RESUFLOUtility::getIndexName()); ?>?m=candidates&amp;a=deleteAttachment&amp;candidateID=<?php echo($this->candidateID); ?>&amp;attachmentID=<?php $this->_($attachmentsData['attachmentID']) ?>" onclick="javascript:return confirm('Delete this attachment?');">
                                                                       <img src="images/actions/delete.gif" alt="" width="16" height="16" border="0" title="Delete" />
                                                                   </a>
                                                               <?php endif; ?>
                                                           <?php else: ?>
                                                           &nbsp;&nbsp;&nbsp;&nbsp;
                                                           <?php endif; ?>&nbsp;&nbsp;
                                                           Picture:&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;
                                                       </td>
                                                   </tr>
                                                   <tr>
                                                       <td class="data">
                                                           <a href="attachments/<?php $this->_($attachmentsData['directoryName']) ?>/<?php $this->_($attachmentsData['storedFilename']) ?>">
                                                               <img src="attachments/<?php $this->_($attachmentsData['directoryName']) ?>/<?php $this->_($attachmentsData['storedFilename']) ?>" border="0" alt="" width="125" />
                                                           </a>
                                                       </td>
                                                   </tr>
                                               </table>
                                           </td>
                                        <?php endif; ?>
                                   <?php endforeach; ?>
                               </tr>
                          </table>-->
                
                
            </DIV>
            
            <div id="print_div1" style="display:none;">
                
                <table width="1000" border="0" align="center" cellpadding="0" cellspacing="0" style="font-family:Arial, Helvetica, sans-serif; font-size:14px; color:#000000;">
                    <?php for ($i = ((intval(count($this->extraFieldRS)))-1); $i < (count($this->extraFieldRS)); $i++): ?>
                    <tr>
                        <td class="vertical">
                            <?php echo($this->extraFieldRS[$i]['display']); ?></td>
                    </tr>
                    <?php endfor; ?>
                </table>
            </div><br/>
            <table class="detailsOutside" width="925">
                
                <tr class="detail_heading">
                    <td style="width:50%" >
                        <?php
                        
                        
                        
                        if($this->prevcandid !='' )
                        { ?>
                        <a href="<?php echo $getpage.'candidateID='.$this->nextcandid;?>&amp;view=1" ><< Previous</a>
                        <?php } ?>
                        
                        
                        
                    </td>
                    <td style="" align="right">
                        <?php
                        
                        if($this->nextcandid !='' )
                        { ?>
                        <a href="<?php echo $getpage.'candidateID='.$this->prevcandid;?>&amp;view=1" >Next>></a>
                        <?php } ?>
                    </td>
                </tr>
            </table>		
            
            <?php if($this->EEOSettingsRS['enabled'] == 1): ?>
            <table class="detailsOutside" width="925">
                <tr>
                    <td>
                        <table class="detailsInside">
                            <?php for ($i = 0; $i < intval(count($this->EEOValues)/2); $i++): ?>
                            <tr>
                                <td class="vertical"><?php $this->_($this->EEOValues[$i]['fieldName']); ?>:</td>
                                <?php if($this->EEOSettingsRS['canSeeEEOInfo']): ?>
                                <td class="data"><?php $this->_($this->EEOValues[$i]['fieldValue']); ?></td>
                                <?php else: ?>
                                <td class="data"><i><a href="javascript:void(0);" title="Ask an administrator to see the EEO info, or have permission granted to see it.">(Hidden)</a></i></td>
                                <?php endif; ?>
                            </tr>
                            <?php endfor; ?>
                        </table>
                    </td>
                    <?php if ($profileImage): ?>
                    <td width="390" height="100%" valign="top">
                        <?php else: ?>
                    </td><td width="50%" height="100%" valign="top">
                        <?php endif; ?>
                        <table class="detailsInside">
                            <?php for ($i = (intval(count($this->EEOValues))/2); $i < intval(count($this->EEOValues)); $i++): ?>
                            <tr>
                                <td class="vertical"><?php $this->_($this->EEOValues[$i]['fieldName']); ?>:</td>
                                <?php if($this->EEOSettingsRS['canSeeEEOInfo']): ?>
                                <td class="data"><?php $this->_($this->EEOValues[$i]['fieldValue']); ?></td>
                                <?php else: ?>
                                <td class="data"><i><a href="javascript:void(0);" title="Ask an administrator to see the EEO info, or have permission  granted to see it.">(Hidden)</a></i></td>
                                <?php endif; ?>
                            </tr>
                            <?php endfor; ?>
                        </table>
                    </td>
                </tr>
            </table>
            <?php endif; ?>
            
            <table class="detailsOutside" width="925">
                <tr>
                    <td>
                        <table class="detailsInside">
                            <tr>
                                <td width="192" valign="top" class="vertical">Misc. Notes:</td>
                                <?php if ($this->isShortNotes): ?>
                                <td width="510" class="data" id="shortNotes" style="display:block;">
                                    <?php echo($this->data['shortNotes']); ?><span class="moreText">...</span>&nbsp;
                                    <p><a href="#" class="moreText" onclick="toggleNotes(); return false;">[More]</a></p>
                                </td>
                                <td width="38" class="data" id="fullNotes" style="display:none;">
                                    <?php echo($this->data['notes']); ?>&nbsp;
                                    <p><a href="#" class="moreText" onclick="toggleNotes(); return false;">[Less]</a></p>
                                </td>
                                <?php else: ?>
                                <td width="510" class="data" id="shortNotes" style="display:block;">
                                    <?php echo($this->data['notes']); ?>                                    </td>
                                <?php endif; ?>
                            </tr>
                            
                            <tr>
                                <td valign="top" class="vertical">Upcoming Events:</td>
                                <td id="shortNotes" style="display:block;" class="data">
                                    <?php foreach ($this->calendarRS as $rowNumber => $calendarData): ?>
                                    <div>
                                        <a href="<?php echo(RESUFLOUtility::getIndexName()); ?>?m=calendar&amp;view=DAYVIEW&amp;month=<?php echo($calendarData['month']); ?>&amp;year=20<?php echo($calendarData['year']); ?>&amp;day=<?php echo($calendarData['day']); ?>&amp;showEvent=<?php echo($calendarData['eventID']); ?>">
                                            <img src="<?php $this->_($calendarData['typeImage']) ?>" alt="" border="0" />
                                            <?php $this->_($calendarData['dateShow']) ?>:
                                            <?php $this->_($calendarData['title']); ?>
                                        </a>
                                    </div>
                                    <?php endforeach; ?>
                                    <?php if ($this->accessLevel >= ACCESS_LEVEL_EDIT): ?>
                                    <?php if($_REQUEST["list"]) { ?>								
                                    <a href="#" onclick="showPopWin('<?php echo(RESUFLOUtility::getIndexName()); ?>?m=candidates&amp;a=addActivityChangeStatus&amp;list=<?php echo($_REQUEST["list"]); ?>&amp;candidateID=<?php echo($this->candidateID); ?>&amp;jobOrderID=-1&amp;onlyScheduleEvent=true', 600, 350, null); return false;">
                                       <?php } else { ?>
                                       <a href="#" onclick="showPopWin('<?php echo(RESUFLOUtility::getIndexName()); ?>?m=candidates&amp;a=addActivityChangeStatus&amp;candidateID=<?php echo($this->candidateID); ?>&amp;jobOrderID=-1&amp;onlyScheduleEvent=true', 600, 350, null); return false;">
                                            <?php } ?>
                                            <img src="images/calendar_add.gif" width="16" height="16" border="0" alt="Schedule Event" class="absmiddle" />&nbsp;Schedule Event
                                        </a>
                                        <?php endif; ?>
                                        
                                        
                                        
                                </td>
                            </tr>
                            
                            <?php if (isset($this->questionnaires) && !empty($this->questionnaires)): ?>
                            <tr>
                                <td valign="top" class="vertical" align="left">Questionnaires:</td>
                                <td class="data" valign="top" align="left">
                                    <table cellpadding="0" cellspacing="0" border="0">
                                        <tr>
                                            <td style="border-bottom: 1px solid #c0c0c0; font-weight: bold; padding-right: 10px;">Title (Internal)</td>
                                            <td style="border-bottom: 1px solid #c0c0c0; font-weight: bold; padding-right: 10px;">Completed</td>
                                            <td style="border-bottom: 1px solid #c0c0c0; font-weight: bold; padding-right: 10px;">Description (Public)</td>
                                        </tr>
                                        <?php foreach ($this->questionnaires as $questionnaire): ?>
                                        <tr>
                                            <td style="padding-right: 10px;" nowrap="nowrap"><a href="<?php echo(RESUFLOUtility::getIndexName()); ?>?m=candidates&amp;a=show_questionnaire&amp;candidateID=<?php echo($this->candidateID); ?>&amp;questionnaireTitle=<?php echo urlencode($questionnaire['questionnaireTitle']); ?>&print=no"><?php echo $questionnaire['questionnaireTitle']; ?></a></td>
                                            <td style="padding-right: 10px;" nowrap="nowrap"><?php echo date('F j. Y', strtotime($questionnaire['questionnaireDate'])); ?></td>
                                            <td style="padding-right: 10px;" nowrap="nowrap"><?php echo $questionnaire['questionnaireDescription']; ?></td>
                                            <td style="padding-right: 10px;" nowrap="nowrap">
                                                <a id="edit_link" href="<?php echo(RESUFLOUtility::getIndexName()); ?>?m=candidates&amp;a=show_questionnaire&amp;candidateID=<?php echo($this->candidateID); ?>&amp;questionnaireTitle=<?php echo urlencode($questionnaire['questionnaireTitle']); ?>&print=no">
                                                    <img src="images/actions/view.gif" width="16" height="16" class="absmiddle" alt="view" border="0" />&nbsp;View
                                                </a>
                                                &nbsp;
                                                <a id="edit_link" href="<?php echo(RESUFLOUtility::getIndexName()); ?>?m=candidates&amp;a=show_questionnaire&amp;candidateID=<?php echo($this->candidateID); ?>&amp;questionnaireTitle=<?php echo urlencode($questionnaire['questionnaireTitle']); ?>&print=yes">
                                                    <img src="images/actions/print.gif" width="16" height="16" class="absmiddle" alt="print" border="0" />&nbsp;Print
                                                </a>
                                            </td>
                                        </tr>
                                        <?php endforeach; ?>
                                    </table>
                                </td>
                            </tr>
                            <?php endif; ?>
                            
                            <tr>
                                <td valign="top" class="vertical">Attachments:</td>
                                <td valign="top" class="data">
                                    <table class="attachmentsTable">
                                        
                                        <?php foreach ($this->attachmentsRS as $rowNumber => $attachmentsData): ?>
                                        <?php if ($attachmentsData['isProfileImage'] != '1'): ?>
                                        <tr>
                                            <td>
                                                <?php echo $attachmentsData['retrievalLink']; ?>
                                                <img src="<?php $this->_($attachmentsData['attachmentIcon']) ?>" alt="" width="16" height="16" border="0" />
                                                &nbsp;
                                                <?php $this->_($attachmentsData['originalFilename']) ?>
                                                </a>
                                            </td>
                                            <td><?php echo($attachmentsData['previewLink']); ?></td>
                                            <td><?php $this->_($attachmentsData['dateCreated']) ?></td>
                                            <td>
                                                <?php if (!$this->isPopup): ?>
                                                <?php if ($this->accessLevel >= ACCESS_LEVEL_DELETE): ?>
                                                <a href="<?php echo(RESUFLOUtility::getIndexName()); ?>?m=candidates&amp;a=deleteAttachment&amp;candidateID=<?php echo($this->candidateID); ?>&amp;attachmentID=<?php $this->_($attachmentsData['attachmentID']) ?>" onclick="javascript:return confirm('Delete this attachment?');">
                                                    <img src="images/actions/delete.gif" alt="" width="16" height="16" border="0" title="Delete" />
                                                </a>
                                                <?php endif; ?>
                                                <?php endif; ?>
                                            </td>
                                        </tr>
                                        <?php endif; ?>
                                        <?php endforeach; ?>
                                    </table>
                                    <?php if (!$this->isPopup): ?>
                                    <?php if ($this->accessLevel >= ACCESS_LEVEL_EDIT): ?>
                                    <?php if (isset($this->attachmentLinkHTML)): ?>
                                    <?php echo($this->attachmentLinkHTML); ?>
                                    <?php else: ?>
                                    <a href="#" onclick="showPopWin('<?php echo(RESUFLOUtility::getIndexName()); ?>?m=candidates&amp;a=createAttachment&amp;candidateID=<?php echo($this->candidateID); ?>', 400, 125, null); return false;">
                                        <?php endif; ?>
                                        <img src="images/paperclip_add.gif" width="16" height="16" border="0" alt="Add Attachment" class="absmiddle" />&nbsp;Add Attachment
                                    </a>
                                    <?php endif; ?>
                                    <?php endif; ?>
                                </td>
                            </tr>
                            
                            <tr>
                                <td valign="top" class="vertical">Reminder Events:</td>
                                <td id="shortNotes" style="display:block;" class="data">
                                    <?php foreach ($this->calendarReminderThanksPhoneRS as $rowNumber => $calendarData){ ?>
                                    <?php if($calendarData['reminderSent'] == '0'){ ?>
                                        Created On: <?php echo $calendarData['dateCreated'];  ?> 
                                        <a href="<?php echo(RESUFLOUtility::getIndexName()); ?>?m=candidates&amp;a=cancelReminder&amp;candidateID=<?php echo($this->candidateID); ?>&amp;eventID=<?php echo($calendarData['eventID']); ?>" onclick="javascript:return confirm('Delete this reminder?');">
                                            Cancel
                                        </a>
                                        <br>
                                    <?php }} ?>
                                </td>
                            </tr>
                            
                            <tr>
                                <td valign="top" class="vertical">Thanks Messages:</td>
                                <td id="shortNotes" style="display:block;" class="data">
                                    <?php foreach ($this->calendarReminderThanksPhoneRS as $rowNumber => $calendarData){ ?>
                                    <?php if($calendarData['thanksSent'] == '0'){ ?>
                                        Created On: <?php echo $calendarData['dateCreated'];  ?> 
                                        <a href="<?php echo(RESUFLOUtility::getIndexName()); ?>?m=candidates&amp;a=cancelThanks&amp;candidateID=<?php echo($this->candidateID); ?>&amp;eventID=<?php echo($calendarData['eventID']); ?>" onclick="javascript:return confirm('Delete this thanks message?');">
                                            Cancel
                                        </a>
                                        <br>
                                    <?php }} ?>
                                </td>
                            </tr>
                            
                        </table>
                    </td>
                </tr>
            </table>
            <?php if (!$this->isPopup): ?>
            <?php if ($this->list): ?>
            <?php if ($this->accessLevel >= ACCESS_LEVEL_EDIT): ?>
            <a id="edit_link" href="<?php echo(RESUFLOUtility::getIndexName()); ?>?m=candidates&amp;a=edit&amp;candidateID=<?php echo($this->candidateID); ?>&amp;list=<?php echo($this->list); ?>">
                <img src="images/actions/edit.gif" width="16" height="16" class="absmiddle" alt="edit" border="0" />&nbsp;Edit
            </a>
            &nbsp;&nbsp;&nbsp;&nbsp;
            <?php endif; ?>
            <?php if ($this->accessLevel >= ACCESS_LEVEL_DELETE): ?>
            <a id="delete_link" href="<?php echo(RESUFLOUtility::getIndexName()); ?>?m=candidates&amp;a=delete&amp;candidateID=<?php echo($this->candidateID); ?>&amp;list=<?php echo($this->list); ?>" onclick="javascript:return confirm('Delete this candidate?');">
                <img src="images/actions/delete.gif" width="16" height="16" class="absmiddle" alt="delete" border="0" />&nbsp;Delete
            </a>
            &nbsp;&nbsp;&nbsp;&nbsp;
            <?php endif; ?>
            
            <?php endif; ?>
            <?php if (!$this->list): ?>
            <?php if ($this->accessLevel >= ACCESS_LEVEL_EDIT): ?>
            <a id="edit_link" href="<?php echo(RESUFLOUtility::getIndexName()); ?>?m=candidates&amp;a=edit&amp;candidateID=<?php echo($this->candidateID); ?>">
                <img src="images/actions/edit.gif" width="16" height="16" class="absmiddle" alt="edit" border="0" />&nbsp;Edit
            </a>
            &nbsp;&nbsp;&nbsp;&nbsp;
            <?php endif; ?>
            <?php if ($this->accessLevel >= ACCESS_LEVEL_DELETE): ?>
            <a id="delete_link" href="<?php echo(RESUFLOUtility::getIndexName()); ?>?m=candidates&amp;a=delete&amp;candidateID=<?php echo($this->candidateID); ?>" onclick="javascript:return confirm('Delete this candidate?');">
                <img src="images/actions/delete.gif" width="16" height="16" class="absmiddle" alt="delete" border="0" />&nbsp;Delete
            </a>
            &nbsp;&nbsp;&nbsp;&nbsp;
            <?php endif; ?>
            
            <?php endif; ?>
            <?php if ($this->privledgedUser): ?>
            <a id="history_link" href="<?php echo(RESUFLOUtility::getIndexName()); ?>?m=settings&amp;a=viewItemHistory&amp;dataItemType=100&amp;dataItemID=<?php echo($this->candidateID); ?>">
                <img src="images/icon_clock.gif" width="16" height="16" class="absmiddle"  border="0" />&nbsp;View History
            </a>
            &nbsp;&nbsp;&nbsp;&nbsp;
            <?php endif; ?>
            <?php if ($this->isTextMessageEnable):?>
                <a target="_blank" href="<?php echo(RESUFLOUtility::getIndexName()); ?>?m=candidates&amp;a=sms&amp;candidateID=<?php echo($this->candidateID); ?>">
                    <img src="images/sms.png" width="16" height="16" class="absmiddle"  border="0" />&nbsp;View Text Messages
                </a>
            <?php endif; ?>
            <?php if ($this->accessLevel >= ACCESS_LEVEL_MULTI_SA): ?>
            <?php if ($this->data['isAdminHidden'] == 1): ?>
            <a href="<?php echo(RESUFLOUtility::getIndexName()); ?>?m=candidates&amp;a=administrativeHideShow&amp;candidateID=<?php echo($this->candidateID); ?>&amp;state=0">
                <img src="images/resume_preview_inline.gif" width="16" height="16" class="absmiddle" alt="delete" border="0" />&nbsp;Administrative Show
            </a>
            <?php else: ?>
            <a href="<?php echo(RESUFLOUtility::getIndexName()); ?>?m=candidates&amp;a=administrativeHideShow&amp;candidateID=<?php echo($this->candidateID); ?>&amp;state=1">
                <img src="images/resume_preview_inline.gif" width="16" height="16" class="absmiddle" alt="delete" border="0" />&nbsp;Administrative Hide
            </a>
            <?php endif; ?>
            &nbsp;&nbsp;&nbsp;&nbsp;
            <?php endif; ?>
            <?php endif; ?>
            <br clear="all" />
            <br />
            
            <p class="top_heading">Job Order Pipeline</p>
            <table class="sortablepair" width="925">
                <tr>
                    <th></th>
                    <th align="left">Match</th>
                    <th align="left">Title</th>
                    <th align="left">Company</th>
                    <th align="left">Owner</th>
                    <th align="left">Added</th>
                    <th align="left">Entered By</th>
                    <th align="left">Status</th>
                    <?php if (!$this->isPopup): ?>
                    <th align="center">Action</th>
                    <?php endif; ?>
                </tr>
                
                <?php foreach ($this->pipelinesRS as $rowNumber => $pipelinesData): ?>
                <tr class="<?php TemplateUtility::printAlternatingRowClass($rowNumber); ?>" id="pipelineRow<?php echo($rowNumber); ?>">
                    <td valign="top">
                        <span id="pipelineOpen<?php echo($rowNumber); ?>">
                            <a href="javascript:void(0);" onclick="document.getElementById('pipelineDetails<?php echo($rowNumber); ?>').style.display=''; document.getElementById('pipelineClose<?php echo($rowNumber); ?>').style.display = ''; document.getElementById('pipelineOpen<?php echo($rowNumber); ?>').style.display = 'none'; PipelineDetails_populate(<?php echo($pipelinesData['candidateJobOrderID']); ?>, 'pipelineInner<?php echo($rowNumber); ?>', '<?php echo($this->sessionCookie); ?>');">
                                <img src="images/arrow_next.png" alt="" border="0" title="Show History" />
                            </a>
                        </span>
                        <span id="pipelineClose<?php echo($rowNumber); ?>" style="display: none;">
                            <a href="javascript:void(0);" onclick="document.getElementById('pipelineDetails<?php echo($rowNumber); ?>').style.display = 'none'; document.getElementById('pipelineClose<?php echo($rowNumber); ?>').style.display = 'none'; document.getElementById('pipelineOpen<?php echo($rowNumber); ?>').style.display = '';">
                                <img src="images/arrow_down.png" alt="" border="0" title="Hide History" />
                            </a>
                        </span>
                    </td>
                    <td valign="top">
                        <?php echo($pipelinesData['ratingLine']); ?>
                    </td>
                    <td valign="top">
                        <a href="<?php echo(RESUFLOUtility::getIndexName()); ?>?m=joborders&amp;a=show&amp;jobOrderID=<?php echo($pipelinesData['jobOrderID']); ?>" class="<?php $this->_($pipelinesData['linkClass']) ?>">
                            <?php $this->_($pipelinesData['title']) ?>
                        </a>
                    </td>
                    <td valign="top">
                        <a href="<?php echo(RESUFLOUtility::getIndexName()); ?>?m=companies&amp;companyID=<?php echo($pipelinesData['companyID']); ?>&amp;a=show">
                            <?php $this->_($pipelinesData['companyName']) ?>
                        </a>
                    </td>
                    <td valign="top"><?php $this->_($pipelinesData['ownerAbbrName']) ?></td>
                    <td valign="top"><?php $this->_($pipelinesData['dateCreated']) ?></td>
                    <td valign="top"><?php $this->_($pipelinesData['addedByAbbrName']) ?></td>
                    <td valign="top" nowrap="nowrap"><?php $this->_($pipelinesData['status']) ?></td>
                    <?php if (!$this->isPopup): ?>
                    <td align="center" nowrap="nowrap">
                        <?php eval(Hooks::get('CANDIDATE_TEMPLATE_SHOW_PIPELINE_ACTION')); ?>
                        <?php if ($_SESSION['RESUFLO']->getAccessLevel() >= ACCESS_LEVEL_EDIT && !$_SESSION['RESUFLO']->hasUserCategory('sourcer')): ?>
                        <?php if ($pipelinesData['ratingValue'] < 0): ?>
                        <a href="#" id="screenLink<?php echo($pipelinesData['candidateJobOrderID']); ?>" onclick="moImageValue<?php echo($pipelinesData['candidateJobOrderID']); ?> = 0; setRating(<?php echo($pipelinesData['candidateJobOrderID']); ?>, 0, 'moImage<?php echo($pipelinesData['candidateJobOrderID']); ?>', '<?php echo($_SESSION['RESUFLO']->getCookie()); ?> '); return false;">
                            <img id="screenImage<?php echo($pipelinesData['candidateJobOrderID']); ?>" src="images/actions/screen.gif" width="16" height="16" class="absmiddle" alt="" border="0" title="Mark as Screened" />
                        </a>
                        <?php else: ?>
                        <img src="images/actions/blank.gif" width="16" height="16" class="absmiddle" alt="" border="0" />
                        <?php endif; ?>
                        <?php endif; ?>
                        <?php if ($this->accessLevel >= ACCESS_LEVEL_EDIT): ?>
                        <a href="#" onclick="showPopWin('<?php echo(RESUFLOUtility::getIndexName()); ?>?m=candidates&amp;a=addActivityChangeStatus&amp;list=<?php echo($this->list); ?>&amp;candidateID=<?php echo($this->candidateID); ?>&amp;jobOrderID=<?php echo($pipelinesData['jobOrderID']); ?>', 600, 480, null); return false;" >
                            <img src="images/actions/edit.gif" width="16" height="16" class="absmiddle" alt="" border="0" title="Log an Activity / Change Status"/>
                        </a>
                        <?php endif; ?>
                        <?php if ($this->accessLevel >= ACCESS_LEVEL_DELETE): ?>
                        <a href="<?php echo(RESUFLOUtility::getIndexName()); ?>?m=candidates&amp;a=removeFromPipeline&amp;candidateID=<?php echo($this->candidateID); ?>&amp;jobOrderID=<?php echo($pipelinesData['jobOrderID']); ?>"  onclick="javascript:return confirm('Delete from <?php $this->_(str_replace('\'', '\\\'', $pipelinesData['title'])); ?> (<?php $this->_(str_replace('\'', '\\\'', $pipelinesData['companyName'])); ?>) pipeline?')">
                            <img src="images/actions/delete.gif" width="16" height="16" class="absmiddle" alt="" border="0" title="Remove from Pipeline"/>
                        </a>
                        <?php endif; ?>
                    </td>
                    <?php endif; ?>
                </tr>
                <tr class="<?php TemplateUtility::printAlternatingRowClass($rowNumber); ?>" id="pipelineDetails<?php echo($rowNumber); ?>" style="display:none;">
                    <td colspan="11" align="center">
                        <table width="98%" border="1" class="detailsOutside" style="margin: 5px;">
                            <tr>
                                <td align="left" style="padding: 6px 6px 6px 6px; background-color: white; clear: both;">
                                    <div style="overflow: auto; height: 200px;" id="pipelineInner<?php echo($rowNumber); ?>">
                                        <img src="images/indicator.gif" alt="" />&nbsp;&nbsp;Loading pipeline details...
                                    </div>
                                </td>
                            </tr>
                        </table>
                    </td>
                </tr>
                
                <?php endforeach; ?>
            </table>
            
            <?php if (!$this->isPopup): ?>
            <?php if ($this->accessLevel >= ACCESS_LEVEL_EDIT): ?>
            <a href="#" onclick="showPopWin('<?php echo(RESUFLOUtility::getIndexName()); ?>?m=candidates&amp;a=considerForJobSearch&amp;candidateID=<?php echo($this->candidateID); ?>', 750, 390, null); return false;">
                <img src="images/consider.gif" width="16" height="16" class="absmiddle" alt="Add to Pipeline" border="0" />&nbsp;Add This Candidate to Job Order Pipeline
            </a>
            <?php endif; ?>
            <?php endif; ?>
            <br clear="all" />
            <br />
            
            <p class="top_heading">Activity</p>
            
            <table id="activityTable" class="sortable" width="925">
                <tr>
                    <th align="left" width="125">Date</th>
                    <th align="left" width="90">Type</th>
                    <th align="left" width="90">Entered</th>
                    <th align="left" width="250">Regarding</th>
                    <th align="left">Notes</th>
                    <?php if (!$this->isPopup): ?>
                    <th align="left" width="40">Action</th>
                    <?php endif; ?>
                </tr>
                
                <?php foreach ($this->activityRS as $rowNumber => $activityData): ?>
                <tr class="<?php TemplateUtility::printAlternatingRowClass($rowNumber); ?>">
                    <td align="left" valign="top" id="activityDate<?php echo($activityData['activityID']); ?>"><?php $this->_($activityData['dateCreated']) ?></td>
                    <td align="left" valign="top" id="activityType<?php echo($activityData['activityID']); ?>"><?php $this->_($activityData['typeDescription']) ?></td>
                    <td align="left" valign="top"><?php $this->_($activityData['enteredByAbbrName']) ?></td>
                    <td align="left" valign="top" id="activityRegarding<?php echo($activityData['activityID']); ?>"><?php $this->_($activityData['regarding']) ?></td>
                    <td align="left" valign="top" id="activityNotes<?php echo($activityData['activityID']); ?>"><?php echo($activityData['notes']); ?></td>
                    <?php if (!$this->isPopup): ?>
                    <td align="center" >
                        <?php if ($this->accessLevel >= ACCESS_LEVEL_EDIT): ?>
                        <a href="#" id="editActivity<?php echo($activityData['activityID']); ?>" onclick="Activity_editEntry(<?php echo($activityData['activityID']); ?>, <?php echo($this->candidateID); ?>, <?php echo(DATA_ITEM_CANDIDATE); ?>, '<?php echo($this->sessionCookie); ?>'); return false;">
                            <img src="images/actions/edit.gif" width="16" height="16" class="absmiddle" alt="" border="0" title="Edit" />
                        </a>
                        <?php endif; ?>
                        <?php if ($this->accessLevel >= ACCESS_LEVEL_DELETE): ?>
                        <a href="#" id="deleteActivity<?php echo($activityData['activityID']); ?>" onclick="Activity_deleteEntry(<?php echo($activityData['activityID']); ?>, '<?php echo($this->sessionCookie); ?>'); return false;">
                            <img src="images/actions/delete.gif" width="16" height="16" class="absmiddle" alt="" border="0" title="Delete" />
                        </a>
                        <?php endif; ?>
                    </td>
                    <?php endif; ?>
                </tr>
                <?php endforeach; ?>
            </table>
            <?php if (!$this->isPopup): ?>
            <div id="addActivityDiv">
                <?php if ($this->accessLevel >= ACCESS_LEVEL_EDIT): ?>
                <a href="#" id="addActivityLink" onclick="showPopWin('<?php echo(RESUFLOUtility::getIndexName()); ?>?m=candidates&amp;a=addActivityChangeStatus&amp;list=<?php echo($this->list); ?>&amp;candidateID=<?php echo($this->candidateID); ?>&amp;jobOrderID=-1', 600, 480, null); return false;">
                    <img src="images/new_activity_inline.gif" width="16" height="16" class="absmiddle" title="Log an Activity / Change Status" alt="Log an Activity / Change Status" border="0" />&nbsp;Log an Activity
                </a>
                <?php endif; ?>
                <img src="images/indicator2.gif" id="addActivityIndicator" alt="" style="visibility: hidden; margin-left: 5px;" height="16" width="16" />
            </div>
        </div>
    </form>
</div>



<?php endif; ?>
<div id="bottomShadow"></div>
<?php TemplateUtility::printFooter(); ?>
