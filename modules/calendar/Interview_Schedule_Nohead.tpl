<?php /* $Id: Calendar_Nohead.tpl 1972 2015-03-10 04:34:10Z jjohnston $ */ ?>
<link rel="stylesheet" type="text/css" href="modules/calendar/Calendar.css"/>
<script src="js/highlightrows.js"></script>
<script src="modules/calendar/Calendar.js"></script>
<script src="modules/calendar/CalendarUI.js"></script>
<script src="modules/calendar/validator.js"></script>

            <div style="display:none;">
                <?php if ($this->userIsSuperUser == 1): ?>
                    <input type="checkbox" name="hideNonPublic" id="hideNonPublic" onclick="refreshView();" <?php if ($this->superUserActive): ?>checked<?php endif; ?>/>Show Entries from Other Users
                <?php else: ?>
                    <input type="checkbox" style="display:none;" name="hideNonPublic" id="hideNonPublic" onclick="" />
                <?php endif; ?>
            </div>

            <p class="note" id="calendarTitle" style="display:none;">Calendar</p>

            <?php require('Interview_Schedule_Guts.tpl'); ?>
