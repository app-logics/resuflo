<style>
    table.optionView{
        border-collapse: collapse;
        border: 1px solid white;
        
    }
    .optionView th
    {
        text-align: left;
    }
    .optionView td
    {
        width: 100px;
        text-align: left;
    }
    .optionView td, th
    {
        padding: 5px;
    }
    .optionView tr:nth-child(even) {background: #FFF}
</style>
<table class="optionView">
    <tr style="background: #C4DCE6;">
        <th></th><th>Day</th><th>From</th><th>End</th><th  style="width: auto;">Location</th>
    </tr>
    <?php 
        if($this->userID != 1585){
            $count = count($this->schedulesArray);
            for ($i = 0; $i < $count; $i++) {
    ?>
        <tr>
            <td style="width: 30px;">
                <input type='radio' name='gender' onclick="handleInterviewScheduleClickEntry(this)" EventId ="<?php echo $this->schedulesArray[$i]['eventID']; ?>" value="<?php echo $this->schedulesArray[$i]['year'];?>-<?php echo $this->schedulesArray[$i]['month']?>-<?php echo $this->schedulesArray[$i]['day']?> <?php echo $this->schedulesArray[$i]['hour24']?>:<?php echo $this->schedulesArray[$i]['minute']?>:00" location="<?php echo $this->schedulesArray[$i]['venue'];?>" style="margin-top: 0px;">
            </td>
            <td>
                <?php echo $this->schedulesArray[$i]['date'];?>
            </td>
            <td>
                <?php echo $this->schedulesArray[$i]['time'];?>
            </td>
            <td>
                <?php echo $this->schedulesArray[$i]['endtime'];?>
            </td>
            <td style="width: auto;">
                <a href="http://www.moodystreet.com/locate-us/" target="_blank">
                    <?php echo $this->schedulesArray[$i]['venue'];?>
                </a>
            </td>
        </tr>
    <?php }}?>
    <?php 
        $count = count($this->acuityEvents);
        for ($i = 0; $i < $count; $i++) {
        $EndDate = $this->acuityEvents[$i]['Date'];
    ?>
        <tr>
            <td style="width: 30px;">
                <input type='radio' name='gender' onclick="handleInterviewScheduleClickEntry(this)" EventId ="0" value="<?php echo $this->acuityEvents[$i]['Date']->format('Y-m-d H:i:s');?>" location="<?php echo $this->acuityEvents[$i]['Name'].' '.$this->acuityEvents[$i]['Description'];?>" cName="<?php echo $this->acuityEvents[$i]['Name']; ?>" Description="<?php echo $this->acuityEvents[$i]['Description']; ?>" Duration="<?php echo $this->acuityEvents[$i]['Duration']; ?>" TimeZone="<?php echo $this->acuityEvents[$i]['TimeZone'];?>" style="margin-top: 0px;">
            </td>
            <td>
                <?php echo $this->acuityEvents[$i]['Date']->format('D m-d-Y');?>
            </td>
            <td>
                <?php echo $this->acuityEvents[$i]['Date']->format('h:i A');?>
            </td>
            <td>
                <?php echo $EndDate->modify("+{$this->acuityEvents[$i]['Duration']} minutes")->format('h:i A');?>
            </td>
            <td style="width: auto;">
                <a href="http://www.moodystreet.com/locate-us/" target="_blank">
                    <?php echo $this->acuityEvents[$i]['Name'].' '.$this->acuityEvents[$i]['Description'];?>
                </a>
            </td>
        </tr>
    <?php }?>
</table>
  