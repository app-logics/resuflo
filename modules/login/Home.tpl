<?php /* $Id: Login.tpl 3530 2007-11-09 18:28:10Z brian $ */ ?>
<!DOCTYPE html PUBLIC "-//W3C//DTD XHTML 1.0 Transitional//EN"
    "http://www.w3.org/TR/xhtml1/DTD/xhtml1-transitional.dtd">

<html>
    <head>
        <title>RESUFLO - Login </title>
        <meta http-equiv="Content-Type" content="text/html; charset=<?php echo(HTML_ENCODING); ?>" />
        <meta charset="utf-8" />
        <meta name="viewport" content="width=device-width, initial-scale=1, shrink-to-fit=no" />
        <link rel="icon" href="modules/login/LandingPage/favicon_resuflo.ico" type="ICO/ico"/>
        
        <link href="modules/login/LandingPage/css/bootstrap.css" rel="stylesheet" type="text/css"/>
        <link href="modules/login/LandingPage/css/style.css" rel="stylesheet" type="text/css"/>
        <link href="modules/login/LandingPage/css/linecons.css" rel="stylesheet" type="text/css"/>
        <link href="modules/login/LandingPage/css/font-awesome.css" rel="stylesheet" type="text/css"/>
        <link href="modules/login/LandingPage/css/responsive.css" rel="stylesheet" type="text/css"/>
        <link href="modules/login/LandingPage/css/animate.css" rel="stylesheet" type="text/css"/>
        
        <link href='https://fonts.googleapis.com/css?family=Lato:400,900,700,700italic,400italic,300italic,300,100italic,100,900italic' rel='stylesheet' type='text/css'/>
        <link href='https://fonts.googleapis.com/css?family=Dosis:400,500,700,800,600,300,200' rel='stylesheet' type='text/css'/>
        
        <script type="text/javascript" src="modules/login/LandingPage/js/jquery.1.8.3.min.js"></script>
        <script type="text/javascript" src="modules/login/LandingPage/js/bootstrap.js"></script>
        <script type="text/javascript" src="modules/login/LandingPage/js/jquery-scrolltofixed.js"></script>
        <script type="text/javascript" src="modules/login/LandingPage/js/jquery.easing.1.3.js"></script>
        <script type="text/javascript" src="modules/login/LandingPage/js/jquery.isotope.js"></script>
        <script type="text/javascript" src="modules/login/LandingPage/js/wow.js"></script>
        <script type="text/javascript" src="modules/login/LandingPage/js/classie.js"></script>
        
        <script type="text/javascript">
            $(document).ready(function (e) {
                $('.res-nav_click').click(function () {
                    $('ul.toggle').slideToggle(600)
                });

                $(document).ready(function () {
                    $(window).bind('scroll', function () {
                        if ($(window).scrollTop() > 0) {
                            $('#header_outer').addClass('fixed');
                        }
                        else {
                            $('#header_outer').removeClass('fixed');
                        }
                    });

                });


            });
            function resizeText() {
                var preferredWidth = 767;
                var displayWidth = window.innerWidth;
                var percentage = displayWidth / preferredWidth;
                var fontsizetitle = 25;
                var newFontSizeTitle = Math.floor(fontsizetitle * percentage);
                $(".divclass").css("font-size", newFontSizeTitle)
            }
        </script>
        
        <style>
            .mt25 {
                margin-top: 25px;
            }
            .contact_block_icon span {
                background: none repeat scroll 0 0 #c90000;
            }
            body{
                padding: 0px !important;
            }
            .hover{
                position: absolute;
                width: 100%;
                height: 100%;
                z-index: 1000;
            }
        </style>
        
        <script type="text/javascript" src="js/lib.js"></script>
        <script type="text/javascript" src="modules/login/validator.js"></script>
        <script type="text/javascript" src="js/submodal/subModal.js"></script>
    </head>
    
    <body>
        <div class="body">
            <!--Header_section-->
            <header id="header_outer">
                <div class="container">
                    <div class="header_section">
                        <div class="logo"><a href="javascript:void(0)"><img src="images/resuflo_logo_transparent.png" alt="" style="width:264px;"></a></div>
                        <nav class="nav mt25" id="nav">
                            <ul class="toggle">
                                <li><a href="#top_content">Home</a></li>
                                <li><a href="#service">Services</a></li>
                                <li><a href="#work_outer">Employers</a></li>
                                <li><a href="#policy">Privacy Policy</a></li>
                                <li><a href="#contact">Contact</a></li>
                                <li><a href="<?php echo SITE_URL; ?>/index.php?m=login&a=showLoginForm" class="btn btn-success"> <i class="fa fa-sign-in"></i> Login</a></li>
                            </ul>
                            <ul class="">
                                <li><a href="#top_content">Home</a></li>
                                <li><a href="#service">Services</a></li>
                                <li><a href="#work_outer">Employers</a></li>
                                <li><a href="#policy">Privacy Policy</a></li>
                                <li><a href="#contact">Contact</a></li>
                                <li><a href="<?php echo SITE_URL; ?>/index.php?m=login&a=showLoginForm" class="btn btn-success"> <i class="fa fa-sign-in"></i> Login</a></li>
                            </ul>
                        </nav>
                        <a class="res-nav_click animated wobble wow" href="javascript:void(0)"><i class="fa-bars"></i></a>
                    </div>
                </div>
            </header>
            <!--Header_section-->
            <!--Top_content-->
            <section id="top_content" class="top_cont_outer">
                <div class="top_cont_inner">
                    <div class="container">
                        <div class="top_content">
                            <div class="row">
                                <div class="col-lg-7 col-sm-7">
                                    <div class="top_left_cont animated fadeInUp wow">
                                        <h2>Introducing <span style="color: #c90000;">ResufloCRM</span></h2>
                                        <p> Custom CRM - Candidate Sourcing - Drip Email Marketing – Custom Landing Page </p>
                                    </div>
                                </div>
                                <div class="col-lg-5 col-sm-5"> </div>
                            </div>
                        </div>
                    </div>
                </div>
            </section>
            <!--Top_content--> <br />
            <div class="container">
                <h2>
                    Prospect Management Solutions for Your Business
                </h2>
                <p style="font-size: 18px;">
                    We are a highly successful recruiting and implementation agency working all over the United States. We focus on developing the best and most effective way for our clients to attract the highest quality candidates in their industries. We help to supply a wide range of industries with candidates varying from human resources and healthcare to insurance and financial services and many more
                </p>
            </div>
            
            <!--Service-->
            <section id="service">
                <div class="container">
                    <h2>Industries We Serve</h2>
                    <div class="service_area">
                        <div class="row">
                            <div class="col-lg-3">
                                <div class="service_block">
                                    <div class="service_icon delay-03s animated wow  zoomIn"> <span><i class="interact"></i><img src="modules/login/LandingPage/img/process64wht.png" /></span> </div>
                                    <h3 class="animated fadeInUp wow">Insurance</h3>
                                </div>
                            </div>
                            <div class="col-lg-3">
                                <div class="service_block">
                                    <div class="service_icon delay-03s animated wow  zoomIn"> <span><i class="interact"></i><img src="modules/login/LandingPage/img/process64wht.png" /></span> </div>
                                    <h3 class="animated fadeInUp wow">Information Technology</h3>
                                </div>
                            </div>
                            <div class="col-lg-3">
                                <div class="service_block">
                                    <div class="service_icon delay-03s animated wow  zoomIn"> <span><i class="interact"></i><img src="modules/login/LandingPage/img/process64wht.png" /></span> </div>
                                    <h3 class="animated fadeInUp wow">Financial Services</h3>
                                </div>
                            </div>
                            <div class="col-lg-3">
                                <div class="service_block">
                                    <div class="service_icon delay-03s animated wow  zoomIn"> <span><i class="interact"></i><img src="modules/login/LandingPage/img/process64wht.png" /></span> </div>
                                    <h3 class="animated fadeInUp wow">Office Support</h3>
                                </div>
                            </div>
                            <div class="col-lg-3">
                                <div class="service_block">
                                    <div class="service_icon delay-03s animated wow  zoomIn"> <span><i class="interact"></i><img src="modules/login/LandingPage/img/process64wht.png" /></span> </div>
                                    <h3 class="animated fadeInUp wow">Clerical & Administrative</h3>
                                </div>
                            </div>
                            <div class="col-lg-3">
                                <div class="service_block">
                                    <div class="service_icon delay-03s animated wow  zoomIn"> <span><i class="interact"></i><img src="modules/login/LandingPage/img/process64wht.png" /></span> </div>
                                    <h3 class="animated fadeInUp wow">Banking</h3>
                                </div>
                            </div>
                            <div class="col-lg-3">
                                <div class="service_block">
                                    <div class="service_icon delay-03s animated wow  zoomIn"> <span><i class="interact"></i><img src="modules/login/LandingPage/img/process64wht.png" /></span> </div>
                                    <h3 class="animated fadeInUp wow">Call Center</h3>
                                </div>
                            </div>
                            <div class="col-lg-3">
                                <div class="service_block">
                                    <div class="service_icon delay-03s animated wow  zoomIn"> <span><i class="interact"></i><img src="modules/login/LandingPage/img/process64wht.png" /></span> </div>
                                    <h3 class="animated fadeInUp wow">Accounting</h3>
                                </div>
                            </div>
                            <div class="col-lg-3">
                                <div class="service_block">
                                    <div class="service_icon delay-03s animated wow  zoomIn"> <span><i class="interact"></i><img src="modules/login/LandingPage/img/process64wht.png" /></span> </div>
                                    <h3 class="animated fadeInUp wow">Manufacturing</h3>
                                </div>
                            </div>
                            <div class="col-lg-3">
                                <div class="service_block">
                                    <div class="service_icon delay-03s animated wow  zoomIn"> <span><i class="interact"></i><img src="modules/login/LandingPage/img/process64wht.png" /></span> </div>
                                    <h3 class="animated fadeInUp wow">Maintenance Tech</h3>
                                </div>
                            </div>
                            <div class="col-lg-3">
                                <div class="service_block">
                                    <div class="service_icon delay-03s animated wow  zoomIn"> <span><i class="interact"></i><img src="modules/login/LandingPage/img/process64wht.png" /></span> </div>
                                    <h3 class="animated fadeInUp wow">Warehouse & Distribution</h3>
                                </div>
                            </div>
                        </div>
                    </div>
                    <h3 style="text-decoration: underline;">Privacy Policy</h3>
                    <p style="font-size: 16px;">
                        ResufloCRM.com, is committed to keeping any and all personal information collected of those individuals that visit our website and make use of our online facilities and services accurate, confidential, secure and private. Our privacy policy has been designed and created to ensure those affiliated with ResufloCRM.com of our commitment and realization of our obligation not only to meet but to exceed most existing privacy standards.
                    </p>
                    <p>
                        Click here to <a href="#" onclick="$('.body').addClass('hide');$('.policy').removeClass('hide');">read more</a>
                    </p>
                </div>
            </section>
            <!--Service-->
            
            <section id="work_outer">
                <!--main-section-start-->
                <div class="">
                    <div class="container">
                        <h2>Employers</h2>
                        <br />
                        <div class="">
                            <div class="row">
                                <div class="col-lg-12 col-sm-12 wow fadeInLeft delay-05s">
                                    <div class="service-list">
                                        <div class="service-list-col2">
                                            <p style="font-size: 18px;">
                                                Recruiters Edge the parent company of Resuflo CRM is an innovative and rapidly growing recruiting firm. We provide our clients with cutting-edge technologies and the know-how to apply them to their individual business models.
                                            </p>
                                            <br /><br />
                                            <p style="font-size: 18px;">
                                                We have been in the recruiting and hiring business for over 20 years and our founders have a combined experience of over 40 years in the industry.  Our sole purpose when developing our system was to create a platform that could easily change and adapt to needs of the recruiting business.  Having a system designed and built by highly successful recruiters is why our system works so well for our clients.  Not only do we understand our clients needs and struggles in the market place we live them each day.  For this reason we are committed to continually looking for new solutions that will attract the highest quality candidates with a combination of disciplined strategies and effective technology solutions.
                                            </p>
                                        </div>
                                    </div>
                                </div>
                            </div>
                        </div>
                    </div>
                    <!--<div class="work_pic"><img src="img/dashboard_pic.png" alt=""></div>-->
                </div>
            </section>
            <!--main-section-end-->
            <section id="policy">
                <!--main-section-start-->
                <div class="">
                    <div class="container">
                        <h2>Privacy Policy</h2>
                        <br />
                        <div class="">
                            <div class="row">
                                <div class="col-lg-12 col-sm-12 wow fadeInLeft delay-05s">
                                    <div class="service-list">
                                        <div class="service-list-col2">
                                            <p>
                            <strong>ONLINE PRIVACY POLICY AGREEMENT</strong></p>
                        <p>
                            &nbsp;</p>
                        <p>
                            ResufloCRM.com, is committed to keeping any and all personal information collected of those individuals that visit our website and make use of our online facilities and services accurate, confidential, secure and private. Our privacy policy has been designed and created to ensure those affiliated with ResufloCRM.com of our commitment and realization of our obligation not only to meet but to exceed most existing privacy standards.</p>
                        <p>
                            &nbsp;</p>
                        <p>
                            <strong><em>THEREFORE</em></strong>, this Privacy Policy Agreement shall apply to ResufloCRM.com , and thus it shall govern any and all data collection and usage thereof. Through the use of www.resuflocrm.com you are herein consenting to the following data procedures expressed within this agreement.</p>
                        <p>
                            &nbsp;</p>
                        <p>
                            <strong>Collection of Information</strong></p>
                        <p>
                            This website collects various types of information, such as:</p>
                        <p>
                            &nbsp;</p>
                        <p>
                            • Voluntarily provided information which may include your name, address, email address, billing and/or credit card information etc., which may be used when you purchase products and/or services and to deliver the services you have requested.</p>
                        <p>
                            Please rest assured that this site shall only collect personal information that you knowingly and willingly provide by way of surveys, completed membership forms, and emails. It is the intent of this site to use personal information only for the purpose for which it was requested and any additional uses specifically provided on this site. It is highly recommended and suggested that you review the privacy policies and statements of any website you choose to use or frequent as a means to better understand the way in which other websites garner, make use of and share information collected.</p>
                        <p>
                            &nbsp;</p>
                        <p>
                            <strong>Use of Information Collected</strong></p>
                        <p>
                            ResufloCRM.com may collect and may make use of personal information to assist in the operation of our website and to ensure delivery of the services you need and request. At times, we may find it necessary to use personally identifiable information as a means to keep you informed of other possible products and/or services that may be available to you from <a href="http://www.resuflocrm.com">www.resuflocrm.com</a>. ResufloCRM.com may also be in contact with you with regards to completing surveys and/or research questionnaires related to your opinion of current or potential future services that may be offered. ResufloCRM.com does not now, nor will it in the future, sell, rent or lease any of our customer lists and/or names to any third parties. ResufloCRM.com may disclose your personal information, without prior notice to you, only if required to do so in accordance with applicable laws and/or in a good faith belief that such action is deemed necessary or is required in an effort to:</p>
                        <p>
                            &nbsp;</p>
                        <p>
                            • Remain in conformance with any decrees, laws and/or statutes or in an effort to comply with any process which may be served upon ResufloCRM.com and/or our website;</p>
                        <p>
                            • Maintain, safeguard and/or preserve all the rights and/or property of ResufloCRM.com; and</p>
                        <p>
                            • Perform under demanding conditions in an effort to safeguard the personal safety of users of www.resuflocrm.com and/or the general public.</p>
                        <p>
                            &nbsp;</p>
                        <p>
                            <strong>Children Under Age of 13</strong></p>
                        <p>
                            ResufloCRM.com does not knowingly collect personal identifiable information from children under the age of thirteen (13) without verifiable parental consent. If it is determined that such information has been inadvertently collected on anyone under the age of thirteen (13), we shall immediately take the necessary steps to ensure that such information is deleted from our system's database. Anyone under the age of thirteen (13) must seek and obtain parent or guardian permission to use this website.</p>
                        <p>
                            &nbsp;</p>
                        <p>
                            <strong>Unsubscribe or Opt-Out</strong></p>
                        <p>
                            All users and/or visitors to our website have the option to discontinue receiving communication from us and/or reserve the right to discontinue receiving communications by way of email or newsletters. To discontinue or unsubscribe to our website please send an email that you wish to unsubscribe to</p>
                        <p>
                            admin@recruiters-edge.com. If you wish to unsubscribe or opt-out from any third party websites, you must go to that specific website to unsubscribe and/or opt-out.</p>
                        <p>
                            &nbsp;</p>
                        <p>
                            <strong>Links to Other Web Sites</strong></p>
                        <p>
                            Our website does contain links to affiliate and other websites. ResufloCRM.com does not claim nor accept responsibility for any privacy policies, practices and/or procedures of other such websites. Therefore, we encourage all users and visitors to be aware when they leave our website and to read the privacy statements of each and every website that collects personally identifiable information. The aforementioned Privacy Policy Agreement applies only and solely to the information collected by our website.</p>
                        <p>
                            &nbsp;</p>
                        <p>
                            <strong>Google Calendar Access</strong></p>
                        <p>
                            ResufloCRM.com will allow recruiters to connect their google calendar from settings using OAuth so scheduled evenets with applicants are added to google calendar also.
                        </p>
                        <p>
                            &nbsp;</p>
                            <strong>Google OAuth Access</strong></p>
                        <p>
                            ResufloCRM.com will allow recruiters to connect their google account using OAuth to only send email to candidates on behalf of recruiters.
                        </p>
                        <p>
                            &nbsp;</p>
                        <p>
                            <strong>Security</strong></p>
                        <p>
                            ResufloCRM.com shall endeavor and shall take every precaution to maintain adequate physical, procedural and technical security with respect to our offices and information storage facilities so as to prevent any loss, misuse, unauthorized access, disclosure or modification of the user's personal information under our control.</p>
                        <p>
                            &nbsp;</p>
                        <p>
                            <strong>Changes to Privacy Policy Agreement</strong></p>
                        <p>
                            ResufloCRM.com reserves the right to update and/or change the terms of our privacy policy, and as such we will post those change to our website homepage at www.resuflocrm.com, so that our users and/or visitors are always aware of the type of information we collect, how it will be used, and under what circumstances, if any, we may disclose such information. If at any point in time</p>
                        <p>
                            ResufloCRM.com decides to make use of any personally identifiable information on file, in a manner vastly different from that which was stated when this information was initially collected, the user or users shall be promptly notified by email. Users at that time shall have the option as to whether or not to permit the use of their information in this separate manner.</p>
                        <p>
                            &nbsp;</p>
                        <p>
                            <strong>Acceptance of Terms</strong></p>
                        <p>
                            Through the use of this website, you are hereby accepting the terms and conditions stipulated within the aforementioned Privacy Policy Agreement. If you are not in agreement with our terms and conditions, then you should refrain from further use of our sites. In addition, your continued use of our website following the posting of any updates or changes to our terms and conditions shall mean that you are in agreement and acceptance of such changes.</p>
                        <p>
                            &nbsp;</p>
                        <p>
                            <strong>How to Contact Us</strong></p>
                        <p>
                            If you have any questions or concerns regarding the Privacy Policy Agreement related to our website, please feel free to contact us at the following email, telephone number or mailing address.</p>
                        <p>
                            <strong>Email: </strong><a href="mailto:admin@recruiters-edge.com">admin@recruiters-edge.com</a></p>
                        <p>
                            <strong>Telephone Number: </strong>(469) 444-0230</p>
                        <p>
                            ResufloCRM.com</p>
                        <p>
                            &nbsp;</p>
                                        </div>
                                    </div>
                                </div>
                            </div>
                        </div>
                    </div>
                    <!--<div class="work_pic"><img src="img/dashboard_pic.png" alt=""></div>-->
                </div>
            </section>
            <!--main-section-end-->
            
            
            
            <footer class="footer_section" id="contact" style="background: none; background-color: #3c3c3c;">
                <div class="container">
                    <section class="main-section contact" id="contact">
                        <div class="contact_section">
                            <h2>Contact Us</h2>
                            <div class="row">
                                <div class="col-lg-4">
                                    <div class="contact_block">
                                        <div class="contact_block_icon animated wow"><span><i class="fa-book"></i></span></div>
                                        <span>
                                            11450  US  Hwy  380 Suite 130 <br/>
                                            #137 Cross  Roads,  TX  76227
                                        </span>
                                    </div>
                                </div>
                                <div class="col-lg-4 mtb50">
                                    <div class="contact_block">
                                        <div class="contact_block_icon animated wow"><span><i class="fa-phone"></i></span></div>
                                        <span> (469) 444-0230 </span>
                                    </div>
                                </div>
                                <div class="col-lg-4">
                                    <div class="contact_block">
                                        <div class="contact_block_icon animated wow"><span><i class="fa-envelope"></i></span></div>
                                        <span> <a href="mailto:admin@recruiters-edge.com"> admin@recruiters-edge.com</a> </span>
                                    </div>
                                </div>
                            </div>
                        </div>
                    </section>
                </div>
                <div class="container">
                    <div class="footer_bottom">
                        <span><!--© App logics--></span>
                        <div class="credits">
                            Copyright &copy; 2019 JSJ Enterprise LLC | Powered by <strong><?php echo SITENAME; ?></strong></a>
                        </div>
                    </div>
                </div>
            </footer>
            <script type="text/javascript">
                $(document).ready(function (e) {
                    $('#header_outer').scrollToFixed();
                    $('.res-nav_click').click(function () {
                        //$("#nav").css('margin-top', "0px");
                        $('.main-nav').slideToggle();
                        $('.nav ul li:last-child').css("margin", "0px");
                        //$('.container').css("padding-right","0px")
                        return false

                    });

                    $("li").click(function () {
                        $('ul.toggle').css("display", "none");
                    });

                    setInterval(function () { loop() }, 1);
                    function loop() {
                        if (window.innerWidth > 767) {
                            $("#nav").css('margin-top', "25px");
                            $("#header_outer").css("padding", "10px 0px");
                            $(".mtb50").css("margin", "0px 0px");
                        }
                        else {
                            $("#nav").css('margin-top', "-3px");
                            $("#header_outer").css("padding", "30px 0px");
                            $(".mtb50").css("margin", "50px 0px");
                        }
                        if (window.innerWidth < 768) {
                            $('.quote_section').css("margin-left", "0px");
                        }
                        else {
                            $('.quote_section').css("margin-left", "35px");
                        }
                    }
                    $('#header_outer > div.container').css("padding", "0px");

                });

            </script>
            <script>
                wow = new WOW(
                        {
                            animateClass: 'animated',
                    offset: 100
                }
                        );
                wow.init();
                //document.getElementById('').onclick = function() {
                //  var section = document.createElement('section');
                //  section.className = 'wow fadeInDown';
                //  section.className = 'wow shake';
                //  section.className = 'wow zoomIn';
                //  section.className = 'wow lightSpeedIn';
                //  this.parentNode.insertBefore(section, this);
                //};
            </script>
            <script type="text/javascript">
                $(window).load(function () {

                    $('a').bind('click', function (event) {
                        var $anchor = $(this);

                        $('html, body').stop().animate({
                            scrollTop: $($anchor.attr('href')).offset().top - 91
                        }, 1500, 'easeInOutExpo');
                        /*
                        if you don't want to use the easing effects:
                        $('html, body').stop().animate({
                            scrollTop: $($anchor.attr('href')).offset().top
                        }, 1000);
                         */
                        event.preventDefault();
                        $(".selectedMenu").removeClass("selectedMenu");
                        $(this).addClass("selectedMenu");
                    });
                })
            </script>
            
            <script type="text/javascript">


                jQuery(document).ready(function ($) {
                    // Portfolio Isotope
                    var container = $('#portfolio-wrap');


                    container.isotope({
                        animationEngine: 'best-available',
                        animationOptions: {
                            duration: 200,
                            queue: false
                        },
                        layoutMode: 'fitRows'
                    });

                    $('#filters a').click(function () {
                        $('#filters a').removeClass('active');
                        $(this).addClass('active');
                        var selector = $(this).attr('data-filter');
                        container.isotope({ filter: selector });
                        setProjects();
                        return false;
                    });


                    function splitColumns() {
                        var winWidth = $(window).width(),
                        columnNumb = 1;


                        if (winWidth > 1024) {
                            columnNumb = 4;
                        } else if (winWidth > 900) {
                            columnNumb = 2;
                        } else if (winWidth > 479) {
                            columnNumb = 2;
                        } else if (winWidth < 479) {
                            columnNumb = 1;
                        }

                        return columnNumb;
                    }

                    function setColumns() {
                        var winWidth = $(window).width(),
                        columnNumb = splitColumns(),
                        postWidth = Math.floor(winWidth / columnNumb);

                        container.find('.portfolio-item').each(function () {
                            $(this).css({
                                width: postWidth + 'px'
                            });
                        });
                    }

                    function setProjects() {
                        setColumns();
                        container.isotope('reLayout');
                    }

                    container.imagesLoaded(function () {
                        setColumns();
                    });


                    $(window).bind('resize', function () {
                        setProjects();
                    });

                });
                $(window).load(function () {
                    jQuery('#all').click();
                    return false;
                });
            </script>
        </div>
    </body>
</html>
