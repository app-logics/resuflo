<?php /* $Id: Show.tpl 3814 2007-12-06 17:54:28Z brian $ */ ?>
<?php if ($this->isPopup): ?>
    <?php TemplateUtility::printHeader('Pipeline - '.$this->data['title'], array('js/sorttable.js', 'js/match.js', 'js/pipeline.js', 'js/attachment.js')); ?>
<?php else: ?>
    <?php TemplateUtility::printHeader('Pipeline - '.$this->data['title'], array( 'js/sorttable.js', 'js/match.js', 'js/pipeline.js', 'js/attachment.js')); ?>
    <?php TemplateUtility::printHeaderBlock(); ?>
    <?php TemplateUtility::printTabs($this->active); ?>
        <div id="main">
            <?php TemplateUtility::printQuickSearch(); ?>
<?php endif; ?>

        <div id="contents">
            <table>
                <tr>
                    <td width="3%">
                        <img src="images/job_orders.gif" width="24" height="24" border="0" alt="Pipeline" style="margin-top: 3px;" />&nbsp;
                    </td>
                    <td><h2>Pipeline: Pipeline Details</h2></td>
                </tr>
            </table>

            <p class="note">Pipeline Details</p>

            <?php if ($this->data['isAdminHidden'] == 1): ?>
                <p class="warning">This Pipeline is hidden.  Only RESUFLO Administrators can view it or search for it.  To make it visible by the site users, click <a href="<?php echo(RESUFLOUtility::getIndexName()); ?>?m=joborders&amp;a=administrativeHideShow&amp;jobOrderID=<?php echo($this->jobOrderID); ?>&amp;state=0" style="font-weight:bold;">Here.</a></p>
            <?php endif; ?>

            <?php if (isset($this->frozen)): ?>
                <table style="font-weight:bold; border: 1px solid #000; background-color: #ffed1a; padding:5px; margin-bottom:7px;" width="<?php /* if ($this->isModal): */ if(false): ?>100%<?php else: ?>925<?php endif; ?>" id="candidateAlreadyInSystemTable">
                    <tr>
                        <td class="tdVertical" style="width:925px;">
                            This Pipeline is <?php $this->_($this->data['status']); ?> and can not be modified.
                           <?php if ($this->accessLevel >= ACCESS_LEVEL_EDIT): ?>
                               <a id="edit_link" href="<?php echo(RESUFLOUtility::getIndexName()); ?>?m=joborders&amp;a=edit&amp;jobOrderID=<?php echo($this->jobOrderID); ?>">
                                   <img src="images/actions/edit.gif" width="16" height="16" class="absmiddle" alt="edit" border="0" />&nbsp;Edit
                               </a>
                               the Pipeline to make it Active.&nbsp;&nbsp;
                           <?php endif; ?>
                        </td>
                    </tr>
                </table>
            <?php endif; ?>

            <table class="detailsOutside" width="925" height="<?php echo((count($this->extraFieldRS)/2 + 12) * 22); ?>">
                <tr style="vertical-align:top;">
                    <td width="50%" height="100%">
                        <table class="detailsInside" height="100%">
                            <tr>
                                <td class="vertical">Title:</td>
                                <td class="data" width="300">
                                    <span class="<?php echo($this->data['titleClass']); ?>"><?php $this->_($this->data['title']); ?></span>
                                    <?php echo($this->data['public']) ?>
                                    <?php TemplateUtility::printSingleQuickActionMenu(DATA_ITEM_JOBORDER, $this->data['jobOrderID']); ?>
                                </td>
                            </tr>

                            <tr>
                                <td class="vertical">Company Name:</td>
                                <td class="data">
                                    <a href="<?php echo(RESUFLOUtility::getIndexName()); ?>?m=companies&amp;a=show&amp;companyID=<?php echo($this->data['companyID']); ?>">
                                        <?php echo($this->data['companyName']); ?>
                                    </a>
                                </td>
                            </tr>

                            <tr>
                                <td class="vertical">Department:</td>
                                <td class="data">
                                    <?php echo($this->data['department']); ?>
                                </td>
                            </tr>

                            <tr>
                                <td class="vertical">RESUFLO Job ID:</td>
                                <td class="data" width="300"><?php $this->_($this->data['jobOrderID']); ?></td>
                            </tr>

                            <tr>
                                <td class="vertical">Company Job ID:</td>
                                <td class="data"><?php echo($this->data['companyJobID']); ?></td>
                            </tr>

                            <!-- CONTACT INFO -->
                            <tr>
                                <td class="vertical">Contact Name:</td>
                                <td class="data">
                                    <a href="<?php echo(RESUFLOUtility::getIndexName()); ?>?m=contacts&amp;a=show&amp;contactID=<?php echo($this->data['contactID']); ?>">
                                        <?php echo($this->data['contactFullName']); ?>
                                    </a>
                                </td>
                            </tr>

                            <tr>
                                <td class="vertical">Contact Phone:</td>
                                <td class="data"><?php echo($this->data['contactWorkPhone']); ?></td>
                            </tr>

                            <tr>
                                <td class="vertical">Contact Email:</td>
                                <td class="data">
                                    <a href="mailto:<?php $this->_($this->data['contactEmail']); ?>"><?php $this->_($this->data['contactEmail']); ?></a>
                                </td>
                            </tr>
                            <!-- /CONTACT INFO -->

                            <tr>
                                <td class="vertical">Location:</td>
                                <td class="data"><?php $this->_($this->data['cityAndState']); ?></td>
                            </tr>

                            <tr>
                                <td class="vertical">Max Rate:</td>
                                <td class="data"><?php $this->_($this->data['maxRate']); ?></td>
                            </tr>

                            <tr>
                                <td class="vertical">Salary:</td>
                                <td class="data"><?php $this->_($this->data['salary']); ?></td>
                            </tr>

                            <tr>
                                <td class="vertical">Start Date:</td>
                                <td class="data"><?php $this->_($this->data['startDate']); ?></td>
                            </tr>

                            <?php for ($i = 0; $i < intval(count($this->extraFieldRS)/2); $i++): ?>
                               <tr>
                                    <td class="vertical"><?php $this->_($this->extraFieldRS[$i]['fieldName']); ?>:</td>
                                    <td class="data"><?php echo($this->extraFieldRS[$i]['display']); ?></td>
                              </tr>
                            <?php endfor; ?>

                            <?php eval(Hooks::get('JO_TEMPLATE_SHOW_BOTTOM_OF_LEFT')); ?>

                        </table>
                    </td>

                    <td width="50%" height="100%" style="vertical-align:top;" >
                        <table class="detailsInside" height="100%">
                            <tr>
                                <td class="vertical">Duration:</td>
                                <td class="data"><?php $this->_($this->data['duration']); ?></td>
                            </tr>

                            <tr>
                                <td class="vertical">Openings:</td>
                                <td class="data"><?php $this->_($this->data['openings']); if ($this->data['openingsAvailable'] != $this->data['openings']): ?> (<?php $this->_($this->data['openingsAvailable']); ?> Available)<?php endif; ?></td>
                            </tr>

                            <tr>
                                <td class="vertical">Type:</td>
                                <td class="data"><?php $this->_($this->data['typeDescription']); ?></td>
                            </tr>

                            <tr>
                                <td class="vertical">Status:</td>
                                <td class="data"><?php $this->_($this->data['status']); ?></td>
                            </tr>

                            <tr>
                                <td class="vertical">Pipeline:</td>
                                <td class="data"><?php $this->_($this->data['pipeline']) ?></td>
                            </tr>

                            <tr>
                                <td class="vertical">Submitted:</td>
                                <td class="data"><?php $this->_($this->data['submitted']) ?></td>
                            </tr>

                            <tr>
                                <td class="vertical">Days Old:</td>
                                <td class="data"><?php $this->_($this->data['daysOld']); ?></td>
                            </tr>

                            <tr>
                                <td class="vertical">Created:</td>
                                <td class="data"><?php $this->_($this->data['dateCreated']); ?> (<?php $this->_($this->data['enteredByFullName']); ?>)</td>
                            </tr>

                            <tr>
                                <td class="vertical">Recruiter:</td>
                                <td class="data"><?php $this->_($this->data['recruiterFullName']); ?></td>
                            </tr>

                            <tr>
                                <td class="vertical">Owner:</td>
                                <td class="data"><?php $this->_($this->data['ownerFullName']); ?></td>
                            </tr>

                            <?php for ($i = (intval(count($this->extraFieldRS))/2); $i < (count($this->extraFieldRS)); $i++): ?>
                                <tr>
                                    <td class="vertical"><?php $this->_($this->extraFieldRS[$i]['fieldName']); ?>:</td>
                                    <td class="data"><?php echo($this->extraFieldRS[$i]['display']); ?></td>
                                </tr>
                            <?php endfor; ?>

                            <?php eval(Hooks::get('JO_TEMPLATE_SHOW_BOTTOM_OF_RIGHT')); ?>
                        </table>
                    </td>
                </tr>
            </table>
            <?php if ($this->isPublic): ?>
            <div style="background-color: #E6EEFE; padding: 10px; margin: 5px 0 12px 0; border: 1px solid #728CC8;">
                <b>This Pipeline is public<?php if ($this->careerPortalURL === false): ?>.</b><?php else: ?>
                    and will be shown on your
                    <?php if ($_SESSION['RESUFLO']->getAccessLevel() >= ACCESS_LEVEL_SA): ?>
                    <a style="font-weight: bold;" target="_blank" href="<?php $this->_($this->careerPortalURL); ?>">Careers Website</a>.
                    <?php else: ?>
                        Careers Website.
                    <?php endif; ?></b>
                <?php endif; ?>

                <?php if ($this->questionnaireID !== false): ?>
                    <br />Applicants must complete the "<i><?php echo $this->questionnaireData['title']; ?></i>" (<a href="<?php echo RESUFLOUtility::getIndexName(); ?>?m=settings&a=careerPortalQuestionnaire&questionnaireID=<?php echo $this->questionnaireID; ?>">edit</a>) questionnaire when applying.
                <?php else: ?>
                    <br />You have not attached any
                    <?php if ($_SESSION['RESUFLO']->getAccessLevel() >= ACCESS_LEVEL_SA): ?>
                        <a href="<?php echo RESUFLOUtility::getIndexName(); ?>?m=settings&a=careerPortalSettings">Questionnaires</a>.
                    <?php else: ?>
                        Questionnaires.
                    <?php endif; ?>
                <?php endif; ?>
            </div>
            <?php endif; ?>

            <table class="detailsOutside" width="925">
                <tr>
                    <td>
                        <table class="detailsInside">
                            <tr>
                                <td valign="top" class="vertical">Attachments:</td>
                                <td valign="top" class="data">
                                    <table class="attachmentsTable">
                                        <?php foreach ($this->attachmentsRS as $rowNumber => $attachmentsData): ?>
                                            <tr>
                                                <td>
                                                    <?php echo $attachmentsData['retrievalLink']; ?>
                                                        <img src="<?php $this->_($attachmentsData['attachmentIcon']) ?>" alt="" width="16" height="16" border="0" />
                                                        &nbsp;
                                                        <?php $this->_($attachmentsData['originalFilename']) ?>
                                                    </a>
                                                </td>
                                                <td><?php $this->_($attachmentsData['dateCreated']) ?></td>
                                                <td>
                                                    <?php if (!$this->isPopup): ?>
                                                        <?php if ($this->accessLevel >= ACCESS_LEVEL_DELETE): ?>
                                                            <a href="<?php echo(RESUFLOUtility::getIndexName()); ?>?m=joborders&amp;a=deleteAttachment&amp;jobOrderID=<?php echo($this->jobOrderID); ?>&amp;attachmentID=<?php $this->_($attachmentsData['attachmentID']) ?>"  title="Delete" onclick="javascript:return confirm('Delete this attachment?');">
                                                                <img src="images/actions/delete.gif" alt="" width="16" height="16" border="0" />
                                                            </a>
                                                        <?php endif; ?>
                                                    <?php endif; ?>
                                                </td>
                                            </tr>
                                        <?php endforeach; ?>
                                    </table>
                                    <?php if (!$this->isPopup): ?>
                                        <?php if ($this->accessLevel >= ACCESS_LEVEL_EDIT): ?>
                                            <?php if (isset($this->attachmentLinkHTML)): ?>
                                                <?php echo($this->attachmentLinkHTML); ?>
                                            <?php else: ?>
                                                <a href="#" onclick="showPopWin('<?php echo(RESUFLOUtility::getIndexName()); ?>?m=joborders&amp;a=createAttachment&amp;jobOrderID=<?php echo($this->jobOrderID); ?>', 400, 125, null); return false;">
                                            <?php endif; ?>
                                                <img src="images/paperclip_add.gif" width="16" height="16" border="0" alt="add attachment" class="absmiddle" />&nbsp;Add Attachment
                                            </a>
                                        <?php endif; ?>
                                    <?php endif; ?>
                                </td>
                            </tr>

                            <tr>
                                <td valign="top" class="vertical">Description:</td>

                                <td class="data" colspan="2">
                                    <?php if($this->data['description'] != ''): ?>
                                    <div id="shortDescription" style="overflow: auto; height:170px; border: #AAA 1px solid; padding:5px;">
                                        <?php echo($this->data['description']); ?>
                                    </div>
                                    <?php endif; ?>
                                </td>

                            </tr>

                            <tr>
                                <td valign="top" class="vertical">Internal Notes:</td>

                                <td class="data" style="width:320px;">
                                    <?php if($this->data['notes'] != ''): ?>
                                        <div id="shortDescription" style="overflow: auto; height:240px; border: #AAA 1px solid; padding:5px;">
                                            <?php echo($this->data['notes']); ?>
                                        </div>
                                    <?php endif; ?>
                                </td>
                                
                                
                                
                            </tr>
                        </table>
                    </td>
                </tr>
            </table>
			<table class="detailsOutside" width="925">
                <tr>
                    <td>
                        <table class="detailsInside">
                            <tr>
                                <td valign="top" class="vertical">Graphical View:</td>
                            </tr>
							<tr>
                               <td style="vertical-align:top;"><?php echo($this->pipelineGraph);  ?> </td>
                                
                            </tr>
                        </table>
                    </td>
                </tr>
            </table>
			
<?php if (!$this->isPopup): ?>
            <div id="actionbar">
                <span style="float:left;">
                    <?php if ($this->accessLevel >= ACCESS_LEVEL_EDIT): ?>
                        <a id="edit_link" href="<?php echo(RESUFLOUtility::getIndexName()); ?>?m=joborders&amp;a=edit&amp;jobOrderID=<?php echo($this->jobOrderID); ?>">
                            <img src="images/actions/edit.gif" width="16" height="16" class="absmiddle" alt="edit" border="0" />&nbsp;Edit
                        </a>
                        &nbsp;&nbsp;&nbsp;&nbsp;
                    <?php endif; ?>
                    <?php if ($this->accessLevel >= ACCESS_LEVEL_DELETE): ?>
                        <a id="delete_link" href="<?php echo(RESUFLOUtility::getIndexName()); ?>?m=joborders&amp;a=delete&amp;jobOrderID=<?php echo($this->jobOrderID); ?>" onclick="javascript:return confirm('Delete this Pipeline?');">
                            <img src="images/actions/delete.gif" width="16" height="16" class="absmiddle" alt="delete" border="0" />&nbsp;Delete
                        </a>
                        &nbsp;&nbsp;&nbsp;&nbsp;
                    <?php endif; ?>
                    <?php if ($this->accessLevel >= ACCESS_LEVEL_MULTI_SA): ?>
                        <?php if ($this->data['isAdminHidden'] == 1): ?>
                            <a href="<?php echo(RESUFLOUtility::getIndexName()); ?>?m=joborders&amp;a=administrativeHideShow&amp;jobOrderID=<?php echo($this->jobOrderID); ?>&amp;state=0">
                                <img src="images/resume_preview_inline.gif" width="16" height="16" class="absmiddle" alt="delete" border="0" />&nbsp;Administrative Show
                            </a>
                            <?php else: ?>
                            <a href="<?php echo(RESUFLOUtility::getIndexName()); ?>?m=joborders&amp;a=administrativeHideShow&amp;jobOrderID=<?php echo($this->jobOrderID); ?>&amp;state=1">
                                <img src="images/resume_preview_inline.gif" width="16" height="16" class="absmiddle" alt="delete" border="0" />&nbsp;Administrative Hide
                            </a>
                        <?php endif; ?>
                        &nbsp;&nbsp;&nbsp;&nbsp;
                    <?php endif; ?>
                </span>
                <span style="float:right;">
                    <?php if (!empty($this->data['public']) && $this->careerPortalEnabled): ?>
                        <a id="public_link" href="<?php echo(RESUFLOUtility::getAbsoluteURI()); ?>careers/<?php echo(RESUFLOUtility::getIndexName()); ?>?p=showJob&amp;ID=<?php echo($this->jobOrderID); ?>">
                            <img src="images/public.gif" width="16" height="16" class="absmiddle" alt="Online Application" border="0" />&nbsp;Online Application
                        </a>
                        &nbsp;&nbsp;&nbsp;&nbsp;
                    <?php endif; ?>
                    <?php /* TODO: Make report available for every site. */ ?>
                    <a id="report_link" href="<?php echo(RESUFLOUtility::getIndexName()); ?>?m=reports&amp;a=customizeJobOrderReportFirst&amp;jobOrderID=<?php echo($this->jobOrderID); ?>&amp;recId=<?php echo $_SESSION['RESUFLO']->getUserID(); ?>">
                        <img src="images/reportsSmall.gif" width="16" height="16" class="absmiddle" alt="report" border="0" />&nbsp;Generate Report
                    </a>
                    <?php if ($this->privledgedUser): ?>
                        &nbsp;&nbsp;&nbsp;&nbsp;
                        <a id="history_link" href="<?php echo(RESUFLOUtility::getIndexName()); ?>?m=settings&amp;a=viewItemHistory&amp;dataItemType=400&amp;dataItemID=<?php echo($this->jobOrderID); ?>">
                            <img src="images/icon_clock.gif" width="16" height="16" class="absmiddle"  border="0" />&nbsp;View History
                        </a>
                    <?php endif; ?>
                    <?php if ($this->isTextMessageEnable):?>
                        <a target="_blank" href="<?php echo(RESUFLOUtility::getIndexName()); ?>?m=candidates&amp;a=sms&amp;candidateID=<?php echo($this->candidateID); ?>">
                            <img src="images/sms.png" width="16" height="16" class="absmiddle"  border="0" />&nbsp;View Text Messages
                        </a>
                    <?php endif; ?>
                </span>
            </div>
<?php endif; ?>
            <br clear="all" />
            <br />
			
            <p class="note">Candidate Pipeline 
            </p>

<table class="viewSelector">
<tbody><tr>
<td valign="top" nowrap="nowrap" align="right">
<input type="checkbox" id="dead" name="showDead" onclick="PipelineJobOrder_populate(<?php $this->_($this->data['jobOrderID']); ?>, 0, <?php $this->_($this->pipelineEntriesPerPage); ?>, 'dateCreatedInt', 'desc', <?php if ($this->isPopup) echo(1); else echo(0); ?>, 'ajaxPipelineTable', '<?php echo($this->sessionCookie); ?>', 'ajaxPipelineTableIndicator', '<?php echo(RESUFLOUtility::getIndexName()); ?>',  document.getElementById('dead').checked, document.getElementById('future').checked);">
<label for="showDead">Show Dead</label>&nbsp;
</td>
<td valign="top" nowrap="nowrap" align="right">
<input type="checkbox" id="future" name="showFuture" onclick="PipelineJobOrder_populate(<?php $this->_($this->data['jobOrderID']); ?>, 0, <?php $this->_($this->pipelineEntriesPerPage); ?>, 'dateCreatedInt', 'desc', <?php if ($this->isPopup) echo(1); else echo(0); ?>, 'ajaxPipelineTable', '<?php echo($this->sessionCookie); ?>', 'ajaxPipelineTableIndicator', '<?php echo(RESUFLOUtility::getIndexName()); ?>',  document.getElementById('dead').checked, document.getElementById('future').checked);">
<label for="showFuture">Show Future</label>&nbsp;
</td>
</tr>
</tbody></table>


            <p id="ajaxPipelineControl">
                Number of visible entries:&nbsp;&nbsp;
                <select id="numberOfEntriesSelect" onchange="PipelineJobOrder_changeLimit(<?php $this->_($this->data['jobOrderID']); ?>, this.value, <?php if ($this->isPopup) echo(1); else echo(0); ?>, 'ajaxPipelineTable', '<?php echo($this->sessionCookie); ?>', 'ajaxPipelineTableIndicator', '<?php echo(RESUFLOUtility::getIndexName()); ?>', document.getElementById('dead').checked, document.getElementById('future').checked);" class="selectBox">
                    <option value="15" <?php if ($this->pipelineEntriesPerPage == 15): ?>selected<?php endif; ?>>15 entries</option>
                    <option value="30" <?php if ($this->pipelineEntriesPerPage == 30): ?>selected<?php endif; ?>>30 entries</option>
                    <option value="50" <?php if ($this->pipelineEntriesPerPage == 50): ?>selected<?php endif; ?>>50 entries</option>
                    <option value="99999" <?php if ($this->pipelineEntriesPerPage == 99999): ?>selected<?php endif; ?>>All entries</option>
                </select>&nbsp;
                <span id="ajaxPipelineNavigation">
                </span>&nbsp;
                <img src="images/indicator.gif" alt="" id="ajaxPipelineTableIndicator" />
            </p>

            <div id="ajaxPipelineTable">
            </div>
            <script type="text/javascript">
                PipelineJobOrder_populate(<?php $this->_($this->data['jobOrderID']); ?>, 0, <?php $this->_($this->pipelineEntriesPerPage); ?>, 'dateCreatedInt', 'desc', <?php if ($this->isPopup) echo(1); else echo(0); ?>, 'ajaxPipelineTable', '<?php echo($this->sessionCookie); ?>', 'ajaxPipelineTableIndicator', '<?php echo(RESUFLOUtility::getIndexName()); ?>',  document.getElementById('dead').checked, document.getElementById('future').checked);
            </script>
            
            <input type="checkbox" id="allBox" name="allBox" title="Select All" onclick="toggleChecksAllDataGrid(exportArray<?php echo md5($this->sessionCookie) ?>, this.checked);" />&nbsp;&nbsp;&nbsp;
            <a href="javascript:void(0);" onclick="toggleHideShowAction('<?php echo md5($this->sessionCookie) ?>');">Action<img src="images/downward.gif" style="border: none;" alt="" /></a>&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;
            <div class="ajaxSearchResults" id="ActionArea<?php echo md5($this->sessionCookie) ?>" align="left" onclick="toggleHideShowAction('<?php echo md5($this->sessionCookie) ?>')" style="width:270px;">
                <?php $additionalParams = 'i=candidates%3AcandidatesListByViewDataGrid&p=a%3A7%3A%7Bs%3A6%3A%22sortBy%22%3Bs%3A15%3A%22dateCreatedSort%22%3Bs%3A13%3A%22sortDirection%22%3Bs%3A4%3A%22DESC%22%3Bs%3A10%3A%22rangeStart%22%3Bi%3A0%3Bs%3A10%3A%22maxResults%22%3Bi%3A100000000%3Bs%3A11%3A%22savedfilter%22%3Ba%3A0%3A%7B%7Ds%3A9%3A%22exportIDs%22%3Bs%3A9%3A%22<dynamic>%22%3Bs%3A16%3A%22noSaveParameters%22%3Bb%3A1%3B%7D'; ?>
                <!--
                <div>
                    <div style="float:left; width:170px;">Add To Campaign</div><div style="float:right; width:95px;">
                        <a href="javascript:void(0);" onclick="if (exportArray<?php echo md5($this->sessionCookie) ?>.length>0) showPopWin('index.php?m=lists&amp;a=addToCampaignFromDatagridModal&amp;dataItemType=100&amp;<?php echo $additionalParams ?>&amp;dynamicArgument=' + urlEncode(serializeArray(exportArray<?php echo md5($this->sessionCookie) ?>)), 450, 350, null); else dataGridNoSelected();">Selected</a>&nbsp;|&nbsp;
                        <a href="javascript:void(0);" onclick="toggleChecksAllDataGrid(exportArray<?php echo md5($this->sessionCookie) ?>, true); if (exportArray<?php echo md5($this->sessionCookie) ?>.length>0) showPopWin('index.php?m=lists&amp;a=addToCampaignFromDatagridModal&amp;dataItemType=100&amp;<?php echo $additionalParams ?>&amp;dynamicArgument=' + urlEncode(serializeArray(exportArray<?php echo md5($this->sessionCookie) ?>)), 450, 350, null);">All</a>
                    </div>
                </div>
                -->
                <?php $string = 'candidates:candidatesListByViewDataGrid'; ?>
                <div>
                    <div style="float:left; width:170px;">Add To List</div><div style="float:right; width:95px;">
                        <a href="javascript:void(0);" onclick="if (exportArray<?php echo md5($this->sessionCookie) ?>.length>0) showPopWin('index.php?m=lists&amp;a=addToListFromPipelineDatagridModal&amp;dataItemType=100&amp;dynamicArgument=' + urlEncode(serializeArray(exportArray<?php echo md5($this->sessionCookie) ?>)), 450, 350, null); else dataGridNoSelected();">Selected</a>&nbsp;|&nbsp;
                        <a href="javascript:void(0);" onclick="toggleChecksAllDataGrid(exportArray<?php echo md5($this->sessionCookie) ?>, true); if (exportArray<?php echo md5($this->sessionCookie) ?>.length>0) showPopWin('index.php?m=lists&amp;a=addToListFromPipelineDatagridModal&amp;dataItemType=100&amp;dynamicArgument=' + urlEncode(serializeArray(exportArray<?php echo md5($this->sessionCookie) ?>)), 450, 350, null);">All</a>
                    </div>
                </div>
                <div>
                    <div style="float:left; width:170px;">Add To Pipeline</div><div style="float:right; width:95px;">
                        <a href="javascript:void(0);" onclick="if (exportArray<?php echo md5($this->sessionCookie) ?>.length>0) showPopWin('index.php?m=candidates&amp;a=considerForJobSearch&amp;delrec=PipeLine&amp;<?php echo $additionalParams ?>&amp;dynamicArgument<?php echo md5($string);?>=' + urlEncode(serializeArray(exportArray<?php echo md5($this->sessionCookie) ?>)), 650, 350, null); else dataGridNoSelected();">Selected</a>&nbsp;|&nbsp;
                        <a href="javascript:void(0);" onclick="toggleChecksAllDataGrid(exportArray<?php echo md5($this->sessionCookie) ?>, true); if (exportArray<?php echo md5($this->sessionCookie) ?>.length>0) showPopWin('index.php?m=candidates&amp;a=considerForJobSearch&amp;delrec=PipeLine&amp;<?php echo $additionalParams ?>&amp;dynamicArgument<?php echo md5($string);?>=' + urlEncode(serializeArray(exportArray<?php echo md5($this->sessionCookie) ?>)), 450, 350, null);">All</a>
                    </div>
                </div>
                <div>
                    <div style="float:left; width:170px;">Send E-Mail</div><div style="float:right; width:95px;">
                        <a href="javascript:void(0);" onclick="if (exportArray<?php echo md5($this->sessionCookie) ?>.length>0){ window.open('index.php?m=candidates&amp;a=emailCandidates&delrec=PipeLine&<?php echo $additionalParams ?>&amp;dynamicArgument<?php echo md5($string);?>=' + urlEncode(serializeArray(exportArray<?php echo md5($this->sessionCookie); ?>)),'_blank');} else {dataGridNoSelected();}">Selected</a>&nbsp;|&nbsp;
                        <a href="javascript:void(0);" onclick="toggleChecksAllDataGrid(exportArray<?php echo md5($this->sessionCookie) ?>, true); if (exportArray<?php echo md5($this->sessionCookie) ?>.length>0){ window.open('index.php?m=candidates&amp;a=emailCandidates&delrec=PipeLine&<?php echo $additionalParams ?>&dynamicArgument<?php echo md5($string);?>=' + urlEncode(serializeArray(exportArray<?php echo md5($this->sessionCookie); ?>)),'_blank');}">All</a>
                    </div>
                </div>
                <div>
                    <div style="float:left; width:170px;">Delete All</div><div style="float:right; width:95px;">
                        <a href="javascript:void(0);" onclick="if (exportArray<?php echo md5($this->sessionCookie) ?>.length>0){ var result=confirm('Delete the selected records?'); if (result==false) { return false;} window.location.href='index.php?m=candidates&amp;a=delete_Candidates&amp;delrec=PipeLine&amp;<?php echo $additionalParams ?>&amp;dynamicArgument<?php echo md5($string);?>=' + urlEncode(serializeArray(exportArray<?php echo md5($this->sessionCookie); ?>))} else dataGridNoSelected();">Selected</a>&nbsp;|&nbsp;
                        <a href="javascript:void(0);" onclick="toggleChecksAllDataGrid(exportArray<?php echo md5($this->sessionCookie) ?>, true); if (exportArray<?php echo md5($this->sessionCookie) ?>.length>0){ var result=confirm('Delete the selected records?'); if (result==false) { return false;} window.location.href='index.php?m=candidates&amp;a=delete_Candidates&amp;delrec=PipeLine&amp;<?php echo $additionalParams ?>&amp;dynamicArgument<?php echo md5($string);?>=' + urlEncode(serializeArray(exportArray<?php echo md5($this->sessionCookie) ?>))}">All</a>
                    </div>
                </div>
                <div>
                    <div style="float:left; width:170px;">Export</div><div style="float:right; width:95px;">
                        <a href="javascript:void(0);" onclick="if (exportArray<?php echo md5($this->sessionCookie) ?>.length>0){ window.location.href='index.php?m=export&amp;a=exportByDataGrid&amp;delrec=PipeLine&amp;<?php echo $additionalParams ?>&amp;dynamicArgument<?php echo md5($string);?>=' + urlEncode(serializeArray(exportArray<?php echo md5($this->sessionCookie) ?>))} else dataGridNoSelected();">Selected</a>&nbsp;|&nbsp;
                        <a href="javascript:void(0);" onclick="toggleChecksAllDataGrid(exportArray<?php echo md5($this->sessionCookie) ?>, true); if (exportArray<?php echo md5($this->sessionCookie) ?>.length>0){ window.location.href='index.php?m=export&amp;a=exportByDataGrid&amp;delrec=PipeLine&amp;<?php echo $additionalParams ?>&amp;dynamicArgument<?php echo md5($string);?>=' + urlEncode(serializeArray(exportArray<?php echo md5($this->sessionCookie) ?>))}">All</a>
                    </div>
                </div>
            </div>
		
<?php if (!$this->isPopup): ?>
            <?php if ($this->accessLevel >= ACCESS_LEVEL_EDIT && !isset($this->frozen)): ?>
                <a href="#" onclick="showPopWin('<?php echo(RESUFLOUtility::getIndexName()); ?>?m=joborders&amp;a=considerCandidateSearch&amp;jobOrderID=<?php echo($this->jobOrderID); ?>', 820, 550, null); return false;">
                    <img src="images/consider.gif" width="16" height="16" class="absmiddle" alt="add candidate" border="0" />&nbsp;Add Candidate to This Job Order Pipeline
                </a>
            <?php endif; ?>
        </div>
    </div>
    
<?php endif; ?>
    <div id="bottomShadow"></div>
<?php TemplateUtility::printFooter(); ?>
