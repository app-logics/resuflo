<?php
/*
 * RESUFLO
 * Admin Find-A-Candiddate-Via-Email Module
 *
 * Copyright (C) 2014 Jeff Edwards @ Recruiters-Edge.com 
 *
 * jj did this one 2014-11-18
 *
 */

class CandidateviaemailurlUI extends UserInterface
{
    public function __construct()
    {
        parent::__construct();
        $this->_db = DatabaseConnection::getInstance();
        $this->basehref = '/?m=candidates&a=show&view=1&candidateID=';
    }

    private function _getEmail()
    {
        if (is_valid_email_address($_SERVER['REQUEST_URI']))
        {
            $email = substr($_SERVER['REQUEST_URI'], 1);
            return $email;
        }
    }

    public function handleRequest()
    {
        $email = $this->_getEmail();
        $sql = 'select candidate_id from candidate where "'.mysql_real_escape_string($email).'" in (email1, email2)';
        $users = $this->_db->getAllAssoc($sql);
        if (empty($users)) die('user with email '.$email.' not found :(');
        if (count($users) > 1)
        {
            $links = '';
            foreach ($users as &$u)
            {
                $links .= '<br><a href="'.$this->basehref.$u['candidate_id'].'" target="_blank">'.$u['candidate_id'].'</a><br>';
            }
            die('uh oh...jj only built this to find a single user...here are some links for your use:<br>'.$links);
        }
        else
        {
            header('Location: '.$this->basehref.$users[0]['candidate_id']);
            die();
        }
    }
}
