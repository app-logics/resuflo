<?php /* $Id: Home.tpl 3563 2007-11-12 07:41:54Z will $ */ ?>
<?php TemplateUtility::printHeader('Home', array('js/sweetTitles.js', 'js/dataGrid.js', 'js/home.js')); ?>
<link href="./google calender/main.css" rel="stylesheet" type="text/css" />
<style>
    body{
        background-color: #E9EBEC;
        background: #E9EBEC;
    }
    .txtbox_small{
        width: 150px;
        margin-right: 2px;
    }
    .hide{
        display: none;
    }
    #main{
        border: none;
        background: none;
        padding-top: 0px;
    }
    #contents{
        padding: 0px;
    }
    .footerBlock{
        margin-top: 0;
        width: auto;
        padding-bottom: 100px;
    }
</style>
<script>
    $("body").css({backgroundColor: '#E9EBEC'});
</script> 
<div id="headerBlock">
    <table cellspacing="0" cellpadding="0" style="margin: 0px; padding: 0px; float: left;">
        <tbody><tr>
                <td rowspan="2"><img src="<?php echo($this->logo); ?>" border="0" height="45" alt="RESUFLO"></td>
            </tr>
        </tbody>
    </table>
</div>
<div id="main" style="min-height: auto; margin-top: 5px; background-color: #E9EBEC;">
    
    <div id="contents" style="padding-top: 10px; min-height: auto; background-color: #E9EBEC;">
        <form name="jobForm" id="jobForm" action="./que/reschedulersub.php" method="post" autocomplete="on">
            <input type="hidden" name="postback" id="postback" value="postback" />
            <input type="hidden" name="scheduledate" id="scheduledate" >
            <input type="hidden" name="scheduleid" id="scheduleid" >
            <input type="hidden" name="location" id="location" >
            <input type="hidden" name="appID" id="siteID" value="<?php echo $_GET['appid']; ?>">
            <input type="hidden" name="userID" id="siteID" value="<?php echo $_GET['cl']; ?>">
            <input type="hidden" name="siteID" id="siteID" value="<?php echo $_GET['siteID']; ?>">
        </form>
        <?php if ($this->jobId > 0){ ?>
        
        <table class="fourthPage" width="950" border="0" align="center" cellpadding="0" cellspacing="0">
            <tr>
                <td style="vertical-align: top; width: 200px;">
                    <div style="padding: 8px; background-color: #C4DCE6; width: 200px;">
                        <span style="font-weight: bolder;">INSTRUCTIONS</span><br>
                        Please follow these simple steps.<br><br>
                        <table>
                            <tr>
                                <td>
                                    <strong>Pick a Date</strong><br>
                                    <span style="">Choose a date to meet with our representatives.</span>
                                </td>
                            </tr>
                        </table>
                    </div>
                </td>
                <td style="padding-left: 10px; vertical-align: top;">
                    <table style="border-collapse: collapse;">
                        <tr>
                            <td colspan="2">
                                <strong>
                                    Pick a Date
                                </strong>
                            </td>
                        </tr>
                        <tr>
                            <td colspan="2">
                                <span>
                                    We'd like to immediately ask you to schedule a meeting with our representatives. Please choose from the open times and dates below.
                                </span>
                            </td>
                        </tr>
                        <tr style="background-color: #E6E5E3;" class="hide">
                            <td colspan="2" style="padding: 10px">
                                <img src="./images/career_page/exlain.png" alt=""/>Based on your contact information, we've determined that you are in the 
                                <strong>
                                    <?php if(isset($this->timezoneValue))
                                    {
                                    if($this->timezoneValue=="Pacific/Wake"){ echo "Pacific/Wake";}
                                    if($this->timezoneValue=="Pacific/Apia"){ echo "Pacific/Apia";} 
                                    if($this->timezoneValue=="Pacific/Honolulu"){ echo "Pacific/Honolulu";} 
                                    if($this->timezoneValue=="America/Anchorage"){ echo "America/Anchorage";} 
                                    if($this->timezoneValue=="America/Los_Angeles"){ echo "America/Los_Angeles";} 
                                    if($this->timezoneValue=="America/Tijuana"){ echo "America/Tijuana";} 
                                    if($this->timezoneValue=="America/Phoenix"){ echo "America/Phoenix";} 
                                    if($this->timezoneValue=="America/Denver"){ echo "America/Denver";}
                                    if($this->timezoneValue=="America/Chihuahua"){ echo "America/Chihuahua";} 
                                    if($this->timezoneValue=="America/Managua"){ echo "America/Managua";} 
                                    if($this->timezoneValue=="America/Chicago"){ echo "America/Chicago";} 
                                    if($this->timezoneValue=="America/Regina"){ echo "America/Regina";} 
                                    if($this->timezoneValue=="America/Mexico_City"){ echo "America/Mexico_City";} 
                                    if($this->timezoneValue=="America/Indiana/Indianapolis"){ echo "America/Indiana/Indianapolis";} 
                                    if($this->timezoneValue=="America/Bogota"){ echo "America/Bogota";} 
                                    if($this->timezoneValue=="America/New_York"){ echo "America/New_York";} 
                                    if($this->timezoneValue=="America/Halifax"){ echo "America/Halifax";} 
                                    if($this->timezoneValue=="America/Caracas"){ echo "America/Caracas";} 
                                    if($this->timezoneValue=="America/Santiago"){ echo "America/Santiago";} 
                                    if($this->timezoneValue=="America/St_Johns"){ echo "America/St_Johns";} 
                                    if($this->timezoneValue=="America/Buenos_Aires"){ echo "America/Buenos_Aires";} 
                                    if($this->timezoneValue=="America/Sao_Paulo"){ echo "America/Sao_Paulo";} 
                                    if($this->timezoneValue=="America/Godthab"){ echo "America/Godthab";} 
                                    if($this->timezoneValue=="America/Noronha"){ echo "America/Godthab";}
                                    }
                                    ?>
                                </strong> 
                                time zone, and we've built the schedule below in your local time.
                            </td>
                        </tr>
                        <tr class="hide">
                            <td colspan="2">
                                <br>
                                <strong>Select your time zone</strong>
                                <select name="TimezoneValue" id="TimezoneValue" class="txtbox" style="width:auto">  
                                    <option value="">Select Timezone</option>
                                    <option value="Pacific/Wake" <?php if(isset($this->timezoneValue)){ if($this->timezoneValue=="Pacific/Wake"){ echo "selected";} } ?>>(GMT-12:00) International Date Line West</option>
                                    <option value="Pacific/Apia" <?php if(isset($this->timezoneValue)){ if($this->timezoneValue=="Pacific/Apia"){ echo "selected";} } ?>>GMT -11:00) Midway Island, Samoa</option>
                                    <option value="Pacific/Honolulu" <?php   if(isset($this->timezoneValue)){ if($this->timezoneValue=="Pacific/Honolulu"){ echo "selected";} } ?>>(GMT -10:00) Hawaii</option>  
                                    <option value="America/Anchorage" <?php if(isset($this->timezoneValue)){ if($this->timezoneValue=="America/Anchorage"){ echo "selected";} } ?>>(GMT -09:00) Alaska</option>  
                                    <option value="America/Los_Angeles" <?php if(isset($this->timezoneValue)){ if($this->timezoneValue=="America/Los_Angeles"){ echo "selected";} } ?>>(GMT-08:00) Pacific Time (US &amp; Canada); Tijuana</option>  
                                    <option value="America/Tijuana" <?php if(isset($this->timezoneValue)){ if($this->timezoneValue=="America/Tijuana"){ echo "selected";} } ?>>(GMT-08:00) Tijuana</option>  
                                    <option value="America/Phoenix" <?php if(isset($this->timezoneValue)){ if($this->timezoneValue=="America/Phoenix"){ echo "selected";} } ?>>(GMT-07:00) Arizona</option>  
                                    <option value="America/Denver" <?php if(isset($this->timezoneValue)){ if($this->timezoneValue=="America/Denver"){ echo "selected";} } ?>>(GMT-07:00) Mountain Time (US &amp; Canada)</option>  
                                    <option value="America/Chihuahua" <?php if(isset($this->timezoneValue)){ if($this->timezoneValue=="America/Chihuahua"){ echo "selected";} } ?>>(GMT-07:00) Chihuahua, La Paz,Mazatlan </option>  
                                    <option value="America/Managua" <?php if(isset($this->timezoneValue)){ if($this->timezoneValue=="America/Managua"){ echo "selected";} } ?>>(GMT-06:00) Central America</option>  
                                    <option value="America/Chicago" <?php if(isset($this->timezoneValue)){ if($this->timezoneValue=="America/Chicago"){ echo "selected";} } ?>>(GMT-06:00) Central Time (US &amp; Canada)</option>  
                                    <option value="America/Regina" <?php if(isset($this->timezoneValue)){ if($this->timezoneValue=="America/Regina"){ echo "selected";} } ?>>(GMT-06:00) Saskatchewan</option>  
                                    <option value="America/Mexico_City" <?php if(isset($this->timezoneValue)){ if($this->timezoneValue=="America/Mexico_City"){ echo "selected";} } ?>>(GMT-06:00) Mexico City, Guadalajara, Monterrey</option>  
                                    <option value="America/Indiana" <?php if(isset($this->timezoneValue)){ if($this->timezoneValue=="America/Indiana/Indianapolis"){ echo "selected";} } ?>>(GMT-05:00) Indiana (East)</option>  
                                    <option value="America/Bogota" <?php if(isset($this->timezoneValue)){ if($this->timezoneValue=="America/Bogota"){ echo "selected";} } ?>>(GMT-05:00) Bogota, Lima, Quito</option>  
                                    <option value="America/New_York" <?php if(isset($this->timezoneValue)){ if($this->timezoneValue=="America/New_York"){ echo "selected";} } ?>>(GMT-05:00) Eastern Time (US &amp; Canada)</option>  
                                    <option value="America/Halifax" <?php if(isset($this->timezoneValue)){ if($this->timezoneValue=="America/Halifax"){ echo "selected";} } ?>>(GMT-04:00) Atlantic Time (Canada)</option>  
                                    <option value="America/Caracas" <?php if(isset($this->timezoneValue)){ if($this->timezoneValue=="America/Caracas"){ echo "selected";} } ?>>(GMT-04:00) Caracas, La Paz</option>  
                                    <option value="America/Santiago" <?php if(isset($this->timezoneValue)){ if($this->timezoneValue=="America/Santiago"){ echo "selected";} } ?>>(GMT-04:00) Santiago</option>  
                                    <option value="America/St_Johns" <?php if(isset($this->timezoneValue)){ if($this->timezoneValue=="America/St_Johns"){ echo "selected";} } ?>>(GMT-03:30) Newfoundland</option>  
                                    <option value="America/Buenos_Aires" <?php if(isset($this->timezoneValue)){ if($this->timezoneValue=="America/Buenos_Aires"){ echo "selected";} } ?>>(GMT-03:00) Buenos Aires, Georgetown</option>  
                                    <option value="America/Sao_Paulo" <?php if(isset($this->timezoneValue)){ if($this->timezoneValue=="America/Sao_Paulo"){ echo "selected";} } ?>>(GMT-03:00) Brasilia</option>  
                                    <option value="America/Godthab" <?php if(isset($this->timezoneValue)){ if($this->timezoneValue=="America/Godthab"){ echo "selected";} } ?>>(GMT-03:00) Greenland</option>  
                                    <option value="America/Noronha" <?php if(isset($this->timezoneValue)){ if($this->timezoneValue=="America/Noronha"){ echo "selected";} } ?>>(GMT-02:00) Mid-Atlantic</option>  
                                </select>
                                <br>
                                <br>
                            </td>
                        </tr>
                        <tr>
                            <td colspan="2" >
                                <div>
                                    <?php echo($this->cal); ?>
                                </div>
                            </td>
                        </tr>
                    </table>
                </td>
            </tr>
            <tr>
                <td colspan="2" style="text-align: center;">
                    <br>
                    <br>
            <center>
                <table>
                    <tr>
                        <td><img src="./images/career_page/back.png" alt="" style="cursor: pointer;" onclick="$('.fourthPage').addClass('hide');$('.thirdPage').removeClass('hide');"/></td><td>Go back</td>
                        <td>&nbsp;&nbsp;&nbsp;&nbsp;Continue</td><td><img src="./images/career_page/forward.png" alt="" style="cursor: pointer;" onclick="validateCalendar()"/></td>
                    </tr>
                </table>
            </center>
            </td>
            </tr>
        </table>
                                
        <table class="fifthPage hide" width="950" border="0" align="center" cellpadding="0" cellspacing="0">
            <tr>
                <td style="vertical-align: top; width: 200px;">
                    <div style="padding: 8px; background-color: #C4DCE6; width: 200px;">
                        <span style="font-weight: bolder;">INSTRUCTIONS</span><br>
                        Please follow these simple steps.<br><br>
                        <table>
                            <tr>
                                <td>
                                    <img src="./images/career_page/tick.png" alt=""/>
                                </td>
                                <td>
                                    <strong>Contact Information</strong>
                                </td>
                            </tr>
                            <tr>
                                <td style="vertical-align: top;">
                                    <img src="./images/career_page/tick.png" alt=""/>
                                </td>
                                <td>
                                    <strong>One Quick Question</strong><br>
                                    <span style="">Answer this quick question to tell us about yourself.</span>
                                </td>
                            </tr>
                            <tr>
                                <td style="vertical-align: top;">
                                    <img src="./images/career_page/tick.png" alt=""/>
                                </td>
                                <td>
                                    <strong>Pick a Date</strong><br>
                                    <span style="">Choose a date to meet with our representatives.</span>
                                </td>
                            </tr>
                        </table>
                    </div>
                </td>
                <td style="padding-left: 10px; vertical-align: top;">
                    <table style="border-collapse: collapse;">
                        <tr>
                            <td colspan="2">
                                <strong>
                                    Success!
                                </strong>
                            </td>
                        </tr>
                        <tr>
                            <td colspan="2">
                                <span>
                                    Thank you for your submitting your information.
                                </span>
                            </td>
                        </tr>
                    </table>
                </td>
            </tr>
        </table>
        <?php }else {?>
            
            No Data Found!

        <?php }?>
        </body>
        </html>
    </div>
</div>
<br>
</div>
<?php if ($this->userID == 1585){ ?>
<div style=" background: #bcbebf url('./images/career_page/pattern-03.png') repeat scroll 0 0; position: absolute; left: 0; width: 100%; height: 183px; ">
    <div class="container">
        <center>
            <div class="footer-columns" style="background: url('./images/career_page/25p.png');width: 950px; text-align: left;">
                <!--<img src="../../images/career_page/unnamed.png" alt=""/>-->
                    <div class="row-fluid footer-div equal" style="background: url('./images/career_page/footer-divider.png') repeat-y 50% 0; height: 183px;">
                    
                    <div style="height: 183px;padding-left: 70px;padding-bottom: 14px;width: 405px;display: inline-block;float: left;" class="span6 footer-contact">
                        <div class="footer-logo" style="padding: 28px 0 25px;">
                            <img src="./images/career_page/Logo-Reversed-Small.png" id="ctl00_FooterUC_ctl00_SiteFooterLogo_1" class="SiteFooterLogo" alt="logo" style="width: 228px;">
                        </div>
                        <address class="footer-address">
                            <p>
                                <span class="address1">134 Rumford Ave</span> <span class="address2">Suite 201</span><span class="address-comma">,</span> <span class="city">Newton,</span> <span class="state">MA</span> <span class="zip">02466</span><br>
                                <span class="footer-phone">Phone <a href="tel:+1617 9165155" style="color: #808080;">(617) 916-5155</a> |</span>
                                    
                                <span class="footer-email"><a href="mailto:jbreedlove@moodystreet.com" style="color: #808080;">jbreedlove@moodystreet.com</a> </span>
                                    
                            </p>
                        </address>
                    </div>
                        
                    <div style="height: 183px;width: 455px;display: inline-block;float: right;padding-left: 20px;padding-bottom: 14px;" class="span6 footer-bd-info">
                        <div class="row-fluid bd-logo" style="padding: 28px 30px 25px 0; float: right;">
                            <a href="https://www.oneamerica.com/" title="OneAmerica" target="_blank"><img src="./images/career_page/logo-oneamerica.png" alt="OneAmerica" class="oa-logo"></a>
                        </div>
                        <div class="row-fluid" style="display: inline-block;">
                            <div class="general-disclaimer">
                                <p>The Moody Street Group, LLC is a general agency appointed with the insurance companies of OneAmerica<sup></sup>.</p>
                            </div>
                        </div>
                    </div>
                        
                </div>
        </center>
    </div>
    <div style="box-shadow: inset 0px 6px 6px rgba(0,0,0,.43);">
        <br>
        <br>
        <?php TemplateUtility::printFooter(); ?>
    </div>
</div>
<?php } else{ ?>
<br>
<div id="bottomShadow"></div>
<?php TemplateUtility::printFooter(); ?>
<?php } ?>
<script type="text/javascript">
    function validateCalendar(){
        var check = true;
        if($("#scheduledate").val() == '')
            check = false;
        if(check == false)
            alert('Select date');
        else{
            $("#jobForm").submit();
            //$('.fourthPage').addClass('hide');
            //$('.fifthPage').removeClass('hide');
        }
    }
    function toggleCalendar(){
        $('#cView').toggleClass('hide');
        if($('#cView').hasClass('hide')){
            $("#timeLabel").html("");
            $("#scheduledate").val("");
        }
    }
    function handleInterviewScheduleClickEntry(pointer){
        var dateTime = $(pointer).val();
        //$("#timeLabel").html(dateTime);
        $("#scheduleid").val($(pointer).attr("eventid"));
        $("#scheduledate").val(dateTime);
        $("#location").val($(pointer).attr("location"));
    }
    
</script>