<?php /* $Id: Home.tpl 3563 2007-11-12 07:41:54Z will $ */ ?>
<?php TemplateUtility::printHeader('Home', array('js/sweetTitles.js', 'js/dataGrid.js', 'js/home.js')); ?>
<?php TemplateUtility::printHeaderBlock(); ?>
<?php TemplateUtility::printTabs($this->active); ?>
<link href="./google calender/main.css" rel="stylesheet" type="text/css" />
    <div id="main">
        <?php TemplateUtility::printQuickSearch(); ?>

        <div id="contents" style="padding-top: 10px;">
		
		<table>
                <tr>
                    <td align="left" valign="top" style="text-align: left; width: 305px; height:50px;">
                        <div class="noteUnsizedSpan" style="width:306px;">My Recent Activity</div>
                        <?php $this->dataGrid2->drawHTML();  ?>
                    </td>

                    <td align="center" valign="top" style="text-align: left; width: 305px; font-size:11px; height:50px;">
                        <?php echo($this->upcomingEventsFupHTML); ?>
                    </td>

                    <td align="center" valign="top" style="text-align: left; width: 305px; font-size:11px; height:50px;">
                        <?php echo($this->upcomingEventsHTML); ?>
                    </td>
                </tr>
            </table>
		<table width="100%">
                <tr>
                    <td width="3%">
                        <img src="images/job_orders.gif" width="24" height="24" border="0" alt="Job Orders" style="margin-top: 3px;" />&nbsp;
                    </td>
                    <td><h2>Events: Home</h2></td>
                    <td align="right">
                        <form name="jobOrdersViewSelectorForm" id="jobOrdersViewSelectorForm" action="<?php echo(RESUFLOUtility::getIndexName()); ?>" method="get">
                            <input type="hidden" name="m" value="joborders" />
                            <input type="hidden" name="a" value="list" />

                            <table class="viewSelector">
                                <tr>
                                    <td>
                                    </td>
                                 </tr>
                            </table>
                        </form>
                    </td>
                </tr>
            </table>
			<p class="note">
<span style="float:left;">Resuflo Calender Events
</span>
<span style="float:right;">
            </span>&nbsp;
</p>

<table width="950" border="0" align="center" cellpadding="0" cellspacing="0">
  <tr>
    <td>
      <?php echo($this->cal); ?>
    </td>
  </tr>
</table>
</body>
</html>

        </div>
    </div>
    <div id="bottomShadow"></div>
<?php TemplateUtility::printFooter(); ?>
