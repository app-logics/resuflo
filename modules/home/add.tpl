<?php /* $Id: Home.tpl 3563 2007-11-12 07:41:54Z will $ */ ?>
<?php TemplateUtility::printHeader('Home', array('js/sweetTitles.js', 'js/dataGrid.js', 'js/home.js')); ?>
<?php TemplateUtility::printHeaderBlock(); ?>
<?php TemplateUtility::printTabs($this->active); ?>
<link href="./google calender/main.css" rel="stylesheet" type="text/css" />
<script src="./google calender/datetimepicker_css.js"></script>
<script type="text/javascript" >
function validationSubmit(){

	var length1 = document.getElementById('sdate_dd').value.length;
	if(length1=='1'){
		document.getElementById('sdate_dd').value = "0" + document.getElementById('sdate_dd').value;
	}
	var length2 = document.getElementById('edate_dd').value.length;
	if(length2=='1'){
		document.getElementById('edate_dd').value = "0" + document.getElementById('edate_dd').value;
	}
	var length3 = document.getElementById('sdate_hh').value.length;
	if(length3=='1'){
		document.getElementById('sdate_hh').value = "0" + document.getElementById('sdate_hh').value;
	}
	var length4 = document.getElementById('edate_hh').value.length;
	if(length4=='1'){
		document.getElementById('edate_hh').value = "0" + document.getElementById('edate_hh').value;
	}


	if(document.getElementById('title').value=="") { 
				alert("Please enter your Event Title");
				document.getElementById('title').focus();
				return false;
			} 
	if((document.getElementById('sdate_dd').value=="") || (document.getElementById('sdate_mm').value=="") || (document.getElementById('sdate_yy').value=="")){ 
				alert("Please enter your Start Date Correctly");
				document.getElementById('sdate_dd').focus();
				return false;
			} 
			
			else{
			if (!(document.getElementById('sdate_dd').value).toString().match(/^\s*\d+\s*$/) || !(document.getElementById('sdate_mm').value).toString().match(/^\s*\d+\s*$/) || !(document.getElementById('sdate_yy').value).toString().match(/^\s*\d+\s*$/)){
				alert('Only Numeric Characters are allowed'); return false;
				} 
			
			}
			
	if((document.getElementById('sdate_hh').value=="") || (document.getElementById('sdate_ii').value=="")){ 
				alert("Please enter your Start Time Correctly");
				document.getElementById('sdate_hh').focus();
				return false;
			} 
	
	else{
			if (!(document.getElementById('sdate_hh').value).toString().match(/^\s*\d+\s*$/) || !(document.getElementById('sdate_ii').value).toString().match(/^\s*\d+\s*$/)){
				alert('Only Numeric Characters are allowed'); return false;
			}
		} 
				
		if((document.getElementById('edate_dd').value=="") || (document.getElementById('edate_mm').value=="") || (document.getElementById('edate_yy').value=="")){ 
				alert("Please enter your End Date Correctly");
				document.getElementById('edate_dd').focus();
				return false;
			} 
			
		else{
			if (!(document.getElementById('edate_dd').value).toString().match(/^\s*\d+\s*$/) || !(document.getElementById('edate_mm').value).toString().match(/^\s*\d+\s*$/) || !(document.getElementById('edate_yy').value).toString().match(/^\s*\d+\s*$/)){
				alert('Only Numeric Characters are allowed'); return false;
				}
			}	 
			
			if((document.getElementById('edate_hh').value=="") || (document.getElementById('edate_ii').value=="") || (document.getElementById('sdate_yy').value=="")){ 
				alert("Please enter your End Time Correctly");
				document.getElementById('edate_hh').focus();
				return false;
			}
		else{
			if (!(document.getElementById('edate_hh').value).toString().match(/^\s*\d+\s*$/) || !(document.getElementById('edate_ii').value).toString().match(/^\s*\d+\s*$/)){
				alert('Only Numeric Characters are allowed'); return false;
			}
		} 
		
		
		var startDate = (document.getElementById('sdate_dd').value + "-" +document.getElementById('sdate_mm').value + "-" +document.getElementById('sdate_yy').value);
		var endDate = (document.getElementById('edate_dd').value + "-" +document.getElementById('edate_mm').value + "-" +document.getElementById('edate_yy').value);
		if(startDate > endDate ){
			alert('Start date should be smaller than End date');
			return false;
		} 
		var startTime = (document.getElementById('sdate_hh').value + "-" +document.getElementById('sdate_ii').value);
		var endTime = (document.getElementById('edate_hh').value + "-" +document.getElementById('edate_ii').value);
		if((startDate == endDate) && (startTime >= endTime) ){
			alert('Start Time should be smaller than End Time');
			return false;
		}
		
	
	return true;

}
</script>
    <div id="main">
        <?php TemplateUtility::printQuickSearch(); ?>

        <div id="contents" style="padding-top: 10px;">
		<table>
                <tr>
                    <td align="left" valign="top" style="text-align: left; width: 305px; height:50px;">
                        <div class="noteUnsizedSpan" style="width:306px;">My Recent Activity</div>
                        <?php $this->dataGrid2->drawHTML();  ?>
                    </td>

                    <td align="center" valign="top" style="text-align: left; width: 305px; font-size:11px; height:50px;">
                        <?php echo($this->upcomingEventsFupHTML); ?>
                    </td>

                    <td align="center" valign="top" style="text-align: left; width: 305px; font-size:11px; height:50px;">
                        <?php echo($this->upcomingEventsHTML); ?>
                    </td>
                </tr>
            </table>
		<table width="100%">
                <tr>
                    <td width="3%">
                        <img src="images/job_orders.gif" width="24" height="24" border="0" alt="Job Orders" style="margin-top: 3px;" />&nbsp;
                    </td>
                    <td><h2>Events: Home</h2></td>
                    <td align="right">
                        <form name="jobOrdersViewSelectorForm" id="jobOrdersViewSelectorForm" action="<?php echo(RESUFLOUtility::getIndexName()); ?>" method="get">
                            <input type="hidden" name="m" value="joborders" />
                            <input type="hidden" name="a" value="list" />

                            <table class="viewSelector">
                                <tr>
                                    <td>
                                    </td>
                                 </tr>
                            </table>
                        </form>
                    </td>
                </tr>
            </table>
			<p class="note">
<span style="float:left;">Add New Event
</span>
<span style="float:right;">
            </span>&nbsp;
</p>
			<table width="900" border="0" align="center" cellpadding="0" cellspacing="0">
  <tr>
    <td valign="top"><table width="280" border="0" cellspacing="0" cellpadding="0">

      <tr>
        <td colspan="3" class="font-top"><img src="./google calender/images/spacer.gif" width="5" height="5" /></td>
      </tr>
      
     
    <form method="post" action="index.php?m=home&a=add">
	
	 <tr>
        <td colspan="3" bgcolor="#CCCCCC" class="grid font-event"><?php echo $this->error; ?></td>
      </tr>
      <tr>
        <td colspan="3" class="font-txt"><img src="./google calender/images/images/spacer.gif" width="5" height="10" /></td>
      </tr>
	  <tr>
        <td colspan="3" class="font-txt">Event Title </td>
      </tr>
      <tr>
        <td colspan="3" class="font-txt"><label>
      <input name="title" id="title" type="text"  class="txtbox" /><p/>
      </label></td>
      </tr>
      <tr>
        <td colspan="3" class="font-txt"><img src="./google calender/images/images/spacer.gif" width="5" height="5" /></td>
      </tr>
      <tr>
        <td width="100" colspan="3" class="font-txt">Start Date (dd/mm/yyyy): <img src="./google calender/images/cal.gif" onClick="javascript:NewCssCal ('demo2','ddMMyyyy','arrow',true,'24')"  style="cursor:pointer"/></td>
		<input type="hidden" id="demo2" maxlength="25" size="10"/>
      </tr>
      <tr>
       <td class="font-txt"><input name="sdate_dd" readonly="readonly" id="sdate_dd" type="text" size="2" class="txtbox-small" /></td>
       <td class="font-txt"><input name="sdate_mm" readonly="readonly" id="sdate_mm" type="text" size="2" class="txtbox-small"/></td>
      <td class="font-txt"> <input name="sdate_yy" readonly="readonly" id="sdate_yy" type="text" size="4" class="txtbox-small"/></td>
       </tr>
      <tr>
        <td colspan="3" class="font-txt"><img src="./google calender/images/images/spacer.gif" width="5" height="5" /></td>
        </tr>
      <tr>
        <td width="100" colspan="3" class="font-txt">Start Time (hh:mm):</td>
      </tr>
      <tr>
      <td class="font-txt"><input name="sdate_hh" readonly="readonly" id="sdate_hh" type="text" size="2" class="txtbox-small"/></td> 
     <td class="font-txt"> <input name="sdate_ii" readonly="readonly" id="sdate_ii" type="text" size="2" class="txtbox-small" /></td>
       </tr>
      <tr>
        <td colspan="3" class="font-txt"><img src="./google calender/images/images/spacer.gif" width="5" height="5" /></td>
        </tr>
      <tr>
         <td width="100" colspan="3" class="font-txt">End Date (dd/mm/yyyy):<img src="./google calender/images/cal.gif" onClick="javascript:NewCssCal ('demo1','ddMMyyyy','arrow',true,'24')"  style="cursor:pointer"/></td>
		<input type="hidden" id="demo1" maxlength="25" size="10"/></td>
      </tr>
      <tr>
      <td class="font-txt"><input name="edate_dd" readonly="readonly" id="edate_dd" type="text" size="2" class="txtbox-small"/></td>
      <td class="font-txt"><input name="edate_mm" readonly="readonly" id="edate_mm" type="text" size="2" class="txtbox-small"/></td>
     <td class="font-txt"> <input name="edate_yy" readonly="readonly" id="edate_yy" type="text" size="4" class="txtbox-small"/></td>
      </tr>
      <tr>
        <td colspan="3" class="font-txt"><img src="./google calender/images/images/spacer.gif" width="5" height="5" /></td>
        </tr>
      <tr>
      <td width="100" colspan="3" class="font-txt">End Time (hh:mm):</td>
      </tr>
      <tr>
      <td class="font-txt"><input name="edate_hh" readonly="readonly" id="edate_hh" type="text" size="2" class="txtbox-small"/> </td>
      <td class="font-txt"><input name="edate_ii" readonly="readonly" id="edate_ii" type="text" size="2" class="txtbox-small"/></td>
	  </tr>
      <tr>
        <td colspan="3" class="font-txt">&nbsp;</td>
        </tr>
      <tr>
        <td colspan="3" class="font-txt"><input name="submit" type="submit" value="Save" onclick="return validationSubmit();" /></td>
        </tr>
      <td class="font-txt"></td>      
    </form>
   
  </table>
	</td>
    <td><table width="600" border="0" cellspacing="1" cellpadding="1">
      <tr>
        <td><iframe src="https://www.google.com/calendar/embed?showTitle=0&amp;showPrint=0&amp;showCalendars=0&amp;height=600&amp;wkst=1&amp;bgcolor=%23FFFFFF&amp;src=<?php echo $this->user; ?>&amp;color=%2329527A" style=" border-width:0 " width="600" height="600" frameborder="0" scrolling="no"></iframe></td>
      </tr>
    </table></td>
  </tr>
</table>

        </div>
    </div>
    <div id="bottomShadow"></div>
<?php TemplateUtility::printFooter(); ?>
