<?php /* $Id: Home.tpl 3563 2007-11-12 07:41:54Z will $ */ ?>
<?php TemplateUtility::printHeader('Home', array('js/sweetTitles.js', 'js/dataGrid.js', 'js/home.js')); ?>
<link href="./google calender/main.css" rel="stylesheet" type="text/css" />
<style>
    body{
        background-color: #E9EBEC;
        background: #E9EBEC;
    }
    .txtbox_small{
        width: 150px;
        margin-right: 2px;
    }
    .hide{
        display: none;
    }
    #main{
        border: none;
        background: none;
        padding-top: 0px;
    }
    #contents{
        padding: 0px;
    }
    .footerBlock{
        margin-top: 0;
        width: auto;
        padding-bottom: 100px;
    }
</style>
<script>
    $("body").css({backgroundColor: '#E9EBEC'});
</script> 
<div id="headerBlock">
    <table cellspacing="0" cellpadding="0" style="margin: 0px; padding: 0px; float: left;">
        <tbody><tr>
                <td rowspan="2"><img src="<?php echo($this->logo); ?>" border="0" alt="RESUFLO" style="max-width: 202px;"></td>
            </tr>
        </tbody>
    </table>
</div>
<div id="main" style="min-height: auto; margin-top: 5px; background-color: #E9EBEC;">
    
    <div id="contents" style="padding-top: 10px; min-height: auto; background-color: #E9EBEC;">
        <br><br>
        <?php if (!empty($this->ScheduleDate)){ ?>
        
        <table class="firstPage" border="0" align="center" cellpadding="0" cellspacing="0">
            <tr>
                <td>
                    <span style="font-weight: bold;">Applicant Name:</span>
                </td>
                <td>
                    <span style="margin-left: 10px;"><?php echo($this->AppName); ?></span>
                </td>
            </tr>
            <tr>
                <td>
                    <span style="font-weight: bold;">Applicant Email:</span>
                </td>
                <td>
                    <span style="margin-left: 10px;"><?php echo($this->AppEmail); ?></span>
                </td>
            </tr>
            <tr>
                <td>
                    <span style="font-weight: bold;">Interview Date:</span>
                </td>
                <td>
                    <span style="margin-left: 10px;"><?php echo($this->ScheduleDate); ?></span>
                </td>
            </tr>
            <tr>
                <td></td>
                <td>
                    <br/>
                    <form name="jobForm" id="jobForm" action="./que/cancelinterview.php" method="post" autocomplete="on">
                        <input type="hidden" name="postback" id="postback" value="postback" />
                        <input type="hidden" name="appID" id="siteID" value="<?php echo $_GET['appid']; ?>">
                        <input type="hidden" name="siteID" id="siteID" value="<?php echo $_GET['siteID']; ?>">
                        <input type="hidden" name="userID" id="siteID" value="<?php echo $_GET['cl']; ?>">
                        <button style="margin-left: 10px;">Cancel Interview</button>
                    </form>
                </td>
            </tr>
        </table>
        <?php }else {?>
            
            No Data Found!

        <?php }?>
        </body>
        </html>
    </div>
</div>
<br>
</div>
<?php if ($this->userID == 1585){ ?>
<div style=" background: #bcbebf url('./images/career_page/pattern-03.png') repeat scroll 0 0; position: absolute; left: 0; width: 100%; height: 183px; ">
    <div class="container">
        <center>
            <div class="footer-columns" style="background: url('./images/career_page/25p.png');width: 950px; text-align: left;">
                <!--<img src="../../images/career_page/unnamed.png" alt=""/>-->
                    <div class="row-fluid footer-div equal" style="background: url('./images/career_page/footer-divider.png') repeat-y 50% 0; height: 183px;">
                    
                    <div style="height: 183px;padding-left: 70px;padding-bottom: 14px;width: 405px;display: inline-block;float: left;" class="span6 footer-contact">
                        <div class="footer-logo" style="padding: 28px 0 25px;">
                            <img src="./images/career_page/Logo-Reversed-Small.png" id="ctl00_FooterUC_ctl00_SiteFooterLogo_1" class="SiteFooterLogo" alt="logo" style="width: 228px;">
                        </div>
                        <address class="footer-address">
                            <p>
                                <span class="address1">134 Rumford Ave</span> <span class="address2">Suite 201</span><span class="address-comma">,</span> <span class="city">Newton,</span> <span class="state">MA</span> <span class="zip">02466</span><br>
                                <span class="footer-phone">Phone <a href="tel:+1617 9165155" style="color: #808080;">(617) 916-5155</a> |</span>
                                    
                                <span class="footer-email"><a href="mailto:jbreedlove@moodystreet.com" style="color: #808080;">jbreedlove@moodystreet.com</a> </span>
                                    
                            </p>
                        </address>
                    </div>
                        
                    <div style="height: 183px;width: 455px;display: inline-block;float: right;padding-left: 20px;padding-bottom: 14px;" class="span6 footer-bd-info">
                        <div class="row-fluid bd-logo" style="padding: 28px 30px 25px 0; float: right;">
                            <a href="https://www.oneamerica.com/" title="OneAmerica" target="_blank"><img src="./images/career_page/logo-oneamerica.png" alt="OneAmerica" class="oa-logo"></a>
                        </div>
                        <div class="row-fluid" style="display: inline-block;">
                            <div class="general-disclaimer">
                                <p>The Moody Street Group, LLC is a general agency appointed with the insurance companies of OneAmerica<sup></sup>.</p>
                            </div>
                        </div>
                    </div>
                        
                </div>
        </center>
    </div>
    <div style="box-shadow: inset 0px 6px 6px rgba(0,0,0,.43);">
        <br>
        <br>
        <?php TemplateUtility::printFooter(); ?>
    </div>
</div>
<?php } else{ ?>
<br>
<div id="bottomShadow"></div>
<?php TemplateUtility::printFooter(); ?>
<?php } ?>
<script type="text/javascript">
    function validateForm(){
        var check = true;
        if($("#emailaddress").val() == '')
            check = false;
        if($("#cname").val() == '')
            check = false;
        if($("#lname").val() == '')
            check = false;
        if($("#phone").val() == '')
            check = false;
        if(check == false)
            alert('Some required fields are missing');
        else{
            $('.secondPage').addClass('hide');
            $('.thirdPage').removeClass('hide');
        }
    }
    function validateCalendar(){
        var check = true;
        if($("#scheduledate").val() == '')
            check = false;
        if(check == false)
            alert('Select date');
        else{
            $("#jobForm").submit();
            //$('.fourthPage').addClass('hide');
            //$('.fifthPage').removeClass('hide');
        }
    }
    function toggleCalendar(){
        $('#cView').toggleClass('hide');
        if($('#cView').hasClass('hide')){
            $("#timeLabel").html("");
            $("#scheduledate").val("");
        }
    }
    function handleInterviewScheduleClickEntry(pointer){
        var dateTime = $(pointer).val();
        //$("#timeLabel").html(dateTime);
        $("#scheduleid").val($(pointer).attr("eventid"));
        $("#scheduledate").val(dateTime);
        $("#location").val($(pointer).attr("location"));
    }
    
</script>