<?php /* $Id: Home.tpl 3563 2007-11-12 07:41:54Z will $ */ ?>
<?php TemplateUtility::printHeader('Home', array('js/sweetTitles.js', 'js/dataGrid.js', 'js/home.js')); ?>
<?php TemplateUtility::printHeaderBlock(); ?>
<?php TemplateUtility::printTabs($this->active); ?>
<link href="./google calender/main.css" rel="stylesheet" type="text/css" />
    <div id="main">
        <?php TemplateUtility::printQuickSearch(); ?>

        <div id="contents" style="padding-top: 10px;">
		
		<table>
                <tr>
                    <td align="left" valign="top" style="text-align: left; width: 305px; height:50px;">
                        <div class="noteUnsizedSpan" style="width:306px;">My Recent Activity</div>
                        <?php $this->dataGrid2->drawHTML();  ?>
                    </td>

                    <td align="center" valign="top" style="text-align: left; width: 305px; font-size:11px; height:50px;">
                        <?php echo($this->upcomingEventsFupHTML); ?>
                    </td>

                    <td align="center" valign="top" style="text-align: left; width: 305px; font-size:11px; height:50px;">
                        <?php echo($this->upcomingEventsHTML); ?>
                    </td>
                </tr>
            </table>
		<table width="100%">
                <tr>
                    <td width="3%">
                        <img src="images/job_orders.gif" width="24" height="24" border="0" alt="Job Orders" style="margin-top: 3px;" />&nbsp;
                    </td>
                    <td><h2>Events: Home</h2></td>
                    <td align="right">
                        <form name="jobOrdersViewSelectorForm" id="jobOrdersViewSelectorForm" action="<?php echo(RESUFLOUtility::getIndexName()); ?>" method="get">
                            <input type="hidden" name="m" value="joborders" />
                            <input type="hidden" name="a" value="list" />

                            <table class="viewSelector">
                                <tr>
                                    <td>
                                    </td>
                                 </tr>
                            </table>
                        </form>
                    </td>
                </tr>
            </table>
			<p class="note">
<span style="float:left;">Google Calender Events - <?php echo $this->feed->totalResults; ?> event(s) found
</span>
<span style="float:right;">
            </span>&nbsp;
</p>

 <?php if ($this->blank_error): ?>
                    Please Add Your Google Calender Credentials <a href="index.php?m=settings&a=calender">here</a> to view you Google Calender
                    <br /><br />
                <?php endif; ?>
 <?php if ($this->wrong_error): ?>
                    There is some error with your credentials.<br />Please Update Your Google Calender Credentials <a href="index.php?m=settings&a=calender">here</a> to view you Google Calender
                    <br /><br />
                <?php endif; ?>

<table width="950" border="0" align="center" cellpadding="0" cellspacing="0">
  <tr>
    <td valign="top"><div style="padding-top:10px; overflow-x:hidden;overflow-y:auto; height:600px;"> <table width="280" border="0" cellspacing="0" cellpadding="0">
		<tr><td>
       <table width="121%" border="0" cellspacing="0" cellpadding="0">
          <tr>
            <!--<td width="147" class="font-top"><a href="index.php?m=home&a=add">Add New Event</a> </td>-->
            <td width="133" class="font-top-new"></td>
			<td width="133" class="font-top-new"></td>
          </tr>
        </table></td>
      </tr>
	  <tr>
        <td colspan="4" class="font-top"><img src="./google calender/images/spacer.gif" width="5" height="5" /></td>
      </tr>
	  <?php foreach($this->feed as $event)  { 
	  if(!empty($event))
	  {
	  	$id = substr($event->id, strrpos($event->id, '/')+1);
		}
	  ?>
	  <tr><td colspan="3">
	  <table width="280" border="0" cellspacing="0" cellpadding="0">
  		<tr>
		  <td width="auto"><img src="./google calender/images/top-img.gif"  /></td>
		</tr>
		<tr>
			 <td class="mid_title" style="background-color:#0094d6;padding:5px;"><strong><?php echo $event->title; ?></strong> </td>
	   </tr>
	</table>
	  </td></tr>
	 <tr bgcolor="#0094d6">
		<td colspan="3" class="font-txt"><strong><?php echo $event->summary; ?></strong></td>
      </tr>
      <tr bgcolor="#0094d6">
	  	<td>&nbsp;</td>
        <td width="19" title="Edit"><a href="index.php?m=home&a=edit&id=<?php echo $id; ?>"><img src="./google calender/images/edit.jpg" width="16" height="16"  /></a></td>
        <td width="31" title="Delete"><a href="index.php?m=home&a=delete&id=<?php echo $id; ?>"><img src="./google calender/images/delete.jpg" width="16" height="16"  /></a></td>        
      </tr>
	  <tr>
		  <td colspan="3" width="auto"><img src="./google calender/images/btm-round.gif"  /></td>
		</tr>
      <tr>
        <td colspan="3"><img src="./google calender/images/spacer.gif" width="5" height="5" /></td>
        </tr>
		
		<?php } ?>
		  <tr>
        <td colspan="3" class="font-top"><img src="./google calender/images/spacer.gif" width="5" height="5" /></td>
      </tr>
	 </table></div>
	</td>
    <td valign="top"><table width="600" border="0" cellspacing="1" cellpadding="1">
      <tr>
        <td valign="top">
<iframe src="https://www.google.com/calendar/embed?showTitle=0&amp;showPrint=0&amp;showCalendars=0&amp;height=600&amp;wkst=1&amp;bgcolor=%23FFFFFF&amp;src=<?php echo $this->user; ?>&amp;color=%2329527A" style=" border-width:0 " width="600" height="600" frameborder="0" scrolling="no"></iframe></td>
      </tr>
    </table></td>
  </tr>
</table>
</body>
</html>

        </div>
    </div>
    <div id="bottomShadow"></div>
<?php TemplateUtility::printFooter(); ?>
