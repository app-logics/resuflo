<?php /* $Id: JobOrderReport.tpl 2441 2007-05-04 20:42:02Z brian $ */ ?>
<?php TemplateUtility::printHeader('Pipeline Report', array('modules/joborders/validator.js', 'js/company.js', 'js/sweetTitles.js')); ?>
<?php TemplateUtility::printHeaderBlock(); ?>
<?php TemplateUtility::printTabs($this->active, $this->subActive); ?>
<link href="css/calendar.css" rel="stylesheet" type="text/css"/>
<script language="javaScript" type="text/javascript" src="js/calendar.js"></script>
<script type="text/javascript">
function getReportValue(){
}
function validationSubmit(){
	if(document.getElementById('start_date').value=="") { 
				alert("Please enter your Start Date");
				document.getElementById('start_date').focus();
				return false;
			} 
	if(document.getElementById('end_date').value=="") { 
				alert("Please enter your End Date");
				document.getElementById('end_date').focus();
				return false;
			} 
	return true;

}
</script>
    <div id="main">
        <?php TemplateUtility::printQuickSearch(); ?>
        <div id="contents">
            <table>
                <tr>
                    <td width="3%">
                        <img src="images/job_orders.gif" width="24" height="24" border="0" alt="Job Orders" style="margin-top: 3px;" />&nbsp;
                    </td>
                    <td><h2>Reports: Pipeline Report</h2></td>
                </tr>
            </table>
            <p class="note">Generate a Pipeline report.</p>
            <form name="jobOrderReportForm" id="jobOrderReportForm" action="<?php echo(RESUFLOUtility::getIndexName()); ?>" method="get">
                <input type="hidden" name="m" value="reports">
                <input type="hidden" name="a" value="customizeJobOrderReport">

                <table class="editTable" width="900">
                    <tr>
                        <td class="tdVertical" style="width: 150px;">
                            <label id="siteNameLabel" for="siteName">Company Name:</label>
                        </td>
                        <td class="tdData">
                            <input type="text" class="inputbox" name="siteName" id="siteName" value="<?php $this->_($this->reportParameters['siteName']); ?>" style="width: 250px;" />&nbsp;*
                        </td>
                    </tr>

                    <tr>
                        <td class="tdVertical">
                            <label id="companyNameLabel" for="companyName">Company:</label>
                        </td>
                        <td class="tdData">
                            <input type="text" class="inputbox" name="companyName" id="companyName" value="<?php $this->_($this->reportParameters['companyName']); ?>" style="width: 250px;" />&nbsp;*
                        </td>
                    </tr>

                    <tr>
                        <td class="tdVertical">
                            <label id="jobOrderNameLabel" for="jobOrderName">Position (Title):</label>
                        </td>
                        <td class="tdData">
                            <input type="text" class="inputbox" name="jobOrderName" id="jobOrderName" value="<?php $this->_($this->reportParameters['jobOrderName']); ?>" style="width: 250px;" />&nbsp;*
                        </td>
                    </tr>

                   <tr>
						<td width="15%" class="menu_txt">Start Date</td>
						<td width="15%"><span class="menu_txt">
						  <input name="start_date" type="text" id="start_date" class="search_txtbox" value="<?php $this->_($this->reportParameters['periodLinestartdate']); ?>" />
						</span>
						<a href="#" onClick="setYears(1947, 2028);
					   showCalender(this, 'start_date');"><img src="images/calender_icon.gif" width="24" height="24" border="0" /></a>
						End Date
						<span class="menu_txt">
						  <input name="end_date" id="end_date" type="text" class="search_txtbox" value="<?php $this->_($this->reportParameters['periodLineenddate']); ?>" />
						</span>
					<a href="#" onClick="setYears(1947, 2028);
					   showCalender(this, 'end_date');"><img src="images/calender_icon.gif" width="24" height="24" border="0" /></a>&nbsp;*</td>
                    </tr>

                    <tr>
                        <td class="tdVertical">
                            <label id="accountManagerLabel" for="accountManager">Account Manager:</label>
                        </td>
                        <td class="tdData">
                            <input type="text" class="inputbox" name="accountManager" id="accountManager" value="<?php $this->_($this->reportParameters['accountManager']); ?>" style="width: 250px;" />&nbsp;*
                        </td>
                    </tr>

                    <tr>
                        <td class="tdVertical">
                            <!-- <label id="recruiterLabel" for="recruiter">Recruiter:</label> --!>
                        </td>
                        <td class="tdData">
						<?php if($_SESSION['RESUFLO']->getClientLogin()==1 && $_SESSION['RESUFLO']->getAccessLevel()==400){ 
						$sql = mysql_query("select * from company where owner = ".$_SESSION['resumeinsertuserid']);
						while($data = mysql_fetch_array($sql)){
							$ownerCompanyName = $data['name'];
						}
						$recID = $this->reportParameters['userID'];
						?>
       		<select class="inputbox" name="recruiter" id="recruiter"  style="width: 250px;display:none;" onChange="OnChange();" />&nbsp;*
				<option <?php if($_REQUEST['recId']==$_SESSION['resumeinsertuserid']){ ?> selected="selected" <?php } ?> value="<?php echo $_SESSION['resumeinsertuserid']; ?>"><?php echo $ownerCompanyName; ?></option>
				<option <?php if($_REQUEST['recId']==$this->reportParameters['userID']){ ?> selected="selected" <?php } ?> value="<?php $this->_($this->reportParameters['userID']); ?>"><?php $this->_($this->reportParameters['recruiter']); ?></option>
			</select>
			 <input type="hidden" name="jobOrderID" id="jobOrderID" value="<?php $this->_($this->reportParameters['jobOrderID']); ?>">	
			 			
		<?php }else if($_SESSION['RESUFLO']->getClientLogin()==1 && $_SESSION['RESUFLO']->getAccessLevel()!=400){ ?>
			<input type="hidden" class="inputbox" name="recruiter" id="recruiter" value="<?php $this->_($this->reportParameters['userID']); ?>" style="width: 250px;" />&nbsp;
			<input type="hidden" name="jobOrderID" id="jobOrderID" value="<?php $this->_($this->reportParameters['jobOrderID']); ?>">	
		<?php } ?>
                            
                        </td>
                    </tr>
                </table>

              
                <input type="submit" class="button" name="submit" value="Generate Report" />&nbsp;
                <input type="reset"  class="button" name="reset"  value="Reset" />&nbsp;
                
                <!-- IE PDF Hack -->
                <input type="hidden" name="ext" value=".pdf" />
				<!-- Calender Script  --> 

    <table id="calenderTable">
        <tbody id="calenderTableHead">
          <tr>
            <td colspan="4" align="center">
	          <select onChange="showCalenderBody(createCalender(document.getElementById('selectYear').value,
	           this.selectedIndex, false));" id="selectMonth">
	              <option value="0">Jan</option>
	              <option value="1">Feb</option>
	              <option value="2">Mar</option>
	              <option value="3">Apr</option>
	              <option value="4">May</option>
	              <option value="5">Jun</option>
	              <option value="6">Jul</option>
	              <option value="7">Aug</option>
	              <option value="8">Sep</option>
	              <option value="9">Oct</option>
	              <option value="10">Nov</option>
	              <option value="11">Dec</option>
	          </select>
            </td>
            <td colspan="2" align="center">
			    <select onChange="showCalenderBody(createCalender(this.value, 
				document.getElementById('selectMonth').selectedIndex, false));" id="selectYear">
				</select>
			</td>
            <td align="center">
			    <a href="#" onClick="closeCalender();"><font color="#003333" size="+1">X</font></a>
			</td>
		  </tr>
       </tbody>
       <tbody id="calenderTableDays">
         <tr style="">
           <td>Sun</td><td>Mon</td><td>Tue</td><td>Wed</td><td>Thu</td><td>Fri</td><td>Sat</td>
         </tr>
       </tbody>
       <tbody id="calender"></tbody>
    </table>

<!-- End Calender Script  --> 
            </form>

            <script type="text/javascript">
                document.jobOrderReportForm.siteName.focus();
            </script>
			<script type="text/javascript" >
			function OnChange()
			{
				 var recId = document.getElementById('recruiter').value;
				 var jobOrderID = document.getElementById('jobOrderID').value;
				 window.location="index.php?m=reports&a=customizeJobOrderReport&jobOrderID="+jobOrderID+"&recId="+recId;
			}
			</script>
        </div>
    </div>
    <div id="bottomShadow"></div>
<?php TemplateUtility::printFooter(); ?>
