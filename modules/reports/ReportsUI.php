<?php
/*
 * RESUFLO
 * Reports Module
 *
 * Copyright (C) 2005 - 2007 Econn Technologies, Inc.
 *
 *
 * The contents of this file are subject to the RESUFLO Public License
 * Version 1.1a (the "License"); you may not use this file except in
 * compliance with the License. You may obtain a copy of the License at
 * http://www.econnoisseur.com/.
 *
 * Software distributed under the License is distributed on an "AS IS"
 * basis, WITHOUT WARRANTY OF ANY KIND, either express or implied. See the
 * License for the specific language governing rights and limitations
 * under the License.
 *
 * The Original Code is "RESUFLO Standard Edition".
 *
 * The Initial Developer of the Original Code is Econn Technologies, Inc.
 * Portions created by the Initial Developer are Copyright (C) 2005 - 2007
 * (or from the year in which this file was created to the year 2007) by
 * Econn Technologies, Inc. All Rights Reserved.
 *
 *
 * $Id: ReportsUI.php 3810 2007-12-05 19:13:25Z brian $
 */

include_once('./lib/Statistics.php');
include_once('./lib/DateUtility.php');
include_once('./lib/Candidates.php');
include_once('./lib/CommonErrors.php');

class ReportsUI extends UserInterface
{
    public function __construct()
    {
        parent::__construct();

        $this->_authenticationRequired = true;
        $this->_moduleDirectory = 'reports';
        $this->_moduleName = 'reports';
        //$this->_moduleTabText = 'Reports';
		$this->_moduleTabText = '';
        $this->_subTabs = array(
                'EEO Reports' => RESUFLOUtility::getIndexName() . '?m=reports&amp;a=customizeEEOReport'
            );
    }


    public function handleRequest()
    {
        if (!eval(Hooks::get('REPORTS_HANDLE_REQUEST'))) return;

        $action = $this->getAction();
        switch ($action)
        {
            case 'graphView':
                $this->graphView();
                break;

            case 'generateJobOrderReportPDF':
                $this->generateJobOrderReportPDF();
                break;

            case 'showSubmissionReport':
                $this->showSubmissionReport();
                break;

            case 'showPlacementReport':
                $this->showPlacementReport();
                break;

            case 'customizeJobOrderReport':
                $this->customizeJobOrderReport();
                break;
			case 'customizeJobOrderReportFirst':
                $this->customizeJobOrderReportFirst();
                break;

            case 'customizeEEOReport':
                $this->customizeEEOReport();
                break;

            case 'generateEEOReportPreview':
                $this->generateEEOReportPreview();
                break;

            case 'reports':
            default:
                $this->reports();
                break;
        }
    }

    private function reports()
    {
        /* Grab an instance of Statistics. */
        $statistics = new Statistics($this->_siteID);

        /* Get company statistics. */
        $statisticsData['totalCompanies']     = $statistics->getCompanyCount(TIME_PERIOD_TODATE);
        $statisticsData['companiesToday']     = $statistics->getCompanyCount(TIME_PERIOD_TODAY);
        $statisticsData['companiesYesterday'] = $statistics->getCompanyCount(TIME_PERIOD_YESTERDAY);
        $statisticsData['companiesThisWeek']  = $statistics->getCompanyCount(TIME_PERIOD_THISWEEK);
        $statisticsData['companiesLastWeek']  = $statistics->getCompanyCount(TIME_PERIOD_LASTWEEK);
        $statisticsData['companiesThisMonth'] = $statistics->getCompanyCount(TIME_PERIOD_THISMONTH);
        $statisticsData['companiesLastMonth'] = $statistics->getCompanyCount(TIME_PERIOD_LASTMONTH);
        $statisticsData['companiesThisYear']  = $statistics->getCompanyCount(TIME_PERIOD_THISYEAR);
        $statisticsData['companiesLastYear']  = $statistics->getCompanyCount(TIME_PERIOD_LASTYEAR);

        /* Get candidate statistics. */
        $statisticsData['totalCandidates']     = $statistics->getCandidateCount(TIME_PERIOD_TODATE);
        $statisticsData['candidatesToday']     = $statistics->getCandidateCount(TIME_PERIOD_TODAY);
        $statisticsData['candidatesYesterday'] = $statistics->getCandidateCount(TIME_PERIOD_YESTERDAY);
        $statisticsData['candidatesThisWeek']  = $statistics->getCandidateCount(TIME_PERIOD_THISWEEK);
        $statisticsData['candidatesLastWeek']  = $statistics->getCandidateCount(TIME_PERIOD_LASTWEEK);
        $statisticsData['candidatesThisMonth'] = $statistics->getCandidateCount(TIME_PERIOD_THISMONTH);
        $statisticsData['candidatesLastMonth'] = $statistics->getCandidateCount(TIME_PERIOD_LASTMONTH);
        $statisticsData['candidatesThisYear']  = $statistics->getCandidateCount(TIME_PERIOD_THISYEAR);
        $statisticsData['candidatesLastYear']  = $statistics->getCandidateCount(TIME_PERIOD_LASTYEAR);

        /* Get submission statistics. */
        $statisticsData['totalSubmissions']     = $statistics->getSubmissionCount(TIME_PERIOD_TODATE);
        $statisticsData['submissionsToday']     = $statistics->getSubmissionCount(TIME_PERIOD_TODAY);
        $statisticsData['submissionsYesterday'] = $statistics->getSubmissionCount(TIME_PERIOD_YESTERDAY);
        $statisticsData['submissionsThisWeek']  = $statistics->getSubmissionCount(TIME_PERIOD_THISWEEK);
        $statisticsData['submissionsLastWeek']  = $statistics->getSubmissionCount(TIME_PERIOD_LASTWEEK);
        $statisticsData['submissionsThisMonth'] = $statistics->getSubmissionCount(TIME_PERIOD_THISMONTH);
        $statisticsData['submissionsLastMonth'] = $statistics->getSubmissionCount(TIME_PERIOD_LASTMONTH);
        $statisticsData['submissionsThisYear']  = $statistics->getSubmissionCount(TIME_PERIOD_THISYEAR);
        $statisticsData['submissionsLastYear']  = $statistics->getSubmissionCount(TIME_PERIOD_LASTYEAR);

		/* Get placement statistics. */
        $statisticsData['totalPlacements']     = $statistics->getPlacementCount(TIME_PERIOD_TODATE);
        $statisticsData['placementsToday']     = $statistics->getPlacementCount(TIME_PERIOD_TODAY);
        $statisticsData['placementsYesterday'] = $statistics->getPlacementCount(TIME_PERIOD_YESTERDAY);
        $statisticsData['placementsThisWeek']  = $statistics->getPlacementCount(TIME_PERIOD_THISWEEK);
        $statisticsData['placementsLastWeek']  = $statistics->getPlacementCount(TIME_PERIOD_LASTWEEK);
        $statisticsData['placementsThisMonth'] = $statistics->getPlacementCount(TIME_PERIOD_THISMONTH);
        $statisticsData['placementsLastMonth'] = $statistics->getPlacementCount(TIME_PERIOD_LASTMONTH);
        $statisticsData['placementsThisYear']  = $statistics->getPlacementCount(TIME_PERIOD_THISYEAR);
        $statisticsData['placementsLastYear']  = $statistics->getPlacementCount(TIME_PERIOD_LASTYEAR);

        /* Get contact statistics. */
        $statisticsData['totalContacts']     = $statistics->getContactCount(TIME_PERIOD_TODATE);
        $statisticsData['contactsToday']     = $statistics->getContactCount(TIME_PERIOD_TODAY);
        $statisticsData['contactsYesterday'] = $statistics->getContactCount(TIME_PERIOD_YESTERDAY);
        $statisticsData['contactsThisWeek']  = $statistics->getContactCount(TIME_PERIOD_THISWEEK);
        $statisticsData['contactsLastWeek']  = $statistics->getContactCount(TIME_PERIOD_LASTWEEK);
        $statisticsData['contactsThisMonth'] = $statistics->getContactCount(TIME_PERIOD_THISMONTH);
        $statisticsData['contactsLastMonth'] = $statistics->getContactCount(TIME_PERIOD_LASTMONTH);
        $statisticsData['contactsThisYear']  = $statistics->getContactCount(TIME_PERIOD_THISYEAR);
        $statisticsData['contactsLastYear']  = $statistics->getContactCount(TIME_PERIOD_LASTYEAR);

        /* Get job order statistics. */
        $statisticsData['totalJobOrders']     = $statistics->getJobOrderCount(TIME_PERIOD_TODATE);
        $statisticsData['jobOrdersToday']     = $statistics->getJobOrderCount(TIME_PERIOD_TODAY);
        $statisticsData['jobOrdersYesterday'] = $statistics->getJobOrderCount(TIME_PERIOD_YESTERDAY);
        $statisticsData['jobOrdersThisWeek']  = $statistics->getJobOrderCount(TIME_PERIOD_THISWEEK);
        $statisticsData['jobOrdersLastWeek']  = $statistics->getJobOrderCount(TIME_PERIOD_LASTWEEK);
        $statisticsData['jobOrdersThisMonth'] = $statistics->getJobOrderCount(TIME_PERIOD_THISMONTH);
        $statisticsData['jobOrdersLastMonth'] = $statistics->getJobOrderCount(TIME_PERIOD_LASTMONTH);
        $statisticsData['jobOrdersThisYear']  = $statistics->getJobOrderCount(TIME_PERIOD_THISYEAR);
        $statisticsData['jobOrdersLastYear']  = $statistics->getJobOrderCount(TIME_PERIOD_LASTYEAR);

        if (!eval(Hooks::get('REPORTS_SHOW'))) return;

        $this->_template->assign('active', $this);
        $this->_template->assign('statisticsData', $statisticsData);
        $this->_template->display('./modules/reports/Reports.tpl');
    }

    private function graphView()
    {
        if (isset($_GET['theImage']))
        {
            $this->_template->assign('theImage', $_GET['theImage']);
        }
        else
        {
            $this->_template->assign('theImage', '');
        }

        if (!eval(Hooks::get('REPORTS_GRAPH'))) return;

        $this->_template->assign('active', $this);
        $this->_template->display('./modules/reports/GraphView.tpl');
    }

    private function showSubmissionReport()
    {
        //FIXME: getTrimmedInput
        if (isset($_GET['period']) && !empty($_GET['period']))
        {
            $period = $_GET['period'];
        }
        else
        {
            $period = '';
        }


        switch ($period)
        {
            case 'yesterday':
                $period = TIME_PERIOD_YESTERDAY;
                $reportTitle = 'Yesterday\'s Report';
                break;

            case 'thisWeek':
                $period = TIME_PERIOD_THISWEEK;
                $reportTitle = 'This Week\'s Report';
                break;

            case 'lastWeek':
                $period = TIME_PERIOD_LASTWEEK;
                $reportTitle = 'Last Week\'s Report';
                break;

            case 'thisMonth':
                $period = TIME_PERIOD_THISMONTH;
                $reportTitle = 'This Month\'s Report';
                break;

            case 'lastMonth':
                $period = TIME_PERIOD_LASTMONTH;
                $reportTitle = 'Last Month\'s Report';
                break;

            case 'thisYear':
                $period = TIME_PERIOD_THISYEAR;
                $reportTitle = 'This Year\'s Report';
                break;

            case 'lastYear':
                $period = TIME_PERIOD_LASTYEAR;
                $reportTitle = 'Last Year\'s Report';
                break;

            case 'toDate':
                $period = TIME_PERIOD_TODATE;
                $reportTitle = 'To Date Report';
                break;

            case 'today':
            default:
                $period = TIME_PERIOD_TODAY;
                $reportTitle = 'Today\'s Report';
                break;
        }

        $statistics = new Statistics($this->_siteID);
        $submissionJobOrdersRS = $statistics->getSubmissionJobOrders($period);

        foreach ($submissionJobOrdersRS as $rowIndex => $submissionJobOrdersData)
        {
            /* Querys inside loops are bad, but I don't think there is any avoiding this. */
            $submissionJobOrdersRS[$rowIndex]['submissionsRS'] = $statistics->getSubmissionsByJobOrder(
                $period, $submissionJobOrdersData['jobOrderID'], $this->_siteID
            );
        }

        if (!eval(Hooks::get('REPORTS_SHOW_SUBMISSION'))) return;

        $this->_template->assign('reportTitle', $reportTitle);
        $this->_template->assign('submissionJobOrdersRS', $submissionJobOrdersRS);
        $this->_template->display('./modules/reports/SubmissionReport.tpl');
    }

    private function showPlacementReport()
    {
        //FIXME: getTrimmedInput
        if (isset($_GET['period']) && !empty($_GET['period']))
        {
            $period = $_GET['period'];
        }
        else
        {
            $period = '';
        }


        switch ($period)
        {
            case 'yesterday':
                $period = TIME_PERIOD_YESTERDAY;
                $reportTitle = 'Yesterday\'s Report';
                break;

            case 'thisWeek':
                $period = TIME_PERIOD_THISWEEK;
                $reportTitle = 'This Week\'s Report';
                break;

            case 'lastWeek':
                $period = TIME_PERIOD_LASTWEEK;
                $reportTitle = 'Last Week\'s Report';
                break;

            case 'thisMonth':
                $period = TIME_PERIOD_THISMONTH;
                $reportTitle = 'This Month\'s Report';
                break;

            case 'lastMonth':
                $period = TIME_PERIOD_LASTMONTH;
                $reportTitle = 'Last Month\'s Report';
                break;

            case 'thisYear':
                $period = TIME_PERIOD_THISYEAR;
                $reportTitle = 'This Year\'s Report';
                break;

            case 'lastYear':
                $period = TIME_PERIOD_LASTYEAR;
                $reportTitle = 'Last Year\'s Report';
                break;

            case 'toDate':
                $period = TIME_PERIOD_TODATE;
                $reportTitle = 'To Date Report';
                break;

            case 'today':
            default:
                $period = TIME_PERIOD_TODAY;
                $reportTitle = 'Today\'s Report';
                break;
        }

        $statistics = new Statistics($this->_siteID);
        $placementsJobOrdersRS = $statistics->getPlacementsJobOrders($period);

        foreach ($placementsJobOrdersRS as $rowIndex => $placementsJobOrdersData)
        {
            /* Querys inside loops are bad, but I don't think there is any avoiding this. */
            $placementsJobOrdersRS[$rowIndex]['placementsRS'] = $statistics->getPlacementsByJobOrder(
                $period, $placementsJobOrdersData['jobOrderID'], $this->_siteID
            );
        }

        if (!eval(Hooks::get('REPORTS_SHOW_SUBMISSION'))) return;

        $this->_template->assign('reportTitle', $reportTitle);
        $this->_template->assign('placementsJobOrdersRS', $placementsJobOrdersRS);
        $this->_template->display('./modules/reports/PlacedReport.tpl');
    }

    private function customizeJobOrderReport()
    {
 	/*echo "<pre>";
	print_r($_GET);
	exit(); */ 
        /* Bail out if we don't have a valid candidate ID. */
        if (!$this->isRequiredIDValid('jobOrderID', $_GET))
        {
            CommonErrors::fatal(COMMONERROR_BADINDEX, $this, 'Invalid job order ID.');
        }

        $jobOrderID = $_GET['jobOrderID'];

        $siteName = $_SESSION['RESUFLO']->getSiteName();

		//$startDate = date("Y-m-d", strtotime($_GET['start_date']));
                $arrdate=explode("-",$_GET['start_date']);
		$startDate= $arrdate[2]."-".$arrdate[1]."-".$arrdate[0];
		$startDate   = date("Y-m-d", strtotime($startDate));
                
		$arrdate=explode("-",$_GET['end_date']);
		$endDate= $arrdate[2]."-".$arrdate[1]."-".$arrdate[0];
		$endDate   = date("Y-m-d", strtotime($endDate));
		$id=0;
        $statistics = new Statistics($this->_siteID);
        $data = $statistics->getJobOrderReport($jobOrderID,$id,$startDate,$endDate);
//        var_dump($data);
		$stageData = $statistics->getJoborderData($jobOrderID);
		/*echo "<pre>";
		print_r($data);
		exit(); */	
		foreach ($data as $rowData => $stageDat)
					{
						if($rowData=="pipeline")
							{
								$reportParameters[0]['dataname0']="Pipeline";
								$reportParameters[0]['dataSet0']=$stageDat;
								
							}
					}
		$i=1;
		foreach ($stageData as $rowNumber => $stageData2)
			{
				foreach ($data as $row => $stageData3)
					{
						if($row=="pipeline")
							{
								
								
							}
						if($stageData2==$row)
							{
								$reportParameters[$i]['dataname'.$i]=$row;
								$reportParameters[$i]['dataSet'.$i]=$stageData3;
								$i++;
							}
						
					}
			}

		
	
        /* Bail out if we got an empty result set. */
        if (empty($data))
        {
            CommonErrors::fatal(COMMONERROR_BADINDEX, $this, 'The specified job order ID could not be found.');
        }
        $reportParameters['siteName'] = $siteName;
        $reportParameters['companyName'] = $data['companyName'];
        $reportParameters['jobOrderName'] = $data['title'];
        $reportParameters['accountManager'] = $data['ownerFullName'];
        $reportParameters['recruiter'] = $data['recruiterFullName'];
		$reportParameters['recID'] = $data['recID'];
		$reportParameters['userID'] = $data['userID'];
		

        $reportParameters['periodLine'] = sprintf(
            '%s - %s',
            strtok($data['dateCreated'], ' '),
            DateUtility::getAdjustedDate('m-d-y')
        );
		$reportParameters['periodLinestartDate']=$_GET['start_date'];
		$reportParameters['periodLineendDate']=$_GET['end_date'];
      /*   $reportParameters['dataSet1'] = $data['pipeline'];
        $reportParameters['dataSet2'] = $data['contacted'];
        $reportParameters['dataSet3'] = $data['1stinterview'];
        $reportParameters['dataSet4'] = $data['apptitudeTest'];
		$reportParameters['dataSet5'] = $data['2ndinterview'];
        $reportParameters['dataSet6'] = $data['backgroungCheck'];
        $reportParameters['dataSet7'] = $data['pending'];
        $reportParameters['dataSet8'] = $data['licensing'];
		$reportParameters['dataSet9'] = $data['reserve'];
        $reportParameters['dataSet10'] = $data['contracted']; */

        $dataSet = array(
            $reportParameters[4]['dataSet4'],
            $reportParameters[3]['dataSet3'],
            $reportParameters[2]['dataSet2'],
            $reportParameters[1]['dataSet1']
        );
		
	

        $this->_template->assign('reportParameters', $reportParameters);
        $this->_template->assign('active', $this);
        $this->_template->assign('subActive', '');
        $this->_template->display('./modules/reports/JobOrderReport.tpl');
    }
	 private function customizeJobOrderReportFirst()
    {
        /* Bail out if we don't have a valid candidate ID. */
        if (!$this->isRequiredIDValid('jobOrderID', $_GET))
        {
            CommonErrors::fatal(COMMONERROR_BADINDEX, $this, 'Invalid job order ID.');
        }

        $jobOrderID = $_GET['jobOrderID'];

        $siteName = $_SESSION['RESUFLO']->getSiteName();


         $statistics = new Statistics($this->_siteID);
        $data = $statistics->getJobOrderReport($jobOrderID);
		/*$stageData = $statistics->getJoborderData($jobOrderID);
	
		foreach ($data as $rowData => $stageDat)
					{
						if($rowData=="pipeline")
							{
								$reportParameters[0]['dataname0']="Pipeline";
								$reportParameters[0]['dataSet0']=$stageDat;
								
							}
					}
		$i=1;
		foreach ($stageData as $rowNumber => $stageData2)
			{
				foreach ($data as $row => $stageData3)
					{
						if($row=="pipeline")
							{
								$reportParameters[$i]['dataname'.$i]="Screened";
								$reportParameters[$i]['dataSet'.$i]=$stageData3;
								
							}
						if($stageData2==$row)
							{
								$reportParameters[$i]['dataname'.$i]=$row;
								$reportParameters[$i]['dataSet'.$i]=$stageData3;
								$i++;
							}
						
					}
			}

		
	
        /* Bail out if we got an empty result set. */
        if (empty($data))
        {
            CommonErrors::fatal(COMMONERROR_BADINDEX, $this, 'The specified job order ID could not be found.');
        }
        $reportParameters['siteName'] = $siteName;
        $reportParameters['companyName'] = $data['companyName'];
        $reportParameters['jobOrderName'] = $data['title'];
        $reportParameters['accountManager'] = $data['ownerFullName'];
        $reportParameters['recruiter'] = $data['recruiterFullName'];
		$reportParameters['recID'] = $data['recID'];
		$reportParameters['userID'] = $data['userID'];
		$reportParameters['jobOrderID']=$jobOrderID;
		

        $reportParameters['periodLine'] = sprintf(
            '%s : %s',
            strtok($data['dateCreated'], ' '),
            DateUtility::getAdjustedDate('m-d-y')
        );
		$getDate=explode(":",$reportParameters['periodLine']);
		$getDate=explode("-",$getDate[0]);
		$chnDate=$getDate[0]."/".$getDate[1]."/20".$getDate[2];
		
		$reportParameters['periodLinestartdate']=date("d-m-Y", strtotime($chnDate));
		$reportParameters['periodLineenddate']=DateUtility::getAdjustedDate('d-m-Y'); 
      /*   $reportParameters['dataSet1'] = $data['pipeline'];
        $reportParameters['dataSet2'] = $data['contacted'];
        $reportParameters['dataSet3'] = $data['1stinterview'];
        $reportParameters['dataSet4'] = $data['apptitudeTest'];
		$reportParameters['dataSet5'] = $data['2ndinterview'];
        $reportParameters['dataSet6'] = $data['backgroungCheck'];
        $reportParameters['dataSet7'] = $data['pending'];
        $reportParameters['dataSet8'] = $data['licensing'];
		$reportParameters['dataSet9'] = $data['reserve'];
        $reportParameters['dataSet10'] = $data['contracted']; */

        $dataSet = array(
            $reportParameters[4]['dataSet4'],
            $reportParameters[3]['dataSet3'],
            $reportParameters[2]['dataSet2'],
            $reportParameters[1]['dataSet1']
        );
		
	
	

        $this->_template->assign('reportParameters', $reportParameters);
        $this->_template->assign('active', $this);
        $this->_template->assign('subActive', '');
        $this->_template->display('./modules/reports/JobOrderReport2.tpl');
    }

    private function customizeEEOReport()
    {
        $this->_template->assign('modePeriod', 'all');
        $this->_template->assign('modeStatus', 'all');
        $this->_template->assign('active', $this);
        $this->_template->assign('subActive', '');
        $this->_template->display('./modules/reports/EEOReport.tpl');
    }

    private function generateJobOrderReportPDF()
    {
	/* 	echo "<pre>";
		print_r($_GET);
		exit();   */
        /* E_STRICT doesn't like FPDF. */
        $errorReporting = error_reporting();
        error_reporting($errorReporting & ~ E_STRICT);
        include_once('./lib/fpdf/fpdf.php');
        error_reporting($errorReporting);

        // FIXME: Hook?
        $isASP = $_SESSION['RESUFLO']->isASP();

        $unixName = $_SESSION['RESUFLO']->getUnixName();

        $siteName       = $this->getTrimmedInput('siteName', $_GET);
        $companyName    = $this->getTrimmedInput('companyName', $_GET);
        $jobOrderName   = $this->getTrimmedInput('jobOrderName', $_GET);
        $periodLine     = $this->getTrimmedInput('startdate', $_GET)."  ".$this->getTrimmedInput('enddate', $_GET);
        $accountManager = $this->getTrimmedInput('accountManager', $_GET);
        $recruiter      = $this->getTrimmedInput('recruiter', $_GET);
        $notes          = $this->getTrimmedInput('notes', $_GET);
		$ratioFirst		= $this->getTrimmedInput('RatiodataSet1Label', $_GET);
		$ratioSecond	= $this->getTrimmedInput('RatiodataSet1Labe2', $_GET);
		$ratioFirstpos	= strpos($ratioFirst,",");
		$ratioSecondpos	= strpos($ratioSecond,",");
		if(!empty($ratioFirstpos))
		{
			$ratio1Data=explode(",",$ratioFirst);
			$ratioDataY[0]=$ratio1Data[0];
			$ratioDataX[0]=$ratio1Data[1];
		}
		if(!empty($ratioSecondpos))
		{
			$ratio2Data=explode(",",$ratioSecond);
			$ratioDataY[1]=$ratio2Data[0];
			$ratioDataX[1]=$ratio2Data[1];
		}
		$ratioDatasetY=implode(",",$ratioDataY);
		$ratioDatasetX=implode(",",$ratioDataX);
	
		
       /*  if (isset($_GET['dataSet']))
        {
            $dataSet = $_GET['dataSet'];
            $dataSet = explode(',', $dataSet);
        }
        else
        {
            $dataSet = array(4, 3, 2, 1);
        } */


        /* PDF Font Face. */
        // FIXME: Customizable.
        $fontFace = 'Arial';

        $pdf = new FPDF();
        $pdf->AddPage();

        if (!eval(Hooks::get('REPORTS_CUSTOMIZE_JO_REPORT_PRE'))) return;

        if ($isASP && $unixName == 'cognizo')
        {
            /* TODO: MAKE THIS CUSTOMIZABLE FOR EVERYONE. */
            $pdf->SetFont($fontFace, 'B', 10);
            $pdf->Image('images/cognizo-logo.jpg', 130, 10, 59, 20);
            $pdf->SetXY(129,27);
            $pdf->Write(5, 'Information Technology Consulting');
        }

        $pdf->SetXY(25, 20);
        $pdf->SetFont($fontFace, 'BU', 14);
        $pdf->Write(5, "Recruiting Summary Report\n");

        $pdf->SetFont($fontFace, '', 10);
        $pdf->SetX(25);
        $pdf->Write(5, DateUtility::getAdjustedDate('l, F d, Y') . "\n\n\n");

        $pdf->SetFont($fontFace, 'B', 10);
        $pdf->SetX(25);
        $pdf->Write(5, 'Company: '. $companyName . "\n");

        $pdf->SetFont($fontFace, '', 10);
        $pdf->SetX(25);
        $pdf->Write(5, 'Position: ' . $jobOrderName . "\n\n");

        $pdf->SetFont($fontFace, '', 10);
        $pdf->SetX(25);
        $pdf->Write(5, 'Period: ' . $periodLine . "\n\n");

        $pdf->SetFont($fontFace, '', 10);
        $pdf->SetX(25);
        $pdf->Write(5, 'Account Manager: ' . $accountManager . "\n");

        $pdf->SetFont($fontFace, '', 10);
        $pdf->SetX(25);
        $pdf->Write(5, 'Recruiter: ' . $recruiter . "\n");
		for($i=0;$i<$_GET['total'];$i++)
		{
			$dataSet2['name'.$i]=substr($_GET['dataSet1NameLabel'.$i], 0, 4);
			
		}
		for($i=0;$i<$_GET['total'];$i++)
		{
			$dataSet['value'.$i]=$_GET['dataSet'.$i];
		}

        /* Note that the server is not logged in when getting this file from
         * itself.
         */
        // FIXME: Pass session cookie in URL? Use cURL and send a cookie? I
        //        really don't like this... There has to be a way.
        // FIXME: "could not make seekable" - http://demo.catsone.net/index.php?m=graphs&a=jobOrderReportGraph&data=%2C%2C%2C
        //        in /usr/local/www/catsone.net/data/lib/fpdf/fpdf.php on line 1500
        $URI = RESUFLOUtility::getAbsoluteURI(
            RESUFLOUtility::getIndexName()
            . '?m=graphs&a=jobOrderReportGraph&data='
            . urlencode(implode(',', $dataSet)).'&data2='.urlencode(implode(',', $dataSet2))
        );
	

        $pdf->Image($URI, 10, 75, 190, 80, 'jpg');
		for($i=0;$i<$_GET['total'];$i++)
		{
			if($i==0)
			{
			$pdf->SetXY(25,165);
			$pdf->SetFont($fontFace, '', 10);
			$pdf->Write(5, 'Total Candidates ');
			$pdf->SetTextColor(255, 0, 0);
			$pdf->Write(5,$_GET['dataSet1NameLabel'.$i]);
			$pdf->SetTextColor(0, 0, 0);
			$pdf->Write(5, ' by ' . $accountManager . ": \n");
			}
			else
			{
				$pdf->SetX(25);
				$pdf->SetFont($fontFace, '', 10);
				$pdf->Write(5, 'Total Candidates ');
				$pdf->SetTextColor(255, 0, 0);
				$pdf->Write(5,$_GET['dataSet1NameLabel'.$i]);
				$pdf->SetTextColor(0, 0, 0);
				$pdf->Write(5, ' by ' . $accountManager . ": \n");
			}
		}
                //Display ration
                $rationSet1Label1 = $_GET['RatiodataSet1Label'];
                $rationSet1Label2 = $_GET['RatiodataSet1Labe2'];

                $rationSet1Label1Name = explode(",",$rationSet1Label1)[0];
                $rationSet1Label1Value = explode(",",$rationSet1Label1)[1];
                
                $rationSet1Label2Name = explode(",",$rationSet1Label2)[0];
                $rationSet1Label2Value = explode(",",$rationSet1Label2)[1];
		
                $t=165;
		for($i=0;$i<$_GET['total'];$i++)
		{
                    //continue;
                    $pdf->SetXY(165,$t);
                    $pdf->SetFont($fontFace, 'B', 10);
                    $pdf->Write(5,$_GET['dataSet'.$i]. "\n");
                    
                    if($_GET['dataSet1NameLabel'.$i] == $rationSet1Label1Name || $_GET['dataSet1NameLabel'.$i] == $rationSet1Label2Name || $rationSet1Label2Name == "All"){
                        $rationPercentage = $_GET['dataSet'.$i] * 100;
                        if($rationSet1Label1Value != 0){
                            $rationPercentage = $rationPercentage/$rationSet1Label1Value;
                        }
                        $pdf->SetXY(175,$t);
                        $pdf->Write(5,sprintf('%0.2f', $rationPercentage)."%". "\n");
                    }
                    
                    $t=$t+5;
			
		}	
       
        if ($notes != '')
        {
            $pdf->SetX(25);
            $pdf->SetFont($fontFace, '', 10);
            $pdf->Write(10, "Notes:\n");

            $len = strlen($notes);
			$notes=substr($notes,0,200);
            $maxChars = 70;
            $pdf->SetLeftMargin(25);
            $pdf->SetRightMargin(25);
            $pdf->SetX(25);
            $pdf->Write(5, $notes . "\n");
        }
	
		if(!empty($ratioDatasetX) && $rationSet1Label2 != "All")
		{
		
		
				 $RATIOURI = RESUFLOUtility::getAbsoluteURI(
					RESUFLOUtility::getIndexName()
					. '?m=graphs&a=jobOrderReportGraph&data='
					. urlencode($ratioDatasetX).'&data2='.urlencode($ratioDatasetY)
				);
                                //$RATIOURI = 'http://localhost/resuflodevelopment/index.php?m=graphs&a=jobOrderReportGraph&data=9,6,72,31,0,6,0,2,0,0,0,0,0&data2=Pipeline,Scheduled,No Show,1st Int,2nd Int,BIG Sent,BIG Comp,BIG Passed,BIG Fail,Lic Start,Lic Comp,Reserve,Career';
                                //echo $RATIOURI;
			$pdf->SetX(25);
            $pdf->SetFont($fontFace, '', 10);
            $pdf->Write(10, "Ratio:\n");

			$ratio = ($ratioDataX[1]/$ratioDataX[0])*100;
			$rtio = number_format($ratio, 2, '.', '');
			$pdf->SetX(25);
			$pdf->SetFont($fontFace, '', 10);
			$pdf->SetTextColor(0, 0, 0);
			$pdf->Write(5,'Ratio between selected stages:'.$rtio.'%'."\n");

			$pdf->Image($RATIOURI,120,250, 70, 40, 'jpg');
		}
        $pdf->Rect(3, 6, 319, 285);

        if (!eval(Hooks::get('REPORTS_CUSTOMIZE_JO_REPORT_POST'))) return;

        $pdf->Output();
        die();
    }

    function generateEEOReportPreview()
    {
        $modePeriod = $this->getTrimmedInput('period', $_GET);
        $modeStatus = $this->getTrimmedInput('status', $_GET);

        $statistics = new Statistics($this->_siteID);
        $EEOReportStatistics = $statistics->getEEOReport($modePeriod, $modeStatus);

        //print_r($EEOReportStatistics);

        switch ($modePeriod)
        {
            case 'week':
                $labelPeriod = ' Last Week';
                break;

            case 'month':
                $labelPeriod = ' Last Month';
                break;

            default:
                $labelPeriod = '';
                break;
        }

        switch ($modeStatus)
        {
            case 'rejected':
                $labelStatus = ' Rejected';
                break;

            case 'placed':
                $labelStatus = ' Placed';
                break;

            default:
                $labelStatus = '';
                break;
        }

        /* Produce the URL to the ethic statistics graph. */
        $labels = array();
        $data = array();

        $rsEthnicStatistics = $EEOReportStatistics['rsEthnicStatistics'];

        foreach ($rsEthnicStatistics as $index => $line)
        {
            $labels[] = $line['EEOEthnicType'];
            $data[] = $line['numberOfCandidates'];
        }

        $urlEthnicGraph = RESUFLOUtility::getAbsoluteURI(
            sprintf("%s?m=graphs&a=generic&title=%s&labels=%s&data=%s&width=%s&height=%s",
                RESUFLOUtility::getIndexName(),
                urlencode('Number of Candidates'.$labelStatus.' by Ethnic Type'.$labelPeriod),
                urlencode(implode(',', $labels)),
                urlencode(implode(',', $data)),
                400,
                240
            ));


        /* Produce the URL to the veteran status statistics graph. */
        $labels = array();
        $data = array();

        $rsVeteranStatistics = $EEOReportStatistics['rsVeteranStatistics'];

        foreach ($rsVeteranStatistics as $index => $line)
        {
            $labels[] = $line['EEOVeteranType'];
            $data[] = $line['numberOfCandidates'];
        }

        $urlVeteranGraph = RESUFLOUtility::getAbsoluteURI(
            sprintf("%s?m=graphs&a=generic&title=%s&labels=%s&data=%s&width=%s&height=%s",
                RESUFLOUtility::getIndexName(),
                urlencode('Number of Candidates'.$labelStatus.' by Veteran Status'.$labelPeriod),
                urlencode(implode(',', $labels)),
                urlencode(implode(',', $data)),
                400,
                240
            ));

        /* Produce the URL to the gender statistics graph. */
        $labels = array();
        $data = array();

        $rsGenderStatistics = $EEOReportStatistics['rsGenderStatistics'];

        $labels[] = 'Male ('.$rsGenderStatistics['numberOfCandidatesMale'].')';
        $data[] = $rsGenderStatistics['numberOfCandidatesMale'];

        $labels[] = 'Female ('.$rsGenderStatistics['numberOfCandidatesFemale'].')';
        $data[] = $rsGenderStatistics['numberOfCandidatesFemale'];

        $urlGenderGraph = RESUFLOUtility::getAbsoluteURI(
            sprintf("%s?m=graphs&a=genericPie&title=%s&labels=%s&data=%s&width=%s&height=%s&legendOffset=%s",
                RESUFLOUtility::getIndexName(),
                urlencode('Number of Candidates by Gender'),
                urlencode(implode(',', $labels)),
                urlencode(implode(',', $data)),
                320,
                300,
                1.575
            ));

        if ($rsGenderStatistics['numberOfCandidatesMale'] == 0 && $rsGenderStatistics['numberOfCandidatesFemale'] == 0)
        {
            $urlGenderGraph = "images/noDataByGender.png";
        }

        /* Produce the URL to the disability statistics graph. */
        $labels = array();
        $data = array();

        $rsDisabledStatistics = $EEOReportStatistics['rsDisabledStatistics'];

        $labels[] = 'Disabled ('.$rsDisabledStatistics['numberOfCandidatesDisabled'].')';
        $data[] = $rsDisabledStatistics['numberOfCandidatesDisabled'];

        $labels[] = 'Non Disabled ('.$rsDisabledStatistics['numberOfCandidatesNonDisabled'].')';
        $data[] = $rsDisabledStatistics['numberOfCandidatesNonDisabled'];

        $urlDisabilityGraph = RESUFLOUtility::getAbsoluteURI(
            sprintf("%s?m=graphs&a=genericPie&title=%s&labels=%s&data=%s&width=%s&height=%s&legendOffset=%s",
                RESUFLOUtility::getIndexName(),
                urlencode('Number of Candidates by Disability Status'),
                urlencode(implode(',', $labels)),
                urlencode(implode(',', $data)),
                320,
                300,
                1.575
            ));

        if ($rsDisabledStatistics['numberOfCandidatesNonDisabled'] == 0 && $rsDisabledStatistics['numberOfCandidatesDisabled'] == 0)
        {
            $urlDisabilityGraph = "images/noDataByDisability.png";
        }

        $EEOSettings = new EEOSettings($this->_siteID);
        $EEOSettingsRS = $EEOSettings->getAll();

        $this->_template->assign('EEOReportStatistics', $EEOReportStatistics);
        $this->_template->assign('urlEthnicGraph', $urlEthnicGraph);
        $this->_template->assign('urlVeteranGraph', $urlVeteranGraph);
        $this->_template->assign('urlGenderGraph', $urlGenderGraph);
        $this->_template->assign('urlDisabilityGraph', $urlDisabilityGraph);
        $this->_template->assign('modePeriod', $modePeriod);
        $this->_template->assign('modeStatus', $modeStatus);
        $this->_template->assign('EEOSettingsRS', $EEOSettingsRS);
        $this->_template->assign('active', $this);
        $this->_template->assign('subActive', '');
        $this->_template->display('./modules/reports/EEOReport.tpl');
    }
}

?>
