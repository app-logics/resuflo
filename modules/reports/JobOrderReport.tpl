<?php /* $Id: JobOrderReport.tpl 2441 2007-05-04 20:42:02Z brian $ */ ?>
<?php TemplateUtility::printHeader('Pipeline Report', array('modules/joborders/validator.js', 'js/company.js', 'js/sweetTitles.js')); ?>
<?php TemplateUtility::printHeaderBlock(); ?>
<?php TemplateUtility::printTabs($this->active, $this->subActive); ?>
<script type="text/javascript">
function getReportValue(){
}
</script>
    <div id="main">
        <?php TemplateUtility::printQuickSearch(); ?>
        <div id="contents">
            <table>
                <tr>
                    <td width="3%">
                        <img src="images/job_orders.gif" width="24" height="24" border="0" alt="Job Orders" style="margin-top: 3px;" />&nbsp;
                    </td>
                    <td><h2>Reports: Pipeline Report</h2></td>
                </tr>
            </table>
            <p class="note">Generate a Pipeline report.</p>
            <form name="jobOrderReportForm" id="jobOrderReportForm" action="<?php echo(RESUFLOUtility::getIndexName()); ?>" method="get">
                <input type="hidden" name="m" value="reports">
                <input type="hidden" name="a" value="generateJobOrderReportPDF">

                <table class="editTable" width="700">
                    <tr>
                        <td class="tdVertical" style="width: 150px;">
                            <label id="siteNameLabel" for="siteName">Company Name:</label>
                        </td>
                        <td class="tdData">
                            <input type="text" class="inputbox" name="siteName" id="siteName" value="<?php $this->_($this->reportParameters['siteName']); ?>" style="width: 250px;" />&nbsp;*
                        </td>
                    </tr>

                    <tr>
                        <td class="tdVertical">
                            <label id="companyNameLabel" for="companyName">Company:</label>
                        </td>
                        <td class="tdData">
                            <input type="text" class="inputbox" name="companyName" id="companyName" value="<?php $this->_($this->reportParameters['companyName']); ?>" style="width: 250px;" />&nbsp;*
                        </td>
                    </tr>

                    <tr>
                        <td class="tdVertical">
                            <label id="jobOrderNameLabel" for="jobOrderName">Position (Title):</label>
                        </td>
                        <td class="tdData">
                            <input type="text" class="inputbox" name="jobOrderName" id="jobOrderName" value="<?php $this->_($this->reportParameters['jobOrderName']); ?>" style="width: 250px;" />&nbsp;*
                        </td>
                    </tr>

                    <tr>
                        <td class="tdVertical">
                            <label id="periodLineLabel" for="periodLine">Period:</label>
                        </td>
                        <td class="tdData">
						 <input type="hidden" name="startdate" value="<?php echo $this->_($this->reportParameters['periodLinestartDate']); ?>">
						 <input type="hidden" name="enddate" value="<?php echo $this->_($this->reportParameters['periodLineendDate']); ?>">
                            <?php echo $this->_($this->reportParameters['periodLinestartDate']); ?>   <?php echo $this->_($this->reportParameters['periodLineendDate']); ?>
                        </td>
                    </tr>

                    <tr>
                        <td class="tdVertical">
                            <label id="accountManagerLabel" for="accountManager">Account Manager:</label>
                        </td>
                        <td class="tdData">
                            <input type="text" class="inputbox" name="accountManager" id="accountManager" value="<?php $this->_($this->reportParameters['accountManager']); ?>" style="width: 250px;" />&nbsp;*
                        </td>
                    </tr>

                    <tr>
                        <td class="tdVertical">
                            <!--<label id="recruiterLabel" for="recruiter">Recruiter:</label>--!>
                        </td>
                        <td class="tdData">
						<?php if($_SESSION['RESUFLO']->getClientLogin()==1 && $_SESSION['RESUFLO']->getAccessLevel()==400){ 
						$sql = mysql_query("select * from company where owner = ".$_SESSION['resumeinsertuserid']);
						while($data = mysql_fetch_array($sql)){
							$ownerCompanyName = $data['name'];
						}
						$recID = $this->reportParameters['userID'];
						?>
       		<select class="inputbox" name="recruiter" id="recruiter"  style="width: 250px;display:none;"" onChange="OnChange();" />&nbsp;*
				<option <?php if($_REQUEST['recId']==$_SESSION['resumeinsertuserid']){ ?> selected="selected" <?php } ?> value="<?php echo $_SESSION['resumeinsertuserid']; ?>"><?php echo $ownerCompanyName; ?></option>
				<option <?php if($_REQUEST['recId']==$this->reportParameters['userID']){ ?> selected="selected" <?php } ?> value="<?php $this->_($this->reportParameters['userID']); ?>"><?php $this->_($this->reportParameters['recruiter']); ?></option>
			</select>
			 <input type="hidden" name="jobOrderID" id="jobOrderID" value="<?php echo $_REQUEST['jobOrderID']; ?>">	
			 			
		<?php }else if($_SESSION['RESUFLO']->getClientLogin()==1 && $_SESSION['RESUFLO']->getAccessLevel()!=400){ ?>
			<input type="text" class="inputbox" name="recruiter" id="recruiter" value="<?php $this->_($this->reportParameters['recruiter']); ?>" style="width: 250px;" />&nbsp;*
		<?php } ?>
                            
                        </td>
                    </tr>
                </table>

                <table class="editTable" width="700">
                    <input type="hidden" name="dataSet" id="dataSet" value="0,0,0,0,0,0,0,0,0,0">
                    <script type="text/javascript">
                        function setDataSet()
                        {
                            	document.getElementById('dataSet').value =
                                document.getElementById('dataSet1').value + ',' +
                                document.getElementById('dataSet2').value + ',' +
                                document.getElementById('dataSet3').value + ',' +
								document.getElementById('dataSet4').value + ',' +
                                document.getElementById('dataSet5').value + ',' +
                                document.getElementById('dataSet6').value + ',' +
								document.getElementById('dataSet7').value + ',' +
                                document.getElementById('dataSet8').value + ',' +
                                document.getElementById('dataSet9').value;
								/*document.getElementById('dataSet10').value + ',' +
                                document.getElementById('dataSet11').value + ',' +
                                document.getElementById('dataSet12').value + ',' +
								document.getElementById('dataSet13').value + ',' +
                                document.getElementById('dataSet14').value;*/


                        }
                    </script>
					<?php
					$i=0;
					foreach($this->reportParameters as $item=>$data)
					{ if(is_array($data)){?>
					<tr>
					<td class="tdVertical" style="width: 150px;">
					 <label id="dataSet1Label<?php echo $i;?>"for="dataSet<?php echo $i;?>"><?php echo $data['dataname'.$i];?></label></td>
                                        <td class="tdData">
					<input type="hidden" name="dataSet1NameLabel<?php echo $i;?>" value="<?php echo $data['dataname'.$i];?>">
					<input type="text" class="inputbox report" name="dataSet<?php echo $i;?>" id="dataSet<?php echo $i;?>" value="<?php echo $data['dataSet'.$i]?>" style="width: 75px;" onchange="setDataSet();" />&nbsp;*
                                        &nbsp;&nbsp;&nbsp;&nbsp;&nbsp;<span class="reportPercent" style=""></span>
                                        </td>
                                        
						</tr>
					<?php
					$i++;
					}
					}
				
					?>
						<input type="hidden" name="total" value="<?php echo $i;?>">
					
                    <!--<tr>
                        <td class="tdVertical" style="width: 150px;">
                           
                        </td>
                        <td class="tdData">
                            <input type="text" class="inputbox" name="dataSet1" id="dataSet1" value="<?php $this->_($this->reportParameters['dataSet1']); ?>" style="width: 75px;" onchange="setDataSet();" />&nbsp;*
                        </td>
                    </tr>
				
					<tr>
                        <td class="tdVertical" style="width: 140px;">

                            <label id="dataSet2Label"for="dataSet2">Candidates Contacted:</label>
                        </td>
                        <td class="tdData">
					
                            <input type="text" class="inputbox" name="dataSet2" id="dataSet2" value="<?php $this->_($this->reportParameters['dataSet2']); ?>" style="width: 75px;" onchange="setDataSet();" />&nbsp;*
                        </td>
                    </tr>
					<tr>
                        <td class="tdVertical" style="width: 140px;">
                            <label id="dataSet3Label"for="dataSet3">Candidates 1st Interviewed:</label>
                        </td>
                        <td class="tdData">
                            <input type="text" class="inputbox" name="dataSet3" id="dataSet3" value="<?php $this->_($this->reportParameters['dataSet3']); ?>" style="width: 75px;" onchange="setDataSet();" />&nbsp;*
                        </td>
                    </tr>
                    <tr>
                        <td class="tdVertical">
                            <label id="dataSet4Label"for="dataSet4">Candidates Aptitude Test:</label>
                        </td>
                        <td class="tdData">
                            <input type="text" class="inputbox" name="dataSet4" id="dataSet4" value="<?php $this->_($this->reportParameters['dataSet4']); ?>" style="width: 75px;" onchange="setDataSet();" />&nbsp;*
                        </td>
                    </tr>
					<tr>
                        <td class="tdVertical" style="width: 140px;">
                            <label id="dataSet5Label"for="dataSet5">Candidates 2nd Interviewed:</label>
                        </td>
                        <td class="tdData">
                            <input type="text" class="inputbox" name="dataSet5" id="dataSet5" value="<?php $this->_($this->reportParameters['dataSet5']); ?>" style="width: 75px;" onchange="setDataSet();" />&nbsp;*
                        </td>
                    </tr>
					<tr>
                        <td class="tdVertical" style="width: 140px;">
                            <label id="dataSet6Label"for="dataSet6">Candidates Background Check:</label>
                        </td>
                        <td class="tdData">
                            <input type="text" class="inputbox" name="dataSet6" id="dataSet6" value="<?php $this->_($this->reportParameters['dataSet6']); ?>" style="width: 75px;" onchange="setDataSet();" />&nbsp;*
                        </td>
                    </tr>
					<tr>
                        <td class="tdVertical" style="width: 140px;">
                            <label id="dataSet7Label"for="dataSet7">Candidates Pending:</label>
                        </td>
                        <td class="tdData">
                            <input type="text" class="inputbox" name="dataSet7" id="dataSet7" value="<?php $this->_($this->reportParameters['dataSet7']); ?>" style="width: 75px;" onchange="setDataSet();" />&nbsp;*
                        </td>
                    </tr>
					<tr>
                        <td class="tdVertical" style="width: 140px;">
                            <label id="dataSet8Label"for="dataSet8">Candidates Licensing:</label>
                        </td>
                        <td class="tdData">
							
                            <input type="text" class="inputbox" name="dataSet8" id="dataSet8" value="<?php $this->_($this->reportParameters['dataSet8']); ?>" style="width: 75px;" onchange="setDataSet();" />&nbsp;*
                        </td>
                    </tr>
					<tr>
                        <td class="tdVertical" style="width: 140px;">
                            <label id="dataSet9Label"for="dataSet9">Candidates Reserved:</label>
                        </td>
                        <td class="tdData">
                            <input type="text" class="inputbox" name="dataSet9" id="dataSet9" value="<?php $this->_($this->reportParameters['dataSet9']); ?>" style="width: 75px;" onchange="setDataSet();" />&nbsp;*
                        </td>
                    </tr>
					<tr>
                        <td class="tdVertical" style="width: 140px;">
                            <label id="dataSet10Label"for="dataSet10">Candidates Contracted:</label>
                        </td>
                        <td class="tdData">
                            <input type="text" class="inputbox" name="dataSet10" id="dataSet10" value="<?php $this->_($this->reportParameters['dataSet10']); ?>" style="width: 75px;" onchange="setDataSet();" />&nbsp;*
                        </td>
                    </tr>--!>
					<!--<tr>
                        <td class="tdVertical" style="width: 140px;">
                            <label id="dataSet11Label"for="dataSet11">Candidates Offered:</label>
                        </td>
                        <td class="tdData">
                            <input type="text" class="inputbox" name="dataSet11" id="dataSet11" value="<?php $this->_($this->reportParameters['dataSet11']); ?>" style="width: 75px;" onchange="setDataSet();" />&nbsp;*
                        </td>
                    </tr>
					<tr>
                        <td class="tdVertical" style="width: 140px;">
                            <label id="dataSet12Label"for="dataSet12">Client Declined:</label>
                        </td>
                        <td class="tdData">
                            <input type="text" class="inputbox" name="dataSet12" id="dataSet12" value="<?php $this->_($this->reportParameters['dataSet12']); ?>" style="width: 75px;" onchange="setDataSet();" />&nbsp;*
                        </td>
                    </tr>
					                    <tr>
                        <td class="tdVertical">
                            <label id="dataSet13Label"for="dataSet13">Candidates Contracted:</label>
                        </td>
                        <td class="tdData">
                            <input type="text" class="inputbox" name="dataSet13" id="dataSet13" value="<?php $this->_($this->reportParameters['dataSet13']); ?>" style="width: 75px;" onchange="setDataSet();" />&nbsp;*
                        </td>
                    </tr>
                    <tr>
                        <td class="tdVertical">
                            <label id="dataSet14Label"for="dataSet14">Candidates Placed:</label>
                        </td>
                        <td class="tdData">
                            <input type="text" class="inputbox" name="dataSet14" id="dataSet14" value="<?php $this->_($this->reportParameters['dataSet14']); ?>" style="width: 75px;" onchange="setDataSet();" />&nbsp;*
                        </td>
                    </tr>-->

                </table>

                <script type="text/javascript">setDataSet();</script>

                <table class="editTable" width="700">
                    <tr>
                        <td class="tdVertical" style="width: 140px;">
                            <label id="notesLabel" for="notes">Misc. Notes:</label>
                        </td>
                        <td class="tdData">
                            <textarea class="inputbox" name="notes" id="notes" rows="5" style="width: 400px;" /></textarea>
                        </td>
                    </tr>
                </table>
				   <table class="editTable" width="700">
                    <tr>
                        <td class="tdVertical" style="width: 140px;">
                            <label id="ratio" for="notes">Ratio Between Stages:</label>
                        </td>
                        <td class="tdData">
						<select id="RatiodataSet1Label" name="RatiodataSet1Label" onchange="ShowPercentage()">
						<option value="select">select</option>
					<?php
					$i=0;
					foreach($this->reportParameters as $item2=>$data2)
					{ 
						if(is_array($data2))
						{?>
						<option name="<?php echo $data2['dataname'.$i];?>" index="<?php echo $i;?>" value="<?php echo $data2['dataname'.$i];?>,<?php echo $data2['dataSet'.$i]?>"><?php echo $data2['dataname'.$i];?></option> 
						<?php
						$i++;
						}
					}?>
					</select>
                                        <select id="RatiodataSet1Labe2" name="RatiodataSet1Labe2" onchange="ShowPercentage()">
					<option value="select">select</option>
                                        <option value="All" index="0">All</option>
					<?php
					$i=0;
					foreach($this->reportParameters as $item3=>$data3)
					{ 
						if(is_array($data3))
						{?>
						<option name="<?php echo $data3['dataname'.$i];?>" index="<?php echo $i;?>" value="<?php echo $data3['dataname'.$i];?>,<?php echo $data3['dataSet'.$i]?>"><?php echo $data3['dataname'.$i];?></option> 
						<?php
						$i++;
						}
					}?>
                        </select>
                        </td>
                    </tr>
                </table>

                <input type="submit" class="button" name="submit" value="Generate Report" />&nbsp;
                <input type="reset"  class="button" name="reset"  value="Reset" />&nbsp;
                
                <!-- IE PDF Hack -->
                <input type="hidden" name="ext" value=".pdf" />
            </form>

            <script type="text/javascript">
                document.jobOrderReportForm.siteName.focus();
            </script>
			<script type="text/javascript" >
                            function ShowPercentage(){
                                var firstStage = $("#RatiodataSet1Label").val();
                                var secondStage = $("#RatiodataSet1Labe2").val();
                                if(firstStage != "select" && secondStage != "select"){
                                    //var reportSum = parseFloat($("#RatiodataSet1Label option[value='"+firstStage+"']").attr("index"));
                                    //alert(reportSum);
                                    //alert(secondStage);
                                    if(secondStage == "All")
                                    {
                                        reportSum = firstStage.split(',')[1];
                                        var count = 0;
                                        $(".report").each(function(){
                                            var thisValue = parseInt($(this).val());
                                            var thisPercent = (thisValue*100)
                                            if(reportSum != 0){
                                                thisPercent = thisPercent/reportSum;
                                            }
                                            $(".reportPercent").eq(count).html(thisPercent.toFixed(2)+"%");
                                            //reportSum = reportSum + ;
                                            count = count + 1;
                                        });
                                    }
                                    else{
                                        reportSum = firstStage.split(',')[1];
                                        var count = 0;
                                        $(".report").each(function(){
                                            $(".reportPercent").eq(count).html("");
                                            count = count + 1;
                                        });
                                        var secondIndex = parseFloat($("#RatiodataSet1Labe2 option[value='"+secondStage+"']").attr("index"));
                                        var thisValue = parseInt($(".report").eq(secondIndex).val());
                                        var thisPercent = (thisValue*100)
                                        if(reportSum != 0){
                                            thisPercent = thisPercent/reportSum;
                                        }
                                        $(".reportPercent").eq(secondIndex).html(thisPercent.toFixed(2)+"%");
                                    }
                                }
                            }
			function OnChange()
			{
				 var recId = document.getElementById('recruiter').value;
				 var jobOrderID = document.getElementById('jobOrderID').value;
				 window.location="index.php?m=reports&a=customizeJobOrderReport&jobOrderID="+jobOrderID+"&recId="+recId;
			}
			</script>
        </div>
    </div>
    <div id="bottomShadow"></div>
<?php TemplateUtility::printFooter(); ?>
