<?php /* $Id: Users.tpl 2452 2007-05-11 17:47:55Z brian $ */ ?>
<?php TemplateUtility::printHeader('Settings', 'js/sorttable.js'); ?>
<?php TemplateUtility::printHeaderBlock(); ?>
<?php TemplateUtility::printTabs($this->active, $this->subActive); ?>
    <div id="main">
        <?php TemplateUtility::printQuickSearch(); ?>

        <div id="contents">
            <table>
                <tr>
                    <td width="3%">
                        <img src="images/settings.gif" width="24" height="24" alt="Settings" style="border: none; margin-top: 3px;" />&nbsp;
                    </td>
                    <td><h2>Settings: User Management</h2></td>
                </tr>
            </table>

            <p class="note">User Management</p>

            <table class="sortable" width="925">
                <thead>
                    <tr>
                        <!--<th align="left" nowrap="nowrap"><?php if($_SESSION['RESUFLO']->getAccessLevel()==400 && $_SESSION['RESUFLO']->getClientLogin()==1 ){ echo "Recruiter";}else{ echo "Company";}?> Name</th>-->
                        <!--<th align="left" nowrap="nowrap">Last Name</th>-->
						 <th align="left" nowrap="nowrap">Company Name</th>
						  <th align="left" nowrap="nowrap">Recruiter Name</th>
                        <th align="left">Username</th>
                        <th align="left" nowrap="nowrap">Access Level</th>
                        <th align="left" nowrap="nowrap">Last Success</th>
                        <th align="left" nowrap="nowrap">Last Fail</th>
                        <th align="left" nowrap="nowrap">Drip</th>
                        <th align="left" nowrap="nowrap">SMTP</th>
                        <th align="left" nowrap="nowrap"></th>
                    </tr>
                </thead>

                <?php if (!empty($this->rs)): ?>
                    <?php foreach ($this->rs as $rowNumber => $data): ?>
                        <tr class="<?php TemplateUtility::printAlternatingRowClass($rowNumber); ?>">
						<td valign="top" align="left">
						<?php if($data['recruiter']!=0 && $data['recruiter']!=1){
									$sql = mysql_query("select first_name from user  where user_id=".$data['recruiter']);
									$result = mysql_fetch_array($sql);
									echo $result['first_name'];
									
								}else{
								?><a href="<?php echo(RESUFLOUtility::getIndexName()); ?>?m=settings&amp;a=showUser&amp;userID=<?php $this->_($data['userID']); ?>">
								<?php echo $data['firstName']."</a>";
								}?>
						</td>
						<td valign="top" align="left">
						 <a href="<?php echo(RESUFLOUtility::getIndexName()); ?>?m=settings&amp;a=showUser&amp;userID=<?php $this->_($data['userID']); ?>">
                                   
									<?php if($data['recruiter']!=0 && $data['recruiter']!=1){
									$sql = mysql_query("select first_name from user  where user_id=".$data['recruiter']);
									$result = mysql_fetch_array($sql);
									echo $data['firstName'];}?>
									</a>
						</td>
                            <!--<td valign="top" align="left">
                                <a href="<?php echo(RESUFLOUtility::getIndexName()); ?>?m=settings&amp;a=showUser&amp;userID=<?php $this->_($data['userID']); ?>">
                                    <?php //$this->_($data['firstName']); ?>
									<?php if($data['recruiter']!=0 && $data['recruiter']!=1){
									$sql = mysql_query("select first_name from user  where user_id=".$data['recruiter']);
									$result = mysql_fetch_array($sql);
									echo $result['first_name']."(Recruiter : ".$data['firstName']." )";
									
									}else{
									
									echo $data['firstName'];
									}?>
                                </a>
                            </td>-->
                           <!-- <td valign="top" align="left">
                                <a href="<?php echo(RESUFLOUtility::getIndexName()); ?>?m=settings&amp;a=showUser&amp;userID=<?php $this->_($data['userID']); ?>">
                                    <?php $this->_($data['lastName']); ?>
                                </a>
                            </td>-->
                            <td valign="top" align="left"><?php $this->_($data['username']); ?></td>
                            <td valign="top" align="left"><?php $this->_($data['accessLevelDescription']); ?></td>
                            <td valign="top" align="left"><?php $this->_($data['successfulDate']); ?></td>
                            <td valign="top" align="left"><?php $this->_($data['unsuccessfulDate']); ?></td>
                            <td valign="top" align="left"><a title="<?php if($data['accessLevelDescription'] == 'Account Disabled'){echo 'Temporarily blocked due to disabled account';} elseif($data['isDripEnable'] == 1){echo 'Click to Disable';}else{echo 'Click to Enable';}; ?>" href="<?php if($data['accessLevelDescription'] == 'Account Disabled'){echo '#';}else{echo ((RESUFLOUtility::getIndexName()).'?m=settings&amp;a=toggleIsDripEnable&amp;userID='.$data['userID'] );} ?>"><?php if($data['accessLevelDescription'] == 'Account Disabled'){echo '<img src="images/circle_yellow.png" alt="" width="16"/>';} elseif($data['isDripEnable'] == 1){echo '<img src="images/circle_green.png" alt="" width="16"/>';}else{echo '<img src="images/circle_red.png" alt="" width="16"/>';}; ?></a></td>
                            <td valign="top" align="left"><a href="<?php echo(RESUFLOUtility::getIndexName()); ?>?m=settings&amp;a=toggleSMTP&amp;userID=<?php $this->_($data['userID']); ?>"><?php if($data['toggle_smtp'] == 1){echo 'Secondary';}else{echo 'Primary';}; ?></a></td>
                            <td valign="top" align="left">
                                <a href="<?php echo(RESUFLOUtility::getIndexName()); ?>?m=settings&amp;a=duplicateUser&amp;userID=<?php $this->_($data['userID']); ?>">Duplicate</a>
                            </td>
                        </tr>
                    <?php endforeach; ?>
                <?php endif; ?>
            </table>
            <a id="add_link" href="<?php echo(RESUFLOUtility::getIndexName()); ?>?m=settings&amp;a=addUser" title="Add new user" ><!--title="You have <?php $this->_($this->license['diff']); ?> user accounts remaining."-->
                <img src="images/candidate_inline.gif" width="16" height="16" class="absmiddle" alt="add" style="border: none;" />&nbsp;Add User
            </a>
        </div>
    </div>
    <div id="bottomShadow"></div>
<?php TemplateUtility::printFooter(); ?>
