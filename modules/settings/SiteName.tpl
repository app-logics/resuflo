<?php /* $Id: SiteName.tpl 1930 2007-02-22 08:39:53Z will $ */ ?>
<?php TemplateUtility::printHeader('Settings', array('modules/settings/validator.js')); ?>
<?php TemplateUtility::printHeaderBlock(); ?>
<?php TemplateUtility::printTabs($this->active, $this->subActive); ?>
    <div id="main">
        <?php TemplateUtility::printQuickSearch(); ?>

        <div id="contents">
            <table>
                <tr>
                    <td width="3%">
                        <img src="images/settings.gif" width="24" height="24" alt="Settings" style="border: none; margin-top: 3px;" />&nbsp;
                    </td>
                    <td><h2>Settings: Administration</h2></td>
                </tr>
            </table>

            <p class="note">Change Site Name and other settings</p>

            <table class="searchTable" width="100%">
                <tr>
                    <td>
                        <form action="<?php echo(RESUFLOUtility::getIndexName()); ?>?m=settings&amp;a=administration" id="changeSiteNameForm" onsubmit="return checkSiteNameForm(document.changeSiteNameForm);" method="post" autocomplete="off">
                            <input type="hidden" name="postback" value="postback" />
                            <input type="hidden" name="administrationMode" value="changeSiteName" />
                            Current site name: <?php echo($_SESSION['RESUFLO']->getSiteName())?><br />
							Current country key: <?php echo($_SESSION['RESUFLO']->getCountryKey())?><br />
                            <br />
                            <label id="siteNameLabel" for="siteName">New Site Name:</label>
                            <br />
                            <input type="text" name="siteName" id="siteName" value="<?php echo($_SESSION['RESUFLO']->getSiteName())?>" style="width:250px;" /><br /><br />
							 <label id="countryKeyLabel" for="countryKey">New Country key:</label>
                            <br />
                            <input type="text" name="countryKey" id="countryKey" value="<?php echo($_SESSION['RESUFLO']->getCountryKey())?>" style="width:250px;" /><br /><br />
							<label id="alert1" for="alert1">i.Alert for Number of days passed when resume is not updated</label>
							<select name="alert1status" >
							<option value="off" <?php if($_SESSION['RESUFLO']->getAlert1() == "off") {echo " selected";} ?>>Off</option>
							<option value="on" <?php if($_SESSION['RESUFLO']->getAlert1() == "on") {echo " selected";} ?>>On</option></select><br />
  Days for which alert should be send <input type="text" name="noofdays" value="<?php echo($_SESSION['RESUFLO']->getNoOfDays())?>" size="5" /><br /><br />

<label id="alert2" for="alert2">ii.	Alert for Hours Sending candidate for Interview and status is not changed</label>
							<select name="alert2status" >
							<option value="off" <?php if($_SESSION['RESUFLO']->getAlert1() == "off") {echo " selected";} ?>>Off</option>
							<option value="on" <?php if($_SESSION['RESUFLO']->getAlert1() == "on") {echo " selected";} ?>>On</option></select> <br />Hour difference between alert <input type="text" name="noofhour" value="<?php echo($_SESSION['RESUFLO']->getNoOfHour())?>" size="5" /><br /><br />

                            <input type="submit" name="save" class = "button" value="Save" />
                            <input type="button" name="back" class = "button" value="Back" onclick="document.location.href='<?php echo(RESUFLOUtility::getIndexName()); ?>?m=settings&amp;a=administration';" />
                        </form>
                    </td>
                </tr>
            </table>

        </div>
    </div>
    <div id="bottomShadow"></div>
<?php TemplateUtility::printFooter(); ?>
