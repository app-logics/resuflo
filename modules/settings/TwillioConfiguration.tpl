<?php /* $Id: ChangePassword.tpl 1927 2007-02-22 06:03:24Z will $ */ ?>
<?php TemplateUtility::printHeader('Settings', array('modules/settings/validator.js', 'js/sorttable.js')); ?>
<?php TemplateUtility::printHeaderBlock(); ?>
<?php TemplateUtility::printTabs($this->active, $this->subActive); ?>
<?php 
	$userid= $_SESSION['RESUFLO']->getuserid(); 
	$sql = mysql_query("select twillioSID, twillioToken, twillioFromNumber from user where user_id = '$userid'");
	$row =mysql_fetch_assoc($sql);
        $sid =$row['twillioSID'];
	$token= $row['twillioToken'];
        $fromNumber =$row['twillioFromNumber'];
?>

    <div id="main">
        <?php TemplateUtility::printQuickSearch(); ?>

        <div id="contents">
            <table>
                <tr>
                    <td width="3%">
                        <img src="images/settings.gif" width="24" height="24" border="0" alt="Settings" style="margin-top: 3px;" />&nbsp;
                    </td>
                    <td><h2>Settings: My Profile</h2></td>
                </tr>
            </table>

            <p class="note">Twillio Configuration</p>

            <form name="twillioConfigForm" id="twillioConfigForm" action="<?php echo(RESUFLOUtility::getIndexName()); ?>?m=settings&amp;a=twillioconfig" method="post" onsubmit="return checkTwillioConfigForm(document.twillioConfigForm);">
                <input type="hidden" name="postback" id="postback" value="postback" />
                <?php if ($this->isDemoUser): ?>
                    Note that as a demo user, you do not have privileges to modify any settings.
                    <br /><br />
                <?php endif; ?>

                <table class="searchTable">
                    <tr>
                        <td colspan="2">
                            <span class="bold">Twillio API Configuration</span>
                            <br />
                            <br />
                            <span id='passwordErrorMessage' style="font:smaller; color: red">
                                <?php if (isset($this->errorMessage)): ?>
                                        <?php $this->_($this->errorMessage); ?>
                                <?php endif; ?>
                            </span>
                        </td>
                    </tr>


                    <tr>
                        <td>
                            <label id="sidLabel" for="sid">Session ID:</label>&nbsp;
                        </td>
                        <td>
                            <input type="text" class="inputbox" id="sid" name="sid" style="width: 250px;" value="<?php echo $sid; ?>" />&nbsp;*
                        </td>
                    </tr>

                    <tr>
                        <td>
                            <label id="tokenLabel" for="token">Access Token:</label>&nbsp;
                        </td>
                        <td>
                            <input type="password" class="inputbox" id="token" name="token" style="width: 250px;" value="<?php echo $token; ?>" />&nbsp;*
                        </td>
                    </tr>

                    <tr>
                        <td>
                            <label id="fromNumberLabel" for="fromNumber">From Number:</label>&nbsp;
                        </td>
                        <td>
                            <input type="text" class="inputbox" id="fromNumber" name="fromNumber" value="<?php echo $fromNumber; ?>" />&nbsp;*
                        </td>
                    </tr>

                    <tr>
                        <td colspan="2">
                            <br />
                            <input type="submit" class="button" id="changePassword" onclick="if(document.getElementById('actualpwd').value!=document.getElementById('currentPassword').value){alert('Please enter correct current password');return false;}" name="changePassword" value="Save" />
                            <input type="button" name="back" class = "button" value="Back" onclick="document.location.href='<?php echo(RESUFLOUtility::getIndexName()); ?>?m=settings';" />
                       </td>
                    </tr>
                </table>
            </form>

            <script type="text/javascript">
                document.twillioConfigForm.sid.focus();
            </script>
        </div>
    </div>
    <div id="bottomShadow"></div>
<?php TemplateUtility::printFooter(); ?>
