<?php /* $Id: ChangePassword.tpl 1927 2007-02-22 06:03:24Z will $ */ ?>
<?php TemplateUtility::printHeader('Settings', array('modules/settings/validator.js', 'js/sorttable.js')); ?>
<?php TemplateUtility::printHeaderBlock(); ?>
<?php TemplateUtility::printTabs($this->active, $this->subActive); ?>

    <div id="main">
        <?php TemplateUtility::printQuickSearch(); ?>

        <div id="contents">
            <table>
                <tr>
                    <td width="3%">
                        <img src="images/settings.gif" width="24" height="24" border="0" alt="Settings" style="margin-top: 3px;" />&nbsp;
                    </td>
                    <td><h2>Settings: My Profile</h2></td>
                </tr>
            </table>

            <p class="note">Status Change Email Templates</p>

            <form name="calenderCred" id="calenderCred" action="<?php echo(RESUFLOUtility::getIndexName()); ?>?m=settings&amp;a=calender" method="post" >
			         <font style="font-weight:bold;" ><?php echo $this->msg; ?></font><br /><br />
                <table width="100%" border="0" cellspacing="0" cellpadding="0" bgcolor="#f8f8f8" >
  <tr>
    <td colspan="4" bgcolor="#f8f8f8">&nbsp;</td>
  </tr>
  <tr>
    <td colspan="4" bgcolor="#f8f8f8" style="padding-left:10px;"><img src="images/button_camp.gif" width="158" border="0" height="31" /></a></td>
    </tr>
  <tr>
    <td colspan="4" bgcolor="#F8F8F8" class="font_tab">&nbsp;</td>
    </tr>
  <tr>
  	<td width="33%" height="35" bgcolor="#e2e2e2" class="font_tab">Campaigns</td>
    <td width="23%" bgcolor="#e2e2e2" class="font_tab">No of Emails</td>
    <td width="22%" bgcolor="#e2e2e2" class="font_tab"></td>
    <td width="14%" bgcolor="#e2e2e2" class="font_tab" >Action</td>
  </tr>
  <tr>
  	<td width="33%" height="35" bgcolor="#e2e2e2" class="font_tab">Campaigns</td>
    <td width="23%" bgcolor="#e2e2e2" class="font_tab">No of Emails</td>
    <td width="22%" bgcolor="#e2e2e2" class="font_tab"></td>
    <td width="14%" bgcolor="#e2e2e2" class="font_tab" >Action</td>
  </tr>
  </table>
            </form>

            <script type="text/javascript">
                document.calenderCred.username.focus();
            </script>
        </div>
    </div>
    <div id="bottomShadow"></div>
<?php TemplateUtility::printFooter(); ?>
