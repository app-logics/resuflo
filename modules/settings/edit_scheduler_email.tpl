<?php /* $Id: Companies.tpl 3460 2007-11-07 03:50:34Z brian $ */ ?>
<?php TemplateUtility::printHeader('Companies', array('js/highlightrows.js', 'js/export.js', 'js/dataGrid.js','javascript/validations.js')); ?>
<?php TemplateUtility::printHeaderBlock(); ?>

<?php TemplateUtility::printTabs($this->active); ?>

<link rel="stylesheet" href="css/tab.css" />
<link rel="stylesheet" href="css/form.css" />
<script type="text/javascript" src="javascript/tab.js"></script>

<script type="text/javascript" src="javascript/validations.js"></script>
<!-- TinyMCE -->
<!--<script src="jscripts/tinymce/tinymce.min.js" type="text/javascript"></script>-->
<!-- /TinyMCE -->

<div id="main">
    
    <div id="contents">
        
        <!--tabs Starts-->		 
        <table width="100%" border="0" cellspacing="0" cellpadding="0" bgcolor="#fff" >
            <tr>
                <td colspan="4" bgcolor="#f8f8f8">&nbsp;</td>
            </tr>
            <tr>
                <td colspan="2"  bgcolor="#f8f8f8" >
                    <a  href="javascript:void(0)" href="javascript:void(0)" onclick="javascript:goToURL('<?php echo(RESUFLOUtility::getIndexName()); ?>?m=settings&a=schedulerEmail');" ><< Back to main page </a>
                </td></tr>
            <tr>
                <td colspan="4" bgcolor="#fff" >
                    <form name="frm1" id="frm1" action="<?php echo(RESUFLOUtility::getIndexName()); ?>?m=settings&a=editSchedulerEmail"   onsubmit="return statusTemplateEmailAdd()" method="post" enctype="multipart/form-data">
                        <table width="100%" border="0" cellspacing="2" cellpadding="2" bgcolor="#f8f8f8";>
                            <tr>
                                <td valign="top">Email Type:<font color="#FF0000">*</font></td>
                                <td align="left" valign="top">
                                    <?php if($this->eventTag == 'SCHEDULER_EMAIL_TEMPLATE_CONFIRMATION' ){
                                    ?>
                                        <input type="radio" id="eventTag"  name="eventTag"  value="SCHEDULER_EMAIL_TEMPLATE_CONFIRMATION" checked="checked"  />Scheduler Confirmation Email
                                        <input type="radio" id="eventTag"  name="eventTag"  value="SCHEDULER_EMAIL_TEMPLATE_REMINDER"  />Scheduler Reminder Email
                                    <?php
                                    }else {
                                    ?>
                                        <input type="radio" id="eventTag"  name="eventTag"  value="SCHEDULER_EMAIL_TEMPLATE_CONFIRMATION"   />Scheduler Confirmation Email
                                        <input type="radio" id="eventTag"  name="eventTag"  value="SCHEDULER_EMAIL_TEMPLATE_REMINDER" checked="checked" />Scheduler Reminder Email
                                    <?php	
                                    }
                                    ?>
                                    
                                </td>
                            </tr>
                            <tr class="reminder_days" style="<?php if($this->eventTag == 'SCHEDULER_EMAIL_TEMPLATE_CONFIRMATION' ){echo 'display: none';} ?>">
                                <td>No of Days <font color="#FF0000">*</font></td>
                                <td><label>
                                    <input type="text" id="reminder_days"  name="reminder_days" class="txtbox_s" value="<?php echo $this->reminder_days; ?>"  /></td>
                            </tr>
                            <tr>
                                <td>Subject of Email Template<font color="#FF0000">*</font></td>
                                <td><label>
                                    <input type="text" id="name"  name="name" class="txtbox" value="<?php echo $this->name ?>"  /></td>
                            </tr>
                            <input type="hidden" id="template_id"  name="template_id" class="txtbox" value="<?php echo $this->id ?>"  />
                            <tr>
                                <td valign="top">Upload Attachment:</td>
                                <td align="left" valign="top">
                                    <input type="file" id="uploadAttach"  name="uploadAttach"  value="" /><span><?php echo $this->attachment ?></span>
                                </td>
                            </tr>
                            <tr>
                                <td style="text-align:top">Message<font color="#FF0000">*</font>
                                    <div style="font-size:11px">
                                        <br/><br/><strong>Directions:<br /><small style="color:#FF0000">(Applicable for Scheduler confirmation)*</small></strong><br/>
                                        %DATETIME% for current Date and Time <br/>
                                        %SCHEDULEDATETIME% for schedule Date and Time <br/>
                                        %CANDFIRSTNAME% for Candidate First Name<br/>
                                        %CANDLASTNAME% for Candidate Last Name<br/>
                                        %CANDFULLNAME% for Candidate FullName<br/>
                                        %CANDEMAIL% for Candidate Email<br/>
                                        %CANDPHONE% for Candidate Phone<br/>
                                        %LOCATION% for Location<br/>
                                        %CLIENTNAME% for Recruiter Name<br/>
                                        %MAP% for Map<br/>
                                        %USERFULLNAME% for owner fullname<br />
                                        %RESCHEDULE% for Rescheduler Interview Link<br />
                                        %CANCEL% to Cancel Interview Link<br /><br />
                                        
                                    </div>
                                    
                                </td>
                                <td valign="top">
                                    <textarea name="txtmessage" id="txtmessage" rows="4" cols="40" style="height:300px;width:500px;" class="txtbox"><?php echo  $this->text; ?></textarea>
                                    <input name="image" type="file" id="upload" class="hidden" onchange="" style="display: none;">
                                </td>
                            </tr>
                            <tr>
                                <td>&nbsp;</td>
                                <td><input type="image" name="edit" src="images/save.gif" width="105" height="43" /></td>
                            </tr>
                            <tr>
                                <td>&nbsp;</td>
                                <td>&nbsp;</td>
                            </tr>
                        </table>   
                    </form>  
                </td>
            </tr>
            
        </table>
        <script src="jscripts/ckeditor/ckeditor.js" type="text/javascript"></script> 
        <script type="text/javascript">
            CKEDITOR.replace('txtmessage');
            $(function() {
                $('input[name="eventTag"]').on('click', function() {
                    if ($(this).val() == 'SCHEDULER_EMAIL_TEMPLATE_REMINDER') {
                        $('.reminder_days').show();
                    }
                    else {
                        $('.reminder_days').hide();
                    }
                });
            });
        </script>       
        <script type="text/javascript">// <![CDATA[
            var countries=new ddtabcontent("countrytabs")
            countries.setpersist(true)
            countries.setselectedClassTarget("link") //"link" or "linkparent"
            countries.init()
            // ]]></script>   
        <!--tabs end-->	
    </div>
</div>
<div id="bottomShadow"></div>
<?php TemplateUtility::printFooter(); ?>
