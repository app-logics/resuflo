<?php /* $Id: EditUser.tpl 2881 2007-08-14 07:47:26Z brian $ */ ?>
<?php TemplateUtility::printHeader('Settings', array('modules/settings/validator.js', 'js/sorttable.js')); ?>
<?php TemplateUtility::printHeaderBlock(); ?>
<?php TemplateUtility::printTabs($this->active, $this->subActive); 

?>
<style>
    td.tdVertical{
        width: 150px;
    }
</style>
    <div id="main">
        <?php TemplateUtility::printQuickSearch(); ?>

        <div id="contents">
            <table>
                <tr>
                    <td width="3%">
                        <img src="images/settings.gif" width="24" height="24" border="0" alt="Settings" style="margin-top: 3px;" />&nbsp;
                    </td>
                    <td><h2>Settings: Edit Site User</h2></td>
                </tr>
            </table>

            <p class="note">
                <span style="float: left;">Edit Site User</span>
                <span style="float: right;"><a style="color:white !important;text-decoration: underline;" href='<?php echo(RESUFLOUtility::getIndexName()); ?>?m=settings&amp;a=manageUsers'>Back to User Management</a></span>&nbsp;
            </p>
            <?php if($this->data['accessLevel'] == ACCESS_LEVEL_DELETED){ ?>
                <h3>Please wait, removing account...</h3>
                <div class="remove">
                    <div><strong>Removing Activities <img src="images/loading.gif" /> </strong><span id="ActivityCount"><?php echo $this->ActivityCount; ?></span><br></div>
                    <div><strong>Removing Attachments <img src="images/loading.gif" /> </strong><span id="AttachmentCount"><?php echo $this->AttachmentCount; ?></span><br></div>
                    <div><strong>Removing Calendar Events <img src="images/loading.gif" /> </strong><span id="CalendarEventCount"><?php echo $this->CalendarEventCount; ?></span><br></div>
                    <div><strong>Removing Job Orders <img src="images/loading.gif" /> </strong><span id="CandidateJoborderCount"><?php echo $this->CandidateJoborderCount; ?></span><br></div>
                    <div><strong>Removing Job Order Status History <img src="images/loading.gif" /> </strong><span id="CandidateJobOrderStatusHistoryCount"><?php echo $this->CandidateJobOrderStatusHistoryCount; ?></span><br></div>
                    <div><strong>Removing Candidates Removed <img src="images/loading.gif" /> </strong><span id="CandidateRemovedCount"><?php echo $this->CandidateRemovedCount; ?></span><br></div>
                    <div><strong>Removing Tsmith Prerestore <img src="images/loading.gif" /> </strong><span id="CandidateTsmithPrerestoreCount"><?php echo $this->CandidateTsmithPrerestoreCount; ?></span><br></div>
                    <div><strong>Removing New Candidates <img src="images/loading.gif" /> </strong><span id="CandidateNewCount"><?php echo $this->CandidateNewCount; ?></span><br></div>
                    <div><strong>Removing Career Portal Questionnaire History <img src="images/loading.gif" /> </strong><span id="CareerPortalQuestionnaireHistoryCount"><?php echo $this->CareerPortalQuestionnaireHistoryCount; ?></span><br></div>
                    <div><strong>Removing Career Builder Resumes <img src="images/loading.gif" /> </strong><span id="CBResumeCount"><?php echo $this->CBResumeCount; ?></span><br></div>
                    <div><strong>Removing Company Departments <img src="images/loading.gif" /> </strong><span id="CompanyDepartmentCount"><?php echo $this->CompanyDepartmentCount; ?></span><br></div>
                    <div><strong>Removing Contacts <img src="images/loading.gif" /> </strong><span id="ContactCount"><?php echo $this->ContactCount; ?></span><br></div>
                    <div><strong>Removing Drip Queue <img src="images/loading.gif" /> </strong><span id="DripQueueCount"><?php echo $this->DripQueueCount; ?></span><br></div>
                    <div><strong>Removing Drip Market Compaign Emails <img src="images/loading.gif" /> </strong><span id="DripMarketCompaignEmailCount"><?php echo $this->DripMarketCompaignEmailCount; ?></span><br></div>
                    <div><strong>Removing Drip Market Configurations <img src="images/loading.gif" /> </strong><span id="DripMarketConfigurationCount"><?php echo $this->DripMarketConfigurationCount; ?></span><br></div>
                    <div><strong>Removing Drip Market Question Answers <img src="images/loading.gif" /> </strong><span id="DripMarketQuesansCount"><?php echo $this->DripMarketQuesansCount; ?></span><br></div>
                    <div><strong>Removing Drip Market Question Answer Dupe Cleanup <img src="images/loading.gif" /> </strong><span id="DripMarketQuesansDupeCleanupCount"><?php echo $this->DripMarketQuesansDupeCleanupCount; ?></span><br></div>
                    <div><strong>Removing Drip Market Questable <img src="images/loading.gif" /> </strong><span id="DripMarketQuestableCount"><?php echo $this->DripMarketQuestableCount; ?></span><br></div>
                    <div><strong>Removing Email History <img src="images/loading.gif" /> </strong><span id="EmailHistoryCount"><?php echo $this->EmailHistoryCount; ?></span><br></div>
                    <div><strong>Removing Email Templates <img src="images/loading.gif" /> </strong><span id="EmailTemplateCount"><?php echo $this->EmailTemplateCount; ?></span><br></div>
                    <div><strong>Removing Extra Fields <img src="images/loading.gif" /> </strong><span id="ExtraFieldCount"><?php echo $this->ExtraFieldCount; ?></span><br></div>
                    <div><strong>Removing Filters <img src="images/loading.gif" /> </strong><span id="FilterCount"><?php echo $this->FilterCount; ?></span><br></div>
                    <div><strong>Removing History <img src="images/loading.gif" /> </strong><span id="HistoryCount"><?php echo $this->HistoryCount; ?></span><br></div>
                    <div><strong>Removing Import Opt Out <img src="images/loading.gif" /> </strong><span id="ImportOptOutCount"><?php echo $this->ImportOptOutCount; ?></span><br></div>
                    <div><strong>Removing Jobs <img src="images/loading.gif" /> </strong><span id="JobCount"><?php echo $this->JobCount; ?></span><br></div>
                    <div><strong>Removing Job Orders <img src="images/loading.gif" /> </strong><span id="JobOrderCount"><?php echo $this->JobOrderCount; ?></span><br></div>
                    <div><strong>Removing MRU <img src="images/loading.gif" /> </strong><span id="MRUCount"><?php echo $this->MRUCount; ?></span><br></div>
                    <div><strong>Removing Process Zips <img src="images/loading.gif" /> </strong><span id="ProcessZipCount"><?php echo $this->ProcessZipCount; ?></span><br></div>
                    <div><strong>Removing Process Zip Logs <img src="images/loading.gif" /> </strong><span id="ProcessZipLogCount"><?php echo $this->ProcessZipLogCount; ?></span><br></div>
                    <div><strong>Removing Reminder Queue <img src="images/loading.gif" /> </strong><span id="ReminderQueueCount"><?php echo $this->ReminderQueueCount; ?></span><br></div>
                    <div><strong>Removing Saved List <img src="images/loading.gif" /> </strong><span id="SavedListCount"><?php echo $this->SavedListCount; ?></span><br></div>
                    <div><strong>Removing Saved List Entry <img src="images/loading.gif" /> </strong><span id="SavedListEntryCount"><?php echo $this->SavedListEntryCount; ?></span><br></div>
                    <div><strong>Removing Saved Search <img src="images/loading.gif" /> </strong><span id="SavedSearchCount"><?php echo $this->SavedSearchCount; ?></span><br></div>
                    <div><strong>Removing Tickets <img src="images/loading.gif" /> </strong><span id="TicketCount"><?php echo $this->TicketCount; ?></span><br></div>
                    <div><strong>Removing Ticket Responses <img src="images/loading.gif" /> </strong><span id="TicketResponseCount"><?php echo $this->TicketResponseCount; ?></span><br></div>
                    <div><strong>Removing Twilio Messages <img src="images/loading.gif" /> </strong><span id="TwilioMessageCount"><?php echo $this->TwilioMessageCount; ?></span><br></div>
                    <div><strong>Removing User Logins <img src="images/loading.gif" /> </strong><span id="UserLoginCount"><?php echo $this->UserLoginCount; ?></span><br></div>
                    <div><strong>Removing Temporary Backup Saved List Entry <img src="images/loading.gif" /> </strong><span id="TempBackupSavedListEntryCount"><?php echo $this->zzzTempBackupSavedListEntryCount; ?></span><br></div>
                    <div><strong>Removing Drip Market Compaigns <img src="images/loading.gif" /> </strong><span id="DripMarketCompaignCount"><?php echo $this->DripMarketCompaignCount; ?></span><br></div>
                    <div><strong>Removing Companies <img src="images/loading.gif" /> </strong><span id="CompanyCount"><?php echo $this->CompanyCount; ?></span><br></div>
                    <div><strong>Removing Candidates <img src="images/loading.gif" /> </strong><span id="CandidateCount"><?php echo $this->CandidateCount; ?></span><br></div>
                    <div><strong>Removing User <img src="images/loading.gif" /> </strong><span id="UserCount"><?php echo $this->UserCount; ?></span><br></div>
                </div>
                <script>
                    var Counts = {
                        ActivityCount : 0,
                        AttachmentCount : 0,
                        CalendarEventCount : 0,
                        CandidateJoborderCount : 0,
                        CandidateJobOrderStatusHistoryCount : 0,
                        CandidateRemovedCount : 0,
                        CandidateTsmithPrerestoreCount : 0,
                        CandidateNewCount : 0,
                        CareerPortalQuestionnaireHistoryCount : 0,
                        CBResumeCount : 0,
                        CompanyDepartmentCount : 0,
                        ContactCount : 0,
                        DripQueueCount : 0,
                        DripMarketCompaignEmailCount : 0,
                        DripMarketConfigurationCount : 0,
                        DripMarketQuesansCount : 0,
                        DripMarketQuesansDupeCleanupCount : 0,
                        DripMarketQuestableCount : 0,
                        EmailHistoryCount : 0,
                        EmailTemplateCount : 0,
                        ExtraFieldCount : 0,
                        FilterCount : 0,
                        HistoryCount : 0,
                        ImportOptOutCount : 0,
                        JobCount : 0,
                        JobOrderCount : 0,
                        MRUCount : 0,
                        ProcessZipCount : 0,
                        ProcessZipLogCount : 0,
                        ReminderQueueCount : 0,
                        SavedListCount : 0,
                        SavedListEntryCount : 0,
                        SavedSearchCount : 0,
                        TicketCount : 0,
                        TicketResponseCount : 0,
                        TwilioMessageCount : 0,
                        UserLoginCount : 0,
                        TempBackupSavedListEntryCount : 0,
                        DripMarketCompaignCount : 0,
                        CompanyCount : 0,
                        CandidateCount : 0,
                        UserCount : 0
                    }
                    $(".remove").find("span").each(function(){
                        if(parseInt($(this).html()) == 0){
                            $(this).parent().css("color", "black");
                            $(this).parent().find("img").remove();
                        }
                        else{
                            $(this).parent().css("color", "red");
                        }
                    });
                    function FetchCounts(Source){
                        if(Counts[Source] == 0)
                        {
                            Counts[Source] = 1;
                            $.ajax({
                                type: "POST",
                                url: "index.php?m=settings&a=Count",
                                data: {UserId: "<?php $this->_($this->data['userID']); ?>", Source: Source},
                                success: function(result){
                                    var myObj = JSON.parse(result);
                                    console.log(Counts);
                                    Counts[myObj.Source] = 0;
                                    console.log(Counts);
                                    $("#"+myObj.Source).html(myObj.Total);
                                    console.log(myObj.Total);
                                    if(myObj.Total == 0){
                                        $("#"+myObj.Source).parent().css("color", "black");
                                        $("#"+myObj.Source).parent().find("img").remove();
                                    }
                                }
                            });
                        }
                    }
                    setInterval(function(){
                        for (var name in Counts) {
                            FetchCounts(name);
                        }
                        var AllRemoved = true;
                        $(".remove").find("span").each(function(){
                            if(parseInt($(this).html()) > 0){
                                AllRemoved = false;
                            }
                        });
                        if(AllRemoved == true){
                            window.location.href = "index.php?m=settings&a=manageUsers";
                        }
                    }, 5000);
                </script>
                
            <?php } else{ ?>
            <form name="editUserForm" id="editUserForm" action="<?php echo(RESUFLOUtility::getIndexName()); ?>?m=settings&amp;a=editUser" method="post" onsubmit="return checkEditUserForm(document.editUserForm);" enctype="multipart/form-data" autocomplete="off">
                <input type="hidden" name="postback" id="postback" value="postback" />
                <input type="hidden" id="userID" name="userID" value="<?php $this->_($this->data['userID']); ?>" />

                <table class="editTable" width="600">
                    <tr>
                        <td class="tdVertical">
                            <label id="firstNameLabel" for="firstName"><?php if($_SESSION['RESUFLO']->getAccessLevel()==400 && $_SESSION['RESUFLO']->getClientLogin()==1 ){ echo "Recruiter";}else{ echo "Company";}?> Name:</label>
                        </td>
                        <td class="tdData">
                            <input type="text" class="inputbox"  maxlength="15" id="firstName" name="firstName" value="<?php $this->_($this->data['firstName']); ?>" style="width: 150px;" />&nbsp;*
                        </td>
                    </tr>
<!--
                    <tr>
                        <td class="tdVertical">
                            <label id="lastNameLabel" for="lastName">Last Name:</label>
                        </td>
                        <td class="tdData">
                            <input type="text" class="inputbox"  maxlength="15" id="lastName" name="lastName" value="<?php $this->_($this->data['lastName']); ?>" style="width: 150px;" />&nbsp;*
                        </td>
                    </tr>-->

                    <tr>
                        <td class="tdVertical">
                            <label id="emailLabel" for="email">E-Mail:</label>
                        </td>
                        <td class="tdData">
                            <input type="text" class="inputbox" id="email" name="email" value="<?php $this->_($this->data['email']); ?>" style="width: 150px;" />
                        </td>
                    </tr>

                    <tr>
                        <td class="tdVertical">
                            <label id="usernameLabel" for="username">Username</label>
                        </td>
                        <td class="tdData">
                            <input type="text" class="inputbox"  maxlength="15" id="username" name="username" value="<?php $this->_($this->data['username']); ?>" style="width: 150px;" />&nbsp;*
                        </td>
                    </tr>
                    <?php if($_SESSION['RESUFLO']->getAccessLevel()==400 && $_SESSION['RESUFLO']->getClientLogin()==1 ){
                    ?>
                    <input type="hidden" name="entered_by" value="1" />
                    <?php }else{?>
                        <tr>
                            <td class="tdVertical">Master Account:</td>
                            <td class="tdData">
                                <select name="entered_by" style="width: 154px;">
                                    <?php foreach ($this->allActiveUsers as $user): ?>
                                        <option value="<?php echo $user['userid']; ?>" <?php echo $this->data['enteredby'].':'.$user['userid']; if($this->data['enteredby']==$user['userid']){ echo " selected";}?>><?php echo $user['username']; ?></option>
                                    <?php endforeach; ?>
                                </select>
                            </td>
                        </tr>
                    <?php } ?>
					<?php if($_SESSION['RESUFLO']->getAccessLevel()==400 && $_SESSION['RESUFLO']->getClientLogin()==1 ){ }else{?>
					<tr>
                                    <td class="tdVertical">
                                        <label id="retypePasswordLabel" for="retypePassword">Current Company Logo:</label>
                                    </td>
                                    <td class="tdData">
									<input type="hidden" name="oldimagename" value="<?php echo $this->data['logo'];?>" />
									<?php
									if($this->data['logo']==''){
									echo "No logo set";
									}else{
									echo "<img src='".$this->data['logo']."' width='100' height='100' />";
									}									
									?>
                                    </td>
                                </tr>
					<tr>
                                    <td class="tdVertical">
                                        <label id="retypePasswordLabel" for="retypePassword">Company Logo:</label>
                                    </td>
                                    <td class="tdData">
                                        <input type="file" class="inputbox" id="logo" name="logo" style="width: 150px;" />&nbsp;
                                    </td>
                                </tr>
                                <tr>
                                    <td class="tdVertical">
                                        <label id="searchcriteriaLabel" for="searchcriteria">Search Criteria</label>
                                    </td>
                                    <td class="tdData">
                                        <textarea type="text" class="inputbox"  id="searchcriteria" name="searchcriteria" style="width: 98%; resize: none; height: 100px;"><?php $this->_($this->data['searchCriteria']); ?></textarea>
                                    </td>
                                </tr>
                                <tr>
                                    <td class="tdVertical">
                                        <label id="cbActiveLabel" for="cbActive">CareerBuilder Enable?</label>
                                    </td>
                                    <td class="tdData">
                                        <select name="cbActive" id="cbActive">
                                            <option value="0" <?php if($this->data['cbActive']==0){ echo " selected";}?>>No</option>
                                            <option value="1" <?php if($this->data['cbActive']==1){ echo " selected";}?>>Yes</option>
                                        </select>
                                    </td>
                                </tr>
					<?php }?>
					<!--<tr>
                                    <td colspan="2" class="tdVertical">
                                       <strong>(NOTE: Please upload a logo of dimension 277*52)</strong>
                                    </td>
                                </tr>-->
                    <tr>
                        <td class="tdVertical">
                            <label id="notesLabel" for="notes">Access Level:</label>
                        </td>
                        <td class="tdData">
                            <?php foreach ($this->accessLevels as $accessLevel): ?>
							<?php if($_SESSION['RESUFLO']->getAccessLevel()==400 && $_SESSION['RESUFLO']->getClientLogin()==1 && ($accessLevel['accessID']==400 || $accessLevel['accessID']==500)){ continue;}?>
                                <?php if ($accessLevel['accessID'] > $this->accessLevel): continue; endif; ?>

                                <?php $radioButtonID = 'access' . $accessLevel['accessID']; ?>

                                <input type="radio" name="accessLevel" id="<?php echo($radioButtonID); ?>" value="<?php $this->_($accessLevel['accessID']); ?>" title="<?php $this->_($accessLevel['longDescription']); ?>"<?php if ($this->data['accessLevel'] == $accessLevel['accessID']): ?> checked<?php endif; ?><?php if (($this->disableAccessChange && $accessLevel['accessID'] > ACCESS_LEVEL_READ) || ($this->currentUser == $this->data['userID'])): ?> disabled<?php endif; ?> onclick="document.getElementById('userAccessStatus').innerHTML='<?php $this->_($accessLevel['longDescription']); ?>'" />
                                <label for="<?php echo($radioButtonID); ?>" title="<?php $this->_($accessLevel['longDescription']); ?>">
                                    <?php $this->_($accessLevel['shortDescription']); ?>
                                    <?php if ($accessLevel['accessID'] == $this->defaultAccessLevel): ?>(Default)<?php endif; ?>
                                </label>
                                <br />
                            <?php endforeach; ?>
                        </td>
                    </tr>

                    <tr>
                        <td class="tdVertical">Access Description:</td>
                        <td class="tdData">
                            <span id="userAccessStatus" style='font-size: smaller'>
                                <?php if ($this->cannotEnableMessage): ?>
                                    You cannot make this account active  without disabling another account or upgrading your license, as doing so would cause you to exceed your allowable number of active accounts.
                                <?php elseif ($this->currentUser == $this->data['userID']): ?>
                                    You are a <?php $this->_($this->data['accessLevelLongDescription']); ?> You cannot edit your own access level.
                                <?php else: ?>
                                    <?php $this->_($this->data['accessLevelLongDescription']); ?>
                                <?php endif; ?>
                            </span>
                        </td>
                    </tr>
					<?php if($_SESSION['RESUFLO']->getAccessLevel()==400 && $_SESSION['RESUFLO']->getClientLogin()==1 ){
					?>
					<input type="hidden" name="clientlogin" value="1" />
                                        <?php }else{?>
                                        <tr>
                                            <td class="tdVertical">Client Login:</td>
                                            <td class="tdData">
                                                <select name="clientlogin">
                                                <option value="0" <?php if($this->data['clientlogin']=='0'){ echo " selected";}?>>No</option>
                                                <option value="1" <?php if($this->data['clientlogin']=='1'){ echo " selected";}?>>Yes</option>
                                                </select>
                                            </td>
                                        </tr>
					<?php } ?>

                    <tr>
                        <td class="tdVertical">
                            <label for="is_nyl">Is NYL?:</label>
                        </td>
                        <td class="tdData">
                            <select name="is_nyl">
                                <option value="0" <?php if($this->data['is_nyl']!='1'){ echo " selected";}?>>No</option>
                                <option value="1" <?php if($this->data['is_nyl']=='1'){ echo " selected";}?>>Yes</option>
                            </select>
                        </td>
                    </tr>
                    <tr>
                        <td class="tdVertical">
                            <label for="is_nyl">Is Source Visible?:</label>
                        </td>
                        <td class="tdData">
                            <select name="isSourceVisible">
                                <option value="0" <?php if($this->data['isSourceVisible']!='1'){ echo " selected";}?>>No</option>
                                <option value="1" <?php if($this->data['isSourceVisible']=='1'){ echo " selected";}?>>Yes</option>
                            </select>
                        </td>
                    </tr>

                    <?php if (count($this->categories) > 0): ?>
                        <tr>
                            <td class="tdVertical">
                                <label id="accessLevelLabel" for="accessLevel">Role:</label>
                            </td>
                            <td class="tdData">
                               <input type="radio" name="role" value="none" title="" <?php if ($this->data['categories'] == ''): ?>checked<?php endif; ?> onclick="document.getElementById('userRoleDesc').innerHTML='This user is a normal user.';"/> Normal User
                               <?php $roleDesc = "This user is a normal user."; ?>
                               <br />
                               <?php foreach ($this->categories as $category): ?>
                                   <input type="radio" name="role" value="<?php $this->_($category[1]); ?>"  <?php if ($this->data['categories'] == $category[1]): ?>checked<?php $roleDesc = $category[2]; ?><?php endif; ?> onclick="document.getElementById('userRoleDesc').innerHTML='<?php echo($category[2]); ?>';" /> <?php $this->_($category[0]); ?>
                                   <br />
                               <?php endforeach; ?>
                            </td>
                        </tr>
                        <tr>
                            <td class="tdVertical">Role Description:</td>
                            <td class="tdData">
                                <span id="userRoleDesc" style='font-size: smaller'><?php $this->_($roleDesc); ?></span>
                            </td>
                        </tr>
                    <?php else: ?>
                        <span style="display:none;">
                            <input type="radio" name="role" value="none" title="" checked /> Normal User
                        </span>
                    <?php endif; ?>
                    
                    <?php if($this->EEOSettingsRS['enabled'] == 1): ?>                    
                         <tr>
                            <td class="tdVertical">Allowed to view EEO Information:</td>
                            <td class="tdData">
                                <span id="eeoIsVisibleCheckSpan">
                                    <input type="checkbox" name="eeoIsVisible" id="eeoIsVisible" <?php if ($this->data['canSeeEEOInfo'] == 1): ?>checked <?php endif; ?>onclick="if (this.checked) document.getElementById('eeoVisibleSpan').style.display='none'; else document.getElementById('eeoVisibleSpan').style.display='';">
                                    &nbsp;This user is <span id="eeoVisibleSpan">not </span>allowed to edit and view candidate's EEO information.
                                </span>
                            </td>
                        </tr>
                    <?php endif; ?>
                    
                    <tr id="isTextMessageEnable">
                        <td class="tdVertical">
                            <label id="isTextMessageEnableLabel" for="isTextMessageEnable">Is Text Message ?:</label>
                        </td>
                        <td class="tdData">
                            <select name="isTextMessageEnable">
                                <option value="0" <?php if($this->data['isTextMessageEnable']!='1'){ echo " selected";}?>>No</option>
                                <option value="1" <?php if($this->data['isTextMessageEnable']=='1'){ echo " selected";}?>>Yes</option>
                            </select>
                        </td>
                    </tr>
                    <tr>
                        <td>JazzHr Connect</td>
                        <td style="padding: 3px;">
                            <div style="padding: 5px; border: 1px solid lightgray;">
                                <table>
                                    <tr>
                                        <td>JazzHR API Key:</td>
                                        <td><input type="text" id="JazzHRAPIKey" name="JazzHRAPIKey" value="<?php $this->_($this->data['JazzHRAPIKey']); ?>"/></td>
                                    </tr>
                                    <tr>
                                        <td>JazzHR User:</td>
                                        <td>
                                            <select id="JazzHRHiringLeadId" name="JazzHRHiringLeadId" style="width: 177px;">
                                                <?php foreach ($this->JazzHRUsers as $jazzHrUser){ ?>
                                                    <option value="<?php echo $jazzHrUser['id']; ?>" <?php if($this->data['JazzHRHiringLeadId']==$jazzHrUser['id']){ echo " selected";}?>><?php echo $jazzHrUser['email']; ?></option>
                                                <?php }?>
                                            </select>
                                        </td>
                                        <td style="width: 20px;"><a href="#" onclick="LoadJazzHRUsers(); return false;" style="border: 1px solid gray;background-color: lightgray;padding: 6px;">Load</a></td>
                                    </tr>
                                    <tr>
                                        <td>Workflow Id:</td>
                                        <td><input type="text" id="JazzHRWorkflowId" name="JazzHRWorkflowId" value="<?php $this->_($this->data['JazzHRWorkflowId']); ?>"/></td>
                                    </tr>
                                    <tr>
                                        <td>Job Posting Limit:</td>
                                        <td><input type="text" id="JazzHRJobPostLimit" name="JazzHRJobPostLimit" value="<?php $this->_($this->data['JazzHRJobPostLimit']); ?>"/></td>
                                    </tr>
                                </table>
                            </div>
                        </td>
                    </tr>
                    <tr id="passwordResetElement1">
                        <td class="tdVertical">
                            <label id="PasswordResetLabel" for="username">Password Reset:</label>
                        </td>
                        <td class="tdData">
                            <input type="button" class="button" name="passwordreset" id="passwordreset" value="Reset Password" onclick="javascript:document.getElementById('passwordResetElement1').style.display = 'none'; document.getElementById('passwordResetElement2').style.display = ''; document.getElementById('passwordResetElement3').style.display = ''; document.getElementById('password1').value=''; document.getElementById('passwordIsReset').value='1';" />
                            <input type="hidden" id="passwordIsReset" name="passwordIsReset" value="0" />
                        </td>
                    </tr>

                    <tr id="passwordResetElement2" style="display:none;">
                        <td class="tdVertical">
                            <label id="password1Label" for="password1">New Password:</label>
                        </td>
                        <td class="tdData">
                                <input type="password"  maxlength="15" class="inputbox" id="password1" name="password1" style="width: 150px;" />&nbsp;*
                        </td>
                    </tr>

                    <tr id="passwordResetElement3" style="display:none;">
                        <td class="tdVertical">
                            <label id="password2Label" for="password2">Retype Password:</label>
                        </td>
                        <td class="tdData">
                                <input type="password"  maxlength="15" class="inputbox" id="password2" name="password2" style="width: 150px;" />&nbsp;*
                        </td>
                    </tr>

                </table>
                <input type="submit" class="button" name="submit" id="submit" value="Save" />&nbsp;
                <input type="reset"  class="button" name="reset"  id="reset"  value="Reset" onclick="document.getElementById('userAccessStatus').innerHTML='<?php $this->_($this->data['accessLevelLongDescription']); ?>'" />&nbsp;
                <input type="button" class="button" name="back"   id="back"   value="Cancel" onclick="javascript:goToURL('<?php echo(RESUFLOUtility::getIndexName()); ?>?m=settings&amp;a=showUser&amp;userID=<?php $this->_($this->data['userID']); ?>');" />
            </form>
            <?php }?>
        </div>
    </div>
    <div id="bottomShadow"></div>
    <script type="text/javascript">
        function LoadJazzHRUsers() {
            $.post("<?php echo(RESUFLOUtility::getIndexName()); ?>?m=settings&a=ajaxJazzHRUsers&JazzHRAPIKey=" + $("#JazzHRAPIKey").val(), function (result) {
                $("#JazzHRHiringLeadId option").remove();
                var users = JSON.parse(result);
                if (users.length == undefined) {
                    $('#JazzHRHiringLeadId').append('<option value=' + users.id + '>' + users.email + '</option>');
                }
                else {
                    for (var i = 0; i < users.length; i++) {
                        $('#JazzHRHiringLeadId').append('<option value=' + users[i].id + '>' + users[i].email + '</option>');
                    }
                }
            });
        }
    </script>
<?php TemplateUtility::printFooter(); ?>
