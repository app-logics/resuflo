<?php /* $Id: ChangePassword.tpl 1927 2007-02-22 06:03:24Z will $ */ ?>
<?php TemplateUtility::printHeader('Settings', array('modules/settings/validator.js', 'js/sorttable.js')); ?>
<?php TemplateUtility::printHeaderBlock(); ?>
<?php TemplateUtility::printTabs($this->active, $this->subActive); ?>

    <div id="main">
        <?php TemplateUtility::printQuickSearch(); ?>

        <div id="contents">
            <table>
                <tr>
                    <td width="3%">
                        <img src="images/settings.gif" width="24" height="24" border="0" alt="Settings" style="margin-top: 3px;" />&nbsp;
                    </td>
                    <td><h2>Settings: My Profile</h2></td>
                </tr>
            </table>
            
            <p class="note">Linkedin</p>
            <form name="calenderCred" id="calenderCred" action="<?php echo(RESUFLOUtility::getIndexName()); ?>?m=settings&amp;a=calender" method="post" autocomplete="off">
                <font style="font-weight:bold;" ><?php echo $this->msg; ?></font>
                <table class="searchTable">
                    <tr>
                        <td>
                            <label id="currentPasswordLabel" for="currentPassword">Linkedin Email:</label>
                        </td>
                        <td>
                            <input type="text" class="inputbox" id="linkedinEmail" name="linkedinEmail" value="linkedin@gmail.com" style="width: 250px;"/>                        
                        </td>
                    </tr>
                    <tr>
                        <td colspan="2">
                            <br />
                            <!--<input type="submit" class="button" id="submit" onclick="if(document.getElementById('username').value==''){alert('Please enter the username');return false;}" name="submit" value="Submit" />-->
                            <input type="submit" class="button" id="submit" name="submit" value="Submit" />
                            <input type="button" name="back" class = "button" value="Back" onclick="document.location.href='<?php echo(RESUFLOUtility::getIndexName()); ?>?m=settings';" />                       
                        </td>
                    </tr>
                </table>
            </form>
            <br><br> 

            <p class="note">Outlook Calender</p>
            <form name="calenderCred" id="calenderCred" action="<?php echo(RESUFLOUtility::getIndexName()); ?>?m=settings&amp;a=calender" method="post" autocomplete="off">
                <input type="hidden" name="Outlook" id="Outlook" value="True" />
                <font style="font-weight:bold;" ><?php echo $this->msg; ?></font>
                <table class="searchTable">
                    <tr>
                        <td>
                            <label id="currentPasswordLabel" for="currentPassword">Outlook Email:</label>
                        </td>
                        <td>
                            <input type="text" class="inputbox" id="username" name="username" value="<?php echo $this->oldName; ?>" style="width: 250px;"/>                        
                        </td>
                    </tr>
                    <tr style="display: none;">
                        <td>
                            <label id="newPasswordLabel" for="newPassword">Password:</label>
                        </td>
                        <td>
                            <input type="password" class="inputbox" id="password" name="password" value="<?php echo $this->oldPass; ?>" />&nbsp;*                        
                        </td>
                    </tr>
                    <tr>
                        <td colspan="2">
                            <br />
                            <!--<input type="submit" class="button" id="submit" onclick="if(document.getElementById('username').value==''){alert('Please enter the username');return false;}" name="submit" value="Submit" />-->
                            <input type="submit" class="button" id="submit" name="submit" value="Submit" />
                            <input type="button" name="back" class = "button" value="Back" onclick="document.location.href='<?php echo(RESUFLOUtility::getIndexName()); ?>?m=settings';" />                       
                        </td>
                    </tr>
                </table>
            </form>
            <br><br> 
            
            <p class="note">Acuity Scheduling Calender</p>
            <form name="calenderCred" id="calenderCred" action="<?php echo(RESUFLOUtility::getIndexName()); ?>?m=settings&amp;a=calender" method="post" autocomplete="off">
                <input type="hidden" name="Acuity" id="Acuity" value="True" />
                <font style="font-weight:bold;" ><?php echo $this->msg; ?></font>
                <table class="searchTable">
                    <tr>
                        <td>
                            <label id="AcuityUserIdLabel" for="currentPassword">Acuity User ID:</label>
                        </td>
                        <td>
                            <input type="text" class="inputbox" id="AcuityUserId" name="AcuityUserId" value="<?php echo $this->acuityUserId; ?>"/>&nbsp;*                          
                        </td>
                    </tr>
                    <tr>
                        <td>
                            <label id="AcuityAPIKeyLabel" for="AcuityAPIKey">API Key:</label>
                        </td>
                        <td>
                            <input type="text" class="inputbox" id="AcuityAPIKey" name="AcuityAPIKey" value="<?php echo $this->acuityAPIKey; ?>" style="width: 250px;"/>&nbsp;*
                        </td>
                    </tr>
                    <tr>
                        <td>
                            <label id="AcuityAPIKeyLabel" for="AcuityAPIKey">Appointment Types:</label>
                        </td>
                        <td>
                            <select id="AcuityAppointmentTypeId" name="AcuityAppointmentTypeId">
                                
                            </select>&nbsp;*
                        </td>
                        <td><input type="button" name="back" class = "button" value="Load Appointments" onclick="LoadAcuityAppoitmentTypes(<?php echo $this->acuityAppointmentTypeId; ?>);"/></td>
                    </tr>
                    <tr>
                        <td colspan="2">
                            <br />
                            <!--<input type="submit" class="button" id="submit" onclick="if(document.getElementById('username').value==''){alert('Please enter the username');return false;}" name="submit" value="Submit" />-->
                            <input type="submit" class="button hide AcuitySubmit" id="submit" name="submit" value="Submit" />
                            <input type="button" name="back" class = "button" value="Back" onclick="document.location.href='<?php echo(RESUFLOUtility::getIndexName()); ?>?m=settings';" />                       
                        </td>
                    </tr>
                </table>
            </form>
            <br><br> 
            
            <p class="note">Google Calender</p>
            <font style="font-weight:bold;" ><?php echo $this->msg; ?></font>
            <table class="searchTable">
                <tr style="display: none;">
                    <td>
                        <label id="currentPasswordLabel" for="currentPassword">Google Email:</label>
                    </td>
                    <td>
                        <input type="text" class="inputbox" id="username" name="username" value="<?php echo $this->googleName; ?>" style="width: 250px;" disabled/>                       
                    </td>
                </tr>
                <tr>
                    <td>
                        <label id="currentPasswordLabel" for="currentPassword">Connected:</label>
                    </td>
                    <td>
                        <?php 
                            if($this->google_access_token != ''){
                                echo '<span style="color: blue";>Yes</span>';
                            }else{
                                echo '<span style="color: red";>No</span>';
                            } 
                        ?>                      
                    </td>
                </tr>
                <tr>
                    <td colspan="2">
                        <br />
                        Connect to Calendar:
                        <br />
                        <a href="./GoogleCalAPI/index.php?id=<?php echo $this->userid; ?>">
                            <img src="images/mru/btn_google_signin_dark_pressed_web.png" alt=""/>
                        </a>
                    </td>
                </tr>
            </table>

            <script type="text/javascript">
                document.calenderCred.username.focus();
            </script>
        </div>
    </div>
    <div id="bottomShadow"></div>
<script type="text/javascript">
    function LoadAcuityAppoitmentTypes(acuityAppointmentTypeId){
        var AcuityUserId = $("#AcuityUserId").val();
        var AcuityAPIKey = $("#AcuityAPIKey").val();
        $.ajax({
            dataType:"json",
            type: "GET",
            url: "index.php?m=settings&a=LoadAcuityAppoitmentTypes&acuityUserId="+AcuityUserId+"&acuityApiKey="+AcuityAPIKey,
            data: "data",
            success: function(result){
                if(result == null)
                {
                    alert("Appointments couldn't fetch.");
                }
                else
                {
                    if(result.length > 0)
                    {
                        var AppointmentTypes = document.getElementById("AcuityAppointmentTypeId");
                        $.each(result , function(index, val) { 
                            $('#AcuityAppointmentTypeId').append($('<option/>', {
                                value: val["id"],
                                text: val["name"],
                                selected: acuityAppointmentTypeId == parseInt(val["id"])  ? true : false
                           }));
                        });
                        $(".AcuitySubmit").removeClass("hide");
                    }
                    else
                    {
                        alert("No appointment fetched");
                    }
                }
            }
        });
    }
    if("<?php echo $this->acuityUserId; ?>" != "")
    {
        LoadAcuityAppoitmentTypes(<?php echo $this->acuityAppointmentTypeId; ?>);
    }
</script>
<?php TemplateUtility::printFooter(); ?>
