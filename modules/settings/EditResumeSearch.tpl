<?php /* $Id: EditUser.tpl 2881 2007-08-14 07:47:26Z brian $ */ ?>
<?php TemplateUtility::printHeader('Settings', array('modules/settings/validator.js', 'js/sorttable.js')); ?>
<?php TemplateUtility::printHeaderBlock(); ?>
<?php TemplateUtility::printTabs($this->active, $this->subActive); 

?>
<style>
    td.tdVertical{
        width: 150px;
    }
</style>
<style>
        .st_mrgn {
            margin: 0px 5px 0px 0px;
            vertical-align: text-top;
        }
        .st_mrgn1 {
            margin: 0px 5px 0px 10px;
            vertical-align: text-top;
        }
        
        .mrgLeft{
            margin: 0px 2px 0px 5px;
        }

        input {
            font-size: 12px;
        }

        select {
            font-size: 12px;
        }
        table tr td {
            padding-bottom: 3px;
        }

        .wth {
            width: 189px;
        }

        .st {
            padding: 2px 2px 2px 2px;
        }

        .with {
            width: 192px;
        }
        
        .module {
            box-shadow: 0 0 0 1px hsl(0, 0%, 100%), 0 0 0 2px hsl(0, 0%, 0%);
        }

        .dv-btn {
            display: inline-block;
            border: 1px solid black;
            margin-right: 7px;
        }

        .btn {
            background-color: #D6D3CE;
            width: 80px;
            height: 25px;
            border: 1px solid white;
        }
    </style>
<div id="main">
    <?php TemplateUtility::printQuickSearch(); ?>

    <div id="contents">
        <table>
            <tr>
                <td width="3%">
                    <img src="images/settings.gif" width="24" height="24" border="0" alt="Settings" style="margin-top: 3px;" />&nbsp;
                </td>
                <td><h2>Career Builder - Search Criteria</h2></td>
            </tr>
        </table>
        <div style=";width: 630px; padding-top: 15px; background-color: #f4f4f4;">
            <form name="editResumeSearch" id="editResumeSearch" action="<?php echo(RESUFLOUtility::getIndexName()); ?>?m=settings&amp;a=resumesearch" method="post" enctype="multipart/form-data">
                <input type="hidden" name="postback" id="postback" value="postback" />
                <div style="margin: 0px 5px 5px 10px">
                    <table>
                        <tr>
                            <td style="width: 128px;">
                                Username:
                            </td>
                            <td>
                                <input type="text" id="Username" name="Username" value="<?php $this->_($this->data['cbUsername']); ?>" style="padding: 1px;" class=""  />
                            </td>
                        </tr>
                        <tr>
                            <td>
                                Password:
                            </td>
                            <td>
                                <input type="password" id="Password" name="Password" value="<?php $this->_($this->data['cbPassword']); ?>" style="padding: 1px;" class=""  />
                            </td>
                        </tr>
                        <tr>
                            <td>
                                 Key Terms:
                            </td>
                            <td>
                                Search for <input type="radio" name="SearchPattern" value="Any" checked  />Any
                            </td>
                            <td>
                                <input type="radio" name="SearchPattern" value="All" <?php if($this->data['cbSearchPattern'] == 'All'){echo 'checked';} ?> />All of these terms
                            </td>
                            <td colspan="2"></td>
                        </tr>
                        <tr>
                            <td style="vertical-align: top;">
                                Search Criteria:
                            </td>
                            <td colspan="2">
                                <textarea type="text" class="inputbox"  id="SearchCriteria" name="SearchCriteria" style="width: 98%; resize: none; height: 100px;"><?php $this->_($this->data['searchCriteria']); ?></textarea>
                            </td>
                        </tr>
                        <tr>
                            <td >
                                <input type="checkbox" <?php if($this->data['cbSearchLimit'] != 0){echo 'checked';} ?> class="st_mrgn" />Daily Limit:
                            </td>
                            <td>
                                <input type="text" id="SearchLimit" name="SearchLimit" value="<?php $this->_($this->data['cbSearchLimit']); ?>" style="padding: 1px; width: 30px;" class="" <?php if($this->data['cbSearchLimit'] == 0){echo 'disabled';} ?>  />
                            </td>
                        </tr>
                        <tr>
                            <td>
                                <input type="checkbox" <?php if($this->data['cbFromDay'] != 0 || $this->data['cbResumeFromLastDay'] != 0){echo 'checked';} ?>  class="st_mrgn" />Date Within:
                            </td>
                            <td>
                                <input type="text" id="FromDay" name="FromDay" value="<?php $this->_($this->data['cbFromDay']); ?>" style="padding: 1px; width: 30px;" class="" <?php if($this->data['cbFromDay'] == 0 && $this->data['cbResumeFromLastDay'] == 0){echo 'disabled';} ?> /> days
                            </td>
                            <!--<td style="padding-left: 33px;">
                                <input type="checkbox" id="ResumeFromLastDay" name="ResumeFromLastDay" <?php if($this->data['cbResumeFromLastDay'] != 0){echo 'checked';} ?>  class="st_mrgn lastDay" <?php if($this->data['cbFromDay'] == 0 && $this->data['cbResumeFromLastDay'] == 0){echo 'disabled';} ?>/>New since the last day this search ran
                            </td>-->
                        </tr>
                        <tr>
                            <td>
                                <input type="checkbox"  <?php if($this->data['cbState'] != '' || $this->data['cbCity'] != ''){echo 'checked';} ?>  class="st_mrgn" />State:
                            </td>
                            <td>
                                <select id="State" name="State" style="width: 50px;" class="" select="<?php echo $this->data['cbState'];?>" <?php if($this->data['cbState'] == '' || $this->data['cbCity'] == ''){echo 'disabled';} ?> >
                                    <option value="AL">AL</option>
                                    <option value="AK">AK</option>
                                    <option value="AS">AS</option>
                                    <option value="AZ">AZ</option>
                                    <option value="AR">AR</option>
                                    <option value="CA">CA</option>
                                    <option value="CO">CO</option>
                                    <option value="CT">CT</option>
                                    <option value="DE">DE</option>
                                    <option value="DC">DC</option>
                                    <option value="FL">FL</option>
                                    <option value="GA">GA</option>
                                    <option value="GU">GU</option>
                                    <option value="HI">HI</option>
                                    <option value="ID">ID</option>
                                    <option value="IL">IL</option>
                                    <option value="IN">IN</option>
                                    <option value="IA">IA</option>
                                    <option value="KS">KS</option>
                                    <option value="KY">KY</option>
                                    <option value="LA">LA</option>
                                    <option value="ME">ME</option>
                                    <option value="MM">MM</option>
                                    <option value="MD">MD</option>
                                    <option value="MA">MA</option>
                                    <option value="MI">MI</option>
                                    <option value="FM">FM</option>
                                    <option value="MN">MN</option>
                                    <option value="MS">MS</option>
                                    <option value="MO">MO</option>
                                    <option value="MT">MT</option>
                                    <option value="NE">NE</option>
                                    <option value="NV">NV</option>
                                    <option value="NH">NH</option>
                                    <option value="NJ">NJ</option>
                                    <option value="NM">NM</option>
                                    <option value="NY">NY</option>
                                    <option value="NC">NC</option>
                                    <option value="ND">ND</option>
                                    <option value="MP">MP</option>
                                    <option value="OH">OH</option>
                                    <option value="OK">OK</option>
                                    <option value="OR">OR</option>
                                    <option value="PW">PW</option>
                                    <option value="PA">PA</option>
                                    <option value="PR">PR</option>
                                    <option value="RI">RI</option>
                                    <option value="SC">SC</option>
                                    <option value="SD">SD</option>
                                    <option value="TN">TN</option>
                                    <option value="TX">TX</option>
                                    <option value="UT">UT</option>
                                    <option value="VT">VT</option>
                                    <option value="VI">VI</option>
                                    <option value="VA">VA</option>
                                    <option value="WA">WA</option>
                                    <option value="WV">WV</option>
                                    <option value="WI">WI</option>
                                    <option value="WY">WY</option>
                                    <option value="AB">AB</option>
                                    <option value="BC">BC</option>
                                    <option value="MB">MB</option>
                                    <option value="NB">NB</option>
                                    <option value="NL">NL</option>
                                    <option value="NT">NT</option>
                                    <option value="NS">NS</option>
                                    <option value="NU">NU</option>
                                    <option value="ON">ON</option>
                                    <option value="PE">PE</option>
                                    <option value="QC">QC</option>
                                    <option value="SK">SK</option>
                                    <option value="YT">YT</option>
                                </select>
                            </td>
                            <td>
                                &ensp;City: <input id="City" name="City" type="text" value="<?php $this->_($this->data['cbCity']); ?>" style="width: 230px" class=" st" <?php if($this->data['cbState'] == '' || $this->data['cbCity'] == ''){echo 'disabled';} ?> />
                            </td>
                        </tr>
                        <tr>
                            <td colspan="3">
                                <input type="checkbox" id="IsWillingToRelocate" name="IsWillingToRelocate" <?php if($this->data['cbIsWillingToRelocate'] == 1){echo 'checked';} ?>  class="st_mrgn" />Include willing to relocate to state
                            </td>
                        </tr>
                        <tr>
                            <td>
                                <input type="checkbox" <?php if($this->data['cbZipCode'] != 0 || $this->data['cbRadius'] != 0){echo 'checked';} ?> class="st_mrgn" />Zip Code:
                            </td>
                            <td>
                                <input type="text" id="ZipCode" name="ZipCode" value="<?php $this->_($this->data['cbZipCode']); ?>" class="st " style="width: 60px; padding: 1px;" <?php if($this->data['cbZipCode'] == 0 || $this->data['cbRadius'] == 0){echo 'disabled';} ?> />
                            </td>
                            <td>
                                within:<input type="text" id="Radius" name="Radius" value="<?php $this->_($this->data['cbRadius']); ?>" style="width: 40px" class=" st" <?php if($this->data['cbZipCode'] == 0 || $this->data['cbRadius'] == 0){echo 'disabled';} ?> />&ensp;mile radius
                            </td>
                        </tr>
                        <tr>
                            <td>
                                <input type="checkbox" <?php if($this->data['cbJobTitle'] != ''){echo 'checked';} ?> class="st_mrgn" /> Job Title:
                            </td>
                            <td colspan="2">
                                <input type="text" id="JobTitle" name="JobTitle" value="<?php $this->_($this->data['cbJobTitle']); ?>" class=" wth " <?php if($this->data['cbJobTitle'] == ''){echo 'disabled';}?> />
                            </td>
                        </tr>
                        <tr>
                            <td>
                                <input type="checkbox" <?php if($this->data['cbCompany'] != ''){echo 'checked';} ?> class="st_mrgn" /> Company:
                            </td>
                            <td colspan="2">
                                <input type="text" id="Company" name="Company" value="<?php $this->_($this->data['cbCompany']); ?>" class=" wth " <?php if($this->data['cbCompany'] == ''){echo 'disabled';}?> />
                            </td>
                        </tr>
                        <!--<tr>
                            <td>
                                <input type="checkbox" <?php if($this->data['cbIndustry'] != ''){echo 'checked';} ?> class="st_mrgn" /> Industry:
                            </td>
                            <td colspan="2">
                                <select id="Industry" name="Industry" class=" with st  clr" select="<?php echo $this->data['cbIndustry'];?>" <?php if($this->data['cbIndustry'] == ''){echo 'disabled';}?>>
                                    <option value="">Any</option>
                                    <option value="Accounting, Finance">Accounting, Finance</option>
                                    <option value="Adminstrative Services">Adminstrative Services</option>
                                    <option value="Advertising, Marketing, PR">Advertising, Marketing, PR</option>
                                    <option value="Agriculture, Fishing, Forestry">Agriculture, Fishing, Forestry</option>
                                    <option value="Arts, Entertainment, Media">Arts, Entertainment, Media</option>
                                    <option value="Automotive">Automotive</option>
                                    <option value="Banking">Banking</option>
                                    <option value="Biotech, Pharmaceutical">Biotech, Pharmaceutical</option>
                                    <option value="Consulting">Consulting</option>
                                    <option value="Customer Service">Customer Service</option>
                                    <option value="Education, Library, Training">Education, Library, Training</option>
                                    <option value="Engineerign">Engineerign</option>
                                    <option value="Government, Miltary">Government, Miltary</option>
                                    <option value="Health Care">Health Care</option>
                                    <option value="Hospitality, Tourism">Hospitality, Tourism</option>
                                    <option value="Human Resources, Recruiting">Human Resources, Recruiting</option>
                                    <option value="Information Technology">Information Technology</option>
                                    <option value="Install, Maintenance, Repair">Install, Maintenance, Repair</option>
                                    <option value="Insurance">Insurance</option>
                                    <option value="Law Enforcement, Security">Law Enforcement, Security</option>
                                    <option value="Legal">Legal</option>
                                    <option value="Management">Management</option>
                                    <option value="Manufacturing">Manufacturing</option>
                                    <option value="Non-profit">Non-profit</option>
                                    <option value="Personal Care and Service">Personal Care and Service</option>
                                    <option value="Purchasing">Purchasing</option>
                                    <option value="Real Estate">Real Estate</option>
                                    <option value="Restaurant, Food Service">Restaurant, Food Service</option>
                                    <option value="Retail">Retail</option>
                                    <option value="Sales">Sales </option>
                                    <option value="Science">Science</option>
                                    <option value="Telecommunications">Telecommunications</option>
                                    <option value="Logistics">Logistics</option>
                                </select>
                            </td>
                        </tr>-->
                        <tr>
                            <td>
                                <input type="checkbox" class="st_mrgn" <?php if($this->data['cbEducationLevel'] != ''){echo 'checked';} ?>/> Education Level:
                            </td>
                            <td colspan="2">
                                <select id="EducationLevel" name="EducationLevel" class=" with st  clr" select="<?php echo $this->data['cbEducationLevel'];?>" <?php if($this->data['cbEducationLevel'] == ''){echo 'disabled';}?>>
                                    <option value="">Any</option>
                                    <option value="CE31">High School</option>
                                    <option value="CE32">2 year degree</option>
                                    <option value="CE321">4 year degree</option>
                                    <option value="CE3210">Graduate degree</option>
                                </select>
                            </td>
                        </tr>
                        <tr>
                            <td>
                                <input type="checkbox" class="st_mrgn" <?php if($this->data['cbExperienceLevel'] != ''){echo 'checked';} ?>/> Experience Level:
                            </td>
                            <td colspan="2">
                                <select id="ExperienceLevel" name="ExperienceLevel" class="with st " select="<?php echo $this->data['cbExperienceLevel'];?>" <?php if($this->data['cbExperienceLevel'] == ''){echo 'disabled';}?>>
                                    <option value="">Any</option>
                                    <option value="0-0">No Experience(0 year)</option>
                                    <option value="0-2">Entry Lever(0-2 year)</option>
                                    <option value="2-5">Experienced(2-5 year)</option>
                                    <option value="5-10">Mid Career(5-10 year)</option>
                                    <option value="10-50">Senior Level(10+ year)</option>
                                </select>
                            </td>
                        </tr>
                    </table>
                    <table>
                        <tr>
                            <td>
                                <input type="checkbox" class="st_mrgn" <?php if($this->data['cbMinSalary'] != 0 || $this->data['cbMaxSalary'] != 0){echo 'checked';} ?>/> Salary <a style="text-decoration: none; color: #808080">&emsp;&emsp;&emsp; Min:</a>
                            </td>
                            <td colspan="2">
                                <select id="MinSalary" name="MinSalary" class=" with st  clr" select="<?php echo $this->data['cbMinSalary'];?>" style="margin-left: 8px" <?php if($this->data['cbMinSalary'] == 0 && $this->data['cbMaxSalary'] == 0){echo 'disabled';} ?>>
                                    <option value="0">Any</option>
                                    <option value="30000">$ 30000</option>
                                    <option value="40000">$ 40000</option>
                                    <option value="50000">$ 50000</option>
                                    <option value="60000">$ 60000</option>
                                    <option value="70000">$ 70000</option>
                                    <option value="80000">$ 80000</option>
                                    <option value="90000">$ 90000</option>
                                    <option value="100000">$100000</option>
                                    <option value="110000">$110000</option>
                                    <option value="120000">$120000</option>
                                    <option value="130000">$130000</option>
                                    <option value="140000">$140000</option>
                                    <option value="150000">$150000</option>
                                </select>
                            </td>
                            <td class="clr">
                                &emsp;&emsp;Max:&ensp;<select id="MaxSalary" name="MaxSalary" class=" with st  clr" select="<?php echo $this->data['cbMaxSalary'];?>" <?php if($this->data['cbMinSalary'] == 0 && $this->data['cbMaxSalary'] == 0){echo 'disabled';} ?>>
                                    <option value="0">Any</option>
                                    <option value="30000">$ 30000</option>
                                    <option value="40000">$ 40000</option>
                                    <option value="50000">$ 50000</option>
                                    <option value="60000">$ 60000</option>
                                    <option value="70000">$ 70000</option>
                                    <option value="80000">$ 80000</option>
                                    <option value="90000">$ 90000</option>
                                    <option value="100000">$100000</option>
                                    <option value="110000">$110000</option>
                                    <option value="120000">$120000</option>
                                    <option value="130000">$130000</option>
                                    <option value="140000">$140000</option>
                                    <option value="150000">$150000</option>
                                </select>
                            </td>
                            <td></td>
                        </tr>
                        <tr>
                            <td>
                                <input type="checkbox" class="st_mrgn" <?php if($this->data['cbEmployment'] != ''){echo 'checked';} ?> /> Employment:
                            </td>
                            <td>
                                <input type="radio" name="Employment" value="ETIN" <?php if($this->data['cbEmployment'] == 'ETIN'){echo 'checked';} if($this->data['cbEmployment'] == ''){echo 'disabled';}?>  />Permanent
                            </td>
                            <td>
                                <input type="radio" name="Employment" value="ETCT" <?php if($this->data['cbEmployment'] == 'ETCT'){echo 'checked';} if($this->data['cbEmployment'] == ''){echo 'disabled';}?> />Contract
                            </td>
                            <td colspan="2"></td>
                        </tr>
                        <tr>
                            <td>
                                <input type="checkbox" class="st_mrgn" <?php if($this->data['cbHours'] != ''){echo 'checked';} ?> /> Hours:
                            </td>
                            <td>
                                <input type="radio" name="Hours" value="ETFE" <?php if($this->data['cbHours'] == 'ETFE'){echo 'checked';} if($this->data['cbHours'] == ''){echo 'disabled';}?>/>Full-time
                            </td>
                            <td>
                                <input type="radio" name="Hours" value="ETPE" <?php if($this->data['cbHours'] == 'ETPE'){echo 'checked';} if($this->data['cbHours'] == ''){echo 'disabled';}?>/>Part-time
                            </td>
                            <td colspan="2"></td>
                        </tr>
                        <tr>
                            <td colspan="5" style="padding-bottom:7px;">
                                <input type="checkbox" id="IsAuthInUS" name="IsAuthInUS" <?php if($this->data['cbIsAuthInUS'] == 1){echo 'checked';} ?> class="st_mrgn"/> Authorized to work in United States?
                            </td>
                        </tr>
                        <tr>
                            <td colspan="5" style="padding-bottom:7px;">
                                <input type="checkbox" id="IsUSCitizen" name="IsUSCitizen" <?php if($this->data['cbIsUSCitizen'] == 1){echo 'checked';} ?> class="st_mrgn"/> Citizen of the United States?
                            </td>
                        </tr>
                        <tr>
                            <td colspan="5" style="padding-bottom:7px;">
                                <input type="checkbox" id="HasClearance" name="HasClearance" <?php if($this->data['cbHasClearance'] == 1){echo 'checked';} ?>  class="st_mrgn"/> Has a security clearance?
                            </td>
                        </tr>
                        <tr>
                            <td>
                                <input type="checkbox" <?php if($this->data['cbScheduleTime'] != ''){echo 'checked';} ?> class="st_mrgn" />Schedule Time:
                            </td>
                            <td>
                                <select id="ScheduleTime" name="ScheduleTime" select="<?php echo $this->data['cbScheduleTime'];?>" <?php if($this->data['cbScheduleTime'] == ''){echo 'disabled';}?> >
                                    <option value="01:00:00">01:00</option>
                                    <option value="01:30:00">01:30</option>
                                    <option value="02:00:00">02:00</option>
                                    <option value="02:30:00">02:30</option>
                                    <option value="03:00:00">03:00</option>
                                    <option value="03:30:00">03:30</option>
                                    <option value="04:00:00">04:00</option>
                                    <option value="04:30:00">04:30</option>
                                    <option value="05:00:00">05:00</option>
                                    <option value="05:30:00">05:30</option>
                                    <option value="06:00:00">06:00</option>
                                    <option value="06:30:00">06:30</option>
                                    <option value="07:00:00">07:00</option>
                                    <option value="07:30:00">07:30</option>
                                    <option value="08:00:00">08:00</option>
                                    <option value="08:30:00">08:30</option>
                                    <option value="09:00:00">09:00</option>
                                    <option value="09:30:00">09:30</option>
                                    <option value="10:00:00">10:00</option>
                                    <option value="10:30:00">10:30</option>
                                    <option value="11:00:00">11:00</option>
                                    <option value="11:30:00">11:30</option>
                                    <option value="12:00:00">12:00</option>
                                    <option value="12:30:00">12:30</option>
                                </select>
                                <select id="ScheduleTimeFormat" name="ScheduleTimeFormat" <?php if($this->data['cbScheduleTime'] == ''){echo 'disabled';}?>>
                                    <option value="AM">AM</option>
                                    <option value="PM">PM</option>
                                </select>
                                <!--<input type="text" id="ScheduleTime" name="ScheduleTime" value="<?php $this->_($this->data['cbScheduleTime']); ?>" style="padding: 1px; width: 30px;" class="" <?php if($this->data['cbScheduleTime'] == 0){echo 'disabled';} ?>  />-->
                            </td>
                        </tr>
                        <tr>
                            <td colspan="5" style="padding-bottom:7px;">
                                <input type="checkbox" id="IsSunday" name="IsSunday" value="1" class="st_mrgn" <?php if($this->data['cbIsSunday'] == 1){echo 'checked';} ?>/><label for="IsSunday">Sun</label>
                                <input type="checkbox" id="IsMonday" name="IsMonday" value="2" class="st_mrgn1" <?php if($this->data['cbIsMonday'] == 2){echo 'checked';} ?>/><label for="IsMonday">Mon</label>
                                <input type="checkbox" id="IsTuesday" name="IsTuesday" value="3" class="st_mrgn1" <?php if($this->data['cbIsTuesday'] == 3){echo 'checked';} ?>/><label for="IsTuesday">Tue</label>
                                <input type="checkbox" id="IsWednesday" name="IsWednesday" value="4" class="st_mrgn1" <?php if($this->data['cbIsWednesday'] == 4){echo 'checked';} ?>/><label for="IsWednesday">Wed</label>
                                <input type="checkbox" id="IsThursday" name="IsThursday" value="5" class="st_mrgn1" <?php if($this->data['cbIsThursday'] == 5){echo 'checked';} ?>/><label for="IsThursday">Thu</label>
                                <input type="checkbox" id="IsFriday" name="IsFriday" value="6" class="st_mrgn1" <?php if($this->data['cbIsFriday'] == 6){echo 'checked';} ?>/><label for="IsFriday">Fri</label>
                                <input type="checkbox" id="IsSaturday" name="IsSaturday" value="7" class="st_mrgn1" <?php if($this->data['cbIsSaturday'] == 7){echo 'checked';} ?>/><label for="IsSaturday">Sat</label>
                            </td>
                        </tr>
                        <!--
                        <tr>
                            <td>
                                <input type="checkbox" class="st_mrgn" /> Additional Action:
                            </td>
                            <td colspan="3">
                                <input type="text" class=" wth  st" value="Save, Export results" style="width: 340px" disabled /> <img src="arrow.png" style="vertical-align: top;" />
                            </td>
                        </tr>
                        <tr>
                            <td>
                                <input type="checkbox" class="st_mrgn" /> Schedule::
                            </td>
                            <td colspan="3">
                                <input type="text" class=" wth  st" value="Auto-run 291st" style="width: 340px" disabled /> <img src="arrow.png" style="vertical-align: top;" />
                            </td>
                        </tr>
                        -->
                    </table>
                    <div style="text-align: right; margin-top: 40px; padding-bottom: 20px;">
                        <div class="dv-btn">
                            <button class="btn">Save</button>
                        </div>
                    </div>
                </div>
            </form>
        </div>
    </div>
</div>
<script type="text/javascript">
    $(':checkbox').change(function() {
        if(!$(this).hasClass("lastDay")){
            if($(this).is(":checked")) {
                $(this).closest("tr").find("input[type='text'],input[type='radio'],select,.lastDay").prop("disabled", false);
            }
            else{
                $(this).closest("tr").find("input[type='text'],input[type='radio'],select,.lastDay").prop("disabled", true);
            }
        }
    });
    var time24 = '<?php echo $this->data["cbScheduleTime"]; ?>';
    var splitTime = time24.split(':');
    if(splitTime.length > 1){
        if(+splitTime[0] > 12){
            var time12 = ":"+splitTime[1]+":"+splitTime[2];
            if(+splitTime[0] < 22){
                time12 = splitTime[0]-12 + time12;
                time12 = "0" + time12;
            }
            else{
                time12 = splitTime[0]-12 + time12;
            }
            $("#ScheduleTime").attr("select", time12);
            $("#ScheduleTimeFormat").attr("select", "PM");
        }
        else{
            $("#ScheduleTime").attr("select", time24);
            $("#ScheduleTimeFormat").attr("select", "AM");
        }
    }
    $("select").each(function(){
        $(this).find("option[value='"+$(this).attr("select")+"']").prop("selected", true);
    });
</script>
    <div id="bottomShadow"></div>
<?php TemplateUtility::printFooter(); ?>
