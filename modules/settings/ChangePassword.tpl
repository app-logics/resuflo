<?php /* $Id: ChangePassword.tpl 1927 2007-02-22 06:03:24Z will $ */ ?>
<?php TemplateUtility::printHeader('Settings', array('modules/settings/validator.js', 'js/sorttable.js')); ?>
<?php TemplateUtility::printHeaderBlock(); ?>
<?php TemplateUtility::printTabs($this->active, $this->subActive); ?>

    <div id="main">
        <?php TemplateUtility::printQuickSearch(); ?>

        <div id="contents">
            <table>
                <tr>
                    <td width="3%">
                        <img src="images/settings.gif" width="24" height="24" border="0" alt="Settings" style="margin-top: 3px;" />&nbsp;
                    </td>
                    <td><h2>Settings: My Profile</h2></td>
                </tr>
            </table>

            <p class="note">Change Password</p>

            <form name="changePasswordForm" id="changePasswordForm" action="<?php echo(RESUFLOUtility::getIndexName()); ?>?m=settings&amp;a=changePassword" method="post" onsubmit="return checkChangePasswordForm(document.changePasswordForm);">
                <input type="hidden" name="postback" id="postback" value="postback" />
                <input type="hidden" name="actualpwd" id="actualpwd" value="<?php echo($_SESSION['RESUFLO']->getPassword())?>">
                <?php if ($this->isDemoUser): ?>
                    Note that as a demo user, you do not have privileges to modify any settings.
                    <br /><br />
                <?php endif; ?>

                <table class="searchTable">
                    <tr>
                        <td colspan="2">
                            <span class="bold">Change Password</span>
                            <br />
                            <br />
                            <span id='passwordErrorMessage' style="font:smaller; color: red">
                                <?php if (isset($this->errorMessage)): ?>
                                        <?php $this->_($this->errorMessage); ?>
                                <?php endif; ?>
                            </span>
                        </td>
                    </tr>


                    <tr>
                        <td>
                            <label id="currentPasswordLabel" for="currentPassword">Current Password:</label>&nbsp;
                        </td>
                        <td>
                            <input type="password" class="inputbox" id="currentPassword" name="currentPassword" />&nbsp;*
                        </td>
                    </tr>

                    <tr>
                        <td>
                            <label id="newPasswordLabel" for="newPassword">New Password:</label>&nbsp;
                        </td>
                        <td>
                            <input type="password" class="inputbox" id="newPassword" name="newPassword" />&nbsp;*
                        </td>
                    </tr>

                    <tr>
                        <td>
                            <label id="retypeNewPasswordLabel" for="retypeNewPassword">Retype New Password:</label>&nbsp;
                        </td>
                        <td>
                            <input type="password" class="inputbox" id="retypeNewPassword" name="retypeNewPassword" />&nbsp;*
                        </td>
                    </tr>
                    
                    <tr>
                        <td colspan="2">
                            <ul>
                                <li id="passwordSixChar">Be at least 6 characters</li>
                                <li id="passwordUppercase">Include an uppercase letter</li>
                                <li id="passwordLowercase">Include a lowercase letter</li>
                                <li id="passwordNumber">Include a number</li>
                                <li id="passwordMatch">New and Retype password must match</li>
                            </ul>
                            <style>
                                ul li{
                                    color: red;
                                    font-weight: bold;
                                }
                                .text-green{
                                    color: green;
                                }
                            </style>
                            <script type="text/javascript" >
                                $( "#newPassword, #retypeNewPassword" ).keyup(function() {
                                    if($(this).val().length >= 6){
                                        $("#passwordSixChar").addClass("text-green");
                                    }
                                    else{
                                        $("#passwordSixChar").removeClass("text-green");
                                    }
                                    var upperCase= new RegExp('[A-Z]');
                                    var lowerCase= new RegExp('[a-z]');
                                    var numbers = new RegExp('[0-9]');
                                    if($(this).val().match(upperCase)){
                                        $("#passwordUppercase").addClass("text-green");
                                    }
                                    else{
                                        $("#passwordUppercase").removeClass("text-green");
                                    }
                                    if($(this).val().match(lowerCase)){
                                        $("#passwordLowercase").addClass("text-green");
                                    }
                                    else{
                                        $("#passwordLowercase").removeClass("text-green");
                                    }
                                    if($(this).val().match(numbers)){
                                        $("#passwordNumber").addClass("text-green");
                                    }
                                    else{
                                        $("#passwordNumber").removeClass("text-green");
                                    }
                                    
                                    if($("#newPassword").val() == $("#retypeNewPassword").val() && ($("#newPassword").val() != "" && $("#retypeNewPassword").val() != "")){
                                        $("#passwordMatch").addClass("text-green");
                                    }
                                    else{
                                        $("#passwordMatch").removeClass("text-green");
                                    }
                                    //if($('.text-green').length != 5){alert('Please follow valid password criteria');return false;}
                                });
                            </script>
                        </td>
                    </tr>

                    <tr>
                        <td colspan="2">
                            <br />
                            <input type="submit" class="button" id="changePassword" onclick="if($('.text-green').length != 5){alert('Please follow valid password criteria');return false;} if(document.getElementById('actualpwd').value!=document.getElementById('currentPassword').value){alert('Please enter correct current passwords');return false;}" name="changePassword" value="Change Password" />
                            <input type="reset"  class="button" id="reset"          name="reset"          value="Reset" />
                            <input type="button" name="back" class = "button" value="Back" onclick="document.location.href='<?php echo(RESUFLOUtility::getIndexName()); ?>?m=settings';" />
                       </td>
                    </tr>
                </table>
            </form>

            <script type="text/javascript">
                document.changePasswordForm.currentPassword.focus();
            </script>
        </div>
    </div>
    <div id="bottomShadow"></div>
<?php TemplateUtility::printFooter(); ?>
