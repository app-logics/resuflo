<?php /* $Id: Companies.tpl 3460 2007-11-07 03:50:34Z brian $ */ ?>
<?php TemplateUtility::printHeader('Companies', array('js/highlightrows.js', 'js/export.js', 'js/dataGrid.js')); ?>
<?php TemplateUtility::printHeaderBlock(); ?>

<?php TemplateUtility::printTabs($this->active); ?>
<link rel="stylesheet" href="css/tab.css" />
<script type="text/javascript" src="javascript/tab.js"></script>

<div id="main">
    <div id="contents">
        
        <!--tabs Starts-->		 
        <ul id="countrytabs" class="shadetabs" style="padding-top:20px;">
            <li><a href="javascript:void(0)" onclick="javascript:goToURL('<?php echo(RESUFLOUtility::getIndexName()); ?>?m=dripMarket&amp;a=showcompaign');" rel="country1" class="selected">Reminders</a></li>
            <!--<li><a href="javascript:void(0)" onclick="javascript:goToURL('<?php echo(RESUFLOUtility::getIndexName()); ?>?m=dripMarket&amp;a=showconfig');" >Configuration</a></li>-->
        </ul>
        <div style="border: 0px solid #32004b; width:900px; height:auto; margin-bottom:1em; padding:0px;">
            <div id="country1" class="tabcontent">
                <table width="100%" border="0" cellspacing="0" cellpadding="0" bgcolor="#f8f8f8" >
                    <tr>
                        <td>&nbsp;</td>
                    </tr>
                    <tr>
                        <td colspan="4" bgcolor="#f8f8f8" style="padding-left:10px;"><a href="javascript:void(0)" onclick="javascript:goToURL('<?php echo(RESUFLOUtility::getIndexName()); ?>?m=settings&amp;a=createreminderinterview');" ><img src="images/button_add_email.gif" border="0"/></a></td>
                    </tr>
                    <tr>
                        <td colspan="4" bgcolor="#F8F8F8" class="font_tab">&nbsp;</td>
                    </tr>
                    <tr>
                        <td width="33%" height="35" bgcolor="#e2e2e2" class="font_tab">Name of Email </td>
                        <td width="23%" bgcolor="#e2e2e2" class="font_tab">Subjects</td>
                        <td width="22%" bgcolor="#e2e2e2" class="font_tab">No of Days </td>
                        <td width="14%" bgcolor="#e2e2e2" class="font_tab" >Action</td>
                    </tr>
                    <?php foreach($this->compaigndata as $row){ ?>
                    <tr>
                        <td class="font_tab_txt"><?php echo $row['nameofemail']; ?></td>
                        <td class="font_tab_txt"><?php echo $row['subject']; ?></td>
                        <td class="font_tab_txt"><?php  echo $row['sendemailon']; ?></td>
                        <td><table width="80%" border="0" cellspacing="0" cellpadding="0">
                                <tr>
                                    <td align="center"><a href="<?php echo(RESUFLOUtility::getIndexName()); ?>?m=settings&amp;a=createreminderinterview&amp;editid=<?php echo $row['eid']; ?>"><img src="images/edit_ico.gif"  border="0"/></a></td>
                                    <td> <a href="<?php echo(RESUFLOUtility::getIndexName()); ?>?m=settings&amp;a=reminderinterviewdelete&amp;delid=<?php echo $row['eid']; ?>"><img src="images/delete_ico.gif"  border="0"/></a></td>
                                </tr>
                            </table></td>
                    </tr>
                    <?php } ?>
                </table>
            </div>
            
        </div>        
        <script type="text/javascript">// <![CDATA[
            var countries=new ddtabcontent("countrytabs")
            countries.setpersist(true)
            countries.setselectedClassTarget("link") //"link" or "linkparent"
            countries.init()
            // ]]></script>   
        <!--tabs end-->		  
    </div>
</div>
<div id="bottomShadow"></div>
<?php TemplateUtility::printFooter(); ?>
