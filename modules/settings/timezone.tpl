<?php /* $Id: ChangePassword.tpl 1927 2007-02-22 06:03:24Z will $ */ ?>
<?php TemplateUtility::printHeader('Settings', array('modules/settings/validator.js', 'js/sorttable.js')); ?>
<?php TemplateUtility::printHeaderBlock(); ?>
<?php TemplateUtility::printTabs($this->active, $this->subActive); ?>

    <div id="main">
        <?php TemplateUtility::printQuickSearch(); ?>

        <div id="contents">
            <table>
                <tr>
                    <td width="3%">
                        <img src="images/settings.gif" width="24" height="24" border="0" alt="Settings" style="margin-top: 3px;" />&nbsp;
                    </td>
                    <td><h2>Settings: My Profile</h2></td>
                </tr>
            </table>

            <p class="note">Timezone settings</p>

            <form name="timezone" id="timezone" action="<?php echo(RESUFLOUtility::getIndexName()); ?>?m=settings&amp;a=timezone" method="post" >
				<input type="hidden" name="postback" id="postback" value="postback" />
			         <font style="font-weight:bold;" ><?php echo $this->msg; ?></font><br /><br />
                <table class="searchTable">
                    <tr>
                        <td>
                            <label id="TimezoneLabel" for="TimezoneValue">Timezone:</label>
&nbsp;                        </td>
                        <td>
                            <select name="TimezoneValue" id="TimezoneValue" class="txtbox" style="width:auto">  
                                <option value="Pacific/Wake" <?php if(isset($this->timezoneValue))                {if($this->timezoneValue=="Pacific/Wake")              { echo "selected";} } ?>>(GMT-12:00) International Date Line West</option>
                                <option value="Pacific/Apia" <?php if(isset($this->timezoneValue))                {if($this->timezoneValue=="Pacific/Apia")              { echo "selected";} } ?>>GMT -11:00) Midway Island, Samoa</option>
                                <option value="Pacific/Honolulu" <?php   if(isset($this->timezoneValue))             {if($this->timezoneValue=="Pacific/Honolulu")             { echo "selected";} } ?>>(GMT -10:00) Hawaii</option>  
                                <option value="America/Anchorage" <?php if(isset($this->timezoneValue))               {if($this->timezoneValue=="America/Anchorage")              { echo "selected";} } ?>>(GMT -09:00) Alaska</option>  
                                <option value="America/Los_Angeles" <?php if(isset($this->timezoneValue))              {if($this->timezoneValue=="America/Los_Angeles")              { echo "selected";} } ?>>(GMT-08:00) Pacific Time (US &amp; Canada); Tijuana</option>  
                                <option value="America/Tijuana" <?php if(isset($this->timezoneValue))               {if($this->timezoneValue=="America/Tijuana")              { echo "selected";} } ?>>(GMT-08:00) Tijuana</option>  
                                <option value="America/Phoenix" <?php if(isset($this->timezoneValue))               {if($this->timezoneValue=="America/Phoenix")              { echo "selected";} } ?>>(GMT-07:00) Arizona</option>  
                                <option value="America/Denver" <?php if(isset($this->timezoneValue))               {if($this->timezoneValue=="America/Denver")              { echo "selected";} } ?>>(GMT-07:00) Mountain Time (US &amp; Canada)</option>  
                                <option value="America/Chihuahua" <?php if(isset($this->timezoneValue))               {if($this->timezoneValue=="America/Chihuahua")              { echo "selected";} } ?>>(GMT-07:00) Chihuahua, La Paz,Mazatlan </option>  
                                <option value="America/Managua" <?php if(isset($this->timezoneValue))               {if($this->timezoneValue=="America/Managua")              { echo "selected";} } ?>>(GMT-06:00) Central America</option>  
                                <option value="America/Chicago" <?php if(isset($this->timezoneValue))               {if($this->timezoneValue=="America/Chicago")              { echo "selected";} } ?>>(GMT-06:00) Central Time (US &amp; Canada)</option>  
                                <option value="America/Regina" <?php if(isset($this->timezoneValue))               {if($this->timezoneValue=="America/Regina")              { echo "selected";} } ?>>(GMT-06:00) Saskatchewan</option>  
                                <option value="America/Mexico_City" <?php if(isset($this->timezoneValue))               {if($this->timezoneValue=="America/Mexico_City")              { echo "selected";} } ?>>(GMT-06:00) Mexico City, Guadalajara, Monterrey</option>  
                                <option value="America/Indiana" <?php if(isset($this->timezoneValue))               {if($this->timezoneValue=="America/Indiana/Indianapolis")              { echo "selected";} } ?>>(GMT-05:00) Indiana (East)</option>  
                                <option value="America/Bogota" <?php if(isset($this->timezoneValue))               {if($this->timezoneValue=="America/Bogota")              { echo "selected";} } ?>>(GMT-05:00) Bogota, Lima, Quito</option>  
                                <option value="America/New_York" <?php if(isset($this->timezoneValue))               {if($this->timezoneValue=="America/New_York")              { echo "selected";} } ?>>(GMT-05:00) Eastern Time (US &amp; Canada)</option>  
                                <option value="America/Halifax" <?php if(isset($this->timezoneValue))               {if($this->timezoneValue=="America/Halifax")              { echo "selected";} } ?>>(GMT-04:00) Atlantic Time (Canada)</option>  
                                <option value="America/Caracas" <?php if(isset($this->timezoneValue))               {if($this->timezoneValue=="America/Caracas")              { echo "selected";} } ?>>(GMT-04:00) Caracas, La Paz</option>  
                                <option value="America/Santiago" <?php if(isset($this->timezoneValue))               {if($this->timezoneValue=="America/Santiago")              { echo "selected";} } ?>>(GMT-04:00) Santiago</option>  
                                <option value="America/St_Johns" <?php if(isset($this->timezoneValue))               {if($this->timezoneValue=="America/St_Johns")              { echo "selected";} } ?>>(GMT-03:30) Newfoundland</option>  
                                <option value="America/Buenos_Aires" <?php if(isset($this->timezoneValue))               {if($this->timezoneValue=="America/Buenos_Aires")              { echo "selected";} } ?>>(GMT-03:00) Buenos Aires, Georgetown</option>  
                                <option value="America/Sao_Paulo" <?php if(isset($this->timezoneValue))               {if($this->timezoneValue=="America/Sao_Paulo")              { echo "selected";} } ?>>(GMT-03:00) Brasilia</option>  
                                <option value="America/Godthab" <?php if(isset($this->timezoneValue))               {if($this->timezoneValue=="America/Godthab")              { echo "selected";} } ?>>(GMT-03:00) Greenland</option>  
                                <option value="America/Noronha" <?php if(isset($this->timezoneValue))               {if($this->timezoneValue=="America/Noronha")              { echo "selected";} } ?>>(GMT-02:00) Mid-Atlantic</option>  
                            </select>                       
                        </td>
                    </tr>
                    <tr>
                        <td colspan="2">
                            <br />
                            <input type="submit" class="button" id="submit" onclick="if(document.getElementById('TimezoneValue').value==''){alert('Please enter the timezone');return false;}" name="submit" value="Submit" />
                           
                            <input type="button" name="back" class = "button" value="Back" onclick="document.location.href='<?php echo(RESUFLOUtility::getIndexName()); ?>?m=settings';" />                       </td>
                    </tr>
                </table>
            </form>

            <script type="text/javascript">
                document.timezone.TimezoneValue.focus();
            </script>
        </div>
    </div>
    <div id="bottomShadow"></div>
<?php TemplateUtility::printFooter(); ?>
