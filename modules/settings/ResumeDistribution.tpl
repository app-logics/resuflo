<?php /* $Id: ChangePassword.tpl 1927 2007-02-22 06:03:24Z will $ */ ?>
<?php TemplateUtility::printHeader('Settings', array('modules/settings/validator.js', 'js/sorttable.js')); ?>
<?php TemplateUtility::printHeaderBlock(); ?>
<?php TemplateUtility::printTabs($this->active, $this->subActive); ?>

    <div id="main">
        <?php TemplateUtility::printQuickSearch(); ?>

        <div id="contents">
            <table>
                <tr>
                    <td width="3%">
                        <img src="images/settings.gif" width="24" height="24" border="0" alt="Settings" style="margin-top: 3px;" />&nbsp;
                    </td>
                    <td><h2>Settings: My Profile</h2></td>
                </tr>
            </table>

            <p class="note">Resume Distribution</p>

            <form name="calenderCred" id="calenderCred" action="<?php echo(RESUFLOUtility::getIndexName()); ?>?m=settings&amp;a=resumedistribution" method="post" >
                <table class="searchTable">
                    <?php foreach($this->recruiterArray as $row){ ?>
                        <tr>
                            <td>
                                <label id="currentPasswordLabel" for="currentPassword"><?php echo $row['first_name']; ?>:</label>
                            </td>
                            <td>
                                <input type="hidden" id="<?php echo $row['user_id'] ?>user_id" name="<?php echo $row['user_id'] ?>user_id" value="<?php echo $row['user_id'] ?>" />
                                <input type="text" style="width: 50px;" id="<?php echo $row['user_id'] ?>resume_distribution" name="<?php echo $row['user_id'] ?>resume_distribution" value="<?php echo $row['resume_distribution'] ?>" />%
                            </td>
                        </tr>
                    <?php } ?>
                    <tr>
                        <td colspan="2">
                            <br />
                            <input type="submit" class="button" id="submit" onclick="if(document.getElementById('username').value==''){alert('Please enter the username');return false;}" name="submit" value="Submit" />
                            <input type="button" name="back" class = "button" value="Back" onclick="document.location.href='<?php echo(RESUFLOUtility::getIndexName()); ?>?m=settings';" />                       
                        </td>
                    </tr>
                </table>
            </form>

            <script type="text/javascript">
                document.calenderCred.username.focus();
            </script>
        </div>
    </div>
    <div id="bottomShadow"></div>
<?php TemplateUtility::printFooter(); ?>
