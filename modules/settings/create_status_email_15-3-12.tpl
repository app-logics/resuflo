<?php /* $Id: Companies.tpl 3460 2007-11-07 03:50:34Z brian $ */ ?>
<?php TemplateUtility::printHeader('Companies', array('js/highlightrows.js', 'js/export.js', 'js/dataGrid.js','javascript/validations.js')); ?>
<?php TemplateUtility::printHeaderBlock(); ?>

<?php TemplateUtility::printTabs($this->active); ?>

<link rel="stylesheet" href="css/tab.css" />
<link rel="stylesheet" href="css/form.css" />
<script type="text/javascript" src="javascript/tab.js"></script>

<script type="text/javascript" src="javascript/validations.js"></script>
<!-- TinyMCE -->
<script type="text/javascript" src="jscripts/tiny_mce/tiny_mce.js"></script>
<script type="text/javascript">
	tinyMCE.init({
		// General options
		mode : "textareas",
		theme : "advanced",
		plugins : "pagebreak,style,layer,table,save,advhr,advimage,advlink,emotions,iespell,insertdatetime,preview,media,searchreplace,print,contextmenu,paste,directionality,fullscreen,noneditable,visualchars,nonbreaking,xhtmlxtras,template,wordcount,advlist,autosave,spellchecker",

		// Theme options
		theme_advanced_buttons3_add : "spellchecker",
        spellchecker_languages : "+English=en,Swedish=sv",

		theme_advanced_buttons1 : "save,newdocument,|,bold,italic,underline,strikethrough,|,justifyleft,justifycenter,justifyright,justifyfull,styleselect,formatselect,fontselect,fontsizeselect",
		theme_advanced_buttons2 : "cut,copy,paste,pastetext,pasteword,|,search,replace,|,bullist,numlist,|,outdent,indent,blockquote,|,undo,redo,|,link,unlink,anchor,image,cleanup,help,code,|,insertdate,inserttime,preview,|,forecolor,backcolor",
		theme_advanced_buttons3 : "tablecontrols,|,hr,removeformat,visualaid,|,sub,sup,|,charmap,emotions,iespell,media,advhr,|,print,|,ltr,rtl,|,fullscreen",
		theme_advanced_buttons4 : "insertlayer,moveforward,movebackward,absolute,|,styleprops,|,cite,abbr,acronym,del,ins,attribs,|,visualchars,nonbreaking,template,pagebreak,restoredraft",
		theme_advanced_toolbar_location : "top",
		theme_advanced_toolbar_align : "left",
		theme_advanced_statusbar_location : "bottom",
		theme_advanced_resizing : true,

		// Example content CSS (should be your site CSS)
		// using false to ensure that the default browser settings are used for best Accessibility
		// ACCESSIBILITY SETTINGS
		content_css : false,
		// Use browser preferred colors for dialogs.
		browser_preferred_colors : true,
		detect_highcontrast : true,

		// Drop lists for link/image/media/template dialogs
		template_external_list_url : "lists/template_list.js",
		external_link_list_url : "lists/link_list.js",
		external_image_list_url : "lists/image_list.js",
		media_external_list_url : "lists/media_list.js",

		// Style formats
		style_formats : [
			{title : 'Bold text', inline : 'b'},
			{title : 'Red text', inline : 'span', styles : {color : '#ff0000'}},
			{title : 'Red header', block : 'h1', styles : {color : '#ff0000'}},
			{title : 'Example 1', inline : 'span', classes : 'example1'},
			{title : 'Example 2', inline : 'span', classes : 'example2'},
			{title : 'Table styles'},
			{title : 'Table row 1', selector : 'tr', classes : 'tablerow1'}
		],

		// Replace values for the template plugin
		template_replace_values : {
			username : "Some User",
			staffid : "991234"
		}
	});
</script>
<!-- /TinyMCE -->

 
    <div id="main">
   
        <div id="contents">
            
			<!--tabs Starts-->		 
<table width="100%" border="0" cellspacing="0" cellpadding="0" bgcolor="#fff" >
  <tr>
    <td colspan="4" bgcolor="#f8f8f8">&nbsp;</td>
  </tr>
  <tr>
  <td colspan="2"  bgcolor="#f8f8f8" >
<a  href="javascript:void(0)" href="javascript:void(0)" onclick="javascript:goToURL('<?php echo(RESUFLOUtility::getIndexName()); ?>?m=settings&a=statusEmail');" ><< Back to main page </a>
    </td></tr>
	<tr>
    <td colspan="4" bgcolor="#fff" >
                                           <form name="frm1" id="frm1" action="<?php echo(RESUFLOUtility::getIndexName()); ?>?m=settings&a=createStatusEmail" onsubmit="return statusTemplateEmailAdd()" method="post">
	<table width="100%" border="0" cellspacing="2" cellpadding="2" bgcolor="#f8f8f8";>
                     
      <tr>
        <td>Name of Email Template<font color="#FF0000">*</font></td>
        <td><label>
        <input type="text" id="name"  name="name"  class="txtbox"  />
      </tr>
	  <tr>
        <td valign="top">Email Type:<font color="#FF0000">*</font></td>
        <td align="left" valign="top">
        <input type="radio" id="eventTag"  name="eventTag"  value="EMAIL_TEMPLATE_STATUSCHANGE" checked="checked"  />Change Status
		<input type="radio" id="eventTag"  name="eventTag"  value="EMAIL_TEMPLATE_SCHEDULEEVENT"  />Schedule Event
		</td>
      </tr>
      
      <tr>
        <td style="text-align:top">Message<font color="#FF0000">*</font>
          <div style="font-size:11px">
			<br/><br/><strong>Directions:<br /><small style="color:#FF0000">(Applicable for both Change status and Scheduling emails)*</small></strong><br/>
			%DATETIME% for current Date and Time <br/>
			%CANDFIRSTNAME% for Candidate First Name<br/>
			%CANDFULLNAME% for Candidate FullName<br/>
			%USERFULLNAME% for owner fullname<br /><br />
			
			<strong>For Change Status emails:<br /><small style="color:#FF0000">(Not applicable for Scheduling emails)*</small></strong><br/>
			%JBODTITLE% for Joborder Name<br/>
			%JBODCLIENT% for Client Name of Joborder<br/>
			%CANDPREVSTATUS% for Previous status<br/>
			%CANDSTATUS% for New status<br/><br />
			
			<strong>For Scheduling event emails<br /><small style="color:#FF0000">(Not applicable for Status change emails)*</small></strong><br/>
			%TITLE% for title of event<br />
			%DESC% for description of event<br />
			%EVENTTYPE% for event type<br />
			%VENUE% for venue of event<br />
			%TOMEET% for person to meet regarding event<br />
			%DATETIMEEVENT% for date and time of event<br />
		
		</div>
  	
     </td>
        <td valign="top"><textarea name="txtmessage" id="txtmessage" rows="4" cols="40" style="height:300px;width:500px;" class="txtbox"></textarea></td>
      </tr>
      <tr>
        <td>&nbsp;</td>
        <td><input type="image" name="create" src="images/save.gif" width="105" height="43" /></td>
      </tr>
      <tr>
        <td>&nbsp;</td>
        <td>&nbsp;</td>
      </tr>
    </table>   
	</form>  
	</td>
  </tr>
    
</table>
       
<script type="text/javascript">// <![CDATA[
var countries=new ddtabcontent("countrytabs")
countries.setpersist(true)
countries.setselectedClassTarget("link") //"link" or "linkparent"
countries.init()
// ]]></script>   
 <!--tabs end-->	
        </div>
    </div>
    <div id="bottomShadow"></div>
<?php TemplateUtility::printFooter(); ?>
