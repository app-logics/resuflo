<?php /* $Id: signature.tpl 1927 2007-02-22 06:03:24Z will $ */ ?>
<?php TemplateUtility::printHeader('Settings', array('modules/settings/validator.js', 'js/sorttable.js')); ?>
<?php TemplateUtility::printHeaderBlock(); ?>
<?php TemplateUtility::printTabs($this->active, $this->subActive); ?>
<?php 
	$userid= $_SESSION['RESUFLO']->getuserid(); 
	$sql = mysql_query("select signature from user where user_id = '$userid'");
	$row =mysql_fetch_assoc($sql);
	 $signature =$row['signature'];
	//$signature= strip_tags($signature);
?>
<!-- nicEdit -->
  <script type="text/javascript" src="jscripts/nicEdit-latest.js"></script> <script type="text/javascript">
//<![CDATA[
  bkLib.onDomLoaded(function() {
        new nicEditor({fullPanel : true}).panelInstance('signature1');
  });
  //]]>
  </script>
    <div id="main">
        <?php TemplateUtility::printQuickSearch(); ?>

        <div id="contents">
            <table>
                <tr>
                    <td width="3%">
                        <img src="images/settings.gif" width="24" height="24" border="0" alt="Settings" style="margin-top: 3px;" />&nbsp;
                    </td>
                    <td><h2>Settings: My Profile</h2></td>
                </tr>
            </table>

            <p class="note">Add/Change Signature</p>

            <form name="signatureform" id="signatureform" action="<?php echo(RESUFLOUtility::getIndexName()); ?>?m=settings&amp;a=signature" method="post" onsubmit="return signatureform(document.signatureform);">
                <input type="hidden" name="postback" id="postback" value="postback" />
          
                <?php if ($this->isDemoUser): ?>
                    Note that as a demo user, you do not have privileges to modify any settings.
                    <br /><br />
                <?php endif; ?>

                <table class="searchTable">
                    <tr>
                        <td colspan="2">
                            <span class="bold">Add/Change Signature</span>
                            <br />
                            <br />
                            <span id='passwordErrorMessage' style="font:smaller; color: red">
                                <?php if (isset($this->errorMessage)): ?>
                                        <?php $this->_($this->errorMessage); ?>
                                <?php endif; ?>
                            </span>
                        </td>
                    </tr>


                    <tr>
                        <td style="vertical-align: top;">
                            <label id="signatureLabel" for="signature">Add Signature:&nbsp;*</label>&nbsp;
                        </td>
                        <td>
                            <textarea class="inputbox" id="signature1" name="signature1" style="width: 500px; height: 100px;"><?php echo $signature; ?></textarea>
                        </td>
                    </tr>

                  
                    <tr>
                        <td></td>
                        <td>
                            <br />
                            <input type="submit" class="button" id="signature"  name="signature" value="Add Signature" />
                            <input type="reset"  class="button" id="reset"          name="reset"          value="Reset" />
                            <input type="button" name="back" class = "button" value="Back" onclick="document.location.href='<?php echo(RESUFLOUtility::getIndexName()); ?>?m=settings';" />
                       </td>
                    </tr>
                </table>
            </form>

            <script type="text/javascript">
                document.signatureform.signature1.focus();
            </script>
        </div>
    </div>
    <div id="bottomShadow"></div>
<?php TemplateUtility::printFooter(); ?>

