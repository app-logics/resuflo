<?php /* $Id: Companies.tpl 3460 2007-11-07 03:50:34Z brian $ */ ?>
<?php TemplateUtility::printHeader('Companies', array('js/highlightrows.js', 'js/export.js', 'js/dataGrid.js','javascript/validations.js')); ?>
<?php TemplateUtility::printHeaderBlock(); ?>

<?php TemplateUtility::printTabs($this->active); ?>

<link rel="stylesheet" href="css/tab.css" />
<link rel="stylesheet" href="css/form.css" />
<script type="text/javascript" src="javascript/tab.js"></script>

<script type="text/javascript" src="javascript/validations.js"></script>
<!-- TinyMCE -->
<!--<script src="jscripts/tinymce/tinymce.min.js" type="text/javascript"></script>-->
<!-- /TinyMCE -->
 
    <div id="main">
   
        <div id="contents">
            
			<!--tabs Starts-->		 
<table width="100%" border="0" cellspacing="0" cellpadding="0" bgcolor="#fff" >
  <tr>
    <td colspan="4" bgcolor="#f8f8f8">&nbsp;</td>
  </tr>
  <tr>
  <td colspan="2"  bgcolor="#f8f8f8" >
<a  href="javascript:void(0)" href="javascript:void(0)" onclick="javascript:goToURL('<?php echo(RESUFLOUtility::getIndexName()); ?>?m=settings&a=statusEmail');" ><< Back to main page </a>
    </td></tr>
	<tr>
    <td colspan="4" bgcolor="#fff" >
                                           <form name="frm1" id="frm1" action="<?php echo(RESUFLOUtility::getIndexName()); ?>?m=settings&a=createStatusEmail" onsubmit="return statusTemplateEmailAdd()" method="post" enctype="multipart/form-data">
	<table width="100%" border="0" cellspacing="2" cellpadding="2" bgcolor="#f8f8f8";>
                     
      <tr>
        <td>Subject of Email Template<font color="#FF0000">*</font></td>
        <td><label>
        <input type="text" id="name"  name="name"  class="txtbox"  />
      </tr>
	  <tr>
        <td valign="top" style="display: none;">Email Type:<font color="#FF0000">*</font></td>
        <td align="left" valign="top" style="display: none;">
        <input type="radio" id="eventTag"  name="eventTag"  value="EMAIL_TEMPLATE_STATUSCHANGE"  />Change Status
		<input type="radio" id="eventTag"  name="eventTag"  value="EMAIL_TEMPLATE_SCHEDULEEVENT"  checked="checked" />Schedule Event
		</td>
      </tr>
	  <tr>
        <td valign="top">Upload Attachment:</td>
        <td align="left" valign="top">
        <input type="file" id="uploadAttach"  name="uploadAttach"  value="" />
		</td>
      </tr>
      <tr>
	  	
	  </tr>
      <tr>
        <td style="text-align:top">Message<font color="#FF0000">*</font>
          <div style="font-size:11px">
			<br/><br/><strong>Directions:<br /><small style="color:#FF0000">(Applicable for both Change status and Scheduling emails)*</small></strong><br/>
			%DATETIME% for current Date and Time <br/>
			%CANDFIRSTNAME% for Candidate First Name<br/>
			%CANDFULLNAME% for Candidate FullName<br/>
			%USERFULLNAME% for owner fullname<br /><br />
			
			<strong>For Change Status emails:<br /><small style="color:#FF0000">(Not applicable for Scheduling emails)*</small></strong><br/>
			%JBODTITLE% for Joborder Name<br/>
			%JBODCLIENT% for Client Name of Joborder<br/>
			%CANDPREVSTATUS% for Previous status<br/>
			%CANDSTATUS% for New status<br/><br />
			
			<strong>For Scheduling event emails<br /><small style="color:#FF0000">(Not applicable for Status change emails)*</small></strong><br/>
			%TITLE% for title of event<br />
			%DESC% for description of event<br />
			%EVENTTYPE% for event type<br />
			%VENUE% for venue of event<br />
			%TOMEET% for person to meet regarding event<br />
			%DATETIMEEVENT% for date and time of event<br />
		
		</div>
  	
     </td>
        <td valign="top">
            <textarea name="txtmessage" id="txtmessage" rows="4" cols="40" style="height:300px;width:500px;" class="txtbox"></textarea>
            <input name="image" type="file" id="upload" class="hidden" onchange="" style="display: none;">
        </td>
      </tr>
      <tr>
        <td>&nbsp;</td>
        <td><input type="image" name="create" src="images/save.gif" width="105" height="43" /></td>
      </tr>
      <tr>
        <td>&nbsp;</td>
        <td>&nbsp;</td>
      </tr>
    </table>   
	</form>  
	</td>
  </tr>
    
</table>
<script src="jscripts/ckeditor/ckeditor.js" type="text/javascript"></script> 
<script type="text/javascript">
    CKEDITOR.replace('txtmessage');
</script>
<script type="text/javascript">// <![CDATA[
var countries=new ddtabcontent("countrytabs")
countries.setpersist(true)
countries.setselectedClassTarget("link") //"link" or "linkparent"
countries.init()
// ]]></script>   
 <!--tabs end-->	
        </div>
    </div>
    <div id="bottomShadow"></div>
<?php TemplateUtility::printFooter(); ?>
