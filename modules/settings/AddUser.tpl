<?php /* $Id: AddUser.tpl 3810 2007-12-05 19:13:25Z brian $ */ ?>
<?php TemplateUtility::printHeader('Settings', array('modules/settings/validator.js', 'js/sorttable.js')); ?>
<?php TemplateUtility::printHeaderBlock(); ?>
<?php TemplateUtility::printTabs($this->active, $this->subActive); ?>
<div id="main">
        <?php TemplateUtility::printQuickSearch(); ?>

        <div id="contents">
            <table>
                <tr>
                    <td width="3%" valign="bottom">
                        <img src="images/settings.gif" width="24" height="24" border="0" alt="Settings" style="margin-top: 3px;" />&nbsp;
                    </td>
                    <td valign="bottom"><h2>Settings: Add Site User</h2></td>
                </tr>
            </table>

            <p class="note">
                <span style="float: left;">Add Site User</span>
                <span style="float: right;"><a href='<?php echo(RESUFLOUtility::getIndexName()); ?>?m=settings&amp;a=manageUsers'>Back to User Management</a></span>&nbsp;
            </p>

            <form name="addUserForm" id="addUserForm" action="<?php echo(RESUFLOUtility::getIndexName()); ?>?m=settings&amp;a=addUser" method="post" onsubmit="return checkAddUserForm(document.addUserForm);" autocomplete="off" enctype="multipart/form-data">
                <input type="hidden" name="postback" id="postback" value="postback" />

                <table width="930">
                    <tr>
                        <td align="left" valign="top">
                            <table class="editTable" width="550">
                                <tr>
                                    <td class="tdVertical">
                                        <label id="firstNameLabel" for="firstName"><?php if($_SESSION['RESUFLO']->getAccessLevel()==400 && $_SESSION['RESUFLO']->getClientLogin()==1 ){ echo "Recruiter";}else{ echo "Company";}?> Name:</label>
                                    </td>
                                    <td class="tdData">
                                        <input type="text" class="inputbox" maxlength="15" id="firstName" name="firstName" style="width: 150px;" />&nbsp;*
                                    </td>
                                </tr>

                             <!--   <tr>
                                    <td class="tdVertical">
                                        <label id="lastNameLabel" for="lastName">Last Name:</label>
                                    </td>
                                    <td class="tdData">
                                        <input type="text" class="inputbox"  maxlength="15" id="lastName" name="lastName" style="width: 150px;" />&nbsp;*
                                    </td>
                                </tr>-->

                                <tr>
                                    <td class="tdVertical">
                                        <label id="emailLabel" for="username">E-Mail:</label>
                                    </td>
                                    <td class="tdData">
                                        <input type="text" class="inputbox" id="email" name="email" style="width: 150px;" />
                                    </td>
                                </tr>

                                <tr>
                                    <td class="tdVertical">
                                        <label id="usernameLabel" for="username">Username</label>
                                    </td>
                                    <td class="tdData">
                                        <input type="text" class="inputbox"  maxlength="15" id="username" name="username" style="width: 150px;" />&nbsp;*
                                    </td>
                                </tr>
                                <?php if($_SESSION['RESUFLO']->getAccessLevel()==400 && $_SESSION['RESUFLO']->getClientLogin()==1 ){?>
                                    <input type="hidden" name="entered_by" value="1" />
                                    <?php }else{?>
                                    <tr>
                                        <td class="tdVertical">Master Account:</td>
                                        <td class="tdData">
                                            <select name="entered_by" style="width: 154px;">
                                                <?php foreach ($this->allActiveUsers as $user): ?>
                                                    <option value="<?php echo $user['userid']; ?>" <?php if($this->data['userID']==$user['userid']){ echo " selected";}?>><?php echo $user['username']; ?></option>
                                                <?php endforeach; ?>
                                            </select>
                                        </td>
                                    </tr>
                                <?php } ?>

                                <tr>
                                    <td class="tdVertical">
                                        <label id="passwordLabel" for="password">Password:</label>
                                    </td>
                                    <td class="tdData">
                                        <input type="password"  maxlength="15" class="inputbox" id="password" name="password" style="width: 150px;" />&nbsp;*
                                    </td>
                                </tr>

                                <tr>
                                    <td class="tdVertical">
                                        <label id="retypePasswordLabel" for="retypePassword">Retype Password:</label>
                                    </td>
                                    <td class="tdData">
                                        <input type="password"  maxlength="15" class="inputbox" id="retypePassword" name="retypePassword" style="width: 150px;" />&nbsp;*
                                    </td>
                                </tr>
<?php if($_SESSION['RESUFLO']->getAccessLevel()==400 && $_SESSION['RESUFLO']->getClientLogin()==1 ){ }else{?>
                                <tr>
                                    <td class="tdVertical">
                                        <label id="retypePasswordLabel" for="retypePassword">Company Logo:</label>
                                    </td>
                                    <td class="tdData">
                                        <input type="file" class="inputbox" id="logo" name="logo" style="width: 150px;" />&nbsp;
                                    </td>
                                </tr>
								<?php } ?>
								<!--<tr>
                                    <td colspan="2" class="tdVertical">
                                       <strong>(NOTE: Please upload a logo of dimension 277*52)</strong>
                                    </td>
                                </tr>-->
                                <tr>
                                    <td class="tdVertical">
                                        <label id="accessLevelLabel" for="accessLevel">Access Level:</label>
                                    </td>
                                    <td class="tdData">
                                        <span id="accessLevelsSpan">
                                            <?php foreach ($this->accessLevels as $accessLevel): ?>
											<?php if($_SESSION['RESUFLO']->getAccessLevel()==400 && $_SESSION['RESUFLO']->getClientLogin()==1 && ($accessLevel['accessID']==400 || $accessLevel['accessID']==500)){ continue;}?>
                                                <?php if ($accessLevel['accessID'] > $this->accessLevel): continue; endif; ?>
                                                <?php if (!$this->license['canAdd'] && !$this->license['unlimited'] && $accessLevel['accessID'] > ACCESS_LEVEL_READ): continue; endif; ?>

                                                <?php $radioButtonID = 'access' . $accessLevel['accessID']; ?>

                                                <input type="radio" name="accessLevel" id="<?php echo($radioButtonID); ?>" value="<?php $this->_($accessLevel['accessID']); ?>" title="<?php $this->_($accessLevel['longDescription']); ?>" <?php if ($accessLevel['accessID'] == $this->defaultAccessLevel): ?>checked<?php endif; ?> onclick="document.getElementById('userAccessStatus').innerHTML='<?php $this->_($accessLevel['longDescription']); ?>'; <?php if($accessLevel['accessID'] >= ACCESS_LEVEL_SA): ?>document.getElementById('eeoIsVisible').checked=true; document.getElementById('eeoIsVisible').disabled=true;  document.getElementById('eeoVisibleSpan').style.display='none';<?php else: ?>document.getElementById('eeoIsVisible').disabled=false;<?php endif; ?>" />
                                                <label for="<?php echo($radioButtonID); ?>" title="<?php $this->_(str_replace('\'', '\\\'', $accessLevel['longDescription'])); ?>">
                                                    <?php $this->_($accessLevel['shortDescription']); ?>
                                                    <?php if ($accessLevel['accessID'] == $this->defaultAccessLevel): ?>(Default)<?php endif; ?>
                                                </label>
                                                <br />
                                            <?php endforeach; ?>
                                        </span>
                                    </td>
                                </tr>

                                <tr>
                                    <td class="tdVertical">Access Description:</td>
                                    <td class="tdData">
                                        <span id="userAccessStatus">Delete - All lower access, plus the ability to delete information on the system.</span>
                                    </td>
                                </tr>
								<?php if($_SESSION['RESUFLO']->getAccessLevel()==400 && $_SESSION['RESUFLO']->getClientLogin()==1 ){?> 
								<input type="hidden" name="clientlogin" value="1" />
								<?php }else{?>
								 <tr>
                                    <td class="tdVertical">Client Login:</td>
                                    <td class="tdData">
									<select name="clientlogin">
									<option value="0">No</option>
									<option value="1">Yes</option>
									</select>
                                    </td>
                                </tr>
<?php } ?>

                                <tr>
                                    <td class="tdVertical">
                                        <label for="is_nyl">Is NYL?:</label>
                                    </td>
                                    <td class="tdData">
                                        <select name="is_nyl">
                                            <option value="0" <?php if($this->data['is_nyl']!='1'){ echo " selected";}?>>No</option>
                                            <option value="1" <?php if($this->data['is_nyl']=='1'){ echo " selected";}?>>Yes</option>
                                        </select>
                                    </td>
                                </tr>

                                <?php if (count($this->categories) > 0): ?>
                                    <tr>
                                        <td class="tdVertical">
                                            <label id="accessLevelLabel" for="accessLevel">Role:</label>
                                        </td>
                                        <td class="tdData">
                                           <input type="radio" name="role" value="none" title="" checked onclick="document.getElementById('userRoleDesc').innerHTML='This user is a normal user.';  document.getElementById('accessLevelsSpan').style.display='';" /> Normal User
                                           <br />
                                           <?php foreach ($this->categories as $category): ?>
                                               <?php if (isset($category[4])): ?>
                                                   <input type="radio" name="role" value="<?php $this->_($category[1]); ?>" onclick="document.getElementById('userRoleDesc').innerHTML='<?php echo(str_replace('\'', '\\\'', $category[2])); ?>'; document.getElementById('access<?php echo($category[4]); ?>').checked=true; document.getElementById('accessLevelsSpan').style.display='none';" /> <?php $this->_($category[0]); ?>
                                               <?php else: ?>
                                                   <input type="radio" name="role" value="<?php $this->_($category[1]); ?>" onclick="document.getElementById('userRoleDesc').innerHTML='<?php echo(str_replace('\'', '\\\'', $category[2])); ?>'; document.getElementById('accessLevelsSpan').style.display='';" /> <?php $this->_($category[0]); ?>
                                               <?php endif; ?>
                                               <br />
                                           <?php endforeach; ?>
                                        </td>
                                    </tr>
                                    <tr>
                                        <td class="tdVertical">Role Description:</td>
                                        <td class="tdData">
                                            <span id="userRoleDesc" style="font-size: smaller">This user is a normal user.</span>
                                        </td>
                                    </tr>
                                <?php else: ?>
                                    <span style="display:none;">
                                        <input type="radio" name="role" value="none" title="" checked /> Normal User
                                    </span>
                                <?php endif; ?>
                                <?php if($this->EEOSettingsRS['enabled'] == 1): ?>
                                     <tr>
                                        <td class="tdVertical">Allowed to view EEO Information:</td>
                                        <td class="tdData">
                                            <span id="eeoIsVisibleCheckSpan">
                                                <input type="checkbox" name="eeoIsVisible" id="eeoIsVisible" onclick="if (this.checked) document.getElementById('eeoVisibleSpan').style.display='none'; else document.getElementById('eeoVisibleSpan').style.display='';">
                                                &nbsp;This user is <span id="eeoVisibleSpan">not </span>allowed to edit and view candidate's EEO information.
                                            </span>
                                        </td>
                                    </tr>
                                <?php endif; ?>
                                <?php if (!$this->license['canAdd'] && !$this->license['unlimited']): ?>
                                    <tr>
                                        <td class="tdVertical">Notice:</td>
                                        <td class="tdData" style="color: #800000;">
                                            <b>You are currently using your full allotment of active user accounts. Disable an existing account or upgrade your license to add another active user.</b>
                                        </td>
                                    </tr>
                                <?php endif; ?>
                            </table>
                        </td>
                        <?php
                        eval(Hooks::get('SETTINGS_USERS_FULLQUOTALICENSES'));
                        if (!$this->license['canAdd'] && !$this->license['unlimited'] && LicenseUtility::isProfessional() && !file_exists('modules/asp'))
                        {
                            echo '<td valign="top" align="center">';
                            $link = 'http://www.econnoisseur.com/professional';
                            $image = 'images/add_licenses.jpg';

                            echo '<a href="' . $link . '">';
                            echo '<img src="' . $image . '" border="0" alt="Click here to add more user licenses"/>';
                            echo '</a>';
                            echo '<div style="text-align: left; padding: 10px 25px 0px 25px;">';
                            echo 'A <i>user license</i>, or <i>seat</i>, is the limit of full-access users you can have. You may ';
                            echo 'have unlimited read only users.';
                            echo '<p>';

                            echo 'This version of RESUFLO is licensed to:<br /><center>';
                            echo '<b>' . LicenseUtility::getName() . '</b><br />';
                            $seats = LicenseUtility::getNumberOfSeats();
                            echo ucfirst(StringUtility::cardinal($seats)) . ' ('.$seats.') user license'.($seats!=1?'s':'').'<br />';
                            echo 'Valid until ' . date('m/d/Y', LicenseUtility::getExpirationDate()) . '<br />';
                            echo '</center>';


                            echo '<p>';
                            echo 'Click <a href="<?php echo $link; ?>">here</a> to purchase additional user seats.';
                            echo '</div></td>';
                        }
                        ?>
                    </tr>
                </table>

                <input type="submit" class="button" name="submit" id="submit" value="Submit" />&nbsp;
                <input type="reset"  class="button" name="reset"  id="reset"  value="Reset" onclick="document.getElementById('userAccessStatus').innerHTML='Delete - All lower access, plus the ability to delete information on the system.'" />&nbsp;
                <input type="button" class="button" name="back"   id="back"   value="Cancel" onclick="javascript:goToURL('<?php echo(RESUFLOUtility::getIndexName()); ?>?m=settings&amp;a=manageUsers');" />
            </form>
        </div>
    </div>

    <div id="bottomShadow"></div>
<?php TemplateUtility::printFooter(); ?>
