<?php /* $Id: ChangePassword.tpl 1927 2007-02-22 06:03:24Z will $ */ ?>
<?php TemplateUtility::printHeader('Settings', array('modules/settings/validator.js', 'js/sorttable.js','javascript/validations.js')); ?>
<?php TemplateUtility::printHeaderBlock(); ?>
<?php TemplateUtility::printTabs($this->active, $this->subActive); ?>

<div id="main">
    <?php TemplateUtility::printQuickSearch(); ?>
    
    <div id="contents">
        <table>
            <tr>
                <td width="3%">
                    <img src="images/settings.gif" width="24" height="24" border="0" alt="Settings" style="margin-top: 3px;" />&nbsp;
                </td>
                <td><h2>Settings: My Profile</h2></td>
            </tr>
        </table>
        
        <form name="calenderCred" id="calenderCred" action="" method="post" >
            <font style="font-weight:bold;" ><?php echo $this->msg; ?></font><br />
            <table width="100%" border="0" cellspacing="0" cellpadding="0" bgcolor="#f8f8f8" >
                <tr>
                    <td colspan="4" bgcolor="#f8f8f8" style="padding-left:10px;"><a href="<?php echo(RESUFLOUtility::getIndexName()); ?>?m=settings&amp;a=createSchedulerEmail"><img src="images/button_template.gif" width="158" border="0" height="31" /></a></td>
                </tr>
                <tr>
                    <td colspan="4" bgcolor="#F8F8F8" class="font_tab">&nbsp;</td>
                </tr>
                <tr>
                    <td width="33%" style="padding-left:30px;" height="35" bgcolor="#e2e2e2" class="font_tab"><strong>Scheduler Confirmation Email</strong></td>
                    <td width="22%" bgcolor="#e2e2e2" class="font_tab"><strong>Type</strong></td>
                    <td width="22%" bgcolor="#e2e2e2" class="font_tab">&nbsp;</td>
                    <td width="14%"style="padding-left:30px;"  bgcolor="#e2e2e2" class="font_tab" ><strong>Actions</strong></td>
                </tr>
                <?php foreach($this->emailArray as $row){ ?>
                <tr>
                    <?php if($row['tag'] != 'SCHEDULER_EMAIL_TEMPLATE_CONFIRMATION')continue;?>
                    <td width="33%" style="padding-left:30px;" height="35"  class="font_tab"><?php echo $row['name']; ?></td>
                    <td width="22%" class="font_tab">Confirmation Email</td>
                    <td width="22%" class="font_tab"></td>
                    <td width="14%"  class="font_tab" ><table width="80%" border="0" cellspacing="0" cellpadding="0">
                            <tr>
                                <td align="center"><a href="<?php echo(RESUFLOUtility::getIndexName()); ?>?m=settings&amp;a=editSchedulerEmail&amp;id=<?php echo $row['email_template_id']; ?>"><img src="images/edit_ico.gif"  border="0"/></a></td>
                                <td><a href="javascript:void(0)" onClick="deleteStatusEmailTemplate('<?php echo(RESUFLOUtility::getIndexName()); ?>?m=settings&amp;a=deleteSchedulerEmail&amp;id=<?php echo $row['email_template_id']; ?>')" ><img src="images/delete_ico.gif"  border="0"/></a></td>
                                <?php 
                                if($row['in_use'] == '0'){
                                echo '<td><a href="index.php?m=settings&amp;a=setDefaultSchedulerConfirmationEmail&amp;email_template_id='.$row['email_template_id'].'"><img src="images/grey_star.jpg" border="0"></a></td>';
                                }
                                else
                                {
                                echo '<td><a href="#"><img src="images/golden_star.jpg" border="0"></a></td>';
                                } 
                                ?>
                            </tr>
                        </table>
                    </td>
                </tr>
                <?php } ?>
                <tr>
                    <td width="33%" style="padding-left:30px;" height="35" bgcolor="#e2e2e2" class="font_tab"><strong>Scheduler Reminder Email</strong></td>
                    <td width="22%" bgcolor="#e2e2e2" class="font_tab"><strong>Type</strong></td>
                    <td width="22%" bgcolor="#e2e2e2" class="font_tab"><strong>Days</strong></td>
                    <td width="14%"style="padding-left:30px;"  bgcolor="#e2e2e2" class="font_tab" ><strong>Actions</strong></td>
                </tr>
                <?php foreach($this->emailArray as $row){ ?>
                <tr>
                    <?php if($row['tag'] != 'SCHEDULER_EMAIL_TEMPLATE_REMINDER')continue;?>
                    <td width="33%" style="padding-left:30px;" height="35"  class="font_tab"><?php echo $row['name']; ?></td>
                    <td width="22%" class="font_tab">Reminder Email</td>
                    <td width="22%" class="font_tab"><?php echo $row['reminder_days']; ?></td>
                    <td width="14%"  class="font_tab" >
                        <table width="80%" border="0" cellspacing="0" cellpadding="0">
                            <tr>
                                <td align="center"><a href="<?php echo(RESUFLOUtility::getIndexName()); ?>?m=settings&amp;a=editSchedulerEmail&amp;id=<?php echo $row['email_template_id']; ?>"><img src="images/edit_ico.gif"  border="0"/></a></td>
                                <td><a href="javascript:void(0)" onClick="deleteStatusEmailTemplate('<?php echo(RESUFLOUtility::getIndexName()); ?>?m=settings&amp;a=deleteSchedulerEmail&amp;id=<?php echo $row['email_template_id']; ?>')" ><img src="images/delete_ico.gif"  border="0"/></a></td>
                                <!--<?php 
                                if($row['in_use'] == '0'){
                                echo '<td><a href="index.php?m=settings&amp;a=setDefaultSchedulerReminderEmail&amp;email_template_id='.$row['email_template_id'].'"><img src="images/grey_star.jpg" border="0"></a></td>';
                                }
                                else
                                {
                                echo '<td><a href="#"><img src="images/golden_star.jpg" border="0"></a></td>';
                                } 
                                ?>-->
                            </tr>
                        </table>
                    </td>
                </tr>
                <?php } ?>
            </table>
        </form>
        
        <script type="text/javascript">
            document.calenderCred.username.focus();
        </script>
    </div>
</div>
<div id="bottomShadow"></div>
<?php TemplateUtility::printFooter(); ?>
