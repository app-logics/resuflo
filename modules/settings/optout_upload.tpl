<?php TemplateUtility::printHeader('Settings');?>
<?php TemplateUtility::printHeaderBlock(); ?>
<?php TemplateUtility::printTabs($this->active, $this->subActive); ?>

    <div id="main">
        <?php TemplateUtility::printQuickSearch(); ?>

        <div id="contents">
            <table>
                <tr>
                    <td width="3%">
                        <img src="images/settings.gif" width="24" height="24" border="0" alt="Settings" style="margin-top: 3px;" />&nbsp;
                    </td>
                    <td><h2>Settings: Upload Optout Emails</h2></td>
                </tr>
            </table>

            <p class="note">Upload a list of email addresses that should never be imported or emailed campaigned to</p>
            <?php if (!empty($this->rows)) : ?>
              <font style="font-weight:bold; font-size:1.5em; font-style:italic; color:red;">Upload complete. Stats:</font><br />
              <ul>
                <li>Emails found in CSV: <?php echo $this->rows['processed']; ?></li>
                <li>Emails added to opt-out: <?php echo $this->rows['entered']; ?></li>
                <li>Emails rejected (for duplication?): <?php echo $this->rows['duplicated']; ?></li>
                <li>Emails rejected for validation: <?php echo $this->rows['invalid']; ?></li>
              </ul>
              <?php if (empty($this->rows['entered'])) : ?>
                <p><em>The first column must be the email column.</em></p>
              <?php endif; ?>

            <?php else : ?>
              <p>The optout csv should have a column of emails, other columns may exist but will be ignored and not imported.</p>
            <?php endif; ?>
            <form action="<?php echo(RESUFLOUtility::getIndexName()); ?>?m=settings&amp;a=optoutProcess" method="post" enctype="multipart/form-data">
                <?php if (!empty($this->msg)) : ?><font style="font-weight:bold; font-size:1.5em; font-style:italic; color:red;"><?php echo $this->msg; ?></font><br /><br /><?php endif; ?>
                <table class="searchTable">
                    <tr>
                        <td><label id="csv" for="csv">CSV Upload:</label> &nbsp;</td>
                        <td><input type="file" class="inputbox" id="csv" name="csv"/>&nbsp;*</td>
                    </tr>
                    <tr>
                        <td> &nbsp;</td>
                        <td><input type="submit" value="Upload CSV"/>&nbsp;</td>
                    </tr>
                </table>
            </form>
        </div>
    </div>
    <div id="bottomShadow"></div>
<?php TemplateUtility::printFooter(); ?>
