<?php /* $Id: Search.tpl 1948 2007-02-23 09:49:27Z will $ */ ?>
<?php TemplateUtility::printHeader('Jobs', array('js/highlightrows.js', 'modules/activity/validator.js', 'js/sweetTitles.js')); ?>
<?php TemplateUtility::printHeaderBlock(); ?>
<?php TemplateUtility::printTabs($this->active, $this->subActive); ?>
<style>
    label{
        text-align: right;
        float: right;
    }
</style>
    <div id="main">
        <div id="contents">
            <br>
            <div style="width: 100%;">
                <form action="index.php?m=job&a=add" method="post" enctype="multipart/form-data">
                    <input type="hidden" name="postback" id="postback" value="postback" />
                    <input type="hidden" name="job_id" id="job_id" value="<?php echo $this->job['job_id']; ?>" />
                    <table>
                        <tr>
                            <td><label for="status">Job Status*</label></td>
                            <td style="width: 320px;">
                                <select class="inputbox" id="status" name="status" required="">
                                    <option value="">Select Status</option>
                                    <option value="1">Active</option>
                                    <option value="2">Drafting</option>
                                    <option value="3">On Hold</option>
                                    <option value="6">Closed</option>
                                </select>
                            </td>
                            <td colspan="2">
                                <?php if($_GET['er'] == -1){ ?>
                                    <h4 style="color: red;">Job could not saved successfully</h4>
                                <?php }?>
                            </td>
                        </tr>
                        <tr>
                            <td><label for="title">Job Title*</label></td>
                            <td><input type="text" class="inputbox" id="title" name="title" value="<?php echo $this->job['title']; ?>" style="width: 266px;" required=""></td>
                        </tr>
                        <tr>
                            <td><label for="category">Job Category*</label></td>
                            <td>
                                <select class="inputbox" id="category" name="category" required="">
                                    <option value="">Select Category</option>
                                    <option value="1">Executive/Senior Level Officials and Managers</option>
                                    <option value="2">Professionals</option>
                                    <option value="3">Technicians</option>
                                    <option value="4">Sales Workers</option>
                                    <option value="5">Administrative Support Workers</option>
                                    <option value="6">Craft Workers</option>
                                    <option value="7">Operatives</option>
                                    <option value="8">Laborers and Helpers</option>
                                    <option value="9">Service Workers</option>
                                    <option value="10">First/Mid Level Officials &amp; Managers</option>
                                </select>
                            </td>
                            <td><label for="company_name">Company Name*</label></td>
                            <td><input type="text" class="inputbox" id="company_name" name="company_name" value="<?php echo $this->job['company_name']; ?>" required=""></td>
                        </tr>
                        <tr>
                            <td><label for="type">Job Type*</label></td>
                            <td>
                                <select class="inputbox" id="type" name="type" required="">
                                    <option value="">Select job type..</option>
                                    <option value="1">Full-Time</option>
                                    <option value="2">Part-Time</option>
                                    <option value="6">Contract</option>
                                    <option value="4">Temporary</option>
                                </select>
                            </td>
                            <td><label for="company_url">Company Website*</label></td>
                            <td><input type="text" class="inputbox" id="company_url" name="company_url" value="<?php echo $this->job['company_url']; ?>" required=""></td>
                        </tr>
                        <tr>
                            <td><label for="experience">Experience*</label></td>
                            <td>
                                <select class="inputbox" id="experience" name="experience" required="">
                                    <option value="">Select experience</option>
                                    <option value="0">0 year</option>
                                    <option value="1">1 year</option>
                                    <option value="2">2 year</option>
                                    <option value="3">3 year</option>
                                    <option value="4">4 year</option>
                                    <option value="5">5 year</option>
                                    <option value="6">6 year</option>
                                    <option value="7">7 year</option>
                                    <option value="8">8 year</option>
                                    <option value="9">9 year</option>
                                    <option value="10">10 year</option>
                                    <option value="11">11 year</option>
                                    <option value="12">12 year</option>
                                    <option value="13">13 year</option>
                                    <option value="14">14 year</option>
                                    <option value="15">15 year</option>
                                    <option value="16">16 year</option>
                                    <option value="17">17 year</option>
                                    <option value="18">18 year</option>
                                    <option value="19">19 year</option>
                                    <option value="20">20 year</option>
                                </select>
                            </td>
                            <td><label for="address">Address*</label></td>
                            <td><input type="text" id="address" name="address" value="<?php echo $this->job['address']; ?>" class="inputbox"> </td>
                        </tr>
                        <tr>
                            <td><label for="email">Email*</label></td>
                            <td><input type="text" id="email" name="email" value="<?php echo $this->job['email']; ?>" class="inputbox"> </td>
                            <td><label for="zip_code">Zip Code*</label></td>
                            <td><input type="text" id="zip_code" name="zip_code" value="<?php echo $this->job['zip_code']; ?>" class="inputbox"> </td>
                        </tr>
                        <tr>
                            <td><label for="phone">Phone*</label></td>
                            <td><input type="text" id="phone" name="phone" value="<?php echo $this->job['phone']; ?>" class="inputbox"> </td>
                            <td><label for="city">City*</label></td>
                            <td><input type="text" id="city" name="city" value="<?php echo $this->job['city']; ?>" class="inputbox"> </td>
                        </tr>
                        <tr>
                            <td><label for="salary">Salary*</label></td>
                            <td><input type="text" id="salary" name="salary" value="<?php echo $this->job['salary']; ?>" class="inputbox"> </td>
                            <td><label for="state">State*</label></td>
                            <td>
                                <select class="inputbox" id="state" name="state" required="">
                                    <option value="">Select State</option>
                                    <option value="AL">AL</option>
                                    <option value="AK">AK</option>
                                    <option value="AZ">AZ</option>
                                    <option value="AR">AR</option>
                                    <option value="CA">CA</option>
                                    <option value="CO">CO</option>
                                    <option value="CT">CT</option>
                                    <option value="DC">DC</option>
                                    <option value="DE">DE</option>
                                    <option value="FL">FL</option>
                                    <option value="GA">GA</option>
                                    <option value="HI">HI</option>
                                    <option value="IL">IL</option>
                                    <option value="IN">IN</option>
                                    <option value="IA">IA</option>
                                    <option value="KS">KS</option>
                                    <option value="KY">KY</option>
                                    <option value="LA">LA</option>
                                    <option value="ME">ME</option>
                                    <option value="MD">MD</option>
                                    <option value="MA">MA</option>
                                    <option value="MI">MI</option>
                                    <option value="MN">MN</option>
                                    <option value="MS">MS</option>
                                    <option value="MO">MO</option>
                                    <option value="MT">MT</option>
                                    <option value="NE">NE</option>
                                    <option value="NV">NV</option>
                                    <option value="NH">NH</option>
                                    <option value="NJ">NJ</option>
                                    <option value="NM">NM</option>
                                    <option value="NY">NY</option>
                                    <option value="NC">NC</option>
                                    <option value="ND">ND</option>
                                    <option value="OH">OH</option>
                                    <option value="OK">OK</option>
                                    <option value="OR">OR</option>
                                    <option value="PA">PA</option>
                                    <option value="RI">RI</option>
                                    <option value="SC">SC</option>
                                    <option value="SD">SD</option>
                                    <option value="TN">TN</option>
                                    <option value="TX">TX</option>
                                    <option value="UT">UT</option>
                                    <option value="VT">VT</option>
                                    <option value="VA">VA</option>
                                    <option value="WA">WA</option>
                                    <option value="WV">WV</option>
                                    <option value="WI">WI</option>
                                    <option value="WY">WY</option>
                                </select>
                            </td>
                        </tr>
                        <tr>
                            <td>Description*</td>
                        </tr>
                    </table>
                    <textarea id="description" name="description" style="width: 100%;"><?php echo $this->job['description']; ?></textarea>
                    <br>
                    <input type="submit" value="Save" style="float: right;">
                </form>
            </div>
            <br>
            <br>
            <br>
        </div>
    </div>
    <div id="bottomShadow"></div>
<script src="jscripts/ckeditor/ckeditor.js" type="text/javascript"></script> 
<script type="text/javascript">
    CKEDITOR.replace('description');
    var state = "<?php echo $this->job['state']; ?> ";
    $('#state option[value='+state+']').attr('selected','selected');
    
    var experience = "<?php echo $this->job['experience']; ?> ";
    $('#experience option[value='+experience+']').attr('selected','selected');
    
    var type = "<?php echo $this->job['type']; ?> ";
    $('#type option[value='+type+']').attr('selected','selected');
    
    var category = "<?php echo $this->job['category']; ?> ";
    $('#category option[value='+category+']').attr('selected','selected');
    
    var status = "<?php echo $this->job['status']; ?> ";
    $('#status option[value='+status+']').attr('selected','selected');
</script>
<?php TemplateUtility::printFooter(); ?>
