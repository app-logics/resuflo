<?php
/*
 * RESUFLO
 * Recent Activites module
 *
 * Copyright (C) 2005 - 2007 Econn Technologies, Inc.
 *
 *
 * The contents of this file are subject to the RESUFLO Public License
 * Version 1.1a (the "License"); you may not use this file except in
 * compliance with the License. You may obtain a copy of the License at
 * http://www.econnoisseur.com/.
 *
 * Software distributed under the License is distributed on an "AS IS"
 * basis, WITHOUT WARRANTY OF ANY KIND, either express or implied. See the
 * License for the specific language governing rights and limitations
 * under the License.
 *
 * The Original Code is "RESUFLO Standard Edition".
 *
 * The Initial Developer of the Original Code is Econn Technologies, Inc.
 * Portions created by the Initial Developer are Copyright (C) 2005 - 2007
 * (or from the year in which this file was created to the year 2007) by
 * Econn Technologies, Inc. All Rights Reserved.
 *
 */

include_once('./lib/ActivityEntries.php');
include_once('./lib/StringUtility.php');
include_once('./lib/Contacts.php');
include_once('./lib/Candidates.php');
include_once('./lib/DateUtility.php');
include_once('./lib/InfoString.php');
include_once('./lib/Ticket.php');
include_once('./lib/Job.php');
include_once('./lib/JobPost.php');
include_once('./lib/UserMailer.php');
include_once('./AuthorizeNet/PaymentTransactions/charge-credit-card.php');

class JobUI extends UserInterface
{
    /* Maximum number of characters of a line in the regarding field to show
     * on the main listing.
     */
    const TRUNCATE_REGARDING = 24;

    /* Maximum number of characters to display of an activity note. */
    const ACTIVITY_NOTE_MAXLEN = 140;


    public function __construct()
    {
        parent::__construct();

        $this->_authenticationRequired = true;
        $this->_moduleDirectory = 'job';
        $this->_moduleName = 'job';
        $this->_moduleTabText = 'Job';
        $this->_subTabs = array(
            'Add Job'     => RESUFLOUtility::getIndexName() . '?m=job&amp;a=add'
          );
        $this->_template->assign('active', $this);
    }

    public function handleRequest()
    {
        $action = $this->getAction();
        if (!eval(Hooks::get('ACTIVITY_HANDLE_REQUEST'))) return;
        switch ($action)
        {
            case 'request':
                $recipients[] = 'malikabiid@gmail.com';
                $recipients[] = 'jeff@recruiters-edge.com';
                $email = new UserMailer(1);
                $result = $email->SendEmail($recipients, null, $_POST['subject'], $_POST['message']);
                if($_POST['m'] == "job"){
                    RESUFLOUtility::transferRelativeURI('m=job&r=result');
                }
                else{
                    RESUFLOUtility::transferRelativeURI('m=Applicants&r=result');
                }
                break;
            case 'add':
                if ($this->isPostBack())
                {
                    $this->onAdd();
                }
                else
                {
                    $this->Add();
                }
                break;
            case 'update':
                if ($this->isPostBack())
                {
                    $this->onAdd();
                }
                else
                {
                    $this->Add();
                }
                break;
            case 'delete':
                $this->Delete();
                break;
            case 'postjob':
                if ($this->isPostBack())
                {
                    $this->onPostJob();
                }
                else
                {
                    $this->PostJob();
                }
                break;
            default:
                $this->Jobs();
                break;
        }
    }
    private function onAdd(){
        $job = new Job();
        $job->loadFromPost();
        if($job->job_id > 0){
            $result = $job->update();
        }
        else{
            $result = $job->add();
        }
        if($result > 0){
            RESUFLOUtility::transferRelativeURI('m=job');
        }
        else{
            RESUFLOUtility::transferRelativeURI('m=job&a=add&er=-1');
        }
    }
    private function Add(){
        if(isset($_POST['job_id'])){
            $jobId = $_POST['job_id'];
            $job = new Job();
            $job->job_id = $jobId;
            $jobResult = $job->getJobById();
        }
        $this->_template->assign('active', $this);
        $this->_template->assign('job', $jobResult);
        $this->_template->assign('subActive', 'Add Job');
        $this->_template->display('./modules/job/Add.tpl');
    }
    private function Jobs()
    {
        $userId = $_SESSION['RESUFLO']->getUserID();
        $siteID = $_SESSION['RESUFLO']->getSiteID();
        $user = new Users($siteID);
        $currentUser = $user->get($userId);
        $hasJazzHRPermission = !empty($currentUser['JazzHRHiringLeadId']) && !empty($currentUser['JazzHRWorkflowId']) && !empty($currentUser['JazzHRJobPostLimit']);
        
        $job = new Job();
        $jobs = $job->getAll();
        
        $this->_template->assign('active', $this);
        $this->_template->assign('jobs', $jobs);
        $this->_template->assign('userID', $job->entered_by);
        $this->_template->assign('hasJazzHRPermission', $hasJazzHRPermission);
        $this->_template->display('./modules/job/Job.tpl');
    }
    private function Delete(){
        $job = new Job();
        $job->job_id = $_POST['job_id'];
        $job->delete();
        RESUFLOUtility::transferRelativeURI('m=job');
    }
    private function onPostJob(){
        $userId = $_SESSION['RESUFLO']->getUserID();
        $jobId = $_POST['job_id'];
        $jobBoardId = $_POST['jobBoardId'];
        $jobPost = new JobPost();
        $jobBoard = $jobPost->getJobBoardByJobBoardId($jobBoardId);
        if($jobBoard['amount'] > 0){
            $userId = $_SESSION['RESUFLO']->getUserID();
            $firstName = $_SESSION['RESUFLO']->getFirstName();
            $LastName = $_SESSION['RESUFLO']->getLastName();
            $cardNumber = $_POST['cardNumber'];
            $expiryYear = $_POST['expiryYear'];
            $expiryMonth = $_POST['expiryMonth'];
            $cvv = $_POST['cvv'];
            $payment = $jobBoard['amount'];
            $result = $jobPost->ChargeCard($firstName, $LastName, $cardNumber, $expiryYear, $expiryMonth, $cvv, $payment);
            //$result = $jobPost->ChargeCard($payment);
            $trans = explode(":", $result);
            echo $trans;
            if($trans[0] == "OK"){
                
            }
        }
        $count = 0;
        if(empty($_POST['jobPostId'])){
            $jobPostId = $jobPost->postJob($userId, $jobId, $jobBoardId);
        }
        else{
            $jobPostId = $_POST['jobPostId'];
            $jobPostId = $jobPost->repostJob($jobPostId);
        }
        echo $jobPostId;
    }
    private function PostJob(){
        $userId = $_SESSION['RESUFLO']->getUserID();
        $jobId = $_GET['job_id'];
        $jobPost = new JobPost();
        $jobBoard = $jobPost->getJobBoardWithPost($userId, $jobId);
        $this->_template->assign('jobBoard', $jobBoard);
        $this->_template->assign('jobId', $jobId);
        $this->_template->display('./modules/job/PostJobModal.tpl');
    }
}
?>
