<?php /* $Id: CreateAttachmentModal.tpl 3093 2007-09-24 21:09:45Z brian $ */ ?>
<?php TemplateUtility::printModalHeader('Jobs', array('modules/companies/validator.js'), 'Post Job'); ?>
<style>
    .hide{
        display: none;
    }
</style>

<table>
    <?php foreach ($this->jobBoard as $jobBoards => $row){?>
        <tr>
            <td>
                <input type="hidden" name="jobPostId" value="<?php echo $row['jobPostId']; ?>" />
                <input type="checkbox" class="jobBoardId" name="jobBoardId" class="jobBoardId" value="<?php echo $row['jobBoardId']; ?>" amount="<?php if($row['amount'] > 0){ echo '$'.$row['amount'];} ?>" /> 
            </td>
            <td>
                <img class="load hide" src="images/ajax-loader.gif" style="width: 15px;">
                <img class="check hide" src="images/blue_check.gif" style="width: 15px;">
                <img class="cross hide" src="images/cross.gif" style="width: 15px;">   
            </td>
            <td><?php echo $row['title']; ?></td>
            <td style="color: red;padding-left: 10px;"><?php if($row['amount'] > 0){ echo '$'.$row['amount'];} ?></td>
            <td style="padding-left: 10px;"><?php if($row['PostDate'] != null){ echo 'Posted On <font style="color: blue;">'.date('m-d-Y', strtotime($row['PostDate'])).'</font>'; } ?></td>
        </tr>
    <?php }?>
</table>
<br>
<table id="Payment" class="hide" style="border: 1px solid #d6d6d6; margin-left: 7px;">
    <tbody>
        <tr>
            <td colspan="2">
                <div id="DialogError" style="color:red;font-weight:bold;margin-top:3px;margin-bottom:3px;"></div>
            </td>
        </tr>
        <tr>
            <td><b>Card Type</b><span class="red_txt">*</span>:</td>
            <td>
                <select name="cardType" id="cardType" style="width: 85px;" tabindex="1">
                    <option value="Visa">Visa</option>
                    <option value="Master">Master</option>
                    <option value="Visa">Discover</option>
                    <option value="Master">Amex</option>
                </select>
            </td>
        </tr>
        <tr>
            <td><b>Credit Card No</b><span class="red_txt">*</span>:</td>
            <td><input name="cardNumber" id="cardNumber" maxlength="16" tabindex="2" type="text"></td>
        </tr>
        <tr>
            <td><b>Cvv</b>:</td>
            <td><input name="cvv" id="cvv" tabindex="3" maxlength="3" style="width:75px;" type="password"></td>
        </tr>
        <tr>
            <td><b>Expiry Date</b><span class="red_txt">*</span>:</td>
            <td>
                <select name="expiryMonth" id="expiryMonth" tabindex="4">
                    <option value="01">Jan</option>
                    <option value="02">Feb</option>
                    <option value="03">Mar</option>
                    <option value="04">Apr</option>
                    <option value="05">May</option>
                    <option value="06">Jun</option>
                    <option value="07">Jul</option>
                    <option value="08">Aug</option>
                    <option value="09">Sep</option>
                    <option value="10">Oct</option>
                    <option value="11">Nov</option>
                    <option value="12">Dec</option>
                </select>
                <select name="expiryYear" id="expiryYear" tabindex="5">
                        <option value="2017">2017</option>
                        <option value="2018">2018</option>
                        <option value="2019">2019</option>
                        <option value="2020">2020</option>
                        <option value="2021">2021</option>
                        <option value="2022">2022</option>
                        <option value="2023">2023</option>
                        <option value="2024">2024</option>
                        <option value="2025">2025</option>
                        <option value="2026">2026</option>
                        <option value="2027">2027</option>
                        <option value="2028">2028</option>
                        <option value="2029">2029</option>
                        <option value="2030">2030</option>
                        <option value="2031">2031</option>
                        <option value="2032">2032</option>
                        <option value="2033">2033</option>
                        <option value="2034">2034</option>
                        <option value="2035">2035</option>
                        <option value="2036">2036</option>
                        <option value="2037">2037</option>
                </select>
            </td>
        </tr>
    </tbody>
</table>
<br><br>
<div style="margin-left: 8px;">
    <input type="submit" class="button" name="submit" id="submit" value="Post Job" />&nbsp;
    <input type="button" class="button" name="cancel" value="Cancel" onclick="parentHidePopWin();" />
</div>
<script type="text/javascript">
    $(document).ready(function(){
        $("#cardNumber").val('');
        $("#cvv").val('');
    });
    $('#PaymentSave').click(function(event) {
        event.preventDefault();
        $('#PaymentSave').html("Wait...");
        var href = "<?php echo RESUFLOUtility::getIndexName(); ?>?m=joborders&a=jobPost&jobOrderID=<?php echo($this->jobOrderID); ?>";
        href = href + "&JobBoardId=" + $(".JobBoardId").val() + "&payment=" + $("#payment").val() + "&cardType=" + $("#cardType").val() + "&cardNumber=" + $("#cardNumber").val()+ "&expiryMonth=" + $("#expiryMonth").val()+ "&expiryYear=" + $("#expiryYear").val();
        $.ajax(href, {
            success: function(data) {
                $('#DialogError').html(data);
                $('#PaymentSave').html("Make Payment");
            },
            error: function() {
                $('#DialogError').html('An error occurred');
                $('#PaymentSave').html("Make Payment");
            }
        });
    });
    $('.jobBoardId').click(function(e){
        var paidJobBoardCount = $('.jobBoardId:checked[amount!=""]').length;
        if(paidJobBoardCount > 0){
            $("#Payment").removeClass('hide');
        }
        else{
            $("#Payment").addClass('hide');
        }
    });
    $("#submit").click(function(event){
        if($(".jobBoardId:checked").length == 0){
            alert("Select at lest one job board to post");
            return false;
        }
        $(".load").addClass('hide');
        $(".check").addClass('hide');
        $(".cross").addClass('hide');
        $('.jobBoardId:checked').each(function(e){
            $(this).parent().next().find(".load").removeClass('hide');
        });
        $('.jobBoardId:checked').each(function(e){
            var pointer = this;
            var href = "<?php echo(RESUFLOUtility::getIndexName()); ?>?m=job&a=postjob";
            //href = href + "&JobBoardId=" + $(".JobBoardId").val() + "&payment=" + $("#payment").val() + "&cardType=" + $("#cardType").val() + "&cardNumber=" + $("#cardNumber").val()+ "&expiryMonth=" + $("#expiryMonth").val()+ "&expiryYear=" + $("#expiryYear").val();
            $.ajax({
                type: "POST",
                url: href,
                data: {postback:'1', job_id: <?php echo($this->jobId); ?>, jobPostId: $(pointer).parent().find("input[name='jobPostId']").val(), jobBoardId: $(pointer).val()},
                success: function(jobPostId){
                    if(jobPostId > 0){
                        $(pointer).parent().next().find(".load").addClass('hide');
                        $(pointer).parent().next().find(".check").removeClass('hide');
                    }
                    else{
                        $(pointer).parent().next().find(".load").addClass('hide');
                        $(pointer).parent().next().find(".cross").removeClass('hide');
                    }
                },
                error: function() {
                    $(pointer).parent().next().find(".load").addClass('hide');
                    $(pointer).parent().next().find(".cross").removeClass('hide');
                }
            });
        });
        return false;
    });
</script>