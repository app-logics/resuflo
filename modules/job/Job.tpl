<?php /* $Id: Search.tpl 1948 2007-02-23 09:49:27Z will $ */ ?>
<?php TemplateUtility::printHeader('Jobs', array('js/highlightrows.js', 'modules/activity/validator.js', 'js/sweetTitles.js')); ?>
<?php TemplateUtility::printHeaderBlock(); ?>
<?php TemplateUtility::printTabs($this->active); ?>
<style>
    #jobs table{
        border-collapse: collapse;
    }
    #jobs table tr:nth-child(even) {
        background: #FFF
    }
    #jobs table tr:nth-child(odd) {
        background: #ececec;
    }
    #jobs table td{
        padding: 10px 5px;
    }
    .hide{
        display: none;
    }
</style>
    <div id="main">
        <div id="contents">
            <br>
            <div style="width: 100%;">
                <?php if($this->hasJazzHRPermission === true){ ?>
                    <div id="jobs">
                        <table style="width: 100%;">
                        <?php foreach ($this->jobs as $jobs => $row){?>
                            <tr>
                                <td>
                                    <strong><?php echo $row['title']; ?></strong>
                                    <br>
                                    <?php echo $row['city']; ?>, <?php echo $row['state']; ?>, <?php echo $row['country']; ?>
                                </td>
                                <td>
                                    <?php if($row['status'] == 1){ echo 'Active'; }elseif($row['status'] == 2){ echo 'Drafting'; }elseif($row['status'] == 3){ echo 'On Hold'; }elseif($row['status'] == 6){ echo 'Closed'; } ?>
                                    <br>
                                    <?php echo 'Added on '.date('m-d-Y', strtotime($row['date_created'])); ?>
                                </td>
                                <td style="text-align: right;">
                                    <!--<input type="button" value="Post Job" class="buttonCalendar" onclick="showPopWin('<?php echo(RESUFLOUtility::getIndexName()); ?>?m=job&amp;a=postjob&amp;job_id=<?php echo $row['job_id']; ?>', 400, 300, null); return false;" > -->
                                    <form action="<?php echo(RESUFLOUtility::getIndexName()); ?>?m=job&amp;a=add" method="post" style="display: inline">
                                        <input type="hidden" id="job_id" name="job_id" value="<?php echo $row['job_id']; ?>" />
                                        <button class="buttonCalendar" ><img src="images/actions/edit.gif" width="16" height="16" class="absmiddle" alt="Update" border="0"> Update</button>
                                    </form>
                                    <form action="<?php echo(RESUFLOUtility::getIndexName()); ?>?m=job&amp;a=delete" method="post" style="display: inline">
                                        <input type="hidden" id="job_id" name="job_id" value="<?php echo $row['job_id']; ?>" />
                                        <button class="buttonCalendar" onclick="return confirm('Are you sure want to delete this job?');"><img src="images/actions/delete.gif" width="16" height="16" class="absmiddle" alt="delete" border="0"> Remove</button>
                                    </form>
                                </td>
                            </tr>
                        <?php }?>
                        </table>
                    </div>
                <?php } else{?>
                    <script type="text/javascript">
                        $("#secondary").find("li").eq(0).remove();
                    </script>
                    <form action="index.php?m=job&a=request" method="post">
                        <input type="hidden" name="m" id="m" value="job" />
                        <table>
                            <?php if($_GET["r"] == "result"){ ?>
                                <tr>
                                    <td></td>
                                    <td><h3 style="color: #0094d6">Job posting request sent please wait for response</h3></td>
                                </tr>
                            <?php }?>
                            <tr>
                                <td>Subject</td>
                                <td><input type="text" id="subject" name="subject" style="width: 300px;" required/></td>
                            </tr>
                            <tr>
                                <td style="vertical-align: top;">Message</td>
                                <td><textarea id="message" name="message" rows="20" cols="80" required></textarea></td>
                            </tr>
                            <tr>
                                <td></td>
                                <td><button type="submit">Request Job Postings</button></td>
                            </tr>
                        </table>
                    </form>
                <?php }?>
            </div>
            <br>
            <br>
            <br>
        </div>
    </div>
    <div id="bottomShadow"></div>
<?php TemplateUtility::printFooter(); ?>
