<?php
/*
 * RESUFLO
 * XML module
 *
 * Copyright (C) 2005 - 2007 Econn Technologies, Inc.
 *
 *
 * The contents of this file are subject to the RESUFLO Public License
 * Version 1.1a (the "License"); you may not use this file except in
 * compliance with the License. You may obtain a copy of the License at
 * http://www.econnoisseur.com/.
 *
 * Software distributed under the License is distributed on an "AS IS"
 * basis, WITHOUT WARRANTY OF ANY KIND, either express or implied. See the
 * License for the specific language governing rights and limitations
 * under the License.
 *
 * The Original Code is "RESUFLO Standard Edition".
 *
 * The Initial Developer of the Original Code is Econn Technologies, Inc.
 * Portions created by the Initial Developer are Copyright (C) 2005 - 2007
 * (or from the year in which this file was created to the year 2007) by
 * Econn Technologies, Inc. All Rights Reserved.
 *
 *
 * This module builds an XML file containing public job postings. The
 * exported XML data can be used to submit, en masse, all public job
 * postings to job bulletin sites such as Indeed.com.
 *
 *
 * $Id: XmlUI.php 3565 2007-11-12 09:09:22Z will $
 */
include_once('./lib/dripmarketting.php');
include_once('./lib/ActivityEntries.php');
include_once('./lib/StringUtility.php');
include_once('./lib/DateUtility.php');
include_once('./lib/JobOrders.php');
include_once('./lib/Site.php');
include_once('./lib/XmlJobExport.php');
include_once('./lib/HttpLogger.php');
include_once('./lib/CareerPortal.php');
include_once('./asset/constants.php');
include_once('./lib/Importoptout.php');


class dripMarketUI extends UserInterface
{
     public function __construct()
    {
        parent::__construct();

        $this->_authenticationRequired = true;
        $this->_moduleDirectory = 'DripMarket';
        $this->_moduleName = 'dripMarket';
        $this->_moduleTabText = 'DripMarket';

    }
    
  function getdata()
    {    $this->_template->assign('active', $this);
        $this->_template->display('./modules/dripMarket/dripMarket.tpl'); 
          
    }
    function handleRequest()
    {
        $action = $this->getAction();
        
$userid=$_SESSION['RESUFLO']->getUserID();
    
 switch ($action)
        {
            case 'showconfig':
                $this->showconfigpage();
                break;
           	case 'saveconfig':
                $this->saveconfigpage();
                break;
         	case 'showcompaign':
                $this->showcompaign();
                break;
            case 'compaignemailedit':
                $this->compaignemailedit();
                break;
            case 'deletecompaign':
                $this->deletecompaign();
                break;
            case 'createcampaign':
                $this->createcampaign();
                break;
            case 'compaigndetail':
                $this->compaigndetail();
                break;
            case 'savecampaign':
                $this->savecampaign();
                break;
            case 'saveemail':
                $this->saveemail();
                break;
            case 'questionnaire':
                $this->questionaire();
                break;
            case 'editcompaign':
                $this->editcompaign();
                break;
            case 'createemail':
                $this->createemail();
                break;
            case 'savequestionnaire':
                $this->savequestionaire();
                break;
            case 'editques':
                $this->editques();
                break;
            case 'delques':
                $this->delques();
                break;
            case 'savequestions':
                $this->savequestions();
                break;
            case 'compaignemaildelete':
                $this->compaignemaildelete();
                break;
       		default:  $this->_template->assign('active', $this);
				$this->showcompaign();
                break;
        }
        
        
    }
    
    
     function editques()
    {
        $userid=$_SESSION['RESUFLO']->getUserID();
        if(isset($_POST['question']))
        {
           $question=$_POST['question']; 
        }
     if(isset($_POST['questionaireid']))
        {
        $questionaireid=$_POST['questionaireid'];

       
        }
         if(isset($_GET['questionaireid']))
        {
        $questionaireid=$_GET['questionaireid'];

       
        }
          if(isset($_GET['id']))
        {
  
      $quesid=$_GET['id'];
       
        }
               
     $sql ="SELECT dripmarket_questable.quesname,id FROM `dripmarket_questable` where id=$quesid";
      // $this->_db->makeQueryInteger($activityID),
    $resource= mysql_query($sql);
    while($row = mysql_fetch_array($resource))
		 {
                    $quesname=$row['quesname'];
                 }
                 $sql ="SELECT dripmarket_questable.quesname,id FROM `dripmarket_questable` where questionaireid=$questionaireid";
           // $this->_db->makeQueryInteger($activityID),

    $resource= mysql_query($sql);
$campaignarray=array();
 $i=0;
 while($row = mysql_fetch_array($resource))
		{
                    $campaignarray[$i]=$row;
                   $i++;
                }
                  $this->_template->assign('baseurl',BASEURL);    
                    $this->_template->assign('taskaction', 'edit');
                $this->_template->assign('task', 'edit');
                       $this->_template->assign('quesid',$quesid);  
                     $this->_template->assign('quesname', $quesname);
                  $this->_template->assign('maxid', $questionaireid);
  $this->_template->assign('compaigndata', $campaignarray);
$this->_template->assign('active', $this);
        $this->_template->display('./modules/dripMarket/questions.tpl');
        
    }
function delques()
{
        $userid=$_SESSION['RESUFLO']->getUserID();
         if(isset($_POST['questionaireid'])){
        $questionaireid=$_POST['questionaireid'];
           }
         if(isset($_GET['questionaireid']))
        {
        $questionaireid=$_GET['questionaireid'];
   }
          if(isset($_GET['id']))
        {
  
      $quesid=$_GET['id'];
       
        }
               
     $sql ="delete from dripmarket_questable where id=$quesid";
      // $this->_db->makeQueryInteger($activityID),
    $resource= mysql_query($sql);

$sql ="SELECT dripmarket_questable.quesname,id FROM `dripmarket_questable` where questionaireid=$questionaireid";
           // $this->_db->makeQueryInteger($activityID),

    $resource= mysql_query($sql);
$campaignarray=array();
 $i=0;
 while($row = mysql_fetch_array($resource))
		{
                    $campaignarray[$i]=$row;
                   $i++;
                }
                $quesname='';
                    $this->_template->assign('baseurl',BASEURL); 
                $this->_template->assign('task', 'save');
                       $this->_template->assign('quesid',$quesid);  
                     $this->_template->assign('quesname', $quesname);
                  $this->_template->assign('maxid', $questionaireid);
  $this->_template->assign('compaigndata', $campaignarray);
$this->_template->assign('active', $this);
        $this->_template->display('./modules/dripMarket/questions.tpl');
        
    }
    
 function savequestionaire()
    {
        
               $userid=$_SESSION['RESUFLO']->getUserID();
               
  
               
  if(isset($_POST['txtmessage']))
        {
              $txtmessage=$_POST['txtmessage'];
        }
                      if(isset($_POST['emailaddress']))
        {
                  $emailaddress=$_POST['emailaddress'];
        }
                      if(isset($_POST['selectstatus']))
        {

              $selectstatus=$_POST['selectstatus'];
        }
        
        
        
        
       $userid=$_SESSION['RESUFLO']->getUserID();


  $sql ="SELECT  dripmarket_questionaire.message ,dripmarket_questionaire.id , dripmarket_questionaire.statuscompletion  ,  dripmarket_questionaire.noofquestion  ,  dripmarket_questionaire.sendemailalertto    FROM `dripmarket_questionaire` where `dripmarket_questionaire`.userid=$userid";

   
           // $this->_db->makeQueryInteger($activityID),
    $resource= mysql_query($sql);
$resourcedetail=mysql_fetch_assoc($resource);
    if(mysql_num_rows($resource)>0)
    { $selectstatus='0';
                           $sql ="update `dripmarket_questionaire` set message='$txtmessage' , statuscompletion='$selectstatus' ,sendemailalertto='$emailaddress' where userid ='$userid'";
     // $this->_db->makeQueryInteger($activityID),
    $resource= mysql_query($sql);
        $ids=$resourcedetail['id'];

    }else{
        $selectstatus='0';
  $sql ="insert into `dripmarket_questionaire`(message,statuscompletion,  sendemailalertto,userid)values('$txtmessage','$selectstatus','$emailaddress','$userid')";
      // $this->_db->makeQueryInteger($activityID),
    $resource= mysql_query($sql);
      $sql ="select max(id)as ids from `dripmarket_questionaire`";
   
           // $this->_db->makeQueryInteger($activityID),
    $resource= mysql_query($sql);
             while($row = mysql_fetch_array($resource))
		{
              $ids=$row['ids'];
     }
        }
      
  
     $sql ="SELECT dripmarket_questable.quesname,id FROM `dripmarket_questable` where questionaireid=$ids";
           // $this->_db->makeQueryInteger($activityID),
    $resource= mysql_query($sql);
$campaignarray=array();
 $i=0;
 while($row = mysql_fetch_array($resource))
		{
                    
                   $campaignarray[$i]=$row;
                   $i++;
                }
                
            $this->_template->assign('baseurl',BASEURL);        
           $this->_template->assign('quesname','');      
    $this->_template->assign('compaigndata',$campaignarray);
            $this->_template->assign('task', 'save');
$this->_template->assign('maxid', $ids);
    $this->_template->assign('active', $this);
        $this->_template->display('./modules/dripMarket/questions.tpl');
             
    }
    
     function savequestions()
    {

        if($_POST['task']=="proceed"||isset($_POST['gethtml']))
         {
       
                   if(isset($_POST['questionaireid']))
        {
            $questionaireid=$_POST['questionaireid'];
         //   $questionaireid=base64_encode($questionaireid); 
        }
         if(isset($_POST['facebookurl']))
         {
            $facebookurl=$_POST['facebookurl'];
            
         } $userid=$_SESSION['RESUFLO']->getUserID();
  $sql ="SELECT `candidate`.candidate_id, candidate.email1, candidate.first_name, candidate.phone_home, candidate.date_created FROM `candidate` where entered_by=$userid";
        
                $resourcedetail= mysql_query($sql);
                  $sql ="SELECT `dripmarket_questionaire`.message FROM `dripmarket_questionaire` where dripmarket_questionaire.id=$questionaireid";
        
                $resourcedetails= mysql_query($sql);
                 $resourcedetailsresult= mysql_fetch_assoc($resourcedetails);
                       $bodytext= $resourcedetailsresult['message'];
      
 
             
             
               $sql ="SELECT `dripmarket_questable`.questionaireid ,`dripmarket_questionaire`.sendemailalertto,dripmarket_questionaire.message ,dripmarket_questionaire.statuscompletion,dripmarket_questionaire.message  ,user.logo,dripmarket_questable.quesname ,user.logo,dripmarket_questable.id as quesid  FROM `dripmarket_questionaire` right join `dripmarket_questable` on `dripmarket_questable`.questionaireid=`dripmarket_questionaire`.id  inner join `user` on `user`.user_id=`dripmarket_questionaire`.userid where `dripmarket_questable`.questionaireid=$questionaireid";
 $resourcedetail= mysql_query($sql);
 $htmldata='<style>
	.txtbox_big{ border:#8cbf11 1px solid; background-color:#FFFFFF; width:850px; height:90px;}
	.txtbox_small{ border:#8cbf11 1px solid; background-color:#FFFFFF; width:250px; height:20px;}
	.txtform{font-family:Arial, Helvetica, sans-serif; font-size:12px; color:#333333;}
</style>';







    
        $i=1;
        $k=1;
          while($row = mysql_fetch_array($resourcedetail))
		{
                    $htmldata=$htmldata.'<div class="divmainwidth">';
                    if($k==1)
                    {
  $htmldata= $htmldata.'<form name="frm1" id="frm1" action="'.BASEURL.'que/quesothersub.php" method="post" ><table width="900" border="0" align="center" cellpadding="2" cellspacing="2"><tr><td bgcolor="#f8f8f8"><img src="'.BASEURL.$row['logo'].'" width="315" height="59" /></td>
  </tr>  <tr>    <td bgcolor="#f8f8f8" class="txtform">Indicate the Required Field <span style="color:#FF0000">*</span> </td>  </tr>  <tr>    <td bgcolor="#f8f8f8"><table width="90%" border="0" align="center" cellpadding="0" cellspacing="0"><tr><td width="6%" class="txtform">Name <span style="color:#FF0000">*</span></td><td width="40%"><label><input name="cname" type="text" id="cname" class="txtbox_small" /></label></td><td width="6%" class="txtform">Email <span style="color:#FF0000">*</span></td><td width="48%"><input name="emailaddress" id="emailaddress" type="text" class="txtbox_small" /></td></tr></table></td></tr>';
               $k++;
                    }
  $htmldata=$htmldata.' <tr>
    <td bgcolor="#f8f8f8" class="txtform">'.$row['quesname'].'</td>  </tr>
    
    <tr>
    <td bgcolor="#f8f8f8" align="center"><input name="txtans'.$i.'" id="txtans'.$i.'"  type="text" class="txtbox_big" />
  <input type="hidden" name="quesid'.$i.'" value="'.$row['quesid'].'" /> </td></tr><input type="hidden" name="sendemailalertto" value="'.$row['sendemailalertto'].'" /><input type="hidden" name="questionaireid" value="'.$row['questionaireid'].'" /><input type="hidden" name="scomplete" value="'.$row['statuscompletion'].'" /></td>
  </tr>';
                   $i++;
                   }
                   $no=$i-1;
          $htmldata=$htmldata.'
          
           <tr>
    <td bgcolor="#f8f8f8" align="center"><input type="hidden" name="noofques" value="'.$no.'" />
              <input type="hidden" name="cid" value="" />
      <input name="sub1" type="image" src="'.BASEURL.'images/save.gif" value="submit" width="105" height="43" /></td>
  </tr></table></form>';
    
     
             
             $this->_template->assign('htmldata', $htmldata);
             
             $this->_template->assign('active', $this);
   if(isset($_POST['gethtml']))
   {
      $this->_template->display('./modules/dripMarket/gethtml.tpl');

   }
   else{
  $this->_template->display('./modules/dripMarket/proceeded.tpl');
   }
   
      
    }
		else     
        {
			$userid=$_SESSION['RESUFLO']->getUserID();
			     
     if(isset($_POST['question']))
        {

            $question=$_POST['question'];
        }
       if(isset($_POST['questionaireid']))
        {

        
        $questionaireid=$_POST['questionaireid'];
        }
              if(isset($_POST['taskaction']))
        {
        $taskaction=$_POST['taskaction'];
        }
                  if(isset($_POST['id']))
        {
  
      $quesid=$_POST['id'];
       
        }

         if($taskaction=='edit')
         {
                 $sql ="update `dripmarket_questable` set quesname ='$question',questionaireid='$questionaireid' where id=$quesid";
         }
         else
         {
			 $sql ="insert into `dripmarket_questable`(quesname ,questionaireid)values('$question','$questionaireid')"; 
		}
		// $this->_db->makeQueryInteger($activityID),
		$resource= mysql_query($sql);
		
		header("location:index.php?m=dripMarket&a=savequestionnaire");
    
//header("location:index.php?m=dripMarket&a=savequestions");

$sql ="SELECT dripmarket_questable.quesname,id FROM `dripmarket_questable` where questionaireid=$questionaireid";
           // $this->_db->makeQueryInteger($activityID),
 $resource= mysql_query($sql);
$campaignarray=array();
 $i=0;
 while($row = mysql_fetch_array($resource))
		{
                    $campaignarray[$i]=$row;
                   $i++;
                }
                   $this->_template->assign('baseurl',BASEURL);  
                  $this->_template->assign('maxid', $questionaireid);
  $this->_template->assign('compaigndata', $campaignarray);
$this->_template->assign('active', $this);
         $this->_template->assign('taskaction','save');
           $this->_template->assign('quesname',''); 
        $this->_template->display('./modules/dripMarket/questions.tpl');
    }
             
    }
    function compaignemailedit()
    {
        if(isset($_GET['editid']))
        {
              $editid=$_GET['editid'];
        }
                if(isset($_GET['id']))
        {
              $cid=$_GET['id'];
        }
      
                     if(isset($_POST['id']))
        {
              $cid=$_POST['id'];
        }
        
            $sql ="SELECT `dripmarket_compaignemail`.sendemailon,`dripmarket_compaignemail`.nameofemail,`dripmarket_compaignemail`.subject,`dripmarket_compaignemail`.creationdate,`dripmarket_compaignemail`.hour,`dripmarket_compaignemail`.min,`dripmarket_compaignemail`.timezone,`dripmarket_compaignemail`.message,`dripmarket_compaignemail`.compaignid,`dripmarket_compaign`.cstatus,`candidate_joborder_status`.short_description FROM `dripmarket_compaign`
INNER JOIN `dripmarket_compaignemail` ON `dripmarket_compaign`.id = `dripmarket_compaignemail`.compaignid left join candidate_joborder_status on
`candidate_joborder_status`.candidate_joborder_status_id= `dripmarket_compaign`.cstatus 
 where `dripmarket_compaignemail`.id=$editid
";

           // $this->_db->makeQueryInteger($activityID),
    $resource= mysql_query($sql);
        
   $resourcedetails= mysql_fetch_assoc($resource);
   
   $mess= stripslashes($resourcedetails['message']);

        $this->_template->assign('resourcedetails', $resourcedetails);
                  $this->_template->assign('task', 'edit');
                           $this->_template->assign('editid', $editid);
          $this->_template->assign('cid', $cid);
        $this->_template->assign('active', $this);
        $this->_template->display('./modules/dripMarket/createemail.tpl');        
        
        
        
    }
    
    function saveconfigpage()
    {
        
        
            $userid=$_SESSION['RESUFLO']->getUserID();
                       if(isset($_POST['timezone']))
        {
    $timezone=$_POST['timezone'];
        }
                              if(isset($_POST['smtphost']))
        {
        $smtphost=$_POST['smtphost'];
        }
                              if(isset($_POST['smtpuser']))
        {
     $smtpuser=$_POST['smtpuser'];
        }
                                      if(isset($_POST['smtppass']))
        {
     $smtppass=$_POST['smtppass'];
        }
                                      if(isset($_POST['smtpport']))
        {
     $smtpport=$_POST['smtpport'];
        }
                                      if(isset($_POST['fromname']))
        {
     $fromname=$_POST['fromname'];
        }
                                      if(isset($_POST['fromemail']))
        {
     $fromemail=$_POST['fromemail'];
        }

       $userid=$_SESSION['RESUFLO']->getUserID();
    $sql ="SELECT  dripmarket_configuration.smtphost  ,  dripmarket_configuration.smtppassword ,  dripmarket_configuration.smtpport  ,  dripmarket_configuration.fromname  ,  dripmarket_configuration.fromemail  , dripmarket_configuration.bccemail,  dripmarket_configuration.smtpuser  FROM `dripmarket_configuration` where `dripmarket_configuration`.userid=$userid";
           // $this->_db->makeQueryInteger($activityID),
    $resource= mysql_query($sql);
    $resourcedetailsresult= mysql_fetch_assoc($resource);

    if(mysql_num_rows($resource)>0)
    {
                           $sql ="update `dripmarket_configuration` set smtphost='$smtphost' , smtpuser='$smtpuser' ,smtppassword='$smtppass',smtpport='$smtpport', fromname='$fromname' ,fromemail='$fromemail', bccemail='$bccemail', where userid ='$userid'";
     // $this->_db->makeQueryInteger($activityID),
    $resource= mysql_query($sql);
        

    }
    else
    {
                    $sql ="insert into `dripmarket_configuration`(smtphost , smtpuser ,smtppassword,smtpport, fromname ,fromemail,bccemail,userid )values('$smtphost','$smtpuser','$smtppass','$smtpport','$fromname','$fromemail','$bccemail','$userid')";
     // $this->_db->makeQueryInteger($activityID),
    $resource= mysql_query($sql);
        
        
    }
    $this->_template->assign('active', $this);

  $this->_template->display('./modules/dripMarket/configsubmit.tpl');
             
    }
        function createemail()
    {
               if(isset($_GET['cid']))
        {
 $cid=$_GET['cid'];
        }
               if(isset($_GET['s']))
        {
   $status=$_GET['s'];
        }
     
     
            $sql ="SELECT `candidate_joborder_status`.candidate_joborder_status_id   	  ,`candidate_joborder_status`.short_description    FROM `candidate_joborder_status` inner join `dripmarket_compaign` on `dripmarket_compaign`.cstatus=`candidate_joborder_status`.candidate_joborder_status_id     where dripmarket_compaign.id=$cid";
                $resourcedetail= mysql_query($sql);
                   $this->_template->assign('editid', '');
   $resourcedetails= mysql_fetch_assoc($resourcedetail);
        $this->_template->assign('resourcedetails', $resourcedetails);
         $this->_template->assign('cid', $cid);
            $this->_template->assign('status', $status);
              $this->_template->assign('task', 'save');
        $this->_template->assign('active', $this);
        $this->_template->display('./modules/dripMarket/createemail.tpl');
            
    }
    
 function questionaire()
    {
     
     $userid=$_SESSION['RESUFLO']->getUserID();
     
  

    $sql ="SELECT  dripmarket_questionaire.message ,user.logo,  dripmarket_questionaire.statuscompletion  ,  dripmarket_questionaire.noofquestion  ,  dripmarket_questionaire.sendemailalertto    FROM `dripmarket_questionaire` right join user on user.user_id=`dripmarket_questionaire`.userid  where `dripmarket_questionaire`.userid=$userid";


           // $this->_db->makeQueryInteger($activityID),
    $resource= mysql_query($sql);
    $resourcedetailsresult= mysql_fetch_assoc($resource);
  
  
  
      $this->_template->assign('resourcedetailsresult', $resourcedetailsresult);                
                

           // $this->_db->makeQueryInteger($activityID),



        $this->_template->assign('statusarray', $statusarray);
        $this->_template->assign('logovalue', $logovalue);
        $this->_template->assign('active', $this);
        $this->_template->display('./modules/dripMarket/questionaire.tpl');
            
    }
    
    
        function createcampaign()
    {
      $this->_template->assign('cname','');
 $this->_template->assign('cid','');
  $this->_template->assign('description', '');
 $this->_template->assign('cstatus','');
  $this->_template->assign('task','add');

       $this->_template->assign('editid', '');
       
          	  
                    $sql ="SELECT `candidate_joborder_status`.candidate_joborder_status_id   	  ,`candidate_joborder_status`.short_description    FROM `candidate_joborder_status`
 ";    $resourcedetail= mysql_query($sql);

 $statusarray=array();
 $i=0;
 while($row = mysql_fetch_array($resourcedetail))
		{
                    
                   $statusarray[$i]=$row;
                   $i++;
                }

           // $this->_db->makeQueryInteger($activityID),


        $this->_template->assign('cstatus','');
               $this->_template->assign('statusarray', $statusarray);
        $this->_template->assign('active', $this);
       $this->_template->display('./modules/dripMarket/createcampaign.tpl');
            
    }
function savecampaign()
    {
        $task='';
      $userid=$_SESSION['RESUFLO']->getUserID();
      if(isset($_POST['cname']))
        {
            
    $cname=$_POST['cname'];
        }
                               if(isset($_POST['editid']))
        {
    $editid=$_POST['editid'];
        }
                              if(isset($_POST['txtcampaign']))
        {
        $txtcampaign=$_POST['txtcampaign'];
        }
                              if(isset($_POST['selectstatus']))
        {
     $selectstatus=$_POST['selectstatus'];
        }
                             if(isset($_POST['task']))
        {
     $task=$_POST['task'];
        }
	$error='';

if($task=='edit'){
    $selectstatus=0;
    $sql ="update `dripmarket_compaign` set cname='$cname',description='$txtcampaign',  cstatus='$selectstatus',userid='$userid' where id=$editid";
     $resource= mysql_query($sql);
    header("location:index.php?m=dripMarket");
    }else{
    $sql ="SELECT `dripmarket_compaign`.cname,`dripmarket_compaign`.id FROM `dripmarket_compaign` where `dripmarket_compaign`.userid='$userid' and cname='$cname'";

    $resource= mysql_query($sql);
    if(mysql_num_rows($resource)>0)
    {
    $error='Campaign exists';
    $this->_template->assign('error', $error);
    $this->_template->display('./modules/dripMarket/createcampaign.tpl');    
         //   $this->showcompaign();
    }else{
				$selectstatus=0;
				$sql ="insert into `dripmarket_compaign`(cname,description,  cstatus,userid)values('$cname','$txtcampaign','$selectstatus','$userid')";
                                $resource= mysql_query($sql);
			header("location:index.php?m=dripMarket");
        }
}
			

        
        
       

           // $this->_db->makeQueryInteger($activityID),
    
    
    
  
        
            
    }
    
function saveemail()
    {
                                   if(isset($_POST['sendemailon']))
        {
   $sendemailon=$_POST['sendemailon'];
        }
                                     if(isset($_POST['selecthour']))
        {
  $selecthour=$_POST['selecthour'];
        }
                                     if(isset($_POST['selectmin']))
        {
      $selectmin=$_POST['selectmin'];
        }
                                     if(isset($_POST['selecttimezone']))
        {
    $selecttimezone=$_POST['selecttimezone'];
        }
                                     if(isset($_POST['subject']))
        {
  $subject=$_POST['subject'];
        }
                                     if(isset($_POST['txtmessage']))
        {
   $txtmessage=$_POST['txtmessage'];
        }
                                     if(isset($_POST['cid']))
        {
    $cid=$_POST['cid'];
        }
                                  if(isset($_POST['cid']))
        {
    $cid=$_POST['cid'];
        }
                                      if(isset($_POST['status']))
        {
   $status=$_POST['status'];
        }
   if(isset($_POST['editid']))
        {
   $editid=$_POST['editid'];
        }
   if(isset($_POST['task']))
        {
   $task=$_POST['task'];
        }

  //$date=date('y-m-d');

  $txtmessagestripped=addslashes($txtmessage);


  if($task=='edit')
{
         $sql ="update `dripmarket_compaignemail` set nameofemail='$subject',subject='$subject',sendemailon='$sendemailon',  hour='$selecthour',  min='$selectmin',   Timezone='$selecttimezone',  Message='$txtmessagestripped'  where id=$editid";

    
}
else
{
    
      $sql ="insert into `dripmarket_compaignemail`(nameofemail,subject,sendemailon,creationdate,hour,min, Timezone,Message,compaignid)values('$subject','$subject','$sendemailon',now(),'$selecthour','$selectmin','$selecttimezone','$txtmessagestripped','$cid')";

   
}



           // $this->_db->makeQueryInteger($activityID),
    $resource= mysql_query($sql);
     
            $sql ="SELECT `candidate_joborder_status`.candidate_joborder_status_id   	  ,`candidate_joborder_status`.short_description    FROM `candidate_joborder_status` inner join `dripmarket_compaign` on `dripmarket_compaign`.cstatus=`candidate_joborder_status`.candidate_joborder_status_id     where dripmarket_compaign.id=$cid";
                $resourcedetail= mysql_query($sql);
   $resourcedetails= mysql_fetch_assoc($resourcedetail);
        $this->_template->assign('resourcedetails', $resourcedetails);
    
    
        $this->_template->assign('active', $this);
                $this->_template->assign('status', $status);
           $this->_template->assign('cid', $cid);
           $this->compaigndetail() ;
       // $this->_template->display('./modules/dripMarket/createemail.tpl');
            
    }
    
        function compaigndetail()
    {

  if(isset($_GET['id']))
        {
        $cid=$_GET['id'];
        }
            
$sql ="SELECT `dripmarket_compaign`.cname,`dripmarket_compaign`.cstatus ,`dripmarket_compaign`.id  FROM `dripmarket_compaign`
 where `dripmarket_compaign`.id=$cid";

           // $this->_db->makeQueryInteger($activityID),
    $resourcedetail= mysql_query($sql);
   $resourcedetails= mysql_fetch_assoc($resourcedetail);

     $sql ="SELECT `dripmarket_compaignemail`.sendemailon,`dripmarket_compaign`.cname,`dripmarket_compaignemail`.nameofemail,`dripmarket_compaignemail`.subject,`dripmarket_compaign`.cstatus ,`dripmarket_compaign`.id,`dripmarket_compaignemail`.id as eid  FROM `dripmarket_compaign`
INNER JOIN `dripmarket_compaignemail` ON `dripmarket_compaign`.id = `dripmarket_compaignemail`.compaignid
 where `dripmarket_compaignemail`.compaignid=$cid
";

           // $this->_db->makeQueryInteger($activityID),
    $resource= mysql_query($sql);
$campaignarray=array();
 $i=0;
 while($row = mysql_fetch_array($resource))
		{
                    
                   $campaignarray[$i]=$row;
                   $i++;
                }
                
                
                       $this->_template->assign('cname', $resourcedetails['cname']);
        $this->_template->assign('cstatus', $resourcedetails['cstatus']);
                  $this->_template->assign('cid', $resourcedetails['id']);
                
                
                

                $this->_template->assign('compaigndata', $campaignarray);
        $this->_template->assign('active', $this);
        $this->_template->display('./modules/dripMarket/compaigndetail.tpl');
            
    }
    function compaignemaildelete()
    {

  if(isset($_GET['id']))
        {
        $cid=$_GET['id'];
        }

  if(isset($_GET['delid']))
        {
     $delid=$_GET['delid'];
        }
        
             $sql ="delete from `dripmarket_compaignemail` where `dripmarket_compaignemail`.id=$delid
";
// $this->_db->makeQueryInteger($activityID),
    $resource= mysql_query($sql);
    
                 $sql ="SELECT `dripmarket_compaign`.cname,`dripmarket_compaign`.cstatus ,`dripmarket_compaign`.id  FROM `dripmarket_compaign`
 where `dripmarket_compaign`.id=$cid
";

           // $this->_db->makeQueryInteger($activityID),
    $resourcedetail= mysql_query($sql);
   $resourcedetails= mysql_fetch_assoc($resourcedetail);

 $sql ="SELECT `dripmarket_compaignemail`.sendemailon,`dripmarket_compaign`.cname,`dripmarket_compaignemail`.nameofemail,`dripmarket_compaignemail`.subject,`dripmarket_compaign`.cstatus ,`dripmarket_compaign`.id  ,`dripmarket_compaignemail`.id as eid FROM `dripmarket_compaign`
INNER JOIN `dripmarket_compaignemail` ON `dripmarket_compaign`.id = `dripmarket_compaignemail`.compaignid
 where `dripmarket_compaignemail`.compaignid=$cid
";

           // $this->_db->makeQueryInteger($activityID),
    $resource= mysql_query($sql);
$campaignarray=array();
 $i=0;
 while($row = mysql_fetch_array($resource))
		{
                    
                   $campaignarray[$i]=$row;
                   $i++;
                }
                    $this->_template->assign('cname', $resourcedetails['cname']);
        $this->_template->assign('cstatus', $resourcedetails['cstatus']);
                  $this->_template->assign('cid', $resourcedetails['id']);
                
                
                

                $this->_template->assign('compaigndata', $campaignarray);
        $this->_template->assign('active', $this);
        
        $this->_template->display('./modules/dripMarket/compaigndetail.tpl');
            
    }
    function showcompaign()
    {
        $userid=$_SESSION['RESUFLO']->getUserID();
        
    $sql ="SELECT count(`dripmarket_compaignemail`.id) AS noofemails, `dripmarket_compaign`.cname,`dripmarket_compaign`.id,  `dripmarket_compaign`.cstatus ,`candidate_joborder_status`.candidate_joborder_status_id   	  ,`candidate_joborder_status`.short_description  FROM `dripmarket_compaign`
left JOIN `dripmarket_compaignemail` ON `dripmarket_compaign`.id = `dripmarket_compaignemail`.compaignid left join candidate_joborder_status on candidate_joborder_status.candidate_joborder_status_id=`dripmarket_compaign`.cstatus where `dripmarket_compaign`.userid=$userid
GROUP BY `dripmarket_compaign`.id ";


           // $this->_db->makeQueryInteger($activityID),
    $resource= mysql_query($sql);
$campaignarray=array();
 $i=0;

 if((mysql_num_rows($resource))>0)
 {
 while($row = mysql_fetch_array($resource))
		{
            
                  
                   $campaignarray[$i]=$row;
        
                   $i++;
                }
 }
  $this->_template->assign('compaigndata', $campaignarray);
        $this->_template->assign('active', $this);
        $this->_template->display('./modules/dripMarket/compaign.tpl');

 }
     function editcompaign()
    {
  if(isset($_GET['editid']))
        {
     $editid=$_GET['editid'];
        }
        
     $sql ="SELECT `dripmarket_compaign`.cname,`dripmarket_compaign`.description,`dripmarket_compaign`.id,  `dripmarket_compaign`.cstatus FROM `dripmarket_compaign`  where dripmarket_compaign.id=$editid";
           // $this->_db->makeQueryInteger($activityID),
    $resource= mysql_query($sql);
   $resourcedetails= mysql_fetch_assoc($resource);
   
   
   
   
                    $sql ="SELECT `candidate_joborder_status`.candidate_joborder_status_id   	  ,`candidate_joborder_status`.short_description    FROM `candidate_joborder_status`
 ";    $resourcedetail= mysql_query($sql);

 $statusarray=array();
 $i=0;
 while($row = mysql_fetch_array($resourcedetail))
		{
                    
                   $statusarray[$i]=$row;
                   $i++;
                }

           // $this->_db->makeQueryInteger($activityID),



               $this->_template->assign('statusarray', $statusarray);
   
   
  $this->_template->assign('editid', $editid);
 $this->_template->assign('cname', $resourcedetails['cname']);
 $this->_template->assign('cid', $resourcedetails['id']);
  $this->_template->assign('description', $resourcedetails['description']);
 $this->_template->assign('cstatus', $resourcedetails['cstatus']);
  $this->_template->assign('task','edit');

        $this->_template->assign('active', $this);
        $this->_template->display('./modules/dripMarket/editcampaign.tpl');
            
    }
    
    
    
    
    function deletecompaign()
    {
        
       if(isset($_GET['delid']))
        {
     $delid=$_GET['delid'];
        }
         $userid=$_SESSION['RESUFLO']->getUserID(); 
        
 $sql="delete from `dripmarket_compaign` where `dripmarket_compaign`.id =$delid";
           // $this->_db->makeQueryInteger($activityID),
    $resource= mysql_query($sql);
$sql="delete from `dripmarket_compaignemail` where `dripmarket_compaignemail`.compaignid =$delid";
           // $this->_db->makeQueryInteger($activityID),
    $resource= mysql_query($sql);
        
        
    $sql ="SELECT count(`dripmarket_compaignemail`.id) AS noofemails, `dripmarket_compaign`.cname,`dripmarket_compaign`.id,  `dripmarket_compaign`.cstatus ,`candidate_joborder_status`.candidate_joborder_status_id   	  ,`candidate_joborder_status`.short_description  FROM `dripmarket_compaign`
left JOIN `dripmarket_compaignemail` ON `dripmarket_compaign`.id = `dripmarket_compaignemail`.compaignid left join candidate_joborder_status on candidate_joborder_status.candidate_joborder_status_id=`dripmarket_compaign`.cstatus where `dripmarket_compaign`.userid=$userid
GROUP BY `dripmarket_compaign`.id ";
           // $this->_db->makeQueryInteger($activityID),
    $resource= mysql_query($sql);
$campaignarray=array();
 $i=0;
 while($row = mysql_fetch_array($resource))
		{
                    
                   $campaignarray[$i]=$row;
                   $i++;
                }
                
                

                $this->_template->assign('compaigndata', $campaignarray);
        $this->_template->assign('active', $this);
        $this->_template->display('./modules/dripMarket/compaign.tpl');
            
    }
    
 function showconfigpage()
    {

$userid=$_SESSION['RESUFLO']->getUserID();
    $sql ="SELECT  dripmarket_configuration.smtphost  ,  dripmarket_configuration.smtppassword ,  dripmarket_configuration.smtpport  ,  dripmarket_configuration.fromname  ,  dripmarket_configuration.fromemail , dripmarket_configuration.bccemail  ,  dripmarket_configuration.smtpuser  FROM `dripmarket_configuration` where `dripmarket_configuration`.userid=$userid";
           // $this->_db->makeQueryInteger($activityID),
    $resource= mysql_query($sql);
    $resourcedetailsresult= mysql_fetch_assoc($resource);
  
      $this->_template->assign('resourcedetailsresult', $resourcedetailsresult);
      $this->_template->assign('active', $this);

       $this->_template->display('./modules/dripMarket/configuration.tpl');
   
    }

}

?>
