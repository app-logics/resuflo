<?php /* $Id: Companies.tpl 3460 2007-11-07 03:50:34Z brian $ */ ?>
<?php TemplateUtility::printHeader('Companies', array('js/highlightrows.js', 'js/export.js', 'js/dataGrid.js')); ?>
<?php TemplateUtility::printHeaderBlock(); ?>

<?php TemplateUtility::printTabs($this->active); ?>

<link rel="stylesheet" href="css/tab.css" />
<link rel="stylesheet" href="css/form.css" />
<script type="text/javascript" src="javascript/tab.js"></script>
<script type="text/javascript" src="javascript/validations.js"></script>
 
    <div id="main">
   
        <div id="contents">
            
			<!--tabs Starts-->		 
<ul id="countrytabs" class="shadetabs" style="padding-top:20px;">
<li><a  href="javascript:void(0)" onclick="javascript:goToURL('<?php echo(RESUFLOUtility::getIndexName()); ?>?m=hotleads&amp;a=showhotleads');" rel="country1">Hot Leads</a></li>

</ul>
<div style="border: 0px solid #32004b; width:900px; height:auto; margin-bottom:1em; padding:0px;">
<div id="country1" class="tabcontent">
<table width="100%" border="0" cellspacing="0" cellpadding="0" bgcolor="#fff" >
  <tr>
    <td colspan="4" bgcolor="#f8f8f8">&nbsp;</td>
  </tr>
    <tr>
    <td colspan="4" bgcolor="#f8f8f8">&nbsp;<a  href="javascript:void(0)" onclick="javascript:goToURL('<?php echo(RESUFLOUtility::getIndexName()); ?>?m=Hotleads');" style="color:black;"><b><< Main Page</b></a></td>
  </tr>
    <tr><td colspan="4" bgcolor="#f8f8f8" style="height:15px"></td></tr>
  <tr>
    <td colspan="4" bgcolor="#f8f8f8" style="padding-left:10px;"><a href="javascript:void(0)" onclick="javascript:goToURL('<?php echo(RESUFLOUtility::getIndexName()); ?>?m=dripMarket&amp;a=createcampaign');" ></a></td>
    </tr>
  <tr>
    <td colspan="4" bgcolor="#fff" >
		<table width="100%" border="0" cellspacing="2" cellpadding="2" bgcolor="#f8f8f8";>
      <tr>
        <td width="29%"><b>Candidate Name </b></td>
        <td width="71%"><table><tr><td style="width:40%"><?php echo $this->compaigndata[0]['first_name']; ?></td><td style="width:60%"><a href="javascript:void(0)" onclick="javascript:goToURL('<?php echo(RESUFLOUtility::getIndexName()); ?>?m=candidates&a=show&candidateID=<?php echo $this->compaigndata[0]['candidate_id']; ?>');" >View Candidate Details</a>
	</td></tr></table>

		 </td>
      </tr>
      <tr>
        <td><b>Email</b> </td>
        <td>
	<?php echo $this->compaigndata[0]['email1']; ?>
	    
	</td>
      </tr>
      <tr>
        <td><b>Questionaire Message </b></td>
        <td><label>
<?php echo substr($this->compaigndata[0]['message'],0,100); ?>
        </label></td>
      </tr>
      <tr><td></td></tr> <tr><td></td></tr>
      <tr><td colspan="2"><table>
        <?php foreach($this->campaignarrayques as $row){ ?>
       <tr>
        <td style="width:50px;color:#8ac923">Q</td>
        <td style="width:600px;"><label><b>
<?php echo $row['quesname']; ?></b>
        </label></td>
      </tr>
              <tr>
        <td style="width:50px;color:#8ac923">A</td>
        <td><label>
<?php echo $row['answer']; ?>
        </label></td>
      </tr> 
       <?php } ?></table></td></tr> </table></td>
  </tr>
    
</table>

</div>

</div>        
<script type="text/javascript">// <![CDATA[
var countries=new ddtabcontent("countrytabs")
countries.setpersist(true)
countries.setselectedClassTarget("link") //"link" or "linkparent"
countries.init()
// ]]></script>   
 <!--tabs end-->	
        </div>
    </div>
    <div id="bottomShadow"></div>
<?php TemplateUtility::printFooter(); ?>
