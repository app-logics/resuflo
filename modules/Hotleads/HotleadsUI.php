<?php
/*
 * RESUFLO
 * XML module
 *
 * Copyright (C) 2005 - 2007 Econn Technologies, Inc.
 *
 *
 * The contents of this file are subject to the RESUFLO Public License
 * Version 1.1a (the "License"); you may not use this file except in
 * compliance with the License. You may obtain a copy of the License at
 * http://www.econnoisseur.com/.
 *
 * Software distributed under the License is distributed on an "AS IS"
 * basis, WITHOUT WARRANTY OF ANY KIND, either express or implied. See the
 * License for the specific language governing rights and limitations
 * under the License.
 *
 * The Original Code is "RESUFLO Standard Edition".
 *
 * The Initial Developer of the Original Code is Econn Technologies, Inc.
 * Portions created by the Initial Developer are Copyright (C) 2005 - 2007
 * (or from the year in which this file was created to the year 2007) by
 * Econn Technologies, Inc. All Rights Reserved.
 *
 *
 * This module builds an XML file containing public job postings. The
 * exported XML data can be used to submit, en masse, all public job
 * postings to job bulletin sites such as Indeed.com.
 *
 *
 * $Id: XmlUI.php 3565 2007-11-12 09:09:22Z will $
 */
//include_once('./lib/dripmarketting.php');
include_once('./lib/ActivityEntries.php');
include_once('./lib/StringUtility.php');
include_once('./lib/DateUtility.php');
include_once('./lib/JobOrders.php');
include_once('./lib/Site.php');
include_once('./lib/XmlJobExport.php');
include_once('./lib/HttpLogger.php');
include_once('./lib/CareerPortal.php');
include_once('./lib/DataGrid.php');

class HotleadsUI extends UserInterface
{
     public function __construct()
    {
        parent::__construct();

        $this->_authenticationRequired = true;
        $this->_moduleDirectory = 'Hotleads';
        $this->_moduleName = 'Hotleads';
        $this->_moduleTabText = 'Hot Leads';
        $this->_subTabs = array( 
			'All Hot Candidates' => RESUFLOUtility::getIndexName() . '?m=Hotleads&view=all'
            );

    }
    

    function handleRequest()
    {
        $action = $this->getAction();
        
           $userid=$_SESSION['RESUFLO']->getUserID();
    
          switch ($action)
             {
                 case 'viewquesans':
                     $this->viewquesans();
                     break;
               case 'delete_Candidates':
                $this->deleteCandidates();
                break;
                case 'deletehotlead':
                     $this->deletehotlead();
                     break;
               default:  $this->_template->assign('active', $this);
              $this->listByView();
                     break;
             }
        
        
    }
    
       /*
     * Called by handleRequest() to process loading the list / main page.
     */
    private function listByView($errMessage = '')
    {
        // Log message that shows up on the top of the list page
        $topLog = '';


        $dataGridProperties = DataGrid::getRecentParamaters("Hotleads:candidatesListByViewDataGrid");

//echo "<pre>"; print_r($dataGridProperties);

        /* If this is the first time we visited the datagrid this session, the recent paramaters will
         * be empty.  Fill in some default values. */
        if ($dataGridProperties == array())
        {
            $dataGridProperties = array('rangeStart'    => 0,
                                        'maxResults'    => 15,
                                        'filterVisible' => false);
        }
          //echo "<pre>";print_r($dataGridProperties);
      
        $dataGrid = DataGrid::get("Hotleads:candidatesListByViewDataGrid", $dataGridProperties);
        
        $candidates = new Candidates($this->_siteID);
        $this->_template->assign('totalCandidates', $candidates->getCount());

        $this->_template->assign('active', $this);
        $this->_template->assign('dataGrid', $dataGrid);
        $this->_template->assign('userID', $_SESSION['RESUFLO']->getUserID());
        $this->_template->assign('errMessage', $errMessage);
        $this->_template->assign('topLog', $topLog);

        if (!eval(Hooks::get('CANDIDATE_LIST_BY_VIEW'))) return;
		

        $this->_template->display('./modules/Hotleads/hotleads.tpl');
    }
    
    
    
    
     function deletehotlead()
    {
      
        $userid=$_SESSION['RESUFLO']->getUserID();

       if(isset($_GET['candid']))
        {
        $candid=$_GET['candid'];

       
        }
               if(isset($_GET['delid']))
        {
        $delid=$_GET['delid'];
         }


          $sql ="delete from dripmarket_quesans where cid=$candid and quesid in (select id from dripmarket_questable where questionaireid=$delid)";




           // $this->_db->makeQueryInteger($activityID),
           $resource= mysql_query($sql);
        
        
        
          $sql ="SELECT  candidate.candidate_id ,dripmarket_quesans.date_created,candidate.email1,candidate.entered_by,dripmarket_questionaire.message,candidate.first_name,dripmarket_quesans.cid, dripmarket_questable.questionaireid  from candidate inner join dripmarket_quesans on dripmarket_quesans.cid=candidate.candidate_id inner join dripmarket_questable on dripmarket_quesans.quesid=dripmarket_questable.id inner join dripmarket_questionaire on dripmarket_questionaire.id=dripmarket_questable.questionaireid inner join user on user.user_id=candidate.entered_by  where user.user_id=$userid and candidate.ishot=1 group by candidate.candidate_id";
          
          
          // $this->_db->makeQueryInteger($activityID),
          $resource= mysql_query($sql);
          $campaignarray=array();
           $i=0;
          
           if((mysql_num_rows($resource))>0)
           {
           while($row = mysql_fetch_array($resource))
                          {
                    $campaignarray[$i]=$row;
                     $i++;
                          }
           }
           
          $this->_template->assign('compaigndata', $campaignarray);
            $this->_template->assign('active', $this);
            $this->_template->display('./modules/Hotleads/hotleads.tpl');

 }
  function showhotleads()
    {
    
          $userid=$_SESSION['RESUFLO']->getUserID();
        
           $sql ="SELECT candidate.candidate_id ,candidate.email1,candidate.entered_by,dripmarket_questionaire.message,candidate.first_name,dripmarket_quesans.cid, dripmarket_quesans.date_created,dripmarket_questable.questionaireid  from candidate inner join dripmarket_quesans on dripmarket_quesans.cid=candidate.candidate_id inner join dripmarket_questable on dripmarket_quesans.quesid=dripmarket_questable.id  inner join dripmarket_questionaire on dripmarket_questionaire.id=dripmarket_questable.questionaireid inner join user on user.user_id=candidate.entered_by  where dripmarket_questionaire.userid=$userid and candidate.entered_by=$userid and candidate.ishot=1 group by candidate.candidate_id ";
    	
  	
                    // $this->_db->makeQueryInteger($activityID),
          $resource= mysql_query($sql);
		  if((mysql_num_rows($resource))>0)
		  {
          $total_rows =  mysql_num_rows($resource);
		  }
		  else 
		  {
		   $total_rows  = 0 ;
		  }
		  $campaignarray=array();
          $i=0;
         
          if((mysql_num_rows($resource))>0)
          {
              while($row = mysql_fetch_array($resource))
              {
                       $campaignarray[$i]=$row;
                        $i++;
              }
          }
		  $dataGridProperties = DataGrid::getRecentParamaters("hotleads:hotleadsListByViewDataGrid");
		   if ($dataGridProperties == array())
        {
            $dataGridProperties = array('rangeStart'    => 0,
                                        'maxResults'    => 15,
                                        'filterVisible' => false);
        }

        
	//	$dataGrid = DataGrid::get("hotleads:hotleadsListByViewDataGrid", $dataGridProperties);
	 
	  $this->_template->assign('dataGrid', $dataGrid);
          $this->_template->assign('total_rows', $total_rows);
          $this->_template->assign('compaigndata', $campaignarray);
          $this->_template->assign('active', $this);
          $this->_template->display('./modules/Hotleads/hotleads.tpl');

 }
 
 //HOTLEADS DELETE
 
   public function deleteCandidates()
    {
		
		if ($this->_accessLevel < ACCESS_LEVEL_DELETE)
        {
            CommonErrors::fatal(COMMONERROR_PERMISSION, $this, 'Invalid user level for action.');
        }

        /* Bail out if we don't have a valid candidate ID. */
		
		$dataGrid = DataGrid::getFromRequest();
		$candidateIDs = $dataGrid->getExportIDs();
		$del=array();
		foreach($candidateIDs as $val =>$key){
			$del['m'] = 'candidates';
			$del['a'] = 'delete_Candidates';
			$del['candidateID'] = $key;
			
			if (!$this->isRequiredIDValid('candidateID', $del))
			{
				CommonErrors::fatal(COMMONERROR_BADINDEX, $this, 'Invalid candidate ID.');
			}
	
			$candidateID = $del['candidateID'];
	
			if (!eval(Hooks::get('CANDIDATE_DELETE'))) return;
	
			$candidates = new Hotleads($this->_siteID);
			$candidates->delete($candidateID);
	
			/* Delete the MRU entry if present. */
			$_SESSION['RESUFLO']->getMRU()->removeEntry(
				DATA_ITEM_CANDIDATE, $candidateID
			);
			
		}
                    RESUFLOUtility::transferRelativeURI('m=Hotleads&a=listByView');
		
		/*$userid = $_SESSION['RESUFLO']->getuserid();  
		        if ($this->_accessLevel < ACCESS_LEVEL_EDIT)
                {
                    CommonErrors::fatal(COMMONERROR_BADINDEX, $this, 'Invalid user level for action.');
                    return;
                }
				$dataGrid = DataGrid::getFromRequest();
                $candidateIDs = $dataGrid->getExportIDs();
		for($i = 0 ; $i<count($candidateIDs) ; $i++ )
                {
                        if($this->_accessLevel == 500){
							$sql=" DELETE FROM candidate where candidate_id = '$candidateIDs[$i]'";
						}else {
							$sql=" DELETE FROM candidate where candidate_id = '$candidateIDs[$i]' AND entered_by ='$userid'";
						}
						
                        $result=mysql_query($sql);
                }
    
       $_SESSION['RESUFLO']->getMRU()->removeEntry(
            DATA_ITEM_CANDIDATE, $candidateID
        );
      

      
       
		RESUFLOUtility::transferRelativeURI('m=candidates&a=listByView');*/
	 }
 
 
 //HOTLEADS DELETE
 
 
 
 
 
 
 function viewquesans()
    {
      
        $userid=$_SESSION['RESUFLO']->getUserID();
              if(isset($_GET['candid']))
        {
        $candid=$_GET['candid'];

       
        }
        
  $sql ="SELECT  candidate.candidate_id ,candidate.email1,candidate.entered_by,dripmarket_questionaire.message,candidate.first_name,dripmarket_quesans.cid, dripmarket_questable.questionaireid  from candidate inner join dripmarket_quesans on dripmarket_quesans.cid=candidate.candidate_id inner join dripmarket_questable on dripmarket_quesans.quesid=dripmarket_questable.id  inner join dripmarket_questionaire on dripmarket_questionaire.id=dripmarket_questable.questionaireid inner join user on user.user_id=candidate.entered_by  where user.user_id=$userid and candidate.candidate_id=$candid  group by candidate.candidate_id";


           // $this->_db->makeQueryInteger($activityID),
    $resource= mysql_query($sql);
$campaignarray=array();
 $i=0;

 if((mysql_num_rows($resource))>0)
 {
 while($row = mysql_fetch_array($resource))
		{
          $campaignarray[$i]=$row;
           $i++;
                }
 }
 $sql ="SELECT  dripmarket_questionaire.message ,dripmarket_questable.quesname,candidate.candidate_id,dripmarket_questable.questionaireid ,dripmarket_questable.questionaireid,dripmarket_quesans.answer   from  dripmarket_quesans inner join dripmarket_questable on dripmarket_quesans.quesid=dripmarket_questable.id  inner join dripmarket_questionaire on dripmarket_questionaire.id=dripmarket_questable.questionaireid inner join candidate on dripmarket_quesans.cid=candidate.candidate_id where dripmarket_questionaire.id=".$campaignarray[0]['questionaireid']." and dripmarket_quesans.cid=".$candid ;

           // $this->_db->makeQueryInteger($activityID),
    $resource= mysql_query($sql);
$campaignarrayques=array();
 $i=0;

 if((mysql_num_rows($resource))>0)
 {
 while($row = mysql_fetch_array($resource))
		{
          $campaignarrayques[$i]=$row;
           $i++;
                }
 }
 $this->_template->assign('campaignarrayques', $campaignarrayques);
 $this->_template->assign('compaigndata', $campaignarray);
 $this->_template->assign('active', $this);
$this->_template->display('./modules/Hotleads/Candidatedetail.tpl');

 }
 
 
  
}

?>
