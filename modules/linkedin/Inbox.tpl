<?php /* $Id: Search.tpl 1948 2007-02-23 09:49:27Z will $ */ ?>
<?php TemplateUtility::printHeader('Linkedin', array('js/highlightrows.js', 'modules/activity/validator.js', 'js/sweetTitles.js')); ?>
<style>
body{
    padding: 0px;
    width: 100% !important;
    height: 650px;
}
#main{
    width: 920px;
}
#bottomShadow{
    width: 920px !important;
}
.conversations {
    top: 60px;
    left: 0px;
    width: 300px;
    height: 650px; 
    bottom: 0px;
    overflow-x: hidden;
    overflow-y: auto;
}
.conversation {
    position: relative;
    display: block;
    height: 60px;
    margin: 1px;
    margin-right: 0px;
    background-color: #fff;
    cursor: pointer;
}
.conversation.new {
    background-color: #d8e9fb;
}
.conversation.active, .conversation:hover {
    background-color: #e8e6e6;
}
.conversation > .pic {
    position: absolute;
    background-color: #f3f1f1;
    top: 15px;
    left: 10px;
    width: 36px;
    height: 36px;
    border-radius: 50%;
}
.conversation > .pic > img {
    width: 100%;
    height: 100%;
}
.conversation > .name {
    position: absolute;
    top: 5px;
    left: 55px;
    font-weight: bold;
    font-size: 12px;
    color: #4b94ea;
    line-height: 20px;
    width: 60%;
    text-overflow: ellipsis;
    white-space: nowrap;
    overflow: hidden;
}
.conversation > .title {
    position: absolute;
    bottom: 5px;
    left: 55px;
    height: 20px;
    font-size: 11px;
    width: 80%;
    text-overflow: ellipsis;
    white-space: nowrap;
    overflow: hidden;
}
.conversation > .date {
    position: absolute;
    top: 15px;
    left: 210px;
    font-size: 11px;
    color: #999;
}
.conversation-head {
    position: relative;
    width: 100%;
    height: 60px;
    background-color: #efefef;
    margin: 2px 2px 2px 0px;
}
.conversation-head > .pic {
    position: absolute;
    background-color: #f3f1f1;
    top: 15px;
    left: 10px;
    width: 36px;
    height: 36px;
    border-radius: 50%;
}
.conversation-head > .pic > img {
    width: 36px;
    height: 36px;
}
.conversation-head > .name {
    position: absolute;
    top: 20px;
    left: 55px;
    font-weight: bold;
    font-size: 12px;
    color: #4b94ea;
    line-height: 20px;
    width: 60%;
    text-overflow: ellipsis;
    white-space: nowrap;
    overflow: hidden;
}
.message-direction-out {
    float: right;
    padding: 15px;
    background-color: #fff;
}
.message-direction {
    display: inline-block;
    width: 50%;
    margin: 10px;
    border-radius: 10px;
    font-size: 12px;
}
.message-direction-in {
    color: #fff;
    float: left;
    padding: 15px;
    background-color: #4499ff;
}
.message-inbox-icon {
    color: #fff;
    display: inline-block;
    padding: 3px;
    margin-right: 5px;
    border: 1px solid #fff;
    background-color: #4499ff;
    font-size: 10px;
    font-weight: bold;
}
.message-date {
    color: #ccc;
    float: right;
    margin-top: 10px;
    font-size: 11px;
}
.hide{
    display: none;
}
.menu{
    padding: 5px 5px;
    text-align: center;
}
.menu img{
    vertical-align: bottom;
}
.active, .active:hover{
    color: #fff;
    background-color: #3bc0c3;
}
.menu > a{
    color: #fff;
    font-weight: bold;
    text-decoration: none;
    display: block;
}
.menu > a:hover{
    opacity: 0.8;
}
</style>
<div id="main" style="padding-top: 0px;">
        <div id="contents" style="padding: 0px;">
            <table width="100%" style="border-collapse: collapse;">
                <!--<tr>
                    <td style="background-color: #1f2944; color: white; padding: 0px;">
                        <table width="100%" style="border-spacing: 0px;">
                            <tr>
                                <td class="menu active"><a href="#"><img src="images/actions/email.gif" /> Inbox</a></td>
                                <td class="menu"><a href="<?php echo RESUFLOUtility::getIndexName(); ?>?m=linkedin&amp;a=loadProfiles"><img src="images/mru/candidate.gif" /> Profiles</a></td>
                            </tr>
                        </table>
                    </td>
                    <td style="background-color: #1f2944; color: white; padding: 0px;">
                        
                    </td>
                </tr>-->
                <tr>
                    <td style="width: 300px;">
                        <div class="conversations">
                            <?php foreach ($this->messages as $message => $row){?>
                                <div class="conversation <?php if(empty($row['is_read'])){ echo 'new';} ?>">
                                    <div class="pic"><img class="profile_photo" src="images/mru/anonymous-person-png.png"></div> 
                                    <div class="name"><?php echo $row['first_name'].' '.$row['last_name']; ?></div> 
                                    <div class="title"><?php echo $row['profile_title']; ?></div> 
                                    <div class="date"><?php echo explode(' ', $row['date_created'])[0]; ?></div>
                                    <div class="candidate_id" style="display: none;"><?php echo $row['candidate_id']; ?></div>
                                    <div class="profile_id" style="display: none;"><?php echo $row['import_id']; ?></div>
                                    <div class="profile_url" style="display: none;"><?php echo RESUFLOUtility::getIndexName().'?m=candidates&list=LinkedIn&a=show&candidateID='.$row['candidate_id'].'&view=1'; ?></div>
                                    <div class="message_thread" style="display: none;"></div>
                                </div>
                            <?php }?>
                        </div>
                    </td>
                    <td style="vertical-align: top" rowspan="2">
                        <div class="conversation-head hide">
                            <a class="pic" target="_blank" href="#" style="width: 200px; text-decoration: none;">
                                <img class="profile_photo" src="images/mru/anonymous-person-png.png">
                                <span class="name" style="margin-top: 10px;position: absolute;font-size: 12px;color: #4b94ea;font-weight: bold;margin-left: 5px;"></span>
                            </a> 
                            <a href="#"  target="_blank" class="pic reply" style="float: right;position: relative;left: 0px;top: 4px;margin-right: 10px;cursor: pointer;text-decoration: none;"><img src="images/mru/reply-512.png"/>Reply</a>
                        </div>
                        <div style="top:120px;right:0px;bottom:30px;overflow:auto;">
                            <div class="busy hide" style="text-align: center;">
                                <img src="images/loading.gif" style="margin-top: 200px;" />
                            </div>
                            <div id="Conversation">
                                <h3 style="text-align: center;">Please click on message to load</h3>
                            </div>
                        </div>
                    </td>
                </tr>
            </table>
        </div>
    </div>
<script type="text/javascript">
    $(document).ready(function(){
        $(".conversation").click(function(){
            var pointer = this;
            $(".conversation-head").find(".name").eq(0).text($(this).find(".name").eq(0).text());
            $(".conversation-head").find(".pic").eq(0).attr("href", $(this).find(".profile_url").eq(0).text());
            $(".conversation-head").find(".reply").eq(0).attr("href", $(this).find(".message_thread").eq(0).text());
            $(".busy").removeClass("hide");
            $("#Conversation").html("");
            var CandidateId = $(this).find(".candidate_id").eq(0).text();
            var ProfileId = $(this).find(".profile_id").eq(0).text();
            $.ajax({
                url: "index.php?m=linkedin&a=loadMessage&candidate_id="+CandidateId, 
                success: function(result)
                {
                    $(".conversation-head").removeClass('hide');
                    $(".busy").addClass("hide");
                    $("#Conversation").html(result);
                    $(pointer).removeClass('new');
                    $(".conversation").removeClass('active');
                    $(pointer).addClass('active');
                }
            });
            $.ajax({
                url: "index.php?m=linkedin&a=getMessageThread&profile_id="+ProfileId, 
                success: function(result)
                {
                    $(".conversation-head").find(".reply").eq(0).attr("href",result);
                }
            });
        });
    });
</script>
