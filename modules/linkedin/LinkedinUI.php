<?php
/*
 * RESUFLO
 * Home Module
 *
 * Copyright (C) 2005 - 2007 Econn Technologies, Inc.
 *
 *
 * The contents of this file are subject to the RESUFLO Public License
 * Version 1.1a (the "License"); you may not use this file except in
 * compliance with the License. You may obtain a copy of the License at
 * http://www.econnoisseur.com/.
 *
 * Software distributed under the License is distributed on an "AS IS"
 * basis, WITHOUT WARRANTY OF ANY KIND, either express or implied. See the
 * License for the specific language governing rights and limitations
 * under the License.
 *
 * The Original Code is "RESUFLO Standard Edition".
 *
 * The Initial Developer of the Original Code is Econn Technologies, Inc.
 * Portions created by the Initial Developer are Copyright (C) 2005 - 2007
 * (or from the year in which this file was created to the year 2007) by
 * Econn Technologies, Inc. All Rights Reserved.
 *
 *
 * $Id: LinkedUI.php 3810 2007-12-05 19:13:25Z brian $
 */

include_once('./lib/NewVersionCheck.php');
include_once('./lib/CommonErrors.php');
include_once('./lib/LinkedIn.php');


class LinkedinUI extends UserInterface
{
    public function __construct()
    {
        parent::__construct();

        $this->_authenticationRequired = true;
        $this->_moduleDirectory = 'linkedin';
        $this->_moduleName = 'linkedin';
        $this->_moduleTabText = '';
            
        // addedd by waleed
        if(!empty($_GET['siteID']) && $_GET['siteID'] != 0){
            $this->_siteID = $_GET['siteID'];
        }
        
    }


    public function handleRequest()
    {
        $action = $this->getAction();

        if (!eval(Hooks::get('HOME_HANDLE_REQUEST'))) return;

        switch ($action)
        {
            case 'loadProfiles':
                $this->Profiles();
                break;
            case 'loadMessage':
                $this->LoadMessages();
                break;
            case 'getMessageThread':
                $message_thread = $this->GetMessageThread();
                echo $message_thread;
                break;
            default:
                $this->Inbox();
                break;
        }
    }


    private function Inbox()
    {   
        $linkedIn = new LinkedIn();
        $messages = $linkedIn->getAllMessages();
        $this->_template->assign('messages', $messages);
        $this->_template->display('./modules/linkedin/Inbox.tpl');
    }
    private function LoadMessages()
    {   
        $CandidateId = $_GET['candidate_id'];
        $linkedIn = new LinkedIn();
        $messages = $linkedIn->getConversation($CandidateId);
        $this->_template->assign('messages', $messages);
        $this->_template->display('./modules/linkedin/Conversation.tpl');
    }
    private function GetMessageThread()
    {   
        $ProfileId = $_GET['profile_id'];
        $linkedIn = new LinkedIn();
        $messages = $linkedIn->GetMessageThread($ProfileId);
        return $messages;
    }
    private function Profiles()
    {
        $linkedIn = new LinkedIn();
        $profiles = $linkedIn->getProfiles();
        $this->_template->assign('profiles', $profiles);
        $this->_template->display('./modules/linkedin/Profile.tpl');
    }
}

?>
