<?php /* $Id: Search.tpl 1948 2007-02-23 09:49:27Z will $ */ ?>
<?php TemplateUtility::printHeader('Linkedin', array('js/highlightrows.js', 'modules/activity/validator.js', 'js/sweetTitles.js')); ?>
<?php TemplateUtility::printHeaderBlock(); ?>
<?php TemplateUtility::printTabs($this->active); ?>
<style>
.conversations {
    top: 60px;
    left: 0px;
    width: 300px;
    height: 650px; 
    bottom: 0px;
    overflow-x: hidden;
    overflow-y: auto;
}
.conversation {
    position: relative;
    display: block;
    height: 60px;
    margin: 1px;
    margin-right: 0px;
    background-color: #fff;
    cursor: pointer;
}
.conversation.new {
    background-color: #d8e9fb;
}
.conversation.active, .conversation:hover {
    background-color: #e8e6e6;
}
.conversation > .pic {
    position: absolute;
    background-color: #f3f1f1;
    top: 15px;
    left: 10px;
    width: 36px;
    height: 36px;
    border-radius: 50%;
}
.conversation > .pic > img {
    width: 100%;
    height: 100%;
}
.conversation > .name {
    position: absolute;
    top: 5px;
    left: 55px;
    font-weight: bold;
    font-size: 12px;
    color: #4b94ea;
    line-height: 20px;
    width: 60%;
    text-overflow: ellipsis;
    white-space: nowrap;
    overflow: hidden;
}
.conversation > .title {
    position: absolute;
    bottom: 5px;
    left: 55px;
    height: 20px;
    font-size: 11px;
    width: 80%;
    text-overflow: ellipsis;
    white-space: nowrap;
    overflow: hidden;
}
.conversation > .date {
    position: absolute;
    top: 15px;
    left: 210px;
    font-size: 11px;
    color: #999;
}
.conversation-head {
    position: relative;
    width: 645px;
    height: 60px;
    background-color: #efefef;
    margin: 2px 2px 2px 0px;
}
.conversation-head > .pic {
    position: absolute;
    background-color: #f3f1f1;
    top: 15px;
    left: 10px;
    width: 36px;
    height: 36px;
    border-radius: 50%;
}
.conversation-head > .pic > img {
    width: 36px;
    height: 36px;
}
.conversation-head > .name {
    position: absolute;
    top: 20px;
    left: 55px;
    font-weight: bold;
    font-size: 12px;
    color: #4b94ea;
    line-height: 20px;
    width: 60%;
    text-overflow: ellipsis;
    white-space: nowrap;
    overflow: hidden;
}
.message-direction-out {
    float: right;
    padding: 15px;
    background-color: #fff;
}
.message-direction {
    display: inline-block;
    width: 50%;
    margin: 10px;
    border-radius: 10px;
    font-size: 12px;
}
.message-direction-in {
    color: #fff;
    float: left;
    padding: 15px;
    background-color: #4499ff;
}
.message-inbox-icon {
    color: #fff;
    display: inline-block;
    padding: 3px;
    margin-right: 5px;
    border: 1px solid #fff;
    background-color: #4499ff;
    font-size: 10px;
    font-weight: bold;
}
.message-date {
    color: #ccc;
    float: right;
    margin-top: 10px;
    font-size: 11px;
}
.hide{
    display: none;
}
.menu{
    padding: 5px 5px;
    text-align: center;
}
.menu img{
    vertical-align: bottom;
}
.active, .active:hover{
    color: #fff;
    background-color: #3bc0c3;
}
.menu > a{
    color: #fff;
    font-weight: bold;
    text-decoration: none;
    display: block;
}
.menu > a:hover{
    opacity: 0.8;
}
.profile-photo{
    width: 32px;
    height: 32px;
}
.profile-photo-sm{
    width: 28px;
    height: 28px;
}
.profile-table{
    border: 1px solid #ccc;
}
.profile-table tr:nth-child(even) {
    background: #FFF;
}
.profile-table tr:nth-child(odd) {
    background: #efefef;
}
.profile-table th{
    border-right: 1px solid gray;
    text-align: left;
    padding-left: 5px;
    border-top: 1px solid #ccc;
    border-bottom: 1px solid #ccc;
    background-image: url(images/sort_table_bg.gif);
}
.profile-table th:last-child{
    border-right: none;
}
</style>
    <div id="main">
        <div id="contents" style="padding: 0px;">
            <table width="100%" style=" border-collapse: collapse;">
                <tr>
                    <td style="width: 300px; background-color: #1f2944; color: white; padding: 0px;">
                        <table width="100%;" style="border-spacing: 0px;">
                            <tr>
                                <td class="menu"><a href="<?php echo RESUFLOUtility::getIndexName(); ?>?m=linkedin"><img src="images/actions/email.gif" /> Inbox</a></td>
                                <td class="menu active"><a href="#"><img src="images/mru/candidate.gif" /> Profiles</a></td>
                            </tr>
                        </table>
                    </td>
                    <td style="background-color: #1f2944; color: white; padding: 0px;"></td>
                </tr>
                <tr>
                    <td colspan="2">
                        <div style="max-height: 650px;overflow-y: auto;">
                            <table class="profile-table" width="100%" style="border-collapse: collapse;">
                                <tr style="background-color: #efefef;">
                                    <th>Photo</th>
                                    <th>Name</th>
                                    <th>Email</th>
                                    <th>Title</th>
                                    <th>Company</th>
                                    <th>Website</th>
                                    <th>Industry</th>
                                </tr>
                                <?php foreach ($this->profiles as $profile => $row){?>
                                    <tr>
                                        <td title="<?php echo $row['profile_linkedin_url']; ?>"><a href="<?php echo $row['profile_linkedin_url']; ?>" target="_blank"><img class="profile-photo" src="images/mru/anonymous-person-png.png"></a></td>
                                        <td><?php echo $row['profile_full_name']; ?></td>
                                        <td><?php echo $row['profile_email']; ?></td>
                                        <td><?php echo $row['profile_title']; ?></td>
                                        <td><?php echo $row['company_name']; ?></td>
                                        <td title="<?php echo $row['company_website']; ?>"><?php if(!empty($row['company_website'])){ ?><a href="http://<?php echo $row['company_website']; ?>" target="_blank"><img class="profile-photo-sm" src="images/mru/www-globe.png"></a><?php }?></td>
                                        <td><?php echo $row['company_industry']; ?></td>
                                    </tr>
                                <?php }?>
                            </table>
                        </div>
                    </td>
                </tr>
            </table>
        </div>
    </div>
    <div id="bottomShadow"></div>
<?php TemplateUtility::printFooter(); ?>
<script type="text/javascript">
    $(document).ready(function(){
        $(".conversation").click(function(){
            var pointer = this;
            $(".conversation-head").find(".name").eq(0).text($(this).find(".name").eq(0).text());
            $(".conversation-head").find(".pic").eq(0).attr("href", $(this).find(".profile_url").eq(0).text());
            $(".conversation-head").find(".reply").eq(0).attr("href", $(this).find(".message_thread").eq(0).text());
            $(".busy").removeClass("hide");
            $("#Conversation").html("");
            var ProfileId = $(this).find(".profile_id").eq(0).text();
            $.ajax({
                url: "index.php?m=linkedin&a=loadMessage&profile_id="+ProfileId, 
                success: function(result)
                {
                    $(".conversation-head").removeClass('hide');
                    $(".busy").addClass("hide");
                    $("#Conversation").html(result);
                    $(pointer).removeClass('new');
                    $(".conversation").removeClass('active');
                    $(pointer).addClass('active');
                }
            });
        });
    });
</script>
