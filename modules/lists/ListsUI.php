<?php
/*
 * RESUFLO
 * Lists Module
 *
 * Copyright (C) 2005 - 2007 Econn Technologies, Inc.
 *
 *
 * The contents of this file are subject to the RESUFLO Public License
 * Version 1.1a (the "License"); you may not use this file except in
 * compliance with the License. You may obtain a copy of the License at
 * http://www.econnoisseur.com/.
 *
 * Software distributed under the License is distributed on an "AS IS"
 * basis, WITHOUT WARRANTY OF ANY KIND, either express or implied. See the
 * License for the specific language governing rights and limitations
 * under the License.
 *
 * The Original Code is "RESUFLO Standard Edition".
 *
 * The Initial Developer of the Original Code is Econn Technologies, Inc.
 * Portions created by the Initial Developer are Copyright (C) 2005 - 2007
 * (or from the year in which this file was created to the year 2007) by
 * Econn Technologies, Inc. All Rights Reserved.
 *
 *
 * $Id: ListsUI.php 3807 2007-12-05 01:47:41Z will $
 */

include_once('./lib/StringUtility.php');
include_once('./lib/DateUtility.php'); /* Depends on StringUtility. */
include_once('./lib/ResultSetUtility.php');
include_once('./lib/Companies.php');
include_once('./lib/Contacts.php');
include_once('./lib/JobOrders.php');
include_once('./lib/Attachments.php');
include_once('./lib/Export.php');
include_once('./lib/ListEditor.php');
include_once('./lib/FileUtility.php');
include_once('./lib/SavedLists.php');
include_once('./lib/ExtraFields.php');


class ListsUI extends UserInterface
{

    public function __construct()
    {
        parent::__construct();

        $this->_authenticationRequired = true;
        $this->_moduleDirectory = 'lists';
        $this->_moduleName = 'lists';
        $this->_moduleTabText = 'Lists';
        $this->_subTabs = array(
            'Show Lists'     => RESUFLOUtility::getIndexName() . '?m=lists'
           /* 'New Static List' => RESUFLOUtility::getIndexName() . '?m=lists&a=newListStatic*al=' . ACCESS_LEVEL_EDIT, */
           /* 'New Dynamic List' => RESUFLOUtility::getIndexName() . '?m=lists&a=newListDynamic*al=' . ACCESS_LEVEL_EDIT */
        );
    }


    public function handleRequest()
    {
        $action = $this->getAction();

        if (!eval(Hooks::get('LISTS_HANDLE_REQUEST'))) return;

        switch ($action)
        {
            case 'show':
                $this->show();
                break;

            case 'showList':
                $this->showList();
                break;

            /* Add to list popup. */
            case 'quickActionAddToListModal':
                $this->quickActionAddToListModal();
                break;

            /* Add to list popup via datagrid. */
            case 'addToListFromDatagridModal':
                $this->addToListFromDatagridModal();
                break;
            
            /* Add to list popup via pipeline datagrid. */
            case 'addToListFromPipelineDatagridModal':
                $this->addToListFromPipelineDatagridModal();
                break;

            /* Add to campaign popup via datagrid. */
            case 'addToCampaignFromDatagridModal':
                $this->addToCampaignFromDatagridModal();
                break;

            case 'removeFromListDatagrid':
                $this->removeFromListDatagrid();
                break;

            case 'deleteStaticList':
                $this->onDeleteStaticList();
                break;

            /* Main list page. */
            case 'listByView':
            default:
                $this->listByView();
                break;
        }
    }

    /*
     * Called by handleRequest() to process loading the list / main page.
     */
    private function listByView()
    {
        /* First, if we are operating in HR mode we will never see the
           companies pager.  Immediantly forward to My Company. */

        $dataGridProperties = DataGrid::getRecentParamaters("lists:ListsDataGrid");

        /* If this is the first time we visited the datagrid this session, the recent paramaters will
         * be empty.  Fill in some default values. */
        if ($dataGridProperties == array())
        {
            $dataGridProperties = array('rangeStart'    => 0,
                                        'maxResults'    => 15,
                                        'filterVisible' => false);
        }

        $dataGrid = DataGrid::get("lists:ListsDataGrid", $dataGridProperties);

        $this->_template->assign('active', $this);
        $this->_template->assign('dataGrid', $dataGrid);
        $this->_template->assign('userID', $_SESSION['RESUFLO']->getUserID());

        if (!eval(Hooks::get('LISTS_LIST_BY_VIEW'))) return;

        $this->_template->display('./modules/lists/Lists.tpl');
    }

    /*
     * Called by handleRequest() to process loading the static list display.
     */

    private function showList()
    {
       
        /* Bail out if we don't have a valid candidate ID. */
        if (!$this->isRequiredIDValid('savedListID', $_GET))
        {
            CommonErrors::fatalModal(COMMONERROR_BADINDEX, $this);
            return;
            //$this->fatalModal('Invalid saved list ID.');
        }

        //$dateAvailable = $this->getTrimmedInput('dateAvailable', $_POST);

        $savedListID = $_GET['savedListID'];

        $savedLists = new SavedLists($this->_siteID);

        $listRS = $savedLists->get($savedListID);

        if ($listRS['isDynamic'] == 0)
        {
            // Handle each kind of static list here:
          
            switch($listRS['dataItemType'])
            {
                case DATA_ITEM_CANDIDATE:
                    $dataGridInstance = 'candidates:candidatesSavedListByViewDataGrid';
                    break;

                case DATA_ITEM_COMPANY:
                    $dataGridInstance = 'companies:companiesSavedListByViewDataGrid';
                    break;

                case DATA_ITEM_CONTACT:
                    $dataGridInstance = 'contacts:contactSavedListByViewDataGrid';
                    break;

                case DATA_ITEM_JOBORDER:
                    $dataGridInstance = 'joborders:joborderSavedListByViewDataGrid';
                    break;
            }
        }

        $dataGridProperties = DataGrid::getRecentParamaters($dataGridInstance, $savedListID);

        /* If this is the first time we visited the datagrid this session, the recent paramaters will
         * be empty.  Fill in some default values. */
        if ($dataGridProperties == array())
        {
            $dataGridProperties = array('rangeStart'    => 0,
                                        'maxResults'    => 15,
                                        'filterVisible' => false,
                                        'savedListStatic' => true);
        }

        /* Add an MRU entry. */
        $_SESSION['RESUFLO']->getMRU()->addEntry(
            DATA_ITEM_LIST, $savedListID, $listRS['description']
        );

        $dataGrid = DataGrid::get($dataGridInstance, $dataGridProperties, $savedListID);

        $this->_template->assign('active', $this);
        $this->_template->assign('dataGrid', $dataGrid);
        $this->_template->assign('listRS', $listRS);
        $this->_template->assign('userID', $_SESSION['RESUFLO']->getUserID());

        $this->_template->display('./modules/lists/List.tpl');

    }

    /*
     * Called by handleRequest to process loading the add to list popup window.
     */
    private function quickActionAddToListModal()
    {
        /* Bail out if we don't have a valid type. */
        if (!$this->isRequiredIDValid('dataItemType', $_GET))
        {
            CommonErrors::fatalModal(COMMONERROR_BADINDEX, $this);
            return;
        }

        /* Bail out if we don't have a valid id. */
        if (!$this->isRequiredIDValid('dataItemID', $_GET))
        {
            CommonErrors::fatalModal(COMMONERROR_BADINDEX, $this);
            return;
        }

        $dataItemType = $_GET['dataItemType'];
        $dataItemID = $_GET['dataItemID'];
        $dataItemIDArray = array($dataItemID);

        $savedLists = new SavedLists($this->_siteID);

        $savedListsRS = $savedLists->getAll($dataItemType, STATIC_LISTS);

        $dataItemDesc = TemplateUtility::getDataItemTypeDescription($dataItemType);

        $this->_template->assign('dataItemDesc', $dataItemDesc);
        $this->_template->assign('savedListsRS', $savedListsRS);
        $this->_template->assign('dataItemType', $dataItemType);
        $this->_template->assign('dataItemIDArray', $dataItemIDArray);
        $this->_template->assign('sessionCookie', $_SESSION['RESUFLO']->getCookie());

        $this->_template->display('./modules/lists/QuickActionAddToListModal.tpl');
    }

    /*
     * Called by handleRequest to process loading the add to list popup window from a datagrid.
     */
    private function addToCampaignFromDatagridModal()
    {
        /* Bail out if we don't have a valid type. */
        if (!$this->isRequiredIDValid('dataItemType', $_GET))
        {
            CommonErrors::fatalModal(COMMONERROR_BADINDEX, $this);
            return;
        }

        $dataGrid = DataGrid::getFromRequest();

        $dataItemIDArray = $dataGrid->getExportIDs();

        /* Validate each ID */
        foreach ($dataItemIDArray as $index => $dataItemID)
        {
            if (!$this->isRequiredIDValid($index, $dataItemIDArray))
            {
                CommonErrors::fatalModal(COMMONERROR_BADINDEX, $this, 'Invalid data item ID.');
                return;
            }
        }

        $dataItemType = $_GET['dataItemType'];

        $savedLists = new SavedLists($this->_siteID);

        $savedListsRS = $savedLists->getAll($dataItemType, STATIC_LISTS);

        $userid=$_SESSION['RESUFLO']->getUserID();
        $sql ="SELECT `dripmarket_compaign`.cname, `dripmarket_compaign`.id, `dripmarket_compaign`.cstatus, `candidate_joborder_status`.candidate_joborder_status_id, `dripmarket_compaign`.default,`candidate_joborder_status`.short_description FROM `dripmarket_compaign`
left JOIN `dripmarket_compaignemail` ON `dripmarket_compaign`.id = `dripmarket_compaignemail`.compaignid left join candidate_joborder_status on candidate_joborder_status.candidate_joborder_status_id=`dripmarket_compaign`.cstatus where `dripmarket_compaign`.userid=$userid
GROUP BY `dripmarket_compaign`.id ";
        $resource= mysql_query($sql);
        $campaignarray=array();
        $i=0;
        if((mysql_num_rows($resource))>0)
        {
            while($row = mysql_fetch_array($resource))
            {
                $campaignarray[$i]=$row;
                $i++;
            }
        }

        $dataItemDesc = TemplateUtility::getDataItemTypeDescription($dataItemType);

        $siteID = $this->_siteID;
        $this->_template->assign('userID', $userid);
        $this->_template->assign('siteID', $siteID);
        $this->_template->assign('dataItemDesc', $dataItemDesc);
        $this->_template->assign('savedCampaignsRS', $campaignarray);
        $this->_template->assign('dataItemType', $dataItemType);
        $this->_template->assign('dataItemIDArray', $dataItemIDArray);
        $this->_template->assign('sessionCookie', $_SESSION['RESUFLO']->getCookie());

        $this->_template->display('./modules/lists/QuickActionAddToCampaignModal.tpl');
    }

    /*
     * Called by handleRequest to process loading the add to list popup window from a datagrid.
     */
    private function addToListFromDatagridModal()
    {
        /* Bail out if we don't have a valid type. */
        if (!$this->isRequiredIDValid('dataItemType', $_GET))
        {
            CommonErrors::fatalModal(COMMONERROR_BADINDEX, $this);
            return;
        }

        $dataGrid = DataGrid::getFromRequest();

        $dataItemIDArray = $dataGrid->getExportIDs();

        /* Validate each ID */
        foreach ($dataItemIDArray as $index => $dataItemID)
        {
            if (!$this->isRequiredIDValid($index, $dataItemIDArray))
            {
                CommonErrors::fatalModal(COMMONERROR_BADINDEX, $this, 'Invalid data item ID.');
                return;
            }
        }

        $dataItemType = $_GET['dataItemType'];

        $savedLists = new SavedLists($this->_siteID);

        $savedListsRS = $savedLists->getAll($dataItemType, STATIC_LISTS);

        $dataItemDesc = TemplateUtility::getDataItemTypeDescription($dataItemType);

        $this->_template->assign('dataItemDesc', $dataItemDesc);
        $this->_template->assign('savedListsRS', $savedListsRS);
        $this->_template->assign('dataItemType', $dataItemType);
        $this->_template->assign('dataItemIDArray', $dataItemIDArray);
        $this->_template->assign('sessionCookie', $_SESSION['RESUFLO']->getCookie());

        $this->_template->display('./modules/lists/QuickActionAddToListModal.tpl');
    }

    private function addToListFromPipelineDatagridModal()
    {
        /* Bail out if we don't have a valid type. */
        if (!$this->isRequiredIDValid('dataItemType', $_GET))
        {
            CommonErrors::fatalModal(COMMONERROR_BADINDEX, $this);
            return;
        }

        $dataItemIDArray = unserialize(urldecode($_REQUEST['dynamicArgument']));

        /* Validate each ID */
        foreach ($dataItemIDArray as $index => $dataItemID)
        {
            if (!$this->isRequiredIDValid($index, $dataItemIDArray))
            {
                CommonErrors::fatalModal(COMMONERROR_BADINDEX, $this, 'Invalid data item ID.');
                return;
            }
        }

        $dataItemType = $_GET['dataItemType'];

        $savedLists = new SavedLists($this->_siteID);

        $savedListsRS = $savedLists->getAll($dataItemType, STATIC_LISTS);

        $dataItemDesc = TemplateUtility::getDataItemTypeDescription($dataItemType);

        $this->_template->assign('dataItemDesc', $dataItemDesc);
        $this->_template->assign('savedListsRS', $savedListsRS);
        $this->_template->assign('dataItemType', $dataItemType);
        $this->_template->assign('dataItemIDArray', $dataItemIDArray);
        $this->_template->assign('sessionCookie', $_SESSION['RESUFLO']->getCookie());

        $this->_template->display('./modules/lists/QuickActionAddToListModal.tpl');
    }
    
    /*
     * Called by handleRequest to process the remove items from datagrid popup.
     */
    private function removeFromListDatagrid()
    {
        /* Bail out if we don't have a valid type. */
        if (!$this->isRequiredIDValid('dataItemType', $_GET))
        {
            CommonErrors::fatalModal(COMMONERROR_BADINDEX, $this);
            return;
        }

        $dataGrid = DataGrid::getFromRequest();

        $dataItemIDArray = $dataGrid->getExportIDs();

        /* Validate each ID */
        foreach ($dataItemIDArray as $index => $dataItemID)
        {
            if (!$this->isRequiredIDValid($index, $dataItemIDArray))
            {
                CommonErrors::fatalModal(COMMONERROR_BADINDEX, $this, 'Invalid data item ID.');
                return;
            }
        }

        $dataItemType = $_GET['dataItemType'];

        $dataItemDesc = TemplateUtility::getDataItemTypeDescription($dataItemType);

        if (!$this->isRequiredIDValid('savedListID', $_GET))
        {
            CommonErrors::fatalModal(COMMONERROR_BADINDEX, $this, 'Invalid saved list ID.');
            return;
        }

        $savedListID = $_GET['savedListID'];

        /* Remove the items */
        $savedLists = new SavedLists($this->_siteID);

        $dataItemIDArrayTemp = array();
        foreach ($dataItemIDArray as $dataItemID)
        {
            $dataItemIDArrayTemp[] = $dataItemID;
            /* Because its too slow adding 1 item at a time, we do it in spurts of 200 items. */
            if (count($dataItemIDArrayTemp) > 200)
            {
                $savedLists->removeEntryMany($savedListID, $dataItemIDArrayTemp);
                $dataItemIDArrayTemp = array();
            }
        }
        if (count($dataItemIDArrayTemp) > 0)
        {
            $savedLists->removeEntryMany($savedListID, $dataItemIDArrayTemp);
        }

        /* Redirect to the saved list page we were on. */
        /* FIXME: What if we are on the last page? */
        RESUFLOUtility::transferRelativeURI('m=lists&a=showList&listrec=view_all&savedListID='.$savedListID);
    }

    /*
     * Called by handleRequest to delete a list.
     */
    private function onDeleteStaticList()
    {
        /* Bail out if we don't have a valid type. */
        if (!$this->isRequiredIDValid('savedListID', $_GET))
        {
            CommonErrors::fatalModal(COMMONERROR_BADINDEX, $this);
            return;
        }

        $savedListID = $_GET['savedListID'];

        $savedLists = new SavedLists($this->_siteID);

        /* Write changes. */
        $savedLists->delete($savedListID);


        RESUFLOUtility::transferRelativeURI('m=lists');
    }
}

?>
