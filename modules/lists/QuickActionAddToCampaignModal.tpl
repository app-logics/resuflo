<?php /* $Id: QuickActionAddToCampaignModal.tpl 3198 2015-03-14 jj $ */ ?>
<?php TemplateUtility::printModalHeader('Candidates', array('js/lists.js'), 'Add to a Drip Campaign'); ?>
    <table>
        <tr>
            <td><!--Add to Static Campaigns-->Add selected clients to the following lists:<?php if (count($this->dataItemIDArray) > 1): ?><?php endif; ?></td>
       </tr>
    </table>
            <div class="addToCampaignCampaignBox" id="addToListBox">
                <input type="hidden" style="width:200px;" id="dataItemArray" value="<?php $this->_(implode(',', $this->dataItemIDArray)); ?>">
                <?php foreach($this->savedCampaignsRS as $index => $data): ?>
                    <div class="<?php TemplateUtility::printAlternatingDivClass($index); ?>" id="savedCampaignRow<?php echo($data['id']); ?>">
                        <span style="float:left;">
                            <input type="radio" name="saved_list" id="savedCampaignRowCheck<?php echo($data['id']); ?>">
                            &nbsp;
                            <span id="savedCampaignRowDescriptionArea<?php echo($data['id']); ?>"><?php $this->_($data['cname']); ?></span>
                        </span>
                    </div>
                    <div class="<?php TemplateUtility::printAlternatingDivClass($index); ?>" style="display:none;" id="savedCampaignRowEditing<?php echo($data['id']); ?>">
                        <span style="float:left;">
                            <input class="inputbox" style="width:220px; padding-left:5px; margin-top:2px;" value="<?php $this->_($data['description']); ?>" id="savedCampaignRowInput<?php echo($data['id']); ?>">
                        </span>
                        <span style="float:right; padding-right:25px;">
                            <a href="javascript:void(0);" onclick="deleteCampaignRow(<?php echo($data['id']); ?>, '<?php echo($this->sessionCookie); ?>', <?php echo($data['numberEntries']); ?>);" style="text-decoration:none;"><img src="images/actions/delete.gif" border="0">&nbsp;Delete</a>&nbsp;
                            <a href="javascript:void(0);" onclick="saveCampaignRow(<?php echo($data['id']); ?>, '<?php echo($this->sessionCookie); ?>');" style="text-decoration:none;"><img src="images/actions/screen.gif" border="0">&nbsp;Save</a>
                        </span>
                    </div>
                    <div class="<?php TemplateUtility::printAlternatingDivClass($index); ?>" style="display:none;" id="savedCampaignRowAjaxing<?php echo($data['id']); ?>">
                        <img src="images/indicator.gif">&nbsp;Saving Changes, Please Wait...
                    </div>
                <?php endforeach; ?>
                <div class="<?php TemplateUtility::printAlternatingDivClass(count($this->savedCampaignsRS)); ?>" style="display:none;" id="savedCampaignNew">
                    <span style="float:left;">
                        <input class="inputbox" style="width:220px; padding-left:5px; margin-top:2px;" value="" id="savedCampaignNewInput">
                    </span>
                    <span style="float:right; padding-right:25px;">
                        <a href="javascript:void(0);" onclick="document.getElementById('savedCampaignNew').style.display='none';" style="text-decoration:none;"><img src="images/actions/delete.gif" border="0">&nbsp;Delete</a>&nbsp;
                        <a href="javascript:void(0);" onclick="commitNewCampaign('<?php echo($this->sessionCookie); ?>', <?php echo($this->dataItemType); ?>);" style="text-decoration:none;"><img src="images/actions/screen.gif" border="0">&nbsp;Save</a>
                    </span>
                </div>
                <div class="<?php TemplateUtility::printAlternatingDivClass(count($this->savedCampaignsRS)); ?>" style="display:none;" id="savedCampaignNewAjaxing">
                    <img src="images/indicator.gif">&nbsp;Saving Changes...
                </div>
            </div>
            <br />
            <div style="float:right;" id="actionArea">
                <input type="button" class="button" value="Add To Campaign" onclick="addItemsToCampaign('<?php echo($this->sessionCookie); ?>', <?php echo($this->dataItemType); ?>, '<?php echo($this->siteID); ?>','<?php echo($this->userID); ?>');">&nbsp;
                <input type="button" class="button" value="Cancel" onclick="parentHidePopWin();">&nbsp;
            </div>
            <div style="display:none; font: normal normal normal 12px/130% Arial, Tahoma, sans-serif;" id="addingToListAjaxing">
                <img src="images/indicator.gif">&nbsp;Adding to Campaign, Please Wait <?php if (count($this->dataItemIDArray) > 20): ?>(This could take awhile)<?php endif; ?>...
            </div>
            <div style="display:none; font: normal normal normal 12px/130% Arial, Tahoma, sans-serif;" id="addingToListAjaxingComplete">
                <img src="images/indicator.gif">&nbsp;Items have been added to campaign successfully.
            </div>
            <script type="text/javascript">
                function relabelEvenOdd()
                {
                    var onEven = 1;
                    <?php foreach($this->savedCampaignsRS as $index => $data): ?>
                        if (document.getElementById("savedCampaignRow<?php echo($data['id']); ?>").style.display == '' || 
                            document.getElementById("savedCampaignRowEditing<?php echo($data['id']); ?>").style.display == '' || 
                            document.getElementById("savedCampaignRowAjaxing<?php echo($data['id']); ?>").style.display == '')
                        {
                            if (onEven == 1)
                            {
                                document.getElementById("savedCampaignRow<?php echo($data['id']); ?>").className = 'evenDivRow';
                                document.getElementById("savedCampaignRowEditing<?php echo($data['id']); ?>").className = 'evenDivRow';
                                document.getElementById("savedCampaignRowAjaxing<?php echo($data['id']); ?>").className = 'evenDivRow';
                                onEven = 0;
                            }
                            else
                            {
                                document.getElementById("savedCampaignRow<?php echo($data['id']); ?>").className = 'oddDivRow';
                                document.getElementById("savedCampaignRowEditing<?php echo($data['id']); ?>").className = 'oddDivRow';
                                document.getElementById("savedCampaignRowAjaxing<?php echo($data['id']); ?>").className = 'oddDivRow';
                                onEven = 1;
                            }
                        }
                    <?php endforeach; ?>
                    if (onEven == 1)
                    {
                        document.getElementById("savedCampaignNew").className = 'evenDivRow';
                        document.getElementById("savedCampaignNewAjaxing").className = 'evenDivRow';
                    }
                    else
                    {
                        document.getElementById("savedCampaignNew").className = 'oddDivRow';
                        document.getElementById("savedCampaignNewAjaxing").className = 'oddDivRow';
                    }
                }
                function getCheckedBoxes()
                {
                    var checked='';
                     <?php foreach($this->savedCampaignsRS as $index => $data): ?>
                        if (document.getElementById("savedCampaignRowCheck<?php echo($data['id']); ?>").checked)
                        {
                            checked += "<?php echo($data['id']); ?>,";
                        }
                    <?php endforeach; ?>  
                    return checked;                 
                }
            </script>
    </body>
</html>
