<?php
/*
 * RESUFLO
 * AJAX Add to Campaign Interface
 *
 * Copyright (C) 2005 - 2007 Econn Technologies, Inc.
 *
 *
 * The contents of this file are subject to the RESUFLO Public License
 * Version 1.1a (the "License"); you may not use this file except in
 * compliance with the License. You may obtain a copy of the License at
 * http://www.econnoisseur.com/.
 *
 * Software distributed under the License is distributed on an "AS IS"
 * basis, WITHOUT WARRANTY OF ANY KIND, either express or implied. See the
 * License for the specific language governing rights and limitations
 * under the License.
 *
 * The Original Code is "RESUFLO Standard Edition".
 *
 * The Initial Developer of the Original Code is Econn Technologies, Inc.
 * Portions created by the Initial Developer are Copyright (C) 2005 - 2007
 * (or from the year in which this file was created to the year 2007) by
 * Econn Technologies, Inc. All Rights Reserved.
 *
 *
 * $Id: addToCampaigns.php 3198 2015-03-14 jj $
 */

include_once('./lib/StringUtility.php');
include_once('./lib/ActivityEntries.php');

include_once('./lib/DatabaseConnection.php');
include_once('./config.php');
include_once('/asset/constants.php');

$db = DatabaseConnection::getInstance();
if (isset($_REQUEST['siteID'])){
    $siteID = $_REQUEST['siteID'];
    $userID = $_REQUEST['userID'];
    $db = DatabaseConnection::getInstance($siteID);
}

//20 minutes
set_time_limit(1200);
ini_set ( "display_errors", "1");
function isRequiredValueValid($value)
{
    $value = (string) $value;

    /* Return false if the key is empty, or if the key is zero and
     * zero-values are not allowed.
     */
    if (empty($value) && ($value !== '0' || !$allowZero))
    {
        return false;
    }

    /* -0 should not be allowed. */
    if ($value === '-0')
    {
        return false;
    }

    /* Only allow digits. */
    if (!ctype_digit($value))
    {
        return false;
    }

    return true;
}



$interface = new SecureAJAXInterface();

if (!isset($_REQUEST['campaignsToAdd']))
{
    $interface->outputXMLErrorPage(-1, 'No campaignsToAdd passed.');
    die();
}

if (!isset($_REQUEST['itemsToAdd']))
{
    $interface->outputXMLErrorPage(-1, 'No itemsToAdd passed.');
    die();
}

if (!$interface->isRequiredIDValid('dataItemType'))
{
    $interface->outputXMLErrorPage(-1, 'Invalid saved list type.');
    die();
}

$siteID = $interface->getSiteID();

$campaignsToAdd = explode(',', $_REQUEST['campaignsToAdd']);
$itemsToAdd = explode(',', $_REQUEST['itemsToAdd']);
$dataItemType = $_REQUEST['dataItemType'];

foreach ($campaignsToAdd as $index => $data)
{
    if ($data == '')
    {
        unset($campaignsToAdd[$index]);
    }
    else
    {
        if (isRequiredValueValid($data) == false)
        {
            $interface->outputXMLErrorPage(-1, 'Invalid campaigns value. ('.$data.')');
            die;
        }
    }
}

foreach ($itemsToAdd as $index => $data)
{
    if ($data == '')
    {
        unset($itemsToAdd[$index]);
    }
    else
    {
        if (isRequiredValueValid($data) == false)
        {
            $interface->outputXMLErrorPage(-1, 'Invalid items value.');
            die;
        }
    }
}

/* Write changes. */
foreach ($campaignsToAdd as $campaign_id)
{
    $cid = (integer) $campaign_id;

    foreach ($itemsToAdd as $candidated_id)
    {
        $cand_id = (integer) $candidated_id;
        //$savedLists->addEntryMany($campaign, $dataItemType, $itemsToAddTemp);
        // DO THIS QUERY MANUALLY SINCE WE DON"T HAVE A LIB FOR DRIPS
        if (!empty($cand_id))
        {
            $sql = "update candidate set campaign_id = $cid, date_modified=NOW() where candidate_id = $cand_id and candidate.entered_by in (select user.user_id from user where user_id = $userID or entered_by = $userID)";
            $resource = mysql_query($sql);

            // let's send out the zero day email now
            $sql ="select candidate.email1, candidate.first_name, candidate.phone_home, candidate.date_created,dripmarket_compaign.userid,dripmarket_compaign.cname,dripmarket_configuration.smtphost,dripmarket_configuration.email_type,dripmarket_configuration.smtppassword ,user.first_name as clientname,dripmarket_configuration.smtpport ,dripmarket_configuration.fromname,dripmarket_questionaire.id as quesid ,dripmarket_configuration.fromemail,dripmarket_configuration.bccemail,dripmarket_configuration.smtpuser,candidate.candidate_id,dripmarket_compaignemail.id dce_id, dripmarket_compaignemail.subject,dripmarket_compaignemail.creationdate,dripmarket_compaignemail.hour,dripmarket_compaignemail.min,dripmarket_compaignemail.Message,dripmarket_compaignemail.Timezone,dripmarket_compaignemail.sendemailon,dripmarket_compaign.id ,dripmarket_compaign.userid FROM `candidate` inner join user on user.user_id =candidate.entered_by INNER JOIN dripmarket_compaign ON user.user_id = dripmarket_compaign.userid inner JOIN dripmarket_compaignemail ON dripmarket_compaign.id = dripmarket_compaignemail.compaignid left JOIN dripmarket_questionaire ON dripmarket_questionaire.userid = dripmarket_compaign.userid inner join dripmarket_configuration on dripmarket_compaign.userid=dripmarket_configuration.userid where candidate_id = $cand_id and dripmarket_compaign.id = $cid and candidate.ishot=0 and candidate.unsubscribe=0 and dripmarket_compaignemail.sendemailon=0";

            $resourcedetail = mysql_query($sql);
            
//            require_once($_SERVER['DOCUMENT_ROOT'].'/ResufloDevelopment/asset/constants.php');

            while ($row = mysql_fetch_array($resourcedetail)) {
                $dcee_id = $row['dce_id'];
                $user_id = $row['userid'];

                $dripQuery = 'insert into drip_queue set smtp_error="addToCampaign", scheduledDate = Now(), candidate_id = "'.$cand_id.'", drip_campaign_id = "'.$cid.'", drip_campemail_id = "'.$dcee_id.'", entered_by = "'.$user_id.'", date_created = now(), date_modified = now()';
                mysql_query($dripQuery);
            
//
//                $mail= new PHPMailer();
//                $mail->IsSMTP(); // telling the class to use SMTP
//                $mail->SMTPAuth   = true;                  // enable SMTP authentication
//                $mail->Host       = $row['smtphost'];
//                $mail->Port       =  $row['smtpport'];
//                $mail->Username   =  $row['smtpuser'];   // SMTP account username
//                $mail->Password   = $row['smtppassword'];
//
//                if ($row['email_type']!='none') {
//                    $mail->SMTPSecure =$row['email_type'];
//                }
//                $mail->SetFrom( $row['fromemail'], $row['fromname']);
//                if (!empty($row['bccemail'])) $mail->AddBcc( $row['bccemail'] );
//                $mail->Subject    = $row['subject'];
//                $Subject    = $row['subject'];
//                $bodytext=stripslashes($row['Message']);
//                $questionaireurl='<a href="'.questionaireurl.'?quesid='.$row['quesid'].'">Please click here</a>';
//                $quesidencode=base64_encode($row['quesid']);
//                $cidencode=base64_encode($row['candidate_id']);
//                $candidate_id = $row['candidate_id'];
//                $unsubscribeurl='To remove your name from our candidate list <a href="'.unsubscribeurl.'?quesid='.$quesidencode.'&cid='.$cidencode.'">Please click here</a>';
//                $datefunc=date("d-M-y");
//                $bodytext=str_replace('$$NAME$$',$row['first_name'],$bodytext);
//                $bodytext=str_replace('$$EMAIL$$',$row['email1'],$bodytext);
//                $bodytext=str_replace('$$PHONE$$',$row['phone_home'],$bodytext);
//                $bodytext=str_replace('$$STARTDATE$$',$row['date_created'],$bodytext);
//                $bodytext=str_replace('$$CURDATE$$',$datefunc,$bodytext);
//                $bodytext=str_replace('$$QUESTIONNAIRE$$',$questionaireurl,$bodytext);
//                $bodytext=str_replace('$$UNSUBSCRIBE$$',$unsubscribeurl,$bodytext);
//                $mail->MsgHTML($bodytext);
//
//                // import opt-out trumps all other things!
//                if (Importoptout::is_optout_email($row['email1'])) {
//                    //return 0;
//                    continue;
//                }    
//
//                $mail->AddAddress($row['email1'],$row['first_name']);

//                try {
//                    $mail->Send();
//                    $sql123 = "INSERT INTO activity (data_item_id, data_item_type, joborder_id, entered_by, type, notes, site_id, date_created, date_modified )VALUES ( '$candidate_id', '100', '0', '$enteredBy', '200', '$Subject', '1', NOW(), NOW() )";
//
//                    $db312 = DatabaseConnection::getInstance();
//                    $resourcedetailmn = $db312->query($sql123);
//
//                    // Econn updating candidate table notes starts
//                    $UpdateNotesQuery = mysql_query("UPDATE candidate SET activity_notes = '$Subject' WHERE candidate_id = '$candidate_id'");
//                    // Econn updating candidate table notes ends
//                } catch(Exception $e2) {
//                }

            }
        }
    }
}

$interface->outputXMLPage(
    "<data>\n" .
    "    <errorcode>0</errorcode>\n" .
    "    <errormessage></errormessage>\n" .
    "    <response>success</response>\n" .
    "</data>\n"
);

?>
