<?php /* $Id: Companies.tpl 3460 2007-11-07 03:50:34Z brian $ */ ?>
<?php TemplateUtility::printHeader('Companies', array('js/highlightrows.js', 'js/export.js', 'js/dataGrid.js')); ?>
<?php TemplateUtility::printHeaderBlock(); ?>

<?php TemplateUtility::printTabs($this->active); ?>
    <style type="text/css">
    div.addCompaniesButton { background: #4172E3 url(images/nodata/companiesButton.jpg); cursor: pointer; width: 337px; height: 67px; }
    div.addCompaniesButton:hover { background: #4172E3 url(images/nodata/companiesButton-o.jpg); cursor: pointer; width: 337px; height: 67px; }
    
    .fullwidth
    {
        width:500px;
        float:left;
        margin:10px 0px 0px 0px;
        
    }
     .halfwidth
    {
       width:200px;float:left;text-align:left;
        
    }
     </style>
    <div id="main">
   
        <div id="contents">
            <table width="100%">
                <tr>
                    <td width="3%">
                        <img src="images/companies.gif" width="24" height="24" border="0" alt="Companies" style="margin-top: 3px;" />&nbsp;
                    </td>
                    <td><h2>Companies: Drip Marketting</h2></td>
                    <td align="right" >
                               
                           
                    </td>
                </tr>
                <tr>
                    <td>
                  <div style="width:500px;float:left;text-align:center;margin:50px 0px 0px 0px">
                             
                    <a href="javascript:void(0)" onclick="javascript:goToURL('<?php echo(RESUFLOUtility::getIndexName()); ?>?m=dripMarket&amp;a=showcompaign');" >Compaigns</a>|
  <a href="javascript:void(0)" onclick="javascript:goToURL('<?php echo(RESUFLOUtility::getIndexName()); ?>?m=dripMarket&amp;a=showconfig');" >Configuration</a>|
                           <a href="javascript:void(0)" onclick="javascript:goToURL('<?php echo(RESUFLOUtility::getIndexName()); ?>?m=dripMarket&amp;a=questionnaire');" >Questionnaire</a>
                        </div>
                        
                    </td>
                    
                    
                </tr>
                <tr>
                    
                    
                    <td>
                        <div style="width:500px;float:left;text-align:center;margin:50px 0px 0px 0px">
                                 <div class="fullwidth">
                            
              <div class="halfwidth" >
                             
         
         Time Zone
                        </div>
                     <div class="halfwidth" >
                             <input type="text" name="timezone" />
         
         
                        </div>
                                 </div>
                                 
                                     <div class="fullwidth">
                            
              <div class="halfwidth" >
                             
         
        SMTP Host:
                        </div>
                     <div class="halfwidth" >
                             <input type="text" name="smtphost" />
         
         
                        </div>
                                 </div>    <div class="fullwidth">
                            
              <div class="halfwidth" >
                             
         
    SMTP User:
                        </div>
                     <div class="halfwidth" >
                             <input type="text" name="smtpuser" />
         
         
                        </div>
                                 </div>    <div class="fullwidth">
                            
              <div class="halfwidth" >
                             
  SMTP Password
                        </div>
                     <div class="halfwidth" >
                             <input type="text" name="smtppass" />
         
         
                        </div>
                                 </div>    <div class="fullwidth">
                            
              <div class="halfwidth" >
                             
         
        SMTP Port
                        </div>
                     <div class="halfwidth" >
                             <input type="text" name="smtpport" />
         
         
                        </div>
                                 </div>    <div class="fullwidth">
                            
              <div class="halfwidth" >
                             
         
From Name
                        </div>
                     <div class="halfwidth" >
                             <input type="text" name="fromname" />
         
         
                        </div>
                                 </div>    <div class="fullwidth">
                            
              <div class="halfwidth" >
                             
         
   From Email
                        </div>
                     <div class="halfwidth" >
                             <input type="text" name="fromemail" />
         
         
                        </div>
                                 </div>    <div class="fullwidth">
                            
              <div class="halfwidth" style="height:10px" >
                             
         
       
                        </div>
                     <div class="halfwidth" >
                             <input type="submit" name="sub" value="save"/>
         
         
                        </div>
                                 </div> 
              </div>
                        
                        
                    </td>
                    
                    
                </tr>
            </table>


        

       
        </div>
    </div>
    <div id="bottomShadow"></div>
<?php TemplateUtility::printFooter(); ?>
