<?php /* $Id: Companies.tpl 3460 2007-11-07 03:50:34Z brian $ */ ?>
<?php TemplateUtility::printHeader('Companies', array('js/highlightrows.js', 'js/export.js', 'js/dataGrid.js', 'css/tab.css', 'css/form.css', 'javascript/tab.js')); ?>
<?php TemplateUtility::printHeaderBlock(); ?>

<?php TemplateUtility::printTabs($this->active); ?>

<!-- TinyMCE -->
<!--<script src="jscripts/tinymce/tinymce.min.js" type="text/javascript"></script>-->
<!-- /TinyMCE -->

<div id="main">
    
    <div id="contents">
        
        <!--tabs Starts-->		 
        <ul id="countrytabs" class="shadetabs" style="padding-top:20px;">
            <li><a  href="javascript:void(0)" onclick="javascript:goToURL('<?php echo(RESUFLOUtility::getIndexName()); ?>?m=dripMarket&amp;a=showcompaign');" >Campaigns</a></li>
            <li><a href="javascript:void(0)" onclick="javascript:goToURL('<?php echo(RESUFLOUtility::getIndexName()); ?>?m=dripMarket&amp;a=showconfig');" >Configuration</a></li>
            <li><a class="selected" href="javascript:void(0)" onclick="javascript:goToURL('<?php echo(RESUFLOUtility::getIndexName()); ?>?m=dripMarket&amp;a=questionnaire');" rel="country1">Questionnaire</a></li>
        </ul>
        <div style="border: 0px solid #32004b; width:900px; height:auto; margin-bottom:1em; padding:0px;">
            <div id="country1" class="tabcontent">
                <table width="100%" border="0" cellspacing="0" cellpadding="0" bgcolor="#fff" >
                    <tr>
                        <td colspan="4" bgcolor="#f8f8f8">&nbsp;</td>
                    </tr>
                    <tr>
                        <td colspan="4" bgcolor="#f8f8f8" style="padding-left:10px;"><a href="javascript:void(0)" onclick="javascript:goToURL('<?php echo(RESUFLOUtility::getIndexName()); ?>?m=dripMarket&amp;a=createcampaign');" ></a></td>
                    </tr>
                    <tr>
                        <td colspan="4" bgcolor="#fff" >
                            <form name="frm1" id="frm1" action="<?php echo(RESUFLOUtility::getIndexName()); ?>?m=dripMarket&amp;a=savequestionnaire" method="post" onsubmit="return questionaire()">
                                <table width="100%" border="0" cellspacing="2" cellpadding="2" bgcolor="#f8f8f8";>
                                    <tr>
                                        <td>Logo</td>
                                        <td><img src="<?php echo $this->resourcedetailsresult['logo']; ?>" name="img1" id="img1" style="max-width: 100%;" /></td>
                                    </tr>
                                    <tr>
                                        <td width="130">  <font style="color:red">*</font>Message
                                            
                                            
                                        </td>
                                        <td width="865"  style="background:#ffffff;">
                                            <textarea name="txtmessage" id="txtmessage" class="txtbox" style="height:300px;width:600px"><?php echo $this->resourcedetailsresult['message']; ?></textarea>
                                            
                                            <input name="image" type="file" id="upload" class="hidden" onchange="" style="display: none;">
                                        </td>
                                    </tr>
                                    <!--      <tr>
                                            <td>New Status after completion of questionaire </td>
                                            <td><select name="selectstatus" id="selectstatus" class="txtbox">
                                                   
                                                                  <?php foreach($this->statusarray as $row){ ?>
                                                                <option value="<?php echo $row['candidate_joborder_status_id']; ?>"  <?php if($this->resourcedetailsresult['statuscompletion']==$row['candidate_joborder_status_id']){echo "selected";} ?>  ><?php echo $row['short_description']; ?></option>
                                                              <?php } ?>
                                                             </select></td>
                                          </tr>-->
                                    <tr>
                                        <td><font style="color:red">*</font>Send Email alert to:</td>
                                        <td><label>
                                                <input type="text" name="emailaddress" id="emailaddress" value="<?php echo $this->resourcedetailsresult['sendemailalertto']; ?>" class="txtbox" />
                                            </label>
                                        </td>
                                    </tr>
                                    <tr>
                                        <td>Redirect After Filling Form:</td>
                                        <td><label>
                                                <input type="text" name="redirect_url" id="redirect_url" value="<?php echo $this->resourcedetailsresult['redirect_url']; ?>" class="txtbox" style="width: 500px;" />
                                            </label>
                                        </td>
                                    </tr>
                                    
                                    <tr>
                                        <td>&nbsp;</td>
                                        <td><input type="image" src="images/next1.gif" width="105" height="43" /></td>
                                    </tr>
                                    <tr>
                                        <td>&nbsp;</td>
                                        <td>&nbsp;</td>
                                    </tr>
                                </table>   
                            </form>  
                        </td>
                    </tr>
                    
                </table>
                
            </div>
            
        </div>        
        <script src="jscripts/ckeditor/ckeditor.js" type="text/javascript"></script> 
        <script type="text/javascript">
            CKEDITOR.replace('txtmessage');
        </script>
        <script type="text/javascript">// <![CDATA[
            var countries=new ddtabcontent("countrytabs")
            countries.setpersist(true)
            countries.setselectedClassTarget("link") //"link" or "linkparent"
            countries.init()
            // ]]></script>   
        <!--tabs end-->	
    </div>
</div>
<div id="bottomShadow"></div>
<?php TemplateUtility::printFooter(); ?>
