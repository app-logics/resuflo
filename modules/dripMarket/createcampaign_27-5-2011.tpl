<?php /* $Id: Companies.tpl 3460 2007-11-07 03:50:34Z brian $ */ ?>
<?php TemplateUtility::printHeader('Companies', array('js/highlightrows.js', 'js/export.js', 'js/dataGrid.js')); ?>
<?php TemplateUtility::printHeaderBlock(); ?>

<?php TemplateUtility::printTabs($this->active); ?>

<link rel="stylesheet" href="css/tab.css" />
<link rel="stylesheet" href="css/form.css" />
<script type="text/javascript" src="javascript/tab.js"></script>
<script type="text/javascript" src="javascript/validations.js"></script>
 
    <div id="main">
   
        <div id="contents">
            
			<!--tabs Starts-->		 
<ul id="countrytabs" class="shadetabs" style="padding-top:20px;">
<li><a class="selected" href="javascript:void(0)" onclick="javascript:goToURL('<?php echo(RESUFLOUtility::getIndexName()); ?>?m=dripMarket&amp;a=showcompaign');" rel="country1">Campaigns</a></li>
<li><a href="javascript:void(0)" onclick="javascript:goToURL('<?php echo(RESUFLOUtility::getIndexName()); ?>?m=dripMarket&amp;a=showconfig');" >Configuration</a></li>
<li><a  href="javascript:void(0)" onclick="javascript:goToURL('<?php echo(RESUFLOUtility::getIndexName()); ?>?m=dripMarket&amp;a=questionaire');">Questionaire</a></li>
</ul>
<div style="border: 0px solid #32004b; width:900px; height:auto; margin-bottom:1em; padding:0px;">
<div id="country1" class="tabcontent">
<table width="100%" border="0" cellspacing="0" cellpadding="0" bgcolor="#fff" >
  <tr>
    <td colspan="4" bgcolor="#f8f8f8">&nbsp;</td>
  </tr>
  <tr>
    
    <td colspan="4" bgcolor="#f8f8f8" style="padding-left:10px;"><a href="javascript:void(0)" onclick="javascript:goToURL('<?php echo(RESUFLOUtility::getIndexName()); ?>?m=dripMarket&amp;a=createcampaign');" ></a></td>
    </tr>
  <tr>
    <td colspan="4" bgcolor="#fff" >
	<form name="frm1" id="frm1" action="<?php echo(RESUFLOUtility::getIndexName()); ?>?m=dripMarket&amp;a=savecampaign" method="post" onsubmit="return compaign()"> 	
	<table width="100%" border="0" cellspacing="2" cellpadding="2" bgcolor="#f8f8f8";>
      <tr>
        <td width="29%">Campaign Name </td>
        <td width="71%"><label>
          <input type="text" name="cname" id="cname" value="<?php echo $this->cname; ?>" class="txtbox" />
		   <input type="hidden" name="task" value="<?php echo $this->task; ?>" />
           <input type="hidden" name="editid" value="<?php echo $this->editid; ?>" />
        </label></td>
      </tr>
      <tr>
        <td>Campaign Descrption </td>
        <td><textarea name="txtcampaign" id="txtcampaign" class="txtbox" rows="5"><?php echo trim($this->description); ?></textarea></td>
      </tr>
      <tr>
        <td>Candidate Status </td>
        <td><label>
          <select name="selectstatus" id="selectstatus" class="txtbox">
                                 <option value="-1">select</option>
                              <?php foreach($this->statusarray as $row){ ?>
                            <option value="<?php echo $row['candidate_joborder_status_id']; ?>"  <?php if($row['candidate_joborder_status_id']==$this->cstatus){echo "selected";} ?> ><?php echo $row['short_description']; ?></option>
                          <?php } ?>
                         </select>
        </label></td>
      </tr>
      
      <tr>
        <td>&nbsp;</td>
        <td><input type="image" src="images/save.gif" width="105" height="43" /></td>
      </tr>
      <tr>
        <td>&nbsp;</td>
        <td>&nbsp;</td>
      </tr>
    </table>   
	</form>  
	</td>
  </tr>
    
</table>

</div>

</div>        
<script type="text/javascript">// <![CDATA[
var countries=new ddtabcontent("countrytabs")
countries.setpersist(true)
countries.setselectedClassTarget("link") //"link" or "linkparent"
countries.init()
// ]]></script>   
 <!--tabs end-->	
        </div>
    </div>
    <div id="bottomShadow"></div>
<?php TemplateUtility::printFooter(); ?>
