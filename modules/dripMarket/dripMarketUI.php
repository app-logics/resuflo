<?php

/*
 * RESUFLO
 * XML module
 *
 * Copyright (C) 2005 - 2007 Econn Technologies, Inc.
 *
 *
 * The contents of this file are subject to the RESUFLO Public License
 * Version 1.1a (the "License"); you may not use this file except in
 * compliance with the License. You may obtain a copy of the License at
 * http://www.econnoisseur.com/.
 *
 * Software distributed under the License is distributed on an "AS IS"
 * basis, WITHOUT WARRANTY OF ANY KIND, either express or implied. See the
 * License for the specific language governing rights and limitations
 * under the License.
 *
 * The Original Code is "RESUFLO Standard Edition".
 *
 * The Initial Developer of the Original Code is Econn Technologies, Inc.
 * Portions created by the Initial Developer are Copyright (C) 2005 - 2007
 * (or from the year in which this file was created to the year 2007) by
 * Econn Technologies, Inc. All Rights Reserved.
 *
 *
 * This module builds an XML file containing public job postings. The
 * exported XML data can be used to submit, en masse, all public job
 * postings to job bulletin sites such as Indeed.com.
 *
 *
 * $Id: XmlUI.php 3565 2007-11-12 09:09:22Z will $
 */
//include_once('./lib/dripmarketting.php');
include_once('./lib/ActivityEntries.php');
include_once('./lib/StringUtility.php');
include_once('./lib/DateUtility.php');
include_once('./lib/JobOrders.php');
include_once('./lib/Site.php');
include_once('./lib/XmlJobExport.php');
include_once('./lib/HttpLogger.php');
include_once('./lib/CareerPortal.php');
include_once('./asset/constants.php');

class dripMarketUI extends UserInterface {

    public function __construct() {
        parent::__construct();

        $this->_authenticationRequired = true;
        $this->_moduleDirectory = 'DripMarket';
        $this->_moduleName = 'dripMarket';
        $this->_moduleTabText = 'DripMarket';
    }

    function getdata() {
        $this->_template->assign('active', $this);
        $this->_template->display('./modules/dripMarket/dripMarket.tpl');
    }

    function handleRequest() {
        $action = $this->getAction();

        $userid = $_SESSION['RESUFLO']->getUserID();

        switch ($action) {
            case 'showconfig':
                $this->showconfigpage();
                break;
            case 'saveconfig':
                $this->saveconfigpage();
                break;
            case 'showcompaign':
                $this->showcompaign();
                break;
            case 'compaignemailedit':
                $this->compaignemailedit();
                break;
            case 'compaigntextmessageedit':
                $this->compaigntextmessageedit();
                break;
            case 'deletecompaign':
                $this->deletecompaign();
                break;
            case 'createcampaign':
                $this->createcampaign();
                break;
            case 'compaigndetail':
                $this->compaigndetail();
                break;
            case 'savecampaign':
                $this->savecampaign();
                break;
            case 'saveemail':
                $this->saveemail();
                break;
            case 'savetextmessage':
                $this->savetextmessage();
                break;
            case 'questionnaire':
                $this->questionaire();
                break;
            case 'editcompaign':
                $this->editcompaign();
                break;
            case 'setDefault':
                $this->setDefault();
                break;
            case 'createemail':
                $this->createemail();
                break;
            case 'createtextmessage':
                $this->createtextmessage();
                break;
            case 'savequestionnaire':
                $this->savequestionaire();
                break;
            case 'editques':
                $this->editques();
                break;
            case 'delques':
                $this->delques();
                break;
            case 'savequestions':
                $this->savequestions();
                break;
            case 'compaignemaildelete':
                $this->compaignemaildelete();
                break;
            case 'faileddripreport':
                $this->FailedDripReport();
                break;
            case 'successdripreport':
                $this->SuccessDripReport();
                break;
            case 'inqueuedripreport':
                $this->InQueueDripReport();
                break;
            case 'failedtextreport':
                $this->FailedTextReport();
                break;
            case 'successtextreport':
                $this->SuccessTextReport();
                break;
            case 'inqueuetextreport':
                $this->InQueueTextReport();
                break;
            case 'deleteDrip':
                $this->deleteDrip();
                break;
            case 'resetDrip':
                $this->resetDrip();
                break;
            case 'resendDrip':
                $this->resendDrip();
                break;
            default: $this->_template->assign('active', $this);
                $this->showcompaign();
                break;
        }
    }

    function editques() {
        $userid = $_SESSION['RESUFLO']->getUserID();
        if (isset($_POST['question'])) {
            $question = $_POST['question'];
        }
        if (isset($_POST['questionaireid'])) {
            $questionaireid = $_POST['questionaireid'];
        }
        if (isset($_GET['questionaireid'])) {
            $questionaireid = $_GET['questionaireid'];
        }
        if (isset($_GET['id'])) {

            $quesid = $_GET['id'];
        }

        $sql = "SELECT dripmarket_questable.quesname,id,quesoption FROM `dripmarket_questable` where id=$quesid";
        // $this->_db->makeQueryInteger($activityID),
        $resource = mysql_query($sql);
        while ($row = mysql_fetch_array($resource)) {
            $quesname = $row['quesname'];
            $quesoption = $row['quesoption'];
        }
        $sql = "SELECT dripmarket_questable.quesname,id FROM `dripmarket_questable` where questionaireid=$questionaireid";
        // $this->_db->makeQueryInteger($activityID),

        $resource = mysql_query($sql);
        $campaignarray = array();
        $i = 0;
        while ($row = mysql_fetch_array($resource)) {
            $campaignarray[$i] = $row;
            $i++;
        }
        $this->_template->assign('baseurl', BASEURL);
        $this->_template->assign('taskaction', 'edit');
        $this->_template->assign('task', 'edit');
        $this->_template->assign('quesid', $quesid);
        $this->_template->assign('quesname', $quesname);
        $this->_template->assign('quesoption', $quesoption);
        $this->_template->assign('maxid', $questionaireid);
        $this->_template->assign('compaigndata', $campaignarray);
        $this->_template->assign('active', $this);
        $this->_template->display('./modules/dripMarket/questions.tpl');
    }

    function delques() {
        $userid = $_SESSION['RESUFLO']->getUserID();
        if (isset($_POST['questionaireid'])) {
            $questionaireid = $_POST['questionaireid'];
        }
        if (isset($_GET['questionaireid'])) {
            $questionaireid = $_GET['questionaireid'];
        }
        if (isset($_GET['id'])) {

            $quesid = $_GET['id'];
        }

        $sql = "delete from dripmarket_questable where id=$quesid";
        // $this->_db->makeQueryInteger($activityID),
        $resource = mysql_query($sql);

        $sql = "SELECT dripmarket_questable.quesname,id FROM `dripmarket_questable` where questionaireid=$questionaireid";
        // $this->_db->makeQueryInteger($activityID),

        $resource = mysql_query($sql);
        $campaignarray = array();
        $i = 0;
        while ($row = mysql_fetch_array($resource)) {
            $campaignarray[$i] = $row;
            $i++;
        }
        $quesname = '';
        $this->_template->assign('baseurl', BASEURL);
        $this->_template->assign('task', 'save');
        $this->_template->assign('quesid', $quesid);
        $this->_template->assign('quesname', $quesname);
        $this->_template->assign('maxid', $questionaireid);
        $this->_template->assign('compaigndata', $campaignarray);
        $this->_template->assign('active', $this);
        $this->_template->display('./modules/dripMarket/questions.tpl');
    }

    function savequestionaire() {
        $userid = $_SESSION['RESUFLO']->getUserID();
        if (isset($_POST['txtmessage'])) {
            $txtmessage = $_POST['txtmessage'];
        }
        if (isset($_POST['emailaddress'])) {
            $emailaddress = $_POST['emailaddress'];
        }
        if (isset($_POST['selectstatus'])) {
            $selectstatus = $_POST['selectstatus'];
        }
        if (isset($_POST['redirect_url'])) {
            $redirect_url = $_POST['redirect_url'];
        }
        $userid = $_SESSION['RESUFLO']->getUserID();
        $sql = "SELECT  dripmarket_questionaire.message ,dripmarket_questionaire.id , dripmarket_questionaire.statuscompletion  ,  dripmarket_questionaire.noofquestion  ,  dripmarket_questionaire.sendemailalertto    FROM `dripmarket_questionaire` where `dripmarket_questionaire`.userid=$userid";
        // $this->_db->makeQueryInteger($activityID),
        $resource = mysql_query($sql);
        $resourcedetail = mysql_fetch_assoc($resource);
        if (mysql_num_rows($resource) > 0) {
            $selectstatus = '0';
            if ($emailaddress == '') {
                
            } else {
                $sql = "update `dripmarket_questionaire` set message='$txtmessage' , statuscompletion='$selectstatus' ,sendemailalertto='$emailaddress',redirect_url='$redirect_url' where userid ='$userid'";
                $resource = mysql_query($sql);
            }
            $ids = $resourcedetail['id'];
            // $this->_db->makeQueryInteger($activityID),
        } else {
            $selectstatus = '0';
            $sql = "insert into `dripmarket_questionaire`(message,statuscompletion,  sendemailalertto,userid, redirect_url)values('$txtmessage','$selectstatus','$emailaddress','$userid','$redirect_url')";
            // $this->_db->makeQueryInteger($activityID),
            $resource = mysql_query($sql);
            $sql = "select max(id)as ids from `dripmarket_questionaire`";
            // $this->_db->makeQueryInteger($activityID),
            $resource = mysql_query($sql);
            while ($row = mysql_fetch_array($resource)) {
                $ids = $row['ids'];
            }
        }
        $sql = "SELECT dripmarket_questable.quesname,id FROM `dripmarket_questable` where questionaireid=$ids ORDER BY dripmarket_questable.id asc";
        // $this->_db->makeQueryInteger($activityID),
        $resource = mysql_query($sql);
        $campaignarray = array();
        $i = 0;
        while ($row = mysql_fetch_array($resource)) {
            $campaignarray[$i] = $row;
            $i++;
        }
        $this->_template->assign('baseurl', BASEURL);
        $this->_template->assign('quesname', '');
        $this->_template->assign('compaigndata', $campaignarray);
        $this->_template->assign('task', 'save');
        $this->_template->assign('maxid', $ids);
        $this->_template->assign('active', $this);
        $this->_template->assign('userId', $userid);
        $this->_template->display('./modules/dripMarket/questions.tpl');
    }

    function savequestions() {

        if ($_POST['task'] == "proceed" || isset($_POST['gethtml'])) {
            if (isset($_POST['questionaireid'])) {
                $questionaireid = $_POST['questionaireid'];
                //$questionaireid=base64_encode($questionaireid); 
            }
            if (isset($_POST['facebookurl'])) {
                $facebookurl = $_POST['facebookurl'];
            }
            $userid = $_SESSION['RESUFLO']->getUserID();
            $sql = "SELECT `candidate`.candidate_id, candidate.email1, candidate.first_name, candidate.phone_home, candidate.date_created FROM `candidate` where entered_by=$userid";

            $resourcedetail = mysql_query($sql);
            $sql = "SELECT `dripmarket_questionaire`.message FROM `dripmarket_questionaire` where dripmarket_questionaire.id=$questionaireid";

            $resourcedetails = mysql_query($sql);
            $resourcedetailsresult = mysql_fetch_assoc($resourcedetails);
            $bodytext = $resourcedetailsresult['message'];

            $sql = "SELECT `dripmarket_questable`.questionaireid ,`dripmarket_questionaire`.sendemailalertto,dripmarket_questionaire.message ,dripmarket_questionaire.statuscompletion,dripmarket_questionaire.message  ,user.logo,dripmarket_questable.quesname,dripmarket_questable.quesoption ,user.logo,dripmarket_questable.id as quesid  FROM `dripmarket_questionaire` right join `dripmarket_questable` on `dripmarket_questable`.questionaireid=`dripmarket_questionaire`.id  inner join `user` on `user`.user_id=`dripmarket_questionaire`.userid where `dripmarket_questable`.questionaireid=$questionaireid order by dripmarket_questable.id asc";

            $resourcedetail = mysql_query($sql);
            $htmldata = '<style>
		 .txtbox_big{ border:#00568b 1px solid; background-color:#FFFFFF; width:450px; height:20px;}
		 .txtbox_small{ border:#00568b 1px solid; background-color:#FFFFFF; width:250px; height:20px;}
		 .txtform{font-family:Arial, Helvetica, sans-serif; font-size:12px; color:#333333;}
            </style>';

            $i = 1;
            $k = 1;
            while ($row = mysql_fetch_array($resourcedetail)) {
                if ($k == 1) {
                    $htmldata = $htmldata . '<div class="divmainwidth">';
                    $htmldata = $htmldata . '<form name="frm1" id="frm1" action="' . BASEURL . 'que/quesothersub.php" method="post" > <table width="500" border="0" align="center" cellpadding="2" cellspacing="2" bgcolor="#f8f8f8"><tr><td bgcolor="#f8f8f8"></td>
                    </tr>    <tr><td colspan="2" bgcolor="#f8f8f8" class="txtform">' . $row['message'] . '</td></tr> <tr><td style="height:20px;" bgcolor="#f8f8f8"></td></tr>
                    <tr>    <td bgcolor="#f8f8f8" class="txtform">Please fill the required fields: </td>  </tr>  <tr>    <td bgcolor="#f8f8f8"><table width="90%" border="0" align="center" cellpadding="0" cellspacing="0"><tr><td width="20%" class="txtform"><input type="hidden" name="siteId" id="siteId" value="' . base64_encode($_SESSION['RESUFLO']->getSiteID()) . '"/>First Name <input type="hidden" name="task" id="task" value="save"/><span style="color:#FF0000">*</span></td><td width="40%"><label><input name="cname" type="text" id="cname" class="txtbox_small" /></label></td><td><input type="hidden" name="sendemailalertto" value="' . $row['sendemailalertto'] . '" /><input type="hidden" name="questionaireid" value="' . $row['questionaireid'] . '" /><input type="hidden" name="scomplete" value="' . $row['statuscompletion'] . '" /></td></tr>
                    <tr><td style="height:10px"></td></tr>
                    <tr><td width="20%" class="txtform">Last Name <span style="color:#FF0000">*</span></td><td width="40%"><label><input name="lname" id="lname" type="text" class="txtbox_small" /></label></td></tr>
                    <tr><td style="height:10px"></td></tr>
                    <tr><td width="20%" class="txtform">Phone <span style="color:#FF0000">*</span></td><td width="40%"><label><input name="phone" id="phone" type="text" class="txtbox_small" /></label></td></tr>
                    <tr><td style="height:10px"></td></tr>
                    <tr><td width="20%" class="txtform">Email <input type="hidden" name="task" id="task" value="save"/><span style="color:#FF0000">*</span></td><td width="40%"><label><input name="emailaddress" id="emailaddress" type="text" class="txtbox_small" /></label></td></tr>

                    </table></td></tr>';
                    $k++;
                }
                $htmldata = $htmldata . '
                <tr>
                    <td bgcolor="#f8f8f8" class="txtform" style="width:450px;">
                        Q' . $i . $row['quesname'] . '
                    </td>  
                </tr>
                <tr>
                    <td bgcolor="#f8f8f8" >';
                if (empty($row['quesoption'])) {
                    $htmldata = $htmldata . '<input name="txtans' . $i . '" id="txtans' . $i . '"  type="text" class="txtbox_big" />';
                } else {
                    $options = explode("|||", $row['quesoption']);
                    foreach ($options as $option) {
                        $value = explode("~~~", $option)[0];
                        $label = explode("~~~", $option)[1];
                        $htmldata = $htmldata . '<input name="txtans' . $i . '" id="txtans' . $i . '"  type="radio" class="txtbox_big" /><br><br>';
                    }
                }
                $htmldata = $htmldata . '<input type="hidden" name="quesid' . $i . '" value="' . $row['quesid'] . '" />
                    </td>
                </tr>';
                $i++;
            }
            $no = $i - 1;
            $htmldata = $htmldata . '<tr>
            <td bgcolor="#f8f8f8" align="left"><input type="hidden" name="noofques" value="' . $no . '" />
            <input type="hidden" name="cid" value="" />
            <input name="sub1" type="image" src="' . BASEURL . 'images/save.gif" value="submit" width="105" height="43" /></td>
            </tr></table></form>';

            $this->_template->assign('htmldata', $htmldata);

            $this->_template->assign('active', $this);
            if (isset($_POST['gethtml'])) {
                $this->_template->display('./modules/dripMarket/gethtml.tpl');
            } else {
                $this->_template->display('./modules/dripMarket/proceeded.tpl');
            }
        } else {
            $userid = $_SESSION['RESUFLO']->getUserID();
            $options = "";
            if (isset($_POST['optionValue'])) {
                $optionValues = $_POST['optionValue'];
                $optionLabels = $_POST['optionLabel'];
                $count = 0;
                foreach ($optionValues as $value) {
                    $options = $options . $value . "~~~" . $optionLabels[$count];
                    if ($count + 1 < count($optionValues)) {
                        $options = $options . "|||";
                    }
                    $count++;
                }
            }
            //echo $options;
            //die("end");

            if (isset($_POST['question'])) {

                $question = $_POST['question'];
            }
            if (isset($_POST['questionaireid'])) {


                $questionaireid = $_POST['questionaireid'];
            }
            if (isset($_POST['taskaction'])) {
                $taskaction = $_POST['taskaction'];
            }
            if (isset($_POST['id'])) {

                $quesid = $_POST['id'];
            }

            if ($taskaction == 'edit') {
                $sql = sprintf("update `dripmarket_questable` set quesname ='$question',questionaireid='$questionaireid',quesoption='%s' where id=$quesid", mysql_real_escape_string($options));
                //die($sql);
            } else {
                $sql = sprintf("insert into `dripmarket_questable`(quesname ,questionaireid, quesoption)values('$question','$questionaireid', '%s')", mysql_real_escape_string($options));
                //die($sql);
            }
            // $this->_db->makeQueryInteger($activityID),
            $resource = mysql_query($sql);

            header("location:index.php?m=dripMarket&a=savequestionnaire");

//header("location:index.php?m=dripMarket&a=savequestions");

            $sql = "SELECT dripmarket_questable.quesname,id FROM `dripmarket_questable` where questionaireid=$questionaireid order by id asc";
            // $this->_db->makeQueryInteger($activityID),
            $resource = mysql_query($sql);
            $campaignarray = array();
            $i = 0;
            while ($row = mysql_fetch_array($resource)) {
                $campaignarray[$i] = $row;
                $i++;
            }
            $this->_template->assign('baseurl', BASEURL);
            $this->_template->assign('maxid', $questionaireid);
            $this->_template->assign('compaigndata', $campaignarray);
            $this->_template->assign('active', $this);
            $this->_template->assign('taskaction', 'save');
            $this->_template->assign('quesname', '');
            $this->_template->display('./modules/dripMarket/questions.tpl');
        }
    }

    function compaignemailedit() {
        if (isset($_GET['editid'])) {
            $editid = $_GET['editid'];
        }
        if (isset($_GET['id'])) {
            $cid = $_GET['id'];
        }

        if (isset($_POST['id'])) {
            $cid = $_POST['id'];
        }

        $sql = "SELECT `dripmarket_compaignemail`.sendemailon,`dripmarket_compaignemail`.nameofemail,`dripmarket_compaignemail`.subject,`dripmarket_compaignemail`.creationdate,`dripmarket_compaignemail`.hour,`dripmarket_compaignemail`.min,`dripmarket_compaignemail`.timezone,`dripmarket_compaignemail`.message,`dripmarket_compaignemail`.compaignid,`dripmarket_compaign`.cstatus,`candidate_joborder_status`.short_description FROM `dripmarket_compaign`
  INNER JOIN `dripmarket_compaignemail` ON `dripmarket_compaign`.id = `dripmarket_compaignemail`.compaignid left join candidate_joborder_status on
  `candidate_joborder_status`.candidate_joborder_status_id= `dripmarket_compaign`.cstatus 
   where `dripmarket_compaignemail`.id=$editid
  ";

        // $this->_db->makeQueryInteger($activityID),
        $resource = mysql_query($sql);

        $resourcedetails = mysql_fetch_assoc($resource);

        $mess = stripslashes($resourcedetails['message']);

        $this->_template->assign('resourcedetails', $resourcedetails);
        $this->_template->assign('task', 'edit');
        $this->_template->assign('editid', $editid);
        $this->_template->assign('cid', $cid);
        $this->_template->assign('active', $this);
        $this->_template->display('./modules/dripMarket/createemail.tpl');
    }

    function compaigntextmessageedit() {
        if (isset($_GET['editid'])) {
            $editid = $_GET['editid'];
        }
        if (isset($_GET['id'])) {
            $cid = $_GET['id'];
        }

        if (isset($_POST['id'])) {
            $cid = $_POST['id'];
        }

        $sql = "SELECT `dripmarket_compaignemail`.sendemailon,`dripmarket_compaignemail`.nameofemail,`dripmarket_compaignemail`.subject,`dripmarket_compaignemail`.creationdate,`dripmarket_compaignemail`.hour,`dripmarket_compaignemail`.min,`dripmarket_compaignemail`.timezone,`dripmarket_compaignemail`.message,`dripmarket_compaignemail`.compaignid,`dripmarket_compaign`.cstatus,`candidate_joborder_status`.short_description FROM `dripmarket_compaign`
  INNER JOIN `dripmarket_compaignemail` ON `dripmarket_compaign`.id = `dripmarket_compaignemail`.compaignid left join candidate_joborder_status on
  `candidate_joborder_status`.candidate_joborder_status_id= `dripmarket_compaign`.cstatus 
   where `dripmarket_compaignemail`.id=$editid
  ";

        // $this->_db->makeQueryInteger($activityID),
        $resource = mysql_query($sql);

        $resourcedetails = mysql_fetch_assoc($resource);

        $mess = stripslashes($resourcedetails['message']);

        $this->_template->assign('resourcedetails', $resourcedetails);
        $this->_template->assign('task', 'edit');
        $this->_template->assign('editid', $editid);
        $this->_template->assign('cid', $cid);
        $this->_template->assign('active', $this);
        $this->_template->display('./modules/dripMarket/createtextmessage.tpl');
    }

    function saveconfigpage() {
        $userid = $_SESSION['RESUFLO']->getUserID();
        if (isset($_GET['userID']) && $this->_accessLevel == ACCESS_LEVEL_ROOT) {
            $userid = $_GET['userID'];
        }
        if (isset($_POST['timezone'])) {
            $timezone = $_POST['timezone'];
        }
        //For primary smtp settings
        if (isset($_POST['sendgrid'])) {
            $sendgrid = $_POST['sendgrid'];
        }
        else{
            $sendgrid = 0;
        }
        if (isset($_POST['smtphost'])) {
            $smtphost = $_POST['smtphost'];
        }
        if (isset($_POST['smtpuser'])) {
            $smtpuser = $_POST['smtpuser'];
        }
        if (isset($_POST['smtppass'])) {
            $smtppass = $_POST['smtppass'];
        }
        if (isset($_POST['smtpport'])) {
            $smtpport = $_POST['smtpport'];
        }
        if (isset($_POST['fromname'])) {
            $fromname = $_POST['fromname'];
        }
        if (isset($_POST['fromemail'])) {
            $fromemail = $_POST['fromemail'];
        }
        if (isset($_POST['bccemail'])) {
            $bccemail = $_POST['bccemail'];
        }
        if (isset($_POST['emailtype'])) {
            $emailtype = $_POST['emailtype'];
        }

        //For secondary smtp settings
        if (isset($_POST['sendgrid2'])) {
            $sendgrid2 = $_POST['sendgrid2'];
        }
        else{
            $sendgrid2 = 0;
        }
        if (isset($_POST['smtphost2'])) {
            $smtphost2 = $_POST['smtphost2'];
        }
        if (isset($_POST['smtpuser2'])) {
            $smtpuser2 = $_POST['smtpuser2'];
        }
        if (isset($_POST['smtppass2'])) {
            $smtppass2 = $_POST['smtppass2'];
        }
        if (isset($_POST['smtpport2'])) {
            $smtpport2 = $_POST['smtpport2'];
        }
        if (isset($_POST['fromname2'])) {
            $fromname2 = $_POST['fromname2'];
        }
        if (isset($_POST['fromemail2'])) {
            $fromemail2 = $_POST['fromemail2'];
        }
        if (isset($_POST['bccemail2'])) {
            $bccemail2 = $_POST['bccemail2'];
        }
        if (isset($_POST['emailtype2'])) {
            $emailtype2 = $_POST['emailtype2'];
        }

        $sql = "SELECT  * FROM `dripmarket_configuration` where `dripmarket_configuration`.userid=$userid";
        // $this->_db->makeQueryInteger($activityID),
        $resource = mysql_query($sql);
        $resourcedetailsresult = mysql_fetch_assoc($resource);

        if (mysql_num_rows($resource) > 0) {
            $sql = "update `dripmarket_configuration` set sendgrid=$sendgrid , smtphost='$smtphost' , smtpuser='$smtpuser' ,smtppassword='$smtppass',smtpport='$smtpport', fromname='$fromname' ,fromemail='$fromemail' ,bccemail='$bccemail' ,email_type='$emailtype', sendgrid2=$sendgrid2 , smtphost2='$smtphost2' , smtpuser2='$smtpuser2' ,smtppassword2='$smtppass2',smtpport2='$smtpport2', fromname2='$fromname2' ,fromemail2='$fromemail2' ,bccemail2='$bccemail2' ,email_type2='$emailtype2' where userid ='$userid'";
            // $this->_db->makeQueryInteger($activityID),
            $resource = mysql_query($sql);
        } else {
            $sql = "insert into `dripmarket_configuration`(sendgrid, smtphost, smtpuser, smtppassword, smtpport, fromname, fromemail, bccemail, userid, email_type, timezone, sendgrid2, smtphost2, smtpuser2, smtppassword2, smtpport2, fromname2, fromemail2, bccemail2, email_type2) values ($sendgrid,'$smtphost','$smtpuser','$smtppass','$smtpport','$fromname','$fromemail','$bccemail','$userid','$emailtype', '', $sendgrid2,'$smtphost2','$smtpuser2','$smtppass2','$smtpport2','$fromname2','$fromemail2','$bccemail2','$emailtype2')";
            // $this->_db->makeQueryInteger($activityID),
            $resource = mysql_query($sql);
        }
        $this->_template->assign('active', $this);
        $this->_template->display('./modules/dripMarket/configsubmit.tpl');
    }

    function createemail() {
        if (isset($_GET['cid'])) {
            $cid = $_GET['cid'];
        }
        if (isset($_GET['s'])) {
            $status = $_GET['s'];
        }


        $sql = "SELECT `candidate_joborder_status`.candidate_joborder_status_id   	  ,`candidate_joborder_status`.short_description    FROM `candidate_joborder_status` inner join `dripmarket_compaign` on `dripmarket_compaign`.cstatus=`candidate_joborder_status`.candidate_joborder_status_id     where dripmarket_compaign.id=$cid";
        $resourcedetail = mysql_query($sql);
        $this->_template->assign('editid', '');
        $resourcedetails = mysql_fetch_assoc($resourcedetail);
        $this->_template->assign('resourcedetails', $resourcedetails);
        $this->_template->assign('cid', $cid);
        $this->_template->assign('status', $status);
        $this->_template->assign('task', 'save');
        $this->_template->assign('active', $this);
        $this->_template->display('./modules/dripMarket/createemail.tpl');
    }

    function createtextmessage() {
        if (isset($_GET['cid'])) {
            $cid = $_GET['cid'];
        }
        if (isset($_GET['s'])) {
            $status = $_GET['s'];
        }


        $sql = "SELECT `candidate_joborder_status`.candidate_joborder_status_id   	  ,`candidate_joborder_status`.short_description    FROM `candidate_joborder_status` inner join `dripmarket_compaign` on `dripmarket_compaign`.cstatus=`candidate_joborder_status`.candidate_joborder_status_id     where dripmarket_compaign.id=$cid";
        $resourcedetail = mysql_query($sql);
        $this->_template->assign('editid', '');
        $resourcedetails = mysql_fetch_assoc($resourcedetail);
        $this->_template->assign('resourcedetails', $resourcedetails);
        $this->_template->assign('cid', $cid);
        $this->_template->assign('status', $status);
        $this->_template->assign('task', 'save');
        $this->_template->assign('active', $this);
        $this->_template->display('./modules/dripMarket/createtextmessage.tpl');
    }

    function questionaire() {

        $userid = $_SESSION['RESUFLO']->getUserID();



        $sql = "SELECT  dripmarket_questionaire.message ,user.logo,  dripmarket_questionaire.statuscompletion  ,  dripmarket_questionaire.noofquestion  ,  dripmarket_questionaire.sendemailalertto, dripmarket_questionaire.redirect_url    FROM `dripmarket_questionaire` right join user on user.user_id=`dripmarket_questionaire`.userid  where `dripmarket_questionaire`.userid=$userid";


        // $this->_db->makeQueryInteger($activityID),
        $resource = mysql_query($sql);
        $resourcedetailsresult = mysql_fetch_assoc($resource);



        $this->_template->assign('resourcedetailsresult', $resourcedetailsresult);


        // $this->_db->makeQueryInteger($activityID),



        $this->_template->assign('statusarray', $statusarray);
        $this->_template->assign('logovalue', $logovalue);
        $this->_template->assign('active', $this);
        $this->_template->display('./modules/dripMarket/questionaire.tpl');
    }

    function createcampaign() {
        $this->_template->assign('cname', '');
        $this->_template->assign('cid', '');
        $this->_template->assign('description', '');
        $this->_template->assign('cstatus', '');
        $this->_template->assign('task', 'add');

        $this->_template->assign('editid', '');


        $sql = "SELECT `candidate_joborder_status`.candidate_joborder_status_id   	  ,`candidate_joborder_status`.short_description    FROM `candidate_joborder_status`
	  ";
        $resourcedetail = mysql_query($sql);

        $statusarray = array();
        $i = 0;
        while ($row = mysql_fetch_array($resourcedetail)) {

            $statusarray[$i] = $row;
            $i++;
        }

        // $this->_db->makeQueryInteger($activityID),


        $this->_template->assign('cstatus', '');
        $this->_template->assign('statusarray', $statusarray);
        $this->_template->assign('active', $this);
        $this->_template->display('./modules/dripMarket/createcampaign.tpl');
    }

    function ms_escape_string($data) {
        if ( !isset($data) or empty($data) ) return '';
        if ( is_numeric($data) ) return $data;

        $non_displayables = array(
            '/%0[0-8bcef]/',            // url encoded 00-08, 11, 12, 14, 15
            '/%1[0-9a-f]/',             // url encoded 16-31
            '/[\x00-\x08]/',            // 00-08
            '/\x0b/',                   // 11
            '/\x0c/',                   // 12
            '/[\x0e-\x1f]/'             // 14-31
        );
        foreach ( $non_displayables as $regex )
            $data = preg_replace( $regex, '', $data );
        $data = str_replace("'", "''", $data );
        return $data;
    }
    function savecampaign() {
        $task = '';
        $userid = $_SESSION['RESUFLO']->getUserID();
        if (isset($_POST['cname'])) {

            $cname = $_POST['cname'];
        }
        if (isset($_POST['editid'])) {
            $editid = $_POST['editid'];
        }
        if (isset($_POST['txtcampaign'])) {
            $txtcampaign = $_POST['txtcampaign'];
        }
        if (isset($_POST['selectstatus'])) {
            $selectstatus = $_POST['selectstatus'];
        }
        if (isset($_POST['task'])) {
            $task = $_POST['task'];
        }
        $error = 'hello';

        if ($task == 'edit') {
            $selectstatus = 0;
            $sql = "update `dripmarket_compaign` set cname='".mysql_real_escape_string($cname)."',description='".mysql_real_escape_string($txtcampaign)."',  cstatus='$selectstatus',userid='$userid' where id=$editid";
            $resource = mysql_query($sql);
            header("location:index.php?m=dripMarket");
        } else {
            $sql = "SELECT `dripmarket_compaign`.cname,`dripmarket_compaign`.id FROM `dripmarket_compaign` where `dripmarket_compaign`.userid='$userid' and cname='".mysql_real_escape_string($cname)."'";

            $resource = mysql_query($sql);
            if (mysql_num_rows($resource) > 0) {
                $error = 'Campaign exists';
                $this->_template->assign('error', $error);
                $this->_template->display('./modules/dripMarket/createcampaign.tpl');
                //   $this->showcompaign();
            } else {
                $selectstatus = 0;
                $sql = "insert into `dripmarket_compaign`(cname,description,  cstatus,userid)values('".mysql_real_escape_string($cname)."','".mysql_real_escape_string($txtcampaign)."','".mysql_real_escape_string($selectstatus)."','$userid')";
                $error = $sql;
                $resource = mysql_query($sql);
                header("location:index.php?m=dripMarket");
            }
        }


        // $this->_db->makeQueryInteger($activityID),
    }

    function saveemail() {
        if (isset($_POST['sendemailon'])) {
            $sendemailon = $_POST['sendemailon'];
        }
        if (isset($_POST['selecthour'])) {
            $selecthour = $_POST['selecthour'];
        }
        if (isset($_POST['selectmin'])) {
            $selectmin = $_POST['selectmin'];
        }
        if (isset($_POST['selecttimezone'])) {
            $selecttimezone = $_POST['selecttimezone'];
        }
        if (isset($_POST['subject'])) {
            $subject = $_POST['subject'];
        }
        if (isset($_POST['name'])) {
            $name = $_POST['name'];
        }
        if (isset($_POST['txtmessage'])) {
            $txtmessage = $_POST['txtmessage'];
        }
        if (isset($_POST['cid'])) {
            $cid = $_POST['cid'];
        }
        if (isset($_POST['cid'])) {
            $cid = $_POST['cid'];
        }
        if (isset($_POST['status'])) {
            $status = $_POST['status'];
        }
        if (isset($_POST['editid'])) {
            $editid = $_POST['editid'];
        }
        if (isset($_POST['task'])) {
            $task = $_POST['task'];
        }

        //$date=date('y-m-d');

        $txtmessagestripped = addslashes($txtmessage);


        if ($task == 'edit') {
            $sql = "update `dripmarket_compaignemail` set nameofemail='".mysql_real_escape_string($name)."',subject='".mysql_real_escape_string($subject)."',sendemailon='$sendemailon',  hour='$selecthour',  min='$selectmin',   Timezone='$selecttimezone',  Message='".mysql_real_escape_string($txtmessagestripped)."'  where id=$editid";
        } else {

            $sql = "insert into `dripmarket_compaignemail`(nameofemail,subject,sendemailon,creationdate,hour,min, Timezone,Message,compaignid)values('".mysql_real_escape_string($name)."','".mysql_real_escape_string($subject)."','$sendemailon',now(),'$selecthour','$selectmin','$selecttimezone','".mysql_real_escape_string($txtmessagestripped)."','$cid')";
        }



        // $this->_db->makeQueryInteger($activityID),
        $resource = mysql_query($sql);

        $sql = "SELECT `candidate_joborder_status`.candidate_joborder_status_id   	  ,`candidate_joborder_status`.short_description    FROM `candidate_joborder_status` inner join `dripmarket_compaign` on `dripmarket_compaign`.cstatus=`candidate_joborder_status`.candidate_joborder_status_id     where dripmarket_compaign.id=$cid";
        $resourcedetail = mysql_query($sql);
        $resourcedetails = mysql_fetch_assoc($resourcedetail);
        $this->_template->assign('resourcedetails', $resourcedetails);


        $this->_template->assign('active', $this);
        $this->_template->assign('status', $status);
        $this->_template->assign('cid', $cid);
        $this->compaigndetail();
        // $this->_template->display('./modules/dripMarket/createemail.tpl');
    }

    function savetextmessage() {

        if (isset($_POST['sendemailon'])) {
            $sendemailon = $_POST['sendemailon'];
        }
        if (isset($_POST['selecthour'])) {
            $selecthour = $_POST['selecthour'];
        }
        if (isset($_POST['selectmin'])) {
            $selectmin = $_POST['selectmin'];
        }
        if (isset($_POST['selecttimezone'])) {
            $selecttimezone = $_POST['selecttimezone'];
        }
        if (isset($_POST['name'])) {
            $name = $_POST['name'];
        }
        if (isset($_POST['txtmessage'])) {
            $txtmessage = $_POST['txtmessage'];
        }
        if (isset($_POST['cid'])) {
            $cid = $_POST['cid'];
        }
        if (isset($_POST['cid'])) {
            $cid = $_POST['cid'];
        }
        if (isset($_POST['status'])) {
            $status = $_POST['status'];
        }
        if (isset($_POST['editid'])) {
            $editid = $_POST['editid'];
        }
        if (isset($_POST['task'])) {
            $task = $_POST['task'];
        }


        //$sql ="SELECT count(*) FROM dripmarket_compaignemail as cm where cm.compaignid=$cid and isTextMessage = 1";

        $sql = "SELECT count(*) as total FROM `dripmarket_compaignemail` "
                . "left join `dripmarket_compaign` on `dripmarket_compaign`.id = `dripmarket_compaignemail`.compaignid where dripmarket_compaign.id=$cid and dripmarket_compaignemail.isTextMessage = 1";
        $resourcedetail = mysql_query($sql);
        $resourcedetails = mysql_fetch_array($resourcedetail);


        //$date=date('y-m-d');

        $txtmessagestripped = addslashes($txtmessage);


        if ($task == 'edit') {
            $isTextMessage = 1; // 1 = True
            $subject = '';
            $sql = "update `dripmarket_compaignemail` set nameofemail='".mysql_real_escape_string($name)."',subject='".mysql_real_escape_string($subject)."',sendemailon='$sendemailon',  hour='$selecthour',  min='$selectmin',   Timezone='$selecttimezone',  Message='".mysql_real_escape_string($txtmessagestripped)."', isTextMessage='".mysql_real_escape_string($isTextMessage)."'  where id=$editid";
        } else {
            if ($resourcedetails['total'] < '1') {
                $isTextMessage = 1; // 1 = True
                $subject = '';
                $sql = "insert into `dripmarket_compaignemail`(nameofemail,subject,sendemailon,creationdate,hour,min, Timezone,Message,compaignid,isTextMessage)values('".mysql_real_escape_string($name)."','".mysql_real_escape_string($subject)."','$sendemailon',now(),'$selecthour','$selectmin','$selecttimezone','".mysql_real_escape_string($txtmessagestripped)."','$cid','".mysql_real_escape_string($isTextMessage)."')";
            } else {
                die('Sorry, You are not allowed to add more than one text message!');
            }
        }



        // $this->_db->makeQueryInteger($activityID),
        $resource = mysql_query($sql);

        $sql = "SELECT `candidate_joborder_status`.candidate_joborder_status_id   	  ,`candidate_joborder_status`.short_description    FROM `candidate_joborder_status` inner join `dripmarket_compaign` on `dripmarket_compaign`.cstatus=`candidate_joborder_status`.candidate_joborder_status_id     where dripmarket_compaign.id=$cid";
        $resourcedetail = mysql_query($sql);
        $resourcedetails = mysql_fetch_assoc($resourcedetail);
        $this->_template->assign('resourcedetails', $resourcedetails);


        $this->_template->assign('active', $this);
        $this->_template->assign('status', $status);
        $this->_template->assign('cid', $cid);
        $this->compaigndetail();
        // $this->_template->display('./modules/dripMarket/createemail.tpl');
    }

    function compaigndetail() {
        // Getting User Detail
        $users = new Users($this->_siteID);
        $userId = $_SESSION['RESUFLO']->getUserID();
        $data = $users->get($userId);

        if (isset($_GET['id'])) {
            $cid = $_GET['id'];
        }

        $sql = "SELECT `dripmarket_compaign`.cname,`dripmarket_compaign`.cstatus ,`dripmarket_compaign`.id  FROM `dripmarket_compaign`
	 where `dripmarket_compaign`.id=$cid";

        // $this->_db->makeQueryInteger($activityID),
        $resourcedetail = mysql_query($sql);
        $resourcedetails = mysql_fetch_assoc($resourcedetail);

        $sql = "SELECT `dripmarket_compaignemail`.sendemailon,`dripmarket_compaign`.cname, `dripmarket_compaignemail`.isTextMessage,`dripmarket_compaignemail`.nameofemail,`dripmarket_compaignemail`.subject,`dripmarket_compaign`.cstatus ,`dripmarket_compaign`.id,`dripmarket_compaignemail`.id as eid, `dripmarket_compaignemail`.isTextMessage FROM `dripmarket_compaign`
	INNER JOIN `dripmarket_compaignemail` ON `dripmarket_compaign`.id = `dripmarket_compaignemail`.compaignid
	 where `dripmarket_compaignemail`.compaignid=$cid
	";

        // $this->_db->makeQueryInteger($activityID),
        $resource = mysql_query($sql);
        $campaignarray = array();
        $i = 0;
        while ($row = mysql_fetch_array($resource)) {

            $campaignarray[$i] = $row;
            $i++;
        }


        $this->_template->assign('cname', $resourcedetails['cname']);
        $this->_template->assign('cstatus', $resourcedetails['cstatus']);
        $this->_template->assign('cid', $resourcedetails['id']);



        $this->_template->assign('isTextMessageEnable', $data['isTextMessageEnable']);
        $this->_template->assign('compaigndata', $campaignarray);
        $this->_template->assign('active', $this);
        $this->_template->display('./modules/dripMarket/compaigndetail.tpl');
    }

    function compaignemaildelete() {

        if (isset($_GET['id'])) {
            $cid = $_GET['id'];
        }

        if (isset($_GET['delid'])) {
            $delid = $_GET['delid'];
        }

        $sql = "delete from `dripmarket_compaignemail` where `dripmarket_compaignemail`.id=$delid
";
// $this->_db->makeQueryInteger($activityID),
        $resource = mysql_query($sql);

        $sql = "SELECT `dripmarket_compaign`.cname,`dripmarket_compaign`.cstatus ,`dripmarket_compaign`.id  FROM `dripmarket_compaign`
 where `dripmarket_compaign`.id=$cid
";

        // $this->_db->makeQueryInteger($activityID),
        $resourcedetail = mysql_query($sql);
        $resourcedetails = mysql_fetch_assoc($resourcedetail);

        $sql = "SELECT `dripmarket_compaignemail`.sendemailon,`dripmarket_compaign`.cname,`dripmarket_compaignemail`.nameofemail,`dripmarket_compaignemail`.subject,`dripmarket_compaign`.cstatus ,`dripmarket_compaign`.id  ,`dripmarket_compaignemail`.id as eid FROM `dripmarket_compaign`
INNER JOIN `dripmarket_compaignemail` ON `dripmarket_compaign`.id = `dripmarket_compaignemail`.compaignid
 where `dripmarket_compaignemail`.compaignid=$cid
";

        // $this->_db->makeQueryInteger($activityID),
        $resource = mysql_query($sql);
        $campaignarray = array();
        $i = 0;
        while ($row = mysql_fetch_array($resource)) {

            $campaignarray[$i] = $row;
            $i++;
        }
        $this->_template->assign('cname', $resourcedetails['cname']);
        $this->_template->assign('cstatus', $resourcedetails['cstatus']);
        $this->_template->assign('cid', $resourcedetails['id']);




        $this->_template->assign('compaigndata', $campaignarray);
        $this->_template->assign('active', $this);

        $this->_template->display('./modules/dripMarket/compaigndetail.tpl');
    }

    function showcompaign() {
        $userid = $_SESSION['RESUFLO']->getUserID();

        $sql = "SELECT count(`dripmarket_compaignemail`.id) AS noofemails, `dripmarket_compaign`.cname,`dripmarket_compaign`.id,  `dripmarket_compaign`.cstatus ,`candidate_joborder_status`.candidate_joborder_status_id   	  ,`dripmarket_compaign`.default,`candidate_joborder_status`.short_description  FROM `dripmarket_compaign`
left JOIN `dripmarket_compaignemail` ON `dripmarket_compaign`.id = `dripmarket_compaignemail`.compaignid left join candidate_joborder_status on candidate_joborder_status.candidate_joborder_status_id=`dripmarket_compaign`.cstatus where `dripmarket_compaign`.userid=$userid
GROUP BY `dripmarket_compaign`.id ";


        // $this->_db->makeQueryInteger($activityID),
        $resource = mysql_query($sql);
        $campaignarray = array();
        $i = 0;

        if ((mysql_num_rows($resource)) > 0) {
            while ($row = mysql_fetch_array($resource)) {


                $campaignarray[$i] = $row;

                $i++;
            }
        }
        $this->_template->assign('compaigndata', $campaignarray);
        $this->_template->assign('active', $this);
        $this->_template->display('./modules/dripMarket/compaign.tpl');
    }

    function failedDripReport() {
        $sql = "SELECT 
	COUNT(*) error_count, DATE(dq.date_modified) error_date, dq.*, u.user_name 
        FROM 
                drip_queue dq LEFT JOIN 
                user u ON dq.entered_by = u.user_id LEFT JOIN 
                dripmarket_compaignemail de on dq.drip_campemail_id = de.id
        WHERE sent = 3 AND (de.isTextMessage is null or de.isTextMessage = 0) AND date_modified >  DATE_SUB(CURDATE(), INTERVAL 14 DAY) 
        GROUP BY dq.entered_by, DATE(dq.date_modified) 
        ORDER BY dq.date_modified DESC";

        $resource = mysql_query($sql);
        $dripReport = array();
        $i = 0;

        if ((mysql_num_rows($resource)) > 0) {
            while ($row = mysql_fetch_array($resource)) {
                $dripReport[$i] = $row;
                $i++;
            }
        }
        $this->_template->assign('dripReport', $dripReport);
        $this->_template->assign('active', $this);
        $this->_template->display('./modules/dripMarket/failedDripReport.tpl');
    }

    function InQueueDripReport() {
        $sql = "SELECT 
	COUNT(*) success_count, DATE(dq.date_modified) success_date, dq.*, u.user_name 
        FROM 
                drip_queue dq LEFT JOIN 
                candidate c on dq.candidate_id = c.candidate_id left join
                user u ON dq.entered_by = u.user_id LEFT JOIN 
                dripmarket_compaignemail de on dq.drip_campemail_id = de.id 
        WHERE sent = 0 AND c.unsubscribe = 0 AND (de.isTextMessage is null or de.isTextMessage = 0) AND date_modified >  DATE_SUB(CURDATE(), INTERVAL 7 DAY) 
        GROUP BY dq.entered_by, DATE(dq.date_modified) 
        ORDER BY dq.date_modified DESC";

        $resource = mysql_query($sql);
        $dripReport = array();
        $i = 0;

        if ((mysql_num_rows($resource)) > 0) {
            while ($row = mysql_fetch_array($resource)) {
                $dripReport[$i] = $row;
                $i++;
            }
        }
        $this->_template->assign('dripReport', $dripReport);
        $this->_template->assign('active', $this);
        $this->_template->display('./modules/dripMarket/inQueueDripReport.tpl');
    }

    function successDripReport() {
        $sql = "SELECT 
	COUNT(*) success_count, DATE(dq.date_modified) success_date, dq.*, u.user_name , smtp_rotation
        FROM 
                drip_queue dq LEFT JOIN 
                user u ON dq.entered_by = u.user_id LEFT JOIN 
                dripmarket_compaignemail de on dq.drip_campemail_id = de.id 
        WHERE sent = 1 AND (de.isTextMessage is null or de.isTextMessage = 0) AND date_modified >  DATE_SUB(CURDATE(), INTERVAL 7 DAY) 
        GROUP BY dq.entered_by, DATE(dq.date_modified), smtp_rotation
        ORDER BY dq.date_modified DESC";

        $resource = mysql_query($sql);
        $dripReport = array();
        $i = 0;

        if ((mysql_num_rows($resource)) > 0) {
            while ($row = mysql_fetch_array($resource)) {
                $dripReport[$i] = $row;
                $i++;
            }
        }
        $this->_template->assign('dripReport', $dripReport);
        $this->_template->assign('active', $this);
        $this->_template->display('./modules/dripMarket/successDripReport.tpl');
    }

    function failedTextReport() {
        $sql = "SELECT 
	COUNT(*) error_count, DATE(dq.date_modified) error_date, dq.*, u.user_name 
        FROM 
                drip_queue dq LEFT JOIN 
                user u ON dq.entered_by = u.user_id LEFT JOIN 
                dripmarket_compaignemail de on dq.drip_campemail_id = de.id
        WHERE sent = 3 AND (de.isTextMessage = 1) AND date_modified >  DATE_SUB(CURDATE(), INTERVAL 14 DAY) 
        GROUP BY dq.entered_by, DATE(dq.date_modified) 
        ORDER BY dq.date_modified DESC";

        $resource = mysql_query($sql);
        $dripReport = array();
        $i = 0;

        if ((mysql_num_rows($resource)) > 0) {
            while ($row = mysql_fetch_array($resource)) {
                $dripReport[$i] = $row;
                $i++;
            }
        }
        $this->_template->assign('dripReport', $dripReport);
        $this->_template->assign('active', $this);
        $this->_template->display('./modules/dripMarket/failedTextReport.tpl');
    }

    function InQueueTextReport() {
        $sql = "SELECT 
	COUNT(*) success_count, DATE(dq.date_modified) success_date, dq.*, u.user_name 
        FROM 
                drip_queue dq LEFT JOIN 
                candidate c on dq.candidate_id = c.candidate_id left join
                user u ON dq.entered_by = u.user_id LEFT JOIN 
                dripmarket_compaignemail de on dq.drip_campemail_id = de.id 
        WHERE sent = 0 AND c.unsubscribe = 0 AND (de.isTextMessage = 1) AND date_modified >  DATE_SUB(CURDATE(), INTERVAL 7 DAY) 
        GROUP BY dq.entered_by, DATE(dq.date_modified) 
        ORDER BY dq.date_modified DESC";

        $resource = mysql_query($sql);
        $dripReport = array();
        $i = 0;

        if ((mysql_num_rows($resource)) > 0) {
            while ($row = mysql_fetch_array($resource)) {
                $dripReport[$i] = $row;
                $i++;
            }
        }
        $this->_template->assign('dripReport', $dripReport);
        $this->_template->assign('active', $this);
        $this->_template->display('./modules/dripMarket/inQueueTextReport.tpl');
    }

    function successTextReport() {
        $sql = "SELECT 
	COUNT(*) success_count, DATE(dq.date_modified) success_date, dq.*, u.user_name , smtp_rotation
        FROM 
                drip_queue dq LEFT JOIN 
                user u ON dq.entered_by = u.user_id LEFT JOIN 
                dripmarket_compaignemail de on dq.drip_campemail_id = de.id 
        WHERE sent = 1 AND (de.isTextMessage = 1) AND date_modified >  DATE_SUB(CURDATE(), INTERVAL 7 DAY) 
        GROUP BY dq.entered_by, DATE(dq.date_modified), smtp_rotation
        ORDER BY dq.date_modified DESC";

        $resource = mysql_query($sql);
        $dripReport = array();
        $i = 0;

        if ((mysql_num_rows($resource)) > 0) {
            while ($row = mysql_fetch_array($resource)) {
                $dripReport[$i] = $row;
                $i++;
            }
        }
        $this->_template->assign('dripReport', $dripReport);
        $this->_template->assign('active', $this);
        $this->_template->display('./modules/dripMarket/successTextReport.tpl');
    }

    function resetDrip() {
        if ($this->_accessLevel != ACCESS_LEVEL_ROOT) {
            die("Don't have access");
        }
        $userId = $_GET['user_id'];
        $errorDate = $_GET['error_date'];

        $sql = "update drip_queue set sent = 0 where entered_by = $userId and sent = 3 and date(date_modified) = '$errorDate'";
        $resource = mysql_query($sql);
        $redirectUrl = "Location: " . RESUFLOUtility::getIndexName() . "?m=dripMarket&a=faileddripreport";
        header($redirectUrl);
    }

    function resendDrip() {
        if ($this->_accessLevel != ACCESS_LEVEL_ROOT) {
            die("Don't have access");
        }
        $userId = $_GET['user_id'];
        $errorDate = $_GET['success_date'];

        $sql = "update drip_queue set sent = 0 where entered_by = $userId and sent = 1 and date(date_modified) = '$errorDate'";
        $resource = mysql_query($sql);
        $redirectUrl = "Location: " . RESUFLOUtility::getIndexName() . "?m=dripMarket&a=successdripreport";
        header($redirectUrl);
    }

    function deleteDrip() {
        if ($this->_accessLevel != ACCESS_LEVEL_ROOT) {
            die("Don't have access");
        }
        $userId = $_GET['user_id'];
        $errorDate = $_GET['success_date'];

        $sql = "delete from drip_queue where entered_by = $userId and sent = 0 and date(date_modified) = '$errorDate'";
        $resource = mysql_query($sql);
        $redirectUrl = "Location: " . RESUFLOUtility::getIndexName() . "?m=dripMarket&a=inqueuedripreport";
        header($redirectUrl);
    }

    function editcompaign() {
        if (isset($_GET['editid'])) {
            $editid = $_GET['editid'];
        }

        $sql = "SELECT `dripmarket_compaign`.cname,`dripmarket_compaign`.description,`dripmarket_compaign`.id,  `dripmarket_compaign`.cstatus FROM `dripmarket_compaign`  where dripmarket_compaign.id=$editid";
        // $this->_db->makeQueryInteger($activityID),
        $resource = mysql_query($sql);
        $resourcedetails = mysql_fetch_assoc($resource);




        $sql = "SELECT `candidate_joborder_status`.candidate_joborder_status_id   	  ,`candidate_joborder_status`.short_description    FROM `candidate_joborder_status`
 ";
        $resourcedetail = mysql_query($sql);

        $statusarray = array();
        $i = 0;
        while ($row = mysql_fetch_array($resourcedetail)) {

            $statusarray[$i] = $row;
            $i++;
        }

        // $this->_db->makeQueryInteger($activityID),



        $this->_template->assign('statusarray', $statusarray);


        $this->_template->assign('editid', $editid);
        $this->_template->assign('cname', $resourcedetails['cname']);
        $this->_template->assign('cid', $resourcedetails['id']);
        $this->_template->assign('description', $resourcedetails['description']);
        $this->_template->assign('cstatus', $resourcedetails['cstatus']);
        $this->_template->assign('task', 'edit');

        $this->_template->assign('active', $this);
        $this->_template->display('./modules/dripMarket/editcampaign.tpl');
    }

    function deletecompaign() {

        if (isset($_GET['delid'])) {
            $delid = $_GET['delid'];
        }
        $userid = $_SESSION['RESUFLO']->getUserID();

        $sql = "delete from `dripmarket_compaign` where `dripmarket_compaign`.id =$delid";
        // $this->_db->makeQueryInteger($activityID),
        $resource = mysql_query($sql);


        $sql = "SELECT count(`dripmarket_compaignemail`.id) AS noofemails, `dripmarket_compaign`.cname,`dripmarket_compaign`.id,  `dripmarket_compaign`.cstatus ,`dripmarket_compaign`.default,`candidate_joborder_status`.candidate_joborder_status_id   	  ,`candidate_joborder_status`.short_description  FROM `dripmarket_compaign`
left JOIN `dripmarket_compaignemail` ON `dripmarket_compaign`.id = `dripmarket_compaignemail`.compaignid left join candidate_joborder_status on candidate_joborder_status.candidate_joborder_status_id=`dripmarket_compaign`.cstatus where `dripmarket_compaign`.userid=$userid
GROUP BY `dripmarket_compaign`.id ";
        // $this->_db->makeQueryInteger($activityID),
        $resource = mysql_query($sql);
        $campaignarray = array();
        $i = 0;
        while ($row = mysql_fetch_array($resource)) {

            $campaignarray[$i] = $row;
            $i++;
        }



        $this->_template->assign('compaigndata', $campaignarray);
        $this->_template->assign('active', $this);
        $this->_template->display('./modules/dripMarket/compaign.tpl');
    }

    function setDefault() {

        if (isset($_GET['campid'])) {
            $campid = $_GET['campid'];
        }
        $userid = $_SESSION['RESUFLO']->getUserID();
        $sql = "UPDATE dripmarket_compaign SET `default` = '0' WHERE userid = '$userid'";
        $resource = mysql_query($sql);
        $sql = "UPDATE dripmarket_compaign SET `default` = '1' where id = '$campid'";
        $resource = mysql_query($sql);
        header("Location:index.php?m=dripMarket");
    }

    function postRequest($url, $params) {
        $query = http_build_query($params);
        //var_dump($query);
        //$query = 'client_id=804494835721-ts28mm1cnt6u3glpd88et7liqu5uaov1.apps.googleusercontent.com&client_secret=-aEkJunJuBUkVc2NkqCI-Fts&grant_type=authorization_code' . '&code=' . $code . '&redirect_uri=' . urlencode('http://localhost:8080/Resuflo/index.php?m=dripMarket&a=showconfig&smtp=1');
        //var_dump($query);
        $ch = curl_init();
        curl_setopt($ch, CURLOPT_RETURNTRANSFER, true);
        curl_setopt($ch, CURLOPT_HEADER, false);
        curl_setopt($ch, CURLOPT_URL, $url);
        curl_setopt($ch, CURLOPT_POST, true);
        curl_setopt($ch, CURLOPT_POSTFIELDS, $query);
        $response = curl_exec($ch);
//        //var_dump($response);
//        $responseJson = json_decode($response, true);
//        //var_dump($responseJson);
//        curl_close($ch);
//        return $responseJson;
        return $response;
    }
    
    function SendEmail($AccessToken, $base64String){
        $curl = curl_init();
        curl_setopt_array($curl, array(
          CURLOPT_URL => "https://www.googleapis.com/gmail/v1/users/me/messages/send?key=804494835721-ts28mm1cnt6u3glpd88et7liqu5uaov1.apps.googleusercontent.com",
          CURLOPT_RETURNTRANSFER => true,
          CURLOPT_ENCODING => "",
          CURLOPT_MAXREDIRS => 10,
          CURLOPT_TIMEOUT => 30,
          CURLOPT_HTTP_VERSION => CURL_HTTP_VERSION_1_1,
          CURLOPT_CUSTOMREQUEST => "POST",
          CURLOPT_POSTFIELDS => "{\"raw\":\"".$base64String."\"}",
          CURLOPT_HTTPHEADER => array(
            "Authorization: Bearer ".$AccessToken,
            "Cache-Control: no-cache",
            "Content-Type: application/json"
          ),
        ));
        $response = curl_exec($curl);
        $responseJson = json_decode($response, true);
        curl_close($curl);
        return $responseJson;
    }

    function showconfigpage() 
    {
        $userid = $_SESSION['RESUFLO']->getUserID();
        if (isset($_GET['userID']) && $this->_accessLevel == ACCESS_LEVEL_ROOT) {
            $userid = $_GET['userID'];
        }
        
        if (isset($_GET['code'])) 
        {
            $code = $_GET['code'];
            $url = 'https://oauth2.googleapis.com/token';
            $params = array("client_id" => GOOGLE_API_CLIENT_ID, "client_secret" => GOOGLE_API_CLIENT_SECRET, "grant_type" => "authorization_code", "code" => $code, "redirect_uri" => SITE_URL."/index.php?m=dripMarket&a=showconfig&smtp=".$_GET['smtp']);
            $response = $this->postRequest($url, $params);
            $responseJson = json_decode($response);
            //var_dump($response);
            
            $userDetails = file_get_contents('https://www.googleapis.com/oauth2/v1/userinfo?access_token='.$responseJson->access_token);
            $userData = json_decode($userDetails);
            //var_dump($userData);
            if($_GET['smtp'] == 1){
                $configQuery = "UPDATE `dripmarket_configuration` SET google_access_token='".$response."', google_email='".$userData->email."', google_name='".$userData->name."' WHERE userid=$userid";
                mysql_query($configQuery);
            }
            else if($_GET['smtp'] == 2){
                $configQuery = "UPDATE `dripmarket_configuration` SET google_access_token2='".$response."', google_email2='".$userData->email."', google_name2='".$userData->name."' WHERE userid=$userid";
                mysql_query($configQuery);
            }
            header("location:index.php?m=dripMarket&a=showconfig");
        }
        
        $sql = "SELECT  * FROM `dripmarket_configuration` where `dripmarket_configuration`.userid=$userid";
        $resource = mysql_query($sql);
        $dripConfig = mysql_fetch_assoc($resource);
        if ($_GET['smtp'] == -1 && !empty($dripConfig['google_access_token'])){
            $dripConfigJson = json_decode($dripConfig['google_access_token']);
            $params = array("token" => $dripConfigJson->access_token);
            $revokeResult = $this->postRequest('https://accounts.google.com/o/oauth2/revoke', $params);
            $configQuery = "UPDATE `dripmarket_configuration` SET google_access_token=null, google_email=null, google_name=null WHERE userid=$userid";
            mysql_query($configQuery);
            header("location:index.php?m=dripMarket&a=showconfig");
        }
        else if ($_GET['smtp'] == -2 && !empty($dripConfig['google_access_token2'])){
            $dripConfigJson = json_decode($dripConfig['google_access_token2']);
            $params = array("token" => $dripConfigJson->access_token);
            $revokeResult = $this->postRequest('https://accounts.google.com/o/oauth2/revoke', $params);
            $configQuery = "UPDATE `dripmarket_configuration` SET google_access_token2=null, google_email2=null, google_name2=null WHERE userid=$userid";
            mysql_query($configQuery);
            header("location:index.php?m=dripMarket&a=showconfig");
        }
        
//        include_once('/../../GoogleCalAPI/GoogleAPIClient.php');
//        $googleAPI = new GoogleAPIClient();
//        $googleAPI->GetGmailClient($dripConfig['google_access_token2']);
//        $labels = $googleAPI->sendMessage('VG86IG1hbGlrYWJpaWRAZ21haWwuY29tDQpTdWJqZWN0OiB0ZXN0DQpDb250ZW50LVR5cGU6IHRleHQvaHRtbDtjaGFyc2V0PXV0Zi04DQoNCnRlc3Q');
//        var_dump($labels);

        $this->_template->assign('resourcedetailsresult', $dripConfig);
        $this->_template->assign('userid', $userid);
        $this->_template->assign('active', $this);
        $this->_template->assign('client_id', GOOGLE_API_CLIENT_ID);
        $this->_template->assign('site_url', SITE_URL);
        $this->_template->display('./modules/dripMarket/configuration.tpl');
    }

}

?>
