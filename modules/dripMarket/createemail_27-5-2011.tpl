<?php /* $Id: Companies.tpl 3460 2007-11-07 03:50:34Z brian $ */ ?>
<?php TemplateUtility::printHeader('Companies', array('js/highlightrows.js', 'js/export.js', 'js/dataGrid.js')); ?>
<?php TemplateUtility::printHeaderBlock(); ?>

<?php TemplateUtility::printTabs($this->active); ?>

<link rel="stylesheet" href="css/tab.css" />
<link rel="stylesheet" href="css/form.css" />
<script type="text/javascript" src="javascript/tab.js"></script>
<script type="text/javascript" src="javascript/validations.js"></script>
 
    <div id="main">
   
        <div id="contents">
            
			<!--tabs Starts-->		 
<ul id="countrytabs" class="shadetabs" style="padding-top:20px;">
<li><a class="selected" href="javascript:void(0)" onclick="javascript:goToURL('<?php echo(RESUFLOUtility::getIndexName()); ?>?m=dripMarket&amp;a=showcompaign');" rel="country1">Campaigns</a></li>
<li><a href="javascript:void(0)" onclick="javascript:goToURL('<?php echo(RESUFLOUtility::getIndexName()); ?>?m=dripMarket&amp;a=showconfig');" >Configuration</a></li>
<li><a  href="javascript:void(0)" onclick="javascript:goToURL('<?php echo(RESUFLOUtility::getIndexName()); ?>?m=dripMarket&amp;a=questionaire');">Questionaire</a></li>
</ul>
<div style="border: 0px solid #32004b; width:900px; height:auto; margin-bottom:1em; padding:0px;">
<div id="country1" class="tabcontent">
<table width="100%" border="0" cellspacing="0" cellpadding="0" bgcolor="#fff" >
  <tr>
    <td colspan="4" bgcolor="#f8f8f8">&nbsp;</td>
  </tr>
  <tr>
    <td colspan="4" bgcolor="#f8f8f8" style="padding-left:10px;"><a href="javascript:void(0)" onclick="javascript:goToURL('<?php echo(RESUFLOUtility::getIndexName()); ?>?m=dripMarket&amp;a=createcampaign');" ></a></td>
    </tr>
  <tr>
    <td colspan="4" bgcolor="#fff" >
                                           <form name="frm1" id="frm1" action="<?php echo(RESUFLOUtility::getIndexName()); ?>?m=dripMarket&amp;a=saveemail" onsubmit="return emailadd()" method="post">
	<table width="100%" border="0" cellspacing="2" cellpadding="2" bgcolor="#f8f8f8";>
      <tr>
        <td width="29%">Candidate Status </td>
        <td width="71%"><label><?php echo $this->resourcedetails['short_description']; ?></label></td>
      </tr>
      <tr>
        <td>Send Email after </td>
        <td><table width="80%" border="0" cellspacing="0" cellpadding="0">
          <tr>
            <td width="10%"><label>
              <input type="text" name="sendemailon" id="sendemailon" value="<?php
              if(isset($this->resourcedetails['sendemailon'])){
              echo $this->resourcedetails['sendemailon'];} ?>"  class="txtbox_small" size="3"/>
            </label></td>
            <td width="30%" align="center">Day After Creation </td>
            <td width="17%"><select name="selecthour" id="selecthour" class="txtbox_s">
                       <option value="0" >HH</option>     
                                  <?php for($i=0;$i<12;$i++)
                     {
              if(isset($this->resourcedetails['hour']))
              {
                if($this->resourcedetails['hour']==$i)
                {
                 $selectedh="selected";   
                }else
                {
                          $selectedh="";  
                    
                }
             } 
                        
                           echo "<option value='$i'  $selectedh >$i</option>";
                     }
                        ?>
                         </select></td>
            <td width="23%" align="center"><select name="selectmin" id="selectmin" class="txtbox_s">
                     <option value="0" >MM</option>
                     <?php for($i=0;$i<60;$i++)
                     {
                                   if(isset($this->resourcedetails['min']))
              {
                if($this->resourcedetails['min']==$i)
                {
                 $selectedm="selected";   
                }else
                {
                          $selectedm="";  
                    
                }
             } 
                        
                        
                           echo "<option value='$i' $selectedm >$i</option>";
                     }
                        ?>
                           
                         </select></td>
            <td width="20%">
              <?php    if(isset($this->resourcedetails['timezone']))
              {
                if($this->resourcedetails['timezone']==1)
                {
                 $selectedt="selected";   
                }else
                {
                          $selectedt="";  
                    
                }
             }
             ?>
                        
                
                
                <select name="selecttimezone" id="selecttimezone" class="txtbox_s">
                     <option value="0" >Time Zone</option> 
                            <option value="1" <?php if(isset($this->resourcedetails['timezone'])){                if($this->resourcedetails['timezone']==1)   {               echo $selectedt="selected";   
                }else{          echo  $selectedt="";   }}
             ?>  >GMT</option>
                            <option value="2" <?php if(isset($this->resourcedetails['timezone'])){                if($this->resourcedetails['timezone']==2)   {               echo $selectedt="selected";   
                }else{          echo  $selectedt="";   }}
             ?>>GMT-2</option>
                         </select></td>
          </tr>
        </table></td>
      </tr>
      <tr>
        <td> Subject</td>
        <td><label>
        <input type="text" id="subject"  name="subject"  class="txtbox" value="<?php
              if(isset($this->resourcedetails['subject'])){
              echo $this->resourcedetails['subject'];} ?>" />
		 <input type="hidden" name="status" value="<?php echo $this->status; ?>" />
                   <input type="hidden" name="task" value="<?php echo $this->task; ?>" />
         <input type="hidden" name="cid" value="<?php echo $this->cid; ?>" />
        </label></td>
      </tr>
      
      <tr>
        <td>Message</td>
        <td><textarea name="txtmessage" id="txtmessage" class="txtbox"><?php if(isset($this->resourcedetails['message'])){echo $this->resourcedetails['message'];} ?></textarea></td>
      </tr>
      <tr>
        <td>&nbsp;</td>
        <td><input type="image" src="images/save.gif" width="105" height="43" /></td>
      </tr>
      <tr>
        <td>&nbsp;</td>
        <td>&nbsp;</td>
      </tr>
    </table>   
	</form>  
	</td>
  </tr>
    
</table>

</div>

</div>        
<script type="text/javascript">// <![CDATA[
var countries=new ddtabcontent("countrytabs")
countries.setpersist(true)
countries.setselectedClassTarget("link") //"link" or "linkparent"
countries.init()
// ]]></script>   
 <!--tabs end-->	
        </div>
    </div>
    <div id="bottomShadow"></div>
<?php TemplateUtility::printFooter(); ?>
