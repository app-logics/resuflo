<?php /* $Id: Companies.tpl 3460 2007-11-07 03:50:34Z brian $ */ ?>
<?php TemplateUtility::printHeader('Companies', array('js/highlightrows.js', 'js/export.js', 'js/dataGrid.js', 'javascript/validations.js')); ?>
<?php TemplateUtility::printHeaderBlock(); ?>

<?php TemplateUtility::printTabs($this->active); ?>
<link rel="stylesheet" href="css/tab.css" />
<style>
    #dripReportTable tr{height: 25px;}
    #dripReportTable tr:nth-child(even) {background: #CCC;}
    #dripReportTable tr:nth-child(odd) {background: #FFF}
</style>
<script type="text/javascript" src="javascript/tab.js"></script>


    <div id="main">
         <div id="contents">
<!--tabs Starts-->		 
<ul id="countrytabs" class="shadetabs" style="padding-top:20px;">
<li><a class="" href="javascript:void(0)" onclick="javascript:goToURL('<?php echo(RESUFLOUtility::getIndexName()); ?>?m=dripMarket&amp;a=faileddripreport');" rel="">Failed Drip Report</a></li>
<li><a class="" href="javascript:void(0)" onclick="javascript:goToURL('<?php echo(RESUFLOUtility::getIndexName()); ?>?m=dripMarket&amp;a=successdripreport');" rel="">Success Drip Report</a></li>
<li><a class="selected" href="javascript:void(0)" onclick="javascript:goToURL('<?php echo(RESUFLOUtility::getIndexName()); ?>?m=dripMarket&amp;a=inqueuedripreport');" rel="country1">InQueue Drip Report</a></li>
<li><a class="" href="javascript:void(0)" onclick="javascript:goToURL('<?php echo(RESUFLOUtility::getIndexName()); ?>?m=dripMarket&amp;a=failedtextreport');" rel="">Failed Text Report</a></li>
<li><a class="" href="javascript:void(0)" onclick="javascript:goToURL('<?php echo(RESUFLOUtility::getIndexName()); ?>?m=dripMarket&amp;a=successtextreport');" rel="">Success Text Report</a></li>
<li><a class="" href="javascript:void(0)" onclick="javascript:goToURL('<?php echo(RESUFLOUtility::getIndexName()); ?>?m=dripMarket&amp;a=inqueuetextreport');" rel="">InQueue Text Report</a></li>
</ul>
<div style="border: 0px solid #32004b; width:900px; height:auto; margin-bottom:1em; padding:0px;">
<div id="country1" class="tabcontent">
<table id="dripReportTable" width="100%" border="0" cellspacing="0" cellpadding="0" bgcolor="#f8f8f8" >
  <tr>
    <td height="25" bgcolor="#0094d6" class="font_tab">User</td>
    <td bgcolor="#0094d6" class="font_tab">Count</td>
    <td bgcolor="#0094d6" class="font_tab">Date</td>
    <td bgcolor="#0094d6" class="font_tab" >Action</td>
  </tr>
    <?php 
        foreach ($this->dripReport as $row) {
    ?>
        <tr>
            <td class="font_tab_txt"><a href="<?php echo(RESUFLOUtility::getIndexName()); ?>?m=dripMarket&a=showconfig&userID=<?php echo($row['entered_by']); ?>"><?php echo $row['user_name']; ?></a></td>
            <td class="font_tab_txt"><?php echo $row['success_count']; ?></td>
            <td class="font_tab_txt"><?php echo $row['success_date']; ?></td>
            <td class="font_tab_txt"><a href="<?php echo(RESUFLOUtility::getIndexName()); ?>?m=dripMarket&amp;a=deleteDrip&amp;user_id=<?php echo($row['entered_by']); ?>&amp;success_date=<?php echo($row['success_date']); ?>">Delete</a></td>
        </tr>
   <?php } ?>
</table>
</div>

</div>        
<script type="text/javascript">// <![CDATA[
var countries=new ddtabcontent("countrytabs")
countries.setpersist(true)
countries.setselectedClassTarget("link") //"link" or "linkparent"
countries.init()
// ]]></script>   
 <!--tabs end-->		  
       </div>
    </div>
    <div id="bottomShadow"></div>
<?php TemplateUtility::printFooter(); ?>
