<?php /* $Id: Companies.tpl 3460 2007-11-07 03:50:34Z brian $ */ ?>
<?php TemplateUtility::printHeader('Companies', array('js/highlightrows.js', 'js/export.js', 'js/dataGrid.js')); ?>
<?php TemplateUtility::printHeaderBlock(); ?>

<?php TemplateUtility::printTabs($this->active); ?>

<link rel="stylesheet" href="css/tab.css" />
<link rel="stylesheet" href="css/form.css" />
<script type="text/javascript" src="javascript/tab.js"></script>

 <script type="text/javascript" src="javascript/validations.js"></script>
    <div id="main">
   
        <div id="contents">
            
			<!--tabs Starts-->		 
<ul id="countrytabs" class="shadetabs" style="padding-top:20px;">
<li><a  href="javascript:void(0)" onclick="javascript:goToURL('<?php echo(RESUFLOUtility::getIndexName()); ?>?m=dripMarket&amp;a=showcompaign');" >Campaigns</a></li>
<li><a href="javascript:void(0)" onclick="javascript:goToURL('<?php echo(RESUFLOUtility::getIndexName()); ?>?m=dripMarket&amp;a=showconfig');" >Configuration</a></li>
<li><a class="selected" href="javascript:void(0)" onclick="javascript:goToURL('<?php echo(RESUFLOUtility::getIndexName()); ?>?m=dripMarket&amp;a=questionaire');" rel="country1">Questionaire</a></li>
</ul>
<div style="border: 0px solid #32004b; width:900px; height:auto; margin-bottom:1em; padding:0px;">
<div id="country1" class="tabcontent">
<table width="100%" border="0" cellspacing="0" cellpadding="0" bgcolor="#fff" >
  <tr>
    <td colspan="4" bgcolor="#f8f8f8">&nbsp;</td>
  </tr>
  <tr>
    <td colspan="4" bgcolor="#f8f8f8" style="padding-left:10px;"><a href="javascript:void(0)" onclick="javascript:goToURL('<?php echo(RESUFLOUtility::getIndexName()); ?>?m=dripMarket&amp;a=createcampaign');" ></a></td>
    </tr>
  <tr>
    <td colspan="4" bgcolor="#fff" >
    <form name="frm1" id="frm1" action="<?php echo(RESUFLOUtility::getIndexName()); ?>?m=dripMarket&amp;a=savequestions" method="post" onsubmit="return questions()">
	<table width="100%" border="0" cellspacing="2" cellpadding="2" bgcolor="#f8f8f8";>
      <tr>
        <td width="29%">Question</td>
        <td width="71%"><textarea name="question" id="question" class="txtbox" ><?php echo $this->quesname; ?></textarea>
		<input type="hidden" name="questionaireid" id="questionaireid" value="<?php echo $this->maxid; ?>" />
                      <input type="hidden" name="task" id="task" value="<?php echo $this->task; ?>" />
                                      <input type="hidden" name="id" id="id" value="<?php echo $this->quesid; ?>" />		</td>
      </tr>
      
      <tr>
        <td>&nbsp;</td>
        <td><input type="image" src="images/save.gif" onClick="checkDatasave(this.id)" width="105" height="43" name="save" /></td>
      </tr>
      <tr>
        <td>&nbsp;</td>
        <td>&nbsp;</td>
      </tr>
      <tr>
        <td colspan="2"><table width="100%" border="0" cellspacing="0" cellpadding="0" bgcolor="#f8f8f8" >
  
  <tr>
    <td colspan="4" bgcolor="#f8f8f8" style="padding-left:10px;"><a href="javascript:void(0)" onclick="javascript:goToURL('<?php echo(RESUFLOUtility::getIndexName()); ?>?m=dripMarket&amp;a=createcampaign');" ></a></td>
    </tr>
  <tr>
    <td colspan="4" bgcolor="#F8F8F8" class="font_tab">&nbsp;</td>
    </tr>
  <tr>
    <td width="35%" height="30" bgcolor="#e2e2e2" class="font_tab">Question Name </td>
    <td width="65%" bgcolor="#e2e2e2" class="font_tab" >Action</td>
  </tr>
 <?php  $k=1;
                              foreach($this->compaigndata as $row){
                                ?>
                            
                     
  <tr>
    
    <td class="font_tab_txt"> <?php echo $row['quesname']; ?></td>
    <td><table width="20%" border="0" cellspacing="0" cellpadding="0">
      <tr>
        <td align="center"><a href="javascript:void(0)" onclick="javascript:goToURL('<?php echo(RESUFLOUtility::getIndexName()); ?>?m=dripMarket&amp;id=<?php echo $row['id']; ?>&amp;a=editques&amp;questionaireid=<?php echo $this->maxid; ?>');" ><img src="images/edit_ico.gif" border="0" /></a></td>
        <td><a href="javascript:void(0)" onclick="javascript:goToURL('<?php echo(RESUFLOUtility::getIndexName()); ?>?m=dripMarket&amp;id=<?php echo $row['id']; ?>&amp;a=delques&amp;questionaireid=<?php echo $this->maxid; ?>');" ><img src="images/delete_ico.gif" border="0" /></a></td>
      </tr>
    </table></td>
  </tr>
   <?php } ?>
</table></td>
        </tr>
            <tr>
  <td colspan="2"><input name="proceed" id="proceed" type="image" src="images/proceed.gif" value="submit" width="105" onClick="checkData(this.id)" height="43" /></td>
      </tr>
    </table>   
</form>  
	</td>
  </tr>
    
</table>

</div>

</div>        
<script type="text/javascript">// <![CDATA[
var countries=new ddtabcontent("countrytabs")
countries.setpersist(true)
countries.setselectedClassTarget("link") //"link" or "linkparent"
countries.init()
// ]]></script>   
 <!--tabs end-->	
        </div>
    </div>
    <div id="bottomShadow"></div>
<?php TemplateUtility::printFooter(); ?>
