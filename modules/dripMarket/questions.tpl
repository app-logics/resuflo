<?php /* $Id: Companies.tpl 3460 2007-11-07 03:50:34Z brian $ */ ?>
<?php TemplateUtility::printHeader('Companies', array('js/highlightrows.js', 'js/export.js', 'js/dataGrid.js')); ?>
<?php TemplateUtility::printHeaderBlock(); ?>

<?php TemplateUtility::printTabs($this->active); ?>

<link rel="stylesheet" href="css/tab.css" />
<link rel="stylesheet" href="css/form.css" />
<script type="text/javascript" src="javascript/tab.js"></script>

 <script type="text/javascript" src="javascript/validations.js"></script>
    <div id="main">
   
        <div id="contents">
            
			<!--tabs Starts-->		 
<ul id="countrytabs" class="shadetabs" style="padding-top:20px;">
<li><a  href="javascript:void(0)" onclick="javascript:goToURL('<?php echo(RESUFLOUtility::getIndexName()); ?>?m=dripMarket&amp;a=showcompaign');" >Campaigns</a></li>
<li><a href="javascript:void(0)" onclick="javascript:goToURL('<?php echo(RESUFLOUtility::getIndexName()); ?>?m=dripMarket&amp;a=showconfig');" >Configuration</a></li>
<li><a class="selected" href="javascript:void(0)" onclick="javascript:goToURL('<?php echo(RESUFLOUtility::getIndexName()); ?>?m=dripMarket&amp;a=questionnaire');" rel="country1">Questionnaire</a></li>
</ul>
<div style="border: 0px solid #32004b; width:900px; height:auto; margin-bottom:1em; padding:0px;">
<div id="country1" class="tabcontent">
<table width="100%" border="0" cellspacing="0" cellpadding="0" bgcolor="#fff" >
  <tr>
    <td colspan="4" bgcolor="#f8f8f8">&nbsp;</td>
  </tr>
  <tr>
    <td colspan="4" bgcolor="#f8f8f8" style="padding-left:10px;"><a href="javascript:void(0)" onclick="javascript:goToURL('<?php echo(RESUFLOUtility::getIndexName()); ?>?m=dripMarket&amp;a=createcampaign');" ></a></td>
    </tr>
  <tr>
    <td colspan="4" bgcolor="#fff" >
    <form name="frm1" id="frm1" action="<?php echo(RESUFLOUtility::getIndexName()); ?>?m=dripMarket&amp;a=savequestions" method="post" onsubmit="return questions()">
	<table width="100%" border="0" cellspacing="2" cellpadding="2" bgcolor="#f8f8f8";>
             <tr>
    <td colspan="2" >
<a  href="javascript:void(0)" href="javascript:void(0)" onclick="javascript:goToURL('<?php echo(RESUFLOUtility::getIndexName()); ?>?m=dripMarket&amp;a=questionnaire');" ><< Back to main page</a>
    </td>
  </tr>
     <tr>
    <td colspan="2" >

    </td>
  </tr>
    <tr>
        <td width="29%">Url</td>
        <td width="71%">
            <?php if($_SERVER['HTTP_HOST'] == LOCALHOST){ ?>
                <a target="_blank" href="<?php echo LOCALHOST_PATH; ?>/que/view_questionnaire.php?userid=<?php echo $this->userId; ?>&quesid=<?php echo $this->maxid; ?>&siteId=<?php echo $_SESSION['RESUFLO']->getSiteID(); ?>">    
                    <?php echo LOCALHOST_PATH; ?>/que/view_questionnaire.php?userid=<?php echo $this->userId; ?>&quesid=<?php echo $this->maxid; ?>&siteId=<?php echo $_SESSION['RESUFLO']->getSiteID(); ?>
                </a>
            <?php }else{ ?>
            <a target="_blank" href="<?php echo $this->baseurl;?>que/view_questionnaire.php?userid=<?php echo $this->userId; ?>&quesid=<?php echo $this->maxid; ?>&siteId=<?php echo $_SESSION['RESUFLO']->getSiteID(); ?>">
                    <?php echo $this->baseurl;?>que/view_questionnaire.php?userid=<?php echo $this->userId; ?>&quesid=<?php echo $this->maxid; ?>&siteId=<?php echo $_SESSION['RESUFLO']->getSiteID(); ?>
                </a>
            <?php } ?>
        </td>
      </tr>

                 
       
      
            
            
            
    <tr>
        <td width="29%">Question</td>
        <td width="71%">
            <textarea name="question" id="question" class="txtbox" ><?php echo $this->quesname; ?></textarea>
            <input type="hidden" name="questionaireid" id="questionaireid" value="<?php echo $this->maxid; ?>" />
            <input type="hidden" name="task" id="task" value="<?php echo $this->task; ?>" />                      
            <input type="hidden" name="taskaction" id="taskaction" value="<?php echo $this->taskaction; ?>" />
            <input type="hidden" name="id" id="id" value="<?php echo $this->quesid; ?>" />
            
        </td>
      </tr>
      <tr>
          <td>
              
          </td>
          <td>
              <a href="#" id="addOption">Add Option</a>
          </td>
      </tr>
      <tr>
          <td></td>
          <td id="optionSection">
              <?php 
                if(!empty($this->quesoption)){
                    $options = explode("|||", $this->quesoption);
                    foreach($options as $option){
                        $value = explode("~~~", $option)[0];
                        $label = explode("~~~", $option)[1];
                        ?>
                        <span>
                            <input type="radio" name="option"><input type="text" name="optionValue[]" class="input100" placeholder="Value" value="<?php echo $value; ?>">&nbsp;<input type="text" name="optionLabel[]" placeholder="Label" value="<?php echo $label; ?>"> <a onclick="delOption(this)" href="#">Delete</a><br><br>
                        </span>
                        <?php
                    }
                }
              ?>
              <!--<span>
                  <input type="radio" name="option"><input type="text" name="value" class="input100" placeholder="Value">&nbsp;<input type="text" name="label" placeholder="Label"> <a onclick="delOption(this)" href="#">Delete</a><br><br>
              </span>-->
          </td>
      </tr>
      <tr>
          <td>
              <style>
                  .input100{
                      width: 100px;
                  }
              </style>
              <script type="text/javascript">
                    $option = "<?php echo $this->quesoption; ?>";
                    
                    $("#addOption").click(function(){
                        $("#optionSection").append('<span><input type="radio" name="option"><input type="text" name="optionValue[]" class="input100" placeholder="Value">&nbsp;<input type="text" name="optionLabel[]" placeholder="Label"> <a onclick="delOption(this)" href="#">Delete</a><br><br></span>');
                    });
                    function delOption(pointer){
                        $(pointer).closest("span").remove();
                    }
                </script>
          </td>
      </tr>
      
      <tr>
        <td>&nbsp;</td>
        <td><input type="image" src="images/save.gif" onClick="checkDatasave(this.id)" width="105" height="43" name="save" /></td>
      </tr>
      <tr>
        <td>&nbsp;</td>
        <td>&nbsp;</td>
      </tr>
      <tr>
        <td colspan="2"><table width="100%" border="0" cellspacing="0" cellpadding="0" bgcolor="#f8f8f8" >
  
  <tr>
    <td colspan="4" bgcolor="#f8f8f8" style="padding-left:10px;"><a href="javascript:void(0)" onclick="javascript:goToURL('<?php echo(RESUFLOUtility::getIndexName()); ?>?m=dripMarket&amp;a=createcampaign');" ></a></td>
    </tr>
  <tr>
    <td colspan="4" bgcolor="#F8F8F8" class="font_tab">&nbsp;</td>
    </tr>
  <tr>
    <td width="35%" height="30" bgcolor="#e2e2e2" class="font_tab">Question Name </td>
    <td width="65%" bgcolor="#e2e2e2" class="font_tab" >Action</td>
  </tr>
 <?php  $k=1;
                              foreach($this->compaigndata as $row){
                                ?>
                            
                     
  <tr>
    
    <td class="font_tab_txt"> <?php echo $row['quesname']; ?>      	<input type="hidden" name="task" id="task" value="myform" /></td>
    <td><table width="20%" border="0" cellspacing="0" cellpadding="0">
      <tr>
        <td align="center"><a href="javascript:void(0)" onclick="javascript:goToURL('<?php echo(RESUFLOUtility::getIndexName()); ?>?m=dripMarket&amp;id=<?php echo $row['id']; ?>&amp;a=editques&amp;questionaireid=<?php echo $this->maxid; ?>');" ><img src="images/edit_ico.gif" border="0" /></a></td>
        <td><a href="javascript:void(0)" onclick="javascript:deleteforall('<?php echo(RESUFLOUtility::getIndexName()); ?>?m=dripMarket&amp;id=<?php echo $row['id']; ?>&amp;a=delques&amp;questionaireid=<?php echo $this->maxid; ?>')" ><img src="images/delete_ico.gif" border="0"  /></a></td>
      </tr>
    </table></td>
  </tr>
   <?php } ?>
   
</table>
        </form> </td>
        </tr>
            <tr>
  <td >
    <form name="frm2" id="frm2" action="<?php echo(RESUFLOUtility::getIndexName()); ?>?m=dripMarket&amp;a=savequestions" method="post" >
    	<input type="hidden" name="questionaireid" id="questionaireid" value="<?php echo $this->maxid; ?>" /><input name="gethtml" id="gethtml" type="image" src="images/gethtml.gif" value="submit" width="105" onClick="checkData()" height="43" />
        	<input type="hidden" name="task" id="task" value="proceed" />
   <!-- <input name="proceed" id="proceed" type="image" src="images/proceed.gif" value="submit" width="105" onClick="checkData()" height="43" />--></td>  <td ></form></td>
      </tr>
    </table>   

	</td>
  </tr>
    
</table>

</div>

</div>        
<script type="text/javascript">// <![CDATA[
var countries=new ddtabcontent("countrytabs")
countries.setpersist(true)
countries.setselectedClassTarget("link") //"link" or "linkparent"
countries.init()
// ]]></script>

 <!--tabs end-->	
        </div>
    </div>
    <div id="bottomShadow"></div>
<?php TemplateUtility::printFooter(); ?>
