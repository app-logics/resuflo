<?php /* $Id: Companies.tpl 3460 2007-11-07 03:50:34Z brian $ */ ?>
<?php TemplateUtility::printHeader('Companies', array('js/highlightrows.js', 'js/export.js', 'js/dataGrid.js')); ?>
<?php TemplateUtility::printHeaderBlock(); ?>

<?php TemplateUtility::printTabs($this->active); ?>
<link rel="stylesheet" href="css/tab.css" />
<script type="text/javascript" src="javascript/tab.js"></script>

    <div id="main">
         <div id="contents">
		 
<!--tabs Starts-->		 
<ul id="countrytabs" class="shadetabs" style="padding-top:20px;">
<li><a href="javascript:void(0)" onclick="javascript:goToURL('<?php echo(RESUFLOUtility::getIndexName()); ?>?m=dripMarket&amp;a=showcompaign');" rel="country1" class="selected">Campaigns</a></li>
<li><a href="javascript:void(0)" onclick="javascript:goToURL('<?php echo(RESUFLOUtility::getIndexName()); ?>?m=dripMarket&amp;a=showconfig');" >Configuration</a></li>
<li><a  href="javascript:void(0)" onclick="javascript:goToURL('<?php echo(RESUFLOUtility::getIndexName()); ?>?m=dripMarket&amp;a=questionnaire');">Questionnaire</a></li>
</ul>
<div style="border: 0px solid #32004b; width:900px; height:auto; margin-bottom:1em; padding:0px;">
<div id="country1" class="tabcontent">
<table width="100%" border="0" cellspacing="0" cellpadding="0" bgcolor="#f8f8f8" >
                        <tr>
                            <td>&nbsp;</td>
                        </tr>
                         
                          <tr>
    <td colspan="2" >
<a  href="javascript:void(0)" href="javascript:void(0)" onclick="javascript:goToURL('<?php echo(RESUFLOUtility::getIndexName()); ?>?m=dripMarket');" ><< Back to main page</a>
    </td>
  </tr>
  <tr>
    <td colspan="4" bgcolor="#f8f8f8">&nbsp;</td>
  </tr>
  <tr>
    <td colspan="4" bgcolor="#f8f8f8" style="padding-left:10px;">
        <input type="button" class="button" value="Add New Email" onclick="javascript:goToURL('<?php echo(RESUFLOUtility::getIndexName()); ?>?m=dripMarket&amp;a=createemail&amp;cid=<?php echo $this->cid; ?>&amp;s=<?php echo $this->cstatus; ?>');" />
        <?php if($this->isTextMessageEnable == 1){ ?>
        <input type="button" class="button" value="Add New Text Message" onclick="javascript:goToURL('<?php echo(RESUFLOUtility::getIndexName()); ?>?m=dripMarket&amp;a=createtextmessage&amp;cid=<?php echo $this->cid; ?>&amp;s=<?php echo $this->cstatus; ?>');" />
        <?php } ?>
    </td>
    </tr>
  <tr>
    <td colspan="4" bgcolor="#F8F8F8" class="font_tab">&nbsp;</td>
    </tr>
  <tr>
    <td height="35" bgcolor="#e2e2e2" class="font_tab">Name</td>
    <td height="35" bgcolor="#e2e2e2" class="font_tab">Type</td>
    <td bgcolor="#e2e2e2" class="font_tab">Subjects</td>
    <td bgcolor="#e2e2e2" class="font_tab">No of Days </td>
    <td bgcolor="#e2e2e2" class="font_tab" >Action</td>
  </tr>
    <?php foreach($this->compaigndata as $row){ ?>
  <tr>
    <td class="font_tab_txt"><?php echo $row['nameofemail']; ?></td>
    <td class="font_tab_txt"><?php echo (($row['isTextMessage'] == 1) ? 'Text Message' : 'Email'); ?></td>
    <td class="font_tab_txt"><?php echo (!empty($row['subject'])) ? $row['subject'] : $row['subject']; ?></td>
    <td class="font_tab_txt"><?php  echo $row['sendemailon']; ?></td>
    <td><table width="80%" border="0" cellspacing="0" cellpadding="0">
      <tr>
        <td align="center">
            <?php 
                if($row['isTextMessage'] == '1'){
            ?>
            <a href="<?php echo(RESUFLOUtility::getIndexName()); ?>?m=dripMarket&amp;a=compaigntextmessageedit&amp;cid=<?php echo $this->cid; ?>&amp;editid=<?php echo $row['eid']; ?>&amp;id=<?php echo $this->compaigndata[0]['id']; ?>"><img src="images/edit_ico.gif"  border="0"/></a>
            <?php }else{ ?>
            <a href="<?php echo(RESUFLOUtility::getIndexName()); ?>?m=dripMarket&amp;a=compaignemailedit&amp;cid=<?php echo $this->cid; ?>&amp;editid=<?php echo $row['eid']; ?>&amp;id=<?php echo $this->compaigndata[0]['id']; ?>"><img src="images/edit_ico.gif"  border="0"/></a>
            <?php } ?>
        </td>
        <td> <a onclick="return confirm('Are you sure you want to delete this ?');" href="<?php echo(RESUFLOUtility::getIndexName()); ?>?m=dripMarket&amp;a=compaignemaildelete&amp;delid=<?php echo $row['eid']; ?>&amp;id=<?php echo $this->compaigndata[0]['id']; ?>"><img src="images/delete_ico.gif"  border="0"/></a></td>
      </tr>
    </table></td>
  </tr>
   <?php } ?>
</table>
</div>

</div>        
  <script>
  $( function() {
    $( ".widget input[type=submit], .widget a, .widget button" ).button();
  } );
  </script>
<script type="text/javascript">// <![CDATA[
var countries=new ddtabcontent("countrytabs")
countries.setpersist(true)
countries.setselectedClassTarget("link") //"link" or "linkparent"
countries.init()
// ]]></script>   
 <!--tabs end-->		  
       </div>
    </div>
    <div id="bottomShadow"></div>
<?php TemplateUtility::printFooter(); ?>
