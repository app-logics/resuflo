<?php /* $Id: Companies.tpl 3460 2007-11-07 03:50:34Z brian $ */ ?>
<?php TemplateUtility::printHeader('Companies', array('js/highlightrows.js', 'js/export.js', 'js/dataGrid.js')); ?>
<?php TemplateUtility::printHeaderBlock(); ?>

<?php TemplateUtility::printTabs($this->active); ?>

<link rel="stylesheet" href="css/tab.css" />
<link rel="stylesheet" href="css/form.css" />
<script type="text/javascript" src="javascript/tab.js"></script>
<script type="text/javascript" src="javascript/validations.js"></script>
<script src="js/lib.js" type="text/javascript"></script>
<script type="text/javascript" >
    function SMTPTest(isSecondSMTPTest = false)
    {
        /* Find and disable the activity entry's action icons. */
        var sendGrid = (isSecondSMTPTest == false) ? $('input[name=sendgrid]').prop("checked") : $('input[name=sendgrid2]').prop("checked");
        var smtpHost = (isSecondSMTPTest == false) ? document.getElementById('smtphost').value : document.getElementById('smtphost2').value;
        var smtpUser = (isSecondSMTPTest == false) ? document.getElementById('smtpuser').value : document.getElementById('smtpuser2').value;
        var smtpPass = (isSecondSMTPTest == false) ? document.getElementById('smtppass').value : document.getElementById('smtppass2').value;
        var smtpPort = (isSecondSMTPTest == false) ? document.getElementById('smtpport').value : document.getElementById('smtpport2').value;
        var fromName = (isSecondSMTPTest == false) ? document.getElementById('fromname').value : document.getElementById('fromname2').value;
        var fromEmail = (isSecondSMTPTest == false) ? document.getElementById('fromemail').value : document.getElementById('fromemail2').value;
        var bccEmail = (isSecondSMTPTest == false) ? document.getElementById('bccemail').value : document.getElementById('bccemail2').value;
        //var emailType = (isSecondSMTPTest == false)? document.getElementById('emailtype').value : document.getElementById('emailtype2').value;
        var emailType = (isSecondSMTPTest == false) ? $('input[name=emailtype]:radio:checked').val() : $('input[name=emailtype2]:radio:checked').val();
        var TestEmail = (isSecondSMTPTest == false) ? document.getElementById('firstTestEmail').value : document.getElementById('secondTestEmail').value;

        /* FIXME: Spinner? */
        var http = AJAX_getXMLHttpObject();
        /* Build HTTP POST data. */
        var POSTData = '&sendGrid=' + sendGrid +'&smtpHost=' + smtpHost + '&smtpUser=' + smtpUser + '&smtpPass=' + smtpPass + '&smtpPort=' + smtpPort + '&fromName=' + fromName + '&fromEmail=' + fromEmail + '&bccEmail=' + bccEmail + '&emailType=' + emailType + '&firstTestEmail=' + TestEmail;
        console.log(POSTData);
        //return false;

        /* Anonymous callback function triggered when HTTP response is received. */
        var callBack = function ()
        {
            if (http.readyState != 4)
            {
                return;
            }
            if (http.responseXML != null) {
                /* Return if we have any errors. */
                var errorCodeNode = http.responseXML.getElementsByTagName('errorcode').item(0);
                var errorMessageNode = http.responseXML.getElementsByTagName('errormessage').item(0);
                if (!errorCodeNode.firstChild || errorCodeNode.firstChild.nodeValue != '0')
                {
                    if (errorMessageNode.firstChild)
                    {
                        var errorMessage = "An error occurred while receiving a response from the server.\n\n"
                                + errorMessageNode.firstChild.nodeValue;
                        alert(errorMessage);
                    } else {
                        alert(http.responseXML.getElementsByTagName('response')[0].innerHTML);
                    }
                    return;
                } else {
                    alert(http.responseXML.getElementsByTagName('response')[0].innerHTML);
                }
            } else {
                alert(http.response);
            }
        }

        AJAX_callRESUFLOFunction(
                http,
                'userMailer',
                POSTData,
                callBack,
                0,
                null,
                false,
                false
                );
    }
    function SendGridClick(pointer)
    {
        $('.SMTP,.SendGrid,.Gmail').hide();
        if($(pointer).prop("checked") == true)
        {
            $('.SendGrid').show();
        }
        else if($(".GmailDisconnect").length > 0)
        {
            $('.Gmail').show();
        }
        else{
            $('.SendGrid,.SMTP,.Gmail').show();
        }
    }
    function SendGrid2Click(pointer)
    {
        $('.SMTP2,.SendGrid2,.Gmail2').hide();
        if($(pointer).prop("checked") == true)
        {
            $('.SendGrid2').show();
        }
        else if($(".GmailDisconnect2").length > 0)
        {
            $('.Gmail2').show();
        }
        else{
            $('.SendGrid2,.SMTP2,.Gmail2').show();
        }
    }
</script>
<style>
    .hide{
        display: none;
    }
</style>

<div id="main">

    <div id="contents">

        <!--tabs Starts-->		 
        <ul id="countrytabs" class="shadetabs" style="padding-top:20px;">
            <li><a  href="javascript:void(0)" onclick="javascript:goToURL('<?php echo(RESUFLOUtility::getIndexName()); ?>?m=dripMarket&amp;a=showcompaign');" >Campaigns</a></li>
            <li><a  class="selected" href="javascript:void(0)" onclick="javascript:goToURL('<?php echo(RESUFLOUtility::getIndexName()); ?>?m=dripMarket&amp;a=showconfig');" rel="country1" >Configuration</a></li>
            <li><a  href="javascript:void(0)" onclick="javascript:goToURL('<?php echo(RESUFLOUtility::getIndexName()); ?>?m=dripMarket&amp;a=questionnaire');">Questionnaire</a></li>
        </ul>
        <div style="border: 0px solid #32004b; width:900px; height:auto; margin-bottom:1em; padding:0px;">
            <div id="country1" class="tabcontent">
                <table width="100%" border="0" cellspacing="0" cellpadding="0" bgcolor="#fff" >
                    <tr>
                        <td colspan="4" bgcolor="#f8f8f8">&nbsp;</td>
                    </tr>
                    <tr>
                        <td colspan="4" bgcolor="#f8f8f8" style="padding-left:10px;"><a href="javascript:void(0)" onclick="javascript:goToURL('<?php echo(RESUFLOUtility::getIndexName()); ?>?m=dripMarket&amp;a=createcampaign');" ></a></td>
                    </tr>
                    <tr>
                        <td colspan="4" bgcolor="#fff" >
                            <form name="frm1" id="frm1" action="<?php echo(RESUFLOUtility::getIndexName()); ?>?m=dripMarket&amp;a=saveconfig&amp;userID=<?php echo ($this->userid); ?>" method="post" onsubmit="return configuration()">
                                <table width="100%" border="0" cellspacing="2" cellpadding="2" bgcolor="#f8f8f8";>

                                    <tr><td colspan="2" style="padding: 20px 0px 20px;"><div style="font-size: 16px; font-weight: bold; border-bottom: 1px solid gray;">First Email Configuration</div></td></tr>
                                    <tr class="SendGrid">
                                        <td>Send Grid </td>
                                        <td><input type="checkbox" name="sendgrid" id="sendgrid" value="1" <?php if($this->resourcedetailsresult['sendgrid'] == 1){echo "checked";}; ?> onclick="SendGridClick(this);" style="margin: 0px;"/></td>
                                    </tr>
                                    <tr class="SMTP <?php if($this->resourcedetailsresult['google_email'] != null){ echo 'hide'; }?>">
                                        <td><font style="color:red">*</font>SMTP Host </td>
                                        <td><input type="text" name="smtphost" value="<?php echo $this->resourcedetailsresult['smtphost']; ?>"  id="smtphost" class="txtbox" /></td>
                                    </tr>
                                    <tr class="SMTP <?php if($this->resourcedetailsresult['google_email'] != null){ echo 'hide'; }?>">
                                        <td><font style="color:red">*</font>SMTP User </td>
                                        <td><label>
                                                <input type="text" name="smtpuser" id="smtpuser" value="<?php echo $this->resourcedetailsresult['smtpuser']; ?>"  class="txtbox" />
                                            </label></td>
                                    </tr>
                                    <tr class="SMTP <?php if($this->resourcedetailsresult['google_email'] != null){ echo 'hide'; }?>">
                                        <td><font style="color:red">*</font>SMTP Password </td>
                                        <td><input type="password" name="smtppass" id="smtppass" value="<?php echo $this->resourcedetailsresult['smtppassword']; ?>"  class="txtbox" /></td>
                                    </tr>
                                    <tr class="SMTP <?php if($this->resourcedetailsresult['google_email'] != null){ echo 'hide'; }?>">
                                        <td><font style="color:red">*</font>SMTP Port </td>
                                        <td><input type="text" name="smtpport" id="smtpport" value="<?php echo $this->resourcedetailsresult['smtpport']; ?>"  class="txtbox" /></td>
                                    </tr>
                                    <tr class="SMTP SendGrid <?php if($this->resourcedetailsresult['google_email'] != null){ echo 'hide'; }?>">
                                        <td><font style="color:red">*</font>From Name </td>
                                        <td><input type="text"  name="fromname" id="fromname" value="<?php echo $this->resourcedetailsresult['fromname']; ?>"  class="txtbox" /></td>
                                    </tr>
                                    <tr class="SMTP SendGrid <?php if($this->resourcedetailsresult['google_email'] != null){ echo 'hide'; }?>">
                                        <td><font style="color:red">*</font>From Email </td>
                                        <td><input type="text"  name="fromemail" id="fromemail" value="<?php echo $this->resourcedetailsresult['fromemail']; ?>"  class="txtbox" /></td>
                                    </tr>
                                    <tr class="SMTP SendGrid">
                                        <td>Bcc Email (optional) </td>
                                        <td><input type="text"  name="bccemail" id="bccemail" value="<?php echo $this->resourcedetailsresult['bccemail']; ?>"  class="txtbox" /></td>
                                    </tr>
                                    <tr class="SMTP <?php if($this->resourcedetailsresult['google_email'] != null){ echo 'hide'; }?>">
                                        <td><font style="color:red">*</font>Email Type </td>
                                        <td>
                                            <input type="radio"  name="emailtype" id="emailtype" <?php if($this->resourcedetailsresult['email_type']=='ssl'){echo "checked='checked'";} ?> value="ssl"  />SSL&nbsp;
                                                   <input type="radio"  name="emailtype" id="emailtype" value="tls" <?php if($this->resourcedetailsresult['email_type']=='tls'){echo "checked='checked'";} ?>  />TLS<input type="radio"  name="emailtype" id="emailtype" value="none" <?php if($this->resourcedetailsresult['email_type']=='none'){echo "checked='checked'";} ?>  />None
                                        </td>
                                    </tr>
                                    <tr class="SMTP SendGrid <?php if($this->resourcedetailsresult['google_email'] != null){ echo 'hide'; }?>">
                                        <td></td>
                                        <td>
                                            <div style='background-color: lightgray; width: 300px;'>
                                                <input name='firstTestEmail' id='firstTestEmail' style='width: 200px;' />
                                                <input type="button" value='Test SMTP' onclick="SMTPTest()" />
                                            </div>
                                        </td>
                                    </tr>
                                    <tr class="Gmail">
                                        <td>*GMail Connect (Preferred)</td>
                                        <td>
                                            <?php if($this->resourcedetailsresult['google_email'] == null){ ?>
                                            <table>
                                                <tr>
                                                    <td>
                                                        <a href="https://accounts.google.com/o/oauth2/v2/auth?access_type=offline&response_type=code&client_id=<?php echo $this->client_id; ?>&redirect_uri=<?php echo urlencode($this->site_url.'/index.php?m=dripMarket&a=showconfig&smtp=1'); ?>&scope=https%3A%2F%2Fwww.googleapis.com%2Fauth%2Fgmail.send%20https%3A%2F%2Fwww.googleapis.com%2Fauth%2Fuserinfo.profile%20https%3A%2F%2Fwww.googleapis.com%2Fauth%2Fuserinfo.email">
                                                            <img src="images/mru/btn_google_signin_dark_pressed_web.png" alt=""/>
                                                        </a>
                                                    </td>
                                                    <td style="width: 185px;">
                                                        <img src="images/mru/info_icon.png" style="width: 14px;">
                                                        <span style="color: dimgray; font-size: 10px; font-weight: normal;">We will use google authentification to send email to candidates.</span>
                                                    </td>
                                                </tr>
                                            </table>
                                            <?php } else{?>
                                                <a class="GmailDisconnect" href="index.php?m=dripMarket&a=showconfig&smtp=-1" style="color: red">Disconnect <?php echo $this->resourcedetailsresult['google_email']; ?></a>
                                            <?php }?>
                                        </td>
                                    </tr>

                                    <tr><td colspan="2" style="padding: 20px 0px 20px;"><div style="font-size: 16px; font-weight: bold; border-bottom: 1px solid gray;">Second Email Configuration</div></td></tr>
                                    <tr class="SendGrid2">
                                        <td>Send Grid </td>
                                        <td><input type="checkbox" name="sendgrid2" id="sendgrid2" value="1" <?php if($this->resourcedetailsresult['sendgrid2'] == 1){echo "checked";}; ?> onclick="SendGrid2Click(this);" style="margin: 0px;"/></td>
                                    </tr>
                                    <tr class="SMTP2 <?php if($this->resourcedetailsresult['google_email2'] != null){ echo 'hide'; }?>">
                                        <td><font style="color:red">*</font>SMTP Host </td>
                                        <td><input type="text" name="smtphost2" value="<?php echo $this->resourcedetailsresult['smtphost2']; ?>"  id="smtphost2" class="txtbox" /></td>
                                    </tr>
                                    <tr class="SMTP2 <?php if($this->resourcedetailsresult['google_email2'] != null){ echo 'hide'; }?>">
                                        <td><font style="color:red">*</font>SMTP User </td>
                                        <td><label>
                                                <input type="text" name="smtpuser2" id="smtpuser2" value="<?php echo $this->resourcedetailsresult['smtpuser2']; ?>"  class="txtbox" />
                                            </label></td>
                                    </tr>
                                    <tr class="SMTP2 <?php if($this->resourcedetailsresult['google_email2'] != null){ echo 'hide'; }?>">
                                        <td><font style="color:red">*</font>SMTP Password </td>
                                        <td><input type="password" name="smtppass2" id="smtppass2" value="<?php echo $this->resourcedetailsresult['smtppassword2']; ?>"  class="txtbox" /></td>
                                    </tr>
                                    <tr class="SMTP2 <?php if($this->resourcedetailsresult['google_email2'] != null){ echo 'hide'; }?>">
                                        <td><font style="color:red">*</font>SMTP Port </td>
                                        <td><input type="text" name="smtpport2" id="smtpport2" value="<?php echo $this->resourcedetailsresult['smtpport2']; ?>"  class="txtbox" /></td>
                                    </tr>
                                    <tr class="SMTP2 SendGrid2 <?php if($this->resourcedetailsresult['google_email2'] != null){ echo 'hide'; }?>">
                                        <td><font style="color:red">*</font>From Name </td>
                                        <td><input type="text"  name="fromname2" id="fromname2" value="<?php echo $this->resourcedetailsresult['fromname2']; ?>"  class="txtbox" /></td>
                                    </tr>
                                    <tr class="SMTP2 SendGrid2 <?php if($this->resourcedetailsresult['google_email2'] != null){ echo 'hide'; }?>">
                                        <td><font style="color:red">*</font>From Email </td>
                                        <td><input type="text"  name="fromemail2" id="fromemail2" value="<?php echo $this->resourcedetailsresult['fromemail2']; ?>"  class="txtbox" /></td>
                                    </tr>
                                    <tr class="SMTP2 SendGrid2">
                                        <td>Bcc Email (optional) </td>
                                        <td><input type="text"  name="bccemail2" id="bccemail2" value="<?php echo $this->resourcedetailsresult['bccemail2']; ?>"  class="txtbox" /></td>
                                    </tr>
                                    <tr class="SMTP2 <?php if($this->resourcedetailsresult['google_email2'] != null){ echo 'hide'; }?>">
                                        <td><font style="color:red">*</font>Email Type </td>
                                        <td><input type="radio"  name="emailtype2" id="emailtype2" <?php if($this->resourcedetailsresult['email_type2']=='ssl'){echo "checked='checked'";} ?> value="ssl"  />SSL&nbsp;
                                                   <input type="radio"  name="emailtype2" id="emailtype2" value="tls" <?php if($this->resourcedetailsresult['email_type2']=='tls'){echo "checked='checked'";} ?>  />TLS<input type="radio"  name="emailtype2" id="emailtype2" value="none" <?php if($this->resourcedetailsresult['email_type2']=='none'){echo "checked='checked'";} ?>  />None</td>
                                    </tr>
                                    <tr class="SMTP2 SendGrid2 <?php if($this->resourcedetailsresult['google_email2'] != null){ echo 'hide'; }?>">
                                        <td></td>
                                        <td>
                                            <div style='background-color: lightgray; width: 300px;'>
                                                <input name='firstTestEmail' id='secondTestEmail' style='width: 200px;' />
                                                <input type="button" value='Test SMTP' onclick="SMTPTest(true)" />
                                            </div>
                                        </td>
                                    </tr>
                                    <tr class="Gmail2">
                                        <td>*GMail Connect (Preferred)</td>
                                        <td>
                                            <?php if($this->resourcedetailsresult['google_email2'] == null){ ?>
                                            <table>
                                                <tr>
                                                    <td>
                                                        <a href="https://accounts.google.com/o/oauth2/v2/auth?access_type=offline&response_type=code&client_id=<?php echo $this->client_id; ?>&redirect_uri=<?php echo urlencode($this->site_url.'/index.php?m=dripMarket&a=showconfig&smtp=2'); ?>&scope=https%3A%2F%2Fwww.googleapis.com%2Fauth%2Fgmail.send%20https%3A%2F%2Fwww.googleapis.com%2Fauth%2Fuserinfo.profile%20https%3A%2F%2Fwww.googleapis.com%2Fauth%2Fuserinfo.email">
                                                            <img src="images/mru/btn_google_signin_dark_pressed_web.png" alt=""/>
                                                        </a>
                                                    </td>
                                                    <td style="width: 185px;">
                                                        <img src="images/mru/info_icon.png" style="width: 14px;">
                                                        <span style="color: dimgray; font-size: 10px; font-weight: normal;">We will use google authentification to send email to candidates.</span>
                                                    </td>
                                                </tr>
                                            </table>
                                            <?php } else{?>
                                                <a class="GmailDisconnect2" href="index.php?m=dripMarket&a=showconfig&smtp=-2" style="color: red">Disconnect <?php echo $this->resourcedetailsresult['google_email2']; ?></a>
                                            <?php }?>
                                        </td>
                                    </tr>

                                    <tr>
                                        <td>&nbsp;</td>
                                        <td><input type="image" src="images/save.gif" name="sub" width="105" height="43" /></td>
                                    </tr>
                                    <tr>
                                        <td>&nbsp;</td>
                                        <td>&nbsp;</td>
                                    </tr>
                                </table>   
                            </form>  
                        </td>
                    </tr>

                </table>

            </div>

        </div>        
        <script type="text/javascript">// <![CDATA[
            var countries = new ddtabcontent("countrytabs")
            countries.setpersist(true)
            countries.setselectedClassTarget("link") //"link" or "linkparent"
            countries.init()
            // ]]>
            $( document ).ready(function() {
                if("<?php echo $this->resourcedetailsresult['sendgrid']; ?>" == "1"){
                    $(".SMTP,.Gmail").hide();
                    $(".SendGrid").show();
                }
                if($(".GmailDisconnect").length > 0){
                    $('.SMTP,.SendGrid').hide();
                    $('.Gmail').show();
                }
                if("<?php echo $this->resourcedetailsresult['sendgrid2']; ?>" == "1"){
                    $(".SMTP2,.Gmail2").hide();
                    $(".SendGrid2").show();
                }
                if($(".GmailDisconnect2").length > 0){
                    $('.SMTP2,.SendGrid2').hide();
                    $('.Gmail2').show();
                }
            });
        </script>   
        <!--tabs end-->	
    </div>
</div>
<div id="bottomShadow"></div>
<?php TemplateUtility::printFooter(); ?>
