<!DOCTYPE HTML PUBLIC "-//W3C//DTD HTML 4.01 Transitional//EN" "http://www.w3.org/TR/html4/loose.dtd">
<html>
 <head>
  <title>Image Dialog</title>
  <meta http-equiv="Content-Type" content="text/html; charset=ISO-8859-1">
 </head>

 <body>
<?php
	// In my system, a function is called that returns the file names of images in a certain format
	// in an array. All images that are in the sample array below are known to exist, etc. and this
	// matches the structure of the return value from the function... (this assumes that the function
	// did return an array with at least one element)

    // this is an example of what would be used
    $images=array(
    	'test.jpg',
    	'test.png',
    	'test.gif'
    );
    $num_images=count($images);

    // loop through and display all current images
    echo '<table cellpadding="2" cellspacing="0" border="0">',"\n";
    foreach($images as $num=>$file){
        $img_src=$CFG['paths']['user_images'].'/'.$file;
        $size=getimagesize($CFG['paths']['sys_path'].$img_src);
        echo '<tr';
        if($num%2) echo ' style="background-color: #dfdfdf"';
        echo '><td><img src="http://',$_SERVER['HTTP_HOST'],$CFG['paths']['site_path'],$img_src,'?id=',md5(time()),'" ',$size[3],' alt="" /></td>',
            '<td valign="top"><a href="javascript:;" onClick="opener.editor1.addImage(\'http://',$_SERVER['HTTP_HOST'],$CFG['paths']['site_path'],$img_src,'\')">Insert Image</a></td></tr>';
    }
?>
 </body>
</html>
