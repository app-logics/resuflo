<?php
/*
KOIVI TTW WYSIWYG Editor Copyright (C) 2004 Justin Koivisto
Version 3.2.4
Last Modified: 4/3/2006

    This library is free software; you can redistribute it and/or modify it
    under the terms of the GNU Lesser General Public License as published by
    the Free Software Foundation; either version 2.1 of the License, or (at
    your option) any later version.

    This library is distributed in the hope that it will be useful, but
    WITHOUT ANY WARRANTY; without even the implied warranty of MERCHANTABILITY
    or FITNESS FOR A PARTICULAR PURPOSE. See the GNU Lesser General Public
    License for more details.

    You should have received a copy of the GNU Lesser General Public License
    along with this library; if not, write to the Free Software Foundation,
    Inc., 59 Temple Place, Suite 330, Boston, MA 02111-1307 USA 

    Full license agreement notice can be found in the LICENSE file contained
    within this distribution package.

    Justin Koivisto
    justin.koivisto@gmail.com
    http://koivi.com
*/
    // prevent some servers from issuing a parse error because of short_open_tags = On
    echo '<?xml version="1.0" encoding="iso-8859-1"?>',"\n";
?>
<!DOCTYPE html PUBLIC "-//W3C//DTD XHTML 1.0 Transitional//EN"
    "http://www.w3.org/TR/xhtml1/DTD/xhtml1-transitional.dtd">
<html xmlns="http://www.w3.org/1999/xhtml">
 <head>
  <title>Through The Web Rich Text (WYSIWYG) Editor</title>
  <meta http-equiv="Content-Type" content="text/html; charset=iso-8859-1" />
  <meta name="description" content="Rich Text (WYSIWYG) HTML Through-The-Web web content editor for Microsoft Internet Explorer and Gecko-based browsers using Object Oriented JavaScript." />
  <script type="text/javascript" src="editor.js"></script>
  <style type="text/css">
   @import url(http://koivi.com/styles.css);
   body{
    font: normal 1em/1.7em Verdana, sans-serif;
   }
   ul{
       list-style-type: none;
   }
   li h1{
    font: bold 1.8em Verdana, sans-serif;
    margin:0px;
    color: #000;
    border: 0;
    background-color: transparent;
   }
   li h2{
    font: bold 1.6em Verdana, sans-serif;
    margin:0px;
    color: #000;
    border: 0;
    padding: 0;
    background-color: transparent;
   }
   li h3{
    font: bold 1.4em Verdana, sans-serif;
    margin:0px;
    color: #000;
    background-color: transparent;
   }
   li h4{
    font: bold 1.2em Verdana, sans-serif;
    margin:0px;
    color: #000;
    background-color: transparent;
   }
   li h5{
    font: bold 1em Verdana, sans-serif;
    margin:0px;
    color: #000;
    background-color: transparent;
   }
   li h6{
    font: bold .8em Verdana, sans-serif;
    margin:0px;
    color: #000;
    background-color: transparent;
   }
   .butClass{    
     border: #dfdfdf 1px solid;
     cursor: pointer;
   }
   li p{
    margin:0px;
    padding:0px;
   }
  </style>
  <!--[if lt IE 7]><script src="/ie7/ie7-standard.js" type="text/javascript"></script><![endif]-->
 
<script src="http://www.google-analytics.com/urchin.js" type="text/javascript">
</script>
<script type="text/javascript">
_uacct = "UA-207722-1";
urchinTracker();
</script>
 </head>


 <body>
  <div id="container">
   <div id="intro">
    <h1>Rich Text (WYSIWYG) HTML Through-The-Web Editor</h1>
    <p>
     On this page is an example of a web content editor for use in custom Content Management Systems (CMS).
     This editor was designed and tested to work with the following browsers:
    </p>
    <ul>
     <li>Mozilla 1.3.1+ (Linux, PC &amp; Mac)</li>
     <li>Netscape 7.1+ (Linux, PC &amp; Mac)</li>
     <li>Firefox 1.0+ (Linux, PC &amp; Mac)</li>
     <li>Internet Explorer 5.5+</li>
     <li>Epiphany 1.6.5 (Linux)</li>
    </ul>
    <p>Also, the <a href="safari-beta.php">beta version</a> actually partially works with Safari 2.0 (412) as well! See that page for more comments and features including language translations for interface elements.</p>
    <p>The editor can be used to visually create content that can be placed on a web site where the user only needs to have familiarity with a word processor.</p>
    <p>
     As initially released on Jan 28, 2005, this editor is programmed using Javascript in an object oriented way. In other words, the editor is no longer
     just a bunch of functions that work together to accomplish this. Instead, the editor.js file now defines a class that can be used to create editor objects
     with.
    </p>
    
    <h1>Multiple Instances</h1>
    <p>Previously I said it could be done, but never had the time to really test it out. Well, I was wrong (nobody's perfect!).</p>
    <p>
     <b>However</b>, now that the source for the editor has been converted to OO Javascript, you can put as many &quot;instances&quot; of the editor on a
     page as you want!. Check out the information to the left for more information about use this feature.
    </p>
   </div>

   <div>
    <h1>TTW HTML Editor Interface</h1>
    <form method="post" onsubmit="editor1.prepareSubmit();" action="<?php echo $_SERVER['REQUEST_URI'] ?>">
     <script type="text/javascript">
      var editor1 = new WYSIWYG_Editor('editor1');
      editor1.display();
     </script>
     <noscript>
      <p style="notSupported">
       Your browser does not support Javascript with your current setings. I am unable to display the editor to you. Check your settings
       and enable Javascript, or use one of the supported web browsers (listed in the right column) to view this page for a demonstration.
      </p>
     </noscript>
     <input type="submit" value="Submit" name="update" /><br /><br />
    </form>
   </div>
   
<?php
    if(isset($_POST['editor1_content'])){
?>
   <div id="pageResults">
    <h1>TTW WYSIWYG Editor Results</h1>
    <p>
     Below is the code that your browser generated using this editor. Note that each browser may generate different code. I have even witnessed different
     versions or platforms of the same browser generating different HTML results.
    </p>
    <div class="example"><?php echo nl2br(htmlentities($_POST['editor1_content'])); ?></div>
   </div>
<?php
    }
?>

   <div>
    <h1>Using the TTW WYSIWYG Editor</h1>
    <p>
     Since I get a lot of requests for code examples on how to use the editor, I will give you some instructions on how to use it now that the source
     code has been converted to object-oriented Javascript. (Of course, you could have just used the &quot;View Source&quot; option in your browser.)
    </p>
    <h2>Displaying the Editor</h2>
    <ol>
     <li>
      <p>
       The first step in getting the editor to work is to <a href="/WYSIWYG-Editor.zip">download the files</a>. Once downloaded, extract the zip archive
       so you can actually use the files. (Yes, some people actually forgot to do that!)
      </p>
     </li>
     <li>
      <p>
       Next, create the HTML in which you want to use the editor. In the <code>head</code> of that document, use the following lines to include the Javascript
       WYSIWYG_Editor class definition file (changing directory names where appropriate to fit your configuration):
      </p>
      <pre class="example">&lt;script type=&quot;text/javascript&quot; src=&quot;editor.js&quot;&gt;&lt;/script&gt;</pre>
     </li>
     <li>
      <p>
       Find the location in your HTML source where you want the editor interface to be displayed. The editor is made to be displayed inside an HTML form so that
       you can actually send the data somewhere else. In your <code>form</code> tag, you will need to add the <code>onsubmit</code> atribute and tell it to call
       a class method before submit (or you will get no data submitted).
      </p>
      <p>Your HTML form tag should look similar to: <code>&lt;form onsubmit=&quot;editor1.prepareSubmit()&quot; method=&quot;post&quot;&gt;</code></p>
      <p>
       Now, insert the following in the place that you want the interface to be displayed (changing variable names to suit your needs):
      </p>
      <pre class="example">
&lt;script type=&quot;text/javascript&quot;&gt;
    var editor1 = new WYSIWYG_Editor('editor1');
    editor1.display();
&lt;/script&gt;
      </pre>
      <p>
       The class will take more optional arguements, and there are also a number of class properties that you can edit to make the editor
       display differently. For now, you will have to view the Javascript source code to find those. Eventually, I may be able to find time to
       outline each of the properties on this page as well.
      </p>
     </li>
    </ol>
    <p>
     <b>That's it!</b> Seriously, that is all you have to to display the editor on your page.
    </p>

    <h2>Loading Content Into The Editor</h2>
    <p>
     So now that this page has become a bit popular, I am starting to get a few email messages (7 or 8 a day) asking how to put content into
     the editor when it is displayed. Obvisouly, people are looking to integrate this into some sort of CMS.
    </p>
    <p>
     In order to do this, you need to be sure that there are no newline or carriage return characters in the content (and that there are no
     <code>&lt;/script&gt;</code> tags in it. (You may be able to get around the script end tag by substituting the brackets with something else,
     but I haven't had a reason to do so as of yet.) To do this in PHP, we can use a simple regex to accomplish this:
    </p>
    <pre class="example">
&lt;?php
    $content=addslashes(preg_replace('`[\r\n]`','\n',$content));
?&gt;
    </pre>
    <p>
     What the code does above is looks for all "\r" and "\n" characters and replaces them with the literal string '\n'. This way, when you generate
     the javascript code, the content will be all on one line so that your browser doesn't get a javascript error. The <code>addslashes</code> function
     will also escape any single quote marks found in the content by prepending a backslash in front of it. This prevents your content's quote mark
     from ending the javascript string prematurely.
    </p>
    <p>
     Now that the content is ready to be used in the JavaScript source inside the HTML output, we can load the data into the editor. To do this,
     you just need to passed the content to be loaded as an argument to the constructor. This example is done using PHP to generate the HTML
     output, so if you are using other languages, you will need to adapt this to your needs.  All that is needed to do is change our original code
     above for displaying the editor to the following:
    </p>
    <pre class="example">
&lt;script type=&quot;text/javascript&quot;&gt;
    var editor1 = new WYSIWYG_Editor('editor1','&lt;?php echo $content ?&gt;');
    editor1.display();
&lt;/script&gt;
    </pre>

    <h2>Displaying Multiple Instances of the Editor</h2>
    <p>Want to have more editors on this page? If so, all you'd have to do is include the following where you want it to display:</p>
    <pre class="example">
&lt;script type=&quot;text/javascript&quot;&gt;
    var editor2 = new WYSIWYG_Editor('editor2');
    editor2.display();
&lt;/script&gt;
    </pre>
    <p>
     Of course, you'd have to edit variable names to include more editor interfaces as well as add the correct method calls to the HTML form tag as well.
     Each of the editors will now operate independent of each other. It really doesn't get much simpler than this!
    </p>
    <h2>Accessing the Submitted Code</h2>
    <p>
     When you submit the form that contains the editor interface, you should be able to access the generated HTML as submitted in <b>editor1</b>_content where
     &quot;editor1&quot; is the variable name (and first arguement passed to the constructor) you used to create the editor.
    </p>
    <p>
     If you are finding that the variable is not submitted, be sure that your form does not have another element with the same <code>name</code> attribute as
     your editor's Javascript variable name. If it is submitted, but is always empty, check the same thing as well as being sure that you have hadded the correct
     <code>onsbumit</code> value.
    </p>
   </div>

   <div>
    <h1>Editor Content Formatting Features</h1>
    <p>Below are descriptions of the features included with this editor. Note that text formatting may change according to style sheets specified in the programming of the website the content will be placed in.</p>

    <div>
     <h2>Font Family</h2>
     <p>There are five different generic font families to choose from for your content. These styles may display differently on different browsers as well as on different computers. These generic font families include:</p>
     <ul>
      <li><span style="font-family: cursive;">Cursive</span></li>
      <li><span style="font-family: fantasy;">Fantasy</span></li>
      <li><span style="font-family: sans-serif;">Sans Serif</span></li>
      <li><span style="font-family: serif;">Serif</span></li>
      <li><span style="font-family: monospace;">Typewriter</span></li>
     </ul>
     <p style="color: #c00; background-color: transparent;">Note that what is displayed for these font families depends on browser/system settings.</p>
    </div>

    <div>
     <h2>Font Size</h2>
     <p>There are five different relative font sizes to choose from for your content. These sizes may display differently on different browsers as well as on different computers. These relative font sizes include:</p>
     <ul>
      <li><font size="-2">X Small</font></li>
      <li><font size="-1">Small</font></li>
      <li><font size="+0">Medium</font></li>
      <li><font size="+1">Large</font></li>
      <li><font size="+2">X Large</font></li>
     </ul>
    </div>

    <div>
     <h2>Text Styles</h2>
     <p>There are eight different text styles to choose from for your content. They include:</p>
     <ul>
      <li><p>Normal</p></li>
      <li><h1>Heading 1</h1></li>
      <li><h2>Heading 2</h2></li>
      <li><h3>Heading 3</h3></li>
      <li><h4>Heading 4</h4></li>
      <li><h5>Heading 5</h5></li>
      <li><h6>Heading 6</h6></li>
      <li><address>Address</address></li>
     </ul>
    </div>

    <div>
     <h2>Font Color and Highlights</h2>
     <p>There is a palette of 70 colors to choose from for the color of the type as well as the background color of the type. The examples below show what a selecting blue color for each would look like.</p>
     <ul>
      <li><span style="color: #006; background-color: white;">Font Color</span></li>
      <li><span style="color: black; background-color: #00c;">Highlight Color</span></li>
     </ul>
    </div>

    <div>
     <h2>Other Text Decorations and Styles</h2>
     <p>There are additional text decorations and styles such as <b>bold</b>, <u>underline</u>, <i>italic</i>, <sup>superscript</sup> and <sub>subscript</sub> that are available through the use of buttons.</p>
    </div>

    <div>
     <h2>Content Alignment</h2>
     <p>The user of this editor can also specify the content to be justified left, center or right. There are also buttons to allow a user to indent and outdent content content blocks.</p>
     <p>There is a button to insert tables. When clicked, the user will be prompted for the number of columns per row, then the number of rows in the table. This option allows users to create tabulated text alignment.</p>
    </div>

    <div>
     <h2>Web Elements</h2>
     <p>Within this form, there are buttons for creating web elements like links, lists, tables and horizontal rules.</p>
     <p>Links are created by selecting some content, clicking the Hyperlink button, and entering the fully-qualified URL when prompted (example: <a href="http://koivi.com/WYSIWYG-Editor/">http://koivi.com/WYSIWYG-Editor/</a>). Removing a link from content is as simple as selecting the content and clicking the button to remove the link.</p>
     <p>There are two different types of lists: ordered and unordered.  Unordered lists are generally referred to as bulleted lists.  Note that depending on style sheets in your site's programming that the bullets or numbers may appear differently.</p>
     <p>An example of a horizontal rule can be found just under the submit button toward the top of this page.</p>
    </div>

    <div>
     <h2>View Source Code</h2>
     <p>For those of you who know HTML, you may want to tweak the code before submitting. The Toggle Mode option that will allow you switch between HTML source editing or the WYSIWYG interface.</p>
    </div>

    <div>
     <h2>Editor Style Sheets</h2>
     <p>What to use this editor for a web site that uses CSS to define text styles, but the people using the editor keep changing the colors from default? If so, use a style sheet so that the web site's text styles are reflected in the editor! Simply pass the path to the style sheet as the 6<sup>th</sup> parameter to the WYSIWYG_Editor function or set the <code>editor1.stylesheet</code> property.</p>
    </div>

    <div>
     <h2>Spell Check with SpellerPages 0.5.1</h2>
     <p>Need to allow for spell checking? Download <a href="http://spellerpages.sourceforge.net/">SpellerPages 0.5.1</a>, and simply pass the path to it as the 7<sup>th</sup> parameter to WYSIWYG_Editor or set the <code>editor1.spell_path</code>.</p>
    </div>
   </div>
   
   <div>
    <h1>Limitations</h1>
    <p style="text-decoration: line-through">There is no support for inserting images in the editor. The purpose of this editor is to allow site owners to do quick text formatting to their site.  Since this is being developed for a custom CMS, the data submitted via this form would be stored in a database - images would be handled in a different area of the CMS. Once the CMS is completed, I may make it public if there is enough interest in it.</p>
    <p>This editor does support the adding of images into the content window. However, at this time, I have the option disabled in the editor.js file because of the need for a server-side script (my implementation is done in PHP). For those of you who are curious or want to implement the image control you must first set the <code>this.image_button = true;</code> in editor.js (around line 135). Then in <code>WYSIWYG_Editor.prototype.insertImage</code>, there is a reference to <code>/_include/back_end/image_dialog.php</code> replace this reference with the page that you want to see in the popup. This page (in my implementation) is simply a list of images with "Insert" links. These links look like <code>&lt;a href="javascript:;" onClick="opener.page.addImage(\'http://example.com/path-to/image.jpg\')"&gt;Insert Image&lt;/a&gt;</code>. That's all you need to enable the image functions.</p>
    <p>You can insert <code>script</code> tags into the content for processing, however, you cannot display them within the editor by setting the editor's content property without first modifying it. When parsing the content and finding <code>&quot;&lt;/script&gt;&quot;</code>, browsers take that as the literal end of the script - even when it is inside the function call. To get around this, you will have to break up any <code>&lt;script&gt;</code> tags so that they look like: <code>&lt;scr'+'ipt&gt;</code></p>
    <p>Also note that when you are providing the content to be displayed in the editor as a method arguement, it must all be on one line and escaped just like all other JavaScript function calls and string assignments. Don't forget to change all single quotes to <code>\'</code> and replace all line feeds and carriage returns with <code>\n</code> and <code>\r</code> respectively.</p>
   </div>
   
   <div>
    <h1>Updates</h1>
    <ul>
     <li><p><b>UPDATE (9/12/2006):</b> I just received a couple reports about "Access denied" errors in MSIE. Initially, I thought this was due to their browser settings; then my browser started doing it as well. Seems that an update to MSIE/windows at some point recently causes this to happen, and makes the editor useless in MSIE. However, by simply removing the SRC attribute from the IFRAME tag in editor.js line 371, it works again. This whill, however, cause problems with secure/insecure content when using with HTTPS protocol. Fix suggestions welcomed and appreciated.</p></li>
     <li><p><b>UPDATE (4/3/2006):</b> A fix was appled that helps MSIE from complainint about access denied errors due to caching. <a href="http://technet2.microsoft.com/WindowsServer/en/Library/8e06b837-0027-4f47-95d6-0a60579904bc1033.mspx">More Info</a> Thanks again to Andr�.</p></li>
     <li><p><b>UPDATE (4/2/2006):</b> A fix was appled that keeps MSIE from complainint about non-secure items over an SSL connection when using the editor by adding a SRC attribute to the generated iframe element. Thanks Andr� for finding this and suggesting the fix!</p></li>
     <li><p><b>UPDATE (9/2/2005):</b> Safari anyone? <a href="safari-beta.php">Check out the BETA verions that supports some function in SAFARI!</a> Granted, it's not nearly as featureful as the one on this page, but it's a definate first step to supporting Safari...</p></li>
     <li><p><b>UPDATE (8/25/2005):</b> I've decided to give a little bit of documentation about adding images on this page. There haven't been any released updated to the code; however, I will probably be doing a large rewrite (already started) so that browsers like Safari are supported.</p></li>
     <li><p><b>UPDATE (4/27/2005):</b> The editor now supports the use of a style sheet for the iframe element. This allows you to make the editor content look how it would on the page the user is editing.</p></li>
     <li><p><b>UPDATE (4/27/2005):</b> The editor now supports the use of SpellerPages 0.5.1 for a spell check feature. Eventually, I may update this article to demonstrate the two new features added today, but for now you're on your own.</p></li>
     <li><p><b>UPDATE (2/9/2005):</b> I have managed to find time to get rid of the browser UA checking. This editor is now self-sufficient - no need for the Browser Detect Lite script anymore.</p></li>
     <li><p><b>MAJOR UPDATE (1/28/2005):</b> I've received many requests and questions regarding the ability to use multiple instances of this editor on a single web page. The previous versions allowed you to display multiple editors, but they would not function properly. The key word here is "instances." That's right! The new version of this editor has had some major work done to the code to make it Object Oriented JavaScript. This page has now been updated to reflect that as well as give some examples on its use. There is still some work to be done within the Javascript source as well as getting some more documentation on this page. Eventually I'll get around to it.</p></li>
     <li><hr /></li>
     <li><p><b>UPDATE (1/11/2005):</b> Thanks to Shimon Young for spotting a bug! If the form is submitted while in source view, the editor view is toggled before processing information so that the HTML code is submitted correctly.</p></li>
     <li><p><b>UPDATE (12/8/2004):</b> Table insertion support has been added for MSIE browsers.</p></li>
     <li><p><b>UPDATE (5/17/2004):</b> The editor now has the ability to display a textarea (and a warning to the user) if the browser does not support the editor. There were a few changes to the script in order to facilitate this, including the ability to easily set the height, width, submitted field name and default content of the editor.</p></li>
     <li><p><b>UPDATE (5/11/2004):</b> OK, the problem I was having is now fixed. Thanks to Alberto Berselli for helping out on this. The hidden textarea was changed to a hidden input type with the name "html_content" (which is the variable that will hold the HTML code submitted) and uses the id "iView_content" in the javascript code.</p>
         <p>The next step of development for this project is to create a loading script that will display either this editor, or a plain textarea box for browsers that do not support it. The goal of this project is to integrate the editor into a CMS that I have been building (which may or may not be released to the public).</p></li>
     <li><p><b>UPDATE (5/10/2004):</b> MSIE font foreground and highlight colors now work! That makes this a nearly complete cross-browser TTW (Through the Web - or works inside a browser) WYWSIWYG editor. I am currently having problems with getting the iView_content field to be set on form submission, but I am going to try and integrate this into a CMS I am working with, so this development should take much less time than the last fix!</p></li>
    </ul>
   </div>

   <div>
    <h1>Questions/Comments</h1>
    <p>If you have questions or comments on any of the programming or description of this page, please <a href="mailto:justin.koivisto@gmail.com?subject=TTW Editor">direct them here</a>.</p>
   </div>

   <div>
    <h1>Source Code</h1>
    <p>You can retrieve a copy of the code used for this demonstration in <a href="/WYSIWYG-Editor.zip">Zip format</a>. This includes this page as well as the Javascript and images required. This page has a reference to a file called &quot;site_menu&quot; that you can safely remove or ignore.</p>
   </div>

<script type="text/javascript"><!--
google_ad_client = "pub-6879264339756189";
google_alternate_ad_url = "http://koivi.com/google_adsense_script.html";
google_ad_width = 728;
google_ad_height = 90;
google_ad_format = "728x90_as";
google_ad_type = "text_image";
google_ad_channel ="7653137181";
google_color_border = "6E6057";
google_color_bg = "DFE0D0";
google_color_link = "313040";
google_color_url = "0000CC";
google_color_text = "000000";
//--></script>
<script type="text/javascript"
  src="http://pagead2.googlesyndication.com/pagead/show_ads.js">
</script>

  </div>

<?php include_once 'site_menu.php'; ?>

 </body>
</html>
