<?php /* $Id: Companies.tpl 3460 2007-11-07 03:50:34Z brian $ */ ?>
<?php TemplateUtility::printHeader('Companies', array('js/highlightrows.js', 'js/export.js', 'js/dataGrid.js')); ?>
<?php TemplateUtility::printHeaderBlock(); ?>

<?php TemplateUtility::printTabs($this->active); ?>

<link rel="stylesheet" href="css/tab.css" />
<link rel="stylesheet" href="css/form.css" />
<script type="text/javascript" src="javascript/tab.js"></script>

<script type="text/javascript" src="javascript/validations.js"></script>
<!-- TinyMCE -->
<script type="text/javascript" src="jscripts/tiny_mce/tiny_mce.js"></script>
<script type="text/javascript">
	tinyMCE.init({
		// General options
		mode : "textareas",
		theme : "advanced",
		plugins : "pagebreak,style,layer,table,save,advhr,advimage,advlink,emotions,iespell,insertdatetime,preview,media,searchreplace,print,contextmenu,paste,directionality,fullscreen,noneditable,visualchars,nonbreaking,xhtmlxtras,template,wordcount,advlist,autosave",

		// Theme options
		theme_advanced_buttons1 : "save,newdocument,|,bold,italic,underline,strikethrough,|,justifyleft,justifycenter,justifyright,justifyfull,styleselect,formatselect,fontselect,fontsizeselect",
		theme_advanced_buttons2 : "cut,copy,paste,pastetext,pasteword,|,search,replace,|,bullist,numlist,|,outdent,indent,blockquote,|,undo,redo,|,link,unlink,anchor,image,cleanup,help,code,|,insertdate,inserttime,preview,|,forecolor,backcolor",
		theme_advanced_buttons3 : "tablecontrols,|,hr,removeformat,visualaid,|,sub,sup,|,charmap,emotions,iespell,media,advhr,|,print,|,ltr,rtl,|,fullscreen",
		theme_advanced_buttons4 : "insertlayer,moveforward,movebackward,absolute,|,styleprops,|,cite,abbr,acronym,del,ins,attribs,|,visualchars,nonbreaking,template,pagebreak,restoredraft",
		theme_advanced_toolbar_location : "top",
		theme_advanced_toolbar_align : "left",
		theme_advanced_statusbar_location : "bottom",
		theme_advanced_resizing : true,

		// Example content CSS (should be your site CSS)
		// using false to ensure that the default browser settings are used for best Accessibility
		// ACCESSIBILITY SETTINGS
		content_css : false,
		// Use browser preferred colors for dialogs.
		browser_preferred_colors : true,
		detect_highcontrast : true,

		// Drop lists for link/image/media/template dialogs
		template_external_list_url : "lists/template_list.js",
		external_link_list_url : "lists/link_list.js",
		external_image_list_url : "lists/image_list.js",
		media_external_list_url : "lists/media_list.js",

		// Style formats
		style_formats : [
			{title : 'Bold text', inline : 'b'},
			{title : 'Red text', inline : 'span', styles : {color : '#ff0000'}},
			{title : 'Red header', block : 'h1', styles : {color : '#ff0000'}},
			{title : 'Example 1', inline : 'span', classes : 'example1'},
			{title : 'Example 2', inline : 'span', classes : 'example2'},
			{title : 'Table styles'},
			{title : 'Table row 1', selector : 'tr', classes : 'tablerow1'}
		],

		// Replace values for the template plugin
		template_replace_values : {
			username : "Some User",
			staffid : "991234"
		}
	});
</script>
<!-- /TinyMCE -->







 
    <div id="main">
   
        <div id="contents">
            
			<!--tabs Starts-->		 
<ul id="countrytabs" class="shadetabs" style="padding-top:20px;">
<li><a class="selected" href="javascript:void(0)" onclick="javascript:goToURL('<?php echo(RESUFLOUtility::getIndexName()); ?>?m=dripMarket&amp;a=showcompaign');" rel="country1">Campaigns</a></li>
<li><a href="javascript:void(0)" onclick="javascript:goToURL('<?php echo(RESUFLOUtility::getIndexName()); ?>?m=dripMarket&amp;a=showconfig');" >Configuration</a></li>
<li><a  href="javascript:void(0)" onclick="javascript:goToURL('<?php echo(RESUFLOUtility::getIndexName()); ?>?m=dripMarket&amp;a=questionnaire');">Questionnaire</a></li>
</ul>
<div style="border: 0px solid #32004b; width:900px; height:auto; margin-bottom:1em; padding:0px;">
<div id="country1" class="tabcontent">
<table width="100%" border="0" cellspacing="0" cellpadding="0" bgcolor="#fff" >
  <tr>
    <td colspan="4" bgcolor="#f8f8f8">&nbsp;</td>
  </tr>
  <tr>
    <td colspan="4" bgcolor="#f8f8f8" style="padding-left:10px;"><a href="javascript:void(0)" onclick="javascript:goToURL('<?php echo(RESUFLOUtility::getIndexName()); ?>?m=dripMarket&amp;a=createcampaign');" ></a></td>
    </tr>
  <tr>
    <td colspan="4" bgcolor="#fff" >
                                           <form name="frm1" id="frm1" action="<?php echo(RESUFLOUtility::getIndexName()); ?>?m=dripMarket&amp;a=saveemail&id=<?php echo $this->cid; ?>" onsubmit="return emailadd()" method="post">
	<table width="100%" border="0" cellspacing="2" cellpadding="2" bgcolor="#f8f8f8";>
                      <tr>
    <td colspan="2" >
<a  href="javascript:void(0)" href="javascript:void(0)" onclick="javascript:goToURL('<?php echo(RESUFLOUtility::getIndexName()); ?>?m=dripMarket');" ><< Back to main page</a>
    </td>
  </tr>
<!--      <tr>
        <td width="29%">Candidate Status </td>
        <td width="71%"><label><?php echo $this->resourcedetails['short_description']; ?></label></td>
      </tr>-->
      <tr>
        <td>Send Email after </td>
        <td><table width="80%" border="0" cellspacing="0" cellpadding="0">
          <tr>
            <td width="10%"><label>
              <input type="text" name="sendemailon" id="sendemailon" value="<?php
              if(isset($this->resourcedetails['sendemailon'])){
              echo $this->resourcedetails['sendemailon'];} ?>"  class="txtbox_small" size="3"/>
            </label></td>
            <td width="30%" align="center">Day After Creation </td>
            <td width="17%"><select name="selecthour" id="selecthour" class="txtbox_s">
                       <option value="-1" >HH</option>     
                                  <?php for($i=0;$i<24;$i++)
                     {
              if(isset($this->resourcedetails['hour']))
              {
                if($this->resourcedetails['hour']==$i)
                {
                 $selectedh="selected";   
                }else
                {
                          $selectedh="";  
                    
                }
             } 
                        if($i<10)
                        {
                          echo "<option value='$i'  $selectedh >0".$i."</option>";
                        }else
                        {
                           echo "<option value='$i'  $selectedh >$i</option>";
                        }
                        
                     }
                        ?>
                         </select></td>
            <td width="23%" align="center"><select name="selectmin" id="selectmin" class="txtbox_s">
                     <option value="-1" >MM</option>
                     <?php for($i=0;$i<60;$i++)
                     {
                                   if(isset($this->resourcedetails['min']))
              {
                if($this->resourcedetails['min']==$i)
                {
                 $selectedm="selected";   
                }else
                {
                          $selectedm="";  
                    
                }
             } 
                           if($i<10)
                        {
                          echo "<option value='$i'  $selectedm >0".$i."</option>";
                        }else
                        {
                           echo "<option value='$i'  $selectedm >$i</option>";
                        }
                        
                          
                     }
                        ?>
                           
                         </select></td>
            <td width="20%">
        
             
             
             
              <select name="selecttimezone" id="selecttimezone" class="txtbox" style="width:200px;">  
     <option value="GMT-12.0" <?php   if(isset($this->resourcedetails['timezone']))
              {if($this->resourcedetails['timezone']=="GMT-12.0")              { echo "selected";} } ?>>(GMT -12:00) Eniwetok, Kwajalein</option>  
 <option value="GMT-11.0" <?php if(isset($this->resourcedetails['timezone']))               {if($this->resourcedetails['timezone']=="GMT-11.0")              { echo "selected";} } ?>>(GMT -11:00) Midway Island, Samoa</option>     <option value="GMT-10.0" <?php if(isset($this->resourcedetails['timezone']))               {if($this->resourcedetails['timezone']=="GMT-10.0")              { echo "selected";} } ?>>(GMT -10:00) Hawaii</option>  
 <option value="GMT-9.0" <?php if(isset($this->resourcedetails['timezone']))               {if($this->resourcedetails['timezone']=="GMT-9.0")              { echo "selected";} } ?>>(GMT -9:00) Alaska</option>  
     <option value="GMT-8.0" <?php if(isset($this->resourcedetails['timezone']))               {if($this->resourcedetails['timezone']=="GMT-8.0")              { echo "selected";} } ?>>(GMT -8:00) Pacific Time (US & Canada)</option>  
      <option value="GMT-7.0" <?php if(isset($this->resourcedetails['timezone']))               {if($this->resourcedetails['timezone']=="GMT-7.0")              { echo "selected";} } ?>>(GMT -7:00) Mountain Time (US & Canada)</option>  
   <option value="GMT-6.0" <?php if(isset($this->resourcedetails['timezone']))               {if($this->resourcedetails['timezone']=="GMT-6.0")              { echo "selected";} } ?>>(GMT -6:00) Central Time (US & Canada), Mexico City</option>  
     <option value="GMT-5.0" <?php if(isset($this->resourcedetails['timezone']))               {if($this->resourcedetails['timezone']=="GMT-5.0")              { echo "selected";} } ?>>(GMT -5:00) Eastern Time (US & Canada), Bogota, Lima</option>  
      <option value="GMT-4.0" <?php if(isset($this->resourcedetails['timezone']))               {if($this->resourcedetails['timezone']=="GMT-4.0")              { echo "selected";} } ?>>(GMT -4:00) Atlantic Time (Canada), Caracas, La Paz</option>  
     <option value="GMT-3.5" <?php if(isset($this->resourcedetails['timezone']))               {if($this->resourcedetails['timezone']=="GMT-3.5")              { echo "selected";} } ?>>(GMT -3:30) Newfoundland</option>  
    <option value="GMT-3.0" <?php if(isset($this->resourcedetails['timezone']))               {if($this->resourcedetails['timezone']=="GMT-3.0")              { echo "selected";} } ?>>(GMT -3:00) Brazil, Buenos Aires, Georgetown</option>  
  <option value="GMT-2.0" <?php if(isset($this->resourcedetails['timezone']))               {if($this->resourcedetails['timezone']=="GMT-2.0")              { echo "selected";} } ?>>(GMT -2:00) Mid-Atlantic</option>  
     <option value="GMT-1.0" <?php if(isset($this->resourcedetails['timezone']))               {if($this->resourcedetails['timezone']=="GMT-1.0")              { echo "selected";} } ?>>(GMT -1:00 hour) Azores, Cape Verde Islands</option>  
      <option value="GMT+0.0" <?php if(isset($this->resourcedetails['timezone']))               {if($this->resourcedetails['timezone']=="GMT+0.0")              { echo "selected";} } ?>>(GMT) Western Europe Time, London, Lisbon, Casablanca</option>  
    <option value="GMT+1.0" <?php if(isset($this->resourcedetails['timezone']))               {if($this->resourcedetails['timezone']=="GMT+1.0")              { echo "selected";} } ?>>(GMT +1:00 hour) Brussels, Copenhagen, Madrid, Paris</option>  
     <option value="GMT+2.0" <?php if(isset($this->resourcedetails['timezone']))               {if($this->resourcedetails['timezone']=="GMT+2.0")              { echo "selected";} } ?>>(GMT +2:00) Kaliningrad, South Africa</option>  
      <option value="GMT+3.0" <?php if(isset($this->resourcedetails['timezone']))               {if($this->resourcedetails['timezone']=="GMT+3.0")              { echo "selected";} } ?>>(GMT +3:00) Baghdad, Riyadh, Moscow, St. Petersburg</option>  
     <option value="GMT+3.5" <?php if(isset($this->resourcedetails['timezone']))               {if($this->resourcedetails['timezone']=="GMT+3.5")              { echo "selected";} } ?>>(GMT +3:30) Tehran</option>  
     <option value="GMT+4.0" <?php if(isset($this->resourcedetails['timezone']))               {if($this->resourcedetails['timezone']=="GMT+4.0")              { echo "selected";} } ?>>(GMT +4:00) Abu Dhabi, Muscat, Baku, Tbilisi</option>  
      <option value="GMT+4.5" <?php if(isset($this->resourcedetails['timezone']))               {if($this->resourcedetails['timezone']=="GMT+4.5")              { echo "selected";} } ?>>(GMT +4:30) Kabul</option>  
      <option value="GMT+5.0" <?php if(isset($this->resourcedetails['timezone']))               {if($this->resourcedetails['timezone']=="GMT+5.0")              { echo "selected";} } ?>>(GMT +5:00) Ekaterinburg, Islamabad, Karachi, Tashkent</option>  
     <option value="GMT+5.5" <?php if(isset($this->resourcedetails['timezone']))               {if($this->resourcedetails['timezone']=="GMT+5.5")              { echo "selected";} } ?>>(GMT +5:30) Bombay, Calcutta, Madras, New Delhi</option>  
      <option value="GMT+5.75" <?php if(isset($this->resourcedetails['timezone']))               {if($this->resourcedetails['timezone']=="GMT+5.75")              { echo "selected";} } ?>>(GMT +5:45) Kathmandu</option>  
      <option value="GMT+6.0" <?php if(isset($this->resourcedetails['timezone']))               {if($this->resourcedetails['timezone']=="GMT+6.0")              { echo "selected";} } ?>>(GMT +6:00) Almaty, Dhaka, Colombo</option>  
      <option value="GMT+7.0" <?php if(isset($this->resourcedetails['timezone']))               {if($this->resourcedetails['timezone']=="GMT+7.0")              { echo "selected";} } ?>>(GMT +7:00) Bangkok, Hanoi, Jakarta</option>  
     <option value="GMT+8.0" <?php if(isset($this->resourcedetails['timezone']))               {if($this->resourcedetails['timezone']=="GMT+8.0")              { echo "selected";} } ?>>(GMT +8:00) Beijing, Perth, Singapore, Hong Kong</option>  
    <option value="GMT+9.0" <?php if(isset($this->resourcedetails['timezone']))               {if($this->resourcedetails['timezone']=="GMT+9.0")              { echo "selected";} } ?>>(GMT +9:00) Tokyo, Seoul, Osaka, Sapporo, Yakutsk</option>  
     <option value="GMT+9.5" <?php if(isset($this->resourcedetails['timezone']))               {if($this->resourcedetails['timezone']=="GMT+9.5")              { echo "selected";} } ?>>(GMT +9:30) Adelaide, Darwin</option>  
    <option value="GMT+10.0" <?php if(isset($this->resourcedetails['timezone']))               {if($this->resourcedetails['timezone']=="GMT+10.0")              { echo "selected";} } ?>>(GMT +10:00) Eastern Australia, Guam, Vladivostok</option>  
     <option value="GMT+11.0" <?php if(isset($this->resourcedetails['timezone']))               {if($this->resourcedetails['timezone']=="GMT+11.0")              { echo "selected";} } ?>>(GMT +11:00) Magadan, Solomon Islands, New Caledonia</option>  
      <option value="GMT+12.0" <?php if(isset($this->resourcedetails['timezone']))               {if($this->resourcedetails['timezone']=="GMT+12.0")              { echo "selected";} } ?>>(GMT +12:00) Auckland, Wellington, Fiji, Kamchatka</option>  
 </select> </td>
          </tr>
        </table></td>
      </tr>
      <tr>
        <td>Subject</td>
        <td><label>
        <input type="text" id="subject"  name="subject"  class="txtbox" value="<?php
              if(isset($this->resourcedetails['subject'])){
              echo $this->resourcedetails['subject'];} ?>" />
		 <input type="hidden" name="status" value="<?php echo $this->status; ?>" />
                   <input type="hidden" name="task" value="<?php echo $this->task; ?>" />
         <input type="hidden" name="editid" value="<?php echo $this->editid; ?>" />
             <input type="hidden" name="cid" value="<?php echo $this->cid; ?>" />
        </label></td>
      </tr>
      
      <tr>
        <td style="text-align:top">Message
          <br/><br/><b>Directions: </b>   <br/>$$NAME$$ for Name <br/>$$EMAIL$$ for EMAIL<br/>$$PHONE$$ for phone<br/>$$STARTDATE$$ for creation date<br/>$$CURDATE$$ for current date<br/>$$QUESTIONNAIRE$$ for Questionnaire page url
           <br/>$$UNSUBSCRIBE$$  For unsubscribe link
  	
     </td>
        <td><textarea name="txtmessage" id="txtmessage" rows="4" cols="40" style="height:300px;width:500px;" class="txtbox"><?php if(isset($this->resourcedetails['message'])){echo stripslashes($this->resourcedetails['message']);} ?></textarea></td>
      </tr>
      <tr>
        <td>&nbsp;</td>
        <td><input type="image" src="images/save.gif" width="105" height="43" /></td>
      </tr>
      <tr>
        <td>&nbsp;</td>
        <td>&nbsp;</td>
      </tr>
    </table>   
	</form>  
	</td>
  </tr>
    
</table>

</div>

</div>        
<script type="text/javascript">// <![CDATA[
var countries=new ddtabcontent("countrytabs")
countries.setpersist(true)
countries.setselectedClassTarget("link") //"link" or "linkparent"
countries.init()
// ]]></script>   
 <!--tabs end-->	
        </div>
    </div>
    <div id="bottomShadow"></div>
<?php TemplateUtility::printFooter(); ?>
