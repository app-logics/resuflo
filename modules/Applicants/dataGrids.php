<?php

//TODO: License
error_reporting(E_ALL ^ E_NOTICE);
include_once('lib/Applicants.php');

class applicantsListByViewDataGrid extends ApplicantsDataGrid
{
    public function __construct($siteID, $parameters, $misc)
    {
        /* Pager configuration. */
        $this->_tableWidth = 915;
        $this->_defaultAlphabeticalSortBy = 'lastName';
        $this->ajaxMode = false;
        $this->showExportCheckboxes = true; //BOXES WILL NOT APPEAR UNLESS SQL ROW exportID IS RETURNED!
        $this->showActionArea = true;
        $this->showChooseColumnsBox = true;
        $this->allowResizing = true;

	    $this->defaultSortBy = 'dateCreatedSort';
	    $this->defaultSortDirection = 'DESC';
	   $this->_defaultColumns = array(
           
            array('name' => 'Job', 'width' => 200),
            array('name' => 'First Name', 'width' => 75),
            array('name' => 'Last Name', 'width' => 85),
            array('name' => 'Company', 'width' => 75),
            array('name' => 'State', 'width' => 50),
            array('name' => 'E-Mail', 'width' => 115),
	    array('name' => 'SubmissionDate', 'width' => 115),

        );

         parent::__construct("Applicants:applicantsListByViewDataGrid",
                             $siteID, $parameters, $misc
                        );
    }


    /**
     * Adds more options to the action area on the pager.  Overloads
     * DataGrid Inner Action Area function.
     *
     * @return html innerActionArea commands.
     */
    public function getInnerActionArea()
    {
        //TODO: Add items:
        //  - Add to Pipeline
        //  - Mass set rank (depends on each candidate having their own personal rank - are we going to do this?)
        $html = '';

    																							
        //$html .= $this->getInnerActionAreaItem('Export', RESUFLOUtility::getIndexName().'?m=export&amp;a=exportByDataGrid');
		//$html .= $this->getInnerActionAreaItemPopup('Add To List', RESUFLOUtility::getIndexName().'?m=lists&amp;a=addToListFromDatagridModal&amp;dataItemType=100', 450, 350);
		$html .= $this->getInnerActionAreaItem('Delete', RESUFLOUtility::getIndexName().'?m=Applicants&amp;a=delete_Applicants');
        $html .= parent::getInnerActionArea();

        return $html;
    }
}

class applicantsSavedListByViewDataGrid extends ApplicantsDataGrid
{
    public function __construct($siteID, $parameters, $misc)
    {
        $this->_tableWidth = 915;
        $this->_defaultAlphabeticalSortBy = 'lastName';
        $this->ajaxMode = false;
        $this->showExportCheckboxes = true; //BOXES WILL NOT APPEAR UNLESS SQL ROW exportID IS RETURNED!
        $this->showActionArea = true;
        $this->showChooseColumnsBox = false;
        $this->allowResizing = true;
		
        $this->defaultSortBy = 'dateCreatedSort';
        $this->defaultSortDirection = 'DESC';

        $this->_defaultColumns = array(
            array('name' => 'Job', 'width' => 200),
            array('name' => 'First Name', 'width' => 75),
            array('name' => 'Last Name', 'width' => 85),
            array('name' => 'Company', 'width' => 75),
            array('name' => 'State', 'width' => 50),
            array('name' => 'E-Mail', 'width' => 115),
			array('name' => 'SubmissionDate', 'width' => 115),
        );

         parent::__construct("Applicants:applicantsSavedListByViewDataGrid",
                             $siteID, $parameters, $misc
                        );
    }

    /**
     * Adds more options to the action area on the pager.  Overloads
     * DataGrid Inner Action Area function.
     *
     * @return html innerActionArea commands.
     */
    public function getInnerActionArea()
    {
        //TODO: Add items:
        //  - Add to List
        //  - Add to Pipeline
        //  - Mass set rank (depends on each candidate having their own personal rank - are we going to do this?)
        $html = '';

        $html .= $this->getInnerActionAreaItem('Remove From This List', RESUFLOUtility::getIndexName().'?m=lists&amp;a=removeFromListDatagrid&amp;dataItemType='.DATA_ITEM_CANDIDATE.'&amp;savedListID='.$this->getMiscArgument(), false);
        $html .= $this->getInnerActionAreaItemPopup('Add To Pipeline', RESUFLOUtility::getIndexName().'?m=candidates&amp;a=considerForJobSearch', 750, 460);
        if(MAIL_MAILER != 0)
        {
            $html .= $this->getInnerActionAreaItem('Send E-Mail', RESUFLOUtility::getIndexName().'?m=candidates&amp;a=emailCandidates');
        }
        $html .= $this->getInnerActionAreaItem('Export', RESUFLOUtility::getIndexName().'?m=export&amp;a=exportByDataGrid');

        $html .= parent::getInnerActionArea();

        return $html;
    }
}


?>