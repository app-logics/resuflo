<?php
/*
 * RESUFLO
 * XML module
 *
 * Copyright (C) 2005 - 2007 Econn Technologies, Inc.
 *
 *
 * The contents of this file are subject to the RESUFLO Public License
 * Version 1.1a (the "License"); you may not use this file except in
 * compliance with the License. You may obtain a copy of the License at
 * http://www.econnoisseur.com/.
 *
 * Software distributed under the License is distributed on an "AS IS"
 * basis, WITHOUT WARRANTY OF ANY KIND, either express or implied. See the
 * License for the specific language governing rights and limitations
 * under the License.
 *
 * The Original Code is "RESUFLO Standard Edition".
 *
 * The Initial Developer of the Original Code is Econn Technologies, Inc.
 * Portions created by the Initial Developer are Copyright (C) 2005 - 2007
 * (or from the year in which this file was created to the year 2007) by
 * Econn Technologies, Inc. All Rights Reserved.
 *
 *
 * This module builds an XML file containing public job postings. The
 * exported XML data can be used to submit, en masse, all public job
 * postings to job bulletin sites such as Indeed.com.
 *
 *
 * $Id: XmlUI.php 3565 2007-11-12 09:09:22Z will $
 */
//include_once('./lib/dripmarketting.php');
include_once('./lib/ActivityEntries.php');
include_once('./lib/StringUtility.php');
include_once('./lib/DateUtility.php');
include_once('./lib/JobOrders.php');
include_once('./lib/Site.php');
include_once('./lib/XmlJobExport.php');
include_once('./lib/HttpLogger.php');
include_once('./lib/CareerPortal.php');
include_once('./lib/DataGrid.php');
include_once('./lib/Attachments.php');

class ApplicantsUI extends UserInterface
{
    public function __construct()
    {
        parent::__construct();
            
        $this->_authenticationRequired = true;
        $this->_moduleDirectory = 'Applicants';
        $this->_moduleName = 'Applicants';
        $this->_moduleTabText = 'Applicants';
    }
    function handleRequest()
    {
        $action = $this->getAction();
        $userid=$_SESSION['RESUFLO']->getUserID();
        switch ($action)
        {
            case 'show':
                $this->show();
                break;
            case 'delete_Applicants':
                $this->deleteApplicants();
                break;
            default:  
                $this->_template->assign('active', $this);
                $this->listByView();
                break;
        }
    }
    /*
     * Called by handleRequest() to process loading the list / main page.
     */
    private function listByView($errMessage = '')
    {
        $userId = $_SESSION['RESUFLO']->getUserID();
        $siteID = $_SESSION['RESUFLO']->getSiteID();
        $user = new Users($siteID);
        $currentUser = $user->get($userId);
        $hasJazzHRPermission = !empty($currentUser['JazzHRHiringLeadId']) && !empty($currentUser['JazzHRWorkflowId']) && !empty($currentUser['JazzHRJobPostLimit']);
        $this->_template->assign('hasJazzHRPermission', $hasJazzHRPermission);
        
        // Log message that shows up on the top of the list page
        $topLog = '';
        $dataGridProperties = DataGrid::getRecentParamaters("Applicants:applicantsListByViewDataGrid");
        //echo "<pre>"; print_r($dataGridProperties);
        /* If this is the first time we visited the datagrid this session, the recent paramaters will
         * be empty.  Fill in some default values. */
        if ($dataGridProperties == array())
        {
            $dataGridProperties = array('rangeStart'    => 0,
                                        'maxResults'    => 15,
                                        'filterVisible' => false);
        }
        //echo "<pre>";print_r($dataGridProperties);
        $dataGrid = DataGrid::get("Applicants:applicantsListByViewDataGrid", $dataGridProperties);
        $candidates = new Candidates($this->_siteID);
        $this->_template->assign('totalCandidates', $candidates->getCount());

        $this->_template->assign('active', $this);
        $this->_template->assign('dataGrid', $dataGrid);
        $this->_template->assign('userID', $_SESSION['RESUFLO']->getUserID());
        $this->_template->assign('errMessage', $errMessage);
        $this->_template->assign('topLog', $topLog);

        if (!eval(Hooks::get('CANDIDATE_LIST_BY_VIEW'))) return;
		
        $this->_template->display('./modules/Applicants/applicants.tpl');
    }
    private function show()
    {
        $dataGridProperties = DataGrid::getRecentParamaters("Applicants:applicantsListByViewDataGrid");
        //Used datagrid for next/previous buttons as they are not working on filters
        $dataGrid = DataGrid::get("Applicants:applicantsListByViewDataGrid", $dataGridProperties);
        
        $candidateID=$_REQUEST['candidateID'];
        $userid= $_SESSION['RESUFLO']->getUserID();		
        //code foer viewed candidates
        $view=$_REQUEST['view'];
        if($view == '1'){
                $sql = " update candidate set view_date =NOW() where candidate_id = '$candidateID'";
                $result = mysql_query($sql);
        }	
        if($userid == '1') {
                $viewed = "SELECT candidate_id FROM candidate WHERE view_date != '0000-00-00 00:00:00'  ORDER BY view_date desc limit 1,10  ";
        } 
        else {
                $viewed = "SELECT candidate_id FROM candidate where entered_by = '$userid' AND view_date != '0000-00-00 00:00:00' ORDER BY view_date desc limit 1,10  ";
        }
		
        $viewed_candidate = mysql_query($viewed);
        $candidate_id_view=array();				
        while($row = mysql_fetch_array($viewed_candidate)) {
                $candidate_id_view[]=$row['candidate_id'];
        }
				
        //code for prev and next button
        $dataGridProperties = DataGrid::getRecentParamaters("Applicants:applicantsListByViewDataGrid");
        $dataGridFilter = $dataGridProperties['filter'];
        $sortbycand= $dataGridProperties['sortBy'];
        $sortdirectioncand=$dataGridProperties['sortDirection'];
       
        if($sortbycand=="lastName") {
            $sortbycand='last_name';
        }
        if($sortbycand=="firstName") {
            $sortbycand='first_name';
        }
        if($sortbycand=="dateCreatedSort") {
            $sortbycand='date_created';
        }
        if($sortbycand=="dateModifiedSort") {
            $sortbycand='date_modified';
        }
        if($sortbycand=="ownerSort") {
            $sortbycand='owner';
        }
        if($sortbycand=="keySkills") {
            $sortbycand='key_skills';
        }
        if($sortbycand=="company") {
            $sortbycand='entered_by';
        } 
        else{
            $sortbycand = 'date_created';
        }
        

        $query="select candidate_id from candidate where entered_by=$userid ORDER BY $sortbycand $sortdirectioncand ";
        if($userid==1) {
                $query="select candidate_id from candidate  ORDER BY $sortbycand $sortdirectioncand ";
        } 
        
        $result = mysql_query($query);
        $dataarray =  $dataGrid->candidateList();
        $candid=$_REQUEST['candidateID'];
        $key= array_search($candid,$dataarray);
        $prev=$key-1;
        $next=$key+1;
		
        $prevcandid=$dataarray[$prev];
        $nextcandid=$dataarray[$next];
        //  echo "<pre>";print_r($dataarray);
		
        //code for prev and next button
        $_accessLevel = $this->_accessLevel;	
        $this->_template->assign('recipients', $_accessLevel);
        /* Is this a popup? */
        if (isset($_GET['display']) && $_GET['display'] == 'popup') {
            $isPopup = true;
        }
        else {
            $isPopup = false;
        }
        /* Bail out if we don't have a valid candidate ID. */
        if (!$this->isRequiredIDValid('candidateID', $_GET) && !isset($_GET['email'])) {
            CommonErrors::fatal(COMMONERROR_BADINDEX, $this, 'Invalid candidate ID.');
        }

        $candidates = new Candidates($this->_siteID);

        if (isset($_GET['candidateID'])) {
            $candidateID = $_GET['candidateID'];
        }
        else {
            $candidateID = $candidates->getIDByEmail($_GET['email']);
        }
        $data = $candidates->get($candidateID);

        /* Bail out if we got an empty result set. */
        if (empty($data))
        {
            CommonErrors::fatal(COMMONERROR_BADINDEX, $this, 'The specified candidate ID could not be found.');
            return;
        }

        if ($data['isAdminHidden'] == 1 && $this->_accessLevel < ACCESS_LEVEL_MULTI_SA)
        {
            $this->listByView('This candidate is hidden - only a RESUFLO Administrator can unlock the candidate.');
            return;
        }

        /* We want to handle formatting the city and state here instead
         * of in the template.
         */
        $data['cityAndState'] = StringUtility::makeCityStateString(
            $data['city'], $data['state']
        );

        /*
         * Replace newlines with <br />, fix HTML "special" characters, and
         * strip leading empty lines and spaces.
         */
        $data['notes'] = trim(
            nl2br(htmlspecialchars($data['notes'], ENT_QUOTES))
        );

        /* Format "can relocate" status. */
        if ($data['canRelocate'] == 1)
        {
            $data['canRelocate'] = 'Yes';
        }
        else
        {
            $data['canRelocate'] = 'No';
        }

        if ($data['isHot'] == 1)
        {
            $data['titleClass'] = 'jobTitleHot';
        }
        else
        {
            $data['titleClass'] = 'jobTitleCold';
        }
        $attachments = new Attachments($this->_siteID);
        $attachmentsRS = $attachments->getAll(
            DATA_ITEM_CANDIDATE, $candidateID
        );
        foreach ($attachmentsRS as $rowNumber => $attachmentsData)
        {
            /* If profile image is not local, force it to be local. */
            if ($attachmentsData['isProfileImage'] == 1)
            {
                $attachments->forceAttachmentLocal($attachmentsData['attachmentID']);
            }

            /* Show an attachment icon based on the document's file type. */
            $attachmentIcon = strtolower(
                FileUtility::getAttachmentIcon(
                    $attachmentsRS[$rowNumber]['originalFilename']
                )
            );

            $attachmentsRS[$rowNumber]['attachmentIcon'] = $attachmentIcon;

            /* If the text field has any text, show a preview icon. */
            if ($attachmentsRS[$rowNumber]['hasText'])
            {
                $attachmentsRS[$rowNumber]['previewLink'] = sprintf(
                    '<a href="#" onclick="window.open(\'%s?m=candidates&amp;a=viewResume&amp;attachmentID=%s\', \'viewResume\', \'scrollbars=1,width=800,height=760\')"><img width="15" height="15" style="border: none;" src="images/search.gif" alt="(Preview)" /></a>',
                    RESUFLOUtility::getIndexName(),
                    $attachmentsRS[$rowNumber]['attachmentID']
                );
            }
            else
            {
                $attachmentsRS[$rowNumber]['previewLink'] = '&nbsp;';
            }
        }
        $pipelines = new Pipelines($this->_siteID);
        $pipelinesRS = $pipelines->getCandidatePipeline($candidateID);
        $sessionCookie = $_SESSION['RESUFLO']->getCookie();

        /* Format pipeline data. */

        //die('<pre>'.print_r($pipelinesRS,true));
        //jobLinkCold in CandidatesUI::show()

        foreach ($pipelinesRS as $rowIndex => $row)
        {
			$pipelinesRS[$rowIndex]['status'] = $pipelines->getJobOrderStatusName($row['jobOrderID'],$row['status']);
            /* Hot jobs [can] have different title styles than normal
             * jobs.
             */
            if ($row['isHot'] == 1)
            {
                $pipelinesRS[$rowIndex]['linkClass'] = 'jobLinkHot';
            }
            else
            {
                $pipelinesRS[$rowIndex]['linkClass'] = 'jobLinkCold';
            }

            $pipelinesRS[$rowIndex]['ownerAbbrName'] = StringUtility::makeInitialName(
                $pipelinesRS[$rowIndex]['ownerFirstName'],
                $pipelinesRS[$rowIndex]['ownerLastName'],
                false,
                LAST_NAME_MAXLEN
            );

            $pipelinesRS[$rowIndex]['addedByAbbrName'] = StringUtility::makeInitialName(
                $pipelinesRS[$rowIndex]['addedByFirstName'],
                $pipelinesRS[$rowIndex]['addedByLastName'],
                false,
                LAST_NAME_MAXLEN
            );

            $pipelinesRS[$rowIndex]['ratingLine'] = TemplateUtility::getRatingObject(
                $pipelinesRS[$rowIndex]['ratingValue'],
                $pipelinesRS[$rowIndex]['candidateJobOrderID'],
                $sessionCookie
            );
        }

        $activityEntries = new ActivityEntries($this->_siteID);
        $activityRS = $activityEntries->getAllByDataItem($candidateID, DATA_ITEM_CANDIDATE, $userid);
        if (!empty($activityRS))
        {
            foreach ($activityRS as $rowIndex => $row)
            {
                if (empty($activityRS[$rowIndex]['notes']))
                {
                    $activityRS[$rowIndex]['notes'] = '(No Notes)';
                }

                if (empty($activityRS[$rowIndex]['jobOrderID']) ||
                    empty($activityRS[$rowIndex]['regarding']))
                {
                    $activityRS[$rowIndex]['regarding'] = 'General';
                }

                $activityRS[$rowIndex]['enteredByAbbrName'] = StringUtility::makeInitialName(
                    $activityRS[$rowIndex]['enteredByFirstName'],
                    $activityRS[$rowIndex]['enteredByLastName'],
                    false,
                    LAST_NAME_MAXLEN
                );
            }
        }

        /* Get upcoming calendar entries. */
        $calendarRS = $candidates->getUpcomingEvents($candidateID);
        /* Get reminder, thanks and phone reminder Events*/
        $calendarReminderThanksPhoneRS = $candidates->getReminderThanksPhoneEvents($candidateID);
        if (!empty($calendarRS))
        {
            foreach ($calendarRS as $rowIndex => $row)
            {
                $calendarRS[$rowIndex]['enteredByAbbrName'] = StringUtility::makeInitialName(
                    $calendarRS[$rowIndex]['enteredByFirstName'],
                    $calendarRS[$rowIndex]['enteredByLastName'],
                    false,
                    LAST_NAME_MAXLEN
                );
            }
        }

        /* Get extra fields. */
        $extraFieldRS = $candidates->extraFields->getValuesForShow($candidateID);
		//echo "<pre>";
		//print_r($extraFieldRS);
        /* Add an MRU entry. */
        $_SESSION['RESUFLO']->getMRU()->addEntry(
            DATA_ITEM_CANDIDATE, $candidateID, $data['firstName'] . ' ' . $data['lastName']
        );
        /* Is the user an admin - can user see history? */
        if ($this->_accessLevel < ACCESS_LEVEL_DEMO)
        {
            $privledgedUser = false;
        }
        else
        {
            $privledgedUser = true;
        }

        $EEOSettings = new EEOSettings($this->_siteID);
        $EEOSettingsRS = $EEOSettings->getAll();
        $EEOValues = array();

        /* Make a list of all EEO related values so they can be positioned by index
         * rather than static positioning (like extra fields). */
        if ($EEOSettingsRS['enabled'] == 1)
        {
            if ($EEOSettingsRS['genderTracking'] == 1)
            {
                $EEOValues[] = array('fieldName' => 'Gender', 'fieldValue' => $data['eeoGenderText']);
            }
            if ($EEOSettingsRS['ethnicTracking'] == 1)
            {
                $EEOValues[] = array('fieldName' => 'Ethnicity', 'fieldValue' => $data['eeoEthnicType']);
            }
            if ($EEOSettingsRS['veteranTracking'] == 1)
            {
                $EEOValues[] = array('fieldName' => 'Veteran Status', 'fieldValue' => $data['eeoVeteranType']);
            }
            if ($EEOSettingsRS['disabilityTracking'] == 1)
            {
                $EEOValues[] = array('fieldName' => 'Disability Status', 'fieldValue' => $data['eeoDisabilityStatus']);
            }
        }
        $users = new Users($this->_siteID);
        $userSearch = $users->get($this->_userID);

        $this->_template->assign('active', $this);
        $this->_template->assign('data', $data);
        $this->_template->assign('isShortNotes', $isShortNotes);
        $this->_template->assign('attachmentsRS', $attachmentsRS);
        $this->_template->assign('pipelinesRS', $pipelinesRS);
        $this->_template->assign('activityRS', $activityRS);
        $this->_template->assign('calendarRS', $calendarRS);
        $this->_template->assign('calendarReminderThanksPhoneRS', $calendarReminderThanksPhoneRS);
        $this->_template->assign('extraFieldRS', $extraFieldRS);
        $this->_template->assign('candidateID', $candidateID);
        $this->_template->assign('isPopup', $isPopup);
        $this->_template->assign('EEOSettingsRS', $EEOSettingsRS);
        $this->_template->assign('EEOValues', $EEOValues);
        $this->_template->assign('privledgedUser', $privledgedUser);
        $this->_template->assign('pipelineEntriesPerPage', $pipelineEntriesPerPage);
        $this->_template->assign('sessionCookie', $_SESSION['RESUFLO']->getCookie());
        $this->_template->assign('list', $_REQUEST["list"]);
        $this->_template->assign('prevcandid', $prevcandid);
        $this->_template->assign('nextcandid', $nextcandid);
        $this->_template->assign('candidate_id_view', $candidate_id_view);
        $this->_template->assign('dataGridFilter', $dataGridFilter);
        $this->_template->assign('searchCriteria', $userSearch["searchCriteria"]);
        $this->_template->assign('isTextMessageEnable', $userSearch["isTextMessageEnable"]);
        //var_dump($_SESSION['RESUFLO']);
        $session = (array)$_SESSION['RESUFLO'];
        //var_dump($session);
        //print_r((array)$_SESSION['RESUFLO']);
        $listId = $_GET["list"];
        if(!empty($listId)){
            $dataGridSavedListProperties = DataGrid::getRecentParamaters("Applicants:applicantsSavedListByViewDataGrid", $listId);
            //var_dump($dataGridSavedListProperties['filter']);
            $this->_template->assign('listId', $listId);
            $this->_template->assign('dataGridSavedListFilter', $dataGridSavedListProperties['filter']);
        }
        
        if (!eval(Hooks::get('CANDIDATE_SHOW'))) return;

        $this->_template->display('./modules/Applicants/Show.tpl');
    }
    //HOTLEADS DELETE
    public function deleteApplicants()
    {
        if ($this->_accessLevel < ACCESS_LEVEL_DELETE)
        {
            CommonErrors::fatal(COMMONERROR_PERMISSION, $this, 'Invalid user level for action.');
        }
        /* Bail out if we don't have a valid candidate ID. */
        $dataGrid = DataGrid::getFromRequest();
        $candidateIDs = $dataGrid->getExportIDs();
        $del=array();
        foreach($candidateIDs as $val =>$key)
        {
            $del['m'] = 'applicants';
            $del['a'] = 'delete_Applicants';
            $del['candidateID'] = $key;
            if (!$this->isRequiredIDValid('candidateID', $del))
            {
                CommonErrors::fatal(COMMONERROR_BADINDEX, $this, 'Invalid candidate ID.');
            }
            $candidateID = $del['candidateID'];
            if (!eval(Hooks::get('CANDIDATE_DELETE'))) return;
            
            $applicant = new Applicants($this->_siteID);
            $applicant->delete($candidateID);
            $attachments = new Attachments($this->_siteID);
            $attachments->deleteAll(DATA_ITEM_CANDIDATE, $candidateID);
            /* Delete the MRU entry if present. */
            $_SESSION['RESUFLO']->getMRU()->removeEntry(
                    DATA_ITEM_CANDIDATE, $candidateID
            );
        }
        RESUFLOUtility::transferRelativeURI('m=Applicants');
    }
    //HOTLEADS DELETE
}
?>
