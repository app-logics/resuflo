<?php /* $Id: Show.tpl 3814 2007-12-06 17:54:28Z brian $ */ ?>
<link rel="stylesheet" href="css/jquery.ui.all.css">
<script src="js/jquery-1.5.1.js"></script>
<script src="js/ui/jquery.ui.core.js"></script>
<script src="js/ui/jquery.ui.widget.js"></script>
<script src="js/ui/jquery.ui.accordion.js"></script>
<link rel="stylesheet" href="css/demos.css">
<script>
    $(function() {
        $( "#accordion" ).accordion({ 
            autoHeight: false,
            navigation: true,
            collapsible: true
        });
    });
</script>

<?php if ($this->isPopup): ?>
<?php TemplateUtility::printHeader('Applicant - '.$this->data['firstName'].' '.$this->data['lastName'], array( 'js/activity.js', 'js/sorttable.js', 'js/match.js', 'js/lib.js', 'js/pipeline.js', 'js/attachmentss.js')); ?>
<?php else: ?>
<?php TemplateUtility::printHeader('Applicant - '.$this->data['firstName'].' '.$this->data['lastName'], array( 'js/activity.js', 'js/sorttable.js', 'js/match.js', 'js/lib.js', 'js/pipeline.js', 'js/attachment.js')); ?>
<?php TemplateUtility::printHeaderBlock(); ?>
<?php TemplateUtility::printTabs($this->active); ?>
<div id="main">
    <?php endif; ?>
    <style type="text/css">
        body {
            margin-left: 0px;
            margin-top: 0px;
            margin-right: 0px;
            margin-bottom: 0px;
        }
        .top_heading{ background:#00568b; font-family:Arial, Helvetica, sans-serif; font-size:12px; color:#fff; padding:5px; font-weight:bold;}
        .detail_heading{ background:#f0f0f0; font-family:Arial, Helvetica, sans-serif; font-size:12px; color:#282626; padding:10px; font-weight:bold; border-bottom:1px solid #ccc; vertical-align:top;}
        .deatils{  font-family:Arial, Helvetica, sans-serif; font-size:12px; color:#282626; padding:10px; border-bottom:1px solid #ccc; vertical-align:top;}
        .top_heading a{ background:#00568b; font-family:Arial, Helvetica, sans-serif; font-size:12px; color:#fff; padding:5px; font-weight:bold;}
    </style>
    <script type="text/javascript">
                <!--
        function printContent(id){
            str=document.getElementById(id).innerHTML;

            newwin=window.open('','','location=no,titlebar=no,status=no,fullscreen=no');
            newwin.document.write('<HTML>\n<HEAD>\n')
            newwin.document.write('<title></title><script>\n')
            newwin.document.write('function chkstate(){\n')
            newwin.document.write('if(document.readyState=="complete"){\n')
            newwin.document.write('window.close()\n')
            newwin.document.write('}\n')
            newwin.document.write('else{\n')
            newwin.document.write('setTimeout("chkstate()",2000)\n')
            newwin.document.write('}\n')
            newwin.document.write('}\n')
                    newwin.document.write('function print_win(){\n')
            newwin.document.write('window.print();\n')
            newwin.document.write('chkstate();\n')
            newwin.document.write('}\n')
                    newwin.document.write('<\/script>\n')
                    newwin.document.write('</HEAD>\n')
                    newwin.document.write('<BODY onload="print_win()">\n')
                    newwin.document.write(str)
                    newwin.document.write('</BODY>\n')
                    newwin.document.write('</HTML>\n')
                    newwin.document.close()
                }
                //-->
    </script>
    <script type="text/javascript">
                <!--
        function viewpdf(id){
            data=document.getElementById(id).innerHTML;

            document.write(data);
        }

        //-->
    </script>
    <script>
        $(function() {
            $( "#accordion" ).accordion();
        });
    </script>
    <form id="form1" name="form1" method="post" action="">
        <div id="contents">
            <table>
                <tr>
                    <td width="3%">
                        <img src="images/candidate.gif" width="24" height="24" border="0" alt="Candidates" style="margin-top: 3px;" />&nbsp;
                    </td>
                    <td><h2>Candidates: Candidate Details</h2></td>
                </tr>
            </table>
            <div>
                <div class="demo11" >
                    <div id="accordion11">
                        <table width="925" border="0"  cellspacing="0" cellpadding="0" style="border:1px solid #cccccc;">
                            <tr>
                                <td colspan="4" class="top_heading"><!--<a href="#Candidate Contact Details" >-->Candidate Contact Details </td>
                            </tr>
                        </table>
                        <div>
                            <table width="925" border="0" cellspacing="0" cellpadding="0" style="border:1px solid #cccccc;">
                                <tr>
                                    <td width="195" class="detail_heading">Name:</td>
                                    <td width="344" class="deatils">
                                        <span style="font-weight: bold;" class="<?php echo($this->data['titleClass']); ?>">
                                            &nbsp;<?php $this->_($this->data['firstName']); ?>
                                            <?php $this->_($this->data['middleName']); ?>
                                            <?php $this->_($this->data['lastName']); ?>
                                        </span>			
                                    </td>
                                    <td width="173" class="detail_heading">Cell Phone :</td>
                                    <td width="218" class="deatils"> &nbsp;<?php $this->_($this->data['phoneCell']); ?></td>
                                </tr>
                                <tr>
                                    <td width="190" class="detail_heading">E-Mail :</td>
                                    <td width="344" class="deatils">
                                        <a href="<?php echo(RESUFLOUtility::getIndexName()); ?>?m=Applicants&amp;a=emailcandidate&amp;<?php echo $this->data['email1']; ?> " >
                                            &nbsp; <?php $this->_(str_replace(",",",\n",$this->data['email1'])); ?></a>
                                    </td>
                                    <td class="detail_heading">Home Phone :</td>
                                    <td class="deatils"> &nbsp;<?php $this->_($this->data['phoneHome']); ?></td>
                                </tr>
                                <tr>
                                    <td class="detail_heading">2nd E-Mail :</td>
                                    <td class="deatils">
                                        <a href="<?php echo(RESUFLOUtility::getIndexName()); ?>?m=Applicants&amp;a=emailcandidate&amp;<?php echo $this->data['email2']; ?> ">
                                            &nbsp; <?php $this->_($this->data['email2']); ?></a>
                                    </td>
                                    <td class="detail_heading">Work Phone :</td>
                                    <td class="deatils"> &nbsp;<?php $this->_($this->data['phoneWork']); ?></td>
                                </tr>
                                <tr>
                                    <td class="detail_heading">Address :</td>
                                    <td class="deatils"> 
                                        &nbsp;<?php echo(nl2br(htmlspecialchars($this->data['address']))); ?>  &nbsp;&nbsp;<?php $this->_($this->data['cityAndState']); ?> 
                                        <?php $this->_($this->data['zip']); ?>
                                    </td>
                                    <td class="detail_heading">Best Time To Call :</td>
                                    <td class="deatils">&nbsp; <?php $this->_($this->data['bestTimeToCall']); ?></td>
                                </tr>
                            </table>
                        </div>
                        <table width="925" border="0" cellspacing="0" cellpadding="0" style="margin-top:1px;">
                            <tr>
                                <td colspan="4" class="top_heading"><!--<a href="#section7" >-->Detail Resume  </td>
                            </tr>
                        </table>
                        <div>
                            <table width="925" border="0" cellspacing="0" cellpadding="0" style="border:1px solid #cccccc;">
                                
                                <tr>
                                    <td width="180" class="detail_heading">Detail Resume :</td>
                                    <td colspan="3" class="deatils"> &nbsp;<?php echo html_entity_decode($this->extraFieldRS[20]['display']); ?></td>
                                </tr>
                            </table>
                        </div>
                        <table width="925" border="0" cellspacing="0" cellpadding="0" style="margin-top:1px;" >
                            <tr>
                                <td colspan="4" class="top_heading"><!--<a href="#section8" >-->Others <!--</a>--></td>
                            </tr>
                        </table>
                        <div>
                            <table width="925" border="0" cellspacing="0" cellpadding="0" style="border:1px solid #cccccc;">
                                
                                <tr>
                                    <td width="198" class="detail_heading">Gap Period :</td>
                                    <td width="342" class="deatils"> &nbsp;<?php echo($this->extraFieldRS[9]['display']); ?></td>
                                    <td width="170" class="detail_heading">Worked Period :</td>
                                    <td width="215" class="deatils"> &nbsp;<?php echo($this->extraFieldRS[8]['display']); ?> </td>
                                    
                                </tr>
                                <tr>
                                    <td class="detail_heading">Date Available :</td>
                                    <td class="deatils">&nbsp;<?php $this->_($this->data['dateAvailable']); ?></td>
                                    <td class="detail_heading">Current Employer :</td>
                                    <td class="deatils">&nbsp;<?php $this->_($this->data['currentEmployer']); ?></td>
                                </tr>
                                <tr>
                                    <td class="detail_heading">Can Relocate :</td>
                                    <td class="deatils"> &nbsp;<?php $this->_($this->data['canRelocate']); ?></td>
                                    <td class="detail_heading">Current Pay :</td>
                                    <td class="deatils"> &nbsp;<?php $this->_($this->data['currentPay']); ?></td>
                                </tr>
                                <tr>
                                    <td class="detail_heading">Desired Pay:</td>
                                    <td class="deatils"> &nbsp;<?php $this->_($this->data['desiredPay']); ?></td>
                                    <td class="detail_heading">Pipeline:</td>
                                    <td class="deatils"> &nbsp;<?php $this->_($this->data['pipeline']); ?></td>
                                </tr>
                                <tr>
                                    <td class="detail_heading">Created:</td>
                                    <td class="deatils"> &nbsp;<?php $this->_($this->data['dateCreated']); ?> (<?php $this->_($this->data['enteredByFullName']); ?>)</td>
                                    <td class="detail_heading">Submitted:</td>
                                    <td class="deatils"> &nbsp;<?php $this->_($this->data['submitted']); ?></td>
                                </tr>
                                <tr>
                                    <td class="detail_heading">Owner:</td>
                                    <td class="deatils"> &nbsp;<?php $this->_($this->data['ownerFullName']); ?></td>
                                    <td class="detail_heading">Fax No :</td>
                                    <td class="deatils">&nbsp;<?php echo($this->extraFieldRS[10]['display']); ?></td>
                                </tr>
                                <tr>
                                    <td class="detail_heading">Language Known:</td>
                                    <td class="deatils">&nbsp;<?php echo($this->extraFieldRS[12]['display']); ?></td>
                                    <td class="detail_heading">License No:</td>
                                    <td class="deatils">&nbsp;<?php echo($this->extraFieldRS[15]['display']); ?></td>
                                </tr>
                                <tr>
                                    <td class="detail_heading">References :</td>
                                    <td class="deatils">&nbsp;<?php echo($this->extraFieldRS[18]['display']); ?></td>
                                    <td class="detail_heading">Hobbies:</td>
                                    <td class="deatils">&nbsp;<?php echo($this->extraFieldRS[13]['display']); ?></td>
                                </tr>
                                
                                
                            </table>
                        </div>
                        
                    </div>
                </div>
                
            </DIV>
            
            <div id="print_div1" style="display:none;">
                
                <table width="1000" border="0" align="center" cellpadding="0" cellspacing="0" style="font-family:Arial, Helvetica, sans-serif; font-size:14px; color:#000000;">
                    <?php for ($i = ((intval(count($this->extraFieldRS)))-1); $i < (count($this->extraFieldRS)); $i++): ?>
                    <tr>
                        <td class="vertical">
                            <?php echo($this->extraFieldRS[$i]['display']); ?></td>
                    </tr>
                    <?php endfor; ?>
                </table>
            </div><br/>
            <?php if($this->EEOSettingsRS['enabled'] == 1): ?>
            <table class="detailsOutside" width="925">
                <tr>
                    <td>
                        <table class="detailsInside">
                            <?php for ($i = 0; $i < intval(count($this->EEOValues)/2); $i++): ?>
                            <tr>
                                <td class="vertical"><?php $this->_($this->EEOValues[$i]['fieldName']); ?>:</td>
                                <?php if($this->EEOSettingsRS['canSeeEEOInfo']): ?>
                                <td class="data"><?php $this->_($this->EEOValues[$i]['fieldValue']); ?></td>
                                <?php else: ?>
                                <td class="data"><i><a href="javascript:void(0);" title="Ask an administrator to see the EEO info, or have permission granted to see it.">(Hidden)</a></i></td>
                                <?php endif; ?>
                            </tr>
                            <?php endfor; ?>
                        </table>
                    </td>
                    <?php if ($profileImage): ?>
                    <td width="390" height="100%" valign="top">
                        <?php else: ?>
                    </td><td width="50%" height="100%" valign="top">
                        <?php endif; ?>
                        <table class="detailsInside">
                            <?php for ($i = (intval(count($this->EEOValues))/2); $i < intval(count($this->EEOValues)); $i++): ?>
                            <tr>
                                <td class="vertical"><?php $this->_($this->EEOValues[$i]['fieldName']); ?>:</td>
                                <?php if($this->EEOSettingsRS['canSeeEEOInfo']): ?>
                                <td class="data"><?php $this->_($this->EEOValues[$i]['fieldValue']); ?></td>
                                <?php else: ?>
                                <td class="data"><i><a href="javascript:void(0);" title="Ask an administrator to see the EEO info, or have permission  granted to see it.">(Hidden)</a></i></td>
                                <?php endif; ?>
                            </tr>
                            <?php endfor; ?>
                        </table>
                    </td>
                </tr>
            </table>
            <?php endif; ?>
            
            <table class="detailsOutside" width="925">
                <tr>
                    <td>
                        <table class="detailsInside">
                            <tr>
                                <td width="192" valign="top" class="vertical">Misc. Notes:</td>
                                <?php if ($this->isShortNotes): ?>
                                <td width="510" class="data" id="shortNotes" style="display:block;">
                                    <?php echo($this->data['shortNotes']); ?><span class="moreText">...</span>&nbsp;
                                    <p><a href="#" class="moreText" onclick="toggleNotes(); return false;">[More]</a></p>
                                </td>
                                <td width="38" class="data" id="fullNotes" style="display:none;">
                                    <?php echo($this->data['notes']); ?>&nbsp;
                                    <p><a href="#" class="moreText" onclick="toggleNotes(); return false;">[Less]</a></p>
                                </td>
                                <?php else: ?>
                                <td width="21" class="data" id="shortNotes" style="display:block;">
                                    <?php echo($this->data['notes']); ?>                                    </td>
                                <?php endif; ?>
                            </tr>
                            
                            <tr>
                                <td valign="top" class="vertical">Upcoming Events:</td>
                                <td id="shortNotes" style="display:block;" class="data">
                                    <?php foreach ($this->calendarRS as $rowNumber => $calendarData): ?>
                                    <div>
                                        <a href="<?php echo(RESUFLOUtility::getIndexName()); ?>?m=calendar&amp;view=DAYVIEW&amp;month=<?php echo($calendarData['month']); ?>&amp;year=20<?php echo($calendarData['year']); ?>&amp;day=<?php echo($calendarData['day']); ?>&amp;showEvent=<?php echo($calendarData['eventID']); ?>">
                                            <img src="<?php $this->_($calendarData['typeImage']) ?>" alt="" border="0" />
                                            <?php $this->_($calendarData['dateShow']) ?>:
                                            <?php $this->_($calendarData['title']); ?>
                                        </a>
                                    </div>
                                    <?php endforeach; ?>
                                    <?php if ($this->accessLevel >= ACCESS_LEVEL_EDIT): ?>
                                    <?php endif; ?>
                                    
                                    
                                    
                                </td>
                            </tr>
                            
                            <?php if (isset($this->questionnaires) && !empty($this->questionnaires)): ?>
                            <tr>
                                <td valign="top" class="vertical" align="left">Questionnaires:</td>
                                <td class="data" valign="top" align="left">
                                    <table cellpadding="0" cellspacing="0" border="0">
                                        <tr>
                                            <td style="border-bottom: 1px solid #c0c0c0; font-weight: bold; padding-right: 10px;">Title (Internal)</td>
                                            <td style="border-bottom: 1px solid #c0c0c0; font-weight: bold; padding-right: 10px;">Completed</td>
                                            <td style="border-bottom: 1px solid #c0c0c0; font-weight: bold; padding-right: 10px;">Description (Public)</td>
                                        </tr>
                                        <?php foreach ($this->questionnaires as $questionnaire): ?>
                                        <tr>
                                            <td style="padding-right: 10px;" nowrap="nowrap"><a href="<?php echo(RESUFLOUtility::getIndexName()); ?>?m=candidates&amp;a=show_questionnaire&amp;candidateID=<?php echo($this->candidateID); ?>&amp;questionnaireTitle=<?php echo urlencode($questionnaire['questionnaireTitle']); ?>&print=no"><?php echo $questionnaire['questionnaireTitle']; ?></a></td>
                                            <td style="padding-right: 10px;" nowrap="nowrap"><?php echo date('F j. Y', strtotime($questionnaire['questionnaireDate'])); ?></td>
                                            <td style="padding-right: 10px;" nowrap="nowrap"><?php echo $questionnaire['questionnaireDescription']; ?></td>
                                            <td style="padding-right: 10px;" nowrap="nowrap">
                                                <a id="edit_link" href="<?php echo(RESUFLOUtility::getIndexName()); ?>?m=candidates&amp;a=show_questionnaire&amp;candidateID=<?php echo($this->candidateID); ?>&amp;questionnaireTitle=<?php echo urlencode($questionnaire['questionnaireTitle']); ?>&print=no">
                                                    <img src="images/actions/view.gif" width="16" height="16" class="absmiddle" alt="view" border="0" />&nbsp;View
                                                </a>
                                                &nbsp;
                                                <a id="edit_link" href="<?php echo(RESUFLOUtility::getIndexName()); ?>?m=candidates&amp;a=show_questionnaire&amp;candidateID=<?php echo($this->candidateID); ?>&amp;questionnaireTitle=<?php echo urlencode($questionnaire['questionnaireTitle']); ?>&print=yes">
                                                    <img src="images/actions/print.gif" width="16" height="16" class="absmiddle" alt="print" border="0" />&nbsp;Print
                                                </a>
                                            </td>
                                        </tr>
                                        <?php endforeach; ?>
                                    </table>
                                </td>
                            </tr>
                            <?php endif; ?>
                            
                            <tr>
                                <td valign="top" class="vertical">Attachments:</td>
                                <td valign="top" class="data">
                                    <table class="attachmentsTable">
                                        
                                        <?php foreach ($this->attachmentsRS as $rowNumber => $attachmentsData): ?>
                                        <?php if ($attachmentsData['isProfileImage'] != '1'): ?>
                                        <tr>
                                            <td>
                                                <?php echo $attachmentsData['retrievalLink']; ?>
                                                <img src="<?php $this->_($attachmentsData['attachmentIcon']) ?>" alt="" width="16" height="16" border="0" />
                                                &nbsp;
                                                <?php $this->_($attachmentsData['originalFilename']) ?>
                                                </a>
                                            </td>
                                            <td><?php echo($attachmentsData['previewLink']); ?></td>
                                            <td><?php $this->_($attachmentsData['dateCreated']) ?></td>
                                            <td>
                                                <?php if (!$this->isPopup): ?>
                                                <?php if ($this->accessLevel >= ACCESS_LEVEL_DELETE): ?>
                                                
                                                <?php endif; ?>
                                                <?php endif; ?>
                                            </td>
                                        </tr>
                                        <?php endif; ?>
                                        <?php endforeach; ?>
                                    </table>
                                    <?php if (!$this->isPopup): ?>
                                    <?php if ($this->accessLevel >= ACCESS_LEVEL_EDIT): ?>
                                    <?php if (isset($this->attachmentLinkHTML)): ?>
                                    <?php echo($this->attachmentLinkHTML); ?>
                                    <?php else: ?>
                                    <?php endif; ?>
                                    <?php endif; ?>
                                    <?php endif; ?>
                                </td>
                            </tr>
                            
                        </table>
                    </td>
                </tr>
            </table>
            <br />
        </div>
    </form>
</div>
<div id="bottomShadow"></div>
<?php TemplateUtility::printFooter(); ?>
