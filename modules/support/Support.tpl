<?php /* $Id: Search.tpl 1948 2007-02-23 09:49:27Z will $ */ ?>
<?php TemplateUtility::printHeader('Activities', array('js/highlightrows.js', 'modules/activity/validator.js', 'js/sweetTitles.js')); ?>
<?php TemplateUtility::printHeaderBlock(); ?>
<?php TemplateUtility::printTabs($this->active); ?>
    <style>
        .demoThumbnail
        {
            font-size: 25px;
            width: 40%;
        }
        .demoThumbnail img
        {
            width: 200px;
            text-align: center;
        }
        .demoThumbnail h2
        {
            padding: 10px 0px 23px 0px;
        }
        a:hover, a:hover h2{
            text-decoration: none;
            color: blue;
        }
        #header ul#primary
        {
            bottom: 0px;
        }
        .ticket{
            padding: 1px;
            border-collapse: collapse;
        }
        .ticket tr td
        {
            border: 1px solid lightgray;
        }
        .ticket tr:first-child td
        {
            border: none;
        }
    </style>
    <div id="main">
        <?php TemplateUtility::printQuickSearch(); ?>
        <div id="contents">
            <table width="100%">
                <tr>
                    <td width="3%">
                        <img src="images/activities.gif" width="24" height="24" alt="Activities" style="border: none; margin-top: 3px;" />&nbsp;
                    </td>
                    <td><h2>Tickets</h2></td>
                </tr>
            </table>
            <div>
                <a href="index.php?m=support&a=addTicket" class="btn blue xx-small">Add Support Ticket</a>
            </div>
            <div style="width: 100%;">
                <?php
                foreach ($this->tickets as $ticket => $row){
                    //date_default_timezone_set('Asia/Karachi');
                    $dbDate = $row["date_created"];
                    $ymd = DateTime::createFromFormat('Y-m-d H:i:s', $dbDate)->format('Y-m-d H:i:s');
                    $duration = round(abs(strtotime(date("Y-m-d H:i:s")) - strtotime($ymd)) / 60);
                    //echo $duration;
                    if($duration < 60){
                        $duration = $duration." minute(s) ago";
                    }
                    else if($duration < 60*24){
                        $duration = round($duration/60)." hour(s) ago";
                    }
                    else{
                        $duration = round(($duration/60)/24)." day(s) ago";
                    }
                    //echo date("Y-m-d H:i:s");
                ?>
                    <div style='background-color: <?php if($row["type"] == "general"){echo "#f5f4f4;";}else{echo "#f7dbdb;";} ?> padding: 5px;'>
                        <div style="color: black;" >
                            <?php if($this->userID == 1){?>
                                <span class="black" style="margin-right: 10px;padding: 0px 5px"><?php echo $row["user_name"];?></span> 
                            <?php }?>
                            <?php echo $duration; ?>
                            <a href="index.php?m=support&a=markResolved&ticketId=<?php echo $row['ticketId']; ?>" class="btn green xx-small" style="float: right;">Mark Resolved</a>
                            <?php if($this->userID != $row['lastUpdatedBy']){ ?>
                                <a href="index.php?m=support&a=addTicketResponse&ticketId=<?php echo $row['ticketId']; ?>" class="btn red xx-small" style="float: right;">Awaiting Response</a>
                            <?php } ?>
                        </div>
                        <div>
                            <a href="index.php?m=support&a=addTicketResponse&ticketId=<?php echo $row['ticketId']; ?>"><h3><?php echo $row["title"]; ?></h3></a>
                        </div>
                        <hr>
                    </div>
                <?php }?>
            </div>
            <br>
            <br>
            <br>
            <table width="100%">
                <tr>
                    <td width="3%">
                        <img src="images/activities.gif" width="24" height="24" alt="Activities" style="border: none; margin-top: 3px;" />&nbsp;
                    </td>
                    <td><h2>Demo</h2></td>
                </tr>
            </table>
            <table width="100%" style="padding: 0px 115px;">
                <!--<tr>
                    <td colspan="4"><iframe style="width: 100%; height: 700px;" frameborder='0' scrolling='no' src='//screencast-o-matic.com/embed?sc=cDVuoLh7T0&w=1280&v=4'></iframe></td>
                </tr>-->
                <tr style="text-align: center;">
                    <td>
                        <a class="demoThumbnail" href="#" onclick="showPopWin('<?php echo $this->Demo[1]; ?>', 1282, 758, null); return false;">
                            <img src="images/firstVideoThumb.JPG" alt=""/>
                            <h2><?php echo $this->Demo[0]; ?></h2>
                        </a>
                    </td>
                    <td>
                        <a class="demoThumbnail" href="#" onclick="showPopWin('<?php echo $this->Demo[3]; ?>', 1282, 758, null); return false;">
                            <img src="images/secondVideoThumb.JPG" alt=""/>
                            <h2><?php echo $this->Demo[2]; ?></h2>
                        </a>
                    </td>
                    <td>
                        <a class="demoThumbnail" href="#" onclick="showPopWin('<?php echo $this->Demo[5]; ?>', 1282, 758, null); return false;">
                            <img src="images/thirdVideoThumb.JPG" alt=""/>
                            <h2><?php echo $this->Demo[4]; ?></h2>
                        </a>
                    </td>
                </tr>
                <tr style="text-align: center;">
                    <td>
                        <a class="demoThumbnail" href="#" onclick="showPopWin('<?php echo $this->Demo[7]; ?>', 1282, 758, null); return false;">
                            <img src="images/fourthVideoThumb.JPG" alt=""/>
                            <h2><?php echo $this->Demo[6]; ?></h2>
                        </a>
                    </td>
                    <td>
                        <a class="demoThumbnail" href="#" onclick="showPopWin('<?php echo $this->Demo[9]; ?>', 1282, 758, null); return false;">
                            <img src="images/fifthVideoThumb.JPG" alt=""/>
                            <h2><?php echo $this->Demo[8]; ?></h2>
                        </a>
                    </td>
                </tr>
            </table>
        </div>
    </div>
    <div id="bottomShadow"></div>
<?php TemplateUtility::printFooter(); ?>
