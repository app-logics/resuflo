<?php /* $Id: Search.tpl 1948 2007-02-23 09:49:27Z will $ */ ?>
<?php TemplateUtility::printHeader('Activities', array('js/highlightrows.js', 'modules/activity/validator.js', 'js/sweetTitles.js')); ?>
<?php TemplateUtility::printHeaderBlock(); ?>
<?php TemplateUtility::printTabs($this->active); ?>
    <style>
        #header ul#primary
        {
            bottom: 0px;
        }
    </style>
    <link href="./google calender/main.css" rel="stylesheet" type="text/css" />
    <div id="main">
        <?php TemplateUtility::printQuickSearch(); ?>
        <div id="contents">
            <form method="post" action="index.php?m=support&a=addTicket" enctype="multipart/form-data" style="padding: 20px;">
                <input type="hidden" name="postback" id="postback" value="postback" />
                Title:
                <input type="text" id="title" name="title" />
                <br>
                <br>
                Query for:
                <select id="type" name="type">
                    <option value="general">General Query</option>
                    <option value="technical">Technical Query</option>
                </select>
                <br>
                <br>
                <textarea name="response" id="response" class="txtbox" style="min-height:300px;width:100%"></textarea>
                <br>
                <input type="file" id="image" name="image" />
                <br>
                <br>
                <button class="btn blue x-small">Submit Ticket</button>
            </form>
        </div>
    </div>
    <div id="bottomShadow"></div>
<script src="jscripts/ckeditor/ckeditor.js" type="text/javascript"></script> 
<script type="text/javascript">
    CKEDITOR.replace('response');
</script>
<?php TemplateUtility::printFooter(); ?>
