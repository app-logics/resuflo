<?php /* $Id: Search.tpl 1948 2007-02-23 09:49:27Z will $ */ ?>
<?php TemplateUtility::printHeader('Activities', array('js/highlightrows.js', 'modules/activity/validator.js', 'js/sweetTitles.js')); ?>
<?php TemplateUtility::printHeaderBlock(); ?>
<?php TemplateUtility::printTabs($this->active); ?>

</script>
    <style>
        .demoThumbnail
        {
            font-size: 25px;
            width: 40%;
        }
        .demoThumbnail img
        {
            width: 200px;
            text-align: center;
        }
        .demoThumbnail h2
        {
            padding: 10px 0px 23px 0px;
        }
        a:hover, a:hover h2{
            text-decoration: none;
            color: blue;
        }
        #header ul#primary
        {
            bottom: 0px;
        }
    </style>
    <div id="main">
        <?php TemplateUtility::printQuickSearch(); ?>
        <div id="contents">
            <table width="100%">
                <tr>
                    <td width="3%">
                        <img src="images/activities.gif" width="24" height="24" alt="Activities" style="border: none; margin-top: 3px;" />&nbsp;
                    </td>
                    <td><h2>Tickets</h2></td>
                </tr>
            </table>
            <div style="text-align: right;">
                <a href="index.php?m=support&a=markResolved&ticketId=<?php echo $this->ticketId; ?>" class="btn green xx-small">Mark Resolved</a>
            </div>
            <div style="width: 100%;">
                <?php
                foreach ($this->responses as $response => $row){
                    //date_default_timezone_set('Asia/Karachi');
                    $dbDate = $row["date_created"];
                    $ymd = DateTime::createFromFormat('Y-m-d H:i:s', $dbDate)->format('Y-m-d H:i:s');
                    $duration = round(abs(strtotime(date("Y-m-d H:i:s")) - strtotime($ymd)) / 60);
                    //echo $duration;
                    if($duration < 60){
                        $duration = $duration." minute(s) ago";
                    }
                    else if($duration < 60*24){
                        $duration = round($duration/60)." hour(s) ago";
                    }
                    else{
                        $duration = round(($duration/60)/24)." day(s) ago";
                    }
                    //echo date("Y-m-d H:i:s");
                ?>
                    <div style='background-color: <?php if($row["userId"] > 1){echo "#f5f4f4;";}else{echo "#dbeff7;";} ?> padding: 5px;'>
                        <div style="color: blue;" ><span class="black" style="margin-right: 10px;padding: 0px 5px"><?php echo $row["user_name"]; ?></span> <?php echo $duration; ?></div>
                        <br>
                        <div>
                            <?php echo $row["response"]; ?>
                        </div>
                        <?php if(!empty($row["image"])){ ?>
                            <a target="_blank" href="./images/ticket/<?php echo $row['image']; ?>">View Image</a>
                        <?php }?>
                        <hr>
                    </div>
                <?php }?>
            </div>
            <br>
            <form method="post" action="index.php?m=support&a=addTicketResponse" enctype="multipart/form-data">
                <input type="hidden" name="postback" id="postback" value="postback" />
                <input type="hidden" name="ticketId" id="ticketId" value="<?php echo $this->ticketId; ?>" />
                <textarea name="response" id="response" class="txtbox" style="min-height:300px;width:100%"></textarea>
                <br>
                <input type="file" id="image" name="image" />
                <br>
                <br>
                <button class="btn blue x-small">Submit Response</button>
            </form>  
            <br>
        </div>
    </div>
    <div id="bottomShadow"></div>
<script src="jscripts/ckeditor/ckeditor.js" type="text/javascript"></script> 
<script type="text/javascript">
    CKEDITOR.replace('response');
</script>
<?php TemplateUtility::printFooter(); ?>
