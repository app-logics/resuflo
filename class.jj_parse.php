<?php

require('lib/Candidates.php');

class jj_parse {

    var $flds = 'Resume Date-Time,First Name,Last Name,Email Address,Phone,City,State,Zipcode,Export Date-Time,Resume Source';

    var $flds1 = 'last name,first name,middle name,phone home,phone cell,phone work,address,city,state,zip,keyskills,email1,eamil2,category,Date Of Birth,Gender,Father Name,Mother Name,Marital Status,Passport No,Nationality,Worked Period,Gap Period,Fax No,License No,Hobbies,Qualification,Achievements,Objectives,Experience,References,Job Profile,desired pay,current pay,best time to call,current employer,Language Known,Detail Resume';

    var $flds2 = 'LastName,FirstName,Middlename,Phone,Mobile,Phonework,Address,City,State,ZipCode,Skills,Email,Email2,Category,DateOfBirth,Gender,MotherName,FatherName,MaritalStatus,PassportNo,Nationality,WorkedPeriod,GapPeriod,FaxNo,LicenseNo,Hobbies,Qualification,Achievments,Objectives,Experience,References,JobProfile,ExpectedSalary,CurrentSalary,besttimetocall,CurrentEmployer,languageknown,DetailResume';

    function __construct() {
        $this->db = DatabaseConnection::getInstance();
        $this->fldsa = explode(',',$this->flds);
    }

    // returns and object that can then be passed to a database save routine
    function ProcessZip($filename,$key,$userid,$db,$emailalert,$camp_id='') {

        // the return value will be a multidimensional array with the resumes to be inserted to the db
        $retval = array();
        $debug = true;

        if ($debug) echo "ProcessZip called on $filename for $userid\n\n";

        //code to get username
        $sql="SELECT user_id, user_name FROM user WHERE user_id=$userid";
        $resourcedetail = $this->db->query($sql);
        $resultarray=mysql_fetch_assoc($resourcedetail);
        $this->_user=$resultarray;
        $this->_username=$resultarray['user_name'];
        $username_unzip=$resultarray['user_name'];

        //code to get username
        //die("filename: $filename");
        if ($filename !="") {

            $archive = new PclZip($filename);

            //code to move file to another folder

            $explodemainfilename = explode(".",$filename);
            $countmainexplode = count($explodemainfilename);
            $filterfilename = $explodemainfilename[$countmainexplode-2] ;

            $filterfilename=$filterfilename.time().".".$explodemainfilename[$countmainexplode-1] ;

            $newfile ="backup/".$filterfilename;

            if (!copy($filename, $newfile)) {
                echo __METHOD__." failed to copy $filename to $newfile...\n";
            }

            $dir_usernamezip = "upload/resume/".$username_unzip;
            @shell_exec("rm -Rf $dir_usernamezip");
            mkdir($dir_usernamezip,0770,true);

            if (($v_result_list = $archive->extract(PCLZIP_OPT_PATH, $dir_usernamezip)) == 0) {
                $msg_ext = 'Zip Cound not extract.<br>Error : '.$archive->errorInfo(true);
                echo $msg_ext."\n";
                $this->_uploaderror .= $msg_ext;
            }

            //start
            //rename all file and folder and set space as underscore upto 3 directory levels
            //echo 'renaming all file and folders replacing spaces with underscores'."\n";

            $handle = opendir($dir_usernamezip);
            if (!$handle) die('the folder '.$dir_usernamezip.' does not exist');
            while ($file = readdir($handle)) {
                if ($file!="." && $file!="..") {
                    //echo $file;
                    $newname1 = str_replace(" ","_",$file);
                    if ($handle2 = @opendir($dir."/".$file)) {
                        while ($file2 = readdir($handle2)) {
                            if ($file2!="." && $file2!="..") {
                                //echo $file2;
                                $newname2 = str_replace(" ","_",$file2);
                                if ($handle3 = @opendir($dir."/".$file."/".$file2)){
                                    while ($file3 = readdir($handle3)) {
                                        if ($file3!="." && $file3!=".."){
                                            //echo $file3;
                                            $newname3 = str_replace(" ","_",$file3);
                                            @rename ($dir."/".$file."/".$file2."/".$file3, $dir."/".$file."/".$file2."/".$newname3);
                                        }
                                    }
                                }
                                @rename ($dir."/".$file."/".$file2,$dir."/".$file."/".$newname2);
                            }
                        }
                    }
                    @rename ($dir."/".$file, $dir."/".$newname1);
                }
            }
            closedir($handle);

            $count=0;
            //print_r($v_result_list);

            if ($debug) echo "\n\nv_result_list:\n\n".print_r($v_result_list,true)."\n\n";

            if (count($v_result_list)){
                foreach ($v_result_list as $k=>&$v) {
                    $filen=explode('.',$v['filename']);

                    if ($debug) echo 'processing '.$v['filename']."...\n";

                    $this->_totalnooffiles++;
                    //if ($debug) echo 'totalnooffiles '.$this->_totalnooffiles;

                    $unzipfilename = escapeshellarg($v['filename']);

                    // i wonder if there is a switch for escapeshellarg that stops it from putting in the single quotes
                    $unzipfilename = substr($unzipfilename, 1, strlen($unzipfilename)-2); // remove single quotes quick

                    //if the file is a TXT then process it further
                    if (substr($v['filename'], -3)!=='TXT') {
                        if (substr($v['filename'])=='csv') {
                            die('TXT2014jj0628: please migrate csv processing module');
                        }
                        die('TXT2014jj0628: please update processing module for '.substr($v['filename'], -3));
                    }

                    $fileurl = str_replace(basename(__FILE__), '', __FILE__).$unzipfilename;
                    if (!file_exists($fileurl)) {
                        echo "FILE $filename could not be found\n";
                        continue;
                    }
                    $contents = file_get_contents($fileurl);

                    // first grab a big glob of what we need
                    $pattern = '/Resume Date-Time.*Resume Source/s'; 
                    preg_match($pattern, $contents, $matches);

                    if (empty($matches)) {
                        echo "error finding Resume Date-Time in $fileurl\n";
                        continue;
                    }

                    // well most of what we need...try to get a resume source
                    $pattern = '/Resume Source(.*)/';
                    preg_match($pattern, $contents, $res_src_mat);

                    // glue the resume source back onto the data blog and declare it
                    $data_glob = $matches[0].substr($res_src_mat[0], strlen('Resume Source'));

                    // loop over the fields in the data glob and get the values of each one out
                    $r = array();
                    foreach ($this->fldsa as &$fld) {
                        $pattern = '/'.$fld.'[:]?\s?(.*)/';
                        preg_match($pattern, $contents, $m);
                        // zip file name: [basename($filename)];
                        $r[$fld] = $m[1];
                    }

                    // die('<pre>'.print_r($r,true));
            /*[Resume Date-Time] => 06/26/2014 00:00:01
              [First Name] => Job
              [Last Name] => Seeker
              [Email Address] => dt0567@gmail.com
              [Phone] => 972-638-7699
              [City] => Flower Mound
              [State] => TX
              [Zipcode] => 75028
              [Export Date-Time] => 06/26/2014 22:37:02
              [Resume Source] => CareerBuilder - Resumes (WSI recent)*/

                    // insert into the database
                    //$resultprocesszip=$this->InsertIntoDBexcelData($fileurl,$userid,$db,$v['filename'],$emailalert,$fileextension,$camp_id);
                    // this->save_into_db($r);
                    if ($debug) echo "\n\n--------------------------------\n<pre>".print_r($r,true)."</pre>\n";
                    $GLOBALS['user_id'] = $this->_user['user_id'];
                    $site_id = $this->_user['site_id'];
                    $candidate = new Candidates($site_id);
                    // candidate add calls other files that need a the userid in the session...so i'm sending them the GLOBAL version
                    $GLOBALS['user_id'] = $this->_user['user_id'];

                    $candidateId = $candidate->add($r['First Name'], @$middle, $r['Last Name'], $r['Email Address'], @$email2, $r['Phone'], $r['Phone'], @$phoneWork, @$address, $r['City'], $r['State'], $r['Zipcode'], $r['Resume Source'], @$keySkills, @$dateAvailable, @$currentEmployer, @$canRelocate, @$currentPay, @$desiredPay, '', @$webSite, @$bestTimeToCall, $this->_user['user_id'], @$owner, @$gender, @$race, @$veteran, @$disability, @$lifeLic, @$propLic, @$ser6, @$ser63, @$ser7, $camp_id, /*$skipHistory = */false);

                    if ($debug) echo "new candidate id? ($candidateId)\n";

                    if (!empty($candidateId)) {
                        //$candidate->InsertExtraFields($xmlArr,$candidateId);
                    }
 
                }
            }

            //echo "removing $dir_usernamezip...\n";
            shell_exec("rm -Rf $dir_usernamezip");
        }

        return $message;
    }


    function UnZip() {

        $archive = new PclZip($_FILES["resumecontent"]["tmp_name"]);
        $temp_array = explode(".",$_FILES["resumecontent"]["name"]);
        $username_unzip = $temp_array(0);

        if (($v_result_list = $archive->extract(PCLZIP_OPT_PATH, 'upload/resume/'.$username_unzip)) == 0) {
            die("Error : ".$archive->errorInfo(true));
        }
        /*echo "<pre>";
        var_dump($v_result_list);
        echo "</pre>";*/
    }


}
