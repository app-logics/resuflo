	function checkQuantity(s)
	{
		// Returns 0 if illegal, 1 if legal but zero, 2 if legal and non-zero
		var result = 1;
		legalChars = ' 0123456789\t\r\n';
		nonZeroChars = '123456789';
		for (var i = 0; i < s.length; i++)
		{
			var c = s.charAt(i);
			if (legalChars.indexOf(c) == -1) return 0;
			if (nonZeroChars.indexOf(c) > -1) result = 2;
		}
		return result;
	}
	function checkAmount(s)
	{
		// Returns 0 if illegal, 1 if legal but zero, 2 if legal and non-zero
		var result = 1;
		legalChars = ' 0123456789\.\t\r\n';
		nonZeroChars = '123456789';
		for (var i = 0; i < s.length; i++)
		{
			var c = s.charAt(i);
			if (legalChars.indexOf(c) == -1) return 0;
			if (nonZeroChars.indexOf(c) > -1) result = 2;
		}
		return result;
	}
	
	function validateform()
	{
		// Returns true if this form is valid
		var foundErrors = false;
		var foundQuant = false;
		var isValid = false;
		for (var i = 0; i < document.mgform.length; i++)
		{
			var e = document.mgform.elements[i];
			if (e.name.substring(0, 6) == 'quant_')
			{
				quantStatus = checkQuantity(e.value)
				if (quantStatus == 2) foundQuant = true;
				if (quantStatus == 0) foundErrors = true;
			}
			if (e.name.substring(0, 4) == 'amt_')
			{
				quantStatus = checkAmount(e.value)
				if (quantStatus == 2) foundQuant = true;
				if (quantStatus == 0) foundErrors = true;
			}
		}
		isValid = ((foundQuant == true) && (foundErrors == false))
		if (isValid == false) {
			alert('Invalid quantity.  Please enter a quantity of 1 or more.');
			return isValid;
		}
		if (document.mgform.selecteddate && document.mgform.selecteddate.selectedIndex < 1) {
			alert('Please select the date you would like to attend.');
			return false;
		}
		return true;
	}
	
	function Show(div)
	{
		var theDiv = document.getElementById(div);
		if (theDiv.style.display == 'none') {
			theDiv.style.display = 'block';
		} else {
			theDiv.style.display = 'none';
		}
	}
	
	function checkContactForm()
	{
		if (!document.contactForm.from_name.value) {
			alert('Please enter your name.');
			return false;
		}
		if (!document.contactForm.from_email.value) {
			alert('Please enter an email address.');
			return false;
		}
		if (!document.contactForm.note.value) {
			alert('Please enter your message.');
			return false;
		}
		if (!document.contactForm.captcha.value) {
			alert('Please enter the code as shown on the image.');
			return false;
		}
		if (!checkEmail(document.contactForm.email.value)){
			alert('Please enter a valid email address.');
			return false;
		}
		return true;
	}
	
	function checkInvitationForm()
	{
		if (!document.invitationForm.from_name.value) {
			alert('Please enter your name.');
			return false;
		}
		if (!document.invitationForm.from_email.value) {
			alert('Please enter your email address.');
			return false;
		}
		if (!document.invitationForm.to_email.value) {
			alert('Please enter a valid email address in the To: field.');
			return false;
		}
		if (!document.invitationForm.subject.value) {
			alert('Please enter a subject for your message.');
			return false;
		}
		if (!document.invitationForm.note.value) {
			alert('Please enter your note.');
			return false;
		}
		if (!document.invitationForm.captcha.value) {
			alert('Please enter the code as shown on the image.');
			return false;
		}
		var emailStr = document.invitationForm.to_email.value;
		emailStr = emailStr.replace(/\s/g, ',');
		emailStr = emailStr.replace(/(,+)/g, ',');
		document.invitationForm.to_email.value = emailStr;
		emails = emailStr.split(',');
		for (var n = 0; n < emails.length; n++) {
			if (emails[n] && !checkEmail(emails[n])){
				alert(emails[n] + ' is not a valid email address.');
				return false;
			}
		}
		if (emails.length > 10) {
			alert('You can only send this invitations to 10 email addresses at a time.');
			return false;
		}
		return true;
	}

	function setColor(val) {
		var radioSelect = document.customizeForm.radioSelect;
		for (var i = 0; i < radioSelect.length; i++)
		{
			if (radioSelect[i].checked) {
				if (radioSelect[i].value == 'backgroundcolor') {
					document.getElementById('backgroundcolor').style.background = val;
					document.getElementById('regpage').style.background = val;
					document.customizeForm.background_color.value = val;
				}
				if (radioSelect[i].value == 'boxbackgroundcolor') {
					changeRoundedBackground('order', 'bottom', val);
					changeRoundedBackground('details', 'top', val);
					changeRoundedBackground('details', 'bottom', val);
					changeRoundedBackground('attendees', 'top', val);
					changeRoundedBackground('attendees', 'bottom', val);
					changeRoundedBackground('date', 'bottom', val);
					changeRoundedBackground('host', 'bottom', val);
					changeBackground("roundcontent", val);
					document.getElementById('boxbackgroundcolor').style.background = val;
					document.customizeForm.box_background_color.value = val;
				}
				if (radioSelect[i].value == 'boxheaderbackgroundcolor') {
					changeRoundedBackground('order', 'top', val);
					changeRoundedBackground('date', 'top', val);
					changeRoundedBackground('host', 'top', val);
					changeBackground("roundheader", val);
					changeBackground("roundcontent2", val);
					document.getElementById('boxheaderbackgroundcolor').style.background = val;
					document.customizeForm.box_header_background_color.value = val;
				}
				if (radioSelect[i].value == 'boxbordercolor') {
					changeRoundedBorder('order', val);
					changeRoundedBorder('details', val);
					changeRoundedBorder('attendees', val);
					changeRoundedBorder('date', val);
					changeRoundedBorder('host', val);
					changeBorder("roundheader", val);
					changeBorder("roundcontent", val);
					changeBorder("roundcontent2", val);
					document.getElementById('regpage').style.borderColor = val;
					document.getElementById('boxbordercolor').style.background = val;
					document.customizeForm.box_border_color.value = val;
				}
				if (radioSelect[i].value == 'textcolor') {
					document.getElementById('regpage').style.color = val;
					document.getElementById('titleDiv').style.color = val;
					document.getElementById('textcolor').style.background = val;
					document.customizeForm.text_color.value = val;
				}
				if (radioSelect[i].value == 'linkcolor') {
					var nodes = document.getElementById('regpage').getElementsByTagName('a');
					for (var j = 0; j < nodes.length; j++) {
						nodes[j].style.color = val;
					}
					document.getElementById('linkcolor').style.background = val;
					document.customizeForm.link_color.value = val;
				}
				if (radioSelect[i].value == 'titletextcolor') {
					changeClassColor('title', 'div', val);
					document.getElementById('titletextcolor').style.background = val;
					document.customizeForm.title_text_color.value = val;
				}
				if (radioSelect[i].value == 'boxtextcolor') {
					changeColor('roundcontent', val);
					changeClassColor('tickets', 'td', val);
					document.getElementById('boxtextcolor').style.background = val;
					document.customizeForm.box_text_color.value = val;
				}
				if (radioSelect[i].value == 'boxheadertextcolor') {
					changeColor('roundheader', val);
					changeClassColor('tickets2', 'td', val);
					document.getElementById('boxheadertextcolor').style.background = val;
					document.customizeForm.box_header_text_color.value = val;
				}
			}
		}
	}
	
	function setColorFromText(input, val) {
		var regex = /^#[a-fA-F0-9]{6}$/i;
		if (!regex.test( val )) {
			alert("\"" + val + "\" is not a properly formatted color.  Please use the format \"#RRGGBB\" for all colors.");
			input.value = '';
			input.focus();
			return;
		}
		setColor(val);
	}
	
	function setRadio(name) {
		name = name.replace("_value", "");
		var radioSelect = document.customizeForm.radioSelect;
		for (var i = 0; i < radioSelect.length; i++)
		{
			if (radioSelect[i].value == name) {
				radioSelect[i].checked = true;
				break;
			}
		}
	}

	function changeBackground( name, color) {
		var nodes = document.getElementsByName(name);
		for (var j = 0; j < nodes.length; j++) {
			nodes[j].style.background = color;
		}
	}

	function changeRoundedBackground( divName, pos, color) {
		if (document.getElementById(divName)) {
			var nodes = document.getElementById(divName).getElementsByTagName('B');
			for (var j = 0; j < nodes.length; j++) {
				if (nodes[j].parentNode.className.indexOf( pos ) > 0) {
					nodes[j].style.background = color;
				}
			}
		}
	}

	function changeBorder( name, color) {
		var nodes = document.getElementsByName(name);
		for (var j = 0; j < nodes.length; j++) {
			nodes[j].style.borderColor = color;
		}
	}
	
	function changeRoundedBorder( divName, color) {
		if (document.getElementById(divName)) {
			var nodes = document.getElementById(divName).getElementsByTagName('B');
			for (var j = 0; j < nodes.length; j++) {
				nodes[j].style.borderColor = color;
			}
		}
	}
	
	function changeColor( name, color) {
		var nodes = document.getElementsByName(name);
		for (var j = 0; j < nodes.length; j++) {
			nodes[j].style.color = color;
		}
	}
	
	function changeClassColor(className, element, color) {
		var elements = document.getElementsByTagName(element);
		for (var i=0; i < elements.length; i++) {
			if (elements[i].className == className || elements[i].id == className ) {
				elements[i].style.color = color;
			}
		}
	}
	
	function AddToCalendar(calendar, server, eid, date) {
		var url = server + '/calendar.ics?eid=' + eid + '&calendar=' + calendar;
		if (date) {
			url += '&date=' + date;
		} else {
			if (document.mgform && document.mgform.selecteddate) {
				if (document.mgform.selecteddate.selectedIndex < 1) {
					alert('Please select the date you would like to attend.');
					return;
				} else {
					url += '&date=' + document.mgform.selecteddate[document.mgform.selecteddate.selectedIndex].value;
				}
			}
		}
		if (calendar == 'outlook' || calendar == 'ical')
			document.location.href = url;
		else
			window.open(url, 'calendar', 'toolbar=yes, menubar=yes, location=yes, status=yes, scrollbars=yes,resizable=yes, width=800, height=600, left=0, top=0');
	}
	
	function Redeem() {
		if (!document.discountForm.discount.value) {
			alert('Please enter a discount code.');
			document.discountForm.discount.focus();
			return false;
		}
		var sUrl = document.location.href.split('?')[0];
		sUrl += '?discount=' + document.discountForm.discount.value;
		document.location.href = sUrl;
		return true;
	}
	
	function ShowRSVP(radio) {
		Hide('RSVPYes');
		Hide('RSVPNo');
		Hide('RSVPUndecided');
		Hide('RSVPInfo');
		Show('RSVP' + radio.id);
		if ((radio.id == 'No' || radio.id == 'Undecided') && document.mgform.invite.value == "")
			Show('RSVPInfo');
	}
	
	function ProcessRSVP(answer, inviteKey) {
		var url = baseUrl + '/inviteaction';
		var pars = '?eid=' + eid + '&r=' + Math.random() + '&i=' + inviteKey;
		pars += '&rsvp=' + answer;
		if ($('full_name'))
			pars += '&name=' + escape($('full_name').value);
		if ($('email'))
			pars += '&email=' + escape($('email').value);
		pars += '&message=' + escape($('rsvp_message_' + answer.toLowerCase()).value);
		window.frames['RSVPFrame'].location.href = url + pars;
		ShowRSVPConfirmation();
		//var myAjax = new Ajax.Request(url, {method: 'get', parameters: pars, onComplete: ShowRSVPConfirmation });
	}
	
	function ShowRSVPConfirmation() {
		$('RSVPDiv').innerHTML = 'Thank you for your reply.';
	}

	function updateCheckout() {
		hasQuantity = 0;
		isFree = 0;
		isPaid = 0;
		hasPaid = 0;
		for (var i = 0; i < document.mgform.length; i++)
		{
			var e = document.mgform.elements[i];
			if (e.name.substring(0, 6) == 'quant_')
			{
				if (e[e.selectedIndex].value > 0) {
					hasQuantity = 1;
					var f = document.mgform.elements[i-1];
					if (f.value > "0.00") {
						isFree = 0;
						isPaid = 1;
						hasPaid = 1;
					} else {
						if (!isPaid)
							isFree = 1;
					}
				}
			}
			if (e.name.substring(0, 4) == 'amt_')
			{
				if (e.value > "0.00") {
					hasQuantity = 1;
					isFree = 0;
					isPaid = 1;
					hasPaid = 1;
				}
			}
			if (e.name.substring(0, 5) == 'cost_')
			{
				if (e.value > "0.00") {
					hasPaid = 1;
				}
			}
		}
		if ((!hasQuantity || isPaid) && !isFree && hasPaid) {
			$('freeButton').style.display = 'none';
			$('paidButton').style.display = 'block';
		} else {
			$('freeButton').style.display = 'block';
			$('paidButton').style.display = 'none';
		}
		return true;
	}
	
	function paypalCheckout() {
		document.mgform.pp_payment_status.value = 'paypal';
		if (validateform())
			document.mgform.submit();
	}
	
	function googleCheckout() {
		document.mgform.pp_payment_status.value = 'google';
		if (validateform())
			document.mgform.submit();
	}
	
	function offlineCheckout() {
		document.mgform.pp_payment_status.value = 'offline';
		if (validateform())
			document.mgform.submit();
	}

	function applyDiscount() {
		document.mgform.action = '/event/' + document.mgform.bigeid.value;
		document.mgform.submit();
	}
