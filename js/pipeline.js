/*
 * RESUFLO
 * Pipeline JavaScript Library
 *
 * Copyright (C) 2005 - 2007 Econn Technologies, Inc.
 *
 *
 * The contents of this file are subject to the RESUFLO Public License
 * Version 1.1a (the "License"); you may not use this file except in
 * compliance with the License. You may obtain a copy of the License at
 * http://www.econnoisseur.com/.
 *
 * Software distributed under the License is distributed on an "AS IS"
 * basis, WITHOUT WARRANTY OF ANY KIND, either express or implied. See the
 * License for the specific language governing rights and limitations
 * under the License.
 *
 * The Original Code is "RESUFLO Standard Edition".
 *
 * The Initial Developer of the Original Code is Econn Technologies, Inc.
 * Portions created by the Initial Developer are Copyright (C) 2005 - 2007
 * (or from the year in which this file was created to the year 2007) by
 * Econn Technologies, Inc. All Rights Reserved.
 *
 *
 * $Id: pipeline.js 1936 2007-02-22 23:35:46Z will $
 */

var _sortBy;
var _sortDirection;

function PipelineDetails_populate(candidateJobOrderID, htmlObjectID, sessionCookie)
{
    var http = AJAX_getXMLHttpObject();

    /* Build HTTP POST data. */
    var POSTData = '&candidateJobOrderID=' + urlEncode(candidateJobOrderID);

    /* Anonymous callback function triggered when HTTP response is received. */
    var callBack = function ()
    {
        if (http.readyState != 4)
        {
            return;
        }

        document.getElementById(htmlObjectID).innerHTML = http.responseText;
    }

    AJAX_callRESUFLOFunction(
        http,
        'getPipelineDetails',
        POSTData,
        callBack,
        0,
        sessionCookie,
        false,
        false
    );
}

function PipelineJobOrder_setLimitDefaultVars(sortBy, sortDirection)
{
    _sortBy = sortBy;
    _sortDirection = sortDirection;
}

function PipelineJobOrder_changeLimit(joborderID, entriesPerPage, isPopup, htmlObjectID, sessionCookie, indicatorID, indexFile, dead, future)
{
    PipelineJobOrder_populate(joborderID, 0, entriesPerPage, _sortBy, _sortDirection, isPopup, htmlObjectID, sessionCookie, indicatorID, indexFile, dead, future)
}

function PipelineJobOrder_populate(joborderID, page, entriesPerPage, sortBy,
    sortDirection, isPopup, htmlObjectID, sessionCookie, indicatorID, indexFile, dead, future)
{
    var http = AJAX_getXMLHttpObject();
	//alert(http);
    /* Build HTTP POST data. */
    var POSTData = '&joborderID=' + joborderID;
    POSTData += '&page=' + page;
    POSTData += '&entriesPerPage=' + entriesPerPage;
    POSTData += '&sortBy=' + urlEncode(sortBy);
    POSTData += '&sortDirection=' + urlEncode(sortDirection);
    POSTData += '&indexFile=' + urlEncode(indexFile);
    POSTData += '&isPopup=' + urlEncode(isPopup);
    POSTData += '&dead=' + dead;
    POSTData += '&future=' + future;
	//alert(POSTData);
    document.getElementById(indicatorID).style.display = '';

    /* Anonymous callback function triggered when HTTP response is received. */
    var callBack = function ()
    {
        if (http.readyState != 4)
        {
            return;
        }

        document.getElementById(indicatorID).style.display = 'none';

        document.getElementById(htmlObjectID).innerHTML = http.responseText;

        execJS(http.responseText);
    }

    AJAX_callRESUFLOFunction(
        http,
        'getPipelineJobOrder',
        POSTData,
        callBack,
        55000,
        sessionCookie,
        false,
        false
    );
}

/* Adds or removes a value from an array depending on if the value is already in the array. */
function addRemoveFromExportArray(arrayObject, theValue)
{
    var inArray = false;
    for (var i = 0; i < arrayObject.length; i++)
    {
        if (arrayObject[i] == theValue && !inArray)
        {
            arrayObject.splice(i, 1);
            inArray = true;
        }
    }

    if (!inArray)
    {
        arrayObject.push(theValue);
    }
    //console.log(arrayObject);
}

function toggleChecksAllDataGrid(arrayObject, isChecked){
    if(isChecked == true){
        $(".cid").prop("checked", true);
        $(".cid").each(function(){
            var cid = parseInt($(this).attr("id").split("_")[1]);
            addRemoveFromExportArray(arrayObject, cid);
        });
    }
    else{
        $(".cid").prop("checked", false);
        $(".cid").each(function(){
            var cid = parseInt($(this).attr("id").split("_")[1]);
            addRemoveFromExportArray(arrayObject, cid);
        });
    }
}

function dataGridNoSelected(){
    alert("Nothing selected");
}
/* Helper for add/remove column window. */
function toggleHideShowAction(md5InstanceName) 
{
    var ActionArea = document.getElementById('ActionArea' + md5InstanceName);
    
    if (ActionArea.style.display=='block') 
    {
        ActionArea.style.display = 'none';
    } 
    else 
    {
        ActionArea.style.display = 'block';
    }
}
