// JavaScript Document
/** * Communication layer between Plaxo AB Chooser popup and parent window. * * Due to the overly restrictive same-origin policy, the popup (which is on a different domain) * can't directly touch its parent, i.e. it can't set the value of the parent's recipient text box. * So instead the parent injects a callback iframe in the parent's domain, which then loads this * script and gets it data over the query string. The iframe can talk to the parent and thus it * can update the text box. Yeesh! * * Contact: Joseph Smarr (joseph@plaxo.com) * Copyright (c) 2005 Plaxo, Inc. All rights reserved. */ 
var data = null; 
var donePage = ''; 
function Plaxo_abc_comm_doOnLoad()
{
if (addRecipientText()) 
{ 
if (donePage && donePage.length > 0) 
top.location.href = donePage; 
else if (top.opener) 
top.close(); 
} 
/*else 
{ 
alert('Unable to add recipients. Please close the address book window and try again.');
} */
} 
function formatRecipient(name, email) 
{ 
return '"' + name + '" <' + email + '>';
}
function formatRecipientList(data) 
{ 
var recips = [];
for (var i = 0;
i < data.length; i++) 
{ 
recips.push(formatRecipient(data[i][0], data[i][1])); 
} 
return recips.join(', '); 
} 
function addRecipientText()
{
if (!data || data.length == 0) 
{ 
return false; 
} 
var p = top.opener ? top.opener : top; 
if (p && p.Plaxo && p.Plaxo.abl && p.Plaxo.abl.addCheckedRecipients) 
{ 
var recipText = formatRecipientList(data); 
p.Plaxo.abl.addCheckedRecipients(recipText); 
if (p.onABCommComplete) p.onABCommComplete(data); 
return true; } return false; 
} 
window.onload = Plaxo_abc_comm_doOnLoad;