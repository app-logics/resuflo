<?php
/*
 * RESUFLO
 * AJAX Activity Entry Deletion Interface
 *
 * Copyright (C) 2005 - 2007 Econn Technologies, Inc.
 *
 *
 * The contents of this file are subject to the RESUFLO Public License
 * Version 1.1a (the "License"); you may not use this file except in
 * compliance with the License. You may obtain a copy of the License at
 * http://www.econnoisseur.com/.
 *
 * Software distributed under the License is distributed on an "AS IS"
 * basis, WITHOUT WARRANTY OF ANY KIND, either express or implied. See the
 * License for the specific language governing rights and limitations
 * under the License.
 *
 * The Original Code is "RESUFLO Standard Edition".
 *
 * The Initial Developer of the Original Code is Econn Technologies, Inc.
 * Portions created by the Initial Developer are Copyright (C) 2005 - 2007
 * (or from the year in which this file was created to the year 2007) by
 * Econn Technologies, Inc. All Rights Reserved.
 *
 *
 * $Id: deleteActivity.php 1479 2007-01-17 00:22:21Z will $
 */

include_once('./lib/UserMailer.php');

$interface = new SecureAJAXInterface();

$siteID = $interface->getSiteID();

$sendgrid = $_REQUEST['sendGrid'];
$smtpHost = $_REQUEST['smtpHost'];
$smtpUser = $_REQUEST['smtpUser'];
$smtpPass = $_REQUEST['smtpPass'];
$smtpPort = $_REQUEST['smtpPort'];
$fromName = $_REQUEST['fromName'];
$fromEmail = $_REQUEST['fromEmail'];
$bccEmail = $_REQUEST['bccEmail'];
$emailType = $_REQUEST['emailType'];
$firstTestEmail = $_REQUEST['firstTestEmail'];

/* Delete the activity entry. */
$userMailer = new UserMailer(0);
$result = $userMailer->TestSMTP($sendgrid,$smtpHost,$smtpUser,$smtpPass,$smtpPort,$fromName,$fromEmail,$bccEmail,$emailType,$firstTestEmail);

if($result == "Success"){
    /* Send back the XML data. */
    $interface->outputXMLSuccessPage($result);
}

?>
