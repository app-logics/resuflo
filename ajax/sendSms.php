<?php
/*
 * RESUFLO
 * AJAX Activity Entry Deletion Interface
 *
 * Copyright (C) 2005 - 2007 Econn Technologies, Inc.
 *
 *
 * The contents of this file are subject to the RESUFLO Public License
 * Version 1.1a (the "License"); you may not use this file except in
 * compliance with the License. You may obtain a copy of the License at
 * http://www.econnoisseur.com/.
 *
 * Software distributed under the License is distributed on an "AS IS"
 * basis, WITHOUT WARRANTY OF ANY KIND, either express or implied. See the
 * License for the specific language governing rights and limitations
 * under the License.
 *
 * The Original Code is "RESUFLO Standard Edition".
 *
 * The Initial Developer of the Original Code is Econn Technologies, Inc.
 * Portions created by the Initial Developer are Copyright (C) 2005 - 2007
 * (or from the year in which this file was created to the year 2007) by
 * Econn Technologies, Inc. All Rights Reserved.
 *
 *
 * $Id: deleteActivity.php 1479 2007-01-17 00:22:21Z will $
 */

include_once('./lib/UserMailer.php');
include_once('./lib/Candidates.php');
// based on where you downloaded and unzipped the SDK
include_once('./que/twilio-php-master/Twilio/autoload.php');

// Use the REST API Client to make requests to the Twilio REST API
use Twilio\Rest\Client;

$interface = new SecureAJAXInterface();
$siteID = $interface->getSiteID();
$userId = 0;
if(isset($_SESSION['RESUFLO'])){
    $userId = $_SESSION['RESUFLO']->getuserid();
}

$CandidateId = $_REQUEST['CandidateId'];
$To = $_REQUEST['To'];
$Message = $_REQUEST['Message'];

$candidates = new Candidates($siteID);
$candidate = $candidates->get($CandidateId);
$datefunc=date("d-M-y");
$Message=str_replace('$$NAME$$',$candidate['firstName'],$Message);
$Message=str_replace('$$FIRSTNAME$$',$candidate['firstName'],$Message);
$Message=str_replace('$$LASTNAME$$',$candidate['lastName'],$Message);
$Message=str_replace('$$EMAIL$$',$candidate['email1'],$Message);
$Message=str_replace('$$PHONE$$',$candidate['phoneCell'],$Message);
$Message=str_replace('$$CSOURCE$$',$candidate['source'],$Message);
$Message=str_replace('$$CURDATE$$',$datefunc,$Message);
            
/* Delete the activity entry. */
$userMailer = new UserMailer($userId, $siteID);
$result = $userMailer->SendSms($CandidateId, $To, $Message);

if($result == TRUE){
    /* Send back the XML data. */
    $interface->outputXMLSuccessPage($result);
}

?>
