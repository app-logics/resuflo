<?php
class jjp {

  var $flds = 'Resume Date-Time,First Name,Last Name,Email Address,Phone,City,State,Zipcode,Export Date-Time,Resume Source';

  var $flds1 = 'last name,first name,middle name,phone home,phone cell,phone work,address,city,state,zip,keyskills,email1,eamil2,category,Date Of Birth,Gender,Father Name,Mother Name,Marital Status,Passport No,Nationality,Worked Period,Gap Period,Fax No,License No,Hobbies,Qualification,Achievements,Objectives,Experience,References,Job Profile,desired pay,current pay,best time to call,current employer,Language Known,Detail Resume';

  var $flds2 = 'LastName,FirstName,Middlename,Phone,Mobile,Phonework,Address,City,State,ZipCode,Skills,Email,Email2,Category,DateOfBirth,Gender,MotherName,FatherName,MaritalStatus,PassportNo,Nationality,WorkedPeriod,GapPeriod,FaxNo,LicenseNo,Hobbies,Qualification,Achievments,Objectives,Experience,References,JobProfile,ExpectedSalary,CurrentSalary,besttimetocall,CurrentEmployer,languageknown,DetailResume';

  function __construct($db,$debug=0,$debug_annoying=0) {
    $this->debug_annoying = $debug_annoying;
    $this->debug = $debug;
    $this->db = $db;
    $this->fldsa = explode(',',$this->flds);
  }

  function process_files($files, $user_name, $glob) {
    $debug_annoying = $this->debug_annoying;
    $this->glob = $glob;
    $this->_getuser($user_name);

    $this->files_sent = count($files);   
    $this->files_parsed = 0;

    foreach ($files as &$f) {
        if ($debug_annoying) echo "processing folder: processing ".basename($f)."\n-------------------------------------------------\n";
        $this->files_parsed += $this->_parse_and_insert_file($f);
    }
	//continue;
    //$this->resume_bak_dir = str_replace(RESUME_UPLOAD_PATH, RESUME_BACKUP_PATH, $this->resume_dir);
    $this->resume_bak_dir = RESUME_BACKUP_PATH;

    // don't forget to zip it up bfore removing it 
    $date = date('_Y-m-d_Hi.s__f').$this->files_sent;
    $res_dir = $this->resume_dir.$user_name;
    $bak_dir = $this->resume_bak_dir.$user_name; // already has username in it
    $targz = $bak_dir.'\\'.$user_name.$date.'.tar.gz';

    require_once('jj-backup.php');
    $bak = new jj_backup();
    $bak->backitup($bak_dir,$res_dir,$targz,$user_name,$this->debug,$debug_annoying);
  }

  private function _parse_and_insert_file($file) {
    $int = 0; // returns integer

    if ($this->debug) echo "\n\n--------------------------------\n";
    if ($this->debug) echo "parsing and inserting $file\n";
    if ($this->debug) echo "--------------------------------\n";

    $contents = file_get_contents($file);
    // first grab a big glob of what we need
    $pattern = '/Resume Date-Time.*Resume Source/s'; 
    preg_match($pattern, $contents, $matches);

    if (empty($matches)) {
        echo "error finding Resume Date-Time in $fileurl\n";
        //Jeff said date doesn't matter so ignore it
        //return 0;
    }

    // well most of what we need...try to get a resume source
    $pattern = '/Resume Source(.*)/';
    preg_match($pattern, $contents, $res_src_mat);

    // glue the resume source back onto the data blog and declare it
    $data_glob = $matches[0].substr($res_src_mat[0], strlen('Resume Source'));

    // loop over the fields in the data glob and get the values of each one out
    $r = array();

    // we need candidate later but for now we need the flds-array

    foreach ($this->fldsa as &$fld) {
        $pattern = '/'.$fld.'[:]?\s?(.*)/';
        preg_match($pattern, $contents, $m);
        // zip file name: [basename($filename)];
        $r[$fld] = $m[1];
    }

    // prevent files from being processed if they have no email address
    if (strpos($r['Email Address'], '@')===false || strpos($r['Email Address'], '.')===false) {
        if ($this->debug) echo("returning on this candidate (Email is empty/invalid)...\n");
        return 0;
    }

    $mrk = '<<Resume Content Follows>>';
    $strpos = strpos($contents, $mrk);
    if ($strpos===false) $strpos = 0;
    // i origianlly accidentally put the resume in notes...but that is Misc Notes and the resume is already being added elsewhere
    $detail_resume = trim(substr($contents, $strpos+strlen($mrk)));
    $notes = '';

    if ($this->debug) echo "$r:<pre>".print_r($r,true)."</pre>\n";

    // candidate add calls other files that need a the userid in the session...so i'm sending them the GLOBAL version
    $GLOBALS['user_id'] = $this->_user['user_id'];
    $site_id = $this->_user['site_id'];
    $this->_siteID = $site_id;
    
    //TODO make sure we prevent duplicate entries
    $candidate = new Candidates($site_id);

    $owner = $entered_by = $this->_user['user_id'];

    // we also need to insert the drip campaign...first get the default drip campaign id
    //$sql = 'select id from dripmarket_compaign where default = 1 and userid = '.$entered_by;
    //$r = $this->db->getAllAssoc($sql);
    //$camp_id = (!empty($r[0]['id'])) ? $r[0]['id'] : 0;
    $camp_id = 0;
    // just put them in nothing, which is calculated later as the default group
    
    // import opt-out trumps all other things!
    if (Importoptout::is_optout_email($r['Email Address'])) {
        if ($this->debug) echo("IMPORT opted out...ignoring candidate...\n");
        return 0;
    }    

    $resume_source = '';
    if(strpos($detail_resume, "Resume Text for MemberID:") !== false)
    {
        $resume_source = "Nexxt";
    }
    else if(strpos($detail_resume, "Monster resume #") !== false)
    {
        $resume_source = "Monster.com - Resumes (API recent)";
    }
    else if(strpos($detail_resume, "Beyond.com") !== false)
    {
        $resume_source = "Beyond.com - Resumes (API recent)";
    }
    else if(strpos($detail_resume, "RecruitMilitary") !== false)
    {
        $resume_source = "RecruitMilitary - Resumes";
    }
    else if(strpos($detail_resume, "TheLadders") !== false)
    {
        $resume_source = "TheLadders - Resumes";
    }
    else if(strpos($detail_resume, "*** Basic Profile for Resume Posted:") !== false)
    {
        $resume_source = "CareerBuilder - Resumes (WSI recent)";
    }
    else
    {
        $resume_source = $r['Resume Source'];
    }
    $candidateId = $candidate->add($r['First Name'], @$middle, $r['Last Name'], $r['Email Address'], @$email2, $r['Phone'], $r['Phone'], @$phoneWork, @$address, $r['City'], $r['State'], $r['Zipcode'], $resume_source, @$keySkills, @$dateAvailable, @$currentEmployer, @$canRelocate, @$currentPay, @$desiredPay, @$notes, @$webSite, @$bestTimeToCall, $entered_by, $entered_by, @$gender, @$race, @$veteran, @$disability, @$lifeLic, @$propLic, @$ser6, @$ser63, @$ser7, $camp_id, /*$skipHistory = */false);

    if ($this->debug) echo "new candidate ids? ($candidateId)\n";

    if (empty($candidateId)) {
        if ($this->debug) echo("returning on this candidate...\n");
        return 0;
    } else if($candidateId > 0) {
        // we just need to insert the resume body...TODO this really belongs in Candidates...but that legacy code is plain poop
        $extra_sql = 'insert into extra_field (field_name, data_item_id, value, import_id, site_id, data_item_type) values ("Detail Resume", '.$candidateId.', "'.mysql_real_escape_string(utf8_encode($detail_resume)).'", 0, '.$this->_siteID.', 100)';
        $this->db->query($extra_sql);
    }
    return $int;
  }

  private function _get_master_account_user_id($user_id) {
    $id = (integer) $user_id;
    $sql='select u1.user_id from user u1 left join user u2 on u1.user_id = u2.entered_by where (u2.user_id = "'.$id.'" and u1.user_id <> 1) || u1.user_id = "'.$id.'" order by u1.user_id asc limit 1';
    $r = $this->db->getAllAssoc($sql);

    if (empty($r)) {
        die('FATAL ERROR: could not find user ('.$user_id.') master account using this query: '.$sql);
    }

    return $r[0]['user_id'];
  }
 
  public function _getuser($user_name) {
    //code to get username
    $sql='SELECT user_id, site_id, user_name, first_name FROM user WHERE access_level > 0 AND user_name="'.mysql_real_escape_string($user_name).'"';
    $r = $this->db->getAllAssoc($sql);

    $this->_userid = $r[0]['user_id'];
    $this->_username = $r[0]['user_name'];
    $this->_user = $r[0];

    if (empty($r)) {
        echo('FATAL ERROR: could not find user matching folder name: '.$user_name);
        return -1;
    }
    else
    {
        return 1;
    }
    
  }

  function _rmdir($dir) {
    // local directory to $resume_dir only
    if (strpos($dir, '..')) die('FATAL ERROR: ".." may not be used in dir path');
    if (strpos($dir, '\\')) die('FATAL ERROR: "/" may not be used in dir path');
    $r = @rmdir($dir);
    //if (!$r) die('FATAL ERROR: could not remove empty dir "'.$dir.'"');
  }
}
