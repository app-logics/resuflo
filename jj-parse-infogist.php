<?php
if (!empty($_SERVER['HTTP_HOST'])) die();
include_once('config.php');

//================================= Resume parsing for virtual server  =====================================
$base = RESUME_UPLOAD_PATH;
$dir_array = array();
if (!is_dir($base)) {
		return $dir_array;
}

if ($dh = opendir($base)) {
    while (($file=readdir($dh)) !== false) {
                    if ($file == '.' || $file == '..') continue;

                    if (is_dir($base.'/'.$file)) {
                            $stat = stat($base.'/'.$file);
                            $dir_array[] = $stat['mtime'];;
                    }
    }
    closedir($dh);
    //file copied in last
    $max_value =max($dir_array);
    $maxDate = date('d M Y H:i:s',$max_value);

    //Convert them to timestamps.
    $date1Timestamp = strtotime($maxDate);
    $date2Timestamp = strtotime(date('d M Y H:i:s'));

    //Calculate how much time has been past to last copied file.
    $difference = ceil(($date2Timestamp - $date1Timestamp)/60);
    //If less then 2 minutes to last copied file; mean may be copy is not complete yet
    if($difference < 2)
    {
            echo 'Wait for write operation';
            die();
    }
}

require('jp.class.php');


$debug_on_commandline = true;

$base_path = str_replace(basename(__FILE__), '', __FILE__);
//$resume_dir = $base_path.RESUME_UPLOAD_PATH.'\\';
$resume_dir = RESUME_UPLOAD_PATH;

// this fixes some issues with the horrid legacy code from econn
chdir($base_path);

// this is for production (set it to false to watch it real time!):
$debug = true;
$debug_annoying = true;

// this causes all output (normally emailed to email_debug csv list) to go to stdout
if ($debug_on_commandline) {
  $email_debug = '';
} else {
  //$email_debug = 'jeff@recruiters-edge.com,malikabiid@gmail.com';
  $email_debug = 'malikabiid@gmail.com';
  ob_start();
}

//if ($debug_annoying) echo "running jj-parse-infogist.php, env:\n".print_r($_SERVER, 1)."\n-------------------------------------------------\n";
if ($debug_annoying) echo "running jj-parse-infogist.php\n-------------------------------------------------\n";

include_once('lib\DatabaseConnection2.php');

require('lib\Candidates.php');
include_once('constants.php');

$db = DatabaseConnection::getInstance();

$jjp = new jjp($db,$debug,$debug_annoying);

// path to resume uploads from cygwin+jeff's batch file
$jjp->base_path = $base_path;
$jjp->resume_dir = $resume_dir;

// this is all the folders that have been uploaded
if ($debug_annoying) echo "finding folders in: ".$resume_dir."*\n-------------------------------------------------\n";
$folders = glob($resume_dir.'*');

if ($debug_annoying) echo "folders found: ".print_r($folders, 1)."\n-------------------------------------------------\n";

// remove the empty ones ... process the others
foreach ($folders as &$f) {
    if ($debug_annoying) echo "processing folder: ".basename($f)."\n-------------------------------------------------\n";
    $p = $f.'/*';
    // note to developer attempting to run this from the web...wrong user. no access to files.
    // MUST test this as root (or maybe jj-ftp but that is not tested)
    $files = glob($p);
    if (empty($files)) {
        if ($debug_annoying) echo "processing folder: ".basename($f)." [ no files were found, removing dir ]\n-------------------------------------------------\n";
        $r = rmdir($f);
        $jjp->_rmdir(basename($f));
        if ($debug) echo 'removed '.basename($f).' (empty)'."\n";
    } else {
        if ($debug_annoying) echo "processing folder: ".basename($f)." [ ".count($files)." files were found ]\n-------------------------------------------------\n";
	// we must have files to process!  let's get crack-alackin!
        if ($debug) echo 'processing '.basename($f).' ('.count($files).' files)'."\n";
		
        $userExist = $jjp->_getuser(basename($f));
        if($userExist == 1){
            //echo "parsed";
            $jjp->process_files($files, basename($f), $p);
        }
        //If folder not matches to database username; then try to fetch next folder
        else{
            //echo "skipped";
            continue;
        }
    }

if (empty($debug_on_commandline)) {
    $message = ob_get_contents();
    if (!empty($message)) mail($email_debug, 'jjparse: '.basename($f).' '.date('Y-m-d'), $message);
    ob_end_clean();
}
    break; // we can only do one folder per thread
}
//die("End");
//================================= Resume parsing for resuflo server  =====================================
$base = DIRECTRESUME_UPLOAD_PATH;
$dir_array = array();
if (!is_dir($base)) {
		return $dir_array;
}

if ($dh = opendir($base)) {
    while (($file=readdir($dh)) !== false) {
                    if ($file == '.' || $file == '..') continue;

                    if (is_dir($base.'/'.$file)) {
                            $stat = stat($base.'/'.$file);
                            $dir_array[] = $stat['mtime'];;
                    }
    }
    closedir($dh);
    $max_value =max($dir_array);
    $maxDate = date('d M Y H:i:s',$max_value);

    //Convert them to timestamps.
    $date1Timestamp = strtotime($maxDate);
    $date2Timestamp = strtotime(date('d M Y H:i:s'));

    //Calculate the difference.
    $difference = ceil(($date2Timestamp - $date1Timestamp)/60);
    if($difference < 2)
    {
            echo 'Wait for write operation';
            die();
    }
}

$debug_on_commandline = true;

$base_path = str_replace(basename(__FILE__), '', __FILE__);
//$resume_dir = $base_path.DIRECTRESUME_UPLOAD_PATH.'\\';
$resume_dir = DIRECTRESUME_UPLOAD_PATH;

// this fixes some issues with the horrid legacy code from econn
chdir($base_path);

// this is for production (set it to false to watch it real time!):
$debug = true;
$debug_annoying = true;

// this causes all output (normally emailed to email_debug csv list) to go to stdout
if ($debug_on_commandline) {
  $email_debug = '';
} else {
  //$email_debug = 'jeff@recruiters-edge.com,jay@toptal.com';
  $email_debug = 'malikabiid@gmail.com';
  ob_start();
}

//if ($debug_annoying) echo "running jj-parse-infogist.php, env:\n".print_r($_SERVER, 1)."\n-------------------------------------------------\n";
if ($debug_annoying) echo "running jj-parse-infogist.php\n-------------------------------------------------\n";


include_once('config.php');
include_once('constants.php');
$db = DatabaseConnection::getInstance();

$jjp = new jjp($db,$debug,$debug_annoying);

// path to resume uploads from cygwin+jeff's batch file
$jjp->base_path = $base_path;
$jjp->resume_dir = $resume_dir;

// this is all the folders that have been uploaded
if ($debug_annoying) echo "finding folders in: ".$resume_dir."*\n-------------------------------------------------\n";
$folders = glob($resume_dir.'*');

if ($debug_annoying) echo "folders found: ".print_r($folders, 1)."\n-------------------------------------------------\n";

// remove the empty ones ... process the others
foreach ($folders as &$f) {
    if ($debug_annoying) echo "processing folder: ".basename($f)."\n-------------------------------------------------\n";
    $p = $f.'/*';
    // note to developer attempting to run this from the web...wrong user. no access to files.
    // MUST test this as root (or maybe jj-ftp but that is not tested)
    $files = glob($p);
    if (empty($files)) {
        if ($debug_annoying) echo "processing folder: ".basename($f)." [ no files were found, removing dir ]\n-------------------------------------------------\n";
        $r = rmdir($f);
        $jjp->_rmdir(basename($f));
        if ($debug) echo 'removed '.basename($f).' (empty)'."\n";
    } else {
        if ($debug_annoying) echo "processing folder: ".basename($f)." [ ".count($files)." files were found ]\n-------------------------------------------------\n";
		// we must have files to process!  let's get crack-alackin!
        if ($debug) echo 'processing '.basename($f).' ('.count($files).' files)'."\n";
	
        $userExist = $jjp->_getuser(basename($f));
        if($userExist){
            $jjp->process_files($files, basename($f), $p);
        }
        //If folder not matches to database username; then try to fetch next folder
        else{
            continue;
        }
    }

if (empty($debug_on_commandline)) {
    $message = ob_get_contents();
    if (!empty($message)) mail($email_debug, 'jjparse: '.basename($f).' '.date('Y-m-d'), $message);
    ob_end_clean();
}

    die(); // we can only do one folder per thread
}
