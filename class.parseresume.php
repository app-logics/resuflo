<?php

include_once('lib/Importoptout.php');
require_once('asset/constants.php');
include_once('lib/phpmailer6/vendor/autoload.php');
include_once('lib/UserMailer.php');

class PARSERESUME {
    var $_uploaderror='';
    var $_resumeuploadfile='';
    var $_resumeuploadsucess = "";
    var $_filterfile = "";
    var $_user = "";
    var $_uploaderrorArr= array();
    var $_countfiles='';
    var $_countresumefile=0;
    /**
    * to give report message
    */
    var $_reportmessage='';
    var $_username;
    var $_totalnooffiles=0;
    var $_curruptfiles=0;
    var $_totalnoofrecordsexcel=0;
    var $_curruptrecordsexcel=0;
    var $_filetype='';
    var $_recordno=0;
    var $_excelfilename='';
    var $_uploadfrombrowser=0;

    var $flds1 = 'last name,first name,middle name,phone home,phone cell,phone work,address,city,state,zip,keyskills,email1,eamil2,category,Date Of Birth,Gender,Father Name,Mother Name,Marital Status,Passport No,Nationality,Worked Period,Gap Period,Fax No,License No,Hobbies,Qualification,Achievements,Objectives,Experience,References,Job Profile,desired pay,current pay,best time to call,current employer,Language Known,Detail Resume';

    var $flds2 = 'LastName,FirstName,Middlename,Phone,Mobile,Phonework,Address,City,State,ZipCode,Skills,Email,Email2,Category,DateOfBirth,Gender,MotherName,FatherName,MaritalStatus,PassportNo,Nationality,WorkedPeriod,GapPeriod,FaxNo,LicenseNo,Hobbies,Qualification,Achievments,Objectives,Experience,References,JobProfile,ExpectedSalary,CurrentSalary,besttimetocall,CurrentEmployer,languageknown,DetailResume';

    function PARSERESUME() {
        // warning: this construct is run on EVERY call to this class.
        // you have been warned

        // jj's fld vs fld2 debugger helper...these MUST match up!
        /*echo count($flds).' = '.count($flds2)."\n";
        for ($i=0;$i<count($flds);$i++) {
          echo $flds[$i].' = '.$flds2[$i]."\n";
        }*/
    }

    /**
    * function for sending resume via mail
    */
    function SubmitResume() {

        $this->SaveUplodedFile();
        //$this->UnZip();
        $message = '';
        if ($this->_uploaderror=='') {

            if ($this->_filterfile !="zip") {
            //$message = "Your File is submitted successfully.Processing of resume going on...<br>You can see the candidates in Candidate list after few minutes.<br>Redirecting....";
            }
        }
        return $message." ".$this->_uploaderror;
    }


    function UnZip() {

        $archive = new PclZip($_FILES["resumecontent"]["tmp_name"]);
        $temp_array = explode(".",$_FILES["resumecontent"]["name"]);
        $username_unzip = $temp_array(0);

        if (($v_result_list = $archive->extract(PCLZIP_OPT_PATH, 'upload/resume/'.$username_unzip)) == 0) {
            die("Error : ".$archive->errorInfo(true));
        }
        /*echo "<pre>";
        var_dump($v_result_list);
        echo "</pre>";*/
    }

    /**
    * function for saving the uploaded file on server
    */
    function SaveUplodedFile() {

        if ($_FILES["resumecontent"]["tmp_name"] ) {

            //file should be upto 1 MB - 1048576
            // 1,073,741,824 - 1 GB
            if ($_FILES["resumecontent"]["size"] <  1073741824) {

                //$filename = explode(".",$_FILES["resumecontent"]["name"]);
                $explodemainfilename = explode(".",basename($_FILES["resumecontent"]["name"]));
                $countmainexplode = count($explodemainfilename);
                $filterfile = $explodemainfilename[$countmainexplode-1] ;

                if ($filterfile == "rtf" || $filterfile == "doc" || $filterfile == "docx" || $filterfile== "zip" || $filterfile == "html" || $filterfile == "htm" || $filterfile == "pdf" || $filterfile == "odt" || $filterfile == "txt"  || $filterfile == "TXT"  || $filterfile == "HTM" || $filterfile == "HTML" || $filterfile == "csv") {

                    if ($_FILES["resumecontent"]["error"] > 0) {

                        $this->_uploaderror.= "<span align='center' class='msg'>Return Code: ".$_FILES["resumecontent"]["error"]."</span><br />";
                    } 
                    else {

                        //explode the name ad append the timestamp
                        //$name = explode(".",str_replace(".","_",$_FILES["resumecontent"]["name"]));

                        $char = array("'",' ');
                        $cleanfilename = str_replace($char, '_', $explodemainfilename[0]);
                        $newName = $cleanfilename.'_'.time().".$filterfile";

                        //$this->_resumeuploadfile = "upload/$newName";
                        $this->_resumeuploadfile = USER_UPLOAD_PATH . "$newName";
                        
                        if (!move_uploaded_file($_FILES["resumecontent"]["tmp_name"], $this->_resumeuploadfile)) {
                          die("the file could not be moved to the upload directory.  this was a fatal error.  615-290-5858.");
                        }

                        //jj working this far
                        //die('<pre>'.print_r("upload/$newName" ,true));

                        $this->_resumeuploadsucess = "yes";
                        $this->_filterfile = $filterfile;
                        $this->_excelfilename=$newName;

                        //if the file is a csv file then process it further

                        if ($filterfile=="xlsx" || $filterfile=="xls" || $filterfile=="csv") {
                            $camp_id = $_REQUEST['camp_id'];
                            $dName = $_SERVER["SERVER_NAME"];
                            //$fileurl = SERVER_NAME.$unzipfilename;
                            //$fileurl = "./upload/".$this->_excelfilename;
                            $fileurl = USER_UPLOAD_PATH .$this->_excelfilename;
                            $this->_uploadfrombrowser=1;
                            $filesize=filesize($fileurl);

                            //file should be upto 1 MB - 1048576
                            if ($filesize > 1048576) {
                                continue;
                            }


                            $userid=$_SESSION['RESUFLO']->getUserID();
                            //code to get username
                            $sql="SELECT user_name, site_id, email FROM user WHERE user_id=$userid";
                            $db7 = DatabaseConnection::getInstance();
                            $resourcedetail = $db7->query($sql);

                            $resultarray=mysql_fetch_assoc($resourcedetail);
                            $this->_username=$resultarray['user_name'];
                            $this->_siteId=$resultarray['site_id'];

                            //code to get username
//die("$fileurl,$userid,'',$fileurl,$emailalert,$filterfile,$camp_id");
                            $resultprocesszip=$this->InsertIntoDBexcelData($fileurl,$userid,'',$this->_excelfilename,$emailalert,$filterfile,$camp_id,$this->_siteId);

                            if ($this->_reportmessage!='') {

                                $resulttotalfile=$this->_totalnooffiles;

                                if ($this->_filetype=='csv'||$this->_filetype=='xls'||$this->_filetype=='xlsx') {
                                    $totalnoofrec=$this->_totalnoofrecordsexcel;
                                    $reportmessage="<br/><b>Total Number of  Records : ".$totalnoofrec."</b><br/><b>Total Number of incorrect records : ".$this->_curruptrecordsexcel."</b><br/><table border='2' style='padding:2px'><tr><td style='padding:0px 5px 0px 5px'>Client Name</td><td style='padding:0px 5px 0px 5px'>Filename</td><td style='padding:0px 5px 0px 5px'>Record no</td><td style='padding:0px 5px 0px 5px'>Reason</td></tr>";
                                }

                                $reportmessage="reportmsg".$reportmessage.$this->_reportmessage."</table>";
                                $this->_curruptfiles=$this->_curruptfiles+1;

                            } else {
                                $reportmessage="reportmsg"."All candidates have been inserted for Client - <strong>" .$this->_username."</strong>";
                            }
                            $repmsg=explode("reportmsg",$reportmessage);
                            try{
                                file_put_contents('C:/Windows/Temp/ResufloLog/UserMailerCaller.txt', PHP_EOL.(new DateTime())->format('d m Y H:i:s').'===== START resumeparser', FILE_APPEND);
                                $email = new UserMailer(1);
                                $result = $email->SendEmail(array(TechnicalReport), null, MAILSUBJECTREPORT, $repmsg[1]);
                                file_put_contents('C:/Windows/Temp/ResufloLog/UserMailerCaller.txt', '=> END ====='.$result, FILE_APPEND);

                                file_put_contents('C:/Windows/Temp/ResufloLog/UserMailerCaller.txt', PHP_EOL.(new DateTime())->format('d m Y H:i:s').'===== START resumeparser', FILE_APPEND);
                                $UserEmail = new UserMailer($userid);
                                $result = $UserEmail->SendEmail(array($resultarray['email']), null, MAILSUBJECTREPORT, $repmsg[1]);
                                file_put_contents('C:/Windows/Temp/ResufloLog/UserMailerCaller.txt', '=> END ====='.$result, FILE_APPEND);
                            }
                            catch (Exception $e) {
                                file_put_contents('C:/Windows/Temp/ResufloLog/UserMailerCaller.txt', (new DateTime())->format('d m Y H:i:s').'resumeparser ====='.$e->getMessage().PHP_EOL, FILE_APPEND);
                            }
                        }

                        //if the file is a csv file then process it further

                        if ($filterfile == "rtf" || $filterfile == "doc" || $filterfile == "docx" || $filterfile == "html" || $filterfile == "htm" || $filterfile == "pdf" || $filterfile == "odt"  || $filterfile == "HTM" || $filterfile == "txt" || $filterfile == "TXT") {

                            $filesize=filesize($this->_resumeuploadfile);

                            if ($filesize > 1048576) {
                                $this->_uploaderror.= "<span align='center' class='msg'>Invalid file.Document upto 1 MB size can be parsed.</span>";
                            } 
                            else {
                                try {
                                    $userid=$_SESSION['RESUFLO']->getUserID();
                                    $camp_id = $_REQUEST['camp_id'];
                                    
                                    $explodefilename = explode(".",basename($this->_resumeuploadfile));
                                    $countexplode = count($explodefilename);
                                    //check the extension
                                    $fileextension = strtolower($explodefilename[$countexplode-1]);
                    
                                    $docObj = new DocxConversion($this->_resumeuploadfile);
                                    $xmlArray['DetailResume'] = $docObj->convertToText();
                                    $xmlArray['Email'] = $docObj->extract_emails_from($xmlArray['DetailResume']);
                                    $xmlArray['FirstName'] = explode("_",$explodefilename[0])[1];
                                    $xmlArray['LastName'] = explode("_",$explodefilename[0])[0];
                                    $xmlArray['Email2']="";
                                    $xmlArray['Middlename']="";
                                    $xmlArray['Phone']="";
                                    $xmlArray['Mobile']="";
                                    $xmlArray['Address']="";
                                    $xmlArray['City']="";
                                    $xmlArray['State']="";
                                    $xmlArray['ZipCode']="";
                                    $xmlArray['CurrentSalary']="";
                                    $xmlArray['Phonework']=""; 
                                    $xmlArray['ExpectedSalary']="";
                                    $xmlArray['Skills']="";
                                    $xmlArray['CurrentEmployer']="";
                                    $xmlArray['besttimetocall']="";
                                    $xmlArray['languageknown']="";

                                } catch(Exception $e) {
                                    echo 'xml parse exception: ' .$e->getMessage();
                                }
                                $xmlArray['errormsg']='';
                                if ($parsedxml=='Caught exception') {
                                    $filenameerror=basename($unzipfilename);
                                    $this->_reportmessage.="<tr><td style='padding:0px 5px 0px 5px'>".$this->_username."</td><td style='padding:0px 5px 0px 5px'>".$filenameerror."</td><td style='padding:0px 5px 0px 5px'>Connection time out it may be big file</td></tr>";
                                    $this->_curruptfiles=$this->_curruptfiles+1;
                                } elseif (trim($xmlArray['FirstName'])=='' || $xmlArray['Email']=='') {

                                    //$filenameerror=str_replace('upload/resume/'.$username_unzip.'/','',$unzipfilename);
                                    $filenameerror=str_replace(USER_UPLOAD_PATH.$username_unzip.'/','',$unzipfilename);
                                    
                                    if ($xmlArray['errormsg']=='') {

                                        $this->_reportmessage.="<tr><td style='padding:0px 5px 0px 5px'>".$this->_username."</td><td style='padding:0px 5px 0px 5px'>".$filenameerror."</td><td style='padding:0px 5px 0px 5px'>Email id is blank</td></tr>";
                                        $this->_curruptfiles=$this->_curruptfiles+1;
                                    } else {
                                        $this->_reportmessage.="<tr><td style='padding:0px 5px 0px 5px'>".$this->_username."</td><td style='padding:0px 5px 0px 5px'>".$filenameerror."</td><td style='padding:0px 5px 0px 5px'>".trim($xmlArray['errormsg'])."</td></tr>";
                                        $this->_curruptfiles=$this->_curruptfiles+1;
                                    }
                                }

                                if ($xmlArray['error_message']!='') {
                                    //$filenameerror=str_replace('upload/resume/'.$username_unzip.'/','',$unzipfilename);
                                    $filenameerror=str_replace(USER_UPLOAD_PATH.$username_unzip.'/','',$unzipfilename);
                                    $this->_reportmessage.="<tr><td style='padding:0px 5px 0px 5px'>".$this->_username."</td><td style='padding:0px 5px 0px 5px'>".$filenameerror."</td><td style='padding:0px 5px 0px 5px'>".$xmlArray['error_message']."</td></tr>";
                                    $this->_curruptfiles=$this->_curruptfiles+1;
                                }

                                if ($xmlArray['FirstName']!='' && $xmlArray['Email']!='') {
                                    $count++;
                                    
                                    //echo "calling InsertIntoDBZipData on {$xmlArray['FirstName']}\n";
                                    $resultprocesszip=$this->InsertIntoDBDocData($xmlArray,$userid,$db,$this->_resumeuploadfile,$emailalert,$camp_id);
                                }
                                return $xmlArray ;

                                // $dName = $SERVER_NAME;
                                // $fileurl = SERVER_NAME.$this->_resumeuploadfile;
                                $fileurl = "./".$this->_resumeuploadfile;

                //$fileurl = "http://122.160.44.162:90/v1global/".$this->_resumeuploadfile;
                $parsedxml = $this->GetXML($fileurl,'',$newName,$this->_username);
                //echo $parsedxml;
                $xmlArray = $this->SplitXML($parsedxml);
                //Removal of special characters from Array Data Date: 30/01/2013

                foreach ($xmlArray as $key1 => $value1) {
                    $xmlArray[$key1]=preg_replace('#[^\w()/.%\-&@]#'," ",$value1);
                    $xmlArray[$key1]= str_replace('/','-',$xmlArray[$key1]);
                }

                unlink($this->_resumeuploadfile);
                return $xmlArray ;
                }
            } elseif ($filterfile == "zip") {

                //$this->ProcessZip();
                $siteid = $_SESSION['resumeinsertsiteid'];
                $candidates = new Candidates($siteid);
                $key = $_SESSION['RESUFLO']->getCountryKey();
                $candidates->SaveZipRecordForProcessing($this->_resumeuploadfile,$key);
            }
            //get xml
            }
        } else {
             $this->_uploaderror.= "<span align='center' class='msg'>Invalid file format. Support .doc, .docx, .csv or .zip having doc and docx formats</span>";
        }
            } else {
                 $this->_uploaderror.= "<span align='center' class='msg'>Invalid file.Please upload file upto 1 GB size</span>";
            }
        }
        //return true;
    }

    function GetXML($doc='',$key='',$newname,$zipName) {
        $type = 'txt';
        $starttime = microtime();
        
        $stream=fopen($doc,'r');
        $content = stream_get_contents($stream);
        
        $ds = new RchilliService();
        $hrxml = $ds->getHRXMLBinary($content,$type,$key,$newname,$zipName);

        $endtime = microtime();
        //echo "<br>Parsed in ".($endtime - $startime). " seconds ";
        fclose($stream);
        //$this->SplitXML($hrxml);
        //echo $hrxml;
        return $hrxml;
    }

    function SplitXML($xml){

        $xmlDoc = new DOMDocument();
        $xmlDoc->loadXML($xml);

        $x = $xmlDoc->documentElement;

        if (empty($x->childNodes)) return false;

        foreach ($x->childNodes AS $item) {
            $arr[$item->nodeName] = $item->nodeValue;
        }
        return $arr;
    }

    //function for processing excel file
    function processexcel($xml) {
    }
    function getAsXMLContent($xmlElement)
    {
        $content=$xmlElement->asXML();

        $end=stripos($content,'>');
        if ($end!==false)
        {
          $tag=substr($content, 1, $end-1);

          return str_replace(array('<'.$tag.'>', '</'.$tag.'>'), '', $content);
        }
        else
          return '';
    }
    function ProcessZip($filename,$key,$userid,$db,$emailalert,$camp_id='',$processzip_id=NULL,$siteId='') {

        //echo "ProcessZip called on $filename for $userid";

        //code to get username
        $sql="SELECT user_name FROM user WHERE user_id=$userid";
        $db7 = DatabaseConnection::getInstance();
        $resourcedetail = $db7->query($sql);

        $resultarray=mysql_fetch_assoc($resourcedetail);

        $this->_username=$resultarray['user_name'];
        //$filename = "upload/".$this->_username.".zip";
        $username_unzip=$resultarray['user_name'];
        //echo @username_unzip;

        //code to get username
        if ($filename !="") {

            //code to move file to another folder

            $explodemainfilename = explode(".",$filename);
            $countmainexplode = count($explodemainfilename);
            $filterfilename = $explodemainfilename[$countmainexplode-2] ;

            //$filterfilename=$filterfilename.time().".".$explodemainfilename[$countmainexplode-1] ;
            $filterfilename=$filterfilename.".".$explodemainfilename[$countmainexplode-1] ;
            
            //$newfile ="backup/".$filterfilename;
            $newfile =RESUME_BACKUP_PATH.$filterfilename;
                        
            $copy_from_path = USER_UPLOAD_PATH.$filterfilename;
            
            /* For backup purpose */
            if (!copy($copy_from_path, $newfile)) {
                echo __METHOD__." failed to copy $filename to " . RESUME_BACKUP_PATH ."\n";
            }
            //$dir_usernamezip = "upload/resume/".$username_unzip;
            $dir_usernamezip = USER_UPLOAD_EXTRACT_PATH.$username_unzip;

            
//            shell_exec("RMDIR /Q /S ".str_replace("/", "\\", $dir_usernamezip));
//            mkdir($dir_usernamezip,0770,true);
//            
//            if (($v_result_list = $archive->extract(PCLZIP_OPT_PATH, $dir_usernamezip)) == 0) {
//                $msg_ext = 'Zip Cound not extract.<br>Error : '.$archive->errorInfo(true);
//                echo $msg_ext."\n";
//                $this->_uploaderror .= $msg_ext;
//            }
                
            if (!is_dir($dir_usernamezip)) {
                if (!mkdir($dir_usernamezip, 0755, true)) {
                    $sqlUpdate = 'UPDATE processzip SET inqueue ="N", status = "pending", queuedcount = queuedcount + 1 WHERE id = '.$processzip_id;
                    $db7->query($sqlUpdate);
                    echo 'Folder not created on this path '. USER_UPLOAD_EXTRACT_PATH . ' , process can not continue.</br>';
                    exit;
                }
            }else{
                $sqlUpdate = 'UPDATE processzip SET inqueue ="N", status = "pending", queuedcount = queuedcount + 1 WHERE id = '.$processzip_id;               
                $db7->query($sqlUpdate);
                die('Process can not continue becasue this ['.$username_unzip.'] folder is already exist on this path. ' . USER_UPLOAD_EXTRACT_PATH);
            }
            
            
            $archive = new PclZip($copy_from_path);
            
            if (($v_result_list = $archive->extract(PCLZIP_OPT_PATH, $dir_usernamezip)) == 0) {
                $msg_ext = 'Zip Cound not extract.<br>Error : '.$archive->errorInfo(true);
                echo $msg_ext."\n";
                $this->_uploaderror .= $msg_ext;
            }
            
            //start
            //rename all file and folder and set space as underscore upto 3 directory levels
            //echo 'renaming all file and folders replacing spaces with underscores'."\n";

            $handle = opendir($dir_usernamezip);
            if (!$handle) die('the folder '.$dir_usernamezip.' does not exist');
            while ($file = readdir($handle)) {
                if ($file!="." && $file!="..") {
                    //echo $file;
                    $newname1 = str_replace(" ","_",$file);
                    if ($handle2 = @opendir($dir."/".$file)) {
                        while ($file2 = readdir($handle2)) {
                            if ($file2!="." && $file2!="..") {
                                //echo $file2;
                                $newname2 = str_replace(" ","_",$file2);
                                if ($handle3 = @opendir($dir."/".$file."/".$file2)){
                                    while ($file3 = readdir($handle3)) {
                                        if ($file3!="." && $file3!=".."){
                                            //echo $file3;
                                            $newname3 = str_replace(" ","_",$file3);
                                            @rename ($dir."/".$file."/".$file2."/".$file3, $dir."/".$file."/".$file2."/".$newname3);
                                        }
                                    }
                                }
                                @rename ($dir."/".$file."/".$file2,$dir."/".$file."/".$newname2);
                            }
                        }
                    }
                    //@rename ($dir."/".$file, $dir."/".$newname1);
                    rename ($dir_usernamezip."/".$file, $dir_usernamezip."/".$newname1);
                }
            }
            closedir($handle);
            
            $count=0;
            //print_r($v_result_list);
            //echo "\n\nv_result_list:\n\n".print_r($v_result_list,true)."\n\n";

            if (count($v_result_list)){
                    
                $firsttimefolder = $z = 0;
                
                foreach ($v_result_list as $k=>&$v) {
                    
//                    echo "</br><pre>";
//                    print_r($v_result_list);
//                    exit;
                    
                    $filen=explode('.',$v['filename']);
                    
                    //echo 'processing '.$v['filename']."...\n";
                    
                    if ($filen[1]!='') {
                        $this->_totalnooffiles=$this->_totalnooffiles+1;
                    }
                    //$z++;
                    $unzipfilename = str_replace(" ","_",$v['filename']);
                    
                    $explodefilename = explode(".",basename($unzipfilename));
                    $countexplode = count($explodefilename);
                    //check the extension
                    $fileextension = strtolower($explodefilename[$countexplode-1]);
                    
                    //if the file is a csv file then process it further

                    if ($fileextension=="xlsx" || $fileextension=="xls" || $fileextension=="csv") {
                        //continue;
                        //echo 'is xlsx, xls, or csv: '.$unzipfilename."\n";
                        // $dName = $_SERVER["SERVER_NAME"];

                        //echo $fileurl = SERVER_NAME.$unzipfilename;
                        //$fileurl = "./".$unzipfilename;
                        $fileurl = $unzipfilename;

                        //$fileurl = "http://122.160.44.162:90/v1global/".$unzipfilename;
                        $filesize=filesize($unzipfilename);
                        //file should be upto 1 MB - 1048576
                        /*
                        if ($filesize > 1048576) {
                            //$filenameerror=str_replace('upload/resume/','',$v['filename']);
                            $filenameerror=str_replace(USER_UPLOAD_EXTRACT_PATH,'',$v['filename']);
                            //copy($fileurl,"backup/" .$filenameerror );
                            copy($fileurl,RESUME_BACKUP_PATH .$filenameerror );
                            $this->_reportmessage.="<tr><td style='padding:0px 5px 0px 5px'>".$this->_username."</td><td style='padding:0px 5px 0px 5px'>".$filenameerror."<a href=\"http://www.resuflo.com/backup/".$filenameerror."\">Download</a></td><td style='padding:0px 5px 0px 5px'></td><td style='padding:0px 5px 0px 5px'>File Size is Big</td></tr>";
                            //$this->_curruptrecordsexcel=$this->_curruptrecordsexcel+1;
                            //continue;
                        }
                        */
                        $resultprocesszip=$this->InsertIntoDBexcelData($fileurl,$userid,$db,$v['filename'],$emailalert,$fileextension,$camp_id,$siteId);
                    }else {
                        //echo 'is NOT xlsx, xls, or csv: '.$unzipfilename."\n";
                    }


                    //if the file is a csv file then process it further
                    //if the file is a word document then process it further
                    //
                    //if ($fileextension=="rtf" || $fileextension=="doc" || $fileextension=="docx" || $fileextension=="txt" || $fileextension == "html" ||$fileextension == "htm" || $fileextension == "pdf" || $fileextension== "odt") {
                    if ($fileextension=="doc" || $fileextension=="docx") {
                        //echo $unzipfilename;
                        //$dName = $_SERVER["SERVER_NAME"];
                        //$fileurl = SERVER_NAME.$unzipfilename;

                        //echo 'is rtf/doc/docx/txt/html/htm/pdf/odt: '.$unzipfilename."\n";

                        //$fileurl = "./".$unzipfilename;
                        $fileurl = $unzipfilename;
                        
                        //$fileurl = "http://122.160.44.162:90/v1global/".$unzipfilename;
                        $filesize=filesize($unzipfilename);

                        //file should be upto 1 MB - 1048576
                        if ($filesize > 1048576) {
                            continue;
                        }

                        try {
                            $docObj = new DocxConversion($fileurl);
                            $xmlArray['DetailResume'] = $docObj->convertToText();
                            $xmlArray['Email'] = $docObj->extract_emails_from($xmlArray['DetailResume']);
                            $xmlArray['FirstName'] = explode("_",$explodefilename[0])[1];
                            $xmlArray['LastName'] = explode("_",$explodefilename[0])[0];
                            $xmlArray['Email2']="";
                            $xmlArray['Middlename']="";
                            $xmlArray['Phone']="";
                            $xmlArray['Mobile']="";
                            $xmlArray['Address']="";
                            $xmlArray['City']="";
                            $xmlArray['State']="";
                            $xmlArray['ZipCode']="";
                            $xmlArray['CurrentSalary']="";
                            $xmlArray['Phonework']=""; 
                            $xmlArray['ExpectedSalary']="";
                            $xmlArray['Skills']="";
                            $xmlArray['CurrentEmployer']="";
                            $xmlArray['besttimetocall']="";
                            $xmlArray['languageknown']="";

                        } catch(Exception $e) {
                            echo 'xml parse exception: ' .$e->getMessage();
                        }

                        $xmlArray['errormsg']='';
                        if ($parsedxml=='Caught exception') {
                            $filenameerror=basename($unzipfilename);
                            $this->_reportmessage.="<tr><td style='padding:0px 5px 0px 5px'>".$this->_username."</td><td style='padding:0px 5px 0px 5px'>".$filenameerror."</td><td style='padding:0px 5px 0px 5px'>Connection time out it may be big file</td></tr>";
                            $this->_curruptfiles=$this->_curruptfiles+1;
                        } elseif (trim($xmlArray['FirstName'])=='' || $xmlArray['Email']=='') {
                            
                            //$filenameerror=str_replace('upload/resume/'.$username_unzip.'/','',$unzipfilename);
                            $filenameerror=str_replace(USER_UPLOAD_EXTRACT_PATH.$username_unzip.'/','',$unzipfilename);

                            if ($xmlArray['errormsg']=='') {

                                $this->_reportmessage.="<tr><td style='padding:0px 5px 0px 5px'>".$this->_username."</td><td style='padding:0px 5px 0px 5px'>".$filenameerror."</td><td style='padding:0px 5px 0px 5px'>Email id is blank</td></tr>";
                                $this->_curruptfiles=$this->_curruptfiles+1;
                            } else {
                                $this->_reportmessage.="<tr><td style='padding:0px 5px 0px 5px'>".$this->_username."</td><td style='padding:0px 5px 0px 5px'>".$filenameerror."</td><td style='padding:0px 5px 0px 5px'>".trim($xmlArray['errormsg'])."</td></tr>";
                                $this->_curruptfiles=$this->_curruptfiles+1;
                            }
                        }

                        if ($xmlArray['error_message']!='') {
                            //$filenameerror=str_replace('upload/resume/'.$username_unzip.'/','',$unzipfilename);
                            $filenameerror=str_replace(USER_UPLOAD_EXTRACT_PATH.$username_unzip.'/','',$unzipfilename);
                            $this->_reportmessage.="<tr><td style='padding:0px 5px 0px 5px'>".$this->_username."</td><td style='padding:0px 5px 0px 5px'>".$filenameerror."</td><td style='padding:0px 5px 0px 5px'>".$xmlArray['error_message']."</td></tr>";
                            $this->_curruptfiles=$this->_curruptfiles+1;
                        }

                        if ($xmlArray['FirstName']!='' && $xmlArray['Email']!='') {
                            $count++;
//                            die();
                            //echo "calling InsertIntoDBZipData on {$xmlArray['FirstName']}\n";
                            $resultprocesszip=$this->InsertIntoDBZipData($xmlArray,$userid,$db,$unzipfilename,$emailalert,$camp_id,$siteId);
                        }
                        //echo $count."last";
                        //echo "<pre>";
                        //print_r($xmlArray);
                        //if firstname and email exist then insert the details in DB
                    } elseif ($fileextension!='' && $fileextension!='csv' && $fileextension!='xls') {
                        continue;
                        if ($firsttimefolder==1) {
                            //$filenameerror=str_replace('upload/resume/'.$username_unzip.'/','',$unzipfilename);
                            $filenameerror=str_replace(USER_UPLOAD_EXTRACT_PATH.$username_unzip.'/','',$unzipfilename);
                            $this->_reportmessage.="<tr><td style='padding:0px 5px 0px 5px'>".$this->_username."</td><td style='padding:0px 5px 0px 5px'>".$filenameerror."</td><td style='padding:0px 5px 0px 5px'>Incorrect format of file</td></tr>";
                            $this->_curruptfiles=$this->_curruptfiles+1;
                        }
                        $firsttimefolder=1;
                    }
                }
            }

            //echo "removing $dir_usernamezip...\n";
            //shell_exec("RMDIR /Q /S ".str_replace("/", "\\", $dir_usernamezip));
        }

        if ($resultprocesszip==1) {

                if ($this->_reportmessage!='') {

                    $resulttotalfile=$this->_totalnooffiles;
                    if ($this->_filetype=='csv'||$this->_filetype=='xls'||$this->_filetype=='xlsx') {
                        $totalnoofrec=$this->_totalnoofrecordsexcel-1;
                        $reportmessage="<br/><b>Total Number of  Records : ".$totalnoofrec."</b><br/><b>Total Number of incorrect records : ".$this->_curruptrecordsexcel."</b><br/><table border='2' style='padding:2px'><tr><td style='padding:0px 5px 0px 5px'>Client Name</td><td style='padding:0px 5px 0px 5px'>Filename</td><td style='padding:0px 5px 0px 5px'>Record no</td><td style='padding:0px 5px 0px 5px'>Reason</td></tr>";
                    } else {
                    $reportmessage="<br/><b>Total Number of  files :".$resulttotalfile."</b><br/><b>Total Number of incorrect files : ".$this->_curruptfiles."</b><br/><table border='2' style='padding:2px'><tr><td style='padding:0px 5px 0px 5px'>Client Name</td><td style='padding:0px 5px 0px 5px'>Filename</td><td style='padding:0px 5px 0px 5px'>Reason</td></tr>";
                    }
                    $reportmessage="reportmsg".$reportmessage.$this->_reportmessage."</table>";
                    $this->_curruptfiles=$this->_curruptfiles+1;
                } else {
                    $reportmessage="reportmsg"."All candidates have been inserted for Client - <strong>" .$this->_username."</strong>";
                }
            $message = "Resumes have been inserted from uploaded zip."." ".$reportmessage;
        } else {
            
            if ($this->_reportmessage!='') {
                
                if ($this->_filetype=='csv'||$this->_filetype=='xls'||$this->_filetype=='xlsx') {
                     $this->_totalnoofrecordsexcel=$this->_totalnoofrecordsexcel-1;
                     //$reportmessage="<br/><b>Total Number of  Records : ".$this->_totalnoofrecordsexcel."</b><br/><b>Total Number of incorrect records : ".$this->_curruptrecordsexcel."</b><br/><table border='2' style='padding:2px'><tr><td style='padding:0px 5px 0px 5px'>Client Name</td><td style='padding:0px 5px 0px 5px'>Filename</td><td style='padding:0px 5px 0px 5px'>Record no</td><td style='padding:0px 5px 0px 5px'>Reason</td></tr>";
                     $reportmessage="<br/><b>Total Number of  Records : ".$this->_curruptrecordsexcel."</b><br/><b>Total Number of incorrect records : ".$this->_curruptrecordsexcel."</b><br/><table border='2' style='padding:2px'><tr><td style='padding:0px 5px 0px 5px'>Client Name</td><td style='padding:0px 5px 0px 5px'>Filename</td><td style='padding:0px 5px 0px 5px'>Record no</td><td style='padding:0px 5px 0px 5px'>Reason</td></tr>";
                 } else {
                     $resulttotalfile=$this->_totalnooffiles;
                     $reportmessage="<br/><b>Total Number of  files : ".$resulttotalfile."</b><br/><b>Total Number of incorrect files : ".$this->_curruptfiles."</b><br/><table border='2' style='padding:2px'><tr><td style='padding:0px 5px 0px 5px'>Client Name</td><td style='padding:0px 5px 0px 5px'>Filename</td><td style='padding:0px 5px 0px 5px'>Reason</td></tr>";
                 }
                 $reportmessage="reportmsg".$reportmessage.$this->_reportmessage."</table>";
                 $this->_curruptfiles=$this->_curruptfiles+1;
             } else {
                 $reportmessage="reportmsg";
             }

            //$message = "No valid resume found in zip."." ".$reportmessage;
            $message = 0;
        }
        /*echo "<script>location.href='index.php?m=candidates'</script>";    */
        return $message;
    }

    function InsertIntoDB($xmlArr) {

        foreach ($xmlArr as $key1 => $value1) {
            $xmlArr[$key1]=preg_replace('#[^\w()/.%\-&@]#'," ",$value1);
            $xmlArr[$key1]= str_replace('/','-',$xmlArr[$key1]);
        }
        $siteid = 1;
        $candidates = new Candidates($siteid);
        $firstName = $xmlArr['FirstName'];
        $middleName =$xmlArr['Middlename'];
        $lastName = $xmlArr['LastName'];
        $email1 =$xmlArr['Email'];
        $phoneCell =$xmlArr['Phone'];
        $phoneHome = $xmlArr['Mobile'];
        $address = $xmlArr['Address'];
        $city =$xmlArr['City'];
        $state =$xmlArr['State'];
        $zip =$xmlArr['ZipCode'];
        $currentPay = $xmlArr['CurrentSalary'];
        $desiredPay = $xmlArr['ExpectedSalary'];
        $keySkills = $xmlArr['Skills'];
        $currentEmployer = $xmlArr['CurrentEmployer'];
        $enteredBy = $_SESSION['resumeinsertuserid'];
        $owner = $_SESSION['resumeinsertuserid'];
        $camp_id = $_REQUEST['camp_id'];

        // import opt-out trumps all other things!
        if (Importoptout::is_optout_email($email1) || Importoptout::is_optout_email($email2)) {
            return 0;
        }    

        $candidateId = $candidates->add($firstName, $middleName, $lastName, $email1, $email2='',
        $phoneHome, $phoneCell, $phoneWork='', $address, $city, $state, $zip,
        $source='', $keySkills, $dateAvailable='', $currentEmployer, $canRelocate='',
        $currentPay, $desiredPay, $notes='', $webSite='', $bestTimeToCall='', $enteredBy, $owner,
        $gender = '', $race = '', $veteran = '', $disability = '',$lifeLic='',$propLic='',$ser6='',$ser63='',$ser7='',$camp_id,
        $skipHistory = false);
        if (!empty($candidateId)) {
            $candidates->InsertExtraFields($xmlArr,$candidateId);
        }
    }

    //function for excel file
    function InsertIntoDBexcelData($fileurl,$userid,$db,$filenameexcel,$emailalert,$fileextension,$camp_id,$siteid) {

        $namefile=$filenameexcel;
        $fileName=$fileurl;
        $this->_filetype= $fileextension;

        // what?

        $dataarray=array();
        $match=0;

        try {
            $fp = fopen($fileName,'r');
            if (!$fp) {
                die('could not open '.$fileName);
            }
        } catch(exception $e) {
            $match=2;
        }

        if ($match!=2) {
            while ($csv_line = fgetcsv($fp,1024)) {
                for ($i = 0, $j = count($csv_line); $i < $j; $i++) {
                    $dataarray[]= $csv_line[$i];
                }
                break;
            }
            fclose($fp);

            $dataarray=array_flip($dataarray);
            //echo "<pre>";
            // TODO: fix this where print_r is dumping incorrectly, 2nd param should be true
            //print_r($dataarray);
            if (array_key_exists('last name',$dataarray) && array_key_exists('first name',$dataarray)) {
                $match=1;
            }
        } else {
            //echo "Not Matched".$filenameexcel;
            //$recordno=$i;
            //$this->_recordno=$recordno;
            //$i++;
            $filenameerror=str_replace(USER_UPLOAD_PATH.$username_unzip.'/','',$filenameexcel);
            copy($fileurl,RESUME_BACKUP_PATH .$filenameerror );
            $this->_reportmessage.="<tr><td style='padding:0px 5px 0px 5px'>".$this->_username."</td><td style='padding:0px 5px 0px 5px'>".$filenameexcel."</td><td style='padding:0px 5px 0px 5px'></td><td style='padding:0px 5px 0px 5px'>Can't Open the File as File CSV Data Coloumn Format Not Supported</td></tr>";
            //$this->_curruptrecordsexcel=$this->_curruptrecordsexcel+1;
            //$this->_totalnoofrecordsexcel=$i;
            // TODO: nothing should EVER be 777 or 666!
            chmod($fileName,777);
            unlink($fileName);
            $count=1;
            return $count;
        }

        if ($match==1) {
            $lastnamecountno="";
            $firstnamecountno="";

            //start of column no get

            $flds = explode(',', $this->flds1);

            // this saves about 100s of lines of code
            foreach ($flds as &$fld) {
                if (isset($dataarray[$fld])) {
                    ${strtolower(str_replace(' ','',$fld)).'countno'}=$dataarray[$fld];
                }
            }

            //end of column get

            $xmlArr=array();
            $count=0;

            $fp = fopen($fileName,'r') or die("can't open file");
            $i=1;
            $firsttime=0;

            $recordno = 0;
            $this->_totalnoofrecordsexcel = 0;
            while ($cells = fgetcsv($fp,1024)) {
                //echo "<pre>";print_r($cells);
                if ($firsttime==0) {
                    $firsttime=1;
                    continue;
                }

                $flds2 = explode(',', $this->flds2);

                for ($i=0;$i<count($flds);$i++) {
                    $f1 = strtolower(str_replace(' ', '', $flds[$i]));
                    $f2 = $flds2[$i];

                    $v = '';
                    if (isset($cells[$lastnamecountno])) {
                        $f = ${$f1.'countno'};
                        $v = $cells[$f];
                        $v = preg_replace('#[^\w()/.%\-&@]#', ' ', $v);
                        $v = str_replace('/', '-', $v);
                    }
                    $xmlArr[$f2]=$v;
                }


                //echo "<pre>";print_r($xmlArr);
                //extra fields

                //add validations   problem in xmlarr name
                //$recordno=$i;
                $recordno = $recordno + 1;
                $this->_recordno=$recordno;
                $i++;

                if (trim($xmlArr['Email'])=='') {

                    $filenameerror=str_replace(USER_UPLOAD_PATH.$username_unzip.'/','',$filenameexcel);

                    $this->_reportmessage.="<tr><td style='padding:0px 5px 0px 5px'>".$this->_username."</td><td style='padding:0px 5px 0px 5px'>".$filenameexcel."</td><td style='padding:0px 5px 0px 5px'>".$recordno."</td><td style='padding:0px 5px 0px 5px'>Email id is blank</td></tr>";
                    $this->_curruptrecordsexcel=$this->_curruptrecordsexcel+1;
                }

                //add validations

                //if (trim($xmlArr['FirstName'])!='' && trim($xmlArr['Email'])!='') {
                if (trim($xmlArr['Email'])!='') {

                    $resultprocesszip=$this->InsertIntoDBZipDataexcel($xmlArr,$userid,$db,$filenameexcel,$emailalert,$camp_id,$siteid);
                    if ($resultprocesszip>0) {
                        $count=1;
                    }
                }
                $this->_totalnoofrecordsexcel = $this->_totalnoofrecordsexcel+1;
            }

            fclose($fp) or die("can't close file");


            //$this->_totalnoofrecordsexcel=$i;
            chmod($fileName,777);
            unlink($fileName);
            return $count;

        } else {
            //echo "Not Matched";
            //$recordno=$i;
            //$this->_recordno=$recordno;
            //$i++;
            $filenameerror=str_replace(USER_UPLOAD_PATH.$username_unzip.'/','',$filenameexcel);
            copy($fileurl,RESUME_BACKUP_PATH .$filenameerror );
            $this->_reportmessage.="<tr><td style='padding:0px 5px 0px 5px'>".$this->_username."</td><td style='padding:0px 5px 0px 5px'>".$filenameexcel."</td><td style='padding:0px 5px 0px 5px'></td><td style='padding:0px 5px 0px 5px'>Can't Open the File as File CSV Data Coloumn Format Not Supported</td></tr>";
            //$this->_curruptrecordsexcel=$this->_curruptrecordsexcel+1;
            //$this->_totalnoofrecordsexcel=$i;
            // TODO: NO 777!
            chmod($fileName,777);
            unlink($fileName);
            $count=1;
            return $count;
        }
    }

    function createdetailresume($xmlArr) {

        // these are set as class properties (this list is just for reference!)
        // LastName,FirstName,Middlename,Phone,Mobile,Phonework,Address,City,State,ZipCode,Skills,Email,Email2,Category,DateOfBirth,Gender,MotherName,FatherName,MaritalStatus,PassportNo,Nationality,WorkedPeriod,GapPeriod,FaxNo,LicenseNo,Hobbies,Qualification,Achievments,Objectives,Experience,References,JobProfile,ExpectedSalary,CurrentSalary,besttimetocall,CurrentEmployer,languageknown,DetailResume
        $flds2 = explode(',', $this->flds2);

        $htmlresume=$htmlresume.'<table>';

        for ($i=0; $i<count($flds2); $i++) {
            $fld2 = $flds2[$i];

            if ($xmlArr[$fld2]!='') {
                switch ($f) {
                  case 'FirstName': $f = 'First Name'; break;
                  case 'LastName': $f = 'Last Name'; break;
                  case 'Mobile': $f = 'Mobile No'; break;
                  case 'Phonework': $f = 'Work Phone'; break;
                  case 'besttimetocall': $f = 'Besttimetocall'; break;
                  default: $f = $fld2;
                }

                // for some reason languageknown has been hidden, so, handle that here
                // but make it easy to hide others if needed...
                $donotshow = explode(',','languageknown');

                if (! in_array($fld2, $donotshow)) {
                    $htmlresume = $htmlresume.'<tr><td valign="top"><b>'.$f.':</b></td><td>'.$xmlArr['FirstName'].'</td></tr>';
                }
            }
        }

        $htmlresume=$htmlresume.'</table>';
        return $htmlresume;
    }
    
    function InsertIntoDBDocData($xmlArr,$userid,$db,$filenamedoc,$emailalert,$camp_id) {

        $siteid = 1;

        $firstName = $xmlArr['FirstName'];
        $email2 = $xmlArr['Email2'];
        $middleName =$xmlArr['Middlename'];
        $lastName = $xmlArr['LastName'];
        $email1 =$xmlArr['Email'];
        $phoneCell =$xmlArr['Phone'];
        $phoneHome = $xmlArr['Mobile'];
        $address = $xmlArr['Address'];
        $city =$xmlArr['City'];
        $state =$xmlArr['State'];
        $zip =$xmlArr['ZipCode'];
        $currentPay = $xmlArr['CurrentSalary'];
        $phoneWork = $xmlArr['Phonework']; 
        $desiredPay = $xmlArr['ExpectedSalary'];
        $keySkills = $xmlArr['Skills'];
        $currentEmployer = $xmlArr['CurrentEmployer'];
        $bestTimeToCall = $xmlArr['besttimetocall'];
        $langknown=$xmlArr['languageknown'];
        $enteredBy = $userid;
        $owner = $userid;

        // import opt-out trumps all other things!
        if (Importoptout::is_optout_email($email1) || Importoptout::is_optout_email($email2)) {
            return 0;
        }    

        $candidateId = $this->addfromdoc($db,$siteid,$firstName, $middleName, $lastName, $email1, $email2,
                                  $phoneHome, $phoneCell, $phoneWork, $address, $city, $state, $zip,
                                  $source='', $keySkills, $dateAvailable='', $currentEmployer, $canRelocate='',
                                  $currentPay, $desiredPay, $notes='', $webSite='', $bestTimeToCall, $enteredBy, $owner,
                                  $gender = '', $race = '', $veteran = '', $disability = '',
                                  $skipHistory = false,$filenamedoc,$emailalert,$camp_id);

        //echo "new candidateid: $candidateId\n";

        $candidateIdarr=explode("candadd",$candidateId);

        $candidateId=$candidateIdarr[0];

        if ($candidateId!=-1) {
            $this->InsertExtraFields($xmlArr,$candidateId,$db);
        }
        return $candidateIdarr[1];
    }


    function InsertIntoDBZipData($xmlArr,$userid,$db,$filenamedoc,$emailalert,$camp_id,$siteId='') {

        $firstName = $xmlArr['FirstName'];
        $email2 = $xmlArr['Email2'];
        $middleName =$xmlArr['Middlename'];
        $lastName = $xmlArr['LastName'];
        $email1 =$xmlArr['Email'];
        $phoneCell =$xmlArr['Phone'];
        $phoneHome = $xmlArr['Mobile'];
        $address = $xmlArr['Address'];
        $city =$xmlArr['City'];
        $state =$xmlArr['State'];
        $zip =$xmlArr['ZipCode'];
        $currentPay = $xmlArr['CurrentSalary'];
        $phoneWork = $xmlArr['Phonework']; 
        $desiredPay = $xmlArr['ExpectedSalary'];
        $keySkills = $xmlArr['Skills'];
        $currentEmployer = $xmlArr['CurrentEmployer'];
        $bestTimeToCall = $xmlArr['besttimetocall'];
        $langknown=$xmlArr['languageknown'];
        $enteredBy = $userid;
        $owner = $userid;

        // import opt-out trumps all other things!
        if (Importoptout::is_optout_email($email1) || Importoptout::is_optout_email($email2)) {
            return 0;
        }    

        $candidateId = $this->add($db,$siteId,$firstName, $middleName, $lastName, $email1, $email2,
                                  $phoneHome, $phoneCell, $phoneWork, $address, $city, $state, $zip,
                                  $source='', $keySkills, $dateAvailable='0000-00-00 00:00:00', $currentEmployer, $canRelocate='',
                                  $currentPay, $desiredPay, $notes='', $webSite='', $bestTimeToCall, $enteredBy, $owner,
                                  $gender = '', $race = 0, $veteran = 0, $disability = '',
                                  $skipHistory = false,$filenamedoc,$emailalert,$camp_id);

        //echo "new candidateid: $candidateId\n";

        $candidateIdarr=explode("candadd",$candidateId);

        $candidateId=$candidateIdarr[0];

        if ($candidateId!=-1) {
            $this->InsertExtraFields($xmlArr,$candidateId,$db);
        }
        return $candidateIdarr[1];
    }

    // insert function for excel file

    function InsertIntoDBZipDataexcel($xmlArr,$userid,$db,$filenamedoc,$emailalert,$camp_id,$siteid) {

        // note that within this class, the xmlArr has already had it's values cleaned
        // TODO find out if this is called anywhere else, if not, recommend to remove!
        /*foreach ($xmlArr as $key1 => $value1) {
            $xmlArr[$key1]=preg_replace('#[^\w()/.%\-&@]#'," ",$value1);
            $xmlArr[$key1]= str_replace('/','-',$xmlArr[$key1]);
        }*/

        $firstName = $xmlArr['FirstName'];
        $email2 = $xmlArr['Email2'];
        $middleName =$xmlArr['Middlename'];
        $lastName = $xmlArr['LastName'];
        $email1 =$xmlArr['Email'];
        $phoneCell =$xmlArr['Phone'];
        $phoneHome = $xmlArr['Mobile'];
        $address = $xmlArr['Address'];
        $city =$xmlArr['City'];
        $state =$xmlArr['State'];
        $zip =$xmlArr['ZipCode'];
        $currentPay = $xmlArr['CurrentSalary'];
        $phoneWork = $xmlArr['Phonework'];
        $desiredPay = $xmlArr['ExpectedSalary'];
        $keySkills = $xmlArr['Skills'];
        $currentEmployer = $xmlArr['CurrentEmployer'];
        $bestTimeToCall = $xmlArr['besttimetocall'];
        $langknown=$xmlArr['languageknown'];
        $enteredBy = $userid;
        $owner = $userid;

        // import opt-out trumps all other things!
        if (Importoptout::is_optout_email($email1) || Importoptout::is_optout_email($email2)) {
            return 0;
        }    

        $candidateId = $this->addfromexcel($db,$siteid,$firstName, $middleName, $lastName, $email1, $email2,
                                           $phoneHome, $phoneCell, $phoneWork, $address, $city, $state, $zip,
                                           $source='', $keySkills, $dateAvailable='0000-00-00 00:00:00', $currentEmployer, $canRelocate='',
                                           $currentPay, $desiredPay, $notes='', $webSite='', $bestTimeToCall, $enteredBy, $owner,
                                           $gender = '', $race = 0, $veteran = 0, $disability = '',
                                           $skipHistory = false,$filenamedoc,$emailalert,$camp_id);

        $candidateIdarr=explode("candadd",$candidateId);

        $candidateId=$candidateIdarr[0];

        if (!empty($candidateId)) {
            $this->InsertExtraFields($xmlArr,$candidateId,$db);
        }
        return $candidateIdarr[1];
    }

    //add function for excel file
    function addfromexcel($db,$siteid,$firstName, $middleName, $lastName, $email1, $email2,
                          $phoneHome, $phoneCell, $phoneWork, $address, $city, $state, $zip,
                          $source, $keySkills, $dateAvailable, $currentEmployer, $canRelocate,
                          $currentPay, $desiredPay, $notes, $webSite, $bestTimeToCall, $enteredBy, $owner,
                          $gender = '', $race = '', $veteran = '', $disability = '',
                          $skipHistory = false,$filenamedoc,$emailalert,$camp_id) {

        $sql ="SELECT count(*) as countno ,email1,user_name FROM `candidate` INNER JOIN user on user.user_id=candidate.entered_by WHERE  (candidate.email1='$email1' OR candidate.email2='$email1')   and candidate.entered_by='$enteredBy' group by candidate.candidate_id";

        $db3 = DatabaseConnection::getInstance();
        $resourcedetail = $db3->query($sql);
        $resource= mysql_fetch_assoc($resourcedetail);

        if ($resource['countno']>0) {

            $filenameerror=str_replace(USER_UPLOAD_PATH.$username_unzip.'/','',$filenamedoc);
            $this->_reportmessage.="<tr><td style='padding:0px 5px 0px 5px'>".$this->_username."</td><td style='padding:0px 5px 0px 5px'>".$filenameerror."</td><td style='padding:0px 5px 0px 5px'>".$this->_recordno."</td><td style='padding:0px 5px 0px 5px'> ".$resource['email1']." already exist in  ".$resource['user_name']." company</td></tr>";

            $this->_curruptrecordsexcel=$this->_curruptrecordsexcel+1;
        } else {

            $db2 = DatabaseConnection::getInstance();
            $checkstate="";

            // the user id vs state things is out...too much to manage, it must be done elsewhere!
            $allow = 1;

            if ($allow) {
                $checkstate="allow";
            } else {

                $filenameerror=str_replace(USER_UPLOAD_PATH.$username_unzip.'/','',$filenamedoc);
                $this->_reportmessage.="<tr><td style='padding:0px 5px 0px 5px'>".$this->_username."</td><td style='padding:0px 5px 0px 5px'>".$filenameerror."</td><td style='padding:0px 5px 0px 5px'>".$this->_recordno."</td><td style='padding:0px 5px 0px 5px'> it is not containing state Ohio/Michigan</td></tr>";

                $this->_curruptrecordsexcel=$this->_curruptrecordsexcel+1;
                $checkstate="notallow";
            }

            // import opt-out trumps all other things!
            if (Importoptout::is_optout_email($email1) || Importoptout::is_optout_email($email2)) {
                return 0;
            }    

            if ($checkstate=="allow" || $checkstate=="") {

                $sql = "INSERT INTO candidate(first_name,middle_name,last_name,email1,email2,phone_home,phone_cell,phone_work,address,city,state,zip,source,key_skills,date_available,current_employer,can_relocate,current_pay,desired_pay,notes,web_site,best_time_to_call,entered_by,owner,site_id,date_created,date_modified,eeo_ethnic_type_id,eeo_veteran_type_id,eeo_disability_status,eeo_gender,campaign_id,campaign_date)VALUES ('$firstName','$middleName','$lastName','$email1','$email2',ExtractNumber('$phoneHome'),ExtractNumber('$phoneCell'),ExtractNumber('$phoneWork'),'$address','$city','$state','$zip','$source','$keySkills','$dateAvailable','$currentEmployer',0,'$currentPay','$desiredPay','$notes','$webSite','$bestTimeToCall','$enteredBy','$owner','$siteid',now(),now(),'$race','$veteran','$disability','$gender','$camp_id',now())";
                $queryResult = $db2->query($sql);
                //candidate inserted
                if($queryResult){
                    //Get last inserted id
                    $lcid = mysql_insert_id();
                    //if default campaign is selected
                    if($camp_id == 0){
                        $defaultCampaignResult = mysql_query('select * from dripmarket_compaign where userid = '.$enteredBy.' and `default` = 1');
                        $dcamp = mysql_fetch_array($defaultCampaignResult);
                        $camp_id = $dcamp['id'];
                    }
                    
                    // let's send out the zero day email now
                    $sql ="select candidate.email1, candidate.first_name, candidate.phone_home, candidate.date_created,dripmarket_compaign.userid,dripmarket_compaign.cname,dripmarket_configuration.smtphost,dripmarket_configuration.email_type,dripmarket_configuration.smtppassword ,user.first_name as clientname,dripmarket_configuration.smtpport ,dripmarket_configuration.fromname,dripmarket_questionaire.id as quesid ,dripmarket_configuration.fromemail,dripmarket_configuration.bccemail,dripmarket_configuration.smtpuser,candidate.candidate_id,dripmarket_compaignemail.id dce_id, dripmarket_compaignemail.subject,dripmarket_compaignemail.creationdate,dripmarket_compaignemail.hour,dripmarket_compaignemail.min,dripmarket_compaignemail.Message,dripmarket_compaignemail.Timezone,dripmarket_compaignemail.sendemailon,dripmarket_compaign.id ,dripmarket_compaign.userid FROM `candidate` inner join user on user.user_id =candidate.entered_by INNER JOIN dripmarket_compaign ON user.user_id = dripmarket_compaign.userid inner JOIN dripmarket_compaignemail ON dripmarket_compaign.id = dripmarket_compaignemail.compaignid left JOIN dripmarket_questionaire ON dripmarket_questionaire.userid = dripmarket_compaign.userid inner join dripmarket_configuration on dripmarket_compaign.userid=dripmarket_configuration.userid where candidate_id = $lcid and dripmarket_compaign.id = $camp_id and candidate.ishot=0 and candidate.unsubscribe=0 and dripmarket_compaignemail.sendemailon=0";
                    $dripParamDetail = mysql_query($sql);
                    while ($row = mysql_fetch_array($dripParamDetail)) {
                        $dcee_id = $row['dce_id'];
                        $user_id = $row['userid'];
                        
                        $dripQuery = 'insert into drip_queue set smtp_error="parseresume", scheduledDate = Now(), candidate_id = "'.$lcid.'", drip_campaign_id = "'.$camp_id.'", drip_campemail_id = "'.$dcee_id.'", entered_by = "'.$user_id.'", date_created = now(), date_modified = now()';
                        $date = new DateTime();
                        $dripLogPath = 'C:\log\\CSVDripQueueLog '.$date->format('d m Y').'.txt';
                        file_put_contents($dripLogPath, $date->format('d m Y H:i:s').' '.$dripQuery.PHP_EOL, FILE_APPEND);
                        mysql_query($dripQuery);
                    }
                }
                
                if (!$queryResult) { 
                    error_log('QUERY ERROR: '.mysql_error($db2->_connection).' on query: '.$sql);
                }
                //        adding the php mailer end
                //email set of after parsing is done
                $this->_countresumefile=1;

        } else {

            $filenamedocext = explode(".",$filenamedoc);
            $countfilendocext=count($filenamedocext);
            $filext=$filenamedocext[$countfilendocext-1];
            $filenamedocnew='./backup/incorrectresume/'.time().".".$filext;
            if (file_exists($filenamedoc)) {
                //echo "exists";
            } else {
                //echo "not exists";
            }

            if (!copy($filenamedoc,$filenamedocnew)) {
                echo basename(__FILE__).' '.__LINE__." failed to copy $file...\n";
            }

            // import opt-out trumps all other things!
            if (Importoptout::is_optout_email($email1) || Importoptout::is_optout_email($email2)) {
                return 0;
            }    

            $sql = "INSERT INTO candidatenew(first_name,middle_name,last_name,email1,email2,phone_home,phone_cell,phone_work,address,city,state,zip,source,key_skills,date_available,current_employer,can_relocate,current_pay,desired_pay,notes,web_site,best_time_to_call,entered_by,owner,site_id,date_created,date_modified,eeo_ethnic_type_id,eeo_veteran_type_id,eeo_disability_status,eeo_gender)
                    VALUES ('$firstName','$middleName','$lastName','$email1','$email2',ExtractNumber('$phoneHome'),ExtractNumber('$phoneCell'),ExtractNumber('$phoneWork'),'$address','$city','$state','$zip','$source','$keySkills','$dateAvailable','$currentEmployer',0,'$currentPay','$desiredPay','$notes','$webSite','$bestTimeToCall','$enteredBy','$owner','$siteid',now(),now(),'$race','$veteran','$disability','$gender')";
                    //echo $sql."<br>";
                    $queryResult = $db2->query($sql);
                if (!$queryResult) {
                    error_log('QUERY ERROR: '.mysql_error($db2->_connection).' on query: '.$sql);
                }
            }
    

                //    $db5 = DatabaseConnection::getInstance();
                    //$queryResult12 = $db5->query($sql123);
    
    
    
            //code parser
            
            
            if ($checkstate=="notallow")
            {
            
                return -1;
            }
            
            //code parser
            
            
             $sql ="select max(candidate.candidate_id) as ids from candidate ";
            $db6 = DatabaseConnection::getInstance();
            $resourcedetail6 = $db6->query($sql);
            
            $rowdetail = mysql_fetch_assoc($resourcedetail6);
        
        
        
        
             $candidateID = $rowdetail['ids'];
            
      
            return $candidateID."candadd".$this->_countresumefile;
        }
    }
    
    function addfromdoc($db,$siteid,$firstName, $middleName, $lastName, $email1, $email2,
                  $phoneHome, $phoneCell, $phoneWork, $address, $city, $state, $zip,
                  $source, $keySkills, $dateAvailable, $currentEmployer, $canRelocate,
                  $currentPay, $desiredPay, $notes, $webSite, $bestTimeToCall, $enteredBy, $owner,
                  $gender = '', $race = '', $veteran = '', $disability = '',
                  $skipHistory = false,$filenamedoc,$emailalert,$camp_id) {
        
        $db3 = DatabaseConnection::getInstance();

        $sql = 'select entered_by from candidate where "'.mysql_real_escape_string($email1).'" in (email1, email2)';
        if (!empty($email2)) $sql .= ' or "'.mysql_real_escape_string($email2).'" in (email1, email2)';

        $resourcedetail = $db3->query($sql);
        $entered_by_list = array();
        while ($row = mysql_fetch_array($resourcedetail)) {
            $entered_by_list[] = $row['entered_by'];
        }

        // the user table entered_by is indicative of a master or sub account
        // the $enteredBy that we are feeding to this is the target recipient of the resumes we're trying to parse
        // and the reason we do this is that j. edwards does not want the same company (master or subs) to get non-unique email/reusmes
        $sql = 'select entered_by from user where user_id = '.$enteredBy;
        $rows = $db3->getAllAssoc($sql);
        $row = $rows[0];

        // find the master account (we may already have it in the target $enteredBy)
        $master_id = ($row['entered_by']=='1') ? $enteredBy : $row['entered_by'];

        // get all sub accounts of that master account
        $sql = 'select user_id from user where '.$master_id.' in (entered_by, user_id)';
        $rows = $db3->getAllAssoc($sql);
        foreach ($rows as &$row) {
            // see if any of our email candidate.entered_by interesect with all company user.entered_by => user_id's
            $entered_by_user[] = $row['user_id']; 
        }

        $ai = array_intersect($entered_by_user, $entered_by_list);

        if (!empty($ai)) {
            
            $filenameerror=str_replace('upload/resume/'.$username_unzip.'/','',$filenamedoc);
            $this->_reportmessage.="<tr><td style='padding:0px 5px 0px 5px'>".$this->_username."</td><td style='padding:0px 5px 0px 5px'>".$filenameerror."</td><td style='padding:0px 5px 0px 5px'> $email1 or $email2 already exists in these user_ids: ".implode(', ',$ai)."</td></tr>";
            $this->_curruptfiles=$this->_curruptfiles+1;
            
        } else {

            $db2 = DatabaseConnection::getInstance();
            $checkstate="";

            // import opt-out trumps all other things!
            if (Importoptout::is_optout_email($email1) || Importoptout::is_optout_email($email2)) {
                return 0;
            }    
 
            if ($checkstate=="allow" || $checkstate=="")
            {
            
            //$queryError=0;     
            //try
            //{
            $sql = "INSERT INTO candidate(first_name,middle_name,last_name,email1,email2,phone_home,phone_cell,phone_work,address,city,state,zip,source,key_skills,date_available,current_employer,can_relocate,current_pay,desired_pay,notes,web_site,best_time_to_call,entered_by,owner,site_id,date_created,date_modified,eeo_ethnic_type_id,eeo_veteran_type_id,eeo_disability_status,eeo_gender,campaign_id,campaign_date)
                VALUES ('$firstName','$middleName','$lastName','$email1','$email2',ExtractNumber('$phoneHome'),ExtractNumber('$phoneCell'),ExtractNumber('$phoneWork'),'$address','$city','$state','$zip','$source','$keySkills','$dateAvailable','$currentEmployer',0,'$currentPay','$desiredPay','$notes','$webSite','$bestTimeToCall','$enteredBy','$owner','$siteid',now(),now(),'$race','$veteran','$disability','$gender','$camp_id',now())";

                $queryResult = $db2->query($sql);

                if (!$queryResult) {
                    error_log('QUERY ERROR: '.mysql_error($db2->_connection).' on query: '.$sql);
                }
                //email set of after parsing is done
                $this->_countresumefile=1;
        /* }
        else
        {
                    $this->_reportmessage.="<tr><td style='padding:0px 5px 0px 5px'>".$this->_username."</td><td style='padding:0px 5px 0px 5px'>".$filenameerror."</td><td style='padding:0px 5px 0px 5px'>Resume Data Corrupted</td></tr>";
                    $this->_curruptfiles=$this->_curruptfiles+1;
        } */
        
        }else
            {
                
                $filenamedocext = explode(".",$filenamedoc);
                $countfilendocext=count($filenamedocext);
                $filext=$filenamedocext[$countfilendocext-1];
                $filenamedocnew='./backup/incorrectresume/'.time().".".$filext;
                
                
                if (file_exists($filenamedoc))
                {
                    //echo "exists";
                    
                }
                else
                {
                    //echo "not exists";
                    
                    
                }
                $bodytext="Please collect incorrect resume<br/><br/>";
                $bodytext.="<b>Incorrect Resume Message:</b><table border='2' style='padding:2px'><tr><td style='padding:0px 5px 0px 5px'>Client Name</td><td style='padding:0px 5px 0px 5px'>Filename</td><td style='padding:0px 5px 0px 5px'>Reason</td><td style='padding:0px 5px 0px 5px'>State</td></tr>$incorrectresumemsg</table><br/><br/>$parsedxml<br/>";
                    
                try{
                    file_put_contents('C:/Windows/Temp/ResufloLog/UserMailerCaller.txt', PHP_EOL.(new DateTime())->format('d m Y H:i:s').'===== START resumeparser', FILE_APPEND);
                    $email = new UserMailer(1);
                    $result = $email->SendEmail(array(TechnicalReport), null, 'Incorrect Resume', $bodytext, $filenamedoc);
                    file_put_contents('C:/Windows/Temp/ResufloLog/UserMailerCaller.txt', '=> END ====='.$result, FILE_APPEND);
                }
                catch (Exception $e) {
                    file_put_contents('C:/Windows/Temp/ResufloLog/UserMailerCaller.txt', (new DateTime())->format('d m Y H:i:s').'resumeparser ====='.$e->getMessage().PHP_EOL, FILE_APPEND);
                }
                
                //send incorrect resume not containing state
                        
                if (!copy($filenamedoc,$filenamedocnew)) {
                    echo "failed to copy $file...\n";
                    
                }
                /* $queryError=0;
                try
                { */
                    // import opt-out trumps all other things!
                    if (Importoptout::is_optout_email($email1) || Importoptout::is_optout_email($email2)) {
                      return 0;
                    }
 
                    $sql = "INSERT INTO candidatenew(first_name,middle_name,last_name,email1,email2,phone_home,phone_cell,phone_work,address,city,state,zip,source,key_skills,date_available,current_employer,can_relocate,current_pay,desired_pay,notes,web_site,best_time_to_call,entered_by,owner,site_id,date_created,date_modified,eeo_ethnic_type_id,eeo_veteran_type_id,eeo_disability_status,eeo_gender)
                    VALUES ('$firstName','$middleName','$lastName','$email1','$email2',ExtractNumber('$phoneHome'),ExtractNumber('$phoneCell'),ExtractNumber('$phoneWork'),'$address','$city','$state','$zip','$source','$keySkills','$dateAvailable','$currentEmployer',0,'$currentPay','$desiredPay','$notes','$webSite','$bestTimeToCall','$enteredBy','$owner','$siteid',now(),now(),'$race','$veteran','$disability','$gender')";
                    //echo $sql."<br>";
                    $queryResult = $db2->query($sql);
                    /* if (!$queryResult)
                    {
                            throw new Exception("error"); 
                    }
                }
                catch(exception $e)
                {
                    $queryError=1;
                } */
            }
    
        
                //    $db5 = DatabaseConnection::getInstance();
                    //$queryResult12 = $db5->query($sql123);
    
    
    
            //code parser
            
            
            if ($checkstate=="notallow")
            {
            
                return -1;
            }
            
            //code parser
            
            
            $sql ="select candidate.candidate_id as ids from candidate where entered_by='$enteredBy' and email1='$email1' ";
            $db6 = DatabaseConnection::getInstance();
            $resourcedetail6 = $db6->query($sql);
            
            $rowdetail = mysql_fetch_assoc($resourcedetail6);
            $candidateID = $rowdetail['ids'];
            
      
            return $candidateID."candadd".$this->_countresumefile;
        }
    }
    
    function add($db,$siteid,$firstName, $middleName, $lastName, $email1, $email2,
                  $phoneHome, $phoneCell, $phoneWork, $address, $city, $state, $zip,
                  $source, $keySkills, $dateAvailable, $currentEmployer, $canRelocate,
                  $currentPay, $desiredPay, $notes, $webSite, $bestTimeToCall, $enteredBy, $owner,
                  $gender = '', $race = '', $veteran = '', $disability = '',
                  $skipHistory = false,$filenamedoc,$emailalert,$camp_id) {
        
        $db3 = DatabaseConnection::getInstance();

        $sql = 'select entered_by from candidate where "'.mysql_real_escape_string($email1).'" in (email1, email2)';
        if (!empty($email2)) $sql .= ' or "'.mysql_real_escape_string($email2).'" in (email1, email2)';

        $resourcedetail = $db3->query($sql);
        $entered_by_list = array();
        while ($row = mysql_fetch_array($resourcedetail)) {
            $entered_by_list[] = $row['entered_by'];
        }

        // the user table entered_by is indicative of a master or sub account
        // the $enteredBy that we are feeding to this is the target recipient of the resumes we're trying to parse
        // and the reason we do this is that j. edwards does not want the same company (master or subs) to get non-unique email/reusmes
        $sql = 'select entered_by from user where user_id = '.$enteredBy;
        $rows = $db3->getAllAssoc($sql);
        $row = $rows[0];

        // find the master account (we may already have it in the target $enteredBy)
        $master_id = ($row['entered_by']=='1') ? $enteredBy : $row['entered_by'];

        // get all sub accounts of that master account
        $sql = 'select user_id from user where '.$master_id.' in (entered_by, user_id)';
        $rows = $db3->getAllAssoc($sql);
        foreach ($rows as &$row) {
            // see if any of our email candidate.entered_by interesect with all company user.entered_by => user_id's
            $entered_by_user[] = $row['user_id']; 
        }

        $ai = array_intersect($entered_by_user, $entered_by_list);

        if (!empty($ai)) {
            
            //$filenameerror=str_replace('upload/resume/'.$username_unzip.'/','',$filenamedoc);
            $filenameerror=str_replace(USER_UPLOAD_PATH.$username_unzip.'/','',$filenamedoc);
            $this->_reportmessage.="<tr><td style='padding:0px 5px 0px 5px'>".$this->_username."</td><td style='padding:0px 5px 0px 5px'>".$filenameerror."</td><td style='padding:0px 5px 0px 5px'> $email1 or $email2 already exists in these user_ids: ".implode(', ',$ai)."</td></tr>";
            $this->_curruptfiles=$this->_curruptfiles+1;
            
        } else {

            $db2 = DatabaseConnection::getInstance();
            $checkstate="";

            // import opt-out trumps all other things!
            if (Importoptout::is_optout_email($email1) || Importoptout::is_optout_email($email2)) {
                return 0;
            }    
 
            if ($checkstate=="allow" || $checkstate=="")
            {
            
            //$queryError=0;     
            //try
            //{
            $sql = "INSERT INTO candidate(first_name,middle_name,last_name,email1,email2,phone_home,phone_cell,phone_work,address,city,state,zip,source,key_skills,date_available,current_employer,can_relocate,current_pay,desired_pay,notes,web_site,best_time_to_call,entered_by,owner,site_id,date_created,date_modified,eeo_ethnic_type_id,eeo_veteran_type_id,eeo_disability_status,eeo_gender,campaign_id,campaign_date)
                VALUES ('$firstName','$middleName','$lastName','$email1','$email2',ExtractNumber('$phoneHome'),ExtractNumber('$phoneCell'),ExtractNumber('$phoneWork'),'$address','$city','$state','$zip','$source','$keySkills','$dateAvailable','$currentEmployer',0,'$currentPay','$desiredPay','$notes','$webSite','$bestTimeToCall','$enteredBy','$owner','$siteid',now(),now(),'$race','$veteran','$disability','$gender','$camp_id',now())";

                $queryResult = $db2->query($sql);

                if (!$queryResult) {
                    error_log('QUERY ERROR: '.mysql_error($db2->_connection).' on query: '.$sql);
                }

                                $sql ="select candidate.email1, candidate.first_name, candidate.last_name, candidate.phone_home, candidate.date_created,dripmarket_compaign.cname,"
                                        . "dripmarket_configuration.smtphost,dripmarket_configuration.email_type,dripmarket_configuration.smtppassword ,"
                                        . "user.user_id, user.first_name as clientname,dripmarket_configuration.smtpport ,dripmarket_configuration.fromname,dripmarket_questionaire.id as quesid ,"
                                        . "dripmarket_configuration.fromemail, dripmarket_configuration.bccemail, dripmarket_configuration.smtpuser,"
                                        . "dripmarket_configuration.fromemail2, dripmarket_configuration.bccemail2, dripmarket_configuration.smtpuser2, dripmarket_configuration.smtphost2, dripmarket_configuration.email_type2,"
                                        . "dripmarket_configuration.smtppassword2, dripmarket_configuration.smtpport2, dripmarket_configuration.fromname2,dripmarket_configuration.toggle_smtp,"
                                        . "candidate.candidate_id,dripmarket_compaignemail.subject,dripmarket_compaignemail.creationdate,dripmarket_compaignemail.hour,dripmarket_compaignemail.min,dripmarket_compaignemail.Message,dripmarket_compaignemail.Timezone,dripmarket_compaignemail.sendemailon,dripmarket_compaign.id ,dripmarket_compaign.userid FROM `candidate` inner join user on user.user_id =candidate.entered_by INNER JOIN dripmarket_compaign ON user.user_id = dripmarket_compaign.userid inner JOIN dripmarket_compaignemail ON dripmarket_compaign.id = dripmarket_compaignemail.compaignid left JOIN dripmarket_questionaire ON dripmarket_questionaire.userid = dripmarket_compaign.userid inner join dripmarket_configuration on dripmarket_compaign.userid=dripmarket_configuration.userid where candidate.ishot=0 and dripmarket_compaign.`default`=1 and candidate.unsubscribe=0 and candidate.candidate_id=(select max(candidate_id) from candidate) and dripmarket_compaignemail.sendemailon=0 and candidate.campaign_id='0'";
                                $db3 = DatabaseConnection::getInstance();
                                $resourcedetail = $db3->query($sql);
                                
                                while ($row = mysql_fetch_array($resourcedetail))
                                {
                                        $Subject    = $row['subject'];
                                        $bodytext=stripslashes($row['Message']);
                                        $questionaireurl='<a href="'.questionaireurl.'?quesid='.$row['quesid'].'">Please click here</a>';
                                        $quesidencode=base64_encode($row['quesid']);
                                        $cidencode=base64_encode($row['candidate_id']);
                                        $candidate_id = $row['candidate_id'];
                                        $unsubscribeurl='To remove your name from our candidate list <a href="'.unsubscribeurl.'?quesid='.$quesidencode.'&cid='.$cidencode.'">Please click here</a>';
                                        $datefunc=date("d-M-y");
                                        $bodytext=str_replace('$$NAME$$',$row['first_name'],$bodytext);
                                        $bodytext=str_replace('$$FIRSTNAME$$',$row['first_name'],$bodytext);
                                        $bodytext=str_replace('$$LASTNAME$$',$row['last_name'],$bodytext);
                                        $bodytext=str_replace('$$EMAIL$$',$row['email1'],$bodytext);
                                        $bodytext=str_replace('$$PHONE$$',$row['phone_home'],$bodytext);
                                        $bodytext=str_replace('$$STARTDATE$$',$row['date_created'],$bodytext);
                                        $bodytext=str_replace('$$CURDATE$$',$datefunc,$bodytext);
                                        $bodytext=str_replace('$$QUESTIONNAIRE$$',$questionaireurl,$bodytext);
                                        $bodytext=str_replace('$$UNSUBSCRIBE$$',$unsubscribeurl,$bodytext);
                                        // import opt-out trumps all other things!
                                        if (Importoptout::is_optout_email($row['email1'])) {
                                          return 0;
                                        }
                                        
                                        try{
                                            file_put_contents('C:/Windows/Temp/ResufloLog/UserMailerCaller.txt', PHP_EOL.(new DateTime())->format('d m Y H:i:s').'===== START resumeparser', FILE_APPEND); 
                                            $email = new UserMailer($row['user_id']);
                                            $r = $email->SendEmail(array($row['email1']), null, $Subject, $bodytext);
                                            file_put_contents('C:/Windows/Temp/ResufloLog/UserMailerCaller.txt', '=> END ====='.$r, FILE_APPEND);
                                        }
                                        catch (Exception $e) {
                                            file_put_contents('C:/Windows/Temp/ResufloLog/UserMailerCaller.txt', (new DateTime())->format('d m Y H:i:s').'resumeparser ====='.$e->getMessage().PHP_EOL, FILE_APPEND);
                                        }
                                        
                                        if ($r === true) 
                                        {
                                            $sql123 = "INSERT INTO activity (data_item_id, data_item_type, joborder_id, entered_by, type, notes, site_id, date_created, date_modified )VALUES ( '$candidate_id', '100', '0', '$enteredBy', '200', '$Subject', '$siteid', NOW(), NOW() )";
                                            $db312 = DatabaseConnection::getInstance();
                                            $resourcedetailmn = $db312->query($sql123);

                                            // Econn updating candidate table notes starts
                                            $UpdateNotesQuery = mysql_query("UPDATE candidate SET activity_notes = '$Subject' WHERE candidate_id = '$candidate_id'");

                                        } else 
                                        {
                                            $output = 'candidate ('.$candidate_id.') email: '.$row['email1'].', from: '.$row['fromemail'].' ('.$row['fromname'].') ';
                                            $error = $r;
                                            $output .= ' ERROR:'.$error.PHP_EOL;

                                            if (!empty($output)) {
                                                $date = new DateTime();
                                                //$dripLogPath = str_replace('\que\\'.basename(__FILE__), '\tmp\\'.$date->format('d m Y').'DripLog.txt', __FILE__);
                                                $dripLogPath = 'C:\log\\DripLog '.$date->format('d m Y').'.txt';
                                                file_put_contents($dripLogPath, PHP_EOL.'===================================================       START '.$date->format('d m Y H:i:s').'     =================================================='.PHP_EOL, FILE_APPEND);
                                                file_put_contents($dripLogPath, $output, FILE_APPEND);
                                                file_put_contents($dripLogPath, PHP_EOL.'===================================================                   END               =================================================='.PHP_EOL, FILE_APPEND);
                                                //if (!mail($email_debug, 'jj-dripqueue-to-smtp: other '.$dtime, $output)) {
                                                        //echo 'ERROR SENDING drip OUTPUT: '.$output;
                                                //}
                                            }

                                            //$error = mysql_real_escape_string($error);
                                        }
                                }
                                        
                                $sql ="select candidate.email1, candidate.first_name, candidate.last_name, candidate.phone_home, candidate.date_created,dripmarket_compaign.cname,"
                                        . "dripmarket_configuration.smtphost,dripmarket_configuration.email_type,dripmarket_configuration.smtppassword ,user.user_id, user.first_name as clientname,"
                                        . "dripmarket_configuration.smtpport ,dripmarket_configuration.fromname,dripmarket_questionaire.id as quesid ,"
                                        . "dripmarket_configuration.fromemail,dripmarket_configuration.bccemail,dripmarket_configuration.smtpuser,"
                                        . "dripmarket_configuration.fromemail2, dripmarket_configuration.bccemail2, dripmarket_configuration.smtpuser2, dripmarket_configuration.smtphost2, dripmarket_configuration.email_type2,"
                                        . "dripmarket_configuration.smtppassword2, dripmarket_configuration.smtpport2, dripmarket_configuration.fromname2,dripmarket_configuration.toggle_smtp,"
                                        . "candidate.candidate_id,dripmarket_compaignemail.subject,dripmarket_compaignemail.creationdate,dripmarket_compaignemail.hour,dripmarket_compaignemail.min,dripmarket_compaignemail.Message,dripmarket_compaignemail.Timezone,dripmarket_compaignemail.sendemailon,dripmarket_compaign.id ,dripmarket_compaign.userid FROM `candidate` inner join user on user.user_id =candidate.entered_by INNER JOIN dripmarket_compaign ON user.user_id = dripmarket_compaign.userid inner JOIN dripmarket_compaignemail ON dripmarket_compaign.id = dripmarket_compaignemail.compaignid left JOIN dripmarket_questionaire ON dripmarket_questionaire.userid = dripmarket_compaign.userid inner join dripmarket_configuration on dripmarket_compaign.userid=dripmarket_configuration.userid where candidate.ishot=0 and dripmarket_compaign.`default`=0 and dripmarket_compaign.id= candidate.campaign_id and candidate.unsubscribe=0 and candidate.candidate_id=(select max(candidate_id) from candidate) and dripmarket_compaignemail.sendemailon=0 and candidate.campaign_id!='none' and candidate.campaign_id!='0'";
                                $db3 = DatabaseConnection::getInstance();
                                $resourcedetail = $db3->query($sql);
                                    
                                while ($row = mysql_fetch_array($resourcedetail))
                                {
                                        $Subject    = $row['subject'];
                                        $bodytext=stripslashes($row['Message']);
                                        $questionaireurl='<a href="'.questionaireurl.'?quesid='.$row['quesid'].'">Please click here</a>';
                                        $quesidencode=base64_encode($row['quesid']);
                                        $cidencode=base64_encode($row['candidate_id']);
                                        $candidate_id = $row['candidate_id'];
                                        $unsubscribeurl='To remove your name from our candidate list <a href="'.unsubscribeurl.'?quesid='.$quesidencode.'&cid='.$cidencode.'">Please click here</a>';
                                        $datefunc=date("d-M-y");
                                        $bodytext=str_replace('$$NAME$$',$row['first_name'],$bodytext);
                                        $bodytext=str_replace('$$FIRSTNAME$$',$row['first_name'],$bodytext);
                                        $bodytext=str_replace('$$LASTNAME$$',$row['last_name'],$bodytext);

                                        $bodytext=str_replace('$$EMAIL$$',$row['email1'],$bodytext);
                                        $bodytext=str_replace('$$PHONE$$',$row['phone_home'],$bodytext);
                                        $bodytext=str_replace('$$STARTDATE$$',$row['date_created'],$bodytext);
                                        $bodytext=str_replace('$$CURDATE$$',$datefunc,$bodytext);
                                        $bodytext=str_replace('$$QUESTIONNAIRE$$',$questionaireurl,$bodytext);
                                        $bodytext=str_replace('$$UNSUBSCRIBE$$',$unsubscribeurl,$bodytext);
                                        // import opt-out trumps all other things!
                                        if (Importoptout::is_optout_email($row['email1'])) {
                                          return 0;
                                        }
                                        try{
                                            file_put_contents('C:/Windows/Temp/ResufloLog/UserMailerCaller.txt', PHP_EOL.(new DateTime())->format('d m Y H:i:s').'===== START resumeparser', FILE_APPEND); 
                                            $email = new UserMailer($row['user_id']);
                                            $r = $email->SendEmail(array($row['email1']), null, $Subject, $bodytext);
                                            file_put_contents('C:/Windows/Temp/ResufloLog/UserMailerCaller.txt', '=> END ====='.$r, FILE_APPEND);
                                        }
                                        catch (Exception $e) {
                                            file_put_contents('C:/Windows/Temp/ResufloLog/UserMailerCaller.txt', (new DateTime())->format('d m Y H:i:s').'resumeparser ====='.$e->getMessage().PHP_EOL, FILE_APPEND);
                                        }
                                        if ($r === true) 
                                        {
                                            $sql123 = "INSERT INTO activity (data_item_id, data_item_type, joborder_id, entered_by, type, notes, site_id, date_created, date_modified )VALUES ( '$candidate_id', '100', '0', '$enteredBy', '200', '$Subject', '$siteid', NOW(), NOW() )";
                                            $db312 = DatabaseConnection::getInstance();
                                            $resourcedetailmn = $db312->query($sql123);

                                            // Econn updating candidate table notes starts
                                            $UpdateNotesQuery = mysql_query("UPDATE candidate SET activity_notes = '$Subject' WHERE candidate_id = '$candidate_id'");
                                        } else 
                                        {
                                            $output = 'candidate ('.$candidate_id.') email: '.$row['email1'].', from: '.$row['fromemail'].' ('.$row['fromname'].') ';
                                            $error = $r;
                                            $output .= ' ERROR:'.$error.PHP_EOL;

                                            if (!empty($output)) {
                                                $date = new DateTime();
                                                //$dripLogPath = str_replace('\que\\'.basename(__FILE__), '\tmp\\'.$date->format('d m Y').'DripLog.txt', __FILE__);
                                                $dripLogPath = 'C:\log\\DripLog '.$date->format('d m Y').'.txt';
                                                file_put_contents($dripLogPath, PHP_EOL.'===================================================       START '.$date->format('d m Y H:i:s').'     =================================================='.PHP_EOL, FILE_APPEND);
                                                file_put_contents($dripLogPath, $output, FILE_APPEND);
                                                file_put_contents($dripLogPath, PHP_EOL.'===================================================                   END               =================================================='.PHP_EOL, FILE_APPEND);
                                                //if (!mail($email_debug, 'jj-dripqueue-to-smtp: other '.$dtime, $output)) {
                                                        //echo 'ERROR SENDING drip OUTPUT: '.$output;
                                                //}
                                            }

                                            //$error = mysql_real_escape_string($error);
                                        }
                                    
                                }
                                
                         
                            
                                //        adding the php mailer end
                                
                                    //email set of after parsing is done
                                    $this->_countresumefile=1;
        /* }
        else
        {
                    $this->_reportmessage.="<tr><td style='padding:0px 5px 0px 5px'>".$this->_username."</td><td style='padding:0px 5px 0px 5px'>".$filenameerror."</td><td style='padding:0px 5px 0px 5px'>Resume Data Corrupted</td></tr>";
                    $this->_curruptfiles=$this->_curruptfiles+1;
        } */
        
        }else
            {
                
                $filenamedocext = explode(".",$filenamedoc);
                $countfilendocext=count($filenamedocext);
                $filext=$filenamedocext[$countfilendocext-1];
                $filenamedocnew='./backup/incorrectresume/'.time().".".$filext;
                
                
                if (file_exists($filenamedoc))
                {
                    //echo "exists";
                    
                }
                else
                {
                    //echo "not exists";
                    
                    
                }
                $bodytext="Please collect incorrect resume<br/><br/>";
                $bodytext.="<b>Incorrect Resume Message:</b><table border='2' style='padding:2px'><tr><td style='padding:0px 5px 0px 5px'>Client Name</td><td style='padding:0px 5px 0px 5px'>Filename</td><td style='padding:0px 5px 0px 5px'>Reason</td><td style='padding:0px 5px 0px 5px'>State</td></tr>$incorrectresumemsg</table><br/><br/>$parsedxml<br/>";
                
                try{
                    file_put_contents('C:/Windows/Temp/ResufloLog/UserMailerCaller.txt', PHP_EOL.(new DateTime())->format('d m Y H:i:s').'===== START resumeparser', FILE_APPEND);
                    $email = new UserMailer(1);
                    $result = $email->SendEmail(array(TechnicalReport), null, 'Incorrect Resume', $bodytext, $filenamedoc);
                    file_put_contents('C:/Windows/Temp/ResufloLog/UserMailerCaller.txt', '=> END ====='.$result, FILE_APPEND);
                }
                catch (Exception $e) {
                    file_put_contents('C:/Windows/Temp/ResufloLog/UserMailerCaller.txt', (new DateTime())->format('d m Y H:i:s').'resumeparser ====='.$e->getMessage().PHP_EOL, FILE_APPEND);
                }
                //send incorrect resume not containing state
                        
                if (!copy($filenamedoc,$filenamedocnew)) {
                    echo "failed to copy $file...\n";
                    
                }
                /* $queryError=0;
                try
                { */
                    // import opt-out trumps all other things!
                    if (Importoptout::is_optout_email($email1) || Importoptout::is_optout_email($email2)) {
                      return 0;
                    }
 
                    $sql = "INSERT INTO candidatenew(first_name,middle_name,last_name,email1,email2,phone_home,phone_cell,phone_work,address,city,state,zip,source,key_skills,date_available,current_employer,can_relocate,current_pay,desired_pay,notes,web_site,best_time_to_call,entered_by,owner,site_id,date_created,date_modified,eeo_ethnic_type_id,eeo_veteran_type_id,eeo_disability_status,eeo_gender)
                    VALUES ('$firstName','$middleName','$lastName','$email1','$email2',ExtractNumber('$phoneHome'),ExtractNumber('$phoneCell'),ExtractNumber('$phoneWork'),'$address','$city','$state','$zip','$source','$keySkills','$dateAvailable','$currentEmployer',0,'$currentPay','$desiredPay','$notes','$webSite','$bestTimeToCall','$enteredBy','$owner','$siteid',now(),now(),'$race','$veteran','$disability','$gender')";
                    //echo $sql."<br>";
                    $queryResult = $db2->query($sql);
                    /* if (!$queryResult)
                    {
                            throw new Exception("error"); 
                    }
                }
                catch(exception $e)
                {
                    $queryError=1;
                } */
            }
    
        
                //    $db5 = DatabaseConnection::getInstance();
                    //$queryResult12 = $db5->query($sql123);
    
    
    
            //code parser
            
            
            if ($checkstate=="notallow")
            {
            
                return -1;
            }
            
            //code parser
            
            
            $sql ="select candidate.candidate_id as ids from candidate where entered_by='$enteredBy' and email1='$email1' ";
            $db6 = DatabaseConnection::getInstance();
            $resourcedetail6 = $db6->query($sql);
            
            $rowdetail = mysql_fetch_assoc($resourcedetail6);
            $candidateID = $rowdetail['ids'];
            
      
            return $candidateID."candadd".$this->_countresumefile;
        }
    }
    
    /*
    *function to insert extra field data while uploading a zip
    */
    function InsertExtraFields($xmlArr,$candidateId,$db){
            //print_r($xmlArr);

        $candidateId = (integer) $candidateId;
        if (empty($candidateId)) return false;

        $db = DatabaseConnection::getInstance();
        $siteid = 1;
        if ($xmlArr['Category']!=''){
            $sql = "INSERT INTO extra_field (data_item_id,field_name,value,import_id,site_id,data_item_type) 
                values('".$candidateId."','Category','".$xmlArr['Category']."','0','".$siteid."','100')";
            
        $queryResult = $db->query($sql);
        }
        if ($xmlArr['DateOfBirth']!=''){
        $sql = "INSERT INTO extra_field (data_item_id,field_name,value,import_id,site_id,data_item_type) 
                values('".$candidateId."','Date Of Birth','".$xmlArr['DateOfBirth']."','0','".$siteid."','100')";
        $queryResult = $db->query($sql);
        }
        if ($xmlArr['Gender']!=''){
        $sql = "INSERT INTO extra_field (data_item_id,field_name,value,import_id,site_id,data_item_type) 
                values('".$candidateId."','Gender','".$xmlArr['Gender']."','0','".$siteid."','100')";
        $queryResult = $db->query($sql);
        }
        if ($xmlArr['FatherName']!=''){
        $sql = "INSERT INTO extra_field (data_item_id,field_name,value,import_id,site_id,data_item_type) 
                values('".$candidateId."','Father Name','".$xmlArr['FatherName']."','0','".$siteid."','100')";
        $queryResult = $db->query($sql);
        }
        if ($xmlArr['MotherName']!=''){
        $sql = "INSERT INTO extra_field (data_item_id,field_name,value,import_id,site_id,data_item_type) 
                values('".$candidateId."','Mother Name','".$xmlArr['MotherName']."','0','".$siteid."','100')";
        $queryResult = $db->query($sql);
        }
        if ($xmlArr['MaritalStatus']!=''){
        $sql = "INSERT INTO extra_field (data_item_id,field_name,value,import_id,site_id,data_item_type) 
                values('".$candidateId."','Marital Status','".$xmlArr['MaritalStatus']."','0','".$siteid."','100')";
        $queryResult = $db->query($sql);
        }
        if ($xmlArr['PassportNo']!=''){
        $sql = "INSERT INTO extra_field (data_item_id,field_name,value,import_id,site_id,data_item_type) 
                values('".$candidateId."','Passport No','".$xmlArr['PassportNo']."','0','".$siteid."','100')";
        $queryResult = $db->query($sql);
        }
        if ($xmlArr['Nationality']!=''){
        $sql = "INSERT INTO extra_field (data_item_id,field_name,value,import_id,site_id,data_item_type) 
                values('".$candidateId."','Nationality','".$xmlArr['Nationality']."','0','".$siteid."','100')";
        $queryResult = $db->query($sql);
        }
        if ($xmlArr['WorkedPeriod']!=''){
        $sql = "INSERT INTO extra_field (data_item_id,field_name,value,import_id,site_id,data_item_type) 
                values('".$candidateId."','Worked Period','".$xmlArr['WorkedPeriod']."','0','".$siteid."','100')";
        $queryResult = $db->query($sql);
        }
        if ($xmlArr['GapPeriod']!=''){
        $sql = "INSERT INTO extra_field (data_item_id,field_name,value,import_id,site_id,data_item_type) 
                values('".$candidateId."','Gap Period','".$xmlArr['GapPeriod']."','0','".$siteid."','100')";
        $queryResult = $db->query($sql);
        }
        if ($xmlArr['FaxNo']!=''){
        $sql = "INSERT INTO extra_field (data_item_id,field_name,value,import_id,site_id,data_item_type) 
                values('".$candidateId."','Fax No','".$xmlArr['FaxNo']."','0','".$siteid."','100')";
        $queryResult = $db->query($sql);
        }
        if ($xmlArr['LicenseNo']!=''){
        $sql = "INSERT INTO extra_field (data_item_id,field_name,value,import_id,site_id,data_item_type) 
                values('".$candidateId."','License No','".$xmlArr['LicenseNo']."','0','".$siteid."','100')";
        $queryResult = $db->query($sql);
        }
        if ($xmlArr['Hobbies']!=''){
        $sql = "INSERT INTO extra_field (data_item_id,field_name,value,import_id,site_id,data_item_type) 
                values('".$candidateId."','Hobbies','".$xmlArr['Hobbies']."','0','".$siteid."','100')";
        $queryResult = $db->query($sql);
        }
        if ($xmlArr['Qualification']!=''){
        $sql = "INSERT INTO extra_field (data_item_id,field_name,value,import_id,site_id,data_item_type) 
                values('".$candidateId."','Qualification','".$xmlArr['Qualification']."','0','".$siteid."','100')";
        $queryResult = $db->query($sql);
        }
        if ($xmlArr['Achievments']!=''){
        $sql = "INSERT INTO extra_field (data_item_id,field_name,value,import_id,site_id,data_item_type) 
                values('".$candidateId."','Achievements','".$xmlArr['Achievments']."','0','".$siteid."','100')";
        $queryResult = $db->query($sql);
        }
        if ($xmlArr['Objectives']!=''){
        $sql = "INSERT INTO extra_field (data_item_id,field_name,value,import_id,site_id,data_item_type) 
                values('".$candidateId."','Objectives','".$xmlArr['Objectives']."','0','".$siteid."','100')";
        $queryResult = $db->query($sql);
        }
        if ($xmlArr['Experience']!=''){
                        
        $experience=htmlentities($xmlArr['Experience']);
        //$experienceresult=str_replace('\r','newline111111111',$experience);
        $sql = "INSERT INTO extra_field (data_item_id,field_name,value,import_id,site_id,data_item_type) 
                values('".$candidateId."','Experience','".$experience."','0','".$siteid."','100')";
        $queryResult = $db->query($sql);
        }
        if ($xmlArr['References']!=''){
        $sql = "INSERT INTO extra_field (data_item_id,field_name,value,import_id,site_id,data_item_type) 
                values('".$candidateId."','References','".$xmlArr['References']."','0','".$siteid."','100')";
        $queryResult = $db->query($sql);
        }
        if ($xmlArr['JobProfile']!=''){
        $sql = "INSERT INTO extra_field (data_item_id,field_name,value,import_id,site_id,data_item_type) 
                values('".$candidateId."','Job Profile','".$xmlArr['JobProfile']."','0','".$siteid."','100')";
        $queryResult = $db->query($sql);
        }
        if ($xmlArr['DetailResume']!=''){
        $sql = "INSERT INTO extra_field (data_item_id,field_name,value,import_id,site_id,data_item_type) 
                values('".$candidateId."','Detail Resume','".mysql_real_escape_string($xmlArr['DetailResume'])."','0','".$siteid."','100')";
        $queryResult = $db->query($sql);
        }        
        if ($xmlArr['Licence No']!=''){
        $sql = "INSERT INTO extra_field (data_item_id,field_name,value,import_id,site_id,data_item_type) 
                values('".$candidateId."','Licence No','".$xmlArr['LicenseNo']."','0','".$siteid."','100')";
        $queryResult = $db->query($sql);
        }
        if ($xmlArr['languageknown']!=''){
        $sql = "INSERT INTO extra_field (data_item_id,field_name,value,import_id,site_id,data_item_type) 
                values('".$candidateId."','Language Known','".$xmlArr['languageknown']."','0','".$siteid."','100')";
        $queryResult = $db->query($sql);
        }
        //die();
    }
    
    function RemoveDirectory($dir){
            
        //rename($dir."/"."threeresumes",$dir."/"."fouresumes");                            
        $handle = @opendir($dir);
            while ($file = @readdir($handle)) 
            {
                if ($file!="." && $file!=".."){
                        //if (is_dir($file)){
                        //echo $file;                    
                        if ($handle2 = @opendir($dir."/".$file)){
                            while ($file2 = @readdir($handle2)) {
                                if ($file2!="." && $file2!=".."){
                                    //echo $file2;
                                    //if (is_dir($file2)){
                                        if ($handle3 = @opendir($dir."/".$file."/".$file2)){
                                            while ($file3 = @readdir($handle3)) {
                                                if ($file3!="." && $file3!=".."){
                                                    //echo $file3;
                                                    //if (is_dir($dir."/".$file."/".$file2."/".$file3)){
                                                        @rmdir($dir."/".$file."/".$file2."/".$file3);
                                                    //}            
                                                    
                                                }
                                            }                                            
                                        }    
                                        //if (is_dir($dir."/".$file."/".$file2)){        
                                        @rmdir($dir."/".$file."/".$file2);
                                        //}                        
                                    //}
                                }                                
                            }
                        }
                        //if (is_dir($dir."/".$file)){
                        @rmdir($dir."/".$file);
                        //}
                    //}    
                }
            
            }
            @rmdir($dir);

    }
    
    
    function removedir($dirname)
    {
    
        //echo $dirname."<br>";
        
        if (is_dir($dirname))
        $dir_handle = opendir($dirname);
        if (!$dir_handle)
        return false;
        while ($file = readdir($dir_handle)) {
            if ($file != "." && $file != "..") {
                if (!is_dir($dirname."/".$file)){
        
        
                unlink($dirname."/".$file);
        }
                else
                {
                    $a=$dirname.'/'.$file;
                    removedir($a);
                }
            }
        }
        closedir($dir_handle);
        rmdir($dirname);
        return true;
    }




}
