<?php

include_once('lib/DatabaseConnection2.php');
include_once('config.php');

$db = DatabaseConnection::getInstance();

if (empty($_GET['date'])) {
  $_GET['date'] = date('Y-m-d H');
}
$date1 = urldecode($_GET['date']).':00:00';
$date2 = urldecode($_GET['date'].':59:59');

$sql = '
select
  c.date_created,
  dce.sendemailon,
  dce.hour,
  dce.min,
  date_format(date_add(convert_tz(concat(date_format(c.date_created, "%Y-%m-%d"), " ", lpad(dce.hour,"2","0"),":",lpad(dce.min,"2","0"), ":00"), replace(replace(dce.timezone, "GMT", ""), ".", ":"), "-6:0"), interval dce.sendemailon day), "%Y-%m-%d %H:%i:%m") as scheduledDate,
  cj.candidate_joborder_id,
  u.user_id, u.user_name,
  c.candidate_id, c.email1,
  dce.subject,
  dce.Timezone
from
  candidate c left join
  candidate_joborder cj on c.candidate_id = cj.candidate_id left join
  user u on u.user_id = c.entered_by left join
  dripmarket_compaign dc on u.user_id = dc.userid left join
  dripmarket_compaignemail dce on dc.id = dce.compaignid left join
  dripmarket_questionaire dq on dq.userid = dc.userid left join
  dripmarket_configuration dcfg on dc.userid=dcfg.userid
where
  cj.candidate_joborder_id is null and
  dce.sendemailon <> 0 and
  cast(date_format(date_add(convert_tz(concat(date_format(c.date_created, "%Y-%m-%d"), " ", lpad(dce.hour,"2","0"),":",lpad(dce.min,"2","0"), ":00"), replace(replace(dce.timezone, "GMT", ""), ".", ":"), "-6:0"), interval dce.sendemailon day), "%Y-%m-%d %H:%i") as datetime) between cast("'.$date1.'" as datetime) and cast("'.$date2.'" as datetime) and
  c.ishot = 0 and
  c.unsubscribe <> 1 and
  c.follow_up <> 1 and
  c.campaign_id <> "none"
order by
  u.user_id, dce.subject,
  c.candidate_id desc
';


$drips = $db->getAllAssoc($sql);
die('<strong>'.count($drips).'</strong> are set to receive drip campaign emails today:<pre>'.print_r($drips,true));
