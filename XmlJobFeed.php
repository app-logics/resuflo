<?php
$xml = new SimpleXMLElement('<xml/>');

error_reporting(E_ERROR | E_PARSE);
/* 
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
//http://localhost/resuflodevelopment/careers/index.php?m=careers&p=applyToJob&ID=227&uid=1567&siteId=2
include "asset/config.php";
include "config_not_change.php";
include "Setting.php";
include "./lib/RESUFLOUtility.php";
if(isset($_GET['feed']))
{
    $feed=$_GET['feed'];
}
if(strtoupper($feed) == "JC"){
    $source = $xml->addChild('source');
    $source->addChild("publisher", "ResufloCRM");
    $source->addChild("publisherurl", "https://resuflocrm.com");
    $source->addChild("lastbuilddate", date('D, d M Y H:i:s')." GMT");
    foreach(\Configuration\Setting::Databases as $currentDb){
        $db = mysql_select_db($currentDb);
        $sql ="SELECT j.title Title, j.joborder_id, j.description, j.salary, j.type, j.entered_by job_owner, u.city, u.state, u.country, u.company, u.zip_code, u.*, max(jp.dateCreated) PostDate FROM joborder j left join job_post jp on j.joborder_id = jp.jobOrderId left join user u on j.entered_by = u.user_id WHERE j.status='Active' and u.access_level != 0 and u.site_id is not null group by j.joborder_id, jp.jobOrderId";
        $jobs= mysql_query($sql);
        while ($job = mysql_fetch_array($jobs)) {
            $xmlJob = $source->addChild("job");
            $xmlJob->addChild("title", $job['Title']);
            $xmlJob->addChild("date", $job['PostDate']);
            $xmlJob->addChild("referencenumber", "JC-".$job['site_id']."-".$job['joborder_id']);
            $xmlJob->addChild("url", RESUFLOUtility::getAbsoluteURI()."careers/index.php?m=careers&amp;p=applyToJob&amp;ID=".$job['joborder_id']."&amp;uid=".$job['job_owner']."&amp;siteId=".$job['site_id']."&amp;jobBoardId=1");
            $xmlJob->addChild("company", $job['company']);
            $xmlJob->addChild("postalcode", $job['zip_code']);
            $xmlJob->addChild("description", htmlspecialchars($job['description']));
            $xmlJob->addChild("experience", "");
            $xmlJob->addChild("salary", $job['salary']);
            $xmlJob->addChild("education", "");
            $xmlJob->addChild("jobtype", $job['type']);
            $xmlJob->addChild("phone", $job['phone_work']);
            $xmlJob->addChild("email", $job['email']);
            $xmlJob->addChild("city", $job['city']);
            $xmlJob->addChild("state", $job['state']);
            $xmlJob->addChild("country", $job['country']);
            $xmlJob->addChild("category", $job['type']);
        }
    }
    Header('Content-type: text/xml');
    print($xml->asXML());
}
?>