<?php
/**
 * EasyPeasyICS Simple ICS/vCal data generator.
 * @author Marcus Bointon <phpmailer@synchromedia.co.uk>
 * @author Manuel Reinhard <manu@sprain.ch>
 *
 * Built with inspiration from
 * http://stackoverflow.com/questions/1463480/how-can-i-use-php-to-dynamically-publish-an-ical-file-to-be-read-by-google-calend/1464355#1464355
 * History:
 * 2010/12/17 - Manuel Reinhard - when it all started
 * 2014 PHPMailer project becomes maintainer
 */

/**
 * Class EasyPeasyICS.
 * Simple ICS data generator
 * @package phpmailer
 * @subpackage easypeasyics
 */
class EasyPeasyICS
{
    /**
     * The name of the calendar
     * @type string
     */
	public $location = '';
	
	public $sequence = 0;
	
	public $status = ''; 
	
	public $startTime = '';
	
	public $endTIme = '';
	
	public $summary = '';
	
	public $description = '';

	public $uid = ''; 
	
	public $url = ''; 
	
    protected $calendarName;

    protected $timezoneValue = '';
    /**
     * The array of events to add to this calendar
     * @type array
     */
    protected $events = array();

    /**
     * Constructor
     * @param string $calendarName
     */
    public function __construct($calendarName = "")
    {
        $this->calendarName = $calendarName;
    }

    /**
     * Add an event to this calendar.
     * @param string $start The start date and time as a unix timestamp
     * @param string $end The end date and time as a unix timestamp
     * @param string $summary A summary or title for the event
     * @param string $description A description of the event
     * @param string $url A URL for the event
     * @param string $uid A unique identifier for the event - generated automatically if not provided
     * @return array An array of event details, including any generated UID
     */
    public function addEvent()
    {
        if (empty($uid)) {
            $uid = md5(uniqid(mt_rand(), true)) . '@EasyPeasyICS';
        }
        $event = array(
           'start' => gmdate('Ymd', $this->startTime) ,
            'end' => gmdate('Ymd', $this->endTime) . 'T' . gmdate('His', $this->endTime) . 'Z',
            'summary' => $this->summary,
            'description' => $this->description,
            'url' => $this->url,
            'uid' => $this->uid,
            'location' => $this->location,
            'timezoneValue' => $this->timezoneValue
        );
        $this->events[] = $event;
        return $event;
    }

    /**
     * @return array Get the array of events.
     */
    public function getEvents()
    {
        return $this->events;
    }

    /**
     * Clear all events.
     */
    public function clearEvents()
    {
        $this->events = array();
    }

    /**
     * Get the name of the calendar.
     * @return string
     */
    public function getName()
    {
        return $this->calendarName;
    }

    /**
     * Set the name of the calendar.
     * @param $name
     */
    public function setName($name)
    {
        $this->calendarName = $name;
    }

    /**
     * Render and optionally output a vcal string.
     * @param bool $output Whether to output the calendar data directly (the default).
     * @return string The complete rendered vlal
     */
    public function render($output = true, $TimeZone = NULL, $offset = NULL)
    {

//Add header
        $ics = 'BEGIN:VCALENDAR
PRODID:Microsoft Exchange Server 2010
VERSION:2.0
CALSCALE:GREGORIAN
METHOD:REQUEST
BEGIN:VTIMEZONE
TZID:'.$TimeZone.'
BEGIN:STANDARD
DTSTART:'.$this->startTime.'
TZOFFSETFROM:'.$offset.'
TZOFFSETTO:'.$offset.'
END:STANDARD
BEGIN:DAYLIGHT
DTSTART:'.$this->startTime.'
TZOFFSETFROM:'.$offset.'
TZOFFSETTO:'.$offset.'
END:DAYLIGHT
END:VTIMEZONE';
//X-WR-CALNAME:' . $this->calendarName;
//PRODID:-//Microsoft Corporation//Outlook 15.0 MIMEDIR//EN
        //Add events
        foreach ($this->events as $event) {
            $ics .= '
BEGIN:VEVENT
CREATED:' . date("Ymd\THis", strtotime($event['start'])).'Z
UID:' . date("Ymd") . 'T' . date('His') . '-' . rand() . "-www.resuflocrm.com" . '
DTSTART;TZID="'.$TimeZone.'":' .  $event['start'] . '
DTEND;TZID="'.$TimeZone.'":' . $event['end'] . '
DTSTAMP:' . gmdate('Ymd') . 'T' . gmdate('His') . 'Z
TRANSP:TRANSPARENT
SEQUENCE:'.$this->sequence.'
STATUS:CONFIRMED'.'
CATEGORIES:MEETING
URL;VALUE=URI:www.resuflocrm.com
SUMMARY:' . str_replace("\n", "\\n", $event['summary']) . '
LOCATION:' . str_replace("\n", "\\n", $event['location']) .
//'DESCRIPTION:' . str_replace("\n", "\\n", $event['description']) . '
'DESCRIPTION:Invitation by Resuflocrm!
CLASS:PUBLIC
PRIORITY:5
LOCATION;LANGUAGE=en-US:' . str_replace("\n", "\\n", $event['location']) . '
X-MICROSOFT-CDO-APPT-SEQUENCE:0
X-MICROSOFT-CDO-BUSYSTATUS:TENTATIVE
X-MICROSOFT-CDO-INTENDEDSTATUS:BUSY
X-MICROSOFT-CDO-ALLDAYEVENT:FALSE
X-MICROSOFT-CDO-IMPORTANCE:1
X-MICROSOFT-CDO-INSTTYPE:0
X-MICROSOFT-DONOTFORWARDMEETING:FALSE
X-MICROSOFT-DISALLOW-COUNTER:FALSE
BEGIN:VALARM
TRIGGER:-PT15M
ACTION:DISPLAY
DESCRIPTION:Reminder
END:VALARM
END:VEVENT';
        }

        //Add footer
        $ics .= '
END:VCALENDAR';

//        if ($output) {
//            //Output
//            $filename = $this->calendarName;
//            //Filename needs quoting if it contains spaces
//            if (strpos($filename, ' ') !== false) {
//                $filename = '"'.$filename.'"';
//            }
//            header('Content-type: text/calendar; charset=utf-8');
//            header('Content-Disposition: inline; filename=schedule.ics');
//            //echo $ics;
//        }
        return $ics;
    }
}
