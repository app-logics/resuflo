<?php
        include_once('./OutlookMailer/PHPMailerAutoload.php');
	
	function AddEventToOutlook($smtphost, $smtpport, $smtpUsername, $smtpPassword, $emailSender, $fromName, $emailType, $bccemail, $To, $Subject, $Location, $StartTime, $EndTime, $allDay, $Description, $Body , $TimeZone = NULL , $offset = NULL)
	{
	    if($allDay != true){
			include_once('./OutlookMailer/extras/EasyPeasyICS.php');
		}
		else{
			include_once('./OutlookMailer/extras/AllDayEasyPeasyICS.php');
		}
		$mail = new PHPCalendarMailer();
		
		try {
 			$mail->isSMTP();
			$mail->Host = $smtphost;
			$mail->SMTPAuth = true;    
			$mail->Username = $smtpUsername; // SMTP username
			$mail->Password = $smtpPassword;
                        if (!empty($bccemail)) $mail->AddBcc( $bccemail );
                        if($emailType!='none') {
                            $mail->SMTPSecure =$emailType;
                        }
			//$mail->SMTPSecure = 'ssl'; // Enable encryption, 'ssl' also accepted
			$mail->Port = $smtpport; 
			
			$mail->isHTML(true);
			$mail->setFrom($emailSender, $fromName);
			$mail->addAddress($To);
			$mail->Subject = $Subject;
			
			$invite = new EasyPeasyICS();
			
			$invite->location = $Location;
			
			
			$invite->startTime = strtotime($StartTime);
			if($allDay != true){
				$invite->endTime = strtotime($EndTime);
			}
			
			$invite->summary = $Subject;
			$invite->description = $Description;

			$invite->addEvent();

			$mail->Body = $Body; 
			$mail->AltBody = "TEST CALENDAR";
			$mail->Ical = $invite->render(true, $TimeZone, $offset);
                        
                        $mail->SMTPAutoTLS = false;
                        $mail->SMTPOptions = array(
                            'ssl' => array(
                                'verify_peer' => false,
                                'verify_peer_name' => false,
                                'allow_self_signed' => true
                            )
                        );
                        //date_default_timezone_set('Asia/Karachi');
                        if(!empty($mail->ErrorInfo)){
                            return $mail->ErrorInfo."<br/>".PHP_EOL;
                        }
			if($mail->send())
			{
				//echo $returnUrl;
				//header($returnUrl);
			   return "<div>Outlook Sync: Event added to outlook</div>".PHP_EOL;
			}
			else
			{
                            return "<div>Outlook Sync:".$mail->ErrorInfo."</div>".PHP_EOL;
			}
		} catch (phpmailerException $e) {
			return $e->errorMessage(); //Pretty error messages from PHPMailer
		} catch (Exception $e) {
			return $e->getMessage(); //Boring error messages from anything else!
		}
	}
	//'2015-07-02T10:15:00+05:00'
?>