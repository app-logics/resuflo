create table processzip_log (
  id integer unsigned primary key auto_increment,
  filename varchar(255),
  emailalert varchar(255),
  userid integer unsigned,
  returnMsg text,
  processing_started datetime,
  processing_ended datetime
);

create table import_optout (
  id integer unsigned primary key auto_increment,
  email varchar(255) unique,
  user_id integer unsigned,
  dateposted datetime,
  index (user_id)
);

alter table user add column is_nyl bool default 0 after is_test_user;

alter table candidate add column campaign_date timestamp after campaign_id;
update candidate set campaign_date = date_created;

delimiter // create trigger campaign_date before update on candidate for each row begin if NEW.campaign_id <> OLD.campaign_id then set NEW.campaign_date = now(); end if; end; //

create table drip_queue (
  id bigint primary key auto_increment,
  scheduledDate datetime,
  candidate_id integer,
  campaign_id integer,
  entered_by integer,
  sent bool default 0,
  smtp_id varchar(255),
  smtp_error text,
  date_created datetime,
  date_modified datetime
);
